import 'dart:ffi';

import 'package:ctour/tool/OwnColors.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';

import 'autolink_utils.dart';
import 'highlighted_text_span.dart';
import 'link_attr.dart';
import 'selectable_text.dart' as my show SelectableText;
import 'selectable_text.dart' hide SelectableText;
import 'tap_and_long_press.dart';
import 'text_element.dart';

typedef OnOpenLinkFunction = void Function(String link);
typedef OnTransformLinkFunction = String Function(String link);
typedef OnTransformLinkAttributeFunction = LinkAttribute Function(String text);
typedef OnDebugMatchFunction = void Function(Match match);

class SelectableAutoLinkText extends StatefulWidget {
  /// Text to be auto link
  final String text;

  /// Regular expression for link
  /// If null, defaults RegExp([AutoLinkUtils._defaultLinkRegExpPattern]).
  final RegExp linkRegExp;

  /// Transform the display of Link
  /// Called when Link is displayed
  final OnTransformLinkAttributeFunction? onTransformDisplayLink;

  /// Called when the user taps on link.
  final OnOpenLinkFunction? onTap;

  /// Called when the user long-press on link.
  final OnOpenLinkFunction? onLongPress;

  /// Called when the user taps on non-link.
  final GesturePointCallback? onTapOther;

  /// Called when the user long-press on non-link.
  final GesturePointCallback? onLongPressOther;

  /// Style of link text
  final TextStyle? linkStyle;

  /// Style of highlighted link text
  final TextStyle? highlightedLinkStyle;

  /// {@macro flutter.material.SelectableText.focusNode}
  final FocusNode? focusNode;

  /// {@macro flutter.material.SelectableText.style}
  final TextStyle? style;

  /// {@macro flutter.widgets.editableText.strutStyle}
  final StrutStyle? strutStyle;

  /// {@macro flutter.widgets.editableText.textAlign}
  final TextAlign? textAlign;

  /// {@macro flutter.widgets.editableText.textDirection}
  final TextDirection? textDirection;

  /// {@macro flutter.widgets.editableText.textScaleFactor}
  final double? textScaleFactor;

  /// {@macro flutter.widgets.editableText.autofocus}
  final bool autofocus;

  /// {@macro flutter.widgets.editableText.minLines}
  final int? minLines;

  /// {@macro flutter.widgets.editableText.maxLines}
  final int? maxLines;

  /// {@macro flutter.widgets.editableText.selectionControls}
  final TextSelectionControls? selectionControls;

  /// {@macro flutter.widgets.editableText.showCursor}
  final bool showCursor;

  /// {@macro flutter.widgets.editableText.cursorWidth}
  final double cursorWidth;

  /// {@macro flutter.widgets.editableText.cursorHeight}
  final double? cursorHeight;

  /// {@macro flutter.widgets.editableText.cursorRadius}
  final Radius? cursorRadius;

  /// {@macro flutter.material.SelectableText.cursorColor}
  final Color? cursorColor;

  /// {@macro flutter.widgets.editableText.enableInteractiveSelection}
  final bool enableInteractiveSelection;

  /// {@macro flutter.widgets.scrollable.dragStartBehavior}
  final DragStartBehavior dragStartBehavior;

  /// {@macro flutter.material.SelectableText.toolbarOptions}
  final ToolbarOptions? toolbarOptions;

  /// {@macro flutter.widgets.editableText.scrollPhysics}
  final ScrollPhysics? scrollPhysics;

  /// {@macro flutter.painting.textPainter.textWidthBasis}
  final TextWidthBasis? textWidthBasis;

  /// {@macro flutter.widgets.editableText.onSelectionChanged}
  final SelectionChangedCallback? onSelectionChanged;

  /// For debugging linkRegExp
  final OnDebugMatchFunction? onDebugMatch;
  final String? moreString;
  bool moreBool = false ;

  final Function(bool val)? callback;

  SelectableAutoLinkText(
    this.text, {
    Key? key,
    String? linkRegExpPattern,
    this.onTransformDisplayLink,
    this.onTap,
    this.onLongPress,
    this.onTapOther,
    this.onLongPressOther,
    this.linkStyle,
    this.highlightedLinkStyle,
    this.focusNode,
    this.style,
    this.strutStyle,
    this.textAlign,
    this.textDirection,
    this.textScaleFactor,
    this.autofocus = false,
    this.minLines,
    this.maxLines,
    this.selectionControls,
    this.showCursor = false,
    this.cursorWidth = 2.0,
    this.cursorHeight,
    this.cursorRadius,
    this.cursorColor,
    this.enableInteractiveSelection = true,
    this.dragStartBehavior = DragStartBehavior.start,
    this.toolbarOptions,
    this.scrollPhysics,
    this.textWidthBasis,
    this.onSelectionChanged,
    this.onDebugMatch,
        this.callback,
        this.moreString,
        required this.moreBool,
  })  : linkRegExp =
            RegExp(linkRegExpPattern ?? AutoLinkUtils.defaultLinkRegExpPattern),
        super(key: key);

  @override
  _SelectableAutoLinkTextState createState() => _SelectableAutoLinkTextState();
}

class _SelectableAutoLinkTextState extends State<SelectableAutoLinkText> {
  final _gestureRecognizers = <TapAndLongPressGestureRecognizer>[];

  @override
  void dispose() {
    _clearGestureRecognizers();
    super.dispose();
  }

  bool _readMore = false;
  String _kEllipsis = '\u2026';

  void _onTapLink() {
    print("_onTapLink");
    setState(() {
      _readMore = !_readMore;
      widget.callback?.call(_readMore);
    });
  }

  @override
  Widget build(BuildContext context) {

    if(widget.moreBool) {
      TextSpan link = TextSpan(
        text: widget.moreString,

        // recognizer: TapGestureRecognizer()..onTap = _onTapLink,
      );
      TextSpan _delimiter = TextSpan(
        text: !_readMore
            ? true
            ? _kEllipsis + " "
            : ''
            : '',
        // style: _defaultDelimiterStyle,
        // recognizer: TapGestureRecognizer()..onTap = _onTapLink,
      );
      print("_generateElements2");

      Widget result = LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            print("_generateElements4");

            assert(constraints.hasBoundedWidth);
            final double maxWidth = constraints.maxWidth;
            print("maxWidth " + maxWidth.toString());
            print("widget.maxLines  " + widget.maxLines.toString());
            // Create a TextSpan with data
            final text2 = TextSpan(
              children: [
                // if (preTextSpan != null) preTextSpan,
                TextSpan(text: widget.text),
                // if (postTextSpan != null) postTextSpan
              ],
            );

            // Layout and measure link

            TextPainter textPainter = TextPainter(
              text: link,
              textAlign: TextAlign.start,
              textDirection: Directionality.of(context),
              textScaleFactor: MediaQuery.textScaleFactorOf(context),
              maxLines: widget.maxLines! - 1,
              ellipsis: _kEllipsis + " ",

              // locale: locale,
            );

            textPainter.layout(minWidth: 0, maxWidth: maxWidth);
            final linkSize = textPainter.size;
            print("linkSize " + linkSize.toString());

            // Layout and measure delimiter
            textPainter.text = _delimiter;
            textPainter.layout(minWidth: 0, maxWidth: maxWidth);
            final delimiterSize = textPainter.size;
            print("delimiterSize " + delimiterSize.toString());

            // Layout and measure text
            textPainter.text = text2;
            textPainter.layout(
                minWidth: constraints.minWidth, maxWidth: maxWidth);
            final textSize = textPainter.size;
            print("textSize " + textSize.toString());

            // Get the endIndex of data
            bool linkLongerThanLine = false;
            int endIndex;

            if (linkSize.width < maxWidth) {
              final readMoreSize = linkSize.width + delimiterSize.width;
              print("readMoreSize " + readMoreSize.toString());

              final pos = textPainter.getPositionForOffset(Offset(
                textSize.width - readMoreSize,
                textSize.height,
              ));
              endIndex = textPainter.getOffsetBefore(pos.offset) ?? 0;
            } else {
              var pos = textPainter.getPositionForOffset(
                textSize.bottomLeft(Offset.zero),
              );
              endIndex = pos.offset;
              linkLongerThanLine = true;
            }
            //
            print("endIndex " + endIndex.toString());
            // print ("endIndex " + widget.text[endIndex].toString());
            // print ("endIndex " + widget.text[endIndex+1].toString());
            // print ("endIndex " + widget.text[endIndex+2].toString());
            // print ("endIndex " + widget.text[endIndex+3].toString());
            // print ("endIndex " + widget.text[endIndex+4].toString());

            var textSpan;
            String text_sub = widget.text;

            if (endIndex != 0) {
              text_sub = widget.text.substring(0, endIndex + 1);
            } else {

            }
            // print ("text_sub " + text_sub.toString());
            // print ("text_sub.length " + text_sub.length.toString());
            // print ("text_sub.substring(0,text_sub.length-1) " + text_sub[text_sub.length-3].toString());

            // if (text_sub.substring(text_sub.length) == "\n"){
            //   text_sub = text_sub.substring(0,text_sub.length-1);
            // }
            // if(text_sub)

            // print ("text_sub " + text_sub.toString());


            List<TextSpan> l = _createTextSpans(text_sub);

            TextStyle d = GoogleFonts.inter(
              height: 1.3,
              fontSize: 16.nsp,
              color: OwnColors.black,
              fontWeight: FontWeight.w500,

            );
            TextSpan link2 = HighlightedTextSpan(
              text: widget.moreString,
              style: d,
              highlightedStyle: null,
              recognizer: TapGestureRecognizer()
                ..onTap = _onTapLink,

            );
            TextSpan _delimiter2 = HighlightedTextSpan(
              text: !_readMore
                  ? true
                  ? _kEllipsis + " "
                  : ''
                  : '',
              style: d,
              highlightedStyle: null,
              recognizer: null,
            );


            if (textPainter.didExceedMaxLines) {
              l.add(_delimiter2);
              l.add(link2);
            } else {

            }

            Widget sel = my.SelectableText.rich(
              TextSpan(
                children: l,
              ),
              focusNode: widget.focusNode,
              style: widget.style,
              strutStyle: widget.strutStyle,
              textAlign: widget.textAlign,
              textDirection: widget.textDirection,
              textScaleFactor: widget.textScaleFactor,
              autofocus: widget.autofocus,
              minLines: widget.minLines,
              // maxLines: widget.maxLines,
              selectionControls: widget.selectionControls,
              showCursor: widget.showCursor,
              cursorWidth: widget.cursorWidth,
              cursorHeight: widget.cursorHeight,
              cursorRadius: widget.cursorRadius,
              cursorColor: widget.cursorColor,
              enableInteractiveSelection: widget.enableInteractiveSelection,
              dragStartBehavior: widget.dragStartBehavior,
              toolbarOptions: widget.toolbarOptions,
              scrollPhysics: widget.scrollPhysics,
              textWidthBasis: widget.textWidthBasis,
              onTap: widget.onTapOther,
              onLongPress: widget.onLongPressOther,
              onSelectionChanged: widget.onSelectionChanged,
            );

            return sel
            ;
          });
      return result;
    }else {
      return my.SelectableText.rich(
        TextSpan(
          children: _createTextSpans(widget.text),
        ),
        focusNode: widget.focusNode,
        style: widget.style,
        strutStyle: widget.strutStyle,
        textAlign: widget.textAlign,
        textDirection: widget.textDirection,
        textScaleFactor: widget.textScaleFactor,
        autofocus: widget.autofocus,
        minLines: widget.minLines,
        maxLines: widget.maxLines,
        selectionControls: widget.selectionControls,
        showCursor: widget.showCursor,
        cursorWidth: widget.cursorWidth,
        cursorHeight: widget.cursorHeight,
        cursorRadius: widget.cursorRadius,
        cursorColor: widget.cursorColor,
        enableInteractiveSelection: widget.enableInteractiveSelection,
        dragStartBehavior: widget.dragStartBehavior,
        toolbarOptions: widget.toolbarOptions,
        scrollPhysics: widget.scrollPhysics,
        textWidthBasis: widget.textWidthBasis,
        onTap: widget.onTapOther,
        onLongPress: widget.onLongPressOther,
        onSelectionChanged: widget.onSelectionChanged,
      );
    }
  }

  TextSpan _buildData({
    required String data,
    TextStyle? textStyle,
    TextStyle? linkTextStyle,
    ValueChanged<String>? onPressed,
    required List<TextSpan> children,
  }) {
    RegExp exp = RegExp(r'(?:(?:https?|ftp):\/\/)?[\w/\-?=%.]+\.[\w/\-?=%.]+');

    List<TextSpan> contents = [];

    while (exp.hasMatch(data)) {
      final match = exp.firstMatch(data);

      final firstTextPart = data.substring(0, match!.start);
      final linkTextPart = data.substring(match.start, match.end);

      contents.add(
        TextSpan(
          text: firstTextPart,
        ),
      );
      contents.add(
        TextSpan(
          text: linkTextPart,
          style: linkTextStyle,
          recognizer: TapGestureRecognizer()
            ..onTap = () => onPressed?.call(
              linkTextPart.trim(),
            ),
        ),
      );
      data = data.substring(match.end, data.length);
    }
    contents.add(
      TextSpan(
        text: data,
      ),
    );
    return TextSpan(
      children: contents..addAll(children),
      style: textStyle,
    );
  }
  List<TextElement> _generateElements(String text) {
    print("_generateElements");
    print("text.isNotEmpty " + text.isNotEmpty.toString());
    if (text.isNotEmpty != true) return [];

    final elements = <TextElement>[];

    final matches = widget.linkRegExp.allMatches(text);







    if (matches.isEmpty) {
      elements.add(TextElement(
        type: TextElementType.text,
        text: text,
      ));
    } else {
      var index = 0;
      matches.forEach((match) {
        if (widget.onDebugMatch != null) {
          widget.onDebugMatch!(match);
        }

        if (match.start != 0) {
          elements.add(TextElement(
            type: TextElementType.text,
            text: text.substring(index, match.start),
          ));
        }
        elements.add(TextElement(
          type: TextElementType.link,
          text: match.group(0) ?? '',
        ));
        index = match.end;
      });

      if (index < text.length) {
        elements.add(TextElement(
          type: TextElementType.text,
          text: text.substring(index),
        ));
      }
    }

    return elements;
  }

  List<TextSpan> _createTextSpans(String text_sub) {
    _clearGestureRecognizers();


    return _generateElements(text_sub).map(
      (e) {
        var isLink = e.type == TextElementType.link;
        final linkAttr = (isLink && widget.onTransformDisplayLink != null)
            ? widget.onTransformDisplayLink!(e.text)
            : null;
        final link = linkAttr != null ? linkAttr.link : e.text;
        isLink &= link != null;

        return HighlightedTextSpan(
          text: linkAttr?.text ?? e.text,
          style: linkAttr?.style ?? (isLink ? widget.linkStyle : widget.style),
          highlightedStyle: isLink
              ? (linkAttr?.highlightedStyle ?? widget.highlightedLinkStyle)
              : null,
          recognizer: isLink ? _createGestureRecognizer(link!) : null,
        );
      },
    ).toList();
  }

  TapAndLongPressGestureRecognizer? _createGestureRecognizer(String link) {
    if (widget.onTap == null && widget.onLongPress == null) {
      return null;
    }
    final recognizer = TapAndLongPressGestureRecognizer();
    _gestureRecognizers.add(recognizer);
    recognizer.onTap = () => widget.onTap?.call(link);
    recognizer.onLongPress = () => widget.onLongPress?.call(link);

    return recognizer;
  }

  void _clearGestureRecognizers() {
    _gestureRecognizers.forEach((r) => r.dispose());
    _gestureRecognizers.clear();
  }
}
