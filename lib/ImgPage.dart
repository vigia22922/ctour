import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ctour/res.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/tool/StatusTools.dart';
import 'package:ctour/tool/Tools.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:image_picker/image_picker.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:photo_view/photo_view.dart';


class  ImgPage extends StatefulWidget {
  final String url;



  const ImgPage({
    Key key,
    this.url,


  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ImgPageState();
  }
}

class ImgPageState extends State<ImgPage> {



  @override
  void initState() {
    super.initState();

    StatusTools().setTranBGWhiteText();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: OwnColors.black,

      body: GestureDetector(
        // onVerticalDragEnd: (details){
        //   backClick();
        // },
        child: Center(
          child: FocusDetector(
            onFocusGained: () {
              print("onFocusGained");
            },
            onForegroundLost: () {
              print("onForegroundLost");
            },
            child: Container(
              width: ScreenSize().getWidth(context),
              height: double.infinity,
              color: OwnColors.black,
              child: Stack(
                children: [


                  Positioned.fill(
                    child: widget.url.contains("http")?
                    CachedNetworkImage(
                      imageUrl: widget.url,
                      imageBuilder: (context, imageProvider) => PhotoView(
                          imageProvider: imageProvider,
                          minScale: PhotoViewComputedScale.contained * 0.9
                      ),
                      placeholder: (context, url) => AspectRatio(
                        aspectRatio: 1,
                        child: FractionallySizedBox(
                          heightFactor: 0.5,
                          widthFactor: 0.5,
                          child: SpinKitRing(
                            lineWidth:5,
                            color:
                            OwnColors.tran ,
                          ),
                        ),
                      ),
                      errorWidget: (context, url, error) =>
                          Icon(Icons.error),
                    ):

                    ClipRRect(borderRadius: BorderRadius.circular(8.0),
                      child: PhotoView(
                        imageProvider: FileImage( File(widget.url)),
                        maxScale: PhotoViewComputedScale.covered * 2.0,
                        minScale: PhotoViewComputedScale.contained * 0.8,
                        initialScale: PhotoViewComputedScale.contained,
                        heroAttributes: const PhotoViewHeroAttributes(tag: "someTag"),
                      ),
                    ),
                  ),
                  Positioned(

                     left: 0,
                     child: Container(
                      height: 0.06.sh + ScreenSize().getTopPad(context),
                      width: 1.sw,
                       color:  Colors.black38,

                    ),),

                  Positioned(
                    top:ScreenSize().getTopPad(context),
                    left: 0.04.sw,child: GestureDetector(
                    onTap: (){
                      backClick();
                    },
                      child: Container(
                      height: 0.048.sh,
                      // height: MediaQuery.of(context).size.height * 0.07,
                      child: Icon(SFSymbols.chevron_left, color: OwnColors.white),
                  ),
                    ),)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }


  Container topBar(double height) {
    return Container(
        width: 0.5 * ScreenSize().getWidth(context),
        height: height,
        color: OwnColors.tran,
        alignment: Alignment.center,
        // color: Colors.blue,
        child: Container(
          width: 1.0 * ScreenSize().getWidth(context),
          decoration: BoxDecoration(
            // color: OwnColors.ironGray,

              border: Border(
                  bottom: BorderSide(color: Colors.transparent, width: 3))),
          child: Row(
            children: <Widget>[
              Container(
                width: 0.15 * ScreenSize().getWidth(context),
                height: 0.06 * ScreenSize().getHeight(context),
                child: FlatButton(

                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    width: 1.5 * ScreenSize().getWidth(context),
                    height: 0.04 * ScreenSize().getHeight(context),
                    child: Image.asset(Res.back_arrow),
                  ),
                  onPressed: backClick,
                ),
              ),
              Expanded(
                flex: 7,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: AutoSizeText(
                    "",
                    minFontSize: 10,
                    maxFontSize: 30,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.white.withOpacity(1),
                      fontWeight: FontWeight.bold,
                      fontSize: 22.nsp,
                    ),
                    maxLines: 1,
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  @override
  void dispose() {
    super.dispose();
    print("dispose");
    StatusTools().setWhitBGDarkText();

  }






  void backClick() {
    print("back");

    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }




}
