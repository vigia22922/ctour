import 'dart:async';
import 'dart:io';


import 'package:ctour/Chat/ChatPage.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Firebase/FirebaseRealtime.dart';
import 'package:ctour/Guild/GuildPage.dart';
import 'package:ctour/Guild/GuilderPage.dart';
import 'package:ctour/Home/HomePage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/Personal/PersonalPage.dart';
import 'package:ctour/Travel/NewTravelPage.dart';
import 'package:ctour/Travel/TravelClass.dart';
import 'package:ctour/Travel/TravelPage.dart';
import 'package:ctour/Travel/TravelerPage.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/StatusTools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:permission_handler/permission_handler.dart';

import 'dart:convert';
import 'package:dio/dio.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'package:status_bar_control/status_bar_control.dart';



class LobbyPage extends StatefulWidget {

  final String  articleID;
  final String  guilderID;


  const LobbyPage(
      {Key key,
        this.articleID,
        this.guilderID,



      })
      : super(key: key);

  @override
  State createState() {
    return _LobbyPageState();
  }
}

class _LobbyPageState extends State<LobbyPage> {
  bool loginAlert = false;
  StreamSubscription<String> fireBaseDBSubScription2;

  int _currentIndex = 0;
  int homeNum = 0;
  int messNum = 0;
  int classNum = 0;
  int expenseNum = 0;
  int contactNum = 0;

  Icon homeIcon;
  Icon peopleIcon;
  Icon mesIcon;
  Icon addIcon;
  Icon personIcon;
  double addH = 0.06.sh;
  List _children = [

  ];
  var _pageController  = PageController();

  FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;

  void onTabTapped(int index) {
    homeIcon = Icon(SFSymbols.house, color: OwnColors.ironGray,);
    peopleIcon = Icon(SFSymbols.person_2_fill, color: OwnColors.ironGray,);
    mesIcon = Icon(SFSymbols.ellipses_bubble, color: OwnColors.ironGray,);

    personIcon = Icon(SFSymbols.person_circle, color: OwnColors.ironGray,);
    if(index != 2) {
      _pageController.jumpToPage(index);
      setState(() {
        print(index);
        _currentIndex = index;
        if (_currentIndex == 0) { //清空
          homeIcon = Icon(SFSymbols.house_fill, color: OwnColors.CTourOrange,);
        } else if (_currentIndex == 1) {
          peopleIcon =
              Icon(SFSymbols.person_2_fill, color: OwnColors.CTourOrange,);
        } else if (_currentIndex == 3) {
          messNum = 0;
          setState(() {

          });
          mesIcon = Icon(
            SFSymbols.ellipses_bubble_fill, color: OwnColors.CTourOrange,);
        } else if (_currentIndex == 4) {
          personIcon =
              Icon(SFSymbols.person_circle_fill, color: OwnColors.CTourOrange,);
        }
      });
    }else {
      PushNewScreen().normalPush(context, NewTravelPage());
    }
  }


  @override
  void initState() {


    StatusTools().setTranBGDarkText();
    homeIcon = Icon(SFSymbols.house_fill, color: OwnColors.CTourOrange,);
    peopleIcon = Icon(SFSymbols.person_2_fill, color: OwnColors.ironGray,);
    mesIcon = Icon(SFSymbols.ellipses_bubble, color: OwnColors.ironGray,);
    personIcon = Icon(SFSymbols.person_circle, color: OwnColors.ironGray,);
    addIcon = Icon(SFSymbols.plus_circle_fill, color: OwnColors.CTourOrange,size: addH,);
    print("is Android: ${Platform.isAndroid}");
    SharePre.prefs.setString(GDefine.guilderFilter, "false");
    SharePre.prefs.setString(GDefine.articleFilter, "false");

    _children = [
      HomePage(),
      GuildPage(),
      ChatPage(),
      ChatPage(),
      PersonalPage(),


    ];
    FirebaseRealtime.subscribe();

    fireBaseDBSubScription2 = FirebaseRealtime.updateBC.stream.listen((event) {
      print("fireBaseDBSubScription2 event" + event.toString());

    if(event.toString().contains("updateMes")){
        Future.delayed(Duration(milliseconds: 100), () {
          // 5s over, navigate to a new page
          messNum = 1;
          setState(() {

          });
        });
      }


    });
    initDynamicLinks();

    if(widget.articleID != ""){

      Future.delayed(Duration(milliseconds: 500), () {
        // 5s over, navigate to a new page
        getMyArticleAPI(widget.articleID);
      });


    }else if (widget.guilderID != ""){
      Future.delayed(Duration(milliseconds: 500), () {
        // 5s over, navigate to a new page
        PushNewScreen().normalPush(context, GuilderPage(id:widget.guilderID ,preview: false));
      });
    }
    super.initState();
  }

  Future<void> initDynamicLinks() async {

    dynamicLinks.onLink.listen((dynamicLinkData) {
      print("onLink");
      print(dynamicLinkData.toString());
      print(dynamicLinkData.link.queryParameters.toString());
      print(dynamicLinkData.link.path.toString());
      if (dynamicLinkData.link.queryParameters.containsKey('articleID')) {
        print("articleID");
        String id = dynamicLinkData.link.queryParameters['articleID'];
        print(id);

        if(SharePre.prefs.getString(GDefine.user_id)!=null){


          Future.delayed(Duration(milliseconds: 200), () {
            // 5s over, navigate to a new page
            getMyArticleAPI(id);
          });

        }
        else {
          WarningDialog.showIOSAlertDialog(context,S().login_first_title,  S().login_first_hint, S().confirm);
        }
      }else if (dynamicLinkData.link.queryParameters.containsKey('guilderID')) {
        print("guilderID");
        String id = dynamicLinkData.link.queryParameters['guilderID'];
        print(id);

        if(SharePre.prefs.getString(GDefine.user_id)!=null){


          PushNewScreen().normalPush(context, GuilderPage(id:id ,preview: false));
        }
        else {
          WarningDialog.showIOSAlertDialog(context, S().login_first_title,  S().login_first_hint, S().confirm);
        }
      }

    }).onError((error) {
      print('onLink error');
      print(error.message);
    });
  }

  // Future<void> _getLoginData() async {
  //
  //   final prefs = await SharedPreferences.getInstance();
  //
  //   String? storeId = prefs.getString("id");
  //   String? storePwd = prefs.getString("pwd");
  //
  //   print("get stored login id:$storeId  pwd:$storePwd");
  //
  //   if(storeId!=null && storePwd!=null){
  //
  //     if(storeId!="" && storePwd!="") {
  //       id = storeId;
  //       pwd = storePwd;
  //
  //       _callLoginApi();
  //     }
  //
  //   }
  //
  //
  // }

  @override
  void dispose() {
    print("Lobby dispse");
    FirebaseRealtime.cancelSub();
    super.dispose();


  }




  @override
  Widget build(BuildContext context) {


      return Scaffold(
        // appBar: AppBar(
        //   title: Text('AI財務長'),
        // ),
        backgroundColor: OwnColors.white,
        body: WillPopScope(
          onWillPop: (){

          },
          child: FocusDetector(
            onFocusGained: (){
              StatusTools().setTranBGDarkText();
            },
            child: SafeArea(
            child: PageView.builder(
                physics: NeverScrollableScrollPhysics(),
                itemCount: 5,
                controller: _pageController,
                itemBuilder:(context,index) => _children[index]),
            ),
          ),
        )

       ,

        bottomNavigationBar: Container(
          decoration: BoxDecoration(
            border: Border(
              top: BorderSide(width: 1.0, color: OwnColors.CF4F4F4),
            )
            // boxShadow: <BoxShadow>[
            //   BoxShadow(
            //     color: OwnColors.C989898,
            //     blurRadius: 5,
            //   ),
            // ],
          ),
          child:Theme(
            data: ThemeData(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
            ),
            child: Stack(
              alignment: Alignment.center,
              children: [
                BottomNavigationBar(
                  elevation: 0,
                  onTap: onTabTapped, // new
                  currentIndex: _currentIndex, // new

                  type: BottomNavigationBarType.fixed, // Fixed
                  backgroundColor: Colors.white, // <-- This works for fixed
                  selectedItemColor: OwnColors.CTourOrange,
                  unselectedItemColor: OwnColors.C989898,
                  selectedLabelStyle: GoogleFonts.inter(
                    fontWeight: FontWeight.bold
                  ),



                  items: [

                    BottomNavigationBarItem(
                        icon: _buildItemWithNotificationIcon(0,homeIcon), label: S().tab_home

                    ),
                    BottomNavigationBarItem(
                        icon: _buildItemWithNotificationIcon(0,peopleIcon), label: S().tab_guilder),
                    BottomNavigationBarItem(


                        icon:


                        _buildItemWithNotificationIcon2(0,addIcon), label:""),
                    BottomNavigationBarItem(
                        icon: _buildItemWithNotificationIcon(messNum,mesIcon), label:S().tab_mess),
                    BottomNavigationBarItem(
                        icon: _buildItemWithNotificationIcon(0,personIcon), label: S().tab_personal),

                  ],
                ),
               Padding(
                 padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
                 child: GestureDetector(onTap: (){
                   PushNewScreen().normalPush(context, NewTravelPage());
                 },child: Padding(
                   padding: const EdgeInsets.only(bottom:4.0),
                   child: SizedBox(height:addH,child: addIcon),
                 )),
               ) ,
 
              ],
            ),
          ),
        ),
      );

  }

  void getMyArticleAPI(String id ) async {
    Loading.show(context);
    print("getMyArticleAPI");

    ResultData resultData = await AppApi.getInstance().getArticle(context, true,
      SharePre.prefs.getString(GDefine.user_id),
      id,);

    //
    //
    Loading.dismiss(context);
    if (resultData.isSuccess()) {
      print(resultData.data.toString());
      var a = resultData.data;

      TravelClass travelClass = TravelClass.fromJson(a);
      travelClass.sectionTag = new List();
      print("name : " + TravelClass
          .fromJson(a)
          .title
          .toString());
      print("contentList : " + TravelClass
          .fromJson(a)
          .contentList
          .toString());
      print("contentList : " + a["content_list"].toString());
      print("DDDDDD" + jsonDecode(a["content_list"].toString()).toString());
      if (a["content_list"] != null) {
        List<dynamic> content_list = jsonDecode(a["content_list"].toString());
        travelClass.contentListDynamic = new List();

        List<TravelSubClass> subList = new List();
        for (Map<String, dynamic> sub in content_list) {
          TravelSubClass travelSubClass = new TravelSubClass();
          List<dynamic> content_list2 = sub["contentList"];
          List<TravelContentClass> travelContentClassList = new List();

          Map<String, dynamic> dy = new Map();
          dy["title"] =
          sub["title"].toString() == "null" ? "" : sub["title"].toString();
          dy["type"] = "0";
          dy["description"] = "";
          dy["google"] = "";
          dy["image"] = [];
          travelClass.contentListDynamic.add(dy);


          for (Map<String, dynamic> subsub in content_list2) {
            TravelContentClass travelContentClass = new TravelContentClass();
            travelContentClass.image = List<String>.from(subsub["image"]);
            travelContentClass.image.removeWhere((element) =>
            element == "" || element == "null");
            travelContentClass.title = subsub["title"];
            travelContentClass.description =
            subsub["description"].toString() == "null"
                ? ""
                : subsub["description"].toString();
            travelContentClass.id = subsub["id"].toString();
            travelContentClass.google =
            subsub["google"].toString() == "null" ? "" : subsub["google"]
                .toString();
            travelContentClass.key = new GlobalKey();

            travelContentClassList.add(travelContentClass);

            Map<String, dynamic> dy2 = new Map();
            dy2["title"] = travelContentClass.title;
            dy2["type"] = "1";
            dy2["description"] = travelContentClass.description;
            dy2["google"] = travelContentClass.google;
            dy2["image"] = List<String>.from(subsub["image"]);

            travelClass.contentListDynamic.add(dy2);
          }
          travelSubClass.title =
          sub["title"].toString() == "null" ? "" : sub["title"].toString();
          travelSubClass.id = sub["id"].toString();
          travelSubClass.key = new GlobalKey();
          if(sub["title"]!="" && sub["title"].toString() != "null") {
            travelClass.sectionTag.add(new SectionClass(title: sub["title"], key:travelSubClass.key  ));
          }

          if (travelContentClassList.isNotEmpty) {
            travelSubClass.content = travelContentClassList;
          }

          subList.add(travelSubClass);
        }

        travelClass.contentList = subList;

        PushNewScreen().normalPush(context, TravelPage(travelClass: travelClass,edit: false,));

      }
    }

    else {
      if(resultData.data.toString().contains("取消")){
        WarningDialog.showIOSAlertDialog(context, S().travel_article_not_found_title  , S().travel_article_not_found_hint2,S().confirm);
      }
      else {
        WarningDialog.showIOSAlertDialog(
            context, S().travel_article_not_found_title,
            S().travel_article_not_found_hint1, S().confirm);
      }
    }



  }






  _buildItemWithNotificationIcon(int num,Icon icon) {
    return Stack(
      children: [
        Container(
            height:0.032*ScreenSize().getHeight(context),
            color: OwnColors.white,
            child: Stack(
              children: [
                icon,
                if( num!=0 ) Positioned( //判斷假如不是零的時候才顯示
                  top: 0,
                  right: 0,
                  child: Transform.translate(
                    offset: Offset( 0.003.sh, 0.000.sh,), //先右上偏移一點點
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          width:  0.01.sh,
                          height:  0.01.sh,
                          decoration:  BoxDecoration(
                            color: OwnColors.white,
                            shape: BoxShape.circle,
                            // border: Border.all(width: 1.5,color: OwnColors.white),

                          ),



                        ),
                        Center(
                          child: Container(
                            width:  0.01.sh-3,
                            height:  0.01.sh-3,
                            decoration:  BoxDecoration(
                              color: OwnColors.CTourOrange,
                              shape: BoxShape.circle,
                              // border: Border.all(width: 1.5,color: OwnColors.white),

                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )

              ],
            )
          //child: Image.asset(icon)
        ),
      ],
    );
  }


  _buildItemWithNotificationIcon2( int num,Icon icon) {
    return Opacity(
      opacity: 0.0,
      child: Stack(
        children: [
          Container(
              height:0.032*ScreenSize().getHeight(context),
              color: OwnColors.white,
              child: Stack(
                children: [
                  icon,


                ],
              )
            //child: Image.asset(icon)
          ),
        ],
      ),
    );
  }

}