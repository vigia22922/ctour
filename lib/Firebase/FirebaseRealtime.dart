import 'dart:async';

import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/SQLite/Database.dart';
import 'package:ctour/SQLite/model/CCModel.dart';
import 'package:ctour/SQLite/model/ChatModel.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


/// Message route arguments.
class FirebaseRealtime {
  static DatabaseReference fireBaseDB;
  static StreamSubscription<DatabaseEvent> fireBaseDBSubScription;
  static StreamSubscription<DatabaseEvent> fireBaseDBSubScriptionPerson;
  static var db = new DatabaseHelper();
  static int timestamp;

  static var updateBC= StreamController<String>.broadcast();

  static List<CCModel> chatPersonList = new List();

  static void init() {
    FirebaseDatabase firebaseDatabase = FirebaseDatabase(
        databaseURL:
            "https://ctour-c1343-default-rtdb.asia-southeast1.firebasedatabase.app/");
    fireBaseDB = firebaseDatabase.ref();
    db = new DatabaseHelper();
    timestamp = DateTime.now().millisecondsSinceEpoch;
    updateBC= StreamController<String>.broadcast();
  }

  static void subscribe() {
    print("FirebaseRealtime subscribe ");
    if (fireBaseDBSubScription == null) {
      print("FirebaseRealtime subscribe acctualy");
      fireBaseDBSubScription = fireBaseDB
          .child("Message")
          .child(SharePre.prefs.getString(GDefine.user_id))
          .child("unread")
          .orderByChild("timeStamp")
          .onValue
          .listen((DatabaseEvent event) {
        if (event.snapshot.value != null) {
          Map map = event.snapshot.value;
          print("FirebaseRealtime listen " +
              timestamp.toString() +
              " unread :" +
              map.toString());
          insertDB(map);
          updateBC.sink.add("updateMes");
        }
      });
    }
    if (fireBaseDBSubScriptionPerson == null) {
      print("fireBaseDBSubScriptionPerson subscribe acctualy");
      print("fireBaseDBSubScriptionPerson subscribe  user id " + SharePre.prefs.getString(GDefine.user_id));
      chatPersonList.clear();
      fireBaseDBSubScriptionPerson = fireBaseDB
          .child("Message")
          .child(SharePre.prefs.getString(GDefine.user_id))
          .child("ChatPersonList")
          .onValue
          .listen((DatabaseEvent event) {
        if (event.snapshot.value != null) {
          Map map = event.snapshot.value;
          print("fireBaseDBSubScriptionPerson listen " +
              timestamp.toString() +
              " ChatPersonList :" +
              map.toString());
          chatPersonList.clear();
          insertCCDB(map);
          updateBC.sink.add("updateChat");
        }
      });
    }

  }

  static void cancelSub() {
    print("cancelSub");
    if (fireBaseDBSubScription != null) {
      print("cancelSub");
      fireBaseDBSubScription.cancel();
      fireBaseDBSubScription = null;
    }
    if (fireBaseDBSubScriptionPerson != null) {
      print("cancelSub");
      fireBaseDBSubScriptionPerson.cancel();
      fireBaseDBSubScriptionPerson = null;
    }
  }

  static Future<void> insertDB(Map map) async {
    print("FirebaseRealtime insertDB");
    List<Object> sender_id_list = map.keys.toList();
    List<ChatModel> chatAddList = new List();
    for (int j = 0; j < sender_id_list.length; j++) {
      String sender_id = sender_id_list[j];
      Map sub = map[sender_id];
      List<Object> key_sub = sub.keys.toList();
      for (int i = key_sub.length - 1; i >= 0; i--) {
        String chat_id = key_sub[i];

        print("chat_id " + chat_id);
        Map mess_obj = sub[chat_id];
        print("mess_obj " + mess_obj.toString());
        List<ChatModel> chatList = await db.getChatByNowIDGroupIDChatID(
            SharePre.prefs.getString(GDefine.user_id), sender_id, chat_id);

        if (chatList.isEmpty &&
            !chatAddList.any((file) => file.chat_id == chat_id)) {
          ChatModel chatModel = new ChatModel(
            now_id: SharePre.prefs.getString(GDefine.user_id),
            chat_id: chat_id,
            chat_group_id: sender_id,
            sender_name: mess_obj["sender_name"],
            sender_avatar: mess_obj["sender_avatar"],
            sender_id: mess_obj["sender_id"],
            message: mess_obj["message"],
            time: mess_obj["time"],
            t: mess_obj["t"],
            timeStamp: mess_obj["timeStamp"],
            read: "0",
            suc: "1",
          );
          chatAddList.add(chatModel);

        }
      }
      FirebaseRealtime.removeAllUnread(sender_id, "");
      FirebaseRealtime.pushToRead(chatAddList);
    }

    print("FirebaseRealtime sort");
    chatAddList.sort(
        (a, b) => int.parse(a.timeStamp).compareTo(int.parse(b.timeStamp)));

    for (ChatModel cc in chatAddList) {
      print("FirebaseRealtime insertChat!");
      db.insertChat(cc);
    }
  }

  static Future<void> insertCCDB(Map map) async {
    print("FirebaseRealtimePerson insertDB");
    List<Object> sender_id_list = map.keys.toList();
    List<ChatModel> chatAddList = new List();
    chatPersonList.clear();
    for (int j = 0; j < sender_id_list.length; j++) {
      String sender_id = sender_id_list[j];
      print("sender _id " + sender_id);
      Map sub = map[sender_id];
      // List<Map> key_sub = sub.keys.toList();
      // for (int i = key_sub.length - 1; i >= 0; i--) {

        String realName = sub["realName"];
        String userId = sub["userId"];
        String userPic = sub["userPic"];
        String FCMToken = sub["FCMToken"];
 
        List<CCModel> ccList = await db.getCCByNowID(
            SharePre.prefs.getString(GDefine.user_id) );
        if(ccList.indexWhere((element) => element.userId == userId.toString()) != -1){
          CCModel ccModel = ccList.firstWhere((element) => element.userId == userId.toString());
          ccModel.realName = realName;
          ccModel.userPic = userPic;
          ccModel.FCMToken = FCMToken;
          db.insertCC(ccModel);
          chatPersonList.add(ccModel);
        }else {
          CCModel ccModel = new CCModel(nowID: SharePre.prefs.getString(GDefine.user_id),userPic: userPic,userId: userId,FCMToken: FCMToken);
          ccModel.realName = realName;
          ccModel.userPic = userPic;
          ccModel.FCMToken = FCMToken;
          db.insertCC(ccModel);
          chatPersonList.add(ccModel);
        }
      // }

    }

    print("FirebaseRealtime sort");
    chatAddList.sort(
            (a, b) => int.parse(a.timeStamp).compareTo(int.parse(b.timeStamp)));

    for (ChatModel cc in chatAddList) {
      print("FirebaseRealtime insertChat!");
      db.insertChat(cc);
    }
  }


  static void pushMyMessage(
      String toUserId,String toAvatar,String toRealname, String mes, String type, String time, String timeStamp) {
    Map<String, String> data = {
      "sender_id": SharePre.prefs.getString(GDefine.user_id),
      "sender_name": SharePre.prefs.getString(GDefine.name),
      "sender_avatar": SharePre.prefs.getString(GDefine.avatar),
      "message": mes,
      "t": type,
      "time": time,
      "timeStamp": timeStamp,
      // "time":DateFormat('yyyy-MM-dd HH:mm:ss').format((DateTime.now()))  ,
      // "timeStamp":DateTime.now().millisecondsSinceEpoch.toString()  ,
    };
    fireBaseDB
        .child("Message")
        .child(SharePre.prefs.getString(GDefine.user_id))
        .child("read")
        .child(toUserId)
        .push()
        .set(data)
        .whenComplete(() {
      print("FirebaseRealtime push data done");
    }).catchError((error) {
      print("FirebaseRealtime push data error");
    });


    Map<String, String> data2 = {
      "userId": toUserId,
      "realName": toRealname,
      "userPic": toAvatar,

    };
    fireBaseDB
        .child("Message")
        .child(SharePre.prefs.getString(GDefine.user_id))
        .child("ChatPersonList")
        .child(toUserId)

        .update(data2)
        .whenComplete(() {
      print("FirebaseRealtime ChatPersonList push data done");
      fetchChatPerson();
    }).catchError((error) {
      print("FirebaseRealtime push data error");
      print(error);
    });
  }

  static void pushToMessage(
      String toUserId, String mes, String type, String time, String timeStamp) {
    print("pushToMessage ");
    print("SharePre.prefs.getString(GDefine.user_img) " +
        SharePre.prefs.getString(GDefine.avatar));
    Map<String, String> data = {
      "sender_id": SharePre.prefs.getString(GDefine.user_id),
      "sender_name": SharePre.prefs.getString(GDefine.name),
      "sender_avatar": SharePre.prefs.getString(GDefine.avatar),
      "message": mes,
      "t": type,
      "time": time,
      "timeStamp": timeStamp,
      // "time":DateFormat('yyyy-MM-dd HH:mm:ss').format((DateTime.now()))  ,
      // "timeStamp":DateTime.now().millisecondsSinceEpoch.toString()  ,
    };
    fireBaseDB
        .child("Message")
        .child(toUserId)
        .child("unread")
        .child(SharePre.prefs.getString(GDefine.user_id))
        .push()
        .set(data)
        .whenComplete(() {
      print("FirebaseRealtime push data done");
    }).catchError((error) {
      print("FirebaseRealtime push data error");
      print(error);
    });


    Map<String, String> data2 = {
      "userId": SharePre.prefs.getString(GDefine.user_id),
      "realName": SharePre.prefs.getString(GDefine.name),
      "userPic": SharePre.prefs.getString(GDefine.avatar),

    };
    fireBaseDB
        .child("Message")
        .child(toUserId)
        .child("ChatPersonList")
        .child(SharePre.prefs.getString(GDefine.user_id))

        .update(data2)
        .whenComplete(() {
      print("FirebaseRealtime push data done");
    }).catchError((error) {
      print("FirebaseRealtime push data error");
      print(error);
    });
  }

  static void pushToChatPerson(
     ) {
    print("pushToChatPerson");
    print("SharePre.prefs.getString(GDefine.avatar) " + SharePre.prefs.getString(GDefine.avatar));
    print(chatPersonList.length);
    for (CCModel c in chatPersonList){
      print(c.userId);
      Map<String, String> data2 = {
        "userId": SharePre.prefs.getString(GDefine.user_id),
        "realName": SharePre.prefs.getString(GDefine.name),
        "userPic": SharePre.prefs.getString(GDefine.avatar),

      };
      fireBaseDB
          .child("Message")
          .child(c.userId)
          .child("ChatPersonList")
          .child(SharePre.prefs.getString(GDefine.user_id))

          .update(data2)
          .whenComplete(() {
        print("FirebaseRealtime push data done");
      }).catchError((error) {
        print("FirebaseRealtime push data error");
        print(error);
      });
    }

  }

  static void pushToRead(
      List<ChatModel> chatAddList) {
    print("pushToMessage ");
    print("SharePre.prefs.getString(GDefine.user_img) " +
        SharePre.prefs.getString(GDefine.avatar));
    for (ChatModel c in chatAddList){
      Map<String, String> data = {
        "sender_id": c.sender_id,
        "sender_name": c.sender_name,
        "sender_avatar": c.sender_avatar,
        "message": c.message,
        "t": c.t,
        "time": c.time,
        "timeStamp": c.timeStamp,
        // "time":DateFormat('yyyy-MM-dd HH:mm:ss').format((DateTime.now()))  ,
        // "timeStamp":DateTime.now().millisecondsSinceEpoch.toString()  ,
      };
      fireBaseDB
          .child("Message")
          .child(SharePre.prefs.getString(GDefine.user_id))
          .child("read")
          .child(c.sender_id)
          .push()
          .set(data)
          .whenComplete(() {
        print("FirebaseRealtime push data done");
      }).catchError((error) {
        print("FirebaseRealtime push data error");
        print(error);
      });
    }

  }

  static void removeUnread(String sender_id, String chat_id) {
    fireBaseDB
        .child("Message")
        .child(SharePre.prefs.getString(GDefine.user_id))
        .child("unread")
        .child(sender_id)
        .child(chat_id)
        .remove()
        .whenComplete(() {
      print("remove finish");
    }).catchError((error) {
      print(error);
    });
  }

  static void removeAllUnread(String sender_id, String chat_id) {
    fireBaseDB
        .child("Message")
        .child(SharePre.prefs.getString(GDefine.user_id))
        .child("unread")
        .child(sender_id)
        .remove()
        .whenComplete(() {
      print("remove finish");
    }).catchError((error) {
      print(error);
    });

  }

  static Future<void> fetchUnread() async {
    DatabaseEvent event = await fireBaseDB
        .child("Message")
        .child(SharePre.prefs.getString(GDefine.user_id))
        .child("unread")
        .once();
    print("FirebaseRealtime fetchUnread : " + event.snapshot.value);
  }

  static Future<void> fetchChatPerson() async {
    DatabaseEvent event = await fireBaseDB
        .child("Message")
        .child(SharePre.prefs.getString(GDefine.user_id))
        .child("ChatPersonList")
        .once();
    print("FirebaseRealtime fetchChatPerson : " + event.snapshot.value.toString());
    if (event.snapshot.value != null) {
      Map map = event.snapshot.value;
      print("fireBaseDBSubScriptionPerson listen " +
          timestamp.toString() +
          " ChatPersonList :" +
          map.toString());
      chatPersonList.clear();
      insertCCDB(map);

    }
  }
}
