import 'dart:async';

import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/SQLite/Database.dart';
import 'package:ctour/SQLite/model/CCModel.dart';
import 'package:ctour/SQLite/model/ChatModel.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


/// Message route arguments.
class FirebaseDynamicLink {
  static Future<String> createDynamicLinkArticle(String article_id,String imgUrl,String article_title,String des) async {

    print("createDynamicLink");
    print("imgUrl " + imgUrl);
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://ctour.page.link',
      // link: Uri.parse("https://www.example.com/" ),
      link: Uri.parse('https://ctour.page.link/article?articleID=$article_id' ),
      longDynamicLink: Uri.parse(
        'https://ctour.page.link/?link=https://ctour.page.link.com/article?articleID=$article_id&apn=com.punchih.ctour&isi=1635886410&ibi=com.ctourapp'+"&ofl=https://linktr.ee/eugeneforctour"
        '&st='  +"CTour - " + "$article_title" + "&sd=" + "$des" + "&si=" + "$imgUrl" ,
      ),
      androidParameters: AndroidParameters(
          packageName: 'com.punchih.ctour',
          // minimumVersion: 1,
          // fallbackUrl: Uri.parse('https://www.google.com/')

      ),
      iosParameters: IOSParameters(
          bundleId: 'com.ctourapp',
          // minimumVersion: '1.0.0',
          // appStoreId: '123456789',
          // fallbackUrl: Uri.parse('https://www.google.com/')

      ),
      // googleAnalyticsParameters: GoogleAnalyticsParameters(
      //   campaign: 'example-promo',
      //   medium: 'social',
      //   source: 'orkut',
      // ),
      // itunesConnectAnalyticsParameters: ItunesConnectAnalyticsParameters(
      //   providerToken: '123456',
      //   campaignToken: 'example-promo',
      // ),
      socialMetaTagParameters: SocialMetaTagParameters(
        title: 'CTour - Travel and Food',
        description: 'CTour - Travel and Food',
          imageUrl :Uri.parse(imgUrl)
      ),
    );
    Uri url;
    FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;
    bool short = true;
    if (short) {
      final ShortDynamicLink shortLink =
      await dynamicLinks.buildShortLink(parameters);
      url = shortLink.shortUrl;
    } else {
      url = await dynamicLinks.buildLink(parameters);
    }






    print(url.toString());
    return url.toString();
  }


  static Future<String> createDynamicLinkGuilder(String guilder_id,String imgUrl,String guilder_name,String des) async {

    print("createDynamicLink");
    print("imgUrl " + imgUrl);
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: 'https://ctour.page.link',
      // link: Uri.parse("https://www.example.com/" ),
      link: Uri.parse('https://ctour.page.link/article?guilderID=$guilder_id' ),
      longDynamicLink: Uri.parse(
        'https://ctour.page.link/?link=https://ctour.page.link.com/article?guilderID=$guilder_id&apn=com.punchih.ctour&isi=1635886410&ibi=com.ctourapp'+"&ofl=https://linktr.ee/eugeneforctour"+
            "&ofl=https://play.google.com/store/apps/details?id=com.punchih.ctour"+
            '&st='        +"CTour - " + "$guilder_name" + "&sd=" + "$des"+ "&si=" + "$imgUrl"+"&ofl=https://linktr.ee/eugeneforctour",
      ),
      androidParameters: AndroidParameters(
          packageName: 'com.punchih.ctour',
          // minimumVersion: 1,
          fallbackUrl: Uri.parse('https://www.google.com/')

      ),
      iosParameters: IOSParameters(
        bundleId: 'com.ctourapp',
        // minimumVersion: '1.0.0',
        // appStoreId: '123456789',
        // fallbackUrl: Uri.parse('https://www.google.com/')

      ),
      // googleAnalyticsParameters: GoogleAnalyticsParameters(
      //   campaign: 'example-promo',
      //   medium: 'social',
      //   source: 'orkut',
      // ),
      // itunesConnectAnalyticsParameters: ItunesConnectAnalyticsParameters(
      //   providerToken: '123456',
      //   campaignToken: 'example-promo',
      // ),
      socialMetaTagParameters: SocialMetaTagParameters(
          title: 'CTour - Travel and Food',
          description: 'CTour - Travel and Food',
          imageUrl :Uri.parse(imgUrl)
      ),
    );
    Uri url;
    FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;
    bool short = true;
    if (short) {
      final ShortDynamicLink shortLink =
      await dynamicLinks.buildShortLink(parameters);
      url = shortLink.shortUrl;
    } else {
      url = await dynamicLinks.buildLink(parameters);
    }



    print(url.toString());
    return url.toString();
  }


}
