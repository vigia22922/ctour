import 'dart:async';
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/Chat/ChattingPage.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Firebase/FirebaseRealtime.dart';
import 'package:ctour/Chat/ChatClass.dart';
import 'dart:ui' as ui;
import 'package:ctour/SQLite/Database.dart';
import 'package:ctour/SQLite/model/CCModel.dart';

import 'package:ctour/SQLite/model/ChatModel.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/DateTools.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';

import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';


class ChatPage extends StatefulWidget{

  Function goNews;



  ChatPage({Key key, this.goNews }):super(key:key);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return ChatPageState();
  }

}



class ChatPageState extends State<ChatPage> with AutomaticKeepAliveClientMixin{
  final String TAG = "ChatPageState";
  TextEditingController searchEdit;
  List<ChatClass> itemList = new List();


  static List<CCModel> chatPersonList = new List();
  int tabState = 0;
  EasyRefreshController refreshController = new EasyRefreshController( ) ;
  ScrollController _scrollController = new ScrollController();
  // StreamSubscription<DatabaseEvent> fireBaseDBSubScription;
  // StreamSubscription<DatabaseEvent> fireBaseDBSubScription2;
  StreamSubscription<String> subscription;
  bool go2ChooseMode = false;
  List<int> _selectedItems = List<int>();
  bool selectAll = false;

  String searchText1 = "";
  FocusNode searchFocus = new FocusNode();
  bool noMessShow = false;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();


    refreshController = EasyRefreshController(
      controlFinishRefresh: true,
      controlFinishLoad: true,
    );
    searchEdit = TextEditingController();

    // itemList.add(new ChatClass(now_id: SharePre.getString(GDefine.user_id),sender_id: "38",sender_name: "Angela",sender_avatar: "https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg",t: "",time: "2022-06-18",timeStamp: "",new_mes_cnt: 0,message: "WWWW",send_to_fcm: ""));
    // print()

    subscription = FirebaseRealtime.updateBC.stream.listen((event) {
     print("event" + event.toString());

      if(event.toString().contains("updateChat")) {
        Future.delayed(Duration(milliseconds: 100), () {
          // 5s over, navigate to a new page
          getChatList();
        });

      }else if(event.toString().contains("updateMes")){
        Future.delayed(Duration(milliseconds: 100), () {
          // 5s over, navigate to a new page
          getChatList();
        });
      }


    });
    Future.delayed(Duration(milliseconds: 100), () {
      // 5s over, navigate to a new page
      refreshController.callRefresh();
    });


    _scrollController.addListener(() {
      print("ss");
      // Tools().dismissK(context);
    });


    // FirebaseRealtime.fetchUnread();

    // getSystemNews();

  }
  Future<void> onRefresh() async {
    print("onRefresh1");
    // setItemList();
    searchEdit.text = "";
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return normal();

  }

  Widget normal(){
    return FocusDetector(
      onFocusGained: (){

        getChatPersonList();

          // refreshController.callRefresh(duration: Duration(milliseconds: 200));
        // refreshController.finishRefresh();
        // Future.delayed(Duration(milliseconds: 500), () {
        //   // 5s over, navigate to a new page
        //   refreshController.callLoad();
        // });

      },

      child: GestureDetector(
        onTap:(){
          Tools().dismissK(context);
          print("onTap");
        },
        child: Scaffold(
          backgroundColor:  OwnColors.white,
          body: SafeArea(
            child: Container(
                height: ScreenSize().getHeight(context)*0.91,
                width: ScreenSize().getWidth(context),
                child :Column(
                  children: [
                    Container(
                      height: ScreenSize().getHeight(context)*0.067,
                      child: Column(
                        children: [
                         Spacer(),
                          Container(

                            width: ScreenSize().getWidth(context)*0.874,
                            color: OwnColors.white,
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: AutoSizeText(
                                S().message,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.inter(
                                    height: 1.5,
                                    fontWeight: FontWeight.w700,
                                    color: OwnColors.black,
                                    fontSize: 27.nsp),
                              ),
                            ),
                          ),
                          Spacer(),
                        ],
                      ),
                    ),
                    Container(
                      height: 0.05 *
                          ScreenSize().getHeight(context),
                      width: ScreenSize().getWidth(context)*0.88,
                      // height: height,
                      decoration: BoxDecoration(
                          color: OwnColors.white,
                          border: Border.all(width: 1,color: OwnColors.CC8C8C8),

                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(30),
                              topRight: Radius.circular(30),
                              bottomLeft: Radius.circular(30),
                              bottomRight:
                              Radius.circular(30)),),

                      alignment: Alignment.center,
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: (){
                              FocusScope.of(context).requestFocus(searchFocus);
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(left: 12.0, bottom: 4.0),
                              child: Container(
                                  height: 0.027 *
                                      ScreenSize().getHeight(context),
                                  child: Icon(SFSymbols.search, color: OwnColors.black)),
                            ),
                          ),
                          Expanded(
                            child: Container(

                              height: 0.027 *
                                  ScreenSize()
                                      .getHeight(context),
                              child: TextField(
                                focusNode: searchFocus,
                                controller: searchEdit,
                                onChanged: (text) {
                                  print("onChanged");
                                  if(searchText1 == "") {
                                    searchText1 = text;
                                    searchChat();
                                  }else {
                                    if (searchText1 != text ){
                                      searchText1 = text;
                                      searchChat();
                                    }
                                  }

                                },
                                autocorrect:false,
                                decoration: InputDecoration(

                                    fillColor: OwnColors.tran,
                                    contentPadding:
                                    EdgeInsets.symmetric(
                                        horizontal: 10,vertical: 0),
                                    //Change this value to custom as you like
                                    // isDense: true,
                                    filled: true,
                                    hintText: S().message_search_hint,
                                    hintStyle: TextStyle(
                                        fontSize: 16.nsp,
                                        color: Colors.grey),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                        new BorderRadius.circular(
                                            20.0),
                                        borderSide: BorderSide.none)),
                                style: TextStyle(
                                    color:
                                    Colors.black.withOpacity(1),
                                    fontSize: ScreenUtil().setSp(16)),
                                maxLines: 1,
                                keyboardType: TextInputType.text,
                                obscureText: false,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    HorizontalSpace().create(context, 0.023),

                    Expanded(
                      child:

                      noMessShow ? Opacity(
                        opacity: noMessShow ? 1.0:0.0,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              height: 0.1.sh,
                              width: 1.0.sw,
                              child: Image.asset(Res.no_mess),
                            ),
                            HorizontalSpace().create(context, 0.019),
                            Text(
                              S().no_mess,
                              maxLines: 1,
                              style: GoogleFonts.inter(
                                fontWeight: FontWeight.w500,
                                fontSize: 16.nsp,
                                color: OwnColors.C989898
                              ),
                            )
                          ],
                        ),
                      ):
                      EasyRefresh(
                        // key: _refresherKey,

                        controller: refreshController,
                        // enablePullUp: false,
                        // enablePullDown: true,

                        header: ClassicHeader(
                            dragText: '',
                            armedText: '',
                            readyText: '',
                            processingText: '',
                            processedText: '',
                            noMoreText: '',
                            failedText: '',
                            messageText: '',
                            iconTheme: IconThemeData(
                                color: OwnColors.CTourOrange
                            )

                        ),
                        // physics: BouncingScrollPhysics(),
                        footer: NotLoadFooter(),
                        onRefresh: () async {
                          //monitor fetch data from network
                          print("onRefresh2");
                          // setItemList();
                          searchEdit.text = "";
                          getChatPersonList();

                          await Future.delayed(Duration(milliseconds: 500));

                          // if (mounted) setState(() {
                          // refreshController.refreshCompleted();
                          // });

                        },

                        // onLoading: () async {
                        //   print("onLoading");
                        //   // monitor fetch data from network
                        //   await Future.delayed(Duration(milliseconds: 300));
                        // },

                        child: ListView.builder(
                            itemCount: itemList.length,

                            controller: _scrollController,
                            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.only(
                                    top: 0.0 ),
                                child: GestureDetector(
                                    child: cardBtn(itemList[index], index),
                                    onTap: () => {}),

                              );
                            }

                        )

                        ,
                      ),
                    )
                  ],
                )
            ),
          ),

        ),
      ),
    );
  }


  Widget cardBtn(ChatClass chatClass,int index) {
    bool sameDay = false;
    bool sameWeek = false;
    bool sameYear = false;
    bool yesterday = false;
    bool empty = false;
    String weekName = "";
    print("chatClass.time " + chatClass.time);
    print("chatClass.message " + chatClass.message);
    print("chatClass.sender_id " + chatClass.sender_id);
    print("chatClass.sender_id " + SharePre.prefs.getString(GDefine.user_id));
    if(chatClass.time.length>10) {
      print("chatClass.time " + chatClass.time);
      DateTime dateTime = DateTime.parse(chatClass.time.split(" ")[0]);
      DateTime now = DateTime(DateTime
          .now()
          .year, DateTime
          .now()
          .month, DateTime
          .now()
          .day);
      sameDay = dateTime == now;
      sameWeek = DateTools().checkSameWeek(dateTime);
      sameYear = dateTime.year == now.year;
      yesterday = dateTime == now.subtract(Duration(days: 1));

      weekName = DateTools().getWeekName(dateTime);
      print("weekName " + weekName.toString());
    }else {
      empty = true;
    }

    print("empty " + empty.toString());
    print("sameDay " + sameDay.toString());
    print("sameWeek " + sameWeek.toString());
    print("weekName " + weekName.toString());
    print("weekName " + weekName.toString());
    return GestureDetector(

      child: Column(
        children: [
          Stack(alignment: Alignment.centerRight, children: <Widget>[
            Center(
              child: Container(

                width:  ScreenSize().getWidth(context ),

                child: Padding(
                  padding: const EdgeInsets.only(top:0.0,bottom: 0 ),
                  child: Column(
                    children: [
                      HorizontalSpace().create(context, 0.002),
                      Container(
                        height: ScreenSize().getHeight(context )*0.084,
                        width: ScreenSize().getWidth(context)*0.874,
                        child: Row(
                          children: [

                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [

                                Container(
                                  height: ScreenSize().getHeight(context )*0.06,
                                  child: Stack(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(top:0.0,left: 0),
                                        child: AspectRatio(
                                          aspectRatio: 1,
                                          child: ClipOval(
                                            child:   Material(
                                              // color: Colors.transparent, // button color
                                              child: Container(
                                                height: ScreenSize().getHeight(context )*0.05,

                                                child: CachedNetworkImage(
                                                    placeholder: (context, url) => AspectRatio(
                                                      aspectRatio: 1,
                                                      child: FractionallySizedBox(
                                                        heightFactor: 0.5,
                                                        widthFactor: 0.5,
                                                        child: SpinKitRing(
                                                          lineWidth:5,
                                                          color:
                                                          OwnColors.tran ,
                                                        ),
                                                      ),
                                                    ),
                                                  errorWidget: (context, url, error) => Icon(Icons.error,color: Colors.red,),
                                                  imageUrl:chatClass.sender_avatar,
                                                  fit: BoxFit.fitHeight,
                                                  imageBuilder: (context, imageProvider) =>
                                                      Container(
                                                        // width: 80.0,
                                                        // height: 80.0,

                                                        decoration: BoxDecoration(
                                                          shape: BoxShape.circle,
                                                          color: OwnColors.tran,
                                                          border: Border.all(width: 1,color: OwnColors.CC8C8C8),
                                                          image: DecorationImage(
                                                              image: imageProvider,
                                                              fit: BoxFit.cover),
                                                        ),
                                                      ),
                                                  // width: MediaQuery.of(context).size.width,
                                                  // placeholder: (context, url) => new CircularProgressIndicator(),
                                                  // placeholder: new CircularProgressIndicator(),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),

                                    ],
                                  ),
                                ),
                              ],
                            ),
                            VerticalSpace().create(context, 0.03),
                            Expanded(child:
                            Container(
                              // height: ScreenSize().getHeight(context )*0.07,
                              child: LayoutBuilder(

                                  builder:(context, size) {
                                    var span = TextSpan(
                                      text: chatClass.t=="i"  ? S().image :chatClass.message,
                                      style: TextStyle(fontSize: 16.nsp),
                                    );

                                    // Use a textpainter to determine if it will exceed max lines
                                    var tp = TextPainter(
                                      maxLines: 1,
                                      textAlign: TextAlign.left,
                                      textDirection:ui. TextDirection.ltr,
                                      text: span,
                                    );

                                    // trigger it to layout
                                    tp.layout(maxWidth: size.maxWidth);

                                    // whether the text overflowed or not
                                    var exceeded = tp.didExceedMaxLines;
                                    print("exceeded" + exceeded.toString());
                                  return Column(
                                    mainAxisAlignment: exceeded ? MainAxisAlignment.start: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        height:
                                        exceeded ? ScreenSize().getHeight(context )*0.024/2 :0 ,
                                      ),
                                      Row(
                                        children: [
                                          Expanded(
                                            child: Text(
                                              chatClass.sender_name,
                                              textAlign: TextAlign.left,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: GoogleFonts.inter(

                                                  fontWeight: FontWeight.w500,
                                                  color: OwnColors.black,
                                                  fontSize: 18.nsp),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          chatClass.t=="i"  ? S().image :chatClass.message,
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          textAlign: TextAlign.left,
                                          style: GoogleFonts.inter(

                                              fontWeight: FontWeight.w500,
                                              color: OwnColors.C989898,
                                              height: 1.1,

                                              fontSize: 14.nsp),
                                        ),
                                      )
                                    ],
                                  );
                                }
                              ),
                            )),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Spacer(),
                                Text(
                            empty?
                            chatClass.time:
                                  sameDay?
                                  DateFormat ("hh:mm a").format(DateTime.parse(chatClass.time)):
                                    yesterday?
                                        S().yesterday:
                                        sameWeek?weekName:
                                            sameYear?
                                             DateFormat ("MM/dd").format(DateTime.parse(chatClass.time)):
                                              DateFormat ("yyyy/MM/dd").format(DateTime.parse(chatClass.time)),


                                  textAlign: TextAlign.left,
                                  maxLines: 1,
                                  style:  GoogleFonts.inter(
                                      fontWeight: FontWeight.w500,
                                      color: OwnColors.C989898,
                                      fontSize: 14.nsp),
                                ),
                                Spacer(),
                                Padding(
                                  padding: const EdgeInsets.only(top:4.0),
                                  child: Container(
                                      decoration: chatClass.new_mes_cnt < 10 ? BoxDecoration(
                                          color:  chatClass.new_mes_cnt!=0?OwnColors.CTourOrange:OwnColors.tran,

                                        shape:  BoxShape.circle
                                      ):BoxDecoration(
                                          color:  chatClass.new_mes_cnt!=0?OwnColors.CTourOrange:OwnColors.tran,

                                          borderRadius:
                                          BorderRadius.all(Radius.circular(35)),
                                          shape: BoxShape.rectangle
                                      ),
                                      child:Padding(
                                        padding: const EdgeInsets.only(top:2.0,bottom: 2.0,left:6,right: 6),
                                        child: AutoSizeText(
                                          (chatClass.new_mes_cnt).toString(),
                                          textAlign: TextAlign.center,
                                          style: TextStyle(

                                              fontWeight: FontWeight.bold,
                                              color: chatClass.new_mes_cnt!=0?OwnColors.white:OwnColors.tran,
                                              fontSize: 16.nsp),
                                        ),
                                      )
                                  ),
                                ),
                                Spacer(),
                              ],
                            )


                          ],
                        ),
                      ),
                      HorizontalSpace().create(context, 0.002),
                      // Padding(
                      //   padding: const EdgeInsets.only(top:0.0, right: 0.0),
                      //   child: Align(
                      //     alignment: Alignment.centerRight,
                      //     child: Container(
                      //       width: ScreenSize().getWidth(context) * 0.8,
                      //       height: 1,
                      //       color: OwnColors.CC8C8C8,
                      //     ),
                      //   ),
                      // )
                    ],
                  ),
                ),
              ),
            ),
            new Positioned.fill(
                child: new Material(
                  color: Colors.transparent,
                  child: new GestureDetector(
                    onTap: ()  {
                      itemClick(chatClass);


                    },
                  ),
                )),
          ]),

        ],
      ),
    );


  }


  @override
  void dispose() {
    super.dispose();
    print("dispose");
    if (subscription != null) {
      subscription.cancel();
      subscription = null;
    }
    FirebaseRealtime.cancelSub();
  }
  void itemClick(ChatClass chatClass ){
    searchEdit.text = "";
    PushNewScreen().normalPush(context, ChattingPage(chat_title: chatClass.sender_name, chat_avatar: chatClass.sender_avatar,send_to_id: chatClass.sender_id,send_to_fcm: chatClass.send_to_fcm,));
  }




  void chatBtnClick(int index){

  }


  Future<void> getChatPersonList() async {
    print("getChatPersonList");
    var db = new DatabaseHelper();
    // List<CCModel> ccList = await db.getCCByNowID(SharePre.prefs.getString(GDefine.user_id));
    List<CCModel> ccList = new List();

    for (CCModel ccModel in FirebaseRealtime.chatPersonList){
      int a = ccList.indexWhere((element) => element.userId == ccModel.userId);
      if(a == -1){
        ccList.add(ccModel);
      }
    }
    // ccList.addAll(FirebaseRealtime.chatPersonList);
    itemList.clear();
    print("FirebaseRealtime.chatPersonList " + FirebaseRealtime.chatPersonList.length.toString());


    for(CCModel ccModel in ccList){


      List<ChatModel> last = await db.getChatByNowIDGroupIDLast(SharePre.prefs.getString(GDefine.user_id), ccModel.userId);
      List<ChatModel> unread = await db.getChatByNowIDGroupIDUnread(SharePre.prefs.getString(GDefine.user_id), ccModel.userId);
      print(ccModel.realName);
      print(ccModel.userPic);
      print(ccModel.userId);
      if(!GDefine.blockUserId.contains(ccModel.userId)) {
        if (last.isNotEmpty) {
          DateTime d = DateTime.parse(last[0].time);
          // String dd = DateTools().dateLineFormate2(d);

          itemList.add(new ChatClass(sender_id: ccModel.userId,
              sender_name: ccModel.realName,
              sender_avatar: ccModel.userPic,
              time: last[0].time,
              timeStamp: last[0].timeStamp,
              t: last[0].t,
              message: last[0].t == "m" ? last[0].message : "圖片",
              new_mes_cnt: unread.length,
              send_to_fcm: ccModel.FCMToken));
        } else {
          itemList.add(new ChatClass(sender_id: ccModel.userId,
              sender_name: ccModel.realName,
              sender_avatar: ccModel.userPic,
              time: "",
              timeStamp: "0",
              t: "",
              message: "",
              new_mes_cnt: 0,
              send_to_fcm: ccModel.FCMToken));
        }
      }
    }
    itemList.sort((a, b) => int.parse(b.timeStamp).compareTo(int.parse(a.timeStamp)));
    if(itemList.isEmpty){
      noMessShow = true;
    }else {
      noMessShow = false;
    }
    if(mounted) {
      setState(() {
        print("setState");
      });
    }

    refreshController.finishRefresh();
    refreshController.resetFooter();



  }



  Future<void> getChatList() async {
    print("getChatList");
    var db = new DatabaseHelper();
    // List<CCModel> ccList = await db.getCCByNowID(SharePre.prefs.getString(GDefine.user_id));
    List<CCModel> ccList = new List();
    ccList.addAll(FirebaseRealtime.chatPersonList);
    itemList.clear();
    print("FirebaseRealtime.chatPersonList " + FirebaseRealtime.chatPersonList.length.toString());
    List<ChatModel> ss = await db.getChat( );
    for(ChatModel c in ss){
      print (c.sender_name);
      print (c.message);
      print (c.time);
      print ("-----");

    }

    for(CCModel ccModel in ccList){


      List<ChatModel> last = await db.getChatByNowIDGroupIDLast(SharePre.prefs.getString(GDefine.user_id), ccModel.userId);
      List<ChatModel> unread = await db.getChatByNowIDGroupIDUnread(SharePre.prefs.getString(GDefine.user_id), ccModel.userId);
      print(ccModel.realName);
      print(ccModel.userPic);
      print(ccModel.userPic);



      if(!GDefine.blockUserId.contains(ccModel.userId)) {
        if (last.isNotEmpty) {
          print("last[0].time " + last[0].time);
          print("last[0].t " + last[0].t);
          print("last[0].message " + last[0].message);
          print("last[0].name " + last[0].sender_name);
          itemList.add(new ChatClass(sender_id: ccModel.userId,
              sender_name: ccModel.realName,
              sender_avatar: ccModel.userPic,
              time: last[0].time,
              timeStamp: last[0].timeStamp,
              t: last[0].t,
              message: last[0].t == "m" ? last[0].message : "圖片",
              new_mes_cnt: unread.length,
              send_to_fcm: ccModel.FCMToken));
        } else {
          itemList.add(new ChatClass(sender_id: ccModel.userId,
              sender_name: ccModel.realName,
              sender_avatar: ccModel.userPic,
              time: "",
              timeStamp: "0",
              t: "",
              message: "",
              new_mes_cnt: 0,
              send_to_fcm: ccModel.FCMToken));
        }
      }
    }
    itemList.sort((a, b) => int.parse(b.timeStamp).compareTo(int.parse(a.timeStamp)));
    if(mounted) {
      setState(() {
        print("setState");
      });
    }
    if(itemList.length==0){
      setState(() {
        noMessShow = true;
      });

    }

    refreshController.finishRefresh();

  }



  Future<void> searchChat() async {
    print("searchChat");
    print("tabState" + tabState.toString());
    print("searchEdit.text" + searchEdit.text);
    itemList.clear();
    var db = new DatabaseHelper();
    List<CCModel> ccList =    FirebaseRealtime.chatPersonList.where((element) => element.realName.toLowerCase().contains(searchEdit.text.toLowerCase()) ).toList();

    // await db.getCCByNowIDName(SharePre.prefs.getString(GDefine.user_sn),tabState==1?"0":"1",searchEdit.text);

    print(FirebaseRealtime.chatPersonList.length);
    print(ccList.length);
    print(itemList.length);
    for(CCModel ccModel in ccList){
      List<ChatModel> last = await db.getChatByNowIDGroupIDLast(SharePre.prefs.getString(GDefine.user_id), ccModel.userId);
      List<ChatModel> unread = await db.getChatByNowIDGroupIDUnread(SharePre.prefs.getString(GDefine.user_id), ccModel.userId);

      if (last.isNotEmpty) {
        print("isNotEmpty");

        itemList.add(new ChatClass(sender_id: ccModel.userId,
            sender_name: ccModel.realName,
            sender_avatar: ccModel.userPic,
            time: last[0].time,
            timeStamp: last[0].timeStamp,
            t: last[0].t,
            message: last[0].message,
            new_mes_cnt: unread.length,
            send_to_fcm: ccModel.FCMToken));
      }else {
        print("isEmpty");
        itemList.add(new ChatClass(sender_id: ccModel.userId,
            sender_name: ccModel.realName,
            sender_avatar: ccModel.userPic,
            time: "",
            timeStamp: "0",
            t: "",
            message: "",
            new_mes_cnt: 0,
            send_to_fcm: ccModel.FCMToken));
      }
    }
    itemList.sort((a, b) => int.parse(b.timeStamp).compareTo(int.parse(a.timeStamp)));
    if(mounted) {
      setState(() {

      });
    }
  }
}