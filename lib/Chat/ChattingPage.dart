import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';


// import 'package:image/image.dart' as img;
import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:bubble/bubble.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/OrangeXCross.dart';
import 'package:ctour/GlobalTask.dart';
import 'package:ctour/Guild/GuildClass.dart';
import '../ImgPage.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Firebase/FirebaseRealtime.dart';
import 'package:ctour/Chat/ChatMessClass.dart';
import 'package:ctour/Guild/GuilderPage.dart';
// import 'package:ctour/Chat/ChattingImgPage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/ReportPage.dart';
import 'package:ctour/SQLite/Database.dart';
import 'package:ctour/SQLite/model/ChatModel.dart';
import 'package:ctour/Travel/TravelerPage.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image/image.dart' as img22;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:selectable_autolink_text/selectable_autolink_text.dart';


import 'package:sticky_grouped_list/sticky_grouped_list.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:url_launcher/url_launcher.dart';

class ChattingPage extends StatefulWidget {
  final String chat_title;
  final String chat_avatar;
  final String send_to_id;
  final String send_to_fcm;

  const ChattingPage({
    Key key,
    this.chat_title,
    this.chat_avatar,
    this.send_to_id,
    this.send_to_fcm,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ChattingPageState();
  }
}

class ChattingPageState extends State<ChattingPage> {
  TextEditingController textEditingController = new TextEditingController();
  GroupedItemScrollController groupedItemScrollController =
      new GroupedItemScrollController();
  List<Map> joinListMQTT = new List();
  final ScrollController listScrollController = ScrollController();

  List<ChatMessClass> chatList = new List();

  int _limit = 20;
  int _limitIncrement = 20;

  ChatMessClass chatMessLast;

  bool start = false;

  int state = 0; //0 wait 1 play
  StreamSubscription<DatabaseEvent> fireBaseDBSubScription;
  String imgBase64 = "";
  String imgExt = "";
  List<String> uploadingImg = new List();
  List<int> uploadingImgDBId = new List();
  Uint8List imgUnit8  ;

  @override
  void initState() {
    super.initState();


    listScrollController.addListener(_scrollListener);
    print("send_to_id " +widget.send_to_id);

    // chatList.add(new ChatMessClass(
    //   sender_id: "5",date: DateTime.parse("2020-05-01"),timeStamp: "195698",
    //   sender_avatar: "https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg",
    // t: "m",
    // suc: "1",message:"DDD"));
    //
    // chatList.add(new ChatMessClass(
    //     sender_id: "10",date: DateTime.parse("2020-05-01"),timeStamp: "195698",
    //     sender_avatar: "https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg",
    //     t: "m",
    //     suc: "1",message:"DDD"));

    fireBaseDBSubScription = FirebaseRealtime.fireBaseDB
        .child("Message")
        .child(SharePre.prefs.getString(GDefine.user_id))
        .child("unread")
        .orderByChild("timeStamp")
        .onValue
        .listen((DatabaseEvent event) {
      if (event.snapshot.value != null) {
        Map map = event.snapshot.value;
        print("Chatting Page receive : " + map.toString());
        print("Chatting Page receive : " + map.keys.toString());
        List<Object> sender_id_list = map.keys.toList();

        for (int j = 0; j < sender_id_list.length; j++) {
          if (sender_id_list[j] == widget.send_to_id) {
            Map sub = map[sender_id_list[j]];
            List<Object> key_sub = sub.keys.toList();
            for (int i = key_sub.length - 1; i >= 0; i--) {
              String chat_id = key_sub[i];
              Map mess_obj = sub[chat_id];
              print("mess_obj " + mess_obj.toString());
              if (!chatList.any((element) => element.chat_id == chat_id)) {
                ChatMessClass chatMessClass = new ChatMessClass(
                    chat_id: chat_id,
                    sender_name: mess_obj["sender_name"],
                    sender_avatar: mess_obj["sender_avatar"],
                    sender_id: mess_obj["sender_id"],
                    message: mess_obj["message"],
                    date: DateTime.parse(mess_obj["time"]),
                    timeStamp: mess_obj["timeStamp"],
                    t: mess_obj["t"],
                    suc: '1');
                chatList.insert(0, chatMessClass);
                // FirebaseRealtime.removeUnread(widget.send_to_id,chat_id);
              }
            }

            break;
          }
        }

        Future.delayed(Duration(milliseconds: 10), () {
          setRead();
        });

        setState(() {});
      }
    });
    getChatData();
  }

  _scrollListener() {
    print(listScrollController.offset);
    print(listScrollController.position.maxScrollExtent);
    // Tools().dismissK(context);
    // if (listScrollController.offset >=
    //         listScrollController.position.maxScrollExtent &&
    //     !listScrollController.position.outOfRange) {
    //   setState(() {
    //     _limit += _limitIncrement;
    //   });
    // }
  }

  void _scrollToBottom() {
    if (groupedItemScrollController.isAttached) {
      groupedItemScrollController.scrollTo(
          index: 0,
          duration: Duration(milliseconds: 300),
          curve: Curves.elasticOut);
    } else {
      Timer(Duration(milliseconds: 400), () => _scrollToBottom());
    }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: OwnColors.white,
      body: GestureDetector(
        onTap: (){
          Tools().dismissK(context);
        },
        child: Center(
          child: FocusDetector(
            onFocusGained: () {
              print("onFocusGained");
            },
            onForegroundLost: () {
              print("onForegroundLost");
            },
            child: SafeArea(
              child: Stack(
                children: [
                  Container(
                    width: ScreenSize().getWidth(context),
                    height: 1 * ScreenSize().getHeight(context),
                    child: Column(
                      children: [
                        HorizontalSpace().create(context, 0.01),
                        Topbar().titleCreateMenuChat(context,widget.chat_title,
                            ScreenSize().getHeight(context) * 0.073,  backClick ,true,showMenu,goPersonal),

                        Expanded(
                          // height: 200,
                          child: GestureDetector(
                            onTap:(){
                              Tools().dismissK(context);
                               },
                            child: Stack(
                              children: [
                                Positioned.fill(

                                    child: Container(
                                  color: OwnColors.white,
                                )),
                                ScrollConfiguration(
                                  behavior: MyBehavior(),
                                  child: ListView.builder(
                                    // shrinkWrap :true,
                                    // primary: false,
                                    keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
                                    shrinkWrap: true,
                                    // physics: BouncingScrollPhysics(),
                                    padding: EdgeInsets.all(10.0),
                                    itemBuilder: (context, index) =>
                                        mess(chatList[index], index),
                                    itemCount: chatList.length,
                                    reverse: true,
                                    controller: listScrollController,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: ScreenSize().getWidth(context),
                          height: ScreenSize().getHeight(context) * 0.097,
                        )
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    child: Container(
                        width: ScreenSize().getWidth(context),
                        height: ScreenSize().getHeight(context) * 0.097,
                        decoration: BoxDecoration(
                          color: OwnColors.white,
                          border: Border(
                            top: BorderSide(width: 1.0, color: OwnColors.white),
                          ),
                        ),
                        child: sendMess()),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget mess(ChatMessClass chatMess, int index) {
    if (chatMess.sender_id.contains(SharePre.getString(GDefine.user_id))) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: [
          Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: const EdgeInsets.only(right: 4.0),
              child: Text(
                "${chatMess.date.hour.toString().padLeft(2, "0")}:${chatMess.date.minute.toString().padLeft(2, "0")}",
                style: TextStyle(color: OwnColors.Gray, fontSize: 10.nsp),
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(right: 4.0, left: 0.0),
            child: isLastMessage(index, chatMess.sender_id)
                ? chatMess.t == "i"
                ? imgMess(chatMess) : Bubble(
                    radius: Radius.circular(16),
                    margin: BubbleEdges.only(top: 10),
                    padding: BubbleEdges.only(
                        left: 10, right: 10, top: 8, bottom: 8),
                    alignment: Alignment.topRight,
                    nip: BubbleNip.rightTop,
                    nipOffset: 10,
                    color: OwnColors.CTourOrange,
                    child: Container(
                        constraints: BoxConstraints(maxWidth: 0.6.sw),
                        child:
                        SelectableAutoLinkText(
                            chatMess.message,
                            linkStyle: TextStyle(
                              color: Colors.blueAccent,
                              decoration: TextDecoration.underline,
                            ),
                            highlightedLinkStyle: TextStyle(
                              color: Colors.blueAccent,
                              backgroundColor:
                              Colors.blueAccent.withAlpha(0x33),
                            ),
                            onTap: (url) =>
                                launch(url, forceSafariVC: false),
                            style: TextStyle(
                                fontSize: 15.nsp,
                                color: OwnColors.white),
                            textAlign: TextAlign.left,moreBool: false,)


                      // Text(chatMess.message,
                        //         softWrap: true,
                        //         style: GoogleFonts.inter(
                        //             fontSize: 15.nsp, color: OwnColors.white),
                        //         textAlign: TextAlign.left)


                    ),
                  )
                : chatMess.t == "i"?imgMess(chatMess) : Bubble(
                    radius: Radius.circular(16),
                    padding: BubbleEdges.only(
                        left: 10, right: 10, top: 8, bottom: 8),
                    margin: BubbleEdges.only(top: 10, right: 8, left: 8),
                    alignment: Alignment.topRight,
                    nip: BubbleNip.no,
                    color: OwnColors.CTourOrange,
                    child: Container(
                        constraints: BoxConstraints(maxWidth: 0.6.sw),
                        child:


                        SelectableAutoLinkText(
                            chatMess.message,
                            linkStyle: TextStyle(
                              color: Colors.blueAccent,
                              decoration: TextDecoration.underline,
                            ),
                            highlightedLinkStyle: TextStyle(
                              color: Colors.blueAccent,
                              backgroundColor:
                              Colors.blueAccent.withAlpha(0x33),
                            ),
                            onTap: (url) =>
                                launch(url, forceSafariVC: false),
                            style: TextStyle(
                                fontSize: 15.nsp,
                                color: OwnColors.white),
                            textAlign: TextAlign.left,moreBool: false)

                        // SelectableText(chatMess.message,
                        //         // softWrap: true,
                        //         style: TextStyle(
                        //             fontSize: 15.nsp, color: OwnColors.white),
                        //         textAlign: TextAlign.left)


                    ),
                  ),
          ),
        ],
      );
    } else {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            child: Container(
              width: 0.1 * ScreenSize().getWidth(context),
              child: AspectRatio(
                  aspectRatio: 1,
                  child: isLastMessage(index, chatMess.sender_id)
                      ? CachedNetworkImage(
                    placeholder: (context, url) => AspectRatio(
                      aspectRatio: 1,
                      child: FractionallySizedBox(
                        heightFactor: 0.5,
                        widthFactor: 0.5,
                        child: SpinKitRing(
                          lineWidth:5,
                          color:
                          OwnColors.tran ,
                        ),
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(
                      Icons.error,
                      color: OwnColors.black.withOpacity(0.5),
                    ),
                    imageUrl: widget.chat_avatar,
                    // imageUrl: chatMess.sender_avatar,
                    fit: BoxFit.fitHeight,
                    imageBuilder: (context, imageProvider) => Container(
                      // width: 80.0,
                      // height: 80.0,
                      decoration: BoxDecoration(
                        color: OwnColors.tran,
                        shape: BoxShape.circle,
                        border:
                        Border.all(color: OwnColors.white, width: 0.0),
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                    ),
                  )
                      : SizedBox()),
            ),
            onTap:() async {

              GuildClass guildclass = await GlobalTask().getGuild(context, widget.send_to_id);
              if(guildclass!=null) {
                PushNewScreen().normalPush(context,
                    GuilderPage(guildClass: guildclass, preview: false,));
              }else {
                PushNewScreen().normalPush(context,
                    GuilderPage(id: widget.send_to_id, preview: false,));
              }
            },
          ),
          Container(
              padding: const EdgeInsets.only(right: 0.0, left: 8.0),
              child: isLastMessage(index, chatMess.sender_id)
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        chatMess.t == "i"
                            ? imgMess(chatMess)
                            : Bubble(
                          radius: Radius.circular(16),
                          padding: BubbleEdges.only(
                              left: 10, right: 10, top: 8, bottom: 8),
                          margin: BubbleEdges.only(top: 10, right: 0),
                          alignment: Alignment.bottomLeft,
                          nip: BubbleNip.leftTop,
                          color: OwnColors.C989898,
                          nipOffset: 10,
                          child: Container(
                              constraints: BoxConstraints(maxWidth: 0.6.sw),
                              child:
                              SelectableAutoLinkText(
                                chatMess.message,
                                linkStyle: TextStyle(color: Colors.blueAccent,decoration: TextDecoration.underline,),
                                highlightedLinkStyle: TextStyle(
                                  color: Colors.blueAccent,
                                  backgroundColor: Colors.blueAccent.withAlpha(0x33),
                                ),
                                onTap: (url) => launch(url, forceSafariVC: false),
                                  style: TextStyle(
                                      fontSize: 15.nsp,
                                      color: OwnColors.white),
                                  textAlign: TextAlign.left,moreBool: false),
                              )

                              // SelectableText(chatMess.message,
                              //         // softWrap: true,
                              //         style: TextStyle(
                              //             fontSize: 15.nsp,
                              //             color: OwnColors.white),
                              //         textAlign: TextAlign.left)),
                        ),
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: Text(
                              "${chatMess.date.hour.toString().padLeft(2, "0")}:${chatMess.date.minute.toString().padLeft(2, "0")}",
                              style: TextStyle(
                                  color: OwnColors.Gray, fontSize: 10.nsp),
                            ),
                          ),
                        )
                      ],
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        chatMess.t == "i"
                            ? imgMess(chatMess)
                            : Bubble(
                          radius: Radius.circular(16),
                          padding: BubbleEdges.only(
                              left: 10, right: 10, top: 8, bottom: 8),
                          margin: BubbleEdges.only(top: 10, left: 8, right: 8),
                          alignment: Alignment.bottomLeft,
                          nip: BubbleNip.no,
                          nipOffset: 10,
                          color: OwnColors.C989898,
                          child: Container(
                              constraints: BoxConstraints(maxWidth: 0.6.sw),
                              child: SelectableAutoLinkText(
                                      chatMess.message,
                                      linkStyle: TextStyle(
                                        color: Colors.blueAccent,
                                        decoration: TextDecoration.underline,
                                      ),
                                      highlightedLinkStyle: TextStyle(
                                        color: Colors.blueAccent,
                                        backgroundColor:
                                            Colors.blueAccent.withAlpha(0x33),
                                      ),
                                      onTap: (url) =>
                                          launch(url, forceSafariVC: false),
                                      style: TextStyle(
                                          fontSize: 15.nsp,
                                          color: OwnColors.white),
                                      textAlign: TextAlign.left,moreBool: false),
                                )

                                //
                              // SelectableText(chatMess.message,
                              //         // softWrap: true,
                              //         style: TextStyle(
                              //             fontSize: 15.nsp,
                              //             color: OwnColors.white),
                              //         textAlign: TextAlign.left)),
                        ),
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: Text(
                              "${chatMess.date.hour.toString().padLeft(2, "0")}:${chatMess.date.minute.toString().padLeft(2, "0")}",
                              style: TextStyle(
                                  color: OwnColors.Gray, fontSize: 10.nsp),
                            ),
                          ),
                        )
                      ],
                    )),
        ],
      );
    }
  }

  Widget imgMess(ChatMessClass chatMess) {
    print(chatMess.message);
    print(chatMess.t);
    return chatMess.suc == "1" && chatMess.message.contains("http")
        ? Padding(
          padding: const EdgeInsets.only(top:12.0),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: GestureDetector(
                onTap: (){
                  print("onTap");
                  PushNewScreen().normalPush(context, ImgPage(url:chatMess.message ,));
                  // PushNewScreen().normalPush(context, ChattingImgPage(url: chatMess.message,));
                },
                child: Container(
                  width: 0.65.sw,
                  child: CachedNetworkImage(

                      imageUrl: chatMess.message,

                      errorWidget: (context, url, error) => Icon(Icons.error)),
                ),
              )),
        )
        : Padding(
      padding: const EdgeInsets.only(top:12.0),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: Stack(
                children: [
                  Container(
                    width: 0.65.sw,
                    child: Image.memory(
                        base64Decode(chatMess.message.split(";base64,").last)),
                  ),
                  Positioned.fill(
                      child: Container(
                    color: OwnColors.black.withOpacity(0.5),
                    child: chatMess.suc == "2"
                        ? Center(
                            child: Icon(
                            Icons.error_outline,
                            color: Colors.red,
                            size: 45,
                          ))
                        : Center(
                            child: SizedBox(
                                width: 45,
                                height: 45,
                                child: Image.asset(Res.upload))),
                  ))
                ],
              )),
        );
  }

  Container sendMess() {
    return Container(
      decoration: BoxDecoration(color: OwnColors.tran,          border: Border(
        top: BorderSide(width: 1.0, color: OwnColors.CF4F4F4),

      ), ),
      child: Row(children: [
        VerticalSpace().create(context, 0.032),
        // Container(
        //   height: ScreenSize().getHeight(context) * 0.05,
        //   child: AspectRatio(
        //     aspectRatio: 1,
        //     child: Material(
        //       color: Colors.transparent, // button color
        //       child: GestureDetector(
        //
        //         child: Padding(
        //           padding: const EdgeInsets.all(4.0),
        //           child: SizedBox(
        //               width: 45, height: 45, child: Icon(SFSymbols.camera, size: 26,)),
        //         ),
        //         onTap: () {
        //           pcikImageFromCamera();
        //         },
        //       ),
        //     ),
        //   ),
        // ),
        // VerticalSpace().create(context, 0.02),
        Container(
          height: ScreenSize().getHeight(context) * 0.05,
          child: AspectRatio(
            aspectRatio: 1.3,
            child: Material(
              color: Colors.transparent, // button color
              child: GestureDetector(

                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child:
                  imgUnit8==null? SizedBox(
                      width:  ScreenSize().getHeight(context) * 0.05/1.3,  height: ScreenSize().getHeight(context) * 0.05,
                      child:  Icon(SFSymbols.photo, size: ScreenSize().getHeight(context) * 0.05/1.3,)


                  ): Stack(
                    children: [
                      GestureDetector(onTap:(){
                        showBottomMenuVideo();
                      },child: Padding(
                        padding: EdgeInsets.only(top:0.007.sh,right:0.007.sh),
                        child: AspectRatio(
                            aspectRatio: 4/3,
                            child:ClipRRect(
                              borderRadius: BorderRadius.circular(5), // Image border
                              child: SizedBox.fromSize(
                                size: Size.fromRadius(5), // Image radius
                                child: Image.memory(imgUnit8,fit: BoxFit.cover,),
                              ),
                            )
                        ),
                      )),

                      Positioned(
                        top:0,
                        right: 0,
                        child: GestureDetector(
                          onTap: (){
                            imgUnit8 = null;
                            imgBase64 = "";
                            setState(() {

                            });
                          },
                          child: OrangeXCross().createWH(context,0.018.sh),
                        ),
                      )

                    ],
                  )
                  ,
                ),
                onTap: () {
                  showBottomMenuVideo();
                  // pcikImageFromPhoto();
                },
              ),
            ),
          ),
        ),
        VerticalSpace().create(context, 0.02),
        Expanded(
          child: Container(
            height: ScreenSize().getHeight(context) * 0.05,
            decoration: BoxDecoration(
              color: OwnColors.Gray,
              border: Border.all(width: 1,color: OwnColors.C989898),
              borderRadius: BorderRadius.all(Radius.circular(80)),

            ),
            alignment: Alignment.center,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                maxHeight: 300.0,
              ),
              child: TextField(
                autofocus: false,
                controller: textEditingController,
                decoration: InputDecoration(
                    fillColor: OwnColors.white,
                    contentPadding: EdgeInsets.symmetric(horizontal: 15),
                    //Change this value to custom as you like
                    // isDense: true,
                    filled: true,
                    hintText: S().send_mes_hint,
                    hintStyle: TextStyle(
                        color: OwnColors.C989898, fontSize: 15.nsp),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(20.0),
                        borderSide: BorderSide.none)),
                style: TextStyle(
                    color: Colors.black.withOpacity(1), fontSize: 15.nsp),
                maxLines: null,
                expands: true,
                keyboardType: TextInputType.multiline,
                obscureText: false,
                onChanged: (S) {
                  setState(() {

                  });
                },
              ),
            ),
          ),
        ),
        VerticalSpace().create(context, 0.03),
        ClipOval(
          child: Material(
            color: Colors.transparent, // button color
            child: GestureDetector(

              child: SizedBox(
                  width: ScreenSize().getHeight(context) * 0.05, height: ScreenSize().getHeight(context) * 0.05,
                  child:Icon(SFSymbols.arrow_up_circle_fill, color: (textEditingController.text.toString().length > 0 || imgUnit8 != null)?OwnColors.CTourOrange:OwnColors.CC8C8C8,size: 33,)),
              onTap: () {
                if((textEditingController.text.toString().length > 0 || imgUnit8 != null)){
                  sendMessBtn();
                }

              },
            ),
          ),
        ),
        VerticalSpace().create(context, 0.032),
      ]),
    );
  }

  @override
  void dispose() {
    super.dispose();
    print("dispose");
    if (fireBaseDBSubScription != null) {
      fireBaseDBSubScription.cancel();
      fireBaseDBSubScription = null;
    }
  }

  void donateClick(){

  }

  bool isLastMessage(int index, String id) {
    print("index ${index}  id ${id}");
    if (index < chatList.length - 1) {
      if ((chatList[index + 1].sender_id != id) ||
          index == chatList.length - 1) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  void sendMessBtn() {
    print("sendMessBtn");
    //
    print("sednoer id " + SharePre.getString(GDefine.user_id));

    if(imgUnit8 != null){
      sendImg(imgBase64);
    }
    if (Tools().stringNotNullOrSpace( textEditingController.text) ) {
      String content = textEditingController.text;
      DateTime date = DateTime.now();
      String timeStamp = DateTime.now().millisecondsSinceEpoch.toString();

      ChatMessClass chatMessClass = ChatMessClass(
          date: date,
          timeStamp: timeStamp,
          sender_avatar: SharePre.getString(GDefine.avatar),
          sender_id: SharePre.getString(GDefine.user_id),
          sender_name: SharePre.getString(GDefine.name),
          message: content,
          t: "m",
          suc: '1');
      chatList.insert(0, chatMessClass);
      textEditingController.text = "";
      setState(() {});
      chatMessLast = chatMessClass;
      listScrollController.animateTo(0.0,
          duration: Duration(milliseconds: 300), curve: Curves.easeOut);
      insertMyDB(content, DateFormat('yyyy-MM-dd HH:mm:ss').format((date)),
          timeStamp, "m");

      FirebaseRealtime.pushMyMessage(widget.send_to_id,widget.chat_avatar,widget.chat_title, content, "m",
          DateFormat('yyyy-MM-dd HH:mm:ss').format((date)), timeStamp);

      FirebaseRealtime.pushToMessage(widget.send_to_id, content, "m",
          DateFormat('yyyy-MM-dd HH:mm:ss').format((date)), timeStamp);

      sendFCM(content);
    }
  }

  Future<void> sendImg(String data) async {

    DateTime date = DateTime.now();
    String timeStamp = DateTime.now().millisecondsSinceEpoch.toString();

    uploadingImg.add(data);
    int id = await insertMyDBImage(
        data, DateFormat('yyyy-MM-dd HH:mm:ss').format((date)), timeStamp, "i");
    uploadingImgDBId.add(id);

    ChatMessClass chatMessClass = ChatMessClass(
        date: date,
        timeStamp: timeStamp,
        sender_avatar: SharePre.getString(GDefine.avatar),
        sender_id: SharePre.getString(GDefine.user_id),
        sender_name: SharePre.getString(GDefine.name),
        message: data,
        t: "i",
        suc: "0");
    chatList.insert(0, chatMessClass);
    setState(() {});

    ResultData resultData = await AppApi.getInstance().setFileBase64(
        context,
        true,

        data);
    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      // Notice how you have to call body from the response if you are using http to retrieve json
      String url = GDefine.imageUrl+resultData.data["path"];
      int index = uploadingImg.indexWhere((element) => data == element);
      var db = new DatabaseHelper();
      List<ChatModel> dbChat = await db.getChatByID(uploadingImgDBId[index]);
      if (dbChat.length > 0) {
        ChatModel chatModel = dbChat[0];
        chatModel.suc = "1";
        chatModel.message = url;
        db.insertChat(chatModel);

        int index2 = chatList.indexWhere((element) =>
            data == element.message &&
            element.timeStamp == chatModel.timeStamp);
        chatList[index2].message = url;
        chatList[index2].suc = "1";

        FirebaseRealtime.pushMyMessage(widget.send_to_id,widget.chat_avatar,widget.chat_title, url, "i",
            chatModel.time, chatModel.timeStamp);

        // FirebaseRealtime.pushMyMessage(
        //     widget.send_to_id, url, "i", chatModel.time, chatModel.timeStamp);
        FirebaseRealtime.pushToMessage(
            widget.send_to_id, url, "i", chatModel.time, chatModel.timeStamp);
        sendFCM(S().image);
      }
      uploadingImg.removeAt(index);
      uploadingImgDBId.removeAt(index);

    } else {
      //上傳失敗
      int index = uploadingImg.indexWhere((element) => data == element);
      var db = new DatabaseHelper();
      List<ChatModel> dbChat = await db.getChatByID(uploadingImgDBId[index]);
      if (dbChat.length > 0) {
        ChatModel chatModel = dbChat[0];
        chatModel.suc = "2";
        db.insertChat(chatModel);
        int index2 = chatList.indexWhere((element) =>
            data == element.message &&
            element.timeStamp == chatModel.timeStamp);
        if(index2!=-1) {
          chatList[index2].suc = "2";

        }

      }
      uploadingImg.removeAt(index);
      uploadingImgDBId.removeAt(index);
    }

    imgBase64 = "";
    imgUnit8 = null;

    setState(() {});
  }

  void insertMyDB(String content, String time, String timeStamp, String t) {
    var db = new DatabaseHelper();
    ChatModel chatModel = new ChatModel(
      now_id: SharePre.prefs.getString(GDefine.user_id),
      chat_id: "",
      chat_group_id: widget.send_to_id,
      sender_name: SharePre.prefs.getString(GDefine.name),
      sender_avatar: SharePre.prefs.getString(GDefine.avatar),
      sender_id: SharePre.prefs.getString(GDefine.user_id),
      message: content,
      time: time,
      t: t,
      timeStamp: timeStamp,
      read: "1",
      suc: "1",
    );
    db.insertChat(chatModel);
  }

  Future<int> insertMyDBImage(
      String content, String time, String timeStamp, String t) {
    var db = new DatabaseHelper();
    ChatModel chatModel = new ChatModel(
      now_id: SharePre.prefs.getString(GDefine.user_id),
      chat_id: "",
      chat_group_id: widget.send_to_id,
      sender_name: SharePre.prefs.getString(GDefine.name),
      sender_avatar: SharePre.prefs.getString(GDefine.avatar),
      sender_id: SharePre.prefs.getString(GDefine.user_id),
      message: content,
      time: time,
      t: t,
      timeStamp: timeStamp,
      read: "1",
      suc: "0",
    );

    return db.insertChatReturnID(chatModel);
  }

  void backClick() {
    // SharePre.prefs.setString(GDefine.user_id, "38");
    print("back");

      Tools().dismissK(context);

    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }

  Future<void> getChatData() async {
    var db = new DatabaseHelper();
    print("widget.send_to_id " + widget.send_to_id);

    List<ChatModel> ccList = await db.getChatByNowIDGroupID(
        SharePre.prefs.getString(GDefine.user_id), widget.send_to_id);
    ccList.reversed;
    for (ChatModel c in ccList) {
      ChatMessClass chatMessClass = new ChatMessClass(
          chat_id: c.chat_id,
          sender_name: c.sender_name,
          sender_avatar: c.sender_avatar,
          sender_id: c.sender_id,
          message: c.message,
          date: DateTime.parse(c.time),
          timeStamp: c.timeStamp,
          suc: c.suc,
          t: c.t);
      print("chatMessClass.time " + chatMessClass.timeStamp.toString());
      print("chatMessClass.time " + chatMessClass.message.toString());
      print("chatMessClass.suc " + chatMessClass.suc.toString());
      if(chatMessClass.suc=="1") {
        chatList.insert(0, chatMessClass);
      }
    }
    chatList.sort(
        (a, b) => int.parse(b.timeStamp).compareTo(int.parse(a.timeStamp)));
    setState(() {});
    setRead();
  }

  Future<void> setRead() async {
    var db = new DatabaseHelper();
    print("setRead ");
    List<ChatModel> ccList = await db.getChatByNowIDGroupIDUnread(
        SharePre.prefs.getString(GDefine.user_id), widget.send_to_id);

    for (ChatModel c in ccList) {
      c.read = "1";
      db.insertChat(c);
    }
    setState(() {});
  }

  void showBottomMenuVideo() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 150,
              child: Column(children: <Widget>[
                ListTile(
                  leading: Icon(Icons.photo_camera),
                  title: Text(S().take_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromCamera();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.photo_library),
                  title: Text(S().choose_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromPhoto();
                  },
                )
              ]));
        });
  }

  final picker = ImagePicker();


  void pcikImageFromCamera() async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);

    File img = File(pickedFile.path);
    img22.Image capturedImage = img22.decodeImage(await File(pickedFile.path).readAsBytes());
    img22.Image orientedImage = img22.bakeOrientation(capturedImage);
    img = await File(pickedFile.path).writeAsBytes(img22.encodeJpg(orientedImage));

    List<int> imageBytes = await img.readAsBytes();
    imgUnit8 = imageBytes;
    imgBase64 = base64Encode(imageBytes);
    imgExt = pickedFile.path.toString().split(".").last;
    imgBase64 = "data:image/" + imgExt + ";base64," + imgBase64;
    setState(() {

    });
    // sendImg(imgBase64);
  }

  void pcikImageFromPhoto() async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);
    File img = File(pickedFile.path);
    List<int> imageBytes = await img.readAsBytes();
    imgUnit8 = imageBytes;

    imgBase64 = base64Encode(imageBytes);
    imgExt = pickedFile.path.toString().split(".").last;
    imgBase64 = "data:image/" + imgExt + ";base64," + imgBase64;
    // sendImg(imgBase64);
    setState(() {

    });
  }


  Future<void> sendFCM( String content) async {

      ResultData resultData = await AppApi.getInstance().sendSingeFCM(context, true,
          widget.send_to_id,
          SharePre.prefs.getString(GDefine.name)  , content
      );

  }

  Future<void> showMenu() async {
    Tools().dismissK(context);
    int result = await showCupertinoModalBottomSheet(
        barrierColor:Colors.black54,
        topRadius: Radius.circular(30),
        expand: false,
        context: context,
        enableDrag: true,
        backgroundColor: Colors.transparent,
        builder: (context) => GestureDetector(
          onTap:(){
            Tools().dismissK(context);
            print("onTap");
          },
          child: Material(
              color: OwnColors.white,
              child: Container(
                // height:  ScreenSize().getHeightWTop(context),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[

                    Container(
                        height: 0.007 * ScreenSize().getHeight(context),
                        width: 0.2 * ScreenSize().getWidth(context),
                        decoration: const BoxDecoration(
                          color: OwnColors.white,
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        )),
                    HorizontalSpace().create(context, 0.01),

                    Stack(
                      children: [
                        Container(
                          width: 0.88.sw,
                          child: Row(
                            children: [

                              SizedBox(
                                  width: 0.043 .sh,
                                  height: 0.043 .sh,
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 0.0,left: 0,right: 0),
                                    child: Center(child:SizedBox(height: 0.03.sh,child: Image.asset(Res.icon_report))),
                                  )),
                              VerticalSpace().create(context, 0.01),
                              Text(
                                S().report,
                                style: GoogleFonts.inter(
                                  fontSize: 16.nsp,
                                  color: OwnColors.CFE3040,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Positioned.fill(child: InkWell(
                          splashColor: OwnColors.transparent,
                          onTap: () {
                            print("report");
                            backClick();
                            PushNewScreen().normalPush(context,ReportPage(type: 1,) );

                          },
                        ))
                      ],
                    ),

                    HorizontalSpace().create(context, 0.02),
                    Stack(
                      children: [
                        Container(
                          width: 0.88.sw,
                          child: Row(
                            children: [

                              SizedBox(
                                  width: 0.043 .sh,
                                  height: 0.043 .sh,
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 0.0,left: 0,right: 0),
                                    child: Center(child:SizedBox(height: 0.03.sh,child: Image.asset(Res.icon_block))),
                                  )),
                              VerticalSpace().create(context, 0.01),
                              Text(
                                S().block,
                                style: GoogleFonts.inter(
                                  fontSize: 16.nsp,
                                  color: OwnColors.CFE3040,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Positioned.fill(child: InkWell(
                          splashColor: OwnColors.transparent,
                          onTap: () {
                            print("block");
                            sendBlockWarning();
                            // backClick();
                            // PushNewScreen().normalPush(context,ReportPage(type: 1,) );

                          },
                        ))
                      ],
                    ),

                    HorizontalSpace().create(context, 0.04),

                  ],
                ),
              )),
        )
    ).whenComplete(() {});


  }


  void sendBlockWarning(){
    WarningDialog.showIOSAlertDialog2(context, S().block_title, S().block_hint, S().confirm, sendBlock);
  }
  void sendBlock( ) async {


    Loading.show(context);
    ResultData resultData = await AppApi.getInstance().setUserBlock(context, true,  widget.send_to_id);
    await getBlockList();
    Loading.dismiss(context);
    backClick();
    backClick();

  }
  void getBlockList( ) async {
    print("getBlockList");


    ResultData resultData = await AppApi.getInstance()
        .getUserBlockList(context, true);

    print(resultData.data.toString());
    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      GDefine.blockUserId.clear();
      for (Map<String, dynamic> a in resultData.data) {
        if(!GDefine.blockUserId.contains(a ["block_user_id"].toString())) {
          GDefine.blockUserId.add(a ["block_user_id"].toString());
        }
      }

    }
  }


  Future<void> goPersonal() async {

    GuildClass guildclass = await GlobalTask().getGuild(context, widget.send_to_id);
    if(guildclass!=null) {
      PushNewScreen().normalPush(context,
          GuilderPage(guildClass: guildclass, preview: false,));
    }else {
      PushNewScreen().normalPush(context,
          GuilderPage(id: widget.send_to_id, preview: false,));
    }
    // PushNewScreen().normalPush(context, GuilderPage(id: widget.send_to_id,preview: false,));
  }



}
class MyBehavior extends ScrollBehavior {
  @override
  Widget buildOverscrollIndicator(
      BuildContext context, Widget child, ScrollableDetails details) {
    return child;
  }
}