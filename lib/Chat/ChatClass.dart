class ChatClass {
  String now_id;
  String sender_id;
  String sender_name;
  String sender_avatar;
  String t;
  String time;
  String timeStamp;
  String message;
  String send_to_fcm;
  int new_mes_cnt;


  ChatClass({this.now_id, this.sender_id, this.sender_name, this.sender_avatar
    , this.t, this.time, this.timeStamp,this.new_mes_cnt,this.message,this.send_to_fcm});
// Map<String, dynamic> toMap() {
//   return {
//     'chat_id': chat_id,
//     'from_user': from_user,
//     'from_user_name': from_user_name,
//     'from_user_avatar': from_user_avatar,
//     'to_user': to_user,
//     'to_user_name': to_user_name,
//     'to_user_avatar': to_user_avatar,
//     'messages': messages,
//     'updated_at': updated_at,
//
//   };
// }
//
// factory ChatModel.fromJson(Map<String, dynamic> json) => ChatModel(
//   chat_id: json["id"],
//   from_user: json["from_user"].toString(),
//   from_user_name: json["from_user_name"].toString(),
//   from_user_avatar: json["from_user_avatar"].toString(),
//   to_user: json["to_user"].toString(),
//   to_user_name: json["to_user_name"].toString(),
//   to_user_avatar: json["to_user_avatar"].toString(),
//   messages: json["messages"].toString(),
//   updated_at: json["updated_at"].toString(),
//
// );

}