import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/TopBar.dart';
import '../res.dart';
import '../tool/Loading.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_star/flutter_star.dart';

class GuildReviewAddCommentSctionAlert extends StatefulWidget {
  GuildClass guildClass;
  Function closeAlert;
  Function closeAlertAndCommentEdit;
  Function setAuthComment;
  bool authPassed;
  //constructor
  GuildReviewAddCommentSctionAlert(this.closeAlert,
      this.closeAlertAndCommentEdit, this.setAuthComment, this.authPassed);

  @override
  State<StatefulWidget> createState() => _GuildReviewAddCommentSctionAlert();
}

class _GuildReviewAddCommentSctionAlert
    extends State<GuildReviewAddCommentSctionAlert> {
  bool _showInfo = false;
  String inviteCode = "";

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Center(
            child: Stack(children: [
      Container(
        width: ScreenSize().getWidth(context) * 1.0,
        height: ScreenSize().getHeight(context) * 1.0,
        color: Color.fromRGBO(0, 0, 0, 0.43),
      ),
      /*
                          Container(
                            alignment: Alignment(0.8,0),
                            child:Container(
                                width: ScreenSize().getWidth(context) * 0.4,
                                height: ScreenSize().getWidth(context) * 0.1,
                                color: Color.fromRGBO(238, 238, 238, 1),
                                child:Align(
                                    alignment: Alignment(0.1,0),
                                    child:Text("test")
                                )
                            ),
                          ),
                          */
      //填碼表單
      Positioned(
        top: ScreenSize().getHeight(context) * 0.4,
        left: ScreenSize().getWidth(context) * 0.2,
        child: Container(
          width: ScreenSize().getWidth(context) * 0.6,
          //height: ScreenSize().getHeight(context) * 0.2,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(15)),
          ),
          child: Column(children: [
            HorizontalSpace().create(context, 0.01),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AutoSizeText("請輸入評價邀請碼 ",
                    minFontSize: 10,
                    maxFontSize: 30,
                    style: TextStyle(
                      color: OwnColors.black,
                      fontSize: 16,
                    )),
                SizedBox(
                    height: 15.0,
                    width: 15.0,
                    child: IconButton(
                      color: Colors.amberAccent,
                      onPressed: () {
                        setState(() {
                          if (!_showInfo) {
                            _showInfo = true;
                          } else {
                            _showInfo = false;
                          }
                        });
                      },
                      padding: EdgeInsets.all(0.0),
                      icon: Image.asset("images/orange_question.png"),
                    ))
              ],
            ),
            Container(
              margin: EdgeInsets.all(10),
              height: 35,
              decoration: BoxDecoration(
                color: Color.fromRGBO(247, 247, 247, 1),
                //borderRadius: BorderRadius.all(Radius.circular(10)),
                //border: Border.all(width: 1.0, color: Colors.white),
              ),
              child: Padding(
                padding: EdgeInsets.all(0),
                child: TextField(
                  style: TextStyle(
                      fontSize: 15.0, height: 1.0, color: Colors.black),
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelStyle: TextStyle(
                        fontSize: 10,
                      )),
                  onChanged: (text) {
                    inviteCode = text;
                  },
                ),
              ),
            ),
            HorizontalSpace().create(context, 0.005),
            Container(
              decoration: const BoxDecoration(
                  border: Border(
                top: BorderSide(
                  //                    <--- top side
                  color: Color.fromRGBO(200, 200, 200, 1),
                  width: 1.0,
                ),
              )),
              child: Row(
                children: [
                  Container(
                    width: ScreenSize().getWidth(context) * 0.3,
                    decoration: const BoxDecoration(
                        border: Border(
                      right: BorderSide(
                        //                    <--- top side
                        color: Color.fromRGBO(200, 200, 200, 1),
                        width: 1.0,
                      ),
                    )),
                    child: TextButton(
                        onPressed: () {
                          
                          widget.closeAlertAndCommentEdit();
                        },
                        child: AutoSizeText(S().cancel,
                            minFontSize: 10,
                            maxFontSize: 30,
                            style: TextStyle(
                              color: Color.fromRGBO(0, 148, 255, 1),
                              fontSize: 15,
                            ))),
                  ),
                  Container(
                    width: ScreenSize().getWidth(context) * 0.3,
                    child: TextButton(
                        onPressed: () {
                          widget.closeAlert();
                          widget.setAuthComment(
                              inviteCode, widget.closeAlertAndCommentEdit);
                        },
                        child: AutoSizeText("完成",
                            minFontSize: 10,
                            maxFontSize: 30,
                            style: TextStyle(
                              color: Color.fromRGBO(0, 148, 255, 1),
                              fontSize: 15,
                            ))),
                  )
                ],
              ),
            )
          ]),
        ),
      ),
      //提示
      if (_showInfo)
        Positioned(
            top: ScreenSize().getHeight(context) * 0.305,
            left: ScreenSize().getWidth(context) * 0.42,
            child: Column(children: [
              Container(
                width: ScreenSize().getWidth(context) * 0.55,
                height: ScreenSize().getHeight(context) * 0.1,
                padding: EdgeInsets.fromLTRB(11, 13, 11, 0),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(238, 238, 238, 1),
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                ),
                child: Text("你必須擁有旅遊達人的評價邀請碼才能對他留下評價"),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(
                    ScreenSize().getWidth(context) * 0.03, 0, 0, 0),
                child: Image.asset(
                  "images/comment_info_arrow.png",
                  height: ScreenSize().getHeight(context) * 0.02,
                ),
              ),
            ])),
    ])));
  }
}
