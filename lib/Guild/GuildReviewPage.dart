import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/GlobalTask.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Guild/GuildReviewAddComment.dart';
import 'package:ctour/Guild/GuildReviewEditComment.dart';
import 'package:ctour/Guild/GuilderPage.dart';
import 'package:ctour/ImgPage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/ReportPage.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/StatusTools.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/TopBar.dart';
import '../res.dart';
import '../tool/Loading.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_star/flutter_star.dart';

class GuildReviewPage extends StatefulWidget {
  final GuildClass guildClass;

  const GuildReviewPage({
    Key key,
    this.guildClass,
  }) : super(key: key);

  @override
  State createState() {
    return _GuildReviewPageState();
  }
}

class CommentItem {
  String user_id;
  String id;
  String name;
  double score;
  String update_time;
  String created_at;
  String comment;
  String avatar;
  int is_public;
  List<String> imageList;

  CommentItem(
      {this.user_id,
      this.name,
      this.score,
      this.update_time,
      this.created_at,
      this.comment,
      this.avatar,
      this.is_public,
      this.imageList});

  factory CommentItem.fromJson(Map<String, dynamic> json) {
    List<String> imageList = [];
    for (var imgURL in json["img"]) {
      if (imgURL.toString().startsWith("http")) {
        imageList.add(imgURL);
      } else {}
    }

    return CommentItem(
      user_id: json["user_id"].toString(),
      name: json["name"].toString(),
      score: double.parse(json["score"]),
      update_time: json["created_at"].toString(),
      created_at: json["created_at"].toString(),
      comment: json["comment"].toString(),
      is_public: int.parse(json["is_public"].toString()),
      avatar: json["avatar"].toString(),
      imageList: imageList,
    );
  }
}

class FormData {
  bool anonymous;
  String comment;
  num stars;
  List<String> picURL;

  FormData(this.anonymous, this.stars, this.comment, this.picURL);
}

class _GuildReviewPageState extends State<GuildReviewPage> {
  bool _showAddCommentSection = false;
  bool _showAddCommentSectionAlert = false;
  bool _showOpacity = false;
  bool authPassed = false;
  FormData formData;
  GuildClass guildClass;


  void _closeAddCommentSection() {
    setState(() {
      print("closeAddcomment");
      _showAddCommentSection = false;
    });
  }

  void _closeAlert() {
    print("close:_closeAlert");
    setState(() {
      _showAddCommentSectionAlert = false;
    });
  }

  void _closeAlertAndCommentSection() {
    print("_closeAlertAndCommentSection");
    setState(() {
      _showAddCommentSectionAlert = false;
      _showAddCommentSection = false;
    });
  }

  void toggleOpacity() {
    print("toggle");
    // setState(() {
    //   if(_showOpacity == true){
    //     _showOpacity = false;
    //   }else{
    //     _showOpacity = true;
    //   }
    //
    // });
  }

  void _openAddCommentSection() {
    if (widget.guildClass.id == SharePre.prefs.getString(GDefine.user_id)) {
      WarningDialog.showIOSAlertDialog(
          context, S().remind, S().personnal_guilder_myself_hint, S().confirm);
    } else {
      showCupertinoModalBottomSheet(
        barrierColor:Colors.black54,
        topRadius: Radius.circular(30),
        expand: false,
        context: context,
        enableDrag: false,
        backgroundColor: Colors.transparent,
        builder: (context) => GuildReviewAddComment(
          guildClass: widget.guildClass,
        ),
      ).whenComplete(() {
        refreshController.callRefresh();

        getComment(widget.guildClass);
        getGuildScore();
      });
    }
    //toggleOpacity();
    // showCupertinoModalBottomSheet(
    //     topRadius: Radius.circular(30),
    //     expand: false,
    //     context: context,
    //     backgroundColor: Colors.transparent,
    //     enableDrag: false,
    //     builder: (context) => Stack(
    //           children: [
    //             GuildReviewAddCommentSction().create(
    //                 context,
    //                 _closeAddCommentSection,
    //                 setFormData,
    //                 true,
    //                 setAuthComment,
    //                 authPassed,
    //                 _showAddCommentSection,
    //                 toggleOpacity,
    //                 ),
    //           ],
    //         )).whenComplete(() {});
    // setState(() {
    //   _showAddCommentSectionAlert = true;
    // });
  }

  @override
  void initState() {
    StatusTools().setTranBGDarkText();
    print(widget.guildClass.score);
    //
    refreshController = EasyRefreshController(
      controlFinishRefresh: true,
      controlFinishLoad: true,
    );
    guildScore = widget.guildClass.score == "null"
        ? "0.0"
        : widget.guildClass.score.toString();

    FocusManager.instance.primaryFocus?.unfocus();
    //getComment(widget.guildClass);
    super.initState();
  }

  @override
  void dispose() {
    StatusTools().setTranBGWhiteText();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void setComment(
      bool anonymous, int score, String comment, List imgPath) async {
    //var imgPath = ["https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg"];
    int is_public = 0;
    if (!anonymous) {
      is_public = 1;
    }
    ResultData resultData = await AppApi.getInstance().setComment(
        context,
        true,
        SharePre.prefs.getString(GDefine.user_id),
        widget.guildClass.id,
        is_public,
        score.toString(),
        comment,
        imgPath);
    if (resultData.isSuccess()) {
      if (Navigator.canPop(context)) {
        Navigator.pop(context);
      } else {
        SystemNavigator.pop();
      }
      getComment(widget.guildClass);
    } else {
      WarningDialog.showIOSAlertDialog(context, S().upload_fail_title, S().upload_fail_hint, S().confirm);
    }
  }

  String guildScore = "";

  Future<void> getGuildScore() async {
    ResultData resultData = await AppApi.getInstance().getTourist(context, true,
        SharePre.prefs.getString(GDefine.user_id), widget.guildClass.id);
    if (resultData.data['score'] == null) {
      resultData.data['score'] = "0.0";

    }else {
      widget.guildClass.score = double.parse(resultData.data['score'].toString()).toStringAsFixed(1);
      widget.guildClass.comment_num  = resultData.data['comment_num'].toString();
      print(" resultData.data resultData.data " +  resultData.data.toString());


    }


    setState(() {});
  }

  void getComment(GuildClass guildClass) async {
    ResultData resultData = await AppApi.getInstance().getTouristCommentList(
        context,
        true,
        SharePre.prefs.getString(GDefine.user_id),
        guildClass.id);
    if (resultData.isSuccess()) {
      commentItemList.clear();
      List itemList = [];
      for (Map<String, dynamic> a in resultData.data) {
        CommentItem c = CommentItem.fromJson(a);
        itemList.add(c);
      }

      setState(() {
        commentItemList = itemList.reversed.toList();
      });
    } else {
      // WarningDialog.showIOSAlertDialog(context, "操作失敗", resultData.msg, "確認");
    }
    refreshController.finishRefresh();
    refreshController.resetFooter();
  }

  var commentItemList = [
    //CommentItem("10", "劉德華", 4.2, "2022/5/10", "從來不知道桃園有那麼", "https://guide.easytravel.com.tw/images/city/taichung.jpg", ["https://image.cache.storm.mg/styles/smg-800x533-fp/s3/media/image/2018/10/08/20181008-034735_U13451_M460725_203e.png?itok=ghV8jlkK"])
  ];

  void setFormData(FormData data) {
    int score = data.stars.toInt();
    setComment(data.anonymous, score, data.comment, data.picURL);
    formData = data;
  }

  EasyRefreshController refreshController = new EasyRefreshController();
  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
    //_GuildReviewPageState().getComment(guildClass);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: OwnColors.white,
        body: FocusDetector(
          onFocusGained: () {
              refreshController.callRefresh();
              getComment(widget.guildClass);
              // getGuildScore();
            },
          child: Padding(
            padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
            child: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                  },
                  child: SingleChildScrollView(
                    child: Container(
                      width: ScreenSize().getWidth(context) * 1.0,
                      height: MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment:
                            CrossAxisAlignment.center,
                        children: [
                          Topbar().starCreate(context, widget.guildClass.score,widget.guildClass.comment_num,  backClick, _openAddCommentSection),
                          // GuildReviewTopBar().create(
                          //     context,
                          //     _openAddCommentSection,
                          //     widget.guildClass,
                          //     guildScore,
                          //     toggleOpacity),
                          //評論區
                          Expanded(
                            child: EasyRefresh.builder(
                              controller: refreshController,

                              header: ClassicHeader(
                                  dragText: '',
                                  armedText: '',
                                  readyText: '',
                                  processingText: '',
                                  processedText: '',
                                  noMoreText: '',
                                  failedText: '',
                                  messageText: '',
                                  iconTheme: IconThemeData(
                                      color:
                                          OwnColors.CTourOrange)),
                              footer: NotLoadFooter(),
                              onRefresh: () async {
                                print("onRefresh");
                                getComment(widget.guildClass);
                              },
                              onLoad: () async {
                                print("onLoad");
                                refreshController.finishLoad(IndicatorResult.noMore);

                              },
                              childBuilder: (context, physics) {
                                return ListView.builder(
                                    physics: physics,
                                    shrinkWrap: false,
                                    padding: const EdgeInsets.only(top:8,right: 0,bottom: 0,left: 0),
                                    itemCount:
                                        commentItemList.length,
                                    scrollDirection: Axis.vertical,
                                    itemBuilder: (context, index) {
                                      return
                                        index == commentItemList.length - 1?
                                        Column(
                                          children: [
                                            commentItem(
                                                commentItemList[index]),
                                            Container(height: MediaQuery.of(context).padding.bottom,)
                                          ],
                                        )
                                            :
                                        commentItem(
                                          commentItemList[index]);
                                    });

                                //
                                // for (var item in commentItemList)
                                // GuildReviewComment()
                                //     .create(context, item);
                              },
                            ),
                          ),

                        ],
                      ),
                    ),
                  )),
          ),
        ));
  }

  Widget commentItem(CommentItem item) {
    String setText(String text) {
      DateTime tempDate = new DateFormat("yyyy-MM-dd").parse(text);
      String result =
          DateFormat('yyyy' + S().year + 'MM' + S().month + 'dd' + S().day)
              .format(tempDate);
      return result == "null" ? "" : result;
    }

    print(item.imageList);
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onLongPress: (){
        showMenu(item);
      },
      child: Column(children: [
        HorizontalSpace().create(context, 0.02),
        Container(
            //height: ScreenSize().getHeight(context) * 0.26,
            width: ScreenSize().getWidth(context) * 0.91,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black26, width: 1.0, style: BorderStyle.solid),
              borderRadius: BorderRadius.all(Radius.circular(15)),
            ),
            child: Column(
              children: [
                HorizontalSpace().create(context, 0.013),
                GestureDetector(
                  onTap: () async {
                    if(item.is_public==1) {
                      GuildClass guildclass = await GlobalTask().getGuild(context,item.user_id);
                      if(guildclass!=null) {
                        PushNewScreen().normalPush(context,
                            GuilderPage(guildClass: guildclass, preview: false,));
                      }else {
                        PushNewScreen().normalPush(context,
                            GuilderPage(id:item.user_id, preview: false,));
                      }

                      // PushNewScreen().normalPush(context, GuilderPage(
                      //   id: item.user_id, preview: false,));
                    }
                  },
                  child: Row(
                    children: [
                      VerticalSpace().create(context, 0.025),
                      Visibility(
                        visible: item.avatar.length > 10 && item.is_public == 1,
                        child: Container(
                          width: 40,
                          height: 40,

                          child: AspectRatio(
                            aspectRatio: 1,
                            child: CachedNetworkImage(
                                errorWidget:
                                    (context, url, error) =>
                                    Icon(
                                      Icons.error,
                                      color: Colors.red,
                                    ),
                                imageUrl: item.avatar,
                                fit: BoxFit.fitHeight,
                                imageBuilder:
                                    (context, imageProvider) =>
                                    Container(
                                      // width: 80.0,
                                      // height: 80.0,
                                      decoration: BoxDecoration(
                                        color: OwnColors.tran,
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            image: imageProvider,
                                            fit: BoxFit.cover),
                                      ),
                                    ),
                                placeholder: (context, url) => AspectRatio(
                                  aspectRatio: 1,
                                  child: FractionallySizedBox(
                                    heightFactor: 0.5,
                                    widthFactor: 0.5,
                                    child: SpinKitRing(
                                      lineWidth:5,
                                      color:
                                      OwnColors.tran ,
                                    ),
                                  ),
                                )
                            ),
                          )
                          ,
                        ),
                      ),
                      Visibility(
                        visible: item.avatar.length < 10 || item.is_public == 0,
                        child: Container(
                          width: 40.0,
                          height: 40.0,
                          child: Image.asset(Res.choose_img),
                        ),
                      ),
                      VerticalSpace().create(context, 0.03),
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Flexible(
                                        child: AutoSizeText(
                                            item.is_public == 1
                                                ? item.name
                                                : S().anonymous ,
                                            minFontSize: 20,
                                            maxFontSize: 30,

                                            style: GoogleFonts.inter(
                                              color: OwnColors.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.w700,
                                            )),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 6),
                                      ),
                                      RatingBarIndicator(
                                        rating: item.score,
                                        itemBuilder: (context, index) => Icon(
                                          Icons.star,
                                          color: OwnColors.CTourOrange,
                                        ),
                                        itemCount: 5,
                                        itemSize: 20.0,
                                        direction: Axis.horizontal,
                                      ),


                                    ],
                                  ),
                                ),
                                GestureDetector(onTap:(){
                                  showMenu(item);
                                },child: Container(width:0.04.sw,height: 0.03.sh,child: Image.asset(Res.three_dots))),
                                VerticalSpace().create(context, 0.051)

                              ],
                            ),
                            AutoSizeText(setText(item.update_time).split(" ")[0],
                                minFontSize: 10,
                                maxFontSize: 30,
                                style: GoogleFonts.inter(
                                  color: Color.fromRGBO(152, 152, 152, 1),
                                  fontWeight: FontWeight.w300,
                                  fontSize: 12,
                                )),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  alignment: AlignmentDirectional.centerStart,
                  margin: const EdgeInsets.only(left: 15.0, right: 20.0, top: 10.0, bottom: 10.0),
                  child: Text(
                      item.comment,
                      style: GoogleFonts.inter(
                          fontSize: 16.nsp,
                        fontWeight: FontWeight.w500
                      )
                  ),
                ),
                item.imageList.length == 1
                    ? GestureDetector(
                  onTap: (){
                    PushNewScreen().normalPush(context, ImgPage(url:item.imageList[0] ,));
                  },
                      child: Container(
                          height: 0.2 * ScreenSize().getHeight(context) +
                              MediaQuery.of(context).padding.top +
                              MediaQuery.of(context).padding.bottom,
                          width: 0.8 * ScreenSize().getWidth(context),
                          child: CachedNetworkImage(
                            imageUrl: item.imageList[0],
                            fit: BoxFit.fill,
                            placeholder: (context, url) => AspectRatio(
                              aspectRatio: 1,
                              child: FractionallySizedBox(
                                heightFactor: 0.5,
                                widthFactor: 0.5,
                                child: SpinKitRing(
                                  lineWidth: 5,
                                  color: OwnColors.tran,
                                ),
                              ),
                            ),
                            errorWidget: (context, url, error) => Icon(
                              Icons.error,
                              color: OwnColors.black.withOpacity(0.5),
                            ),
                            imageBuilder: (context, imageProvider) => Container(
                              width: ScreenSize().getWidth(context) * 0.8,
                              decoration: BoxDecoration(
                                color: OwnColors.tran,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15.0)),
                                image: DecorationImage(
                                    image: imageProvider, fit: BoxFit.cover),
                              ),
                            ),
                            // width: MediaQuery.of(context).size.width,
                          ),
                        ),
                    )
                    : Container(
                        width: 1,
                      ),
                Visibility(
                    visible: item.imageList.length>0,
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    margin: const EdgeInsets.only(
                        left: 20.0, right: 20.0, top: 10.0, bottom: 10.0),
                    child: VerticalSpace().create(context, 0.03),
                  )
                ),
              ],
            )),
      ]),
    );
  }

  Future<void> showMenu (CommentItem travelCommentClass) async {
    Tools().dismissK(context);
    int result = await showCupertinoModalBottomSheet(
        barrierColor:Colors.black54,
        topRadius: Radius.circular(30),
        expand: false,
        context: context,
        enableDrag: true,
        backgroundColor: Colors.transparent,
        builder: (context) => GestureDetector(
          onTap:(){
            Tools().dismissK(context);
            print("onTap");
          },
          child: Material(
              color: OwnColors.white,
              child: Container(
                // height:  ScreenSize().getHeightWTop(context),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[

                    Container(
                        height: 0.007 * ScreenSize().getHeight(context),
                        width: 0.2 * ScreenSize().getWidth(context),
                        decoration: const BoxDecoration(
                          color: OwnColors.white,
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        )),
                    HorizontalSpace().create(context, 0.01),
                    Visibility(
                      visible: travelCommentClass.user_id == SharePre.prefs.getString(GDefine.user_id),
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Container(
                                width: 0.88.sw,
                                child: Row(
                                  children: [
                                    SizedBox(
                                        width: 0.043 .sh,
                                        height:
                                        0.043.sh,
                                        child: Center(
                                          child: Padding(
                                              padding: const EdgeInsets.only(top:0.0, left: 0.0,right: 0.0),
                                              child: Image.asset(Res.highlighter,height: 0.03.sh,)
                                            // Icon(SFSymbols., color: OwnColors.CTourOrange,size:  0.03.sh,)
                                            // Image.asset(Res.black_heart):
                                            // Image.asset(Res.collected_heart)
                                          ),
                                        )),
                                    VerticalSpace().create(context, 0.01),
                                    Text(
                                      S().edit,
                                      style: GoogleFonts.inter(
                                        fontSize: 16.nsp,
                                        color: OwnColors.black,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              new Positioned.fill(child: InkWell(
                                splashColor: OwnColors.transparent,
                                onTap: (){
                                  goEditComment(travelCommentClass);


                                },
                              ))
                            ],
                          ),
                          HorizontalSpace().create(context, 0.02),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: travelCommentClass.user_id == SharePre.prefs.getString(GDefine.user_id),
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Container(
                                width: 0.88.sw,
                                child: Row(
                                  children: [
                                    SizedBox(
                                        width: 0.043 .sh,
                                        height:
                                        0.043.sh,
                                        child: Center(
                                          child: Padding(
                                              padding: const EdgeInsets.only(top:0.0, left: 0.0,right: 0.0),
                                              child: Image.asset(Res.orange_trash,height: 0.03.sh,)
                                            // Icon(SFSymbols., color: OwnColors.CTourOrange,size:  0.03.sh,)
                                            // Image.asset(Res.black_heart):
                                            // Image.asset(Res.collected_heart)
                                          ),
                                        )),
                                    VerticalSpace().create(context, 0.01),
                                    Text(
                                      S().delete,
                                      style: GoogleFonts.inter(
                                        fontSize: 16.nsp,
                                        color: OwnColors.CFE3040,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              new Positioned.fill(child: InkWell(
                                splashColor: OwnColors.transparent,
                                onTap: (){

                                  goDelComment(travelCommentClass);

                                },
                              ))
                            ],
                          ),
                          HorizontalSpace().create(context, 0.02),
                        ],
                      ),
                    ),



                    Stack(
                      children: [
                        Container(
                          width: 0.88.sw,
                          child: Row(
                            children: [

                              SizedBox(
                                  width: 0.043 .sh,
                                  height: 0.043 .sh,
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 0.0,left: 0,right: 0),
                                    child: Center(child:SizedBox(height: 0.03.sh,child: Image.asset(Res.icon_report))),
                                  )),
                              VerticalSpace().create(context, 0.01),
                              Text(
                                S().report,
                                style: GoogleFonts.inter(
                                  fontSize: 16.nsp,
                                  color: OwnColors.CFE3040,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Positioned.fill(child: InkWell(
                          splashColor: OwnColors.transparent,
                          onTap: () {
                            print("report");
                            backClick();
                            PushNewScreen().normalPush(context,ReportPage(type: 4,) );

                          },
                        ))
                      ],
                    ),

                    HorizontalSpace().create(context, 0.04),

                  ],
                ),
              )),
        )
    ).whenComplete(() {});


  }

  void goEditComment(CommentItem commentItem){
    backClick();
    showCupertinoModalBottomSheet(
      barrierColor:Colors.black54,
      topRadius: Radius.circular(30),
      expand: false,
      context: context,
      enableDrag: false,
      backgroundColor: Colors.transparent,
      builder: (context) => GuildReviewEditComment(
        guildClass: widget.guildClass,commentItem: commentItem,
      ),
    ).whenComplete(() {
      refreshController.callRefresh();

      getComment(widget.guildClass);
      getGuildScore();
    });

  }

  Future<void> goDelComment(CommentItem commentItem) async {

    backClick();
    Tools().dismissK(context);

    Loading.show(context);



    ResultData resultData = await AppApi.getInstance().delTouristComment(
        context, true,widget.guildClass.id, commentItem.created_at,
    );


    Loading.dismiss(context);
    if (resultData.isSuccess()) {

      getGuildScore();

      getComment(widget.guildClass);


    } else {
      WarningDialog.showIOSAlertDialog(
          context, S().delete_fail_title, S().delete_fail_hint, S().confirm);
    }


  }


}


