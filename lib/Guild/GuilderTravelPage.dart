import 'dart:async';
import 'dart:convert';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/TravelCard.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Guild/GuildReviewPage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/Travel/TravelPage.dart';
import 'package:ctour/tool/DateTools.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import '../Travel/TravelClass.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:shimmer/shimmer.dart';
import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/TopBar.dart';
import '../res.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_star/flutter_star.dart';

class GuilderTravelPage extends StatefulWidget {
  final GuildClass guildClass;


  const GuilderTravelPage({
    Key key,
    this.guildClass,

  }) : super(key: key);

  @override
  State createState() {
    return _GuilderTravelPageState();
  }
}

class _GuilderTravelPageState extends State<GuilderTravelPage> {


  List<TravelClass> hotTravelList = new List();
  List<TravelClass> showTravelList = new List();
  TextEditingController searchEdit= TextEditingController();
  String searchText1 = "";

  bool loadingBool = true;
  EasyRefreshController refreshController = new EasyRefreshController( ) ;
  FocusNode searchFocus = new FocusNode();
  @override
  void initState() {
    super.initState();
    // if (Platform.isAndroid) {
    SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(
        statusBarColor: OwnColors.tran, //设置为透明
        statusBarIconBrightness: Brightness.dark);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
    refreshController = EasyRefreshController(
      controlFinishRefresh: true,
      controlFinishLoad: true,
    );

    // }
    // hotTravelList.add(new TravelClass(
    //     id: "",
    //     cover_image:
    //     "https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg",
    //     title: "台東成功無敵海景露營，喝著咖啡聽著海聲",
    //     date: "2022年06月17日",
    //     duration: "三天",
    //     views: "100",
    //     comment_num: "100",
    //     guilder_name: "Amber",
    //     guilder_avatar:
    //     "https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg",
    //     guilder_comment: "18",
    //     guilder_rate: "4.7"));


    Future.delayed(Duration(milliseconds: 100), () {
      // 5s over, navigate to a new page
      refreshController.callRefresh();
    });
    getGuilderArticleAPI();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.white,
        body: Column(
          children: [
            ScreenSize().createTopPad(context),
            HorizontalSpace().create(context, 0.01),
            Container(
              height: ScreenSize().getHeight(context) * 0.05,
              child: Topbar().titleCreate(context,widget.guildClass.name, ScreenSize().getHeight(context) * 0.073,  backClick ,false),
            ),
            HorizontalSpace().create(context, 0.02),
            Container(
              height: 0.05 * ScreenSize().getHeight(context),
              width: ScreenSize().getWidth(context) * 0.88,
              // height: height,
              decoration: BoxDecoration(
                color: OwnColors.white,
                border: Border.all(color: OwnColors.CC8C8C8),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30)),
              ),

              alignment: Alignment.center,
              child: Row(
                children: [
                  GestureDetector(
                    onTap: (){
                      FocusScope.of(context).requestFocus(searchFocus);
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(left: 12.0, bottom: 4.0),
                      child: Container(
                          height: 0.027 * ScreenSize().getHeight(context),
                          child: Icon(SFSymbols.search, color: OwnColors.black)),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: 0.027 * ScreenSize().getHeight(context),
                      child: TextField(
                        controller: searchEdit,
                        focusNode: searchFocus,
                        onChanged: (text) {
                          print("onChanged");
                          if (searchText1 == "") {
                            searchText1 = text;
                            searchTravel();
                          } else {
                            if (searchText1 != text) {
                              searchText1 = text;
                              searchTravel();
                            }
                          }
                        },
                        autocorrect: false,
                        decoration: InputDecoration(
                            fillColor: OwnColors.tran,
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 0),
                            //Change this value to custom as you like
                            // isDense: true,
                            hintText: S().guilder_travel_search_hint,
                            filled: true,
                            hintStyle: TextStyle(
                                fontSize: 16.nsp, color:OwnColors.C989898),
                            border: OutlineInputBorder(
                                borderRadius:
                                new BorderRadius.circular(20.0),
                                borderSide: BorderSide.none)),
                        style: TextStyle(
                            color: Colors.black.withOpacity(1),
                            fontSize: ScreenUtil().setSp(16)),
                        maxLines: 1,
                        keyboardType: TextInputType.text,
                        obscureText: false,
                      ),
                    ),
                  ),

                ],
              ),
            ),
            HorizontalSpace().create(context, 0.015),
            Expanded(
              child: Container(
                width: 0.88 * ScreenSize().getWidth(context),
                child:  EasyRefresh.builder(

                    controller: refreshController,
                    // header: ListenerHeader(
                    //   triggerOffset: 100,
                    //   // listenable: _listenable,
                    //   safeArea: false,
                    // ),
                    header: ClassicHeader(
                        dragText: '',
                        armedText: '',
                        readyText: '',
                        processingText: '',
                        processedText: '',
                        noMoreText: '',
                        failedText: '',
                        messageText: '',
                        iconTheme: IconThemeData(
                            color: OwnColors.CTourOrange
                        )

                    ),
                    footer: NotLoadFooter(),

                    onRefresh: () async {
                      print("onRefresh");

                      getGuilderArticleAPI();
                    },
                    onLoad: () async {
                      print("onLoad");
                      refreshController.finishLoad(IndicatorResult.noMore);

                    },

                    childBuilder: (context, physics) {
                    return ListView.builder(
                        physics:physics,
                        shrinkWrap: false,
                        padding: EdgeInsets.zero,
                        itemCount: showTravelList.length,
                        scrollDirection: Axis.vertical,
                        itemBuilder: (context, index) {
                          return 
                            index == showTravelList.length -1 ?
                            Column(
                              children: [
                                TravelCard().recommendCard(context,
                                    showTravelList[index], index,travelClick,0.88 * ScreenSize().getWidth(context),),
                                ScreenSize().createBottomPad(context)
                              ],
                            )
                                :
                                
                            TravelCard().recommendCard(context,
                              showTravelList[index], index,travelClick,0.88 * ScreenSize().getWidth(context),);
                        });
                  }
                ),
              ),
            )
          ],
        ));
  }

  // Widget recommendCard(TravelClass travelClass, int index) {
  //   return GestureDetector(
  //     onTap: () => {travelClick(travelClass)},
  //     child: Container(
  //       width: 0.9 * ScreenSize().getWidth(context),
  //
  //
  //       child: Container(
  //         decoration: BoxDecoration(
  //           color: OwnColors.white,
  //           borderRadius: BorderRadius.only(
  //             topRight: Radius.circular(15),
  //             topLeft: Radius.circular(15),
  //             bottomLeft: Radius.circular(15),
  //             bottomRight: Radius.circular(15),
  //           ),
  //         ),
  //         child: Column(
  //           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //           crossAxisAlignment: CrossAxisAlignment.center,
  //           children: [
  //             HorizontalSpace().create(context, 0.015),
  //             AspectRatio(
  //               aspectRatio: 16/9,
  //               child: Container(
  //                 width: 0.9 * ScreenSize().getWidth(context),
  //                 child: Stack(
  //                   children: [
  //                     CachedNetworkImage(
  //                       imageUrl: travelClass.cover_image,
  //                       fit: BoxFit.fill,
  //                       placeholder: (context, url) => AspectRatio(
  //                         aspectRatio: 1,
  //                         child: FractionallySizedBox(
  //                           heightFactor: 0.5,
  //                           widthFactor: 0.5,
  //                           child: SpinKitRing(
  //                             lineWidth:5,
  //                             color:
  //                             OwnColors.tran ,
  //                           ),
  //                         ),
  //                       ),
  //                       errorWidget: (context, url, error) => Icon(
  //                         Icons.error,
  //                         color: OwnColors.black.withOpacity(0.5),
  //                       ),
  //                       imageBuilder: (context, imageProvider) =>
  //                           Container(
  //                             width: 0.9 * ScreenSize().getWidth(context),
  //                             decoration: BoxDecoration(
  //                               color: OwnColors.black,
  //                               borderRadius: BorderRadius.all(
  //                                   Radius.circular(15.0)),
  //                               image: DecorationImage(
  //                                   image: imageProvider,
  //                                   fit: BoxFit.cover),
  //                             ),
  //                           ),
  //                       // width: MediaQuery.of(context).size.width,
  //                     ),
  //                     // Positioned(
  //                     //   right: 10,
  //                     //   top: 10,
  //                     //   child: Container(
  //                     //       height: 0.03 *
  //                     //           ScreenSize().getHeight(context),
  //                     //       child: Image.asset(Res.favorite)),
  //                     // ),
  //                     Positioned(
  //                       bottom: 0,
  //                       // right: 5,
  //                       child: Container(
  //                         height: 0.1 *
  //                             ScreenSize().getHeight(context),
  //                         width: 0.88 *
  //                             ScreenSize().getWidth(context),
  //                         decoration: BoxDecoration(
  //                             borderRadius: BorderRadius.only(
  //                                 bottomLeft: Radius.circular(15),
  //                                 bottomRight: Radius.circular(15)),
  //                             gradient: LinearGradient(
  //                               begin: Alignment.topCenter,
  //                               end: Alignment.bottomCenter,
  //                               colors: [
  //                                 OwnColors.C797979
  //                                     .withOpacity(0.0),
  //                                 OwnColors.C787878
  //                                     .withOpacity(0.0073),
  //                                 OwnColors.C3E3E3E
  //                                     .withOpacity(0.3391),
  //                                 Colors.black.withOpacity(0.7),
  //                               ],
  //                             )),
  //                         child: Center(
  //                           child: Container(
  //                             width: 0.83 *
  //                                 ScreenSize().getWidth(context),
  //                             child: Column(
  //                               mainAxisAlignment:
  //                               MainAxisAlignment.start,
  //                               crossAxisAlignment:
  //                               CrossAxisAlignment.start,
  //                               children: [
  //                                 Spacer(
  //                                   flex: 10,
  //                                 ),
  //                                 AutoSizeText(
  //                                   travelClass.type == "1"?
  //                                   travelClass.duration.toString() + S().home_filter_type2_string :
  //                                   travelClass.type == "2"? S().home_filter_type3 :
  //                                   S(). home_filter_type4,
  //                                   minFontSize: 12,
  //                                   overflow: TextOverflow.ellipsis,
  //                                   maxLines: 1,
  //                                   style: TextStyle(
  //                                       fontSize: 12.nsp,
  //                                       color: OwnColors.white),
  //                                 ),
  //                                 AutoSizeText(
  //                                   travelClass.title,
  //                                   minFontSize: 16,
  //                                   overflow: TextOverflow.ellipsis,
  //                                   maxLines: 1,
  //                                   style: TextStyle(
  //                                       fontSize: 18.nsp,
  //                                       color: OwnColors.white),
  //                                 ),
  //                                 HorizontalSpace()
  //                                     .create(context, 0.008),
  //                               ],
  //                             ),
  //                           ),
  //                         ),
  //                       ),
  //                     ),
  //
  //                     Container(
  //                       height: 0.1 *
  //                           ScreenSize().getHeight(context),
  //                       decoration: BoxDecoration(
  //                           borderRadius: BorderRadius.only(
  //                               topLeft: Radius.circular(15),
  //                               topRight: Radius.circular(15)),
  //                           gradient: LinearGradient(
  //                             begin: Alignment.bottomCenter ,
  //                             end: Alignment.topCenter,
  //                             colors: [
  //                               OwnColors.C797979
  //                                   .withOpacity(0.0),
  //                               OwnColors.C787878
  //                                   .withOpacity(0.0073),
  //                               OwnColors.C3E3E3E
  //                                   .withOpacity(0.3391),
  //                               Colors.black.withOpacity(0.7),
  //                             ],
  //                           )),
  //                       child: Padding(
  //                         padding: const EdgeInsets.only(top:5.0,left: 10),
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.start,
  //                           crossAxisAlignment: CrossAxisAlignment.start,
  //                           children: [
  //                             SizedBox(
  //                               height: 0.045 *  ScreenSize().getHeight(context),
  //                               child: AspectRatio(
  //                                 aspectRatio: 1,
  //                                 child: CachedNetworkImage(
  //                                   placeholder: (context, url) => AspectRatio(
  //                                     aspectRatio: 1,
  //                                     child: FractionallySizedBox(
  //                                       heightFactor: 0.5,
  //                                       widthFactor: 0.5,
  //                                       child: SpinKitRing(
  //                                         lineWidth:5,
  //                                         color:
  //                                         OwnColors.tran ,
  //                                       ),
  //                                     ),
  //                                   ),
  //                                   errorWidget:
  //                                       (context, url, error) =>
  //                                       Icon(
  //                                         Icons.error,
  //                                         color: Colors.red,
  //                                       ),
  //                                   imageUrl: travelClass.guilder_avatar,
  //                                   fit: BoxFit.fitHeight,
  //                                   imageBuilder: (context,
  //                                       imageProvider) =>
  //                                       Container(
  //                                         // width: 80.0,
  //                                         // height: 80.0,
  //                                         decoration: BoxDecoration(
  //                                           shape: BoxShape.circle,
  //                                           border: Border.all(width: 1,color: OwnColors.CC8C8C8
  //
  //                                           ),
  //                                           image: DecorationImage(
  //                                               image: imageProvider,
  //                                               fit: BoxFit.cover),
  //                                         ),
  //                                       ),
  //                                   // width: MediaQuery.of(context).size.width,
  //                                   // placeholder: (context, url) => new CircularProgressIndicator(),
  //                                   // placeholder: new CircularProgressIndicator(),
  //                                 ),
  //                               ),
  //                             ),
  //
  //                             VerticalSpace().create(context, 0.017),
  //                             Container(
  //                               child: Column(
  //                                 mainAxisAlignment: MainAxisAlignment.start,
  //                                 crossAxisAlignment: CrossAxisAlignment.start,
  //                                 mainAxisSize: MainAxisSize.min,
  //                                 children: [
  //                                   Text(travelClass.guilder_name,
  //                                     maxLines: 1,
  //
  //                                     overflow: TextOverflow.ellipsis,
  //                                     style: TextStyle(
  //                                         fontSize: 16.nsp,
  //                                         color: OwnColors.white
  //                                     ),),
  //                                   Row(
  //                                     mainAxisAlignment: MainAxisAlignment.end,
  //                                     crossAxisAlignment: CrossAxisAlignment.end,
  //                                     children: [
  //                                       Container(
  //                                         height: 0.02 * ScreenSize().getHeight(context),
  //                                         child: Image.asset(Res.star),
  //                                       ),
  //                                       Container(width: 2,),
  //                                       Text(
  //                                         travelClass.guilder_rate  ,
  //
  //                                         style: TextStyle(
  //                                             fontSize: 12.nsp,
  //                                             color: OwnColors.white,
  //                                             fontWeight: FontWeight.bold
  //                                         ),
  //                                       ),
  //                                       Text(
  //                                         " (" + double.parse(travelClass.guilder_comment).toStringAsFixed(0)+S().home_rate_number+")",
  //
  //                                         style: TextStyle(
  //                                           fontSize: 12.nsp,
  //                                           color: OwnColors.white,
  //
  //                                         ),
  //                                       ),
  //                                     ],
  //                                   ),
  //                                 ],
  //                               ),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //             ),
  //             HorizontalSpace().create(context, 0.01),
  //             Container(
  //               width: 0.83 * ScreenSize().getWidth(context),
  //               height: 0.025 * ScreenSize().getHeight(context),
  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.start,
  //                 crossAxisAlignment: CrossAxisAlignment.center,
  //                 children: [
  //
  //                   Padding(
  //                     padding: const EdgeInsets.only(
  //                         top: 2.0, bottom: 2.0, right: 4),
  //                     child: Image.asset(Res.peoples),
  //                   ),
  //                   AutoSizeText(
  //                     travelClass.views + " " + S().travel_views,
  //                     minFontSize: 10,
  //                     style:GoogleFonts.inter(
  //                       fontSize: 10.nsp,
  //                       color: OwnColors.black,
  //                       fontWeight: FontWeight.w500,
  //                     ),
  //                   ),
  //                   Padding(
  //                     padding: const EdgeInsets.only(
  //                         top: 2.0, bottom: 2.0, right: 4, left: 8),
  //                     child: Image.asset(Res.chat),
  //                   ),
  //                   Expanded(
  //                     child: AutoSizeText(
  //                       travelClass.comment_num +
  //                           " " +
  //                           S().travel_comment,
  //                       minFontSize: 10,
  //                       style:GoogleFonts.inter(
  //                         fontSize: 10.nsp,
  //                         color: OwnColors.black,
  //                         fontWeight: FontWeight.w500,
  //                       ),
  //                     ),
  //                   ),
  //                   AutoSizeText(
  //                     DateTools().intlYMD( travelClass.date),
  //                     minFontSize: 10,
  //                     style: TextStyle(
  //                       fontSize: 12.nsp,
  //                       color: OwnColors.CCCCCCC,
  //                     ),
  //                   )
  //                 ],
  //               ),
  //             ),
  //
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }


  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }
  void travelClick(TravelClass travelClass){

    PushNewScreen().normalPush(context, TravelPage(travelClass:travelClass,edit: false));
  }

  void getGuilderArticleAPI( ) async {
    print("getGuilderArticleAPI");

    ResultData resultData = await AppApi.getInstance().getArticleList(context, true,
        SharePre.prefs.getString(GDefine.user_id),
        widget.guildClass.id,"","","","","","","","false");
    if (resultData.isSuccess()) {
      print(resultData.data.toString());
      hotTravelList.clear();
      for (Map<String, dynamic> a in resultData.data) {
        TravelClass travelClass = TravelClass.fromJson(a);
        travelClass.sectionTag = new List();
        print("name : " + TravelClass.fromJson(a).title.toString());
        print("contentList : " + TravelClass.fromJson(a).contentList.toString());
        print("contentList : " + a["content_list"].toString());
        print("DDDDDD" + jsonDecode(a["content_list"].toString()).toString());
        if(a["content_list"] != null) {
          List<dynamic> content_list = jsonDecode(a["content_list"].toString());

          travelClass.contentListDynamic = new List();
          List<TravelSubClass> subList = new List();
          for (Map<String, dynamic> sub in content_list) {
            TravelSubClass travelSubClass = new TravelSubClass();
            List<dynamic> content_list2 = sub["contentList"];
            List<TravelContentClass> travelContentClassList = new List();
            // if(content_list2.length>0) {
            Map<String, dynamic> dy = new Map();
            dy["title"] =
            sub["title"].toString() == "null" ? "" : sub["title"].toString();
            dy["type"] = "0";
            dy["description"] = "";
            dy["google"] = "";
            dy["image"] = [];
            travelClass.contentListDynamic.add(dy);
            for (Map<String, dynamic> subsub in content_list2) {
              TravelContentClass travelContentClass = new TravelContentClass();
              travelContentClass.image = List<String>.from(subsub["image"]);
              travelContentClass.image.removeWhere((element) =>
              element == "" || element == "null");
              travelContentClass.title = subsub["title"];
              travelContentClass.description =
              subsub["description"].toString() == "null"
                  ? ""
                  : subsub["description"].toString();
              travelContentClass.id = subsub["id"].toString();
              travelContentClass.google =
              subsub["google"].toString() == "null" ? "" : subsub["google"]
                  .toString();
              travelContentClass.key = new GlobalKey();

              travelContentClassList.add(travelContentClass);

              Map<String,dynamic> dy2 = new Map();
              dy2["title"] = travelContentClass.title;
              dy2["type"] = "1";
              dy2["description"] = travelContentClass.description;
              dy2["google"] = travelContentClass.google;
              dy2["image"] = List<String>.from(subsub["image"]);

              travelClass.contentListDynamic.add(dy2);

            }
            travelSubClass.title =
            sub["title"].toString() == "null" ? "" : sub["title"].toString();
            travelSubClass.id = sub["id"].toString();
            travelSubClass.key = new GlobalKey();
            if(sub["title"]!="" && sub["title"].toString() != "null") {
              travelClass.sectionTag.add(new SectionClass(title: sub["title"], key:travelSubClass.key  ));
            }

            if (travelContentClassList.isNotEmpty) {
              travelSubClass.content = travelContentClassList;
            }
            subList.add(travelSubClass);
          }

          travelClass.contentList = subList;
          hotTravelList.add(travelClass);
        }


      }
      showTravelList.clear();
      showTravelList.addAll(hotTravelList);


      setState(() {
        print("getTouristAPI");


      });
      print (refreshController.controlFinishLoad);
      refreshController.finishRefresh();
      refreshController.resetFooter();

    } else {
      WarningDialog.showIOSAlertDialog(context, S().download_fail_title,  S().download_fail_hint,S().confirm);
    }
  }

  Future<void> searchTravel() async {
    print("searchTravel");

    print("searchEdit.text" + searchEdit.text);
    showTravelList.clear();


    if(searchEdit.text == ""){
      showTravelList.addAll(hotTravelList);

    }else {
      List<TravelClass> temp = hotTravelList.where((element) =>
      element.title.contains(searchEdit.text) ||
          element.guilder_name.contains(searchEdit.text) ||
          element.tag.indexWhere((cc) => cc.toString().contains(searchEdit.text)) != -1).toList();
      showTravelList.addAll(temp);

    }

    setState(() {

    });


  }



}
