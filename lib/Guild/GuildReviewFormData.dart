class FormData {
  bool anonymous;
  String comment;
  num stars;
  List<String> picURL;
  FormData(this.anonymous, this.stars, this.comment, this.picURL);
}