import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/OrangeXCross.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Guild/GuildReviewPage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/Personal/BecomeGuilderPage.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/CropSample.dart';
import 'package:ctour/tool/CropTool.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_star/custom_rating.dart';
import 'package:flutter_star/flutter_star.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_button/group_button.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:math' as math; // import this

import 'package:intl/intl.dart';

import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

class GuildReviewEditComment extends StatefulWidget {

  final GuildClass guildClass;
  final CommentItem commentItem;

  const GuildReviewEditComment({
    Key key,
    this.guildClass,
    this.commentItem,
  }) : super(key: key);



  @override
  _GuildReviewEditCommentState createState() => _GuildReviewEditCommentState();
}

class _GuildReviewEditCommentState extends State<GuildReviewEditComment> {

  List<String> trafficList = [S().guild_traffic1,S().guild_traffic2, S().guild_traffic3];
  List<String> genderList = [S().guild_gender1,S().guild_gender2, S().guild_gender3];
  double rating = 5;

  bool annoyBool = false;
  TextEditingController contentEdit = new TextEditingController();
  TextEditingController inviteEdit = new TextEditingController();
  String imgUrl = "";



  @override
  Future<void> initState() {
    super.initState();


    contentEdit.text = widget.commentItem.comment;
    if(widget.commentItem.imageList.length >0) {
      imgUrl = widget.commentItem.imageList[0];
    }

    annoyBool = widget.commentItem.is_public != 1;

    print(widget.guildClass.invite_code);
    Future.delayed(Duration(milliseconds: 100), () {
      rating = widget.commentItem.score;
      setState(() {

      });
      if(widget.guildClass.invite_code.length>0 && widget.guildClass.invite_code.toString() != "null"){
        WarningDialog.showTextFieldInviteCodeDialog(context, S().guilder_add_comment_invite_title,
            "", S().guilder_add_comment_invite_content_hint, inviteEdit, widget.guildClass.invite_code,setState,setAuthComment);
      }else{

      }
    });

  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap:(){
        Tools().dismissK(context);
        print("onTap");
      },
      child: Material(
          color: OwnColors.white,
          child: Column(
            children: <Widget>[

              Container(
                  height: 0.007 * ScreenSize().getHeight(context),
                  width: 0.2 * ScreenSize().getWidth(context),
                  decoration: const BoxDecoration(
                    color: OwnColors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  )),
              HorizontalSpace().create(context, 0.02),
              Topbar().saveModalCreate(context, checkIfSave,  saveClick),

              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      HorizontalSpace().create(context, 0.02),
                      Container(
                        // height: 0.023 * ScreenSize().getHeight(context),
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              S().guilder_add_comment_annoy,
                              style: GoogleFonts.inter(
                                  fontWeight: FontWeight.w700,
                                  color: OwnColors.black,
                                  fontSize: 16.nsp),
                            ),
                            Transform.scale(
                              scale: 0.65,
                              child: CupertinoSwitch(
                                value: annoyBool,
                                activeColor: OwnColors.CFF644E,
                                onChanged: (bool value) {
                                  setState(() {
                                    annoyBool = value;

                                  });
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      HorizontalSpace().create(context, 0.02),
                      Container(
                        // height: 0.023 * ScreenSize().getHeight(context),
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: AutoSizeText(
                          S().guild_rate,
                          textScaleFactor: 1.0,
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.w700,
                              fontSize: 16.sp,
                              color: OwnColors.black),
                        ),
                      ),
                      HorizontalSpace().create(context, 0.015),
                      Container(
                        height: 0.043.sh,
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: Row(
                          children: <Widget>[
                            RatingBar.builder(
                            initialRating: 5,
                              minRating: 0,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                            rateValue:rating,
                            glow:false,
                              itemSize: 0.043.sh,
                              itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: OwnColors.CTourOrange,
                              ),
                              onRatingUpdate: (r) {
                                print(r);
                                rating = r;
                              },
                            ),
                          ],
                        ),
                      ),
                      HorizontalSpace().create(context, 0.035),

                      Container(
                        // height: 0.023 * ScreenSize().getHeight(context),
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: AutoSizeText(
                          S().guilder_add_comment_content,
                          textScaleFactor: 1.0,
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.sp,
                              color: OwnColors.black),
                        ),
                      ),
                      HorizontalSpace().create(context, 0.015),

                      Container(
                        height: 0.392.sh,
                        width: 0.892.sw,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          border:
                          Border.all(color: OwnColors.C989898, width: 1),
                        ),
                        child: TextField(
                            controller: contentEdit,
                            style: TextStyle(
                                fontSize: 16.nsp, color: OwnColors.black),
                            onChanged: (S) {
                              setState(() {});
                            },
                            maxLength: 300,
                            maxLines: null,
                            decoration: InputDecoration(
                                hintText:
                                S().guilder_add_comment_content_hint,
                                fillColor: OwnColors.white,
                                counterStyle:
                                GoogleFonts.inter(color: Colors.transparent),
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 0),

                                //Change this value to custom as you like
                                isDense: true,
                                filled: true,
                                counterText: "",
                                border: OutlineInputBorder(
                                    borderRadius:
                                    new BorderRadius.circular(20.0),
                                    borderSide: BorderSide.none))),
                      ),
                      HorizontalSpace().create(context, 0.005),
                      Container(
                        width: 0.9.sw,
                        child: Row(
                          children: [
                            Spacer(),
                            Text(
                              contentEdit.text.length.toString() + "/300",
                              style: GoogleFonts.inter(
                                  fontSize: 16.nsp, color: OwnColors.C989898),
                            )
                          ],
                        ),
                      ),
                      HorizontalSpace().create(context, 0.007),
                      Container(
                        // height: 0.023 * ScreenSize().getHeight(context),
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: AutoSizeText(
                          S().guilder_add_comment_pic,
                          textScaleFactor: 1.0,
                          style: GoogleFonts.inter(
                              fontWeight: FontWeight.w700,
                              fontSize: 16.sp,
                              color: OwnColors.black),
                        ),
                      ),
                      HorizontalSpace().create(context, 0.015),
                      Container(
                        width: 0.92 * ScreenSize().getWidth(context),
                        child: AspectRatio(
                          aspectRatio: 4/3,
                          child: imgUrl.contains("http")
                              ? Stack(
                                children: [
                                  GestureDetector(
                            onTap: () {
                                  showBottomMenu();
                            },
                            child: Padding(
                              padding:  EdgeInsets.only(top:0.01.sh,right: 0.01.sh),
                              child: CachedNetworkImage(
                                    imageUrl: imgUrl,
                                    fit: BoxFit.fill,
                                    placeholder: (context, url) => AspectRatio(
                                      aspectRatio: 1,
                                      child: FractionallySizedBox(
                                        heightFactor: 0.5,
                                        widthFactor: 0.5,
                                        child: SpinKitRing(
                                          lineWidth:5,
                                          color:
                                          OwnColors.tran ,
                                        ),
                                      ),
                                    ),
                                    errorWidget: (context, url, error) => Icon(
                                      Icons.error,
                                      color: OwnColors.black.withOpacity(0.5),
                                    ),
                                    imageBuilder: (context, imageProvider) =>
                                        Container(
                                          // width:0.92 * ScreenSize().getWidth(context),
                                          // height: 0.92 * ScreenSize().getWidth(context),
                                          decoration: BoxDecoration(
                                            color: OwnColors.tran,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(14)),
                                            image: DecorationImage(
                                                image: imageProvider,
                                                fit: BoxFit.cover),
                                          ),
                                        ),
                                    // width: MediaQuery.of(context).size.width,
                              ),
                            ),
                          ),
                                  Positioned(
                                    top:0,
                                    right: 0,
                                    child: GestureDetector(
                                      onTap: (){
                                        imgUrl = "";
                                        setState(() {

                                        });
                                      },
                                      child: OrangeXCross().create(context),
                                    ),
                                  )

                                ],
                              )
                              : Container(
                            // width:0.9 * ScreenSize().getWidth(context),
                            // height: 0.9 * ScreenSize().getWidth(context),
                              decoration: BoxDecoration(
                                color: OwnColors.CF7F7F7,
                                borderRadius:
                                BorderRadius.all(Radius.circular(14)),
                              ),
                              child: GestureDetector(
                                child:Center(
                                  child: Stack(
                                      alignment: Alignment.topLeft,
                                      children:[
                                        Transform(
                                          alignment: Alignment.center,
                                          transform: Matrix4.rotationY(math.pi),
                                          child: Padding(
                                            padding: const EdgeInsets.only(right: 6),
                                            child: Icon(SFSymbols.photo_fill, color: OwnColors.CE6E1E1,size: ScreenSize().getWidth(context)*0.2,),
                                          ),
                                        ),
                                        Container(
                                          height: 0.10 * ScreenSize().getWidth(context),
                                          width: 0.10 * ScreenSize().getWidth(context),
                                          child: Padding(
                                            padding: const EdgeInsets.only(top:5.0,right: 20),
                                            child:Stack(
                                              children: [
                                                Image.asset(Res.add_fill),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ]
                                  ),
                                ),
                                onTap: () {
                                  print("onTap");
                                  showBottomMenu();
                                },
                              )

                          ),
                        ),
                      ),
                      HorizontalSpace().create(context, 0.03),
                      ScreenSize().createBottomPad(context)
                    ],
                  ),
                ),
              ),





            ],
          ))
    );
  }

  void checkIfSave(){
    WarningDialog.showIOSAlertDialog3(context, S().save_not_yet, S().save_not_yet_hint2, S().cancel, S().personal_edit_save,backClick, saveClick);
  }

  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
    //_GuildReviewPageState().getComment(guildClass);
  }

  void showBottomMenu() {
    Tools().dismissK(context);
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 150,
              child: Column(children: <Widget>[
                ListTile(
                  leading: Icon(Icons.photo_camera),
                  title: Text(S().take_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromCamera();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.photo_library),
                  title: Text(S().choose_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromPhoto();
                  },
                )
              ]));
        });
  }

  final picker = ImagePicker();

  void pcikImageFromCamera( ) async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);

    File img = File(pickedFile.path);
    Uint8List imageBytes = await pickedFile.readAsBytes();

    CroppedFile  croppedFile = await CropTool().crop(4, 3, CropAspectRatioPreset.ratio4x3, false, img.path);

    if(croppedFile != null) {
      imageBytes = await croppedFile.readAsBytes();

      String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + base64Image);
      _uploadImage(base64Image);
    }


  }

  void pcikImageFromPhoto() async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);
    File img = File(pickedFile.path);
    Uint8List imageBytes = await pickedFile.readAsBytes();

    CroppedFile  croppedFile = await CropTool().crop(4, 3, CropAspectRatioPreset.ratio4x3, false, img.path);

    if(croppedFile != null) {
      imageBytes = await croppedFile.readAsBytes();

      String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + base64Image);
      _uploadImage(base64Image);
    }
  }

  // void pcikImageFromPhoto() async {
  //   PickedFile pickedFile = await picker.getImage(
  //       source: ImageSource.gallery,
  //       maxHeight: 1440,
  //       maxWidth: 1440,
  //       imageQuality: 80);
  //   File img = File(pickedFile.path);
  //   List<int> imageBytes = await img.readAsBytes();
  //   /*
  //   avatar = img.path;
  //   avatarFileName = avatar
  //       .split("/")
  //       .last;
  //   */
  //   String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
  //   print("base64Image: " + base64Image);
  //   _uploadImage(base64Image);
  //
  //   setState(() {});
  // }

  Future<void> _uploadImage(String data) async {
    // Loading.show(context);

    ResultData resultData =
    await AppApi.getInstance().setFileBase64(context, true, data);

    if (resultData.isSuccess()) {
      // Loading.dismiss(context);
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data);
      String img_url = GDefine.imageUrl + resultData.data["path"];
      print("img_url " + img_url);
      imgUrl = img_url;
      setState(() {});
    } else {
      // Loading.dismiss(context);
      WarningDialog.showIOSAlertDialog(context, S().upload_fail_title, S().upload_fail_hint,S().confirm);
    }
  }

   void setRating(num) {
    rating = num;
  }




  void closeBtn() {
    Navigator.of(context).pop();
  }

  void saveClick(){
    setComment();
    // if( Tools().stringNotNullOrSpace(contentEdit.text )) {
    //   setComment();
    // }else {
    //   WarningDialog.showIOSAlertDialog(context, S().guilder_add_comment_no_content_title, S().guilder_add_comment_no_content_hint,S().confirm);
    // }
    // Navigator.of(context).pop();
  }

  @override
  void dispose() {
    print("dispose");

    super.dispose();
  }

  @override
  void deactivate() {
    print("deactivate");

    super.deactivate();
  }



  void setComment(
       ) async {
    Loading.show(context);
    int is_public = 0;
    if (!annoyBool) {
      is_public = 1;
    }
    ResultData resultData = await AppApi.getInstance().editTouristComment(
        context,
        true,
        SharePre.prefs.getString(GDefine.user_id),
        widget.guildClass.id,
        is_public,
        rating.toStringAsFixed(1) ,
        contentEdit.text,
        [imgUrl],
    widget.commentItem.created_at);
    Loading.dismiss(context);
    if (resultData.isSuccess()) {

      if (Navigator.canPop(context)) {
        Navigator.pop(context,"update");
        // Navigator.pop(context);
      } else {
        SystemNavigator.pop();
      }

    } else {
      WarningDialog.showIOSAlertDialog(context, S().upload_fail_title, S().upload_fail_hint,S().confirm);
    }
  }

  void setAuthComment( ) async {
    Loading.show(context);
    ResultData resultData = await AppApi.getInstance().setAuthComment(
        context,
        true,
        SharePre.prefs.getString(GDefine.user_id),
        widget.guildClass.id,
        inviteEdit.text);
    Loading.dismiss(context);
    if (resultData.isSuccess()) {
      //widget.closeAlert();

      backClick();
    } else {
      print("here" + resultData.data);
      if (!resultData.data.toString().contains("已存在")) {
        WarningDialog.showIOSAlertDialog(context, S().guilder_add_comment_invite_fail_title, S().guilder_add_comment_invite_fail_hint
            , S().confirm);
      } else {
        backClick();
      }

      //widget.closeAlertAndCommentEdit();
    }
  }




}