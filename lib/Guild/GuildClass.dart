class GuildClass {
  String id;
  String name;
  String avatar;
  String gender;
  String tel;
  String birthday;
  String score;
  String comment_num;
  List<dynamic> trans;
  List<dynamic> location;
  List<dynamic> tourist_img;

  String is_collect;
  String description;
  String invite_code;
  String want_companion;
  List<dynamic> lang;

  GuildClass({this.id,this.gender, this.name, this.trans, this.location, this.tourist_img, this.avatar,
    this.is_collect,this.invite_code, this.lang,this.comment_num,this.description,this.score,this.want_companion});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'gender': gender,
      'score': score,
      'comment_num': comment_num,
      'trans': trans,
      'location': location,
      'avatar': avatar,
      'tourist_img': tourist_img,
      'is_collect': is_collect,
      'description': description,
      'invite_code': invite_code,
      'lang': lang,
      'want_companion': want_companion,


    };
  }

  factory GuildClass.fromJson(Map<String, dynamic> json) => GuildClass(
    id: json["id"].toString(),
    name: json["name"].toString(),
    gender: json["gender"].toString(),
    score: double.parse(json["score"].toString()).toStringAsFixed(1),
    comment_num: json["comment_num"].toString(),
    trans: json["trans"] ,
    location: json["location"] ,
    tourist_img: json["tourist_img"] ,
    avatar: json["avatar"],
    is_collect: json["is_collect"].toString(),
    description: json["description"].toString(),
    invite_code: json["invite_code"].toString(),
    lang: json["lang"] ,
    want_companion: json["want_companion"].toString(),

  );
}