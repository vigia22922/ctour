import 'dart:async';
import 'dart:io';


import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/GuildCard.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Define/GlobalData.dart';
import 'package:ctour/Firebase/FirebaseRealtime.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Guild/GuildFilterPage.dart';
import 'package:ctour/Guild/GuilderPage.dart';
import 'package:ctour/Home/HomePage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/DateTools.dart';
import 'package:ctour/tool/FilterData.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/StatusTools.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:extended_wrap/extended_wrap.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:permission_handler/permission_handler.dart';

import 'dart:convert';
import 'package:dio/dio.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shimmer/shimmer.dart';



class GuildPage extends StatefulWidget {
  @override
  State createState() {
    return _GuildPageState();
  }
}

class _GuildPageState extends State with AutomaticKeepAliveClientMixin, TickerProviderStateMixin{

  StreamSubscription<String> fireBaseDBSubScription;

  String searchText1 = "";
  TextEditingController searchEdit;
  double x = 0;

  List<GuildClass> bestTourList = new List();
  List<GuildClass> tourList = new List();
  List<GuildClass> topList = new List();
  List<GuildClass> bottomList = new List();
  List<GuildClass> searchList = new List();
  SwiperController swiperControl;
  double currentIndexPage = 0;

  bool loadingBest = true;
  bool loadingTotal = true;

  bool loadingBool   = true;
  bool filter = false;
  EasyRefreshController refreshController = new EasyRefreshController( ) ;
  @override
  bool get wantKeepAlive => true;
  FocusNode searchFocus = new FocusNode();
  List<String > rankImg = [Res.num1,Res.num2,Res.num3,Res.num4,Res.num5,Res.num6,Res.num7,Res.num8,Res.num9,Res.num10,];



  AnimationController searchAnimationController,
      spacerAnimationController,
      logoAnimationController,
      logoSizeAnimationController;
  AnimationController _TextAnimationController;
  Animation _searchTween, _spacerTween;

  Animation<Offset> offset;
  Animation<double> sizeOffset,searchOffset,heightSpaceOffset,heightOffset;
  Animation _curve1, _curve2, _curve3, _curve4;

  bool showSearch = true;

  int commentMax =5;

  double per = 0.05;
  int pageCnt = 1;
  int totalCnt = 10;
  int cntPerPage = 10;

  int pageCntSearch = 1;
  bool   searchLoading = false;
  @override
  void initState() {
    SharePre.prefs.setString(GDefine.guilderFilter, "false");
    SharePre.prefs.setString(GDefine.articleFilter, "false");
    refreshController = EasyRefreshController(
      controlFinishRefresh: true,
      controlFinishLoad: true,
    );

    searchEdit = TextEditingController();
    swiperControl = SwiperController();
    currentIndexPage =0;

    Future.delayed(Duration(milliseconds: 100), () {
      // 5s over, navigate to a new page
      refreshController.callRefresh();
    });

    print("init");
    super.initState();

    if(SharePre.prefs.getInt(GDefine.commentMax )!= null){
      commentMax = SharePre.prefs.getInt(GDefine.commentMax );
      if(FilterData.guild_endNum == -1){
        FilterData.guild_endNum = double.parse(commentMax.toString());
      }
    }


    searchAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    _curve1 = CurvedAnimation(
        parent: searchAnimationController, curve: Curves.fastOutSlowIn);
    // _searchTween = Tween(begin: 0.0, end: 1.0).animate(_curve1);
    searchOffset = Tween<double>(begin: 0, end: 1)        .animate(_curve1);

    double logoEndWidth = ( per.sh-0.017.sh)/140 * 450;

    spacerAnimationController =
        AnimationController(vsync: this, duration: Duration(seconds: 0));
    // _curve2 = CurvedAnimation(parent: spacerAnimationController, curve: Curves.fastOutSlowIn);
    _spacerTween =
        Tween(begin: 0.0, end: 1.0).animate(spacerAnimationController);

    logoAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    _curve3 = CurvedAnimation(
        parent: logoAnimationController, curve: Curves.fastOutSlowIn);
    offset = Tween<Offset>(begin: Offset(0.0, 0.0), end: Offset(
        - (0.44.sw - logoEndWidth/2) /logoEndWidth
        , 0))
        .animate(_curve3);

    logoSizeAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    _curve4 = CurvedAnimation(
        parent: logoSizeAnimationController, curve: Curves.fastOutSlowIn);
    // sizeOffset = Tween<double>(begin: 0.065.sh / 140 * 450, end: 0.88.sw / 3)
    //     .animate(_curve4);
    sizeOffset = Tween<double>(begin: per.sh / 140 * 450, end: logoEndWidth)
        .animate(_curve4);

    heightOffset = Tween<double>(begin: (per+ 0.017). sh, end: per.sh-0.017.sh)
        .animate(_curve4);
    heightSpaceOffset = Tween<double>(begin: 0.0  , end:  0.017.sh)
        .animate(_curve4);

  }



  @override
  void dispose() {

    super.dispose();

    StatusTools().setWhitBGDarkText();
  }

  @override
  void didChangeDependencies() {
    print(" ScreenSize().getWidth(context)  " +  ScreenSize().getWidth(context).toString());
    print(" ScreenSize().getHeight(context)  " +  ScreenSize().getHeight(context).toString());
    // x = (1.5*ScreenSize().getWidth(context)-0.35*ScreenSize().getHeight(context))/2 - 0.08*ScreenSize().getWidth(context);
  }



  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Tools().dismissK(context);
      },
      child: Scaffold(
        backgroundColor: OwnColors.white,
        body:SafeArea(
          child: FocusDetector(
            onFocusGained: (){

              if(GlobalData.refreshGuild) {
                pageCnt  = 1;
                getTouristAPI(false);
                getWeekTouristAPI();
                getScoreCount();
              }else {
                GlobalData.refreshGuild = true;
                getScoreCount();

              }
            },
            child: Center(
              child: Container(
                height: ScreenSize().getHeight(context)*0.91,
                width: ScreenSize().getWidth(context)*1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [




                    Expanded(child:
                    Stack(
                      children: [
                        Column(
                          children: [
                            AnimatedBuilder(
                              animation: logoSizeAnimationController,
                              builder: (context, child) => Container(
                                height:
                                (per+0.017).sh ,
                              ),
                            ),

                            AnimatedBuilder(
                              animation: spacerAnimationController,
                              builder: (context, child) => Container(
                                height:
                                0.07 *
                                    (1 - spacerAnimationController.value) *
                                    ScreenSize().getHeight(context)  ,
                              ),
                            ),
                            Expanded(
                              child:
                              searchEdit.text.length > 0 && searchList.length == 0 && !searchLoading ?
                              SingleChildScrollView(
                                keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    HorizontalSpace().create(context, 0.2),
                                    Container(
                                      height: 0.118.sh,
                                      child: Image.asset(Res.not_found),
                                    ),
                                    HorizontalSpace().create(context, 0.01),
                                    Text(
                                      S().search_not_found_title,
                                      style: GoogleFonts.inter(
                                          fontSize: 16.nsp,
                                          fontWeight: FontWeight.w600,
                                          color: OwnColors.black
                                      ),
                                    ),
                                    HorizontalSpace().create(context, 0.01),
                                    Text(
                                      S().search_not_found_hint,
                                      style: GoogleFonts.inter(
                                          fontSize: 12.nsp,
                                          fontWeight: FontWeight.w500,
                                          color: OwnColors.C989898
                                      ),
                                    ),
                                    HorizontalSpace().create(context, 0.1),
                                    HorizontalSpace().create(context, 0.1),
                                    HorizontalSpace().create(context, 0.1),




                                  ],
                                ),
                              )
                                  :
                              EasyRefresh.builder(
                                // header: ListenerHeader(
                                //   triggerOffset: 100,
                                //   // listenable: _listenable,
                                //   safeArea: false,
                                // ),
                                controller: refreshController,
                                  header: ClassicHeader(
                                      dragText: '',
                                      armedText: '',
                                      readyText: '',
                                      processingText: '',
                                      processedText: '',
                                      noMoreText: '',
                                      failedText: '',
                                      messageText: '',
                                      iconTheme: IconThemeData(
                                          color: OwnColors.CTourOrange
                                      )

                                  ),
                                  footer: ClassicFooter(
                                      // position: IndicatorPosition.locator,
                                      dragText: '',
                                      armedText: '',
                                      readyText: '',
                                      processingText: '',
                                      processedText: '',
                                      noMoreText: '',
                                      failedText: '',
                                      messageText: '',
                                      noMoreIcon: Container(
                                        width: 0,
                                        height: 0,
                                      ),

                                      iconTheme: IconThemeData(
                                          color: OwnColors.CTourOrange
                                      )
                                  ),

                                  onRefresh: () async {
                                    print("onRefresh");
                                    if(searchEdit.text == "") {
                                      pageCnt = 1;
                                      getTouristAPI(false);
                                      getWeekTouristAPI();
                                      getScoreCount();
                                    }else {
                                      refreshController.finishRefresh();
                                      refreshController.resetFooter();
                                    }



                                  },
                                  onLoad: () async {
                                    print("onLoad");
                                    print("totalCnt " + totalCnt.toString());

                                    if(searchEdit.text == "") {
                                      if(pageCnt * cntPerPage <totalCnt){
                                        pageCnt = pageCnt+1;
                                        if(SharePre.prefs.getString(GDefine.guilderFilter) == "true"){
                                          getTouristAPI(true);
                                        }else {
                                          getTouristAPI(true);
                                        }
                                        refreshController.finishLoad();
                                      }else {
                                        refreshController.finishLoad(IndicatorResult.noMore);
                                      }
                                    }else {

                                      if(pageCntSearch * cntPerPage <totalCnt){
                                        pageCntSearch = pageCntSearch+1;
                                        if(SharePre.prefs.getString(GDefine.guilderFilter) == "true"){
                                          getTouristSearchAPI(true);
                                        }else {
                                          getTouristSearchAPI(true);
                                        }
                                        refreshController.finishLoad();
                                      }else {
                                        refreshController.finishLoad(IndicatorResult.noMore);
                                      }
                                    }


                                  },


                                  childBuilder: (context, physics) {
                                    return  NotificationListener<
                                        ScrollNotification>(
                                      onNotification: (scrollNotification) {
                                        // print("position " +
                                        //     scrollNotification.metrics.pixels
                                        //         .toString());
                                        // print("position " +
                                        //     scrollNotification.metrics.axisDirection
                                        //         .toString());
                                        // print("searchOffset " +
                                        //     searchOffset.value
                                        //         .toString());
                                        // Tools().dismissK(context);


                                        if((
                                            scrollNotification.metrics.axisDirection == AxisDirection.down ||
                                                scrollNotification.metrics.axisDirection == AxisDirection.up)) {

                                          spacerAnimationController.animateTo(
                                              scrollNotification.metrics.pixels /
                                                  100);
                                          if (scrollNotification.metrics
                                              .pixels >
                                              100) {
                                            // searchAnimationController.animateTo((
                                            //     scrollNotification.metrics
                                            //         .pixels-100)  / 100);
                                            if (searchAnimationController
                                                .value !=
                                                1 &&
                                                !searchAnimationController
                                                    .isAnimating) {
                                              searchAnimationController
                                                  .animateTo(1);
                                              setState(() {
                                                showSearch = false;
                                                print("showSearch false!");
                                              });

                                              logoAnimationController
                                                  .animateTo(1);
                                              logoSizeAnimationController
                                                  .animateTo(1);
                                            }
                                          } else {
                                            if (searchAnimationController
                                                .value !=
                                                0 &&
                                                !searchAnimationController
                                                    .isAnimating) {
                                              searchAnimationController
                                                  .animateTo(0);
                                              logoAnimationController
                                                  .animateTo(0);
                                              logoSizeAnimationController
                                                  .animateTo(0);
                                              setState(() {
                                                showSearch = true;
                                                print("showSearch true!");
                                              });
                                            }
                                          }
                                        }

                                        //
                                        // else if(scrollNotification.metrics.pixels <100      ){
                                        //   searchAnimationController.animateTo(
                                        //       scrollNotification.metrics
                                        //           .pixels / 100);
                                        // }

                                        // print("_ColorAnimationController " + _ColorAnimationController.value.toString());
                                      },
                                      child: SingleChildScrollView(
                                          physics: searchEdit.text.length == 0? physics:physics,
                                          keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,

                                          child:  searchEdit.text.length> 0? refreshSearchContent() : refreshContent()),
                                    );

                                  }
                              ),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Stack(

                              children: [
                                AnimatedBuilder(
                                    animation: searchAnimationController,
                                    builder: (context, child) =>

                                        Container(

                                          width: 0.88.sw,
                                          // height: sizeOffset.value/450*165,

                                          child: Stack(
                                            alignment: Alignment.centerRight,
                                            children: [
                                              Container(
                                                height : (per + 0.017).sh,
                                                color: OwnColors.tran,
                                              ),
                                              Column(

                                                children: [
                                                  AnimatedBuilder(
                                                      animation:
                                                      logoSizeAnimationController,
                                                      builder: (context, child) =>
                                                          Container(
                                                            width: heightSpaceOffset.value,
                                                            height:  heightSpaceOffset.value,
                                                          )),
                                                  Align(
                                                    alignment: Alignment.bottomCenter,
                                                    child: SlideTransition(
                                                      position: offset,
                                                      child: AnimatedBuilder(
                                                          animation:
                                                          logoSizeAnimationController,
                                                          builder: (context, child) =>
                                                              Container(
                                                                  width: sizeOffset.value,
                                                                  height: heightOffset.value  ,
                                                                  child: Image.asset(
                                                                      Res.top_icon))),
                                                    ),
                                                  ),
                                                  AnimatedBuilder(
                                                      animation:
                                                      logoSizeAnimationController,
                                                      builder: (context, child) =>
                                                          Container(
                                                            width: heightSpaceOffset.value,
                                                            height:  heightSpaceOffset.value,
                                                          )),


                                                ],
                                              ),
                                              Positioned(
                                                bottom: 0.017.sh,
                                                child: AnimatedBuilder(
                                                    animation:
                                                    searchAnimationController,
                                                    builder: (context, child) =>
                                                        Opacity(
                                                          opacity: searchAnimationController.value,
                                                          child: GestureDetector(
                                                            onTap: () {
                                                              if(searchAnimationController.value == 1 && showSearch == false){
                                                                print("onTap");
                                                                searchAnimationController.animateTo(0);
                                                                searchAnimationController
                                                                    .animateTo(0);
                                                                logoAnimationController
                                                                    .animateTo(0);
                                                                logoSizeAnimationController
                                                                    .animateTo(0);
                                                                setState(() {
                                                                  showSearch = true;
                                                                });
                                                              }

                                                            },
                                                            child: Container(
                                                                height:
                                                                0.88.sw / 3 / 450 * 100,
                                                                child: Image.asset(
                                                                    Res.search_black)),
                                                          ),
                                                        )),
                                              )
                                            ],
                                          ),
                                        ))
                              ],
                            ),
                            ExpandedSection(
                              child: Container(
                                width: 1.sw,
                                height: 0.07 * ScreenSize().getHeight(context),
                                color: OwnColors.white,
                                child: Column(
                                  children: [
                                    Container(
                                      height: 0.05 *
                                          ScreenSize().getHeight(context),
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.center,
                                        crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            height: 0.05 *
                                                ScreenSize().getHeight(context),
                                            width:
                                            ScreenSize().getWidth(context) *
                                                0.88,
                                            // height: height,
                                            decoration: BoxDecoration(
                                              color: OwnColors.white,
                                              border: Border.all(
                                                  color: OwnColors.CC8C8C8),
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(30),
                                                  topRight: Radius.circular(30),
                                                  bottomLeft:
                                                  Radius.circular(30),
                                                  bottomRight:
                                                  Radius.circular(30)),
                                            ),

                                            alignment: Alignment.center,
                                            child: Row(
                                              children: [
                                                GestureDetector(
                                                  onTap: () {
                                                    print("search click");
                                                    FocusScope.of(context)
                                                        .requestFocus(
                                                        searchFocus);
                                                  },
                                                  child: Padding(
                                                    padding:
                                                    const EdgeInsets.only(
                                                        left: 12.0,
                                                        bottom: 4.0),
                                                    child: Container(
                                                        height: 0.027 *
                                                            ScreenSize()
                                                                .getHeight(
                                                                context),
                                                        child: Icon(
                                                            SFSymbols.search,
                                                            color: OwnColors
                                                                .black)),
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Container(
                                                    height: 0.027 *
                                                        ScreenSize()
                                                            .getHeight(context),
                                                    child: TextField(
                                                      controller: searchEdit,
                                                      focusNode: searchFocus,
                                                      onChanged: (text) {
                                                        print("len " +
                                                            searchEdit
                                                                .text.length
                                                                .toString());
                                                        setState(() {});
                                                        print("onChanged");
                                                        pageCntSearch = 1;
                                                        if (searchText1 == "") {
                                                          searchText1 = text;
                                                          searchLoading = true;
                                                          refreshController.callRefresh();
                                                          getTouristSearchAPI(false);
                                                        } else {
                                                          if (searchText1 !=
                                                              text) {
                                                            searchText1 = text;
                                                            searchLoading = true;
                                                            refreshController.callRefresh();
                                                            getTouristSearchAPI(false);
                                                          }
                                                        }
                                                      },
                                                      autocorrect: false,
                                                      decoration:
                                                      InputDecoration(
                                                          fillColor:
                                                          OwnColors
                                                              .tran,
                                                          contentPadding:
                                                          EdgeInsets.symmetric(
                                                              horizontal:
                                                              10,
                                                              vertical:
                                                              0),
                                                          //Change this value to custom as you like
                                                          // isDense: true,
                                                          hintText: S()
                                                              .home_search_hint,
                                                          filled: true,
                                                          hintStyle:
                                                          GoogleFonts
                                                              .inter(
                                                            fontSize:
                                                            16.nsp,
                                                            color: OwnColors
                                                                .black,
                                                            fontWeight:
                                                            FontWeight
                                                                .w500,
                                                            //fontStyle: FontStyle.normal
                                                          ),
                                                          border: OutlineInputBorder(
                                                              borderRadius:
                                                              new BorderRadius
                                                                  .circular(
                                                                  20.0),
                                                              borderSide:
                                                              BorderSide
                                                                  .none)),
                                                      style: GoogleFonts.inter(
                                                          color: Colors.black
                                                              .withOpacity(1),
                                                          fontSize: ScreenUtil()
                                                              .setSp(16)),
                                                      maxLines: 1,
                                                      keyboardType:
                                                      TextInputType.text,
                                                      obscureText: false,
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                  const EdgeInsets.only(
                                                      right: 8.0),
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      setFilter();
                                                    },
                                                    child: Container(
                                                        height: 0.033 *
                                                            ScreenSize()
                                                                .getHeight(
                                                                context),
                                                        width: 0.033 *
                                                            ScreenSize()
                                                                .getHeight(
                                                                context),
                                                        decoration: BoxDecoration(
                                                            color:
                                                            OwnColors.white,
                                                            shape:
                                                            BoxShape.circle,
                                                            border: Border.all(
                                                                color: OwnColors
                                                                    .CC8C8C8)),
                                                        child: Center(
                                                            child: Icon(
                                                              SFSymbols
                                                                  .line_horizontal_3_decrease,
                                                              size: 0.026 *
                                                                  ScreenSize()
                                                                      .getHeight(
                                                                      context),
                                                            ))),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    HorizontalSpace().create(context, 0.02),
                                  ],
                                ),
                              ),
                              expand: showSearch,
                            ),
                          ],
                        )
                      ],
                    ))




                  ],
                ),
              ),
            ),
          ),
        ),

      ),
    );

  }

  Widget refreshSearchContent(){
    return                 Column(
      children: [

        Container(
          width:  0.88*ScreenSize().getWidth(context),

          child:
          Container(
            constraints: BoxConstraints(
                minHeight: 0.8.sh
            ),
            child: ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: searchList.length,
                scrollDirection: Axis.vertical,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(
                        top: 0.0 ),
                    child: Padding(
                      padding: const EdgeInsets.only(left:0.0,right: 0),
                      child: GuildCard().tourCard(context,searchList[index],index,tourClick),),

                  );
                }

            ),
          )
          ,
        )
      ],
    );
  }

  Widget refreshContent(){
    return                 Column(
      children: [
        Container(
          width: ScreenSize().getWidth(context)*0.88,
          child: Align(
            alignment: Alignment.centerLeft,
            child: AutoSizeText(
              S().home_title1,
              maxLines: 1,
              style: GoogleFonts.inter(fontWeight: FontWeight.w600,
                fontSize: 20.nsp, color: OwnColors.black,),
            ),
          ),
        ),
        HorizontalSpace().create(context, 0.01),
        Transform.translate(
          offset: Offset(-x, 0),
          child: Container(
            width:  1*ScreenSize().getWidth(context),
            height: 1*ScreenSize().getWidth(context)/16*9 + 0.025*ScreenSize().getHeight(context),

            child:

            topList.length == 0?
            Container(height: 0.2.sh,)
                :
            ListView.builder(
                physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
                shrinkWrap: true,
                itemCount: topList.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(
                        top: 0.0 ),
                    child: Padding(
                      padding: const EdgeInsets.only(left:0.0,right: 8),
                      child: bestCard(topList[index],index),),

                  );
                }

            )
            ,
          ),
        ),
        HorizontalSpace().create(context, 0.037),
        Container(
          width: ScreenSize().getWidth(context)*0.88,
          child: Align(
            alignment: Alignment.centerLeft,
            child: AutoSizeText(
              S().home_title2,
              maxLines: 1,
              style: GoogleFonts.inter(fontWeight: FontWeight.w600,
                fontSize: 20.nsp, color: OwnColors.black,),
            ),
          ),
        ),
        Container(
          width:  0.88*ScreenSize().getWidth(context),

          child:bottomList.length == 0?
    SharePre.prefs.getString(GDefine.guilderFilter).contains("true") ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              HorizontalSpace().create(context, 0.08),
              Container(
                height: 0.118.sh,
                child: Image.asset(Res.not_found),
              ),
              HorizontalSpace().create(context, 0.01),
              Text(
                S().search_not_found_title,
                style: GoogleFonts.inter(
                    fontSize: 16.nsp,
                    fontWeight: FontWeight.w600,
                    color: OwnColors.black
                ),
              ),
              HorizontalSpace().create(context, 0.01),
              Text(
                S().search_not_found_hint,
                style: GoogleFonts.inter(
                    fontSize: 12.nsp,
                    fontWeight: FontWeight.w500,
                    color: OwnColors.C989898
                ),
              ),
              HorizontalSpace().create(context, 0.1),


            ],
          ):Container(
      height: 0.3.sh,
    )

              :

          Container(
            constraints: BoxConstraints(
              minHeight: 0.4.sh
            ),
            child: ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: bottomList.length,
                scrollDirection: Axis.vertical,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(
                        top: 0.0 ),
                    child: Padding(
                      padding: const EdgeInsets.only(left:0.0,right: 0),
                      child: GuildCard().tourCard(context,bottomList[index],index,tourClick),),

                  );
                }

            ),
          )
          ,
        )
      ],
    );
  }


  Container bestCard(GuildClass guildClass,int index) {

    print("guildClass.score  " + guildClass.score );
    print("guildClass.score  " + guildClass.name );
    return Container(
      width: 0.4128.sw + (index == 0? 0.06.sw : 0.02.sw),
      // height: 0.154 *  ScreenSize().getHeight(context),

      child: Container(
        decoration: BoxDecoration(
          color: OwnColors.white,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(15),
            topLeft: Radius.circular(15),
            bottomLeft: Radius.circular(15),
            bottomRight: Radius.circular(15),
          ),

        ),
        child: GestureDetector(
          onTap: () => {bestClick(guildClass)},
          child: Row(
            children: [
              VerticalSpace().create(context, index == 0? 0.06 : 0.02 ),
              Expanded(
                child: Stack(
                  alignment: Alignment.topLeft,
                  children:[
                    Padding(
                      padding: const EdgeInsets.only(left:4.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(height: 2,),
                          Container(
                            width: 0.4128.sw,
                            child: Center(
                              child: AspectRatio(
                                aspectRatio: 1,
                                child: Stack(
                                  children: [
                                    CachedNetworkImage(

                                      imageUrl: guildClass.avatar.contains("choose_img.png") ? GDefine.guilderDefault : guildClass.avatar
                                      ,//guildClass.tourist_img.length>0?  guildClass.tourist_img[0]:guildClass.avatar,
                                      fit: BoxFit.fill ,
                                      placeholder: (context, url) => AspectRatio(
                                        aspectRatio: 1,
                                        child: FractionallySizedBox(
                                          heightFactor: 0.5,
                                          widthFactor: 0.5,
                                          child: SpinKitRing(
                                            lineWidth:5,
                                            color:
                                            OwnColors.tran ,
                                          ),
                                        ),
                                      ),
                                      errorWidget: (context, url, error) => Icon(Icons.error,color: OwnColors.black.withOpacity(0.5),),
                                      imageBuilder: (context, imageProvider) => Container(

                                        decoration: BoxDecoration(
                                          color: OwnColors.tran,
                                          borderRadius: BorderRadius.all(Radius.circular(15.0)),

                                          image: DecorationImage(
                                              image: imageProvider, fit: BoxFit.cover),
                                        ),
                                      ),
                                      // width: MediaQuery.of(context).size.width,

                                    ),
                                    Positioned.fill(
                                      bottom: 0,
                                      // right: 5,
                                      child: Column(
                                        children: [
                                          Spacer(),
                                          Container(

                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.only(
                                                    bottomLeft: Radius.circular(15),
                                                    bottomRight: Radius.circular(15)),
                                                gradient: LinearGradient(
                                                  begin: Alignment.topCenter,
                                                  end: Alignment.bottomCenter,
                                                  colors: [
                                                    OwnColors.C797979
                                                        .withOpacity(0.0),
                                                    OwnColors.C787878
                                                        .withOpacity(0.0073),
                                                    OwnColors.C3E3E3E
                                                        .withOpacity(0.3391),
                                                    Colors.black.withOpacity(0.7),
                                                  ],
                                                )),
                                            child: Align(
                                              alignment: Alignment.centerLeft,
                                              child: Padding(
                                                padding: const EdgeInsets.only(top:14,right: 10,bottom: 8,left: 8),
                                                child: Text(
                                                  guildClass.name,

                                                  overflow: TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                  style:GoogleFonts.inter (
                                                      fontWeight: FontWeight.w500,
                                                      fontSize: 16.nsp,
                                                      color: OwnColors.white),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )                              ],
                                ),
                              ),
                            ),
                          ),

                          Container(height: 4),
                          Container(

                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [

                                Padding(
                                  padding: const EdgeInsets.only(bottom: 4.0,left: 3),
                                  child: Container(width:0.04.sw,height: 0.017.sh,child: Icon(SFSymbols.star_fill, color: OwnColors.CTourOrange,size: 17,)),
                                ),
                                Container(width: 5,),
                                Expanded(
                                  child: Row(

                                      children:[
                                        Flexible(
                                          child: Text(
                                            guildClass.score,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                            style: GoogleFonts.inter(
                                                fontSize: 12.nsp,
                                                color: OwnColors.black,
                                                fontWeight: FontWeight.w500

                                            ),
                                          ),
                                        ),
                                        Flexible(
                                          child: Text(
                                            " (" +guildClass.comment_num+

                                                (int.parse(guildClass.comment_num) > 1 ? S().home_rate_number : S().home_rate_number_single)

                                                +")",
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                            style: GoogleFonts.inter(
                                                fontSize: 10.nsp,
                                                height: 1.2,
                                                color: OwnColors.black,
                                                fontWeight: FontWeight.w300

                                            ),
                                          ),
                                        )

                                      ]


                                  ),
                                ),
                               ],
                            ),
                          ),
                          HorizontalSpace().create(context, 0.005),
                          Row(

                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [

                              Padding(
                                padding: const EdgeInsets.only(bottom: 2.0,left: 4),
                                child: Container(width:0.04.sw,height: 0.017.sh,child: Icon(SFSymbols.globe, size: 16,)),
                              ),
                              Container(width: 2,),
                              Expanded(
                                child: Text(
                                  guildClass.lang.join(" | ") ,


                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  style: GoogleFonts.inter(
                                    fontSize: 12.nsp,
                                    fontWeight: FontWeight.w400,
                                    color: OwnColors.black,

                                  ),
                                ),
                              ),
                            ],
                          ),
                          HorizontalSpace().create(context, 0.005),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Visibility(
                              visible: guildClass.lang.length > 0,
                              child: ExtendedWrap(

                                spacing: 8,
                                maxLines: 1,
                                runSpacing : 5.0,
                                children: List.generate(
                                  // guildClass.location.length <= 2 ?
                                  guildClass.location.length ,
                                      // : 2,

                                      (i) {
                                    return Container(
                                        height: 0.023*ScreenSize().getHeight(context),
                                        decoration: BoxDecoration(
                                            color: OwnColors.white,
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(10),
                                                topRight: Radius.circular(10),
                                                bottomLeft: Radius.circular(10),
                                                bottomRight: Radius.circular(10)),
                                            border: Border.all(
                                                width: 0.5,color: OwnColors.CC8C8C8
                                            ),
                                            boxShadow: [
                                              BoxShadow(
                                                color: OwnColors.primaryText.withOpacity(0.4),
                                                spreadRadius: 1,
                                                blurRadius: 4,
                                                offset: Offset(0, 3), // changes position of shadow
                                              ),
                                            ]),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(left:8.0,right: 8,bottom: 0),
                                              child: Text(guildClass.location[i],
                                                textAlign: TextAlign.center,
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                                style: GoogleFonts.inter(
                                                    height: 1.2,
                                                    fontSize: 12.nsp,
                                                    color: OwnColors.black,
                                                    fontWeight: FontWeight.w400
                                                ),
                                              ),
                                            ),
                                          ],
                                        ));
                                  },
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),

                    Positioned(
                      top: 0,
                      left: 0,                    child: Container(
                        height: ScreenSize().getHeight(context)*0.04,
                        child: AspectRatio(
                          aspectRatio: 27/30,
                          child: Image.asset(rankImg[index],fit: BoxFit.fill,),
                        ),
                      ),
                    )

                  ] ,
                ),
              ),
              // VerticalSpace().create(context, index == bestTourList.length-1? 0.05:0.0),
            ],
          ),
        ),
      ),
    );
  }


  Container bestCardLoading( ) {

    return Container(
      decoration: BoxDecoration(
        color: OwnColors.white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(15),
          topLeft: Radius.circular(15),
          bottomLeft: Radius.circular(15),
          bottomRight: Radius.circular(15),
        ),

      ),
      child: Row(
        children: [
          VerticalSpace().create(context, 0.05 ),
          Stack(
            children:[
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    flex:161 ,
                    child: Center(
                      child: AspectRatio(
                        aspectRatio: 1,
                        child: Stack(
                          children: [
                            CachedNetworkImage(
                              imageUrl:
                              "https://img.ltn.com.tw/Upload/playing/page/2019/09/14/190914-21024-01-WvNZA.jpg",
                              fit: BoxFit.fill ,
                              placeholder: (context, url) => AspectRatio(
                                aspectRatio: 1,
                                child: FractionallySizedBox(
                                  heightFactor: 0.5,
                                  widthFactor: 0.5,
                                  child: SpinKitRing(
                                    lineWidth:5,
                                    color:
                                    OwnColors.tran ,
                                  ),
                                ),
                              ),
                              errorWidget: (context, url, error) => Icon(Icons.error,color: OwnColors.black.withOpacity(0.5),),
                              imageBuilder: (context, imageProvider) => Container(

                                decoration: BoxDecoration(
                                  color: OwnColors.tran,
                                  borderRadius: BorderRadius.all(Radius.circular(15.0)),

                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover),
                                ),
                              ),
                              // width: MediaQuery.of(context).size.width,

                            ),
                            Positioned(
                              right: 10,
                              top: 10,
                              child: Container(height: 0.03*ScreenSize().getHeight(context),child: Image.asset(Res.favorite)),

                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Spacer(flex: 10,),
                  Expanded(
                    flex: 22,
                    child: Container(
                      width:  0.26 * ScreenSize().getHeight(context) / 211*161,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: AutoSizeText(
                              "",
                              maxLines: 1,
                              minFontSize: 16,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                fontSize: 16.nsp,
                                color: OwnColors.black,

                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top:2.0,bottom: 2.0,left: 4),
                            child: Image.asset(Res.star),
                          ),
                          AutoSizeText(
                           "" + " (" + ""+S().home_rate_number+")",
                            minFontSize: 1,
                            style: TextStyle(
                              fontSize: 12.nsp,
                              color: OwnColors.black,

                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 18,
                    child: Row(

                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [

                        Padding(
                          padding: const EdgeInsets.only(top:2.0,bottom: 2.0,right: 4),
                          child: Image.asset(Res.location),
                        ),
                        AutoSizeText(
                          "",
                          minFontSize: 1,
                          style: TextStyle(
                            fontSize: 12.nsp,
                            color: OwnColors.black,

                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),



            ] ,
          ),
          VerticalSpace().create(context, 0.03),
        ],
      ),
    );
  }

  Widget tourCardLoading( int index) {

    return Container(
      width: double.infinity,
      height: 0.154 *  ScreenSize().getHeight(context),

      child: Container(
        decoration: BoxDecoration(
          color: OwnColors.white,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(15),
            topLeft: Radius.circular(15),
            bottomLeft: Radius.circular(15),
            bottomRight: Radius.circular(15),
          ),

        ),
        child: Column(
          children: [

            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: AspectRatio(
                      aspectRatio: 1.384,
                      child: Stack(
                        children: [
                          CachedNetworkImage(
                            imageUrl:
                            "https://img.ltn.com.tw/Upload/playing/page/2019/09/14/190914-21024-01-WvNZA.jpg",
                            fit: BoxFit.fill ,
                            placeholder: (context, url) => AspectRatio(
                              aspectRatio: 1,
                              child: FractionallySizedBox(
                                heightFactor: 0.5,
                                widthFactor: 0.5,
                                child: SpinKitRing(
                                  lineWidth:5,
                                  color:
                                  OwnColors.tran ,
                                ),
                              ),
                            ),
                            errorWidget: (context, url, error) => Icon(Icons.error,color: OwnColors.black.withOpacity(0.5),),
                            imageBuilder: (context, imageProvider) => Container(

                              decoration: BoxDecoration(
                                color: OwnColors.tran,
                                borderRadius: BorderRadius.all(Radius.circular(15.0)),

                                image: DecorationImage(
                                    image: imageProvider, fit: BoxFit.cover),
                              ),
                            ),
                            // width: MediaQuery.of(context).size.width,

                          ),
                          Positioned(
                            right: 10,
                            top: 10,
                            child: Container(height: 0.03*ScreenSize().getHeight(context),child: Image.asset(Res.favorite)),
                          )
                        ],
                      ),
                    ),
                  ),
                  Spacer(flex: 15,),
                  Expanded(
                    flex: 150,
                    child: Column(

                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Expanded(
                              child: AutoSizeText(

                                "HHHHHH",
                                maxLines: 1,
                                minFontSize: 16,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: 16.nsp,
                                  color: OwnColors.black,

                                ),
                              ),
                            ),
                            Container(
                              height: 0.02 * ScreenSize().getHeight(context),
                              child: Padding(
                                padding: const EdgeInsets.only(top:2.0,bottom: 2.0,left: 4),
                                child: Image.asset(Res.star),
                              ),
                            ),
                            AutoSizeText(
                              "50"  ,
                              minFontSize: 1,
                              style: TextStyle(
                                  fontSize: 12.nsp,
                                  color: OwnColors.black,
                                  fontWeight: FontWeight.bold
                              ),
                            ),
                            AutoSizeText(
                              " (" + "HHHHHH"+S().home_rate_number+")",
                              minFontSize: 1,
                              style: TextStyle(
                                fontSize: 12.nsp,
                                color: OwnColors.black,

                              ),
                            )

                          ],
                        ),
                        Row(

                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [

                            Container(
                              height: 0.02* ScreenSize().getHeight(context),
                              child: Padding(
                                padding: const EdgeInsets.only(top:2.0,bottom: 2.0,right: 4),
                                child: Image.asset(Res.location),
                              ),
                            ),
                            AutoSizeText(
                              "HHHHHH",
                              minFontSize: 1,
                              style: TextStyle(
                                fontSize: 12.nsp,
                                color: OwnColors.black,

                              ),
                            )
                          ],
                        ),
                        Row(

                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [

                            Container(
                              height: 0.02* ScreenSize().getHeight(context),
                              child: Padding(
                                padding: const EdgeInsets.only(top:2.0,bottom: 2.0,right: 4),
                                child: Image.asset(Res.car),
                              ),
                            ),
                            AutoSizeText(
                            "",
                              minFontSize: 1,
                              style: TextStyle(
                                fontSize: 12.nsp,
                                color: OwnColors.black,

                              ),
                            )
                          ],
                        ),
                        Container(height: 5,),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Wrap(

                            spacing: 5,
                            runSpacing : 5.0,
                            children: List.generate(
                              2,

                                  (i) {
                                return Container(
                                    height: 0.023*ScreenSize().getHeight(context),
                                    decoration: BoxDecoration(
                                        color: OwnColors.white,
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(10),
                                            topRight: Radius.circular(10),
                                            bottomLeft: Radius.circular(10),
                                            bottomRight: Radius.circular(10)),
                                        border: Border.all(
                                            width: 0.5,color: OwnColors.CC8C8C8
                                        ),
                                        boxShadow: [
                                          BoxShadow(
                                            color: OwnColors.primaryText.withOpacity(0.4),
                                            spreadRadius: 1,
                                            blurRadius: 4,
                                            offset: Offset(0, 3), // changes position of shadow
                                          ),
                                        ]),
                                    child: Padding(
                                      padding: const EdgeInsets.only(left:8.0,right: 8,bottom: 0),
                                      child: AutoSizeText("i++",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            height: 1.3,
                                            fontSize: 12.nsp,
                                            color: OwnColors.black
                                        ),
                                      ),
                                    ));
                              },
                            ),
                          ),
                        )

                      ],
                    ),
                  )
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }


  void bestClick(GuildClass GuildClass){
    GlobalData.refreshGuild = false;
    PushNewScreen().normalPushThen(context, GuilderPage(guildClass: GuildClass,preview: false),refreshScore);
  }
  void tourClick(GuildClass GuildClass){
    GlobalData.refreshGuild = false;

    PushNewScreen().normalPushThen(context, GuilderPage(guildClass: GuildClass,preview: false),refreshScore);
  }

  void refreshScore(){
    print("refreshScore");
    if(GlobalData.curernt_guilder !=null) {
      int index = topList.indexWhere((element) =>
      element.id == GlobalData.curernt_guilder.id);
      if(index != -1) {
        topList[index].score = GlobalData.curernt_guilder.score;
        topList[index].comment_num = GlobalData.curernt_guilder.comment_num;
      }
      index = searchList.indexWhere((element) =>
      element.id == GlobalData.curernt_guilder.id);
      if(index != -1) {
        searchList[index].score = GlobalData.curernt_guilder.score;
        searchList[index].comment_num = GlobalData.curernt_guilder.comment_num;
      }
      index = bottomList.indexWhere((element) =>
      element.id == GlobalData.curernt_guilder.id);
      if(index != -1) {
        bottomList[index].score = GlobalData.curernt_guilder.score;
        bottomList[index].comment_num = GlobalData.curernt_guilder.comment_num;
      }

      setState(() {
        print("refreshScore setState");

      });
    }
  }

  Future<void> _pushSaved() async {
    print("_pushSaved");

  }

  Future<void> setFilter() async {
    // PushNewScreen().normalPush(context, GuildFilterPage());
    Tools().dismissK(context);
    print("DDDCCCD " + FilterData.guild_transSelList.toString());
    int result = await  showCupertinoModalBottomSheet(
      barrierColor:Colors.black54,
      topRadius: Radius.circular(30),
      expand: false,
      context: context,
      enableDrag: false,
      backgroundColor: Colors.transparent,
      builder: (context) => GuildFilterPage(),
    ).whenComplete(() {

    });
    print("DDDD " + FilterData.guild_transSelList.toString());
    print("result " + result.toString());
    if(result == 1){
      pageCnt = 1;
      SharePre.prefs.setString(GDefine.guilderFilter, "true");
      tourList.clear();
      filter = true;
      pageCnt = 1;
      pageCntSearch = 1;

      if(searchEdit.text == "") {
        getTouristAPI(false);
      }else {
        getTouristSearchAPI(false);
      }

      // getTouristFilterAPI();
    }else if(result == 2){
      SharePre.prefs.setString(GDefine.guilderFilter, "false");
      filter = false;
      pageCnt = 1;
      pageCntSearch = 1;
      if(searchEdit.text == "") {
        getTouristAPI(false);
      }else {
        getTouristSearchAPI(false);
      }
    }

  }

  void getWeekTouristAPI( ) async {
    final date = DateTime.now();
    // print('Start of week: ${getDate(date.subtract(Duration(days: date.weekday - 2)))}');
    // DateTime start  = getDate(date.subtract(Duration(days: date.weekday - 2)));
    // DateTime end  = getDate(date.add(Duration(days: DateTime.daysPerWeek - date.weekday)));

    int nowyear = DateTime.now().year; //今年
    int nowmonth = DateTime.now().month; //这个月是几月
    print("month="+nowmonth.toString());
    int nowday = DateTime.now().day; //今天
    int weekday = DateTime.now().weekday; //今天是本周的第几天
    DateTime lastweekstart = new DateTime(nowyear, nowmonth, nowday - weekday - 6); //上周一的时间
    DateTime Sundayweekstart = new DateTime(nowyear, nowmonth, nowday - weekday); //上周一的时间

    String start_string =DateFormat('yyyy/MM/dd').format( DateTools().findFirstDateOfTheWeek(lastweekstart));
    String end_string =DateFormat('yyyy/MM/dd').format( DateTools().findLastDateOfTheWeek(lastweekstart));

    ResultData resultData = await AppApi.getInstance().getTouristList(context, true,  SharePre.prefs.getString(GDefine.user_id),
        start_string,end_string,10,"zh","true");

    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      // Notice how you have to call body from the response if you are using http to retrieve json
      // print(resultData.data.toString());
      bestTourList.clear();
      for (Map<String, dynamic> a in resultData.data) {
        GuildClass c = GuildClass.fromJson(a);
        bestTourList.add(c);
      }

      topList.clear();
      topList.addAll(bestTourList);

      setState(() {
        print("getWeekTouristAPI");
        loadingBest = false;

      });
    } else {
      // WarningDialog.showIOSAlertDialog(context, "下載達人失敗", resultData.msg,"確認");
    }
  }




  void getTouristAPI(bool page ) async {
    print("getTouristFilterAPI");
    List<String> transSend = new List();
    for(int index = 0; index < FilterData.guild_transSelList.length;index++){
      if(FilterData.guild_transSelList[index]){
        transSend.add(index.toString());
      }
    }

    List<String> langSend = new List();
    for(int index = 0; index < FilterData.guild_lansSelList.length;index++){
      if(FilterData.guild_lansSelList[index]){
        langSend.add(FilterData.lansList[index]);
      }
    }

    ResultData resultData = await AppApi.getInstance().getTouristFilterPageList(context, true,
        SharePre.prefs.getString(GDefine.user_id),
        "","",cntPerPage,
        filter? langSend : [],
        filter? FilterData.guild_rating.toString(): "",
        filter? FilterData.guild_gender: "",
        filter? transSend:  [],
        filter? FilterData.guild_startNum.toString(): "",
        filter? FilterData.guild_endNum.toString(): "",
        "false",
        filter? FilterData.guild_want: 3,
        pageCnt);

    // ResultData resultData = await AppApi.getInstance().getTouristListPage(context, true,
    //     SharePre.prefs.getString(GDefine.user_id), "","",cntPerPage,"zh","false",pageCnt);
    int commentMax = 1;

    if (resultData.isSuccess()) {
      print(resultData.data.toString());
      if(!page) {
        tourList.clear();
      }
      totalCnt = resultData.data["total"];
      for (Map<String, dynamic> a in resultData.data["datas"]) {
        // GuildClass c = GuildClass.fromJson(a);
        // tourList.add(new GuildClass(id: c.id,tourist_img: [c.avatar],name: c.name,score: double.parse(c.score).toStringAsFixed(1),location: c.location ,comment_num: c.comment_num, avatar: c.avatar, lang: c.lang));
        tourList.add( GuildClass.fromJson(a));
        print("name : " + GuildClass.fromJson(a).name.toString());
        print("comment_num : " + GuildClass.fromJson(a).comment_num.toString());
        print("invite_code : " + GuildClass.fromJson(a).invite_code.toString());
        print("commentMax " + commentMax.toString());
        if(int.parse(GuildClass.fromJson(a).comment_num) > commentMax){
          commentMax = int.parse(GuildClass.fromJson(a).comment_num);


        }
        //bestTourList.add(new GuildClass(id: c.id,tourist_img: [c.avatar],name: c.name,score: double.parse(c.score).toStringAsFixed(1),location: c.location ,comment_num: c.comment_num, avatar: c.avatar, lang: c.lang));

      }


      bottomList.clear();
      bottomList.addAll(tourList);

      setState(() {
        print("getTouristAPI");
        loadingTotal = false;

      });
      refreshController.finishRefresh();
      refreshController.resetFooter();
    } else {
      refreshController.finishRefresh();
      refreshController.resetFooter();
      // WarningDialog.showIOSAlertDialog(context, "下載達人失敗", resultData.msg,"確認");
    }
  }




  // void getTouristFilterAPI() async {
  //   print("getTouristFilterAPI");
  //   List<String> transSend = new List();
  //   for(int index = 0; index < FilterData.guild_transSelList.length;index++){
  //     if(FilterData.guild_transSelList[index]){
  //       transSend.add(index.toString());
  //     }
  //   }
  //
  //   List<String> langSend = new List();
  //   for(int index = 0; index < FilterData.guild_lansSelList.length;index++){
  //     if(FilterData.guild_lansSelList[index]){
  //       langSend.add(FilterData.lansList[index]);
  //     }
  //   }
  //
  //   ResultData resultData = await AppApi.getInstance().getTouristFilterPageList(context, true,  SharePre.prefs.getString(GDefine.user_id),
  //       "","",cntPerPage,langSend, FilterData.guild_rating.toString(),FilterData.guild_gender, transSend,
  //       FilterData.guild_startNum.toString(), FilterData.guild_endNum.toString(),"false",FilterData.guild_want,pageCnt);
  //
  //   if (resultData.isSuccess()) {
  //     print(resultData.data.toString());
  //     // tourList.clear();
  //     for (Map<String, dynamic> a in resultData.data) {
  //       // GuildClass c = GuildClass.fromJson(a);
  //       // tourList.add(new GuildClass(id: c.id,tourist_img: [c.avatar],name: c.name,score: double.parse(c.score).toStringAsFixed(1),location: c.location ,comment_num: c.comment_num, avatar: c.avatar, lang: c.lang));
  //       tourList.add( GuildClass.fromJson(a));
  //       print("name : " + GuildClass.fromJson(a).name.toString());
  //       print("comment_num : " + GuildClass.fromJson(a).comment_num.toString());
  //       print("invite_code : " + GuildClass.fromJson(a).invite_code.toString());
  //
  //       //bestTourList.add(new GuildClass(id: c.id,tourist_img: [c.avatar],name: c.name,score: double.parse(c.score).toStringAsFixed(1),location: c.location ,comment_num: c.comment_num, avatar: c.avatar, lang: c.lang));
  //
  //     }
  //
  //     bottomList.clear();
  //     bottomList.addAll(tourList);
  //
  //     setState(() {
  //       print("getTouristAPI");
  //       loadingTotal = false;
  //
  //     });
  //     if(searchEdit.text.isNotEmpty){
  //       searchTravel();
  //     }
  //     refreshController.finishRefresh();
  //     refreshController.resetFooter();
  //   } else {
  //     WarningDialog.showIOSAlertDialog(context, S().download_expert_fail, S().download_expert_fail_hint,S().confirm);
  //   }
  // }

  // void getTouristFilterPageAPI() async {
  //   print("getTouristFilterPageAPI");
  //   List<String> transSend = new List();
  //   for(int index = 0; index < FilterData.guild_transSelList.length;index++){
  //     if(FilterData.guild_transSelList[index]){
  //       transSend.add(index.toString());
  //     }
  //   }
  //
  //   List<String> langSend = new List();
  //   for(int index = 0; index < FilterData.guild_lansSelList.length;index++){
  //     if(FilterData.guild_lansSelList[index]){
  //       langSend.add(FilterData.lansList[index]);
  //     }
  //   }
  //
  //   ResultData resultData = await AppApi.getInstance().getTouristFilterPageList(context, true,  SharePre.prefs.getString(GDefine.user_id),
  //       "","",50,langSend, FilterData.guild_rating.toString(),FilterData.guild_gender, transSend,
  //       FilterData.guild_startNum.toString(), FilterData.guild_endNum.toString(),"false", FilterData.guild_want,pageCnt);
  //
  //   if (resultData.isSuccess()) {
  //     print(resultData.data.toString());
  //     tourList.clear();
  //     for (Map<String, dynamic> a in resultData.data) {
  //       // GuildClass c = GuildClass.fromJson(a);
  //       // tourList.add(new GuildClass(id: c.id,tourist_img: [c.avatar],name: c.name,score: double.parse(c.score).toStringAsFixed(1),location: c.location ,comment_num: c.comment_num, avatar: c.avatar, lang: c.lang));
  //       tourList.add( GuildClass.fromJson(a));
  //       print("name : " + GuildClass.fromJson(a).name.toString());
  //       print("comment_num : " + GuildClass.fromJson(a).comment_num.toString());
  //       print("invite_code : " + GuildClass.fromJson(a).invite_code.toString());
  //
  //       //bestTourList.add(new GuildClass(id: c.id,tourist_img: [c.avatar],name: c.name,score: double.parse(c.score).toStringAsFixed(1),location: c.location ,comment_num: c.comment_num, avatar: c.avatar, lang: c.lang));
  //
  //     }
  //
  //     bottomList.clear();
  //     bottomList.addAll(tourList);
  //
  //     setState(() {
  //       print("getTouristAPI");
  //       loadingTotal = false;
  //
  //     });
  //     if(searchEdit.text.isNotEmpty){
  //       searchTravel();
  //     }
  //     refreshController.finishRefresh();
  //     refreshController.resetFooter();
  //   } else {
  //     WarningDialog.showIOSAlertDialog(context, S().download_expert_fail, S().download_expert_fail_hint,S().confirm);
  //   }
  // }


  void getTouristSearchAPI(bool page ) async {
    print("getTouristSearchAPI");
    List<String> transSend = new List();
    for(int index = 0; index < FilterData.guild_transSelList.length;index++){
      if(FilterData.guild_transSelList[index]){
        transSend.add(index.toString());
      }
    }

    List<String> langSend = new List();
    for(int index = 0; index < FilterData.guild_lansSelList.length;index++){
      if(FilterData.guild_lansSelList[index]){
        langSend.add(FilterData.lansList[index]);
      }
    }

    ResultData resultData = await AppApi.getInstance().getTouristSearchList(context, true,
        SharePre.prefs.getString(GDefine.user_id),
        "","",cntPerPage,
        filter? langSend : [],
        filter? FilterData.guild_rating.toString(): "",
        filter? FilterData.guild_gender: "",
        filter? transSend:  [],
        filter? FilterData.guild_startNum.toString(): "",
        filter? FilterData.guild_endNum.toString(): "",
        "false",
        filter? FilterData.guild_want: 3,
        pageCntSearch,searchEdit.text);

    // ResultData resultData = await AppApi.getInstance().getTouristListPage(context, true,
    //     SharePre.prefs.getString(GDefine.user_id), "","",cntPerPage,"zh","false",pageCnt);
    int commentMax = 1;

    if (resultData.isSuccess()) {
      print(resultData.data.toString());
      if(!page) {
        searchList.clear();
      }
      totalCnt = resultData.data["total"];
      for (Map<String, dynamic> a in resultData.data["datas"]) {
        // GuildClass c = GuildClass.fromJson(a);
        // tourList.add(new GuildClass(id: c.id,tourist_img: [c.avatar],name: c.name,score: double.parse(c.score).toStringAsFixed(1),location: c.location ,comment_num: c.comment_num, avatar: c.avatar, lang: c.lang));
        searchList.add( GuildClass.fromJson(a));
        print("name : " + GuildClass.fromJson(a).name.toString());
        print("comment_num : " + GuildClass.fromJson(a).comment_num.toString());
        print("invite_code : " + GuildClass.fromJson(a).invite_code.toString());
        print("commentMax " + commentMax.toString());
        if(int.parse(GuildClass.fromJson(a).comment_num) > commentMax){
          commentMax = int.parse(GuildClass.fromJson(a).comment_num);


        }
        //bestTourList.add(new GuildClass(id: c.id,tourist_img: [c.avatar],name: c.name,score: double.parse(c.score).toStringAsFixed(1),location: c.location ,comment_num: c.comment_num, avatar: c.avatar, lang: c.lang));

      }


      refreshController.finishRefresh();
      refreshController.resetFooter();
    } else {
      refreshController.finishRefresh();
      refreshController.resetFooter();
      // WarningDialog.showIOSAlertDialog(context, "下載達人失敗", resultData.msg,"確認");
    }
    searchLoading = false;

    setState(() {});
  }

  void getScoreCount() async {
    print("getScoreCount");


    ResultData resultData = await AppApi.getInstance().getScoreCount(
      context,
      true,
      SharePre.prefs.getString(GDefine.user_id),
    );
    if (resultData.isSuccess()) {
      print(resultData.data.toString());
      try{
        GlobalData.article_duration_max = double.parse (resultData.data["article_duration_max"].toString());
        GlobalData.tourist_comment_max = double.parse (resultData.data["tourist_count"].toString());

      }catch (e){

      }
      print("article_duration_max " + GlobalData.article_duration_max.toString());
      print("tourist_comment_max " + GlobalData.tourist_comment_max.toString());

    } else {

    }
  }


  void sendFavorite(GuildClass guildClass) async {

    ResultData resultData = await AppApi.getInstance().setCollect(context, true,  SharePre.prefs.getString(GDefine.user_id),guildClass.id);
    if (resultData.isSuccess()) {
      if(guildClass.is_collect.contains("1")){
        guildClass.is_collect = "0";
      }else{
        guildClass.is_collect = "1";
      }
      setState(() {

      });
    } else {
      WarningDialog.showIOSAlertDialog(context, S().send_like_fail_title,S().send_like_fail_hint,S().confirm);
    }
  }


  DateTime getDate(DateTime d) => DateTime(d.year, d.month, d.day);


  // Future<void> searchTravel() async {
  //   print("searchTravel");
  //
  //   print("searchEdit.text" + searchEdit.text);
  //   topList.clear();
  //   bottomList.clear();
  //
  //   if(searchEdit.text == ""){
  //     topList.addAll(bestTourList);
  //     bottomList.addAll(tourList);
  //   }else {
  //     List<GuildClass> temp = bestTourList.where((element) =>
  //     element.name.toLowerCase().contains(searchEdit.text.toLowerCase())  ||
  //         element.location.indexWhere((cc) => cc.toString().toLowerCase().contains(searchEdit.text.toLowerCase())) != -1).toList();
  //     topList.addAll(temp);
  //
  //     List<GuildClass> temp2 = tourList.where((element) =>
  //     element.name.toLowerCase().contains(searchEdit.text.toLowerCase()) ||
  //
  //         element.location.indexWhere((cc) => cc.toString().toLowerCase().contains(searchEdit.text.toLowerCase())) != -1).toList();
  //     bottomList.addAll(temp2);
  //   }
  //
  //   setState(() {
  //
  //   });
  //
  //
  // }

}