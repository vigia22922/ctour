import 'dart:async';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/Chat/ChattingPage.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Define/GlobalData.dart';
import 'package:ctour/Firebase/FirebaseDynamicLink.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Guild/GuildReviewPage.dart';
import 'package:ctour/Guild/GuilderTravelPage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/ReportPage.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/StatusTools.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icons.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:selectable_autolink_text/selectable_autolink_text.dart';
import 'package:url_launcher/url_launcher.dart';
import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/TopBar.dart';
import '../res.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_star/flutter_star.dart';

class GuilderPage extends StatefulWidget {

  GuildClass guildClass;
  final String id;
  final bool preview;


   GuilderPage(
      {Key key,
        this.guildClass,
        this.id,
        this.preview,


      })
      : super(key: key);

  @override
  State createState() {
    return _GuilderPageState();
  }
}

class _GuilderPageState extends State<GuilderPage> {
  List<String> imageList = [
    GDefine.guilderDefault,
    GDefine.guilderDefault,
    GDefine.guilderDefault,
  ];

  // GuildClass guildClass;
  GuildClass guildClass = new GuildClass(name:"", score:"0.0", comment_num: "0", lang: [], trans: [], location: [], is_collect: "0", description: "",want_companion: "0");

  bool more  = false;
  final ScrollController _controller = ScrollController();
  double topShirkOffset = 0;
  double maxTop = 0.0;


  GlobalKey titleKey = new  GlobalKey();
  bool showTitle = false ;

  String link = "";
  @override
  void initState() {
    super.initState();
    StatusTools().setTranBGWhiteText();
    if(widget.guildClass == null) {

      Future.delayed(Duration(milliseconds: 200), () async {
        // 5s over, navigate to a new page
        await getGuild();
        imageList.clear();

        imageList.add(widget.guildClass.avatar.contains("choose_img.png") ? GDefine.guilderDefault : widget.guildClass.avatar);
        for (dynamic c in widget.guildClass.tourist_img){

          if(c.toString().contains("http")){
            imageList.add(c.toString());
          }
        }

        getLink();
      });
    }else {
      guildClass = widget.guildClass;
      imageList.clear();
      if(widget.guildClass.score == null){
        guildClass.score = "0.0";
        guildClass.comment_num = "0";
        Future.delayed(Duration(milliseconds: 200), () async {
          // 5s over, navigate to a new page
          await getGuild();

        });

      }

      imageList.add(widget.guildClass.avatar.contains("choose_img.png") ? GDefine.guilderDefault : widget.guildClass.avatar);

      print("widget.guildClass.avatar " +widget.guildClass.avatar.toString());
      print("widget.guildClass.tourist_img " +widget.guildClass.tourist_img.toString());
      for (dynamic c in widget.guildClass.tourist_img){

        if(c.toString().contains("http")){
          imageList.add(c.toString());
        }
      }

      getLink();
    }
    print("imageList" + imageList.toString());

    GlobalData.curernt_guilder = guildClass;



    // guildClass = new GuildClass(id: "",
    // tourist_img: ["https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg"],
    // name: "Ben",score: "5.0",location: ["台北","桃園"], trans:[0,2],lang: ["中文","英文","英文"],comment_num: "18"
    // ,about_me: "Hi~ 大家好 我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi~ 大家好 我是Melissa歡迎大家來找我玩    Hi~ 大家好 我是Melissa    歡迎大家來找我玩Hi~ 大家好 我是Melissa         ");
  }
  Future<void> getLink() async {
    link = await FirebaseDynamicLink.createDynamicLinkGuilder(widget.guildClass.id,imageList[0],widget.guildClass.name,widget.guildClass.description);
    print("link : " + link);
  }

  @override
  void dispose() {
    StatusTools().setWhitBGDarkText();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.white,

        body: Stack(
          children: [
            Container(
              width: ScreenSize().getWidth(context) * 1.0,
              color: OwnColors.white,
              height: MediaQuery.of(context).size.height,
              child: SingleChildScrollView(
                child: Container(
                  width: ScreenSize().getWidth(context) * 1.0,
                  height: MediaQuery.of(context).size.height,
                  child: Stack(
                    children: [
                      NotificationListener<ScrollUpdateNotification>(
                        onNotification: (notification) {

                          RenderBox titleBox = titleKey.currentContext.findRenderObject() as RenderBox;
                          double topH2 = 0.07 * ScreenSize().getHeight(context)  + MediaQuery.of(context).padding.top;
                          print ("titlebox " + titleBox.getTransformTo(null).getTranslation().y.toString());
                          if(titleBox.getTransformTo(null).getTranslation().y <= topH2) {
                            StatusTools().setWhitBGDarkText();
                            if(showTitle == false ){
                              setState(() {
                                showTitle = true;
                              });
                            }
                          }else {
                            StatusTools().setTranBGWhiteText();
                            if(showTitle == true ){
                              setState(() {
                                showTitle = false;
                              });
                            }
                          }


                        },
                        child: CustomScrollView(
                          controller: _controller,
                          physics: const BouncingScrollPhysics(),
                          slivers: [
                            SliverAppBar(
                                backgroundColor: OwnColors.white,
                                expandedHeight:0.5 * ScreenSize().getHeight(context),
                                shadowColor: OwnColors.white,
                                elevation: 0,
                                stretch: true,
                                bottom: PreferredSize(
                                  child: Container(
                                    color: Colors.orange,
                                  ),
                                  preferredSize: Size(0, 0),
                                ),
                                pinned: true,


                                flexibleSpace:

                                LayoutBuilder(
                                    builder: (BuildContext context, BoxConstraints constraints) {
                                      if(maxTop == 0){
                                        maxTop = constraints.biggest.height;
                                      }
                                      topShirkOffset = constraints.biggest.height;

                                      // if((topShirkOffset  - (0.07 * ScreenSize().getHeight(context) + MediaQuery.of(context).padding.top  )) <0.1){
                                      //   StatusTools().setWhitBGDarkText();
                                      // }else {
                                      //   StatusTools().setTranBGWhiteText();
                                      // }

                                    return Stack(
                                      children: [
                                        FlexibleSpaceBar(

                                          background:Swiper(

                                            itemBuilder: (BuildContext context, int index) {
                                              return CachedNetworkImage(
                                                imageUrl: imageList[index],
                                                fit: BoxFit.fill,
                                                placeholder: (context, url) => AspectRatio(
                                                  aspectRatio: 1,
                                                  child: FractionallySizedBox(
                                                    heightFactor: 0.5,
                                                    widthFactor: 0.5,
                                                    child: SpinKitRing(
                                                      lineWidth:5,
                                                      color:
                                                      OwnColors.tran ,
                                                    ),
                                                  ),
                                                ),
                                                errorWidget: (context, url, error) => Icon(
                                                  Icons.error,
                                                  color: OwnColors.black.withOpacity(0.5),
                                                ),
                                                imageBuilder: (context, imageProvider) =>
                                                    Container(
                                                      decoration: BoxDecoration(
                                                        color: OwnColors.tran,
                                                        image: DecorationImage(
                                                            image: imageProvider,
                                                            fit: BoxFit.cover),
                                                      ),
                                                    ),
                                                // width: MediaQuery.of(context).size.width,
                                              );
                                            },
                                            itemCount: imageList.length,
                                            autoplay: false,
                                            loop: false,

                                            pagination: new SwiperPagination(
                                                builder: new DotSwiperPaginationBuilder(
                                                  color: Colors.white,
                                                  size: 8,
                                                  activeSize: 8,
                                                  activeColor: imageList.length == 1? OwnColors.tran:OwnColors.CTourOrange,

                                                ),
                                                margin: EdgeInsets.only(
                                                    bottom: 0.05 *
                                                        ScreenSize()
                                                            .getHeight(context) +
                                                        10)),
                                            // control: new SwiperControl(),
                                          ) ,
                                        ),
                                        Positioned(
                                          child: Container(
                                            height: 0.05.sw  ,
                                            decoration: const BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.vertical(
                                                top: Radius.circular(40),
                                              ),
                                            ),
                                          ),
                                          bottom: -3,
                                          left: 0,
                                          right: 0,
                                        )                 ,
                                        // Center(
                                        //   child: Padding(
                                        //     padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top,
                                        //         left: 0.04 * ScreenSize().getHeight(context)+ 0.05.sw,
                                        //         right: 0.04 * ScreenSize().getHeight(context)*2 + 0.05.sw +10),
                                        //     child: Text(
                                        //       widget.guildClass.name,
                                        //       maxLines: 1,
                                        //       overflow: TextOverflow.ellipsis,
                                        //       style:TextStyle (
                                        //         fontFamily : 'NotoSansTC',
                                        //         fontSize: 20.nsp,
                                        //         color:  showTitle? OwnColors.black:OwnColors.tran,
                                        //         fontWeight: FontWeight.w700,
                                        //       ),
                                        //
                                        // ),
                                        //       )
                                        //
                                        //
                                        //
                                        //
                                        // )
// Positioned(

                                      ],
                                    );
                                  }
                                )

                            ),
                            SliverToBoxAdapter(
                              child: Column(
                                children: [
                                  Container(
                                    width: double.infinity,
                                    color: OwnColors.white,
                                    child: Column(
                                      children: [

                                        Container(
                                          width: 0.88 *
                                              ScreenSize().getWidth(context),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Flexible(
                                                child: Text(
                                                  guildClass.name,
                                                  key: titleKey,
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis,
                                                  style:GoogleFonts.inter(
                                                    fontSize: 25.nsp,
                                                    color: OwnColors.black,
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(top: 4.0, left:4.0,right: 4),
                                                child: Container(
                                                    height: 0.024 * ScreenSize().getHeight(context),
                                                    child: Image.asset(Res.star)),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(top: 4.0),
                                                child:  Text(guildClass.score,
                                                  style:GoogleFonts.inter(
                                                    fontSize: 20.nsp,
                                                    color: OwnColors.black,
                                                    fontWeight: FontWeight.w600,
                                                  ),),
                                              ),
                                              GestureDetector(
                                                onTap: (){
                                                  goComment(guildClass);
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.only(top: 4.0, left:4.0,right: 4),
                                                  child: Center(
                                                    child:


                                                    Row(
                                                      children: [
                                                        Container(

                                                          child: Text("(" ,
                                                              textAlign: TextAlign.center,
                                                              style:GoogleFonts.inter(
                                                                fontSize: 20.nsp,
                                                                height: 1.2,
                                                                color: OwnColors.black,
                                                                fontWeight: FontWeight.w300,

                                                              )

                                                          ),
                                                        ),
                                                        Container(
                                                          decoration: BoxDecoration(
                                                            border: Border(bottom: BorderSide(width: 0.5, color: OwnColors.black))
                                                          ),
                                                          child: Text( guildClass.comment_num +
                                                              ( int.parse(guildClass.comment_num) > 1 ?
                                                              S().home_rate_number :  S().home_rate_number_single) ,
                                                              textAlign: TextAlign.center,
                                                              style:GoogleFonts.inter(
                                                                fontSize: 16.nsp,
                                                                height: 1.0,
                                                                color: OwnColors.black,
                                                                fontWeight: FontWeight.w300,

                                                              )

                                                          ),
                                                        ),
                                                        Container(

                                                          child: Text(  ")",
                                                              textAlign: TextAlign.center,
                                                              style:GoogleFonts.inter(
                                                                fontSize: 20.nsp,
                                                                height: 1.2,
                                                                color: OwnColors.black,
                                                                fontWeight: FontWeight.w300,

                                                              )

                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        HorizontalSpace().create(context, 0.008),
                                        Container(height: 4,),
                                        Visibility(
                                          visible: guildClass.want_companion == "1"?true:false,
                                          child: Container(
                                            width: 0.88*ScreenSize().getWidth(context),
                                            child: Row(

                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [

                                                Container(
                                                  height: 0.022* ScreenSize().getHeight(context),
                                                  width: 0.046 * ScreenSize().getWidth(context),
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(top:0.0),
                                                    child: Image.asset(Res.icon_companion),
                                                  ),

                                                ),
                                                Flexible(
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(left:7.0),
                                                    child: Text(
                                                      S().find_companion ,

                                                      style:GoogleFonts.inter(
                                                        fontSize: 18.nsp,
                                                        color: OwnColors.CFFAE4E,
                                                        fontWeight: FontWeight.w600,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: 0.88*ScreenSize().getWidth(context),
                                          child: Row(

                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [

                                              Container(
                                                height: 0.022* ScreenSize().getHeight(context),
                                                width: 0.046 * ScreenSize().getWidth(context),
                                                child: Padding(
                                                  padding: const EdgeInsets.only(top:0.0),
                                                  child: Image.asset(Res.icon_global),
                                                ),

                                              ),
                                              Flexible(
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left:7.0),
                                                  child: Text(
                                                    guildClass.lang.join(" | ") ,

                                                      style:GoogleFonts.inter(
                                                        fontSize: 18.nsp,
                                                        color: OwnColors.black,
                                                        fontWeight: FontWeight.w500,
                                                      ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        Visibility(
                                          visible:! guildClass.trans.contains("3"),
                                          child: Container(
                                            width: 0.88*ScreenSize().getWidth(context),
                                            child: Row(

                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [

                                                Container(
                                                  height: 0.022* ScreenSize().getHeight(context),
                                                  width: 0.046 * ScreenSize().getWidth(context),
                                                    child: Image.asset(Res.icon_car),

                                                    // child: Icon(SFSymbols.car_fill, size: 0.044 * ScreenSize().getWidth(context),)
                                                ),
                                                Flexible(
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(left:8.0),
                                                    child: AutoSizeText(
                                                      !guildClass.trans.contains("3") ? S().home_have_car:S().home_have_no_car,

                                                      minFontSize: 1,
                                                      style:GoogleFonts.inter(
                                                        fontSize: 18.nsp,
                                                        color: OwnColors.black,
                                                        fontWeight: FontWeight.w500,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(height: 10,),
                                        Container(
                                          width: 0.88 * ScreenSize().getWidth(context),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Wrap(

                                              spacing: 16,
                                              runSpacing : 5.0,
                                              children: List.generate(
                                                guildClass.location.length,
                                                    (i) {
                                                  return Container(

                                                      decoration: BoxDecoration(
                                                          color: OwnColors.white,
                                                          borderRadius: BorderRadius.only(
                                                              topLeft: Radius.circular(13),
                                                              topRight: Radius.circular(13),
                                                              bottomLeft: Radius.circular(13),
                                                              bottomRight: Radius.circular(13)),
                                                          border: Border.all(
                                                              width: 1,color: OwnColors.CF4F4F4
                                                          ),
                                                          boxShadow: [
                                                            BoxShadow(
                                                              color: OwnColors.primaryText.withOpacity(0.4),
                                                              spreadRadius: 1,
                                                              blurRadius: 4,
                                                              offset: Offset(0, 3), // changes position of shadow
                                                            ),
                                                          ]),
                                                      child: Padding(
                                                        padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                                        child: Text(guildClass.location[i],
                                                          textAlign: TextAlign.center,
                                                          style:GoogleFonts.inter(
                                                            height: 1.2,
                                                            fontSize: 14.nsp,
                                                            color: OwnColors.black,
                                                            fontWeight: FontWeight.w300,
                                                          ),
                                                        ),
                                                      ));
                                                },
                                              ),
                                            ),
                                          ),
                                        ),
                                        HorizontalSpace().create(context, 0.027),

                                        Container(
                                          color: OwnColors.CF4F4F4,
                                          height: 1,
                                          width: 0.92 * ScreenSize().getWidth(context),
                                        ),
                                        HorizontalSpace().create(context, 0.027),
                                        Container(
                                          width: 0.84 *
                                              ScreenSize().getWidth(context),

                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [

                                                    // Align(
                                                    //   alignment: Alignment.centerLeft,
                                                    //   child: AutoSizeText(
                                                    //     S().guilder_abountme,
                                                    //     minFontSize: 1,
                                                    //     style:GoogleFonts.inter(
                                                    //       fontSize: 18.nsp,
                                                    //       color: OwnColors.black,
                                                    //       fontWeight: FontWeight.w600,
                                                    //     ),
                                                    //   ),
                                                    // ),
                                                    LayoutBuilder(
                                                      builder:(context, size) {


                                                        int maxLines = ((0.2.sh / 16.nsp) * 0.8).floor();

                                                        maxLines = maxLines > 0 ? maxLines : 1;
                                                        print ("maxLines " + maxLines.toString());
                                                        var span = TextSpan(
                                                          text: guildClass.description == "null"?
                                                          "":guildClass.description,
                                                          style: TextStyle(fontSize: 16.nsp),
                                                        );

                                                        // Use a textpainter to determine if it will exceed max lines
                                                        var tp = TextPainter(
                                                          maxLines: 6,
                                                          textAlign: TextAlign.left,
                                                          textDirection: TextDirection.ltr,
                                                          text: span,

                                                        );

                                                        // trigger it to layout
                                                        tp.layout(maxWidth: size.maxWidth);

                                                        // whether the text overflowed or not
                                                        var exceeded = tp.didExceedMaxLines;
                                                        print("exceeded" + exceeded.toString());
                                                        return Padding(
                                                          padding: const EdgeInsets.only(left:0.0),
                                                          child: Row(
                                                            mainAxisAlignment: MainAxisAlignment.end,
                                                            crossAxisAlignment: CrossAxisAlignment.end,
                                                            children: [

                                                              Expanded(
                                                                  child:   Container(
                                                                    // height: 0.185.sh,

                                                                    constraints: BoxConstraints(
                                                                      minHeight: 0.2.sh,
                                                                    ),

                                                                    child:
                                                                    SelectableAutoLinkText(
                                                                      guildClass.description == "null"?
                                                                      "":guildClass.description,
                                                                      maxLines: !more ? maxLines:100000,


                                                                      linkStyle: TextStyle(color: Colors.blueAccent,decoration: TextDecoration.underline,),
                                                                      highlightedLinkStyle: TextStyle(
                                                                        color: Colors.blueAccent,
                                                                        backgroundColor: Colors.blueAccent.withAlpha(0x33),
                                                                      ),


                                                                      onTap: (url) => launch(url, forceSafariVC: false),
                                                                      style:GoogleFonts.inter(
                                                                        height: 1.3,
                                                                        fontSize: 16.nsp,
                                                                        color: OwnColors.CC8C8C8,
                                                                        fontWeight: FontWeight.w500,

                                                                      ),
                                                                      callback: _tapMore,
                                                                      moreString: S().guilder_more
                                                                        ,moreBool: true

                                                                    )


                                                                    //
                                                                    // Text(
                                                                    //   guildClass.description == "null"?
                                                                    //   "":guildClass.description,
                                                                    //   overflow: TextOverflow.ellipsis,
                                                                    //   // maxLines: maxLines,
                                                                    //   // maxLines: null,
                                                                    //   maxLines:!more ? maxLines:100000,
                                                                    //   style:GoogleFonts.inter(
                                                                    //     height: 1.3,
                                                                    //     fontSize: 16.nsp,
                                                                    //     color: OwnColors.CC8C8C8,
                                                                    //     fontWeight: FontWeight.w500,
                                                                    //   ),
                                                                    // ),
                                                                  )

                                                              ),

                                                              // Visibility(
                                                              //   visible: !more && exceeded,
                                                              //   child: Padding(
                                                              //     padding: const EdgeInsets.only(left:2.0),
                                                              //     child: GestureDetector(
                                                              //       onTap: (){
                                                              //         more = !more;
                                                              //         setState(() {
                                                              //
                                                              //         });
                                                              //         Future.delayed(Duration(milliseconds: 100), () {
                                                              //           // 5s over, navigate to a new page
                                                              //           _controller.animateTo(_controller.position.maxScrollExtent,duration: Duration(milliseconds: 400),    curve: Curves.fastOutSlowIn,);
                                                              //         });
                                                              //       },
                                                              //       child: Text(
                                                              //         S().guilder_more,
                                                              //         style:GoogleFonts.inter(
                                                              //           fontSize: 18.nsp,
                                                              //           color: OwnColors.black,
                                                              //           fontWeight: FontWeight.w600,
                                                              //         ),
                                                              //       ),
                                                              //     ),
                                                              //   ),
                                                              // ),
                                                              // Visibility(
                                                              //   child:
                                                              //   VerticalSpace().create(context, 0.08),
                                                              // )
                                                            ],
                                                          ),
                                                        );
                                                      }
                                                    ) ,
                                                  ],
                                                ),
                                              ),
                                              Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  // HorizontalSpace().create(context, 0.03),
                                                  GestureDetector(
                                                    onTap:(){
                                                      goMess();
                                                    },
                                                    child: Column(
                                                      children: [
                                                        Container(height: 0.035.sh,child: Icon(SFSymbols.ellipses_bubble)),
                                                        Text(
                                                          S().guilder_mess,
                                                          style:GoogleFonts.inter(
                                                            fontSize: 15.nsp,
                                                            color: OwnColors.black,
                                                            fontWeight: FontWeight.w300,
                                                          ),
                                                        )
                                                      ],),
                                                  ),
                                                  HorizontalSpace().create(context, 0.01),
                                                  GestureDetector(
                                                    onTap:(){
                                                      goComment(guildClass);
                                                    },
                                                    child: Column(

                                                      children: [
                                                        Container(height: 0.035.sh,
                                                            // child: Image.asset(Res.guilder_comment)
                                                            child: Icon(SFSymbols.square_pencil)
                                                        ),
                                                        Text(
                                                          S().guilder_comment,
                                                          style:GoogleFonts.inter(
                                                            fontSize: 15.nsp,
                                                            color: OwnColors.black,
                                                            fontWeight: FontWeight.w300,
                                                          ),
                                                        )
                                                      ],),
                                                  )

                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                        HorizontalSpace().create(context, 0.027),
                                        HorizontalSpace().create(context, 0.02),
                                        HorizontalSpace().create(context, 0.09),
                                        HorizontalSpace().create(context, 0.02),

                                        // Container(
                                        //   color: OwnColors.CC8C8C8,
                                        //   height: 1,
                                        //   width: 0.92 * ScreenSize().getWidth(context),
                                        // ),
                                        // HorizontalSpace().create(context, 0.024),
                                        // Container(
                                        //   height :0.054.sh,
                                        //   width: 0.315.sw,
                                        //   child: FlatButton(
                                        //     color: OwnColors.CTourOrange,
                                        //     splashColor: Colors.transparent,
                                        //     highlightColor: Colors.transparent,
                                        //     textColor: OwnColors.white,
                                        //     // elevation: 10,
                                        //     child: Padding(
                                        //       padding: const EdgeInsets.all(8.0),
                                        //       child: AutoSizeText(
                                        //         S().guilder_travel,
                                        //         minFontSize: 10,
                                        //         maxFontSize: 30,
                                        //         style:GoogleFonts.inter(
                                        //           fontSize: 18.nsp,
                                        //           color: OwnColors.white,
                                        //           fontWeight: FontWeight.w600,
                                        //         ),
                                        //         maxLines: 1,
                                        //       ),
                                        //     ),
                                        //     shape: RoundedRectangleBorder(
                                        //       borderRadius: BorderRadius.circular(20.0),
                                        //       // side: BorderSide(color: Colors.red)
                                        //     ),
                                        //     onPressed: goTravel,
                                        //   ),
                                        // ),
                                        // HorizontalSpace().create(context, 0.024),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),





                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
              child: Container(
                height: 0.07 * ScreenSize().getHeight(context),
                child: Center(
                  child: Container(
                    width: 0.92 * ScreenSize().getWidth(context),
                    child: Row(
                      children: [
                        ClipOval(
                          child: Material(
                            color: Colors.white, // button color
                            child: GestureDetector(

                              child: SizedBox(
                                  width: 0.04 .sh,
                                  height:
                                  0.04 .sh,
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(bottom: 0.0,left: 0.0,right: 0.0),
                                      // child: Icon(Icons.chevron_left_rounded, color: OwnColors.black,size:  0.04.sh,),
                                      // child: Center(child: Icon(Ionicons.chevron_back_outline, color: OwnColors.black,size:  0.03.sh,)),
                                      child: Center(child: Icon(LineIcons.angleLeft, color: OwnColors.black,size:  0.03.sh,)),

                                    ),
                                  )),
                              onTap: () {
                                backClick();
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left:8.0,right: 8),
                            child: Text(
                              guildClass.name,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: GoogleFonts.inter(
                                color: showTitle? OwnColors.black:
                                OwnColors.tran,
                                fontSize: 20.nsp,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ),
                        ClipOval(
                          child: Material(
                            color: Colors.white, // button color
                            child: GestureDetector(

                              child: SizedBox(
                                  width: 0.043 * ScreenSize().getHeight(context),
                                  height:
                                  0.043 * ScreenSize().getHeight(context),
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(bottom: 3,left: 0,right: 0),
                                      child: Icon(SFSymbols.square_arrow_up,size:  0.03.sh,),
                                    ),
                                  )),
                              onTap: () async {

                                await FlutterShare.share(
                                    title: S().travel_share_title,
                                    text: guildClass.name,
                                    linkUrl: link,
                                    chooserTitle: ''
                                );

                              },
                            ),
                          ),
                        ),
                        Container(
                          width: 10,
                        ),
                        ClipOval(
                          child: Material(
                            color: Colors.white, // button color
                            child: GestureDetector(

                              child: SizedBox(
                                  width: 0.043 .sh,
                                  height:
                                  0.043.sh,
                                  child: Center(
                                    child: Padding(
                                        padding: const EdgeInsets.only(top:0.0, left: 4.0,right: 4.0),
                                        child:
                                        Image.asset(Res.icon_menu)
                                      // Icon(SFSymbols.menu, color: OwnColors.black,size: 0.03.sh,)
                                      // Image.asset(Res.black_heart):。
                                      // Image.asset(Res.collected_heart)
                                    ),
                                  )),
                              onTap: () {
                                showMenu();
                              },
                            ),
                          ),
                        )

                        // ClipOval(
                        //   child: Material(
                        //     color: Colors.white, // button color
                        //     child: GestureDetector(
                        //
                        //       child: SizedBox(
                        //           width: 0.043 * ScreenSize().getHeight(context),
                        //           height:
                        //           0.043 * ScreenSize().getHeight(context),
                        //           child: Center(
                        //             child: Padding(
                        //                 padding: const EdgeInsets.only(top:0.0, left: 0,right: 0.0),
                        //               //child: Image.asset(Res.collected_heart),
                        //               child: guildClass.is_collect.contains("0")? Icon(SFSymbols.heart, color: OwnColors.black,size:  0.03.sh,):
                        //               Icon(SFSymbols.heart_fill, color: OwnColors.CTourOrange,size:  0.03.sh,)
                        //             ),
                        //           )),
                        //       onTap: () {
                        //         sendFavorite(guildClass);
                        //       },
                        //     ),
                        //   ),
                        // )
                      ],
                    ),
                  ),
                ),
              ),
            ),

            // Positioned(
            //   right: 0.08.sw,
            //   bottom: 0.14.sh,
            //   child: Column(
            //     mainAxisAlignment: MainAxisAlignment.start,
            //     crossAxisAlignment: CrossAxisAlignment.start,
            //     children: [
            //
            //       Container(
            //         decoration: BoxDecoration(
            //           color: OwnColors.white,
            //             borderRadius:
            //             BorderRadius.all(Radius.circular(8)),),
            //         child: GestureDetector(
            //           onTap:(){
            //             goMess();
            //           },
            //           child: Column(
            //             children: [
            //               Container(
            //                   height: 0.035.sh,
            //                   child: Icon(SFSymbols.ellipses_bubble)//Image.asset(Res.guilder_mes)
            //               ),
            //               Text(
            //                 S().guilder_mess ,
            //                 style: GoogleFonts.inter(
            //                   fontSize: 15.nsp,
            //                   fontWeight: FontWeight.w400
            //                 ),
            //               )
            //             ],),
            //         ),
            //       ),
            //       HorizontalSpace().create(context, 0.02),
            //       Container(
            //         decoration: BoxDecoration(
            //           color: OwnColors.white,
            //           borderRadius:
            //           BorderRadius.all(Radius.circular(8)),),
            //         child: GestureDetector(
            //           onTap:(){
            //             goComment(guildClass);
            //           },
            //           child: Column(
            //             children: [
            //               Container(height: 0.035.sh,
            //                   // child: Image.asset(Res.guilder_comment)
            //                   child:Icon(SFSymbols.square_pencil)
            //               ),
            //               Text(
            //                 S().guilder_comment,
            //                 style:GoogleFonts.inter(
            //                     fontSize: 15.nsp,
            //                     fontWeight: FontWeight.w400
            //                 ),
            //               )
            //             ],),
            //         ),
            //       )
            //
            //     ],
            //   ),
            // ),
            Positioned(
                bottom: 0,child: Container(
              color: OwnColors.white,
              width: 1.sw,
                  child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
              Container(
                  color: OwnColors.CF4F4F4,
                  height: 1,
                  width: 0.92 * ScreenSize().getWidth(context),
              ),
              HorizontalSpace().create(context, 0.024),
              Container(
                  height :0.054.sh,
                  width: 0.315.sw,
                  child: FlatButton(
                    color: OwnColors.CTourOrange,
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    textColor: OwnColors.white,
                    // elevation: 10,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: AutoSizeText(
                        S().guilder_travel,
                        minFontSize: 10,
                        maxFontSize: 30,
                        style:GoogleFonts.inter(
                          fontSize: 18.nsp,
                          color: OwnColors.white,
                          fontWeight: FontWeight.w600,
                        ),
                        maxLines: 1,
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(28.0),
                      // side: BorderSide(color: Colors.red)
                    ),
                    onPressed: goTravel,
                  ),
              ),
              HorizontalSpace().create(context, 0.024),
            ],),
                ))
          ],
        ));
  }


  Future<void> showMenu() async {
    Tools().dismissK(context);
    int result = await showCupertinoModalBottomSheet(
        topRadius: Radius.circular(30),
        barrierColor:Colors.black54,
        expand: false,
        context: context,
        enableDrag: true,
        backgroundColor: Colors.transparent,
        builder: (context) => GestureDetector(
          onTap:(){
            Tools().dismissK(context);
            print("onTap");
          },
          child: Material(
              color: OwnColors.white,
              child: Container(
                // height:  ScreenSize().getHeightWTop(context),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[

                    Container(
                        height: 0.007 * ScreenSize().getHeight(context),
                        width: 0.2 * ScreenSize().getWidth(context),
                        decoration: const BoxDecoration(
                          color: OwnColors.white,
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        )),
                    HorizontalSpace().create(context, 0.01),
                    Stack(
                      children: [
                        Container(
                          width: 0.88.sw,
                          child: Row(
                            children: [
                              SizedBox(
                                  width: 0.043 .sh,
                                  height:
                                  0.043.sh,
                                  child: Center(
                                    child: Padding(
                                        padding: const EdgeInsets.only(top:0.0, left: 0.0,right: 0.0),
                                        child: guildClass.is_collect == "0" ?
                                        Icon(SFSymbols.heart, color: OwnColors.black,size: 0.03.sh,):
                                        Icon(SFSymbols.heart_fill, color: OwnColors.CTourOrange,size:  0.03.sh,)
                                      // Image.asset(Res.black_heart):
                                      // Image.asset(Res.collected_heart)
                                    ),
                                  )),
                              VerticalSpace().create(context, 0.01),
                              Text(
                                S().collect,
                                style: GoogleFonts.inter(
                                  fontSize: 16.nsp,
                                  color: OwnColors.black,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Positioned.fill(child: InkWell(
                          splashColor: OwnColors.transparent,
                          onTap: (){
                            sendFavorite(guildClass);
                            backClick();
                          },
                        ))
                      ],
                    ),
                    HorizontalSpace().create(context, 0.02),
                    Stack(
                      children: [
                        Container(
                          width: 0.88.sw,
                          child: Row(
                            children: [

                              SizedBox(
                                  width: 0.043 .sh,
                                  height: 0.043 .sh,
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 0.0,left: 0,right: 0),
                                    child: Center(child: Icon(SFSymbols.square_arrow_up ,size: 0.03.sh,)),
                                  )),
                              VerticalSpace().create(context, 0.01),
                              Text(
                                S().share,
                                style: GoogleFonts.inter(
                                  fontSize: 16.nsp,
                                  color: OwnColors.black,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Positioned.fill(child: InkWell(
                          splashColor: OwnColors.transparent,
                          onTap: () async {
                            await FlutterShare.share(
                                title: S().travel_share_title,
                                text: guildClass.name,
                                linkUrl: link,
                                chooserTitle: ''
                            );
                            backClick();
                          },
                        ))
                      ],
                    ),
                    HorizontalSpace().create(context, 0.02),
                    Stack(
                      children: [
                        Container(
                          width: 0.88.sw,
                          child: Row(
                            children: [

                              SizedBox(
                                  width: 0.043 .sh,
                                  height: 0.043 .sh,
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 0.0,left: 0,right: 0),
                                    child: Center(child:SizedBox(height: 0.03.sh,child: Image.asset(Res.icon_report))),
                                  )),
                              VerticalSpace().create(context, 0.01),
                              Text(
                                S().report,
                                style: GoogleFonts.inter(
                                  fontSize: 16.nsp,
                                  color: OwnColors.CFE3040,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Positioned.fill(child: InkWell(
                          splashColor: OwnColors.transparent,
                          onTap: () {
                            print("report");
                            backClick();
                            PushNewScreen().normalPush(context,ReportPage(type: 1,) );

                          },
                        ))
                      ],
                    ),

                    HorizontalSpace().create(context, 0.02),
                    Stack(
                      children: [
                        Container(
                          width: 0.88.sw,
                          child: Row(
                            children: [

                              SizedBox(
                                  width: 0.043 .sh,
                                  height: 0.043 .sh,
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 0.0,left: 0,right: 0),
                                    child: Center(child:SizedBox(height: 0.03.sh,child: Image.asset(Res.icon_block))),
                                  )),
                              VerticalSpace().create(context, 0.01),
                              Text(
                                S().block,
                                style: GoogleFonts.inter(
                                  fontSize: 16.nsp,
                                  color: OwnColors.CFE3040,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Positioned.fill(child: InkWell(
                          splashColor: OwnColors.transparent,
                          onTap: () {
                            print("block");
                            sendBlockWarning();
                            // backClick();
                            // PushNewScreen().normalPush(context,ReportPage(type: 1,) );

                          },
                        ))
                      ],
                    ),

                    HorizontalSpace().create(context, 0.04),

                  ],
                ),
              )),
        )
    ).whenComplete(() {});


  }

  void backClick() {
    print("back");

    GlobalData.curernt_guilder = widget.guildClass;
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }

  void goMess(){
    print("goMess");
    PushNewScreen().normalPush(context, ChattingPage(chat_title: widget.guildClass.name,chat_avatar: widget.guildClass.avatar,send_to_id: widget.guildClass.id,send_to_fcm: "",));
  }
  void goTravel(){
    print("goTravel");
    PushNewScreen().normalPush(context, GuilderTravelPage(guildClass:guildClass));
  }
  void goComment(GuildClass guildClass){
    print("goComment");
    PushNewScreen().normalPush(context, GuildReviewPage(guildClass: guildClass,));
  }

  void sendFavorite(GuildClass guildClass) async {
    // Loading.show(context);
    ResultData resultData = await AppApi.getInstance().setCollect(context, true,  SharePre.prefs.getString(GDefine.user_id),guildClass.id);
    // Loading.dismiss(context);
    if (resultData.isSuccess()) {
      if(guildClass.is_collect.contains("1")){
        guildClass.is_collect = "0";
      }else{
        guildClass.is_collect = "1";
      }
      setState(() {

      });
    } else {
      WarningDialog.showIOSAlertDialog(context, S().send_like_fail_title,S().send_like_fail_hint,S().confirm);
    }
  }



  void sendBlockWarning(){
    WarningDialog.showIOSAlertDialog2(context, S().block_title, S().block_hint, S().confirm, sendBlock);
  }
  void sendBlock( ) async {


    Loading.show(context);
    ResultData resultData = await AppApi.getInstance().setUserBlock(context, true,  widget.guildClass.id);
    await getBlockList();
    Loading.dismiss(context);
    backClick();
    backClick();

  }

  void getBlockList( ) async {
    print("getBlockList");


    ResultData resultData = await AppApi.getInstance()
        .getUserBlockList(context, true);

    print(resultData.data.toString());
    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      GDefine.blockUserId.clear();
      for (Map<String, dynamic> a in resultData.data) {
        if(!GDefine.blockUserId.contains(a ["block_user_id"].toString())) {
          GDefine.blockUserId.add(a ["block_user_id"].toString());
        }
      }

    }
  }

  void _tapMore(bool m){
    print ("_tapMore " + m.toString());
    more = m;
    setState(() {

    });
  }


  Future<void> getGuild() async {
    print("getGuild");
    Loading.show(context);
    ResultData resultData = await AppApi.getInstance().getTourist(context, true, SharePre.prefs.getString(GDefine.user_id), widget.id);
    Loading.dismiss(context);
    if(resultData.isSuccess()) {
      if (resultData.data['score'] == null) {
        resultData.data['score'] = "0.0";
        guildClass = GuildClass.fromJson(resultData.data);
        print("001");
        if(!widget.preview ) {
          imageList.add(guildClass.avatar);
          for (dynamic c in guildClass.tourist_img) {
            if (c.toString().contains("http")) {
              imageList.add(c.toString());
            }
          }
        }
      } else {

        guildClass = GuildClass.fromJson(resultData.data);
        if(!widget.preview ) {
          imageList.add(guildClass.avatar);
          print("002");
          for (dynamic c in guildClass.tourist_img) {
            if (c.toString().contains("http")) {
              imageList.add(c.toString());
            }
          }
        }
      }
    }else {
      WarningDialog.showIOSAlertDialogWithFunc(context, S().travel_guilder_not_found_title, S().travel_guilder_not_found_hint, S().confirm,backClick);
    }
    widget.guildClass = guildClass;
    link =guildClass.id;


    setState(() {});
  }
  Future<void> getGuildScore() async {
    print("getGuildScore");

    GuildClass g ;
    Loading.show(context);
    ResultData resultData = await AppApi.getInstance().getTourist(context, true, SharePre.prefs.getString(GDefine.user_id), widget.id);
    Loading.dismiss(context);
    if(resultData.isSuccess()) {
      if (resultData.data['score'] == null) {
        resultData.data['score'] = "0.0";
        g = GuildClass.fromJson(resultData.data);
        print("001");
        imageList.add(guildClass.avatar);
        for (dynamic c in guildClass.tourist_img) {
          if (c.toString().contains("http")) {
            imageList.add(c.toString());
          }
        }
      } else {

        g = GuildClass.fromJson(resultData.data);
        imageList.add(guildClass.avatar);
        print("002");
        for (dynamic c in guildClass.tourist_img) {
          if (c.toString().contains("http")) {
            imageList.add(c.toString());
          }
        }
      }
    }else {
      WarningDialog.showIOSAlertDialogWithFunc(context, S().travel_guilder_not_found_title, S().travel_guilder_not_found_hint, S().confirm,backClick);
    }
    guildClass.score = g.score;
    guildClass.comment_num = g.comment_num;

    setState(() {});
  }



}
