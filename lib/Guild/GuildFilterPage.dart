
import 'dart:async';
import 'dart:ffi';
import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Personal/BecomeGuilderPage.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/FilterData.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_star/custom_rating.dart';
import 'package:flutter_star/flutter_star.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_button/group_button.dart';


import 'package:intl/intl.dart';

import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

class GuildFilterPage extends StatefulWidget {
  GuildFilterPage({Key key}) : super(key: key);

  @override
  _GuildFilterPageState createState() => _GuildFilterPageState();
}

class _GuildFilterPageState extends State<GuildFilterPage> {

  bool isClean = false;

  List<bool> lansSelList = FilterData.guild_lansSelList;
  List<String> trafficList = [S().guild_traffic1,S().guild_traffic2, S().guild_traffic3];
  List<String> genderList = [S().guild_gender1,S().guild_gender2, S().guild_gender3];

  String traffic = S().guild_traffic1;
  String gender = FilterData.guild_gender;
  // double duration = 3;
  bool transBool = FilterData.guild_trans;
  GroupButtonController _checkboxesController;
  GroupButtonController lansController;
  List<bool> transSelList = FilterData.guild_transSelList;
  SfRangeValues duration = SfRangeValues(FilterData.guild_startNum, FilterData.guild_endNum == -1? 1:FilterData.guild_endNum);
  //num rating = 0;
  String startNum = "0", endNum = "50";
  bool wantCompanion = false;
  @override
  Future<void> initState() {
    super.initState();
    _checkboxesController = GroupButtonController(

      // onDisablePressed: (index) => ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(content: Text('${_checkboxButtons[index]} is disabled')),
      // ),
    );
    lansController = GroupButtonController(

      // onDisablePressed: (index) => ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(content: Text('${_checkboxButtons[index]} is disabled')),
      // ),
    );
    if(SharePre.prefs.getInt(GDefine.commentMax)!=null) {
      endNum = SharePre.prefs.getInt(GDefine.commentMax).toString();
    }


    if(FilterData.guild_endNum > double.parse(endNum)){
      duration = SfRangeValues(FilterData.guild_startNum,double.parse(endNum));
      FilterData.guild_endNum = double.parse(endNum);
    }

    if(FilterData.guild_startNum> double.parse(endNum)){
      duration = SfRangeValues(0.0,FilterData.guild_endNum);
      FilterData.guild_startNum = 0.0;
    }




    if(FilterData.guild_want == 1){
      wantCompanion = true;
      transBool = wantCompanion;

      FilterData.guild_want = wantCompanion ? 1 : 0 ;
      if(transBool == false){
        _checkboxesController.unselectAll();
        _checkboxesController.disableIndexes([0,1,2]);
      }else {
        _checkboxesController.enableIndexes([0,1,2]);
        // _checkboxesController.selectIndex(0);
        // transSelList[0] = true;
      }
    }else {
      wantCompanion = false;
      transBool = wantCompanion;
      FilterData.guild_want = wantCompanion ? 1 : 0 ;
      if(transBool == false){
        _checkboxesController.unselectAll();
        _checkboxesController.disableIndexes([0,1,2]);
      }else {
        _checkboxesController.enableIndexes([0,1,2]);
        _checkboxesController.selectIndex(0);
        transSelList[0] = true;
      }
    }
    // List<String> lang = SharePre.getString(GDefine.lang).toString().split(",") ;
    // print(lang);
    for (int idex = 0 ; idex<FilterData.guild_lansSelList.length; idex++){
      if(FilterData.guild_lansSelList[idex]){
        lansController.selectIndex(idex);
      }
    }

    print("FilterData.guild_transSelList " + FilterData.guild_transSelList.toString());
    if(transBool!=null){
      if(transBool){
        _checkboxesController.enableIndexes([0,1,2]);
        for (int idex = 0 ; idex<FilterData.guild_transSelList.length; idex++){
          if(FilterData.guild_transSelList[idex]){
            _checkboxesController.selectIndex(idex);
          }
        }

      }else {
        _checkboxesController.disableIndexes  ([0,1,2]);
      }
    }

  }

  
  @override
  Widget build(BuildContext context) {
    return Material(
        color: OwnColors.white,
        child: Container(
          height:  ScreenSize().getHeightWTop(context),
          child: Column(
            children: <Widget>[

              Container(
                  height: 0.007 * ScreenSize().getHeight(context),
                  width: 0.2 * ScreenSize().getWidth(context),
                  decoration: const BoxDecoration(
                    color: OwnColors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  )),
              HorizontalSpace().create(context, 0.02),
              Topbar().filterCleanCreate(context, backClick, cleanClick, filterClick),
              // Container(
              //   height: 0.038*ScreenSize().getHeight(context),
              //   width:  0.9*ScreenSize().getWidth(context),
              //   child: Row(
              //     children: [
              //       GestureDetector(onTap: (){
              //         Navigator.of(context).pop();
              //       },child: Container(height: 0.02*ScreenSize().getHeight(context),child: Image.asset(Res.cross))),
              //       Spacer(
              //         flex: 1,
              //       ),
              //       GestureDetector(
              //           onTap: (){
              //             cleanClick();
              //           },
              //         child: AutoSizeText(
              //           S().home_filter_clean,
              //           style: TextStyle(
              //             color: OwnColors.black,
              //             fontSize: 16.nsp,
              //             fontWeight: FontWeight.bold,decoration: TextDecoration.underline
              //           ),
              //         ),
              //       ),
              //       VerticalSpace().create(context, 0.03),
              //       GestureDetector(
              //         onTap: (){
              //           filterClick();
              //         },
              //         child: Container(
              //           decoration: BoxDecoration(
              //             color: OwnColors.CTourOrange,
              //               borderRadius:
              //               BorderRadius.all(Radius.circular(16)),),
              //           child: Padding(
              //             padding: const EdgeInsets.only(top:2.0,left: 10,right: 10,bottom: 2),
              //             child: AutoSizeText(
              //               S().home_filter_filter,
              //               style: TextStyle(
              //                 color: OwnColors.white,
              //                 fontSize: 16.nsp,
              //                   fontWeight: FontWeight.bold
              //               ),
              //             ),
              //           ),
              //         ),
              //       )
              //     ],
              //   ),
              // ),
              // HorizontalSpace().create(context, 0.02),
              // Container(
              //   height:1,
              //   width:1 * ScreenSize().getWidth(context),
              //   color: OwnColors.CF4F4F4,
              //
              // ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      HorizontalSpace().create(context, 0.02),
                      Container(
                        // height: 0.023 * ScreenSize().getHeight(context),
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: AutoSizeText(
                          S().guild_rate,
                          textScaleFactor: 1.0,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.sp,
                              color: OwnColors.black),
                        ),
                      ),
                      HorizontalSpace().create(context, 0.005),
                      Container(
                        height: 0.04.sh,
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: Row(
                          children: <Widget>[
                            RatingBar.builder(
                              initialRating: double.parse(FilterData.guild_rating.toString()),
                              minRating: 0,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              glow:false,
                              itemSize: 0.04.sh,
                              rateValue: double.parse(FilterData.guild_rating.toString()),

                              itemPadding: EdgeInsets.symmetric(horizontal: 1.0),
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: OwnColors.CTourOrange,
                              ),
                              onRatingUpdate: (r) {
                                print("onRatingUpdate " + r.toString());
                                FilterData.guild_rating = num.parse(r.toString());
                              },
                            ),
                          ],
                        ),
                      ),
                      HorizontalSpace().create(context, 0.02),
                      Container(
                        height: 1,
                        width: 0.9 * ScreenSize().getWidth(context),
                        color: OwnColors.CF4F4F4,
                      ),
                      HorizontalSpace().create(context, 0.02),
                      Container(
                        // height: 0.023 * ScreenSize().getHeight(context),
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: AutoSizeText(
                          S().comment_num_title,
                          textScaleFactor: 1.0,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.sp,
                              color: OwnColors.black),
                        ),
                      ),
                      HorizontalSpace().create(context, 0.02),

                      Container(
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [

                            AutoSizeText(double.parse(
                                duration.start.toString()).toStringAsFixed(0) +

                                (duration.start > 1? S().guild_comment_num:S().guild_comment_num_single),

                              style: TextStyle(
                                fontSize: 13.nsp,color: OwnColors.black
                            ),),
                            AutoSizeText(double.parse(
                                duration.end.toString()).toStringAsFixed(0) +

                                (duration.end > 1? S().guild_comment_num:S().guild_comment_num_single),

                              style: TextStyle(
                                fontSize: 13.nsp,color: OwnColors.black
                            ),)
                          ],),
                      ),
                      HorizontalSpace().create(context, 0.02),
                      Container(
                        width: 0.9 * ScreenSize().getWidth(context),

                        child: SfRangeSliderTheme(
                          data: SfRangeSliderThemeData(
                            overlayRadius : 0.0,
                            thumbColor:  OwnColors.white,
                            thumbStrokeWidth:  1.0,
                            activeTrackHeight: 2.0,
                            inactiveTrackHeight: 2.0,
                            thumbStrokeColor: OwnColors.CC8C8C8,
                            activeTrackColor: OwnColors.CTourOrange,
                            inactiveTrackColor: OwnColors.CC8C8C8,
                          ),
                          child: SfRangeSlider(

                            min: 0.0,
                            max: double.parse(endNum),
                            // activeColor: OwnColors.CTourOrange,
                            // inactiveColor: OwnColors.CC8C8C8,

                            values: duration,
                            stepSize: 1.0,



                            // enableTooltip: true,
                            minorTicksPerInterval: 1,

                            onChanged: (SfRangeValues  newValue){
                              setState(() {
                                print("newValue.start.toString() " + newValue.start.toString());
                                print("newValue.start.toString() " + (newValue.start.round()).toString() );
                                duration = newValue;
                                FilterData.guild_startNum = double.parse ((newValue.start.round()).toString() );
                                FilterData.guild_endNum =double.parse ((newValue.end.round()).toString() );
                                print("duration : " + newValue.start.toString());
                              });
                            },
                          ),
                          // child: SfSlider(
                          //
                          //
                          //       min: 0.0,
                          //       max: 1300.0,
                          //       value: lightTime,
                          //       interval: 20,
                          //       showTicks: false,
                          //       showLabels: false,
                          //       enableTooltip: false,
                          //       activeColor:OwnColors.BtnDeepBlue,
                          //       inactiveColor: OwnColors.EBEBEB,
                          //       minorTicksPerInterval: 1,
                          //       onChanged: (dynamic value){
                          //         setState(() {
                          //           lightTime = value;
                          //         });}),
                        ),

                      ),
                      HorizontalSpace().create(context, 0.02),
                      Container(
                        height: 1,
                        width: 0.9 * ScreenSize().getWidth(context),
                        color: OwnColors.CF4F4F4,
                      ),
                      HorizontalSpace().create(context, 0.02),
                      Container(
                        // height: 0.023 * ScreenSize().getHeight(context),
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              S().personal_become_guilder_want_companion,
                              style: GoogleFonts.inter(
                                  fontWeight: FontWeight.bold,
                                  color: OwnColors.black,
                                  fontSize: 16.nsp),
                            ),
                            Transform.scale(
                              scale: 0.8,
                              child: CupertinoSwitch(
                                value: wantCompanion,
                                activeColor: OwnColors.CFF644E,
                                onChanged: (bool value) {

                                  setState(() {
                                    wantCompanion = value;
                                    transBool = wantCompanion;

                                    FilterData.guild_want = wantCompanion ? 1 : 0 ;
                                    if(transBool == false){
                                      _checkboxesController.unselectAll();
                                      _checkboxesController.disableIndexes([0,1,2]);
                                    }else {
                                      _checkboxesController.enableIndexes([0,1,2]);
                                      _checkboxesController.selectIndex(0);
                                      transSelList[0] = true;
                                    }
                                  });
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        // height: 0.023 * ScreenSize().getHeight(context),
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: Padding(
                          padding:   EdgeInsets.only(left:0.09.sw,bottom: 8,top: 4),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                S().gilder_filter_looking_companion_traffic,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: wantCompanion ?  OwnColors.black : OwnColors.C989898,
                                    fontSize: 16.nsp),
                              ),
                              // Transform.scale(
                              //   scale: 0.8,
                              //   child: CupertinoSwitch(
                              //     value: transBool,
                              //     activeColor: OwnColors.CFF644E,
                              //     onChanged: (bool value) {
                              //       setState(() {
                              //         transBool = value;
                              //         FilterData.guild_trans = value;
                              //         if(transBool == false){
                              //           _checkboxesController.unselectAll();
                              //           _checkboxesController.disableIndexes([0,1,2]);
                              //         }else {
                              //           _checkboxesController.enableIndexes([0,1,2]);
                              //           _checkboxesController.selectIndex(0);
                              //           transSelList[0] = true;
                              //         }
                              //       });
                              //     },
                              //   ),
                              // )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        // height: 0.023 * ScreenSize().getHeight(context),
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: Padding(
                          padding:   EdgeInsets.only(right: 8,left: 0.09.sw),
                          child: GroupButton(
                            controller: _checkboxesController,
                            isRadio: false,
                            onSelected: (val,index, isSelected) {
                              print("index : " + index.toString());
                            },

                            buttons: trafficList,
                            buttonIndexedBuilder: (selected, index, context) {
                              return
                                CheckBoxTile(
                                  title: trafficList[index],
                                  selected: selected,
                                  canSelected: transBool,
                                  onTap: () {
                                    print("onTap");
                                    if (!selected) {
                                      _checkboxesController.selectIndex(index);
                                      transSelList[index] = true;
                                      FilterData.guild_transSelList[index] = true;
                                      return;
                                    }else{
                                      int check = 0;
                                      // for(bool value in transSelList){
                                      //   if(value){
                                      //     check++;
                                      //   }
                                      // }
                                      // if(check > 1){
                                        transSelList[index] = false;
                                        _checkboxesController.unselectIndex(index);
                                        FilterData.guild_transSelList[index] = false;
                                      // }

                                    }
                                    print(FilterData.guild_transSelList.toString());
                                  },
                                );
                            },
                            options: GroupButtonOptions(
                              // selectedShadow: const [],
                              // selectedTextStyle: TextStyle(
                              //   fontSize: 20,
                              //   color: OwnColors.white,
                              // ),
                              // selectedColor: OwnColors.CF39800,
                              // unselectedShadow: const [],
                              // unselectedColor: OwnColors.white,
                              // unselectedTextStyle: TextStyle(
                              //   fontSize: 20,
                              //   color: OwnColors.black_a48,
                              // ),
                              // selectedBorderColor: OwnColors.CFFD796,
                              // unselectedBorderColor: OwnColors.black_a12,
                              borderRadius: BorderRadius.circular(8),
                              spacing: 0,
                              runSpacing: 0,
                              groupingType: GroupingType.column,
                              direction: Axis.horizontal,
                              buttonHeight: 40,
                              buttonWidth: 60,
                              mainGroupAlignment: MainGroupAlignment.start,
                              crossGroupAlignment: CrossGroupAlignment.center,
                              groupRunAlignment: GroupRunAlignment.center,
                              textAlign: TextAlign.center,
                              textPadding: EdgeInsets.only(left: 8,right: 8),
                              alignment: Alignment.center,
                              elevation: 0,
                            ),
                          ),
                        ),
                      ),
                      HorizontalSpace().create(context, 0.02),
                      Container(
                        height: 1,
                        width: 0.9 * ScreenSize().getWidth(context),
                        color: OwnColors.CF4F4F4,
                      ),
                      HorizontalSpace().create(context, 0.02),
                      Container(
                        // height: 0.023 * ScreenSize().getHeight(context),
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: AutoSizeText(
                          S().guild_gender,
                          textScaleFactor: 1.0,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.sp,
                              color: OwnColors.black),
                        ),
                      ),
                      HorizontalSpace().create(context, 0.005),
                      Container(
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: genderList.length,
                            physics: NeverScrollableScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding:
                                EdgeInsets.only(left: 0.0, right: 8),
                                child: radioItem(
                                    genderList[index],gender,updateGender
                                ),
                              );
                            }),
                      ),
                      HorizontalSpace().create(context, 0.02),
                      Container(
                        height: 1,
                        width: 0.9 * ScreenSize().getWidth(context),
                        color: OwnColors.CF4F4F4,
                      ),
                      HorizontalSpace().create(context, 0.02),
                      Container(
                        // height: 0.023 * ScreenSize().getHeight(context),
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: AutoSizeText(
                          S().guild_language,
                          textScaleFactor: 1.0,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.sp,
                              color: OwnColors.black),
                        ),
                      ),
                      HorizontalSpace().create(context, 0.02),
                      Container(
                        width: 0.9 * ScreenSize().getWidth(context),
                        child: GroupButton(
                          controller: lansController,
                          isRadio: false,
                          onSelected: (val,index, isSelected) {
                            if(FilterData.guild_lansSelList[index]){
                              lansController.unselectIndex(index);
                              lansSelList[index] = false;
                              FilterData.guild_lansSelList[index] = false;
                            }else{
                              lansController.selectIndex(index);
                              lansSelList[index] = true;
                              FilterData.guild_lansSelList[index] = true;
                            }
                            print("onSelected");
                            setState(() {

                            });
                          },

                          buttons: FilterData.lansList,
                          buttonIndexedBuilder: (selected, index, context) {
                            return  selected?Padding(
                              padding: const EdgeInsets.only(left:8.0,right: 8),
                              child: Container(

                                  decoration: BoxDecoration(
                                      color: OwnColors.white,
                                      border: Border.all(color: OwnColors.CC8C8C8,width:0.5),
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(12),
                                          topRight: Radius.circular(12),
                                          bottomLeft: Radius.circular(12),
                                          bottomRight: Radius.circular(12)),
                                      boxShadow: [
                                        BoxShadow(
                                          color: OwnColors.primaryText.withOpacity(0.4),
                                          spreadRadius: 1,
                                          blurRadius: 4,
                                          offset: Offset(0, 3), // changes position of shadow
                                        ),
                                      ]),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                    child: Text(FilterData.lansList[index] + "",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          height: 1.3,
                                          fontSize: 14.nsp,
                                          color: OwnColors.black
                                      ),
                                    ),
                                  )),
                            ):
                            Padding(
                              padding: const EdgeInsets.only(left:8.0,right: 8),
                              child: Container(

                                  decoration: BoxDecoration(
                                    color: OwnColors.white,
                                    border: Border.all(color: OwnColors.CC8C8C8,width: 0.5),
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(12),
                                        topRight: Radius.circular(12),
                                        bottomLeft: Radius.circular(12),
                                        bottomRight: Radius.circular(12)),),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                    child: Text(FilterData.lansList[index],
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          height: 1.3,
                                          fontSize: 14.nsp,
                                          color: OwnColors.C989898
                                      ),
                                    ),
                                  )),
                            );

                          },
                          options: GroupButtonOptions(
                            // selectedShadow: const [],
                            // selectedTextStyle: TextStyle(
                            //   fontSize: 20,
                            //   color: OwnColors.white,
                            // ),
                            // selectedColor: OwnColors.CF39800,
                            // unselectedShadow: const [],
                            // unselectedColor: OwnColors.white,
                            // unselectedTextStyle: TextStyle(
                            //   fontSize: 20,
                            //   color: OwnColors.black_a48,
                            // ),
                            // selectedBorderColor: OwnColors.CFFD796,
                            // unselectedBorderColor: OwnColors.black_a12,
                            borderRadius: BorderRadius.circular(8),
                            spacing: 0,
                            runSpacing: 10,
                            groupingType: GroupingType.wrap,
                            direction: Axis.horizontal,
                            buttonHeight: 60,
                            buttonWidth: 60,
                            mainGroupAlignment: MainGroupAlignment.start,
                            crossGroupAlignment: CrossGroupAlignment.start,
                            groupRunAlignment: GroupRunAlignment.start,
                            textAlign: TextAlign.center,
                            textPadding: EdgeInsets.only(left: 8,right: 8),
                            alignment: Alignment.center,
                            elevation: 0,
                          ),
                        ),
                      ),
                      HorizontalSpace().create(context, 0.02),
                      ScreenSize().createBottomPad(context)
                    ],
                  ),
                ),
              ),





            ],
          ),
        ));
  }

  Widget radioItem(String item,String selected,Function update){
    return Container(
      // height: 0.04 * ScreenSize().getHeight(context),
      child: Column(
        children: [
          HorizontalSpace().create(context, 0.008),
          Row(
            children: [
              AutoSizeText(item,style: TextStyle(fontSize : 14.nsp,color: OwnColors.black),),
              Spacer(),
              Theme(
                data: ThemeData(
                  //here change to your color
                  unselectedWidgetColor: OwnColors.CC8C8C8,
                ),
                child: Radio(
                    visualDensity: const VisualDensity(
                        horizontal: VisualDensity.minimumDensity,
                        vertical: VisualDensity.minimumDensity),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    value: item,
                    groupValue: selected,
                    activeColor: OwnColors.CTourOrange,
                    // focusColor: OwnColors.black,
                    onChanged: (T){
                      update(T);
                    }
                ),
              )
            ],
          ),
          HorizontalSpace().create(context, 0.008),
        ],

      ),
    );
  }

  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      if(isClean){
        print("isClean");
        Navigator.of(context).pop(2);
      }else{
        Navigator.of(context).pop(0);
      }
    } else {
      SystemNavigator.pop();
    }
  }

  void setRating(num) {
    FilterData.guild_rating = num;
  }
  void updateTraffic(String item ){
    print(item);
    setState(() {
      traffic = item;
    });
  }

  void updateGender(String item ){
    print(item);
    setState(() {
      gender = item;
      FilterData.guild_gender = item;
    });
  }

  void closeBtn() {
    if(isClean){
      print("isClean");
      Navigator.of(context).pop(2);
    }else{
      Navigator.of(context).pop(0);
    }
  }

  void filterClick(){
    if(isClean){
      print("isClean");
      Navigator.of(context).pop(2);
    }else{
      Navigator.of(context).pop(1);
    }


    // Navigator.of(context).pop([rating.toString(),duration.start.toString(),duration.end.toString(),transSelList,result_gender,result_lang]);
  }
  void cleanClick(){
    print("cleanClick");
    setState(() {

      FilterData.guild_rating = 0;
      print("FilterData.guild_rating : " + FilterData.guild_rating.toString());

      FilterData.guild_trans = false;
      FilterData.guild_transSelList = [false, false, false];
      traffic = S().home_filter_order_method1;
      transBool = false;
      wantCompanion = false;
      _checkboxesController.unselectAll();
      transSelList = FilterData.guild_transSelList;

      FilterData.guild_gender = S().home_filter_date1;
      gender = S().home_filter_date1;

      FilterData.guild_startNum = 0;
      // FilterData.guild_endNum = 50;
      duration = SfRangeValues(0.0,
          double.parse(SharePre.prefs.getInt(GDefine.commentMax).toString()));


      FilterData.guild_lansSelList = [false,false,false,false,false,false,false];
      lansSelList = FilterData.guild_lansSelList;
      for(int index = 0; index < FilterData.guild_lansSelList.length;index++){
        lansController.unselectIndex(index);
      }


      isClean = true;
    });
  }

  @override
  void dispose() {
    print("dispose");

    super.dispose();
  }

  @override
  void deactivate() {
    print("deactivate");

    super.deactivate();
  }
}
class CheckBoxTile extends StatelessWidget {
  const CheckBoxTile({
    Key key,
    this.selected,
    this.canSelected,
    this.onTap,
    this.title,
  }) : super(key: key);

  final String title;
  final bool selected;
  final bool canSelected;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return
      Row(
        children: [
          new Text(title,
            style: TextStyle(
                fontSize: 14.nsp,
                color: canSelected ? OwnColors.black : OwnColors.C989898
            ),),
          Expanded(child: Container(
            color: OwnColors.white,
            height: 0.03.sh,
          )),
          // CustomCheckBox(
          //   value: selected,
          //   shouldShowBorder: true,
          //   borderColor: Colors.red,
          //   checkedFillColor: Colors.red,
          //   borderRadius: 8,
          //   borderWidth: 1,
          //   checkBoxSize: 22,
          //   onChanged: (val) {
          //     //do your stuff here
          //     onTap();
          //   },
          // ),
          Padding(
            padding: const EdgeInsets.only(right: 3.0),
            child: Transform.scale(
              scale: 1.1,
              child: Checkbox(
                value: selected,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(3.0))),
                visualDensity: VisualDensity(horizontal: -4, vertical: -4),
                activeColor: OwnColors.CTourOrange,
                side: MaterialStateBorderSide.resolveWith(
                      (states) => BorderSide(width: 1.0, color: selected ? OwnColors.tran : canSelected?
                  OwnColors.black : OwnColors.C989898),
                ),
                onChanged: (val) {
                  if(canSelected) {
                    onTap();
                  }
                },
              ),
            ),
          ),
        ],

      );
  }


}