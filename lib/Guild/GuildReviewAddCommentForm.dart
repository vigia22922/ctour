import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/TopBar.dart';
import '../res.dart';
import '../tool/Loading.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_star/flutter_star.dart';

import 'GuildReviewAddCommentSctionAlert.dart';
import 'GuildReviewFormData.dart';
import 'dart:math' as math; // import this

class GuildReviewAddCommentSction {
  Container create(
      BuildContext context,
      Function close,
      Function setFormData,
      bool _showAddCommentSectionAlert,
      Function setAuthComment,
      bool authPassed,
      bool _showAddCommentSection,
      Function showOpacity,
      Function hideOpacity) {
    return Container(
        width: ScreenSize().getWidth(context) * 1.0,
        height: ScreenSize().getHeight(context) * 1.0,
        child: GuildReviewAddCommentForm(
            close,
            setFormData,
            _showAddCommentSectionAlert,
            setAuthComment,
            authPassed,
            _showAddCommentSection,
            showOpacity,
            hideOpacity));
  }
}

//填寫評論
class GuildReviewAddCommentForm extends StatefulWidget {
  Function close;
  Function(FormData) setFormData;
  bool _showAddCommentSectionAlert;
  bool _showAddCommentSection;
  Function setAuthComment;
  bool authPassed;
  Function  showOpacity;
  Function  hideOpacity;
  GuildReviewAddCommentForm(this.close, this.setFormData,
      this._showAddCommentSectionAlert, this.setAuthComment, this.authPassed,this._showAddCommentSection,this.showOpacity,this.hideOpacity);
  @override
  State<StatefulWidget> createState() => _GuildReviewAddCommentForm();
}

class _GuildReviewAddCommentForm extends State<GuildReviewAddCommentForm> {
  bool anonymous = false;
  //bool authPassed = false;
  String comment;
  num commentLength = 0;
  String picURL;
  num rating = 3;
  //Function setAuthComment;
  void showBottomMenu() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 150,
              child: Column(children: <Widget>[
                ListTile(
                  leading: Icon(Icons.photo_camera),
                  title: Text(S().take_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromCamera();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.photo_library),
                  title: Text(S().choose_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromPhoto();
                  },
                )
              ]));
        });
  }

  final picker = ImagePicker();
  List<String> imgList = ["", "", "", "", ""];
  int imgIndex = 0;
  int currentIndexPage = 0;
  void pcikImageFromCamera() async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);
    if (pickedFile.path != null) {
      File img = File(pickedFile.path);
      List<int> imageBytes = await img.readAsBytes();
      String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + base64Image);
      _uploadImage(base64Image);
      setState(() {});
    }
  }

  void pcikImageFromPhoto() async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);
    File img = File(pickedFile.path);
    List<int> imageBytes = await img.readAsBytes();
    /*
    avatar = img.path;
    avatarFileName = avatar
        .split("/")
        .last;
    */
    String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
    print("base64Image: " + base64Image);
    _uploadImage(base64Image);

    setState(() {});
  }

  Future<void> _uploadImage(String data) async {
    // Loading.show(context);

    ResultData resultData =
        await AppApi.getInstance().setFileBase64(context, true, data);

    if (resultData.isSuccess()) {
      // Loading.dismiss(context);
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data);
      String img_url = GDefine.imageUrl + resultData.data["path"];
      print("img_url " + img_url);
      imgList[imgIndex] = img_url;
      setState(() {});
    } else {
      // Loading.dismiss(context);
      WarningDialog.showIOSAlertDialog(context, S().upload_fail_title, S().upload_fail_hint, S().confirm);
    }
  }

  void setRating(num) {
    rating = num;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: SafeArea(
        child: Stack(
          children: [
            GestureDetector(
              onTap: () {
                FocusManager.instance.primaryFocus?.unfocus();
              },
              child: Container(
                height: 0.95 * ScreenSize().getHeight(context),
                child: Column(
                  children: [
                    Container(
                        height: 0.007 * ScreenSize().getHeight(context),
                        width: 0.2 * ScreenSize().getWidth(context),
                        decoration: const BoxDecoration(
                          color: OwnColors.white,
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        )),
                    HorizontalSpace().create(context, 0.02),
                    Container(
                      height: 0.03 * ScreenSize().getHeight(context),
                      width: 0.9 * ScreenSize().getWidth(context),
                      child: Row(
                        children: [
                          GestureDetector(
                              onTap: () {
                                WarningDialog.showIOSAlertDialog3(context, S().save_not_yet, S().save_not_yet_hint2, S().cancel, S().personal_edit_save,backClick, saveComment);
                              },
                              child: Image.asset(Res.xmark)),
                          Spacer(),
                          Container(
                            child: GestureDetector(
                              child: AutoSizeText(S().personal_edit_save,
                                  minFontSize: 10,
                                  maxFontSize: 30,
                                  style: TextStyle(
                                      color: OwnColors.black,
                                      decoration: TextDecoration.underline,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold)),
                              onTap: () {

                                saveComment();
                                //backClick();
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                    HorizontalSpace().create(context, 0.02),
                    Divider(
                      height: 1.0,
                      indent: 0,
                      color: OwnColors.Black30,
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            HorizontalSpace().create(context, 0.02),
                            Container(
                              width: ScreenSize().getWidth(context) * 0.92,
                              //height: ScreenSize().getHeight(context) * 0.06,
                              child: Column(
                                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(children: [
                                    AutoSizeText(S().guilder_add_comment_annoy,
                                        minFontSize: 10,
                                        maxFontSize: 30,
                                        style: TextStyle(
                                          color: OwnColors.black,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        )),
                                    Switch(
                                        value: anonymous,
                                        onChanged: (value) {
                                          setState(() {
                                            anonymous = !anonymous;
                                          });
                                        }),
                                  ]),
                                  HorizontalSpace().create(context, 0.015),
                                  AutoSizeText("評分",
                                      minFontSize: 10,
                                      maxFontSize: 30,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        color: OwnColors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      )),
                                  HorizontalSpace().create(context, 0.005),
                                  //評分
                                  _buildCustomRating(setRating),
                                  HorizontalSpace().create(context, 0.035),
                                  AutoSizeText("評價內容",
                                      minFontSize: 10,
                                      maxFontSize: 30,
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        color: OwnColors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      )),
                                  HorizontalSpace().create(context, 0.015),
                                  Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      border: Border.all(
                                          width: 1.0,
                                          color: const Color(0xFF989898)),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: TextField(
                                        maxLines: 13, //or null
                                        decoration: InputDecoration.collapsed(
                                            hintText: "請輸入評價內容..."),
                                        onChanged: (text) {
                                          setState(() {
                                            comment = text;
                                            commentLength = comment.length;
                                          });
                                        },
                                      ),
                                    ),
                                  ),
                                  HorizontalSpace().create(context, 0.01),
                                  Container(
                                    width: ScreenSize().getWidth(context) * 1.0,
                                    child: AutoSizeText("$commentLength/300",
                                        minFontSize: 10,
                                        maxFontSize: 30,
                                        textAlign: TextAlign.right,
                                        style: TextStyle(
                                          color:
                                              Color.fromRGBO(152, 152, 152, 1),
                                          fontSize: 16,
                                        )),
                                  ),
                                  HorizontalSpace().create(context, 0.015),
                                  Container(
                                    width: ScreenSize().getWidth(context) * 1.0,
                                    child: AutoSizeText("照片",
                                        minFontSize: 10,
                                        maxFontSize: 30,
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          color: OwnColors.black,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        )),
                                  ),
                                  HorizontalSpace().create(context, 0.015),
                                  /*Container(
                                  decoration: BoxDecoration(
                                    color: Color.fromRGBO(247, 247, 247, 1),
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(15)),
                                  ),
                                  width: ScreenSize().getWidth(context) * 1.0,
                                  height: ScreenSize().getHeight(context) * 0.3,
                                  child: Center(
                                    child: Image.asset(
                                      Res.upload_photo,
                                      width:
                                      ScreenSize().getWidth(context) * 0.4,
                                    ),
                                  ),
                                )*/
                                  Container(
                                    width:
                                        0.92 * ScreenSize().getWidth(context),
                                    height:
                                        0.92 * ScreenSize().getWidth(context),
                                    child: Swiper(
                                      loop: false,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return imgList[index].startsWith("http")
                                            ? GestureDetector(
                                                onTap: () {
                                                  showBottomMenu();
                                                  imgIndex = index;
                                                },
                                                child: CachedNetworkImage(
                                                  imageUrl: imgList[index],
                                                  fit: BoxFit.fill,
                                                  placeholder: (context, url) => AspectRatio(
                                                    aspectRatio: 1,
                                                    child: FractionallySizedBox(
                                                      heightFactor: 0.5,
                                                      widthFactor: 0.5,
                                                      child: SpinKitRing(
                                                        lineWidth:5,
                                                        color:
                                                        OwnColors.tran ,
                                                      ),
                                                    ),
                                                  ),
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          Icon(
                                                    Icons.error,
                                                    color: OwnColors.black
                                                        .withOpacity(0.5),
                                                  ),
                                                  imageBuilder: (context,
                                                          imageProvider) =>
                                                      Container(
                                                    // width:0.92 * ScreenSize().getWidth(context),
                                                    // height: 0.92 * ScreenSize().getWidth(context),
                                                    decoration: BoxDecoration(
                                                      color: OwnColors.tran,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  14)),
                                                      image: DecorationImage(
                                                          image: imageProvider,
                                                          fit: BoxFit.cover),
                                                    ),
                                                  ),
                                                  // width: MediaQuery.of(context).size.width,
                                                ),
                                              )
                                            : Container(
                                                // width:0.9 * ScreenSize().getWidth(context),
                                                // height: 0.9 * ScreenSize().getWidth(context),
                                                decoration: BoxDecoration(
                                                  color: OwnColors.CF7F7F7,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(14)),
                                                ),
                                                child: GestureDetector(
                                                  child:
                                                      // bestTourList.length==0?

                                                      Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 0.0,
                                                            right: 8),
                                                    child: Column(
                                                      children: [
                                                        Spacer(),
                                                        Expanded(
                                                            child: Center(
                                                              child: Stack(
                                                                  alignment: Alignment.topLeft,
                                                                  children:[
                                                                    Transform(
                                                                      alignment: Alignment.center,
                                                                      transform: Matrix4.rotationY(math.pi),
                                                                      child: Padding(
                                                                        padding: const EdgeInsets.only(right: 6),
                                                                        child: Icon(SFSymbols.photo_fill, color: OwnColors.CE6E1E1,size: ScreenSize().getWidth(context)*0.2,),
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      height: 0.10 * ScreenSize().getWidth(context),
                                                                      width: 0.10 * ScreenSize().getWidth(context),
                                                                      child: Padding(
                                                                        padding: const EdgeInsets.only(top:5.0,right: 20),
                                                                        child:Stack(
                                                                          children: [
                                                                            Image.asset(Res.add_fill),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ]
                                                              ),
                                                            ),),
                                                        Spacer(),
                                                      ],
                                                    ),
                                                  ),
                                                  onTap: () {
                                                    print("onTap");
                                                    showBottomMenu();
                                                    imgIndex = index;
                                                  },
                                                )
                                                // child: Column(
                                                //   children: [
                                                //     Spacer(),
                                                //     Expanded(
                                                //         child: Image.asset(
                                                //             Res.upload_photo)),
                                                //     Spacer(),
                                                //   ],
                                                // ),
                                                );
                                      },
                                      itemCount: imgList.length,
                                      itemWidth: 300.0,
                                      itemHeight: 200.0,
                                      onIndexChanged: (index) {
                                        currentIndexPage = index;
                                        setState(() {});
                                      },
//                             pagination: new SwiperPagination(
//                                 builder: new DotSwiperPaginationBuilder(
//                                   color: Colors.white,
//                                   activeColor: OwnColors.CTourOrange,
//                                 ),
// ),
                                      // control: new SwiperControl(),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            HorizontalSpace().create(context, 0.02),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            if (widget._showAddCommentSectionAlert && !widget.authPassed)
              GuildReviewAddCommentSctionAlert(
                  _closeAlert,
                  _closeAlertAndCommentSection,
                  widget.setAuthComment,
                  widget.authPassed),
          ],
        ),
      ),
    );
  }

  void saveComment(){
    FormData data = FormData(anonymous, rating, comment, imgList);
    widget.setFormData(data);
    widget.hideOpacity();
  }

  void backClick() {
    widget.hideOpacity();
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }

  void _closeAlert() {
    setState(() {
      widget._showAddCommentSectionAlert = false;
    });
  }

  void _closeAlertAndCommentSection() {
    setState(() {
      widget._showAddCommentSectionAlert = false;
      widget._showAddCommentSection = false;
      backClick();
      // _showAddCommentSection = false;
    });
  }
}

Widget _buildCustomRating(Function setRating) => Column(
      children: <Widget>[
        CustomRating(
            max: 5,
            score: 3.0,
            star: Star(
                size: 27,
                num: 5,
                fillColor: Color.fromRGBO(255, 100, 78, 1),
                fat: 0.4,
                emptyColor: Colors.grey.withAlpha(88)),
            onRating: (s) {
              setRating(s);
            }),
      ],
    );
