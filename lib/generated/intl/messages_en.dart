// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "about_me_extra" : MessageLookupByLibrary.simpleMessage("\'About me\', limited to 500 characters."),
    "account_unactivated_hint" : MessageLookupByLibrary.simpleMessage("Your account has been disabled for violating our terms. Contact us:\nctourtw@gmail.com"),
    "account_unactivated_title" : MessageLookupByLibrary.simpleMessage("Account Disabled"),
    "am" : MessageLookupByLibrary.simpleMessage("AM"),
    "anonymous" : MessageLookupByLibrary.simpleMessage("Anonymous"),
    "block" : MessageLookupByLibrary.simpleMessage("Block"),
    "block_hint" : MessageLookupByLibrary.simpleMessage("Block"),
    "block_title" : MessageLookupByLibrary.simpleMessage("Block"),
    "cancel" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "choose_pic" : MessageLookupByLibrary.simpleMessage("Photo Library"),
    "collect" : MessageLookupByLibrary.simpleMessage("Save"),
    "comment_num" : MessageLookupByLibrary.simpleMessage("reviews"),
    "comment_num_title" : MessageLookupByLibrary.simpleMessage("Comment count"),
    "confirm" : MessageLookupByLibrary.simpleMessage("OK"),
    "day" : MessageLookupByLibrary.simpleMessage(""),
    "delete" : MessageLookupByLibrary.simpleMessage("Delete"),
    "deleteAccount" : MessageLookupByLibrary.simpleMessage("Are you sure you want to delete your account?"),
    "delete_account_content" : MessageLookupByLibrary.simpleMessage("Are you sure you want to delete your account? Once you delete your account, it can\'t be restored."),
    "delete_account_title" : MessageLookupByLibrary.simpleMessage("Delete Account?"),
    "delete_fail_hint" : MessageLookupByLibrary.simpleMessage("Please try again later."),
    "delete_fail_title" : MessageLookupByLibrary.simpleMessage("Failed to Delete it"),
    "download_expert_fail" : MessageLookupByLibrary.simpleMessage("Download Failed"),
    "download_expert_fail_hint" : MessageLookupByLibrary.simpleMessage("Please try again later."),
    "download_fail" : MessageLookupByLibrary.simpleMessage("Loading Failed"),
    "download_fail_hint" : MessageLookupByLibrary.simpleMessage("Please try again later."),
    "download_fail_title" : MessageLookupByLibrary.simpleMessage("Download Failed"),
    "dynamic_title" : MessageLookupByLibrary.simpleMessage("Travel and Food Community"),
    "edit" : MessageLookupByLibrary.simpleMessage("Edit"),
    "edit_avatar" : MessageLookupByLibrary.simpleMessage("Change profile photo"),
    "email" : MessageLookupByLibrary.simpleMessage("Email"),
    "fail" : MessageLookupByLibrary.simpleMessage("Fail"),
    "find_companion" : MessageLookupByLibrary.simpleMessage("Find Travel Buddies"),
    "finish" : MessageLookupByLibrary.simpleMessage("Done"),
    "forget_password_email_format_fail_content" : MessageLookupByLibrary.simpleMessage("Please check your username and try again."),
    "forget_password_email_format_fail_title" : MessageLookupByLibrary.simpleMessage("Invalid Email Format"),
    "forget_password_email_noUser_fail_content" : MessageLookupByLibrary.simpleMessage("The username you entered doesn\'t appear to belong to an account. Sign up with this email?"),
    "forget_password_email_noUser_fail_title" : MessageLookupByLibrary.simpleMessage("Incorrect Username"),
    "forget_password_success_content" : MessageLookupByLibrary.simpleMessage("An email has been sent to your email address, and don\'t forget to check your spam folder."),
    "forget_password_success_title" : MessageLookupByLibrary.simpleMessage("Check Email for Reset Link"),
    "forget_pwd" : MessageLookupByLibrary.simpleMessage("Forgot password"),
    "friday" : MessageLookupByLibrary.simpleMessage("Friday"),
    "gilder_filter_looking_companion" : MessageLookupByLibrary.simpleMessage("Find travel buddies"),
    "gilder_filter_looking_companion_traffic" : MessageLookupByLibrary.simpleMessage("Vehicle"),
    "guild_comment_num" : MessageLookupByLibrary.simpleMessage(" reviews"),
    "guild_comment_num1" : MessageLookupByLibrary.simpleMessage("0 review"),
    "guild_comment_num2" : MessageLookupByLibrary.simpleMessage("50 reviews"),
    "guild_comment_num_single" : MessageLookupByLibrary.simpleMessage(" review"),
    "guild_gender" : MessageLookupByLibrary.simpleMessage("Gender"),
    "guild_gender1" : MessageLookupByLibrary.simpleMessage("Both"),
    "guild_gender2" : MessageLookupByLibrary.simpleMessage("Man"),
    "guild_gender3" : MessageLookupByLibrary.simpleMessage("Woman"),
    "guild_language" : MessageLookupByLibrary.simpleMessage("Language"),
    "guild_rate" : MessageLookupByLibrary.simpleMessage("Rating"),
    "guild_traffic" : MessageLookupByLibrary.simpleMessage("Vehicle"),
    "guild_traffic1" : MessageLookupByLibrary.simpleMessage("Car"),
    "guild_traffic2" : MessageLookupByLibrary.simpleMessage("Scooter"),
    "guild_traffic3" : MessageLookupByLibrary.simpleMessage("Others"),
    "guilder_abountme" : MessageLookupByLibrary.simpleMessage("About me"),
    "guilder_add_comment_annoy" : MessageLookupByLibrary.simpleMessage("Anonymous Posting"),
    "guilder_add_comment_content" : MessageLookupByLibrary.simpleMessage("Review"),
    "guilder_add_comment_content_hint" : MessageLookupByLibrary.simpleMessage("Write your review here..."),
    "guilder_add_comment_invite_content_hint" : MessageLookupByLibrary.simpleMessage("1234"),
    "guilder_add_comment_invite_fail_hint" : MessageLookupByLibrary.simpleMessage("Please check review password again."),
    "guilder_add_comment_invite_fail_title" : MessageLookupByLibrary.simpleMessage("Incorrect Password"),
    "guilder_add_comment_invite_title" : MessageLookupByLibrary.simpleMessage("Please Enter\nReview Password"),
    "guilder_add_comment_invite_tool_hint" : MessageLookupByLibrary.simpleMessage("You need to know travel expert\'s review password to leave a review for him/her."),
    "guilder_add_comment_no_content_hint" : MessageLookupByLibrary.simpleMessage("Review field is required"),
    "guilder_add_comment_no_content_title" : MessageLookupByLibrary.simpleMessage("No Review"),
    "guilder_add_comment_pic" : MessageLookupByLibrary.simpleMessage("Photo"),
    "guilder_add_comment_rat" : MessageLookupByLibrary.simpleMessage("Rating"),
    "guilder_comment" : MessageLookupByLibrary.simpleMessage("Reviews"),
    "guilder_mess" : MessageLookupByLibrary.simpleMessage("Message"),
    "guilder_more" : MessageLookupByLibrary.simpleMessage("more"),
    "guilder_travel" : MessageLookupByLibrary.simpleMessage("Aritcles"),
    "guilder_travel_comment" : MessageLookupByLibrary.simpleMessage("comments"),
    "guilder_travel_search_hint" : MessageLookupByLibrary.simpleMessage("Keyword, Title and More "),
    "home_filter_clean" : MessageLookupByLibrary.simpleMessage("Clear All"),
    "home_filter_date" : MessageLookupByLibrary.simpleMessage("Upload Date"),
    "home_filter_date1" : MessageLookupByLibrary.simpleMessage("Anytime"),
    "home_filter_date2" : MessageLookupByLibrary.simpleMessage("This week"),
    "home_filter_date3" : MessageLookupByLibrary.simpleMessage("This month"),
    "home_filter_date4" : MessageLookupByLibrary.simpleMessage("This year"),
    "home_filter_duration" : MessageLookupByLibrary.simpleMessage("Duration"),
    "home_filter_duration1" : MessageLookupByLibrary.simpleMessage("0-day"),
    "home_filter_duration2" : MessageLookupByLibrary.simpleMessage("5-day"),
    "home_filter_filter" : MessageLookupByLibrary.simpleMessage("Apply"),
    "home_filter_order_method" : MessageLookupByLibrary.simpleMessage("Sort by"),
    "home_filter_order_method1" : MessageLookupByLibrary.simpleMessage("Popularity"),
    "home_filter_order_method2" : MessageLookupByLibrary.simpleMessage("View count"),
    "home_filter_order_method3" : MessageLookupByLibrary.simpleMessage("Donate count"),
    "home_filter_order_method4" : MessageLookupByLibrary.simpleMessage("Author"),
    "home_filter_type" : MessageLookupByLibrary.simpleMessage("Theme"),
    "home_filter_type1" : MessageLookupByLibrary.simpleMessage("Any"),
    "home_filter_type2" : MessageLookupByLibrary.simpleMessage("Itinerary"),
    "home_filter_type2_string" : MessageLookupByLibrary.simpleMessage("-day | Itinerary"),
    "home_filter_type3" : MessageLookupByLibrary.simpleMessage("Food"),
    "home_filter_type4" : MessageLookupByLibrary.simpleMessage("Others"),
    "home_have_car" : MessageLookupByLibrary.simpleMessage("Vehicle"),
    "home_have_no_car" : MessageLookupByLibrary.simpleMessage("No Vehicle"),
    "home_rate_number" : MessageLookupByLibrary.simpleMessage(" reviews"),
    "home_rate_number_single" : MessageLookupByLibrary.simpleMessage(" review"),
    "home_search_hint" : MessageLookupByLibrary.simpleMessage("Where to?"),
    "home_title1" : MessageLookupByLibrary.simpleMessage("Top Picks This Week"),
    "home_title2" : MessageLookupByLibrary.simpleMessage("Explore With Travel Experts"),
    "image" : MessageLookupByLibrary.simpleMessage("Photo"),
    "invite_code_extra" : MessageLookupByLibrary.simpleMessage("Review password limited to 4 characters."),
    "keep_going" : MessageLookupByLibrary.simpleMessage("Continue"),
    "like" : MessageLookupByLibrary.simpleMessage("Like"),
    "login" : MessageLookupByLibrary.simpleMessage("Log in"),
    "login_choose_lasttime_type" : MessageLookupByLibrary.simpleMessage("Last used "),
    "login_fail_email_format_hint" : MessageLookupByLibrary.simpleMessage("Invalid email format"),
    "login_fail_hint" : MessageLookupByLibrary.simpleMessage("Login failed. Please try again later."),
    "login_fail_no_user_hint" : MessageLookupByLibrary.simpleMessage("Couldn\'t find your account. Please sign up first."),
    "login_fail_no_user_title" : MessageLookupByLibrary.simpleMessage("Login Failed"),
    "login_fail_pwd_hint" : MessageLookupByLibrary.simpleMessage("Incorrect password"),
    "login_fail_pwd_title" : MessageLookupByLibrary.simpleMessage("Login Failed"),
    "login_fail_title" : MessageLookupByLibrary.simpleMessage("Login Failed"),
    "login_fail_wrong_password_content" : MessageLookupByLibrary.simpleMessage("At least 6 characters."),
    "login_fail_wrong_password_title" : MessageLookupByLibrary.simpleMessage("Incorrect Password"),
    "login_first_hint" : MessageLookupByLibrary.simpleMessage("Please log in or sign up first."),
    "login_first_title" : MessageLookupByLibrary.simpleMessage("No login Info."),
    "login_no_register_content" : MessageLookupByLibrary.simpleMessage("The username you entered doesn\'t appear to belong to an account. Go to sign up?"),
    "login_no_register_title" : MessageLookupByLibrary.simpleMessage("Incorrect Username"),
    "message" : MessageLookupByLibrary.simpleMessage("Chats"),
    "message_search_hint" : MessageLookupByLibrary.simpleMessage("Search"),
    "monday" : MessageLookupByLibrary.simpleMessage("Monday"),
    "month" : MessageLookupByLibrary.simpleMessage("-"),
    "name" : MessageLookupByLibrary.simpleMessage("Name"),
    "name_hint" : MessageLookupByLibrary.simpleMessage("Max. 64 characters"),
    "network_fail" : MessageLookupByLibrary.simpleMessage("Network error. Please try again later."),
    "new_register_my_gender" : MessageLookupByLibrary.simpleMessage("I am a"),
    "new_register_my_gender_female" : MessageLookupByLibrary.simpleMessage("WOMAN"),
    "new_register_my_gender_hint" : MessageLookupByLibrary.simpleMessage("This won\'t appear on your profile"),
    "new_register_my_gender_male" : MessageLookupByLibrary.simpleMessage("MAN"),
    "new_register_my_lan" : MessageLookupByLibrary.simpleMessage("Languages"),
    "new_register_my_lan_hint" : MessageLookupByLibrary.simpleMessage("Let people who speak other languages to feel free to communicate with you"),
    "new_register_my_trip" : MessageLookupByLibrary.simpleMessage("Interests"),
    "new_register_my_trip_hint" : MessageLookupByLibrary.simpleMessage("Let everyone know what you\'re interested in by adding it to your profile"),
    "new_register_name" : MessageLookupByLibrary.simpleMessage("Name"),
    "new_register_name_hint" : MessageLookupByLibrary.simpleMessage("Max. 64 characters"),
    "new_register_name_hint2" : MessageLookupByLibrary.simpleMessage("This is how it will appear in CTour, and you can edit it after registration."),
    "new_register_next" : MessageLookupByLibrary.simpleMessage("NEXT"),
    "new_register_photo" : MessageLookupByLibrary.simpleMessage("Add photos"),
    "new_register_photo_error" : MessageLookupByLibrary.simpleMessage("Add at least 2 photos to continue"),
    "new_register_photo_hint" : MessageLookupByLibrary.simpleMessage("Add at least 2 photos to continue"),
    "new_register_start" : MessageLookupByLibrary.simpleMessage("START"),
    "no_image_hint" : MessageLookupByLibrary.simpleMessage("Please upload at least one photo."),
    "no_language" : MessageLookupByLibrary.simpleMessage("\'Language\' is required"),
    "no_language_hint" : MessageLookupByLibrary.simpleMessage("Please add at least one language."),
    "no_mess" : MessageLookupByLibrary.simpleMessage("No Messages Yet"),
    "no_name_title" : MessageLookupByLibrary.simpleMessage("Name Field is Required"),
    "no_name_title_hint" : MessageLookupByLibrary.simpleMessage("Please fill in the field"),
    "no_trans_hint" : MessageLookupByLibrary.simpleMessage("Please add at least one vehicle."),
    "no_trip" : MessageLookupByLibrary.simpleMessage("\'Interest\', is required"),
    "no_trip_hint" : MessageLookupByLibrary.simpleMessage("Please add at least one interest."),
    "not_tourist" : MessageLookupByLibrary.simpleMessage("Your profile is hidden, the article feature can\'t be used, please turn off hidden button and try again"),
    "one_day_ago" : MessageLookupByLibrary.simpleMessage("Yesterday"),
    "or" : MessageLookupByLibrary.simpleMessage("OR"),
    "personal_become_add_location_hint" : MessageLookupByLibrary.simpleMessage("Interests"),
    "personal_become_add_location_title" : MessageLookupByLibrary.simpleMessage("Your interests"),
    "personal_become_guilder_about" : MessageLookupByLibrary.simpleMessage("About me"),
    "personal_become_guilder_about_hint" : MessageLookupByLibrary.simpleMessage("Maximum 300 characters allowed."),
    "personal_become_guilder_gender" : MessageLookupByLibrary.simpleMessage("Gender"),
    "personal_become_guilder_have_car" : MessageLookupByLibrary.simpleMessage("I have my own transport, and I can carry my travel buddies."),
    "personal_become_guilder_invite" : MessageLookupByLibrary.simpleMessage("Review Password"),
    "personal_become_guilder_invite_hint" : MessageLookupByLibrary.simpleMessage("Please set your review password."),
    "personal_become_guilder_invite_hint2" : MessageLookupByLibrary.simpleMessage("People who want to leave a review for you need to know your review password."),
    "personal_become_guilder_lan" : MessageLookupByLibrary.simpleMessage("Language"),
    "personal_become_guilder_location" : MessageLookupByLibrary.simpleMessage("Your interests"),
    "personal_become_guilder_name_hint" : MessageLookupByLibrary.simpleMessage("Max. 64 characters"),
    "personal_become_guilder_preview" : MessageLookupByLibrary.simpleMessage("Preview"),
    "personal_become_guilder_save" : MessageLookupByLibrary.simpleMessage("Save"),
    "personal_become_guilder_title" : MessageLookupByLibrary.simpleMessage("I\'m a travel expert"),
    "personal_become_guilder_trans" : MessageLookupByLibrary.simpleMessage("I have vehicles"),
    "personal_become_guilder_trans1" : MessageLookupByLibrary.simpleMessage("Car"),
    "personal_become_guilder_trans2" : MessageLookupByLibrary.simpleMessage("Scooter"),
    "personal_become_guilder_trans3" : MessageLookupByLibrary.simpleMessage("Others"),
    "personal_become_guilder_upload" : MessageLookupByLibrary.simpleMessage("Post"),
    "personal_become_guilder_visible" : MessageLookupByLibrary.simpleMessage("Don\'t show me on CTour"),
    "personal_become_guilder_want_companion" : MessageLookupByLibrary.simpleMessage("I\'m trying to find travel buddies"),
    "personal_become_outside" : MessageLookupByLibrary.simpleMessage("road trip"),
    "personal_become_taipei" : MessageLookupByLibrary.simpleMessage("Grand Canyon"),
    "personal_collection" : MessageLookupByLibrary.simpleMessage("Saved"),
    "personal_collection_search_hint1" : MessageLookupByLibrary.simpleMessage("Hashtag / Title / Travel expert"),
    "personal_collection_search_hint2" : MessageLookupByLibrary.simpleMessage("Travel expert / Interest"),
    "personal_collection_tab1" : MessageLookupByLibrary.simpleMessage("Travel experts"),
    "personal_collection_tab2" : MessageLookupByLibrary.simpleMessage("Articles"),
    "personal_edit" : MessageLookupByLibrary.simpleMessage("Edit profile"),
    "personal_edit_avatar" : MessageLookupByLibrary.simpleMessage("Edit"),
    "personal_edit_delete" : MessageLookupByLibrary.simpleMessage("Delete account"),
    "personal_edit_delete_fail_hint" : MessageLookupByLibrary.simpleMessage("Failed to delete account, please try again later."),
    "personal_edit_delete_fail_title" : MessageLookupByLibrary.simpleMessage("Failed to Delete Account"),
    "personal_edit_delete_no_firebase_hint" : MessageLookupByLibrary.simpleMessage("Please log in again and delete later to ensure your account security."),
    "personal_edit_delete_no_firebase_title" : MessageLookupByLibrary.simpleMessage("Session Expired"),
    "personal_edit_name" : MessageLookupByLibrary.simpleMessage("Name"),
    "personal_edit_name_empty_hint" : MessageLookupByLibrary.simpleMessage("Please fill in the field."),
    "personal_edit_name_empty_title" : MessageLookupByLibrary.simpleMessage("Name Field is Required"),
    "personal_edit_name_overflow_hint" : MessageLookupByLibrary.simpleMessage("Maximum 64 characters allowed."),
    "personal_edit_name_overflow_title" : MessageLookupByLibrary.simpleMessage("Name Length is Too Long"),
    "personal_edit_save" : MessageLookupByLibrary.simpleMessage("Save"),
    "personal_logout" : MessageLookupByLibrary.simpleMessage("Log Out"),
    "personal_logout_content" : MessageLookupByLibrary.simpleMessage("You\'ll need to enter your username and password next time you want to log in.\n(;´༎ຶД༎ຶ`)"),
    "personal_mail_hint" : MessageLookupByLibrary.simpleMessage("Here are my suggestions for CTour..."),
    "personal_mail_title" : MessageLookupByLibrary.simpleMessage("Feedback from a CTourist"),
    "personal_my_travel" : MessageLookupByLibrary.simpleMessage("My articles"),
    "personal_my_travel_article" : MessageLookupByLibrary.simpleMessage("Article"),
    "personal_my_travel_comment_no_content_hint" : MessageLookupByLibrary.simpleMessage("Please new a comment."),
    "personal_my_travel_comment_no_content_title" : MessageLookupByLibrary.simpleMessage("Comment Is Required"),
    "personal_my_travel_content_hint" : MessageLookupByLibrary.simpleMessage("Enter content here"),
    "personal_my_travel_content_length_hint" : MessageLookupByLibrary.simpleMessage("Content limited to 500 characters."),
    "personal_my_travel_content_length_title" : MessageLookupByLibrary.simpleMessage("Maximum characters exceeded"),
    "personal_my_travel_content_title" : MessageLookupByLibrary.simpleMessage("Content"),
    "personal_my_travel_day" : MessageLookupByLibrary.simpleMessage("-day"),
    "personal_my_travel_default_title" : MessageLookupByLibrary.simpleMessage("No title"),
    "personal_my_travel_del_hint" : MessageLookupByLibrary.simpleMessage("It will be permanently deleted, and unable to undo it."),
    "personal_my_travel_del_title" : MessageLookupByLibrary.simpleMessage("Delete Article?"),
    "personal_my_travel_description" : MessageLookupByLibrary.simpleMessage("Introduction"),
    "personal_my_travel_description_hint" : MessageLookupByLibrary.simpleMessage("Max. 500 characters"),
    "personal_my_travel_foody" : MessageLookupByLibrary.simpleMessage("Food"),
    "personal_my_travel_google" : MessageLookupByLibrary.simpleMessage("Google Maps"),
    "personal_my_travel_google_error" : MessageLookupByLibrary.simpleMessage("Invalid Link"),
    "personal_my_travel_google_error_hint" : MessageLookupByLibrary.simpleMessage("The link has an invalid format. Please try again."),
    "personal_my_travel_google_link" : MessageLookupByLibrary.simpleMessage("Google Maps Link"),
    "personal_my_travel_image" : MessageLookupByLibrary.simpleMessage("Photo"),
    "personal_my_travel_journey" : MessageLookupByLibrary.simpleMessage(" | Itinerary"),
    "personal_my_travel_no_content_hint" : MessageLookupByLibrary.simpleMessage("Please fill in the field."),
    "personal_my_travel_no_content_title" : MessageLookupByLibrary.simpleMessage("Content Field Is Required"),
    "personal_my_travel_no_description_hint" : MessageLookupByLibrary.simpleMessage("Please fill in the field."),
    "personal_my_travel_no_description_title" : MessageLookupByLibrary.simpleMessage("Introduction Field Is Required"),
    "personal_my_travel_no_img_hint" : MessageLookupByLibrary.simpleMessage("Please upload a cover photo."),
    "personal_my_travel_no_img_title" : MessageLookupByLibrary.simpleMessage("Cover Photo Field Is Required"),
    "personal_my_travel_no_para_hint" : MessageLookupByLibrary.simpleMessage("Please new an article."),
    "personal_my_travel_no_para_title" : MessageLookupByLibrary.simpleMessage("Article Is Required"),
    "personal_my_travel_no_tag_hint" : MessageLookupByLibrary.simpleMessage("Please new a label."),
    "personal_my_travel_no_tag_title" : MessageLookupByLibrary.simpleMessage("Label Is Required"),
    "personal_my_travel_no_theme_hint" : MessageLookupByLibrary.simpleMessage("Please choose a theme."),
    "personal_my_travel_no_theme_title" : MessageLookupByLibrary.simpleMessage("Theme Field Is Required"),
    "personal_my_travel_no_title_hint" : MessageLookupByLibrary.simpleMessage("Please fill in the field."),
    "personal_my_travel_no_title_title" : MessageLookupByLibrary.simpleMessage("Title Field Is Required"),
    "personal_my_travel_not_posted" : MessageLookupByLibrary.simpleMessage("Drafts"),
    "personal_my_travel_other" : MessageLookupByLibrary.simpleMessage("Others"),
    "personal_my_travel_paragraph" : MessageLookupByLibrary.simpleMessage("Paragraph"),
    "personal_my_travel_posted" : MessageLookupByLibrary.simpleMessage("Posted"),
    "personal_my_travel_search_hint" : MessageLookupByLibrary.simpleMessage("Search articles"),
    "personal_my_travel_tag" : MessageLookupByLibrary.simpleMessage("Label"),
    "personal_my_travel_tag_title" : MessageLookupByLibrary.simpleMessage("Hashtag"),
    "personal_my_travel_tag_title2" : MessageLookupByLibrary.simpleMessage("Hashtag"),
    "personal_my_travel_tag_title_hint" : MessageLookupByLibrary.simpleMessage("Enter hashtag here"),
    "personal_my_travel_theme" : MessageLookupByLibrary.simpleMessage("Theme"),
    "personal_my_travel_title" : MessageLookupByLibrary.simpleMessage("Title"),
    "personal_my_travel_title_hint" : MessageLookupByLibrary.simpleMessage("Enter title here"),
    "personal_my_travel_title_img" : MessageLookupByLibrary.simpleMessage("Cover photo"),
    "personal_my_travel_travel_tag" : MessageLookupByLibrary.simpleMessage("Hashtag"),
    "personal_my_travel_warning_structure_hint" : MessageLookupByLibrary.simpleMessage("At least one label be placed before paragraphs"),
    "personal_my_travel_warning_structure_title" : MessageLookupByLibrary.simpleMessage("Structure Reverse"),
    "personal_no_email_app" : MessageLookupByLibrary.simpleMessage("Please send your seggestions to\nctourtw@gmail.com"),
    "personal_no_email_title" : MessageLookupByLibrary.simpleMessage("Thanks a lot."),
    "personal_title1" : MessageLookupByLibrary.simpleMessage("Saved"),
    "personal_title2" : MessageLookupByLibrary.simpleMessage("Travel expert"),
    "personal_title3" : MessageLookupByLibrary.simpleMessage("Notifications"),
    "personal_title4" : MessageLookupByLibrary.simpleMessage("Rate our App"),
    "personal_title5" : MessageLookupByLibrary.simpleMessage("Feedback"),
    "personal_title6" : MessageLookupByLibrary.simpleMessage("Terms of Service"),
    "personal_title7" : MessageLookupByLibrary.simpleMessage("Privacy Policy"),
    "personal_title8" : MessageLookupByLibrary.simpleMessage("Delete account"),
    "personal_title9" : MessageLookupByLibrary.simpleMessage("Delete account"),
    "personal_travel_first_chapter" : MessageLookupByLibrary.simpleMessage("Day 1"),
    "personal_travel_first_section" : MessageLookupByLibrary.simpleMessage("Times Square"),
    "personnal_guilder_myself_hint" : MessageLookupByLibrary.simpleMessage("You can\'t leave a review for yourself."),
    "please_wait" : MessageLookupByLibrary.simpleMessage("Please try again later."),
    "pm" : MessageLookupByLibrary.simpleMessage("PM"),
    "preview" : MessageLookupByLibrary.simpleMessage("Preview"),
    "pwd" : MessageLookupByLibrary.simpleMessage("Password"),
    "pwd_hint" : MessageLookupByLibrary.simpleMessage("At least 6 characters"),
    "rate_btn" : MessageLookupByLibrary.simpleMessage("Rate now"),
    "rate_btn_2" : MessageLookupByLibrary.simpleMessage("Maybe later"),
    "rate_btn_3" : MessageLookupByLibrary.simpleMessage("No thanks"),
    "rate_hint" : MessageLookupByLibrary.simpleMessage("Would you please leave us a review if you like this app?"),
    "rate_title" : MessageLookupByLibrary.simpleMessage("Rate CTour"),
    "register" : MessageLookupByLibrary.simpleMessage("Sign up"),
    "register_agree" : MessageLookupByLibrary.simpleMessage("By signing up or logging in, you agree to CTour\'s "),
    "register_and" : MessageLookupByLibrary.simpleMessage("and"),
    "register_exit_content" : MessageLookupByLibrary.simpleMessage("This email is on another account. You can log into the account associated with that email."),
    "register_exit_title" : MessageLookupByLibrary.simpleMessage("Log into Existing Account?"),
    "register_fail_hint" : MessageLookupByLibrary.simpleMessage("Sign up failed. Please try again later."),
    "register_fail_title" : MessageLookupByLibrary.simpleMessage("Sign Up Failed"),
    "register_password_easy_content" : MessageLookupByLibrary.simpleMessage("Your password should be minimum of 6 characters."),
    "register_password_easy_title" : MessageLookupByLibrary.simpleMessage("Weak Password"),
    "register_privacy" : MessageLookupByLibrary.simpleMessage("Privacy Policy"),
    "register_service" : MessageLookupByLibrary.simpleMessage("Terms of Service"),
    "remind" : MessageLookupByLibrary.simpleMessage("Remind"),
    "report" : MessageLookupByLibrary.simpleMessage("Report"),
    "report_reason" : MessageLookupByLibrary.simpleMessage("Reason for report"),
    "report_reason1" : MessageLookupByLibrary.simpleMessage("False information"),
    "report_reason2" : MessageLookupByLibrary.simpleMessage("Slander, discriminate or provoke others"),
    "report_reason3" : MessageLookupByLibrary.simpleMessage("Nudity or sexual activity"),
    "report_reason4" : MessageLookupByLibrary.simpleMessage("Intellectual property violation"),
    "report_reason5" : MessageLookupByLibrary.simpleMessage("Bullying or harassment"),
    "report_reason6" : MessageLookupByLibrary.simpleMessage("Sale of illegal or regulated goods"),
    "report_reason7" : MessageLookupByLibrary.simpleMessage("Spamming or site interference"),
    "report_reason8" : MessageLookupByLibrary.simpleMessage("The topic of this article has nothing to do with travel"),
    "report_reason9" : MessageLookupByLibrary.simpleMessage("Other reasons"),
    "report_res_content" : MessageLookupByLibrary.simpleMessage("Thanks for your report, your report has been submitted."),
    "report_res_title" : MessageLookupByLibrary.simpleMessage("Report submitted."),
    "reset_pwd" : MessageLookupByLibrary.simpleMessage("Reset Password"),
    "saturday" : MessageLookupByLibrary.simpleMessage("Saturday"),
    "save" : MessageLookupByLibrary.simpleMessage("Save"),
    "save_draft" : MessageLookupByLibrary.simpleMessage("Save"),
    "save_not_yet" : MessageLookupByLibrary.simpleMessage("Unsaved Changes"),
    "save_not_yet_hint" : MessageLookupByLibrary.simpleMessage("Do you want to save this post as a draft?"),
    "save_not_yet_hint2" : MessageLookupByLibrary.simpleMessage("Do you want to save changes?"),
    "search_not_found_hint" : MessageLookupByLibrary.simpleMessage("Try different keywords or turn off some filters "),
    "search_not_found_title" : MessageLookupByLibrary.simpleMessage("No results found"),
    "send_like_fail_hint" : MessageLookupByLibrary.simpleMessage("Please try again later."),
    "send_like_fail_title" : MessageLookupByLibrary.simpleMessage("Saved Failed"),
    "send_mes_hint" : MessageLookupByLibrary.simpleMessage("Message..."),
    "set_favorite_fail" : MessageLookupByLibrary.simpleMessage("Save Failed"),
    "share" : MessageLookupByLibrary.simpleMessage("Share"),
    "start" : MessageLookupByLibrary.simpleMessage("Start"),
    "sunday" : MessageLookupByLibrary.simpleMessage("Sunday"),
    "tab_guilder" : MessageLookupByLibrary.simpleMessage("Experts"),
    "tab_home" : MessageLookupByLibrary.simpleMessage("Home"),
    "tab_mess" : MessageLookupByLibrary.simpleMessage("Chats"),
    "tab_personal" : MessageLookupByLibrary.simpleMessage("Profile"),
    "take_pic" : MessageLookupByLibrary.simpleMessage("Take Photo"),
    "the_email_used_social" : MessageLookupByLibrary.simpleMessage("This Email is on Another Account"),
    "the_email_used_social_apple" : MessageLookupByLibrary.simpleMessage("This email is associated with Apple, please log in with Apple."),
    "the_email_used_social_fb" : MessageLookupByLibrary.simpleMessage("This email is associated with Facebook, please log in with Facebook."),
    "the_email_used_social_google" : MessageLookupByLibrary.simpleMessage("This Email is on Another Account\", \"This email is associated with Google, please log in with Google."),
    "three_day_ago" : MessageLookupByLibrary.simpleMessage("3 days ago"),
    "thursday" : MessageLookupByLibrary.simpleMessage("Thursday"),
    "travel_article_not_found_hint1" : MessageLookupByLibrary.simpleMessage("This article has been deleted."),
    "travel_article_not_found_hint2" : MessageLookupByLibrary.simpleMessage("This article has been hidden."),
    "travel_article_not_found_title" : MessageLookupByLibrary.simpleMessage("Article Not Available"),
    "travel_cant_delete" : MessageLookupByLibrary.simpleMessage("Can\'t Delete This Item"),
    "travel_collect_fail_hint" : MessageLookupByLibrary.simpleMessage("Save failed, please try again later."),
    "travel_collect_fail_title" : MessageLookupByLibrary.simpleMessage("Save Failed"),
    "travel_comment" : MessageLookupByLibrary.simpleMessage("comments"),
    "travel_comment_edit_hint" : MessageLookupByLibrary.simpleMessage("write a comment..."),
    "travel_comment_filter_hot" : MessageLookupByLibrary.simpleMessage("popularity"),
    "travel_comment_filter_new" : MessageLookupByLibrary.simpleMessage("time"),
    "travel_comment_single" : MessageLookupByLibrary.simpleMessage("comment"),
    "travel_comment_title" : MessageLookupByLibrary.simpleMessage("Comments"),
    "travel_guilder_not_found_hint" : MessageLookupByLibrary.simpleMessage("This account has been deleted or the profile has been hidden."),
    "travel_guilder_not_found_title" : MessageLookupByLibrary.simpleMessage("Expert Not Found"),
    "travel_img_max_hint" : MessageLookupByLibrary.simpleMessage("請先刪除照片後再新增"),
    "travel_img_max_title" : MessageLookupByLibrary.simpleMessage("照片最多三張"),
    "travel_share_title" : MessageLookupByLibrary.simpleMessage("invites you to read this article."),
    "travel_title1" : MessageLookupByLibrary.simpleMessage("Top Picks This Week"),
    "travel_title2" : MessageLookupByLibrary.simpleMessage("Where to go?"),
    "travel_views" : MessageLookupByLibrary.simpleMessage("views"),
    "travel_views_single" : MessageLookupByLibrary.simpleMessage("view"),
    "tuesday" : MessageLookupByLibrary.simpleMessage("Tuesday"),
    "two_day_ago" : MessageLookupByLibrary.simpleMessage("2 days ago"),
    "update" : MessageLookupByLibrary.simpleMessage("Update"),
    "upload_fail_hint" : MessageLookupByLibrary.simpleMessage("Try again later"),
    "upload_fail_title" : MessageLookupByLibrary.simpleMessage("Update Failed"),
    "warning" : MessageLookupByLibrary.simpleMessage("Warning"),
    "wednesday" : MessageLookupByLibrary.simpleMessage("Wednesday"),
    "year" : MessageLookupByLibrary.simpleMessage("-"),
    "yesterday" : MessageLookupByLibrary.simpleMessage("Yesterday")
  };
}
