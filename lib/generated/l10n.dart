// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Sign up`
  String get register {
    return Intl.message(
      'Sign up',
      name: 'register',
      desc: '',
      args: [],
    );
  }

  /// `Log in`
  String get login {
    return Intl.message(
      'Log in',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get email {
    return Intl.message(
      'Email',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get pwd {
    return Intl.message(
      'Password',
      name: 'pwd',
      desc: '',
      args: [],
    );
  }

  /// `At least 6 characters`
  String get pwd_hint {
    return Intl.message(
      'At least 6 characters',
      name: 'pwd_hint',
      desc: '',
      args: [],
    );
  }

  /// `Forgot password`
  String get forget_pwd {
    return Intl.message(
      'Forgot password',
      name: 'forget_pwd',
      desc: '',
      args: [],
    );
  }

  /// `OR`
  String get or {
    return Intl.message(
      'OR',
      name: 'or',
      desc: '',
      args: [],
    );
  }

  /// `Change profile photo`
  String get edit_avatar {
    return Intl.message(
      'Change profile photo',
      name: 'edit_avatar',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get name {
    return Intl.message(
      'Name',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `Max. 64 characters`
  String get name_hint {
    return Intl.message(
      'Max. 64 characters',
      name: 'name_hint',
      desc: '',
      args: [],
    );
  }

  /// `Reset Password`
  String get reset_pwd {
    return Intl.message(
      'Reset Password',
      name: 'reset_pwd',
      desc: '',
      args: [],
    );
  }

  /// `Start`
  String get start {
    return Intl.message(
      'Start',
      name: 'start',
      desc: '',
      args: [],
    );
  }

  /// `Chats`
  String get message {
    return Intl.message(
      'Chats',
      name: 'message',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get message_search_hint {
    return Intl.message(
      'Search',
      name: 'message_search_hint',
      desc: '',
      args: [],
    );
  }

  /// `Message...`
  String get send_mes_hint {
    return Intl.message(
      'Message...',
      name: 'send_mes_hint',
      desc: '',
      args: [],
    );
  }

  /// `Saved`
  String get personal_title1 {
    return Intl.message(
      'Saved',
      name: 'personal_title1',
      desc: '',
      args: [],
    );
  }

  /// `Travel expert`
  String get personal_title2 {
    return Intl.message(
      'Travel expert',
      name: 'personal_title2',
      desc: '',
      args: [],
    );
  }

  /// `Login Failed`
  String get login_fail_title {
    return Intl.message(
      'Login Failed',
      name: 'login_fail_title',
      desc: '',
      args: [],
    );
  }

  /// `Login failed. Please try again later.`
  String get login_fail_hint {
    return Intl.message(
      'Login failed. Please try again later.',
      name: 'login_fail_hint',
      desc: '',
      args: [],
    );
  }

  /// `Incorrect Password`
  String get login_fail_wrong_password_title {
    return Intl.message(
      'Incorrect Password',
      name: 'login_fail_wrong_password_title',
      desc: '',
      args: [],
    );
  }

  /// `At least 6 characters.`
  String get login_fail_wrong_password_content {
    return Intl.message(
      'At least 6 characters.',
      name: 'login_fail_wrong_password_content',
      desc: '',
      args: [],
    );
  }

  /// `Login Failed`
  String get login_fail_pwd_title {
    return Intl.message(
      'Login Failed',
      name: 'login_fail_pwd_title',
      desc: '',
      args: [],
    );
  }

  /// `Incorrect password`
  String get login_fail_pwd_hint {
    return Intl.message(
      'Incorrect password',
      name: 'login_fail_pwd_hint',
      desc: '',
      args: [],
    );
  }

  /// `Login Failed`
  String get login_fail_no_user_title {
    return Intl.message(
      'Login Failed',
      name: 'login_fail_no_user_title',
      desc: '',
      args: [],
    );
  }

  /// `Couldn't find your account. Please sign up first.`
  String get login_fail_no_user_hint {
    return Intl.message(
      'Couldn\'t find your account. Please sign up first.',
      name: 'login_fail_no_user_hint',
      desc: '',
      args: [],
    );
  }

  /// `Invalid email format`
  String get login_fail_email_format_hint {
    return Intl.message(
      'Invalid email format',
      name: 'login_fail_email_format_hint',
      desc: '',
      args: [],
    );
  }

  /// `Sign Up Failed`
  String get register_fail_title {
    return Intl.message(
      'Sign Up Failed',
      name: 'register_fail_title',
      desc: '',
      args: [],
    );
  }

  /// `Sign up failed. Please try again later.`
  String get register_fail_hint {
    return Intl.message(
      'Sign up failed. Please try again later.',
      name: 'register_fail_hint',
      desc: '',
      args: [],
    );
  }

  /// `By signing up or logging in, you agree to CTour's `
  String get register_agree {
    return Intl.message(
      'By signing up or logging in, you agree to CTour\'s ',
      name: 'register_agree',
      desc: '',
      args: [],
    );
  }

  /// `Terms of Service`
  String get register_service {
    return Intl.message(
      'Terms of Service',
      name: 'register_service',
      desc: '',
      args: [],
    );
  }

  /// `and`
  String get register_and {
    return Intl.message(
      'and',
      name: 'register_and',
      desc: '',
      args: [],
    );
  }

  /// `Privacy Policy`
  String get register_privacy {
    return Intl.message(
      'Privacy Policy',
      name: 'register_privacy',
      desc: '',
      args: [],
    );
  }

  /// `Last used `
  String get login_choose_lasttime_type {
    return Intl.message(
      'Last used ',
      name: 'login_choose_lasttime_type',
      desc: '',
      args: [],
    );
  }

  /// `Home`
  String get tab_home {
    return Intl.message(
      'Home',
      name: 'tab_home',
      desc: '',
      args: [],
    );
  }

  /// `Experts`
  String get tab_guilder {
    return Intl.message(
      'Experts',
      name: 'tab_guilder',
      desc: '',
      args: [],
    );
  }

  /// `Chats`
  String get tab_mess {
    return Intl.message(
      'Chats',
      name: 'tab_mess',
      desc: '',
      args: [],
    );
  }

  /// `Profile`
  String get tab_personal {
    return Intl.message(
      'Profile',
      name: 'tab_personal',
      desc: '',
      args: [],
    );
  }

  /// `OK`
  String get confirm {
    return Intl.message(
      'OK',
      name: 'confirm',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Warning`
  String get warning {
    return Intl.message(
      'Warning',
      name: 'warning',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete your account?`
  String get deleteAccount {
    return Intl.message(
      'Are you sure you want to delete your account?',
      name: 'deleteAccount',
      desc: '',
      args: [],
    );
  }

  /// `Notifications`
  String get personal_title3 {
    return Intl.message(
      'Notifications',
      name: 'personal_title3',
      desc: '',
      args: [],
    );
  }

  /// `Rate our App`
  String get personal_title4 {
    return Intl.message(
      'Rate our App',
      name: 'personal_title4',
      desc: '',
      args: [],
    );
  }

  /// `Feedback`
  String get personal_title5 {
    return Intl.message(
      'Feedback',
      name: 'personal_title5',
      desc: '',
      args: [],
    );
  }

  /// `Terms of Service`
  String get personal_title6 {
    return Intl.message(
      'Terms of Service',
      name: 'personal_title6',
      desc: '',
      args: [],
    );
  }

  /// `Privacy Policy`
  String get personal_title7 {
    return Intl.message(
      'Privacy Policy',
      name: 'personal_title7',
      desc: '',
      args: [],
    );
  }

  /// `Delete account`
  String get personal_title8 {
    return Intl.message(
      'Delete account',
      name: 'personal_title8',
      desc: '',
      args: [],
    );
  }

  /// `Delete account`
  String get personal_title9 {
    return Intl.message(
      'Delete account',
      name: 'personal_title9',
      desc: '',
      args: [],
    );
  }

  /// `Log Out`
  String get personal_logout {
    return Intl.message(
      'Log Out',
      name: 'personal_logout',
      desc: '',
      args: [],
    );
  }

  /// `You'll need to enter your username and password next time you want to log in.\n(;´༎ຶД༎ຶ')`
  String get personal_logout_content {
    return Intl.message(
      'You\'ll need to enter your username and password next time you want to log in.\n(;´༎ຶД༎ຶ`)',
      name: 'personal_logout_content',
      desc: '',
      args: [],
    );
  }

  /// `My articles`
  String get personal_my_travel {
    return Intl.message(
      'My articles',
      name: 'personal_my_travel',
      desc: '',
      args: [],
    );
  }

  /// `Search articles`
  String get personal_my_travel_search_hint {
    return Intl.message(
      'Search articles',
      name: 'personal_my_travel_search_hint',
      desc: '',
      args: [],
    );
  }

  /// `Posted`
  String get personal_my_travel_posted {
    return Intl.message(
      'Posted',
      name: 'personal_my_travel_posted',
      desc: '',
      args: [],
    );
  }

  /// `Drafts`
  String get personal_my_travel_not_posted {
    return Intl.message(
      'Drafts',
      name: 'personal_my_travel_not_posted',
      desc: '',
      args: [],
    );
  }

  /// `Title`
  String get personal_my_travel_title {
    return Intl.message(
      'Title',
      name: 'personal_my_travel_title',
      desc: '',
      args: [],
    );
  }

  /// `Enter title here`
  String get personal_my_travel_title_hint {
    return Intl.message(
      'Enter title here',
      name: 'personal_my_travel_title_hint',
      desc: '',
      args: [],
    );
  }

  /// `Enter content here`
  String get personal_my_travel_content_hint {
    return Intl.message(
      'Enter content here',
      name: 'personal_my_travel_content_hint',
      desc: '',
      args: [],
    );
  }

  /// `Cover photo`
  String get personal_my_travel_title_img {
    return Intl.message(
      'Cover photo',
      name: 'personal_my_travel_title_img',
      desc: '',
      args: [],
    );
  }

  /// `Theme`
  String get personal_my_travel_theme {
    return Intl.message(
      'Theme',
      name: 'personal_my_travel_theme',
      desc: '',
      args: [],
    );
  }

  /// `-day`
  String get personal_my_travel_day {
    return Intl.message(
      '-day',
      name: 'personal_my_travel_day',
      desc: '',
      args: [],
    );
  }

  /// ` | Itinerary`
  String get personal_my_travel_journey {
    return Intl.message(
      ' | Itinerary',
      name: 'personal_my_travel_journey',
      desc: '',
      args: [],
    );
  }

  /// `Food`
  String get personal_my_travel_foody {
    return Intl.message(
      'Food',
      name: 'personal_my_travel_foody',
      desc: '',
      args: [],
    );
  }

  /// `Others`
  String get personal_my_travel_other {
    return Intl.message(
      'Others',
      name: 'personal_my_travel_other',
      desc: '',
      args: [],
    );
  }

  /// `Introduction`
  String get personal_my_travel_description {
    return Intl.message(
      'Introduction',
      name: 'personal_my_travel_description',
      desc: '',
      args: [],
    );
  }

  /// `Hashtag`
  String get personal_my_travel_travel_tag {
    return Intl.message(
      'Hashtag',
      name: 'personal_my_travel_travel_tag',
      desc: '',
      args: [],
    );
  }

  /// `Article`
  String get personal_my_travel_article {
    return Intl.message(
      'Article',
      name: 'personal_my_travel_article',
      desc: '',
      args: [],
    );
  }

  /// `Label`
  String get personal_my_travel_tag {
    return Intl.message(
      'Label',
      name: 'personal_my_travel_tag',
      desc: '',
      args: [],
    );
  }

  /// `Paragraph`
  String get personal_my_travel_paragraph {
    return Intl.message(
      'Paragraph',
      name: 'personal_my_travel_paragraph',
      desc: '',
      args: [],
    );
  }

  /// `Hashtag`
  String get personal_my_travel_tag_title {
    return Intl.message(
      'Hashtag',
      name: 'personal_my_travel_tag_title',
      desc: '',
      args: [],
    );
  }

  /// `Hashtag`
  String get personal_my_travel_tag_title2 {
    return Intl.message(
      'Hashtag',
      name: 'personal_my_travel_tag_title2',
      desc: '',
      args: [],
    );
  }

  /// `Enter hashtag here`
  String get personal_my_travel_tag_title_hint {
    return Intl.message(
      'Enter hashtag here',
      name: 'personal_my_travel_tag_title_hint',
      desc: '',
      args: [],
    );
  }

  /// `Content`
  String get personal_my_travel_content_title {
    return Intl.message(
      'Content',
      name: 'personal_my_travel_content_title',
      desc: '',
      args: [],
    );
  }

  /// `Max. 500 characters`
  String get personal_my_travel_description_hint {
    return Intl.message(
      'Max. 500 characters',
      name: 'personal_my_travel_description_hint',
      desc: '',
      args: [],
    );
  }

  /// `Photo`
  String get personal_my_travel_image {
    return Intl.message(
      'Photo',
      name: 'personal_my_travel_image',
      desc: '',
      args: [],
    );
  }

  /// `Google Maps`
  String get personal_my_travel_google {
    return Intl.message(
      'Google Maps',
      name: 'personal_my_travel_google',
      desc: '',
      args: [],
    );
  }

  /// `Google Maps Link`
  String get personal_my_travel_google_link {
    return Intl.message(
      'Google Maps Link',
      name: 'personal_my_travel_google_link',
      desc: '',
      args: [],
    );
  }

  /// `Structure Reverse`
  String get personal_my_travel_warning_structure_title {
    return Intl.message(
      'Structure Reverse',
      name: 'personal_my_travel_warning_structure_title',
      desc: '',
      args: [],
    );
  }

  /// `At least one label be placed before paragraphs`
  String get personal_my_travel_warning_structure_hint {
    return Intl.message(
      'At least one label be placed before paragraphs',
      name: 'personal_my_travel_warning_structure_hint',
      desc: '',
      args: [],
    );
  }

  /// `Invalid Link`
  String get personal_my_travel_google_error {
    return Intl.message(
      'Invalid Link',
      name: 'personal_my_travel_google_error',
      desc: '',
      args: [],
    );
  }

  /// `The link has an invalid format. Please try again.`
  String get personal_my_travel_google_error_hint {
    return Intl.message(
      'The link has an invalid format. Please try again.',
      name: 'personal_my_travel_google_error_hint',
      desc: '',
      args: [],
    );
  }

  /// `Cover Photo Field Is Required`
  String get personal_my_travel_no_img_title {
    return Intl.message(
      'Cover Photo Field Is Required',
      name: 'personal_my_travel_no_img_title',
      desc: '',
      args: [],
    );
  }

  /// `Please upload a cover photo.`
  String get personal_my_travel_no_img_hint {
    return Intl.message(
      'Please upload a cover photo.',
      name: 'personal_my_travel_no_img_hint',
      desc: '',
      args: [],
    );
  }

  /// `Label Is Required`
  String get personal_my_travel_no_tag_title {
    return Intl.message(
      'Label Is Required',
      name: 'personal_my_travel_no_tag_title',
      desc: '',
      args: [],
    );
  }

  /// `Please new a label.`
  String get personal_my_travel_no_tag_hint {
    return Intl.message(
      'Please new a label.',
      name: 'personal_my_travel_no_tag_hint',
      desc: '',
      args: [],
    );
  }

  /// `Introduction Field Is Required`
  String get personal_my_travel_no_description_title {
    return Intl.message(
      'Introduction Field Is Required',
      name: 'personal_my_travel_no_description_title',
      desc: '',
      args: [],
    );
  }

  /// `Please fill in the field.`
  String get personal_my_travel_no_description_hint {
    return Intl.message(
      'Please fill in the field.',
      name: 'personal_my_travel_no_description_hint',
      desc: '',
      args: [],
    );
  }

  /// `Theme Field Is Required`
  String get personal_my_travel_no_theme_title {
    return Intl.message(
      'Theme Field Is Required',
      name: 'personal_my_travel_no_theme_title',
      desc: '',
      args: [],
    );
  }

  /// `Please choose a theme.`
  String get personal_my_travel_no_theme_hint {
    return Intl.message(
      'Please choose a theme.',
      name: 'personal_my_travel_no_theme_hint',
      desc: '',
      args: [],
    );
  }

  /// `Title Field Is Required`
  String get personal_my_travel_no_title_title {
    return Intl.message(
      'Title Field Is Required',
      name: 'personal_my_travel_no_title_title',
      desc: '',
      args: [],
    );
  }

  /// `Please fill in the field.`
  String get personal_my_travel_no_title_hint {
    return Intl.message(
      'Please fill in the field.',
      name: 'personal_my_travel_no_title_hint',
      desc: '',
      args: [],
    );
  }

  /// `Content Field Is Required`
  String get personal_my_travel_no_content_title {
    return Intl.message(
      'Content Field Is Required',
      name: 'personal_my_travel_no_content_title',
      desc: '',
      args: [],
    );
  }

  /// `Please fill in the field.`
  String get personal_my_travel_no_content_hint {
    return Intl.message(
      'Please fill in the field.',
      name: 'personal_my_travel_no_content_hint',
      desc: '',
      args: [],
    );
  }

  /// `Article Is Required`
  String get personal_my_travel_no_para_title {
    return Intl.message(
      'Article Is Required',
      name: 'personal_my_travel_no_para_title',
      desc: '',
      args: [],
    );
  }

  /// `Please new an article.`
  String get personal_my_travel_no_para_hint {
    return Intl.message(
      'Please new an article.',
      name: 'personal_my_travel_no_para_hint',
      desc: '',
      args: [],
    );
  }

  /// `Maximum characters exceeded`
  String get personal_my_travel_content_length_title {
    return Intl.message(
      'Maximum characters exceeded',
      name: 'personal_my_travel_content_length_title',
      desc: '',
      args: [],
    );
  }

  /// `Content limited to 500 characters.`
  String get personal_my_travel_content_length_hint {
    return Intl.message(
      'Content limited to 500 characters.',
      name: 'personal_my_travel_content_length_hint',
      desc: '',
      args: [],
    );
  }

  /// `Comment Is Required`
  String get personal_my_travel_comment_no_content_title {
    return Intl.message(
      'Comment Is Required',
      name: 'personal_my_travel_comment_no_content_title',
      desc: '',
      args: [],
    );
  }

  /// `Please new a comment.`
  String get personal_my_travel_comment_no_content_hint {
    return Intl.message(
      'Please new a comment.',
      name: 'personal_my_travel_comment_no_content_hint',
      desc: '',
      args: [],
    );
  }

  /// `Delete Article?`
  String get personal_my_travel_del_title {
    return Intl.message(
      'Delete Article?',
      name: 'personal_my_travel_del_title',
      desc: '',
      args: [],
    );
  }

  /// `It will be permanently deleted, and unable to undo it.`
  String get personal_my_travel_del_hint {
    return Intl.message(
      'It will be permanently deleted, and unable to undo it.',
      name: 'personal_my_travel_del_hint',
      desc: '',
      args: [],
    );
  }

  /// `No title`
  String get personal_my_travel_default_title {
    return Intl.message(
      'No title',
      name: 'personal_my_travel_default_title',
      desc: '',
      args: [],
    );
  }

  /// `Day 1`
  String get personal_travel_first_chapter {
    return Intl.message(
      'Day 1',
      name: 'personal_travel_first_chapter',
      desc: '',
      args: [],
    );
  }

  /// `Times Square`
  String get personal_travel_first_section {
    return Intl.message(
      'Times Square',
      name: 'personal_travel_first_section',
      desc: '',
      args: [],
    );
  }

  /// `No login Info.`
  String get login_first_title {
    return Intl.message(
      'No login Info.',
      name: 'login_first_title',
      desc: '',
      args: [],
    );
  }

  /// `Please log in or sign up first.`
  String get login_first_hint {
    return Intl.message(
      'Please log in or sign up first.',
      name: 'login_first_hint',
      desc: '',
      args: [],
    );
  }

  /// `Article Not Available`
  String get travel_article_not_found_title {
    return Intl.message(
      'Article Not Available',
      name: 'travel_article_not_found_title',
      desc: '',
      args: [],
    );
  }

  /// `This article has been deleted.`
  String get travel_article_not_found_hint1 {
    return Intl.message(
      'This article has been deleted.',
      name: 'travel_article_not_found_hint1',
      desc: '',
      args: [],
    );
  }

  /// `This article has been hidden.`
  String get travel_article_not_found_hint2 {
    return Intl.message(
      'This article has been hidden.',
      name: 'travel_article_not_found_hint2',
      desc: '',
      args: [],
    );
  }

  /// `Expert Not Found`
  String get travel_guilder_not_found_title {
    return Intl.message(
      'Expert Not Found',
      name: 'travel_guilder_not_found_title',
      desc: '',
      args: [],
    );
  }

  /// `This account has been deleted or the profile has been hidden.`
  String get travel_guilder_not_found_hint {
    return Intl.message(
      'This account has been deleted or the profile has been hidden.',
      name: 'travel_guilder_not_found_hint',
      desc: '',
      args: [],
    );
  }

  /// `Can't Delete This Item`
  String get travel_cant_delete {
    return Intl.message(
      'Can\'t Delete This Item',
      name: 'travel_cant_delete',
      desc: '',
      args: [],
    );
  }

  /// `Delete`
  String get delete {
    return Intl.message(
      'Delete',
      name: 'delete',
      desc: '',
      args: [],
    );
  }

  /// `Done`
  String get finish {
    return Intl.message(
      'Done',
      name: 'finish',
      desc: '',
      args: [],
    );
  }

  /// `Download Failed`
  String get download_fail_title {
    return Intl.message(
      'Download Failed',
      name: 'download_fail_title',
      desc: '',
      args: [],
    );
  }

  /// `Failed to Delete it`
  String get delete_fail_title {
    return Intl.message(
      'Failed to Delete it',
      name: 'delete_fail_title',
      desc: '',
      args: [],
    );
  }

  /// `Please try again later.`
  String get download_fail_hint {
    return Intl.message(
      'Please try again later.',
      name: 'download_fail_hint',
      desc: '',
      args: [],
    );
  }

  /// `Please try again later.`
  String get delete_fail_hint {
    return Intl.message(
      'Please try again later.',
      name: 'delete_fail_hint',
      desc: '',
      args: [],
    );
  }

  /// `Edit profile`
  String get personal_edit {
    return Intl.message(
      'Edit profile',
      name: 'personal_edit',
      desc: '',
      args: [],
    );
  }

  /// `Edit`
  String get personal_edit_avatar {
    return Intl.message(
      'Edit',
      name: 'personal_edit_avatar',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get personal_edit_name {
    return Intl.message(
      'Name',
      name: 'personal_edit_name',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get personal_edit_save {
    return Intl.message(
      'Save',
      name: 'personal_edit_save',
      desc: '',
      args: [],
    );
  }

  /// `Delete account`
  String get personal_edit_delete {
    return Intl.message(
      'Delete account',
      name: 'personal_edit_delete',
      desc: '',
      args: [],
    );
  }

  /// `Session Expired`
  String get personal_edit_delete_no_firebase_title {
    return Intl.message(
      'Session Expired',
      name: 'personal_edit_delete_no_firebase_title',
      desc: '',
      args: [],
    );
  }

  /// `Please log in again and delete later to ensure your account security.`
  String get personal_edit_delete_no_firebase_hint {
    return Intl.message(
      'Please log in again and delete later to ensure your account security.',
      name: 'personal_edit_delete_no_firebase_hint',
      desc: '',
      args: [],
    );
  }

  /// `Name Field is Required`
  String get personal_edit_name_empty_title {
    return Intl.message(
      'Name Field is Required',
      name: 'personal_edit_name_empty_title',
      desc: '',
      args: [],
    );
  }

  /// `Please fill in the field.`
  String get personal_edit_name_empty_hint {
    return Intl.message(
      'Please fill in the field.',
      name: 'personal_edit_name_empty_hint',
      desc: '',
      args: [],
    );
  }

  /// `Name Length is Too Long`
  String get personal_edit_name_overflow_title {
    return Intl.message(
      'Name Length is Too Long',
      name: 'personal_edit_name_overflow_title',
      desc: '',
      args: [],
    );
  }

  /// `Maximum 64 characters allowed.`
  String get personal_edit_name_overflow_hint {
    return Intl.message(
      'Maximum 64 characters allowed.',
      name: 'personal_edit_name_overflow_hint',
      desc: '',
      args: [],
    );
  }

  /// `Failed to Delete Account`
  String get personal_edit_delete_fail_title {
    return Intl.message(
      'Failed to Delete Account',
      name: 'personal_edit_delete_fail_title',
      desc: '',
      args: [],
    );
  }

  /// `Failed to delete account, please try again later.`
  String get personal_edit_delete_fail_hint {
    return Intl.message(
      'Failed to delete account, please try again later.',
      name: 'personal_edit_delete_fail_hint',
      desc: '',
      args: [],
    );
  }

  /// `Saved`
  String get personal_collection {
    return Intl.message(
      'Saved',
      name: 'personal_collection',
      desc: '',
      args: [],
    );
  }

  /// `Travel experts`
  String get personal_collection_tab1 {
    return Intl.message(
      'Travel experts',
      name: 'personal_collection_tab1',
      desc: '',
      args: [],
    );
  }

  /// `Articles`
  String get personal_collection_tab2 {
    return Intl.message(
      'Articles',
      name: 'personal_collection_tab2',
      desc: '',
      args: [],
    );
  }

  /// `Hashtag / Title / Travel expert`
  String get personal_collection_search_hint1 {
    return Intl.message(
      'Hashtag / Title / Travel expert',
      name: 'personal_collection_search_hint1',
      desc: '',
      args: [],
    );
  }

  /// `Travel expert / Interest`
  String get personal_collection_search_hint2 {
    return Intl.message(
      'Travel expert / Interest',
      name: 'personal_collection_search_hint2',
      desc: '',
      args: [],
    );
  }

  /// `Preview`
  String get personal_become_guilder_preview {
    return Intl.message(
      'Preview',
      name: 'personal_become_guilder_preview',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get personal_become_guilder_save {
    return Intl.message(
      'Save',
      name: 'personal_become_guilder_save',
      desc: '',
      args: [],
    );
  }

  /// `Post`
  String get personal_become_guilder_upload {
    return Intl.message(
      'Post',
      name: 'personal_become_guilder_upload',
      desc: '',
      args: [],
    );
  }

  /// `I'm a travel expert`
  String get personal_become_guilder_title {
    return Intl.message(
      'I\'m a travel expert',
      name: 'personal_become_guilder_title',
      desc: '',
      args: [],
    );
  }

  /// `Your interests`
  String get personal_become_guilder_location {
    return Intl.message(
      'Your interests',
      name: 'personal_become_guilder_location',
      desc: '',
      args: [],
    );
  }

  /// `About me`
  String get personal_become_guilder_about {
    return Intl.message(
      'About me',
      name: 'personal_become_guilder_about',
      desc: '',
      args: [],
    );
  }

  /// `Maximum 300 characters allowed.`
  String get personal_become_guilder_about_hint {
    return Intl.message(
      'Maximum 300 characters allowed.',
      name: 'personal_become_guilder_about_hint',
      desc: '',
      args: [],
    );
  }

  /// `Language`
  String get personal_become_guilder_lan {
    return Intl.message(
      'Language',
      name: 'personal_become_guilder_lan',
      desc: '',
      args: [],
    );
  }

  /// `Gender`
  String get personal_become_guilder_gender {
    return Intl.message(
      'Gender',
      name: 'personal_become_guilder_gender',
      desc: '',
      args: [],
    );
  }

  /// `I have vehicles`
  String get personal_become_guilder_trans {
    return Intl.message(
      'I have vehicles',
      name: 'personal_become_guilder_trans',
      desc: '',
      args: [],
    );
  }

  /// `Car`
  String get personal_become_guilder_trans1 {
    return Intl.message(
      'Car',
      name: 'personal_become_guilder_trans1',
      desc: '',
      args: [],
    );
  }

  /// `Scooter`
  String get personal_become_guilder_trans2 {
    return Intl.message(
      'Scooter',
      name: 'personal_become_guilder_trans2',
      desc: '',
      args: [],
    );
  }

  /// `Others`
  String get personal_become_guilder_trans3 {
    return Intl.message(
      'Others',
      name: 'personal_become_guilder_trans3',
      desc: '',
      args: [],
    );
  }

  /// `Review Password`
  String get personal_become_guilder_invite {
    return Intl.message(
      'Review Password',
      name: 'personal_become_guilder_invite',
      desc: '',
      args: [],
    );
  }

  /// `Please set your review password.`
  String get personal_become_guilder_invite_hint {
    return Intl.message(
      'Please set your review password.',
      name: 'personal_become_guilder_invite_hint',
      desc: '',
      args: [],
    );
  }

  /// `People who want to leave a review for you need to know your review password.`
  String get personal_become_guilder_invite_hint2 {
    return Intl.message(
      'People who want to leave a review for you need to know your review password.',
      name: 'personal_become_guilder_invite_hint2',
      desc: '',
      args: [],
    );
  }

  /// `Your interests`
  String get personal_become_add_location_title {
    return Intl.message(
      'Your interests',
      name: 'personal_become_add_location_title',
      desc: '',
      args: [],
    );
  }

  /// `Interests`
  String get personal_become_add_location_hint {
    return Intl.message(
      'Interests',
      name: 'personal_become_add_location_hint',
      desc: '',
      args: [],
    );
  }

  /// `Grand Canyon`
  String get personal_become_taipei {
    return Intl.message(
      'Grand Canyon',
      name: 'personal_become_taipei',
      desc: '',
      args: [],
    );
  }

  /// `road trip`
  String get personal_become_outside {
    return Intl.message(
      'road trip',
      name: 'personal_become_outside',
      desc: '',
      args: [],
    );
  }

  /// `Feedback from a CTourist`
  String get personal_mail_title {
    return Intl.message(
      'Feedback from a CTourist',
      name: 'personal_mail_title',
      desc: '',
      args: [],
    );
  }

  /// `Here are my suggestions for CTour...`
  String get personal_mail_hint {
    return Intl.message(
      'Here are my suggestions for CTour...',
      name: 'personal_mail_hint',
      desc: '',
      args: [],
    );
  }

  /// `Thanks a lot.`
  String get personal_no_email_title {
    return Intl.message(
      'Thanks a lot.',
      name: 'personal_no_email_title',
      desc: '',
      args: [],
    );
  }

  /// `Please send your seggestions to\nctourtw@gmail.com`
  String get personal_no_email_app {
    return Intl.message(
      'Please send your seggestions to\nctourtw@gmail.com',
      name: 'personal_no_email_app',
      desc: '',
      args: [],
    );
  }

  /// `You can't leave a review for yourself.`
  String get personnal_guilder_myself_hint {
    return Intl.message(
      'You can\'t leave a review for yourself.',
      name: 'personnal_guilder_myself_hint',
      desc: '',
      args: [],
    );
  }

  /// `Where to?`
  String get home_search_hint {
    return Intl.message(
      'Where to?',
      name: 'home_search_hint',
      desc: '',
      args: [],
    );
  }

  /// `Top Picks This Week`
  String get home_title1 {
    return Intl.message(
      'Top Picks This Week',
      name: 'home_title1',
      desc: '',
      args: [],
    );
  }

  /// `Explore With Travel Experts`
  String get home_title2 {
    return Intl.message(
      'Explore With Travel Experts',
      name: 'home_title2',
      desc: '',
      args: [],
    );
  }

  /// ` reviews`
  String get home_rate_number {
    return Intl.message(
      ' reviews',
      name: 'home_rate_number',
      desc: '',
      args: [],
    );
  }

  /// ` review`
  String get home_rate_number_single {
    return Intl.message(
      ' review',
      name: 'home_rate_number_single',
      desc: '',
      args: [],
    );
  }

  /// `Vehicle`
  String get home_have_car {
    return Intl.message(
      'Vehicle',
      name: 'home_have_car',
      desc: '',
      args: [],
    );
  }

  /// `No Vehicle`
  String get home_have_no_car {
    return Intl.message(
      'No Vehicle',
      name: 'home_have_no_car',
      desc: '',
      args: [],
    );
  }

  /// `Comment count`
  String get comment_num_title {
    return Intl.message(
      'Comment count',
      name: 'comment_num_title',
      desc: '',
      args: [],
    );
  }

  /// `invites you to read this article.`
  String get travel_share_title {
    return Intl.message(
      'invites you to read this article.',
      name: 'travel_share_title',
      desc: '',
      args: [],
    );
  }

  /// `views`
  String get travel_views {
    return Intl.message(
      'views',
      name: 'travel_views',
      desc: '',
      args: [],
    );
  }

  /// `view`
  String get travel_views_single {
    return Intl.message(
      'view',
      name: 'travel_views_single',
      desc: '',
      args: [],
    );
  }

  /// `comments`
  String get travel_comment {
    return Intl.message(
      'comments',
      name: 'travel_comment',
      desc: '',
      args: [],
    );
  }

  /// `comment`
  String get travel_comment_single {
    return Intl.message(
      'comment',
      name: 'travel_comment_single',
      desc: '',
      args: [],
    );
  }

  /// `write a comment...`
  String get travel_comment_edit_hint {
    return Intl.message(
      'write a comment...',
      name: 'travel_comment_edit_hint',
      desc: '',
      args: [],
    );
  }

  /// `Top Picks This Week`
  String get travel_title1 {
    return Intl.message(
      'Top Picks This Week',
      name: 'travel_title1',
      desc: '',
      args: [],
    );
  }

  /// `Where to go?`
  String get travel_title2 {
    return Intl.message(
      'Where to go?',
      name: 'travel_title2',
      desc: '',
      args: [],
    );
  }

  /// `Comments`
  String get travel_comment_title {
    return Intl.message(
      'Comments',
      name: 'travel_comment_title',
      desc: '',
      args: [],
    );
  }

  /// `popularity`
  String get travel_comment_filter_hot {
    return Intl.message(
      'popularity',
      name: 'travel_comment_filter_hot',
      desc: '',
      args: [],
    );
  }

  /// `time`
  String get travel_comment_filter_new {
    return Intl.message(
      'time',
      name: 'travel_comment_filter_new',
      desc: '',
      args: [],
    );
  }

  /// `Save Failed`
  String get travel_collect_fail_title {
    return Intl.message(
      'Save Failed',
      name: 'travel_collect_fail_title',
      desc: '',
      args: [],
    );
  }

  /// `Save failed, please try again later.`
  String get travel_collect_fail_hint {
    return Intl.message(
      'Save failed, please try again later.',
      name: 'travel_collect_fail_hint',
      desc: '',
      args: [],
    );
  }

  /// `Clear All`
  String get home_filter_clean {
    return Intl.message(
      'Clear All',
      name: 'home_filter_clean',
      desc: '',
      args: [],
    );
  }

  /// `Apply`
  String get home_filter_filter {
    return Intl.message(
      'Apply',
      name: 'home_filter_filter',
      desc: '',
      args: [],
    );
  }

  /// `Sort by`
  String get home_filter_order_method {
    return Intl.message(
      'Sort by',
      name: 'home_filter_order_method',
      desc: '',
      args: [],
    );
  }

  /// `Popularity`
  String get home_filter_order_method1 {
    return Intl.message(
      'Popularity',
      name: 'home_filter_order_method1',
      desc: '',
      args: [],
    );
  }

  /// `View count`
  String get home_filter_order_method2 {
    return Intl.message(
      'View count',
      name: 'home_filter_order_method2',
      desc: '',
      args: [],
    );
  }

  /// `Donate count`
  String get home_filter_order_method3 {
    return Intl.message(
      'Donate count',
      name: 'home_filter_order_method3',
      desc: '',
      args: [],
    );
  }

  /// `Author`
  String get home_filter_order_method4 {
    return Intl.message(
      'Author',
      name: 'home_filter_order_method4',
      desc: '',
      args: [],
    );
  }

  /// `Upload Date`
  String get home_filter_date {
    return Intl.message(
      'Upload Date',
      name: 'home_filter_date',
      desc: '',
      args: [],
    );
  }

  /// `Anytime`
  String get home_filter_date1 {
    return Intl.message(
      'Anytime',
      name: 'home_filter_date1',
      desc: '',
      args: [],
    );
  }

  /// `This week`
  String get home_filter_date2 {
    return Intl.message(
      'This week',
      name: 'home_filter_date2',
      desc: '',
      args: [],
    );
  }

  /// `This month`
  String get home_filter_date3 {
    return Intl.message(
      'This month',
      name: 'home_filter_date3',
      desc: '',
      args: [],
    );
  }

  /// `This year`
  String get home_filter_date4 {
    return Intl.message(
      'This year',
      name: 'home_filter_date4',
      desc: '',
      args: [],
    );
  }

  /// `Duration`
  String get home_filter_duration {
    return Intl.message(
      'Duration',
      name: 'home_filter_duration',
      desc: '',
      args: [],
    );
  }

  /// `0-day`
  String get home_filter_duration1 {
    return Intl.message(
      '0-day',
      name: 'home_filter_duration1',
      desc: '',
      args: [],
    );
  }

  /// `5-day`
  String get home_filter_duration2 {
    return Intl.message(
      '5-day',
      name: 'home_filter_duration2',
      desc: '',
      args: [],
    );
  }

  /// `Theme`
  String get home_filter_type {
    return Intl.message(
      'Theme',
      name: 'home_filter_type',
      desc: '',
      args: [],
    );
  }

  /// `Any`
  String get home_filter_type1 {
    return Intl.message(
      'Any',
      name: 'home_filter_type1',
      desc: '',
      args: [],
    );
  }

  /// `Itinerary`
  String get home_filter_type2 {
    return Intl.message(
      'Itinerary',
      name: 'home_filter_type2',
      desc: '',
      args: [],
    );
  }

  /// `-day | Itinerary`
  String get home_filter_type2_string {
    return Intl.message(
      '-day | Itinerary',
      name: 'home_filter_type2_string',
      desc: '',
      args: [],
    );
  }

  /// `Food`
  String get home_filter_type3 {
    return Intl.message(
      'Food',
      name: 'home_filter_type3',
      desc: '',
      args: [],
    );
  }

  /// `Others`
  String get home_filter_type4 {
    return Intl.message(
      'Others',
      name: 'home_filter_type4',
      desc: '',
      args: [],
    );
  }

  /// `Rating`
  String get guild_rate {
    return Intl.message(
      'Rating',
      name: 'guild_rate',
      desc: '',
      args: [],
    );
  }

  /// ` reviews`
  String get guild_comment_num {
    return Intl.message(
      ' reviews',
      name: 'guild_comment_num',
      desc: '',
      args: [],
    );
  }

  /// ` review`
  String get guild_comment_num_single {
    return Intl.message(
      ' review',
      name: 'guild_comment_num_single',
      desc: '',
      args: [],
    );
  }

  /// `0 review`
  String get guild_comment_num1 {
    return Intl.message(
      '0 review',
      name: 'guild_comment_num1',
      desc: '',
      args: [],
    );
  }

  /// `50 reviews`
  String get guild_comment_num2 {
    return Intl.message(
      '50 reviews',
      name: 'guild_comment_num2',
      desc: '',
      args: [],
    );
  }

  /// `Vehicle`
  String get guild_traffic {
    return Intl.message(
      'Vehicle',
      name: 'guild_traffic',
      desc: '',
      args: [],
    );
  }

  /// `Car`
  String get guild_traffic1 {
    return Intl.message(
      'Car',
      name: 'guild_traffic1',
      desc: '',
      args: [],
    );
  }

  /// `Scooter`
  String get guild_traffic2 {
    return Intl.message(
      'Scooter',
      name: 'guild_traffic2',
      desc: '',
      args: [],
    );
  }

  /// `Others`
  String get guild_traffic3 {
    return Intl.message(
      'Others',
      name: 'guild_traffic3',
      desc: '',
      args: [],
    );
  }

  /// `Gender`
  String get guild_gender {
    return Intl.message(
      'Gender',
      name: 'guild_gender',
      desc: '',
      args: [],
    );
  }

  /// `Both`
  String get guild_gender1 {
    return Intl.message(
      'Both',
      name: 'guild_gender1',
      desc: '',
      args: [],
    );
  }

  /// `Man`
  String get guild_gender2 {
    return Intl.message(
      'Man',
      name: 'guild_gender2',
      desc: '',
      args: [],
    );
  }

  /// `Woman`
  String get guild_gender3 {
    return Intl.message(
      'Woman',
      name: 'guild_gender3',
      desc: '',
      args: [],
    );
  }

  /// `Language`
  String get guild_language {
    return Intl.message(
      'Language',
      name: 'guild_language',
      desc: '',
      args: [],
    );
  }

  /// `About me`
  String get guilder_abountme {
    return Intl.message(
      'About me',
      name: 'guilder_abountme',
      desc: '',
      args: [],
    );
  }

  /// `more`
  String get guilder_more {
    return Intl.message(
      'more',
      name: 'guilder_more',
      desc: '',
      args: [],
    );
  }

  /// `Message`
  String get guilder_mess {
    return Intl.message(
      'Message',
      name: 'guilder_mess',
      desc: '',
      args: [],
    );
  }

  /// `Reviews`
  String get guilder_comment {
    return Intl.message(
      'Reviews',
      name: 'guilder_comment',
      desc: '',
      args: [],
    );
  }

  /// `Aritcles`
  String get guilder_travel {
    return Intl.message(
      'Aritcles',
      name: 'guilder_travel',
      desc: '',
      args: [],
    );
  }

  /// `Keyword, Title and More `
  String get guilder_travel_search_hint {
    return Intl.message(
      'Keyword, Title and More ',
      name: 'guilder_travel_search_hint',
      desc: '',
      args: [],
    );
  }

  /// `comments`
  String get guilder_travel_comment {
    return Intl.message(
      'comments',
      name: 'guilder_travel_comment',
      desc: '',
      args: [],
    );
  }

  /// `Anonymous Posting`
  String get guilder_add_comment_annoy {
    return Intl.message(
      'Anonymous Posting',
      name: 'guilder_add_comment_annoy',
      desc: '',
      args: [],
    );
  }

  /// `Rating`
  String get guilder_add_comment_rat {
    return Intl.message(
      'Rating',
      name: 'guilder_add_comment_rat',
      desc: '',
      args: [],
    );
  }

  /// `Review`
  String get guilder_add_comment_content {
    return Intl.message(
      'Review',
      name: 'guilder_add_comment_content',
      desc: '',
      args: [],
    );
  }

  /// `Photo`
  String get guilder_add_comment_pic {
    return Intl.message(
      'Photo',
      name: 'guilder_add_comment_pic',
      desc: '',
      args: [],
    );
  }

  /// `Write your review here...`
  String get guilder_add_comment_content_hint {
    return Intl.message(
      'Write your review here...',
      name: 'guilder_add_comment_content_hint',
      desc: '',
      args: [],
    );
  }

  /// `No Review`
  String get guilder_add_comment_no_content_title {
    return Intl.message(
      'No Review',
      name: 'guilder_add_comment_no_content_title',
      desc: '',
      args: [],
    );
  }

  /// `Review field is required`
  String get guilder_add_comment_no_content_hint {
    return Intl.message(
      'Review field is required',
      name: 'guilder_add_comment_no_content_hint',
      desc: '',
      args: [],
    );
  }

  /// `Please Enter\nReview Password`
  String get guilder_add_comment_invite_title {
    return Intl.message(
      'Please Enter\nReview Password',
      name: 'guilder_add_comment_invite_title',
      desc: '',
      args: [],
    );
  }

  /// `1234`
  String get guilder_add_comment_invite_content_hint {
    return Intl.message(
      '1234',
      name: 'guilder_add_comment_invite_content_hint',
      desc: '',
      args: [],
    );
  }

  /// `You need to know travel expert's review password to leave a review for him/her.`
  String get guilder_add_comment_invite_tool_hint {
    return Intl.message(
      'You need to know travel expert\'s review password to leave a review for him/her.',
      name: 'guilder_add_comment_invite_tool_hint',
      desc: '',
      args: [],
    );
  }

  /// `Incorrect Password`
  String get guilder_add_comment_invite_fail_title {
    return Intl.message(
      'Incorrect Password',
      name: 'guilder_add_comment_invite_fail_title',
      desc: '',
      args: [],
    );
  }

  /// `Please check review password again.`
  String get guilder_add_comment_invite_fail_hint {
    return Intl.message(
      'Please check review password again.',
      name: 'guilder_add_comment_invite_fail_hint',
      desc: '',
      args: [],
    );
  }

  /// `No Messages Yet`
  String get no_mess {
    return Intl.message(
      'No Messages Yet',
      name: 'no_mess',
      desc: '',
      args: [],
    );
  }

  /// `Photo`
  String get image {
    return Intl.message(
      'Photo',
      name: 'image',
      desc: '',
      args: [],
    );
  }

  /// `Yesterday`
  String get yesterday {
    return Intl.message(
      'Yesterday',
      name: 'yesterday',
      desc: '',
      args: [],
    );
  }

  /// `Monday`
  String get monday {
    return Intl.message(
      'Monday',
      name: 'monday',
      desc: '',
      args: [],
    );
  }

  /// `Tuesday`
  String get tuesday {
    return Intl.message(
      'Tuesday',
      name: 'tuesday',
      desc: '',
      args: [],
    );
  }

  /// `Wednesday`
  String get wednesday {
    return Intl.message(
      'Wednesday',
      name: 'wednesday',
      desc: '',
      args: [],
    );
  }

  /// `Thursday`
  String get thursday {
    return Intl.message(
      'Thursday',
      name: 'thursday',
      desc: '',
      args: [],
    );
  }

  /// `Friday`
  String get friday {
    return Intl.message(
      'Friday',
      name: 'friday',
      desc: '',
      args: [],
    );
  }

  /// `Saturday`
  String get saturday {
    return Intl.message(
      'Saturday',
      name: 'saturday',
      desc: '',
      args: [],
    );
  }

  /// `Sunday`
  String get sunday {
    return Intl.message(
      'Sunday',
      name: 'sunday',
      desc: '',
      args: [],
    );
  }

  /// `AM`
  String get am {
    return Intl.message(
      'AM',
      name: 'am',
      desc: '',
      args: [],
    );
  }

  /// `PM`
  String get pm {
    return Intl.message(
      'PM',
      name: 'pm',
      desc: '',
      args: [],
    );
  }

  /// `Remind`
  String get remind {
    return Intl.message(
      'Remind',
      name: 'remind',
      desc: '',
      args: [],
    );
  }

  /// `Preview`
  String get preview {
    return Intl.message(
      'Preview',
      name: 'preview',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get save {
    return Intl.message(
      'Save',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get save_draft {
    return Intl.message(
      'Save',
      name: 'save_draft',
      desc: '',
      args: [],
    );
  }

  /// `Your profile is hidden, the article feature can't be used, please turn off hidden button and try again`
  String get not_tourist {
    return Intl.message(
      'Your profile is hidden, the article feature can\'t be used, please turn off hidden button and try again',
      name: 'not_tourist',
      desc: '',
      args: [],
    );
  }

  /// `Anonymous`
  String get anonymous {
    return Intl.message(
      'Anonymous',
      name: 'anonymous',
      desc: '',
      args: [],
    );
  }

  /// `reviews`
  String get comment_num {
    return Intl.message(
      'reviews',
      name: 'comment_num',
      desc: '',
      args: [],
    );
  }

  /// `Take Photo`
  String get take_pic {
    return Intl.message(
      'Take Photo',
      name: 'take_pic',
      desc: '',
      args: [],
    );
  }

  /// `Photo Library`
  String get choose_pic {
    return Intl.message(
      'Photo Library',
      name: 'choose_pic',
      desc: '',
      args: [],
    );
  }

  /// `Update Failed`
  String get upload_fail_title {
    return Intl.message(
      'Update Failed',
      name: 'upload_fail_title',
      desc: '',
      args: [],
    );
  }

  /// `Try again later`
  String get upload_fail_hint {
    return Intl.message(
      'Try again later',
      name: 'upload_fail_hint',
      desc: '',
      args: [],
    );
  }

  /// `-`
  String get year {
    return Intl.message(
      '-',
      name: 'year',
      desc: '',
      args: [],
    );
  }

  /// `-`
  String get month {
    return Intl.message(
      '-',
      name: 'month',
      desc: '',
      args: [],
    );
  }

  /// ``
  String get day {
    return Intl.message(
      '',
      name: 'day',
      desc: '',
      args: [],
    );
  }

  /// `Yesterday`
  String get one_day_ago {
    return Intl.message(
      'Yesterday',
      name: 'one_day_ago',
      desc: '',
      args: [],
    );
  }

  /// `2 days ago`
  String get two_day_ago {
    return Intl.message(
      '2 days ago',
      name: 'two_day_ago',
      desc: '',
      args: [],
    );
  }

  /// `3 days ago`
  String get three_day_ago {
    return Intl.message(
      '3 days ago',
      name: 'three_day_ago',
      desc: '',
      args: [],
    );
  }

  /// `Unsaved Changes`
  String get save_not_yet {
    return Intl.message(
      'Unsaved Changes',
      name: 'save_not_yet',
      desc: '',
      args: [],
    );
  }

  /// `Do you want to save this post as a draft?`
  String get save_not_yet_hint {
    return Intl.message(
      'Do you want to save this post as a draft?',
      name: 'save_not_yet_hint',
      desc: '',
      args: [],
    );
  }

  /// `Do you want to save changes?`
  String get save_not_yet_hint2 {
    return Intl.message(
      'Do you want to save changes?',
      name: 'save_not_yet_hint2',
      desc: '',
      args: [],
    );
  }

  /// `Update`
  String get update {
    return Intl.message(
      'Update',
      name: 'update',
      desc: '',
      args: [],
    );
  }

  /// `Fail`
  String get fail {
    return Intl.message(
      'Fail',
      name: 'fail',
      desc: '',
      args: [],
    );
  }

  /// `Network error. Please try again later.`
  String get network_fail {
    return Intl.message(
      'Network error. Please try again later.',
      name: 'network_fail',
      desc: '',
      args: [],
    );
  }

  /// `Please try again later.`
  String get please_wait {
    return Intl.message(
      'Please try again later.',
      name: 'please_wait',
      desc: '',
      args: [],
    );
  }

  /// `'Language' is required`
  String get no_language {
    return Intl.message(
      '\'Language\' is required',
      name: 'no_language',
      desc: '',
      args: [],
    );
  }

  /// `Please add at least one language.`
  String get no_language_hint {
    return Intl.message(
      'Please add at least one language.',
      name: 'no_language_hint',
      desc: '',
      args: [],
    );
  }

  /// `'Interest', is required`
  String get no_trip {
    return Intl.message(
      '\'Interest\', is required',
      name: 'no_trip',
      desc: '',
      args: [],
    );
  }

  /// `Please add at least one interest.`
  String get no_trip_hint {
    return Intl.message(
      'Please add at least one interest.',
      name: 'no_trip_hint',
      desc: '',
      args: [],
    );
  }

  /// `Please add at least one vehicle.`
  String get no_trans_hint {
    return Intl.message(
      'Please add at least one vehicle.',
      name: 'no_trans_hint',
      desc: '',
      args: [],
    );
  }

  /// `Review password limited to 4 characters.`
  String get invite_code_extra {
    return Intl.message(
      'Review password limited to 4 characters.',
      name: 'invite_code_extra',
      desc: '',
      args: [],
    );
  }

  /// `Please upload at least one photo.`
  String get no_image_hint {
    return Intl.message(
      'Please upload at least one photo.',
      name: 'no_image_hint',
      desc: '',
      args: [],
    );
  }

  /// `'About me', limited to 500 characters.`
  String get about_me_extra {
    return Intl.message(
      '\'About me\', limited to 500 characters.',
      name: 'about_me_extra',
      desc: '',
      args: [],
    );
  }

  /// `Loading Failed`
  String get download_fail {
    return Intl.message(
      'Loading Failed',
      name: 'download_fail',
      desc: '',
      args: [],
    );
  }

  /// `Save Failed`
  String get set_favorite_fail {
    return Intl.message(
      'Save Failed',
      name: 'set_favorite_fail',
      desc: '',
      args: [],
    );
  }

  /// `No results found`
  String get search_not_found_title {
    return Intl.message(
      'No results found',
      name: 'search_not_found_title',
      desc: '',
      args: [],
    );
  }

  /// `Try different keywords or turn off some filters `
  String get search_not_found_hint {
    return Intl.message(
      'Try different keywords or turn off some filters ',
      name: 'search_not_found_hint',
      desc: '',
      args: [],
    );
  }

  /// `Check Email for Reset Link`
  String get forget_password_success_title {
    return Intl.message(
      'Check Email for Reset Link',
      name: 'forget_password_success_title',
      desc: '',
      args: [],
    );
  }

  /// `An email has been sent to your email address, and don't forget to check your spam folder.`
  String get forget_password_success_content {
    return Intl.message(
      'An email has been sent to your email address, and don\'t forget to check your spam folder.',
      name: 'forget_password_success_content',
      desc: '',
      args: [],
    );
  }

  /// `Invalid Email Format`
  String get forget_password_email_format_fail_title {
    return Intl.message(
      'Invalid Email Format',
      name: 'forget_password_email_format_fail_title',
      desc: '',
      args: [],
    );
  }

  /// `Please check your username and try again.`
  String get forget_password_email_format_fail_content {
    return Intl.message(
      'Please check your username and try again.',
      name: 'forget_password_email_format_fail_content',
      desc: '',
      args: [],
    );
  }

  /// `Incorrect Username`
  String get forget_password_email_noUser_fail_title {
    return Intl.message(
      'Incorrect Username',
      name: 'forget_password_email_noUser_fail_title',
      desc: '',
      args: [],
    );
  }

  /// `The username you entered doesn't appear to belong to an account. Sign up with this email?`
  String get forget_password_email_noUser_fail_content {
    return Intl.message(
      'The username you entered doesn\'t appear to belong to an account. Sign up with this email?',
      name: 'forget_password_email_noUser_fail_content',
      desc: '',
      args: [],
    );
  }

  /// `Weak Password`
  String get register_password_easy_title {
    return Intl.message(
      'Weak Password',
      name: 'register_password_easy_title',
      desc: '',
      args: [],
    );
  }

  /// `Your password should be minimum of 6 characters.`
  String get register_password_easy_content {
    return Intl.message(
      'Your password should be minimum of 6 characters.',
      name: 'register_password_easy_content',
      desc: '',
      args: [],
    );
  }

  /// `Incorrect Username`
  String get login_no_register_title {
    return Intl.message(
      'Incorrect Username',
      name: 'login_no_register_title',
      desc: '',
      args: [],
    );
  }

  /// `The username you entered doesn't appear to belong to an account. Go to sign up?`
  String get login_no_register_content {
    return Intl.message(
      'The username you entered doesn\'t appear to belong to an account. Go to sign up?',
      name: 'login_no_register_content',
      desc: '',
      args: [],
    );
  }

  /// `Delete Account?`
  String get delete_account_title {
    return Intl.message(
      'Delete Account?',
      name: 'delete_account_title',
      desc: '',
      args: [],
    );
  }

  /// `Are you sure you want to delete your account? Once you delete your account, it can't be restored.`
  String get delete_account_content {
    return Intl.message(
      'Are you sure you want to delete your account? Once you delete your account, it can\'t be restored.',
      name: 'delete_account_content',
      desc: '',
      args: [],
    );
  }

  /// `Account Disabled`
  String get account_unactivated_title {
    return Intl.message(
      'Account Disabled',
      name: 'account_unactivated_title',
      desc: '',
      args: [],
    );
  }

  /// `Your account has been disabled for violating our terms. Contact us:\nctourtw@gmail.com`
  String get account_unactivated_hint {
    return Intl.message(
      'Your account has been disabled for violating our terms. Contact us:\nctourtw@gmail.com',
      name: 'account_unactivated_hint',
      desc: '',
      args: [],
    );
  }

  /// `Log into Existing Account?`
  String get register_exit_title {
    return Intl.message(
      'Log into Existing Account?',
      name: 'register_exit_title',
      desc: '',
      args: [],
    );
  }

  /// `This email is on another account. You can log into the account associated with that email.`
  String get register_exit_content {
    return Intl.message(
      'This email is on another account. You can log into the account associated with that email.',
      name: 'register_exit_content',
      desc: '',
      args: [],
    );
  }

  /// `Continue`
  String get keep_going {
    return Intl.message(
      'Continue',
      name: 'keep_going',
      desc: '',
      args: [],
    );
  }

  /// `Name Field is Required`
  String get no_name_title {
    return Intl.message(
      'Name Field is Required',
      name: 'no_name_title',
      desc: '',
      args: [],
    );
  }

  /// `Please fill in the field`
  String get no_name_title_hint {
    return Intl.message(
      'Please fill in the field',
      name: 'no_name_title_hint',
      desc: '',
      args: [],
    );
  }

  /// `Download Failed`
  String get download_expert_fail {
    return Intl.message(
      'Download Failed',
      name: 'download_expert_fail',
      desc: '',
      args: [],
    );
  }

  /// `Please try again later.`
  String get download_expert_fail_hint {
    return Intl.message(
      'Please try again later.',
      name: 'download_expert_fail_hint',
      desc: '',
      args: [],
    );
  }

  /// `Saved Failed`
  String get send_like_fail_title {
    return Intl.message(
      'Saved Failed',
      name: 'send_like_fail_title',
      desc: '',
      args: [],
    );
  }

  /// `Please try again later.`
  String get send_like_fail_hint {
    return Intl.message(
      'Please try again later.',
      name: 'send_like_fail_hint',
      desc: '',
      args: [],
    );
  }

  /// `This Email is on Another Account`
  String get the_email_used_social {
    return Intl.message(
      'This Email is on Another Account',
      name: 'the_email_used_social',
      desc: '',
      args: [],
    );
  }

  /// `This email is associated with Facebook, please log in with Facebook.`
  String get the_email_used_social_fb {
    return Intl.message(
      'This email is associated with Facebook, please log in with Facebook.',
      name: 'the_email_used_social_fb',
      desc: '',
      args: [],
    );
  }

  /// `This email is associated with Apple, please log in with Apple.`
  String get the_email_used_social_apple {
    return Intl.message(
      'This email is associated with Apple, please log in with Apple.',
      name: 'the_email_used_social_apple',
      desc: '',
      args: [],
    );
  }

  /// `This Email is on Another Account", "This email is associated with Google, please log in with Google.`
  String get the_email_used_social_google {
    return Intl.message(
      'This Email is on Another Account", "This email is associated with Google, please log in with Google.',
      name: 'the_email_used_social_google',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get collect {
    return Intl.message(
      'Save',
      name: 'collect',
      desc: '',
      args: [],
    );
  }

  /// `Share`
  String get share {
    return Intl.message(
      'Share',
      name: 'share',
      desc: '',
      args: [],
    );
  }

  /// `Report`
  String get report {
    return Intl.message(
      'Report',
      name: 'report',
      desc: '',
      args: [],
    );
  }

  /// `Like`
  String get like {
    return Intl.message(
      'Like',
      name: 'like',
      desc: '',
      args: [],
    );
  }

  /// `Block`
  String get block {
    return Intl.message(
      'Block',
      name: 'block',
      desc: '',
      args: [],
    );
  }

  /// `Block`
  String get block_title {
    return Intl.message(
      'Block',
      name: 'block_title',
      desc: '',
      args: [],
    );
  }

  /// `Block`
  String get block_hint {
    return Intl.message(
      'Block',
      name: 'block_hint',
      desc: '',
      args: [],
    );
  }

  /// `Edit`
  String get edit {
    return Intl.message(
      'Edit',
      name: 'edit',
      desc: '',
      args: [],
    );
  }

  /// `Reason for report`
  String get report_reason {
    return Intl.message(
      'Reason for report',
      name: 'report_reason',
      desc: '',
      args: [],
    );
  }

  /// `False information`
  String get report_reason1 {
    return Intl.message(
      'False information',
      name: 'report_reason1',
      desc: '',
      args: [],
    );
  }

  /// `Slander, discriminate or provoke others`
  String get report_reason2 {
    return Intl.message(
      'Slander, discriminate or provoke others',
      name: 'report_reason2',
      desc: '',
      args: [],
    );
  }

  /// `Nudity or sexual activity`
  String get report_reason3 {
    return Intl.message(
      'Nudity or sexual activity',
      name: 'report_reason3',
      desc: '',
      args: [],
    );
  }

  /// `Intellectual property violation`
  String get report_reason4 {
    return Intl.message(
      'Intellectual property violation',
      name: 'report_reason4',
      desc: '',
      args: [],
    );
  }

  /// `Bullying or harassment`
  String get report_reason5 {
    return Intl.message(
      'Bullying or harassment',
      name: 'report_reason5',
      desc: '',
      args: [],
    );
  }

  /// `Sale of illegal or regulated goods`
  String get report_reason6 {
    return Intl.message(
      'Sale of illegal or regulated goods',
      name: 'report_reason6',
      desc: '',
      args: [],
    );
  }

  /// `Spamming or site interference`
  String get report_reason7 {
    return Intl.message(
      'Spamming or site interference',
      name: 'report_reason7',
      desc: '',
      args: [],
    );
  }

  /// `The topic of this article has nothing to do with travel`
  String get report_reason8 {
    return Intl.message(
      'The topic of this article has nothing to do with travel',
      name: 'report_reason8',
      desc: '',
      args: [],
    );
  }

  /// `Other reasons`
  String get report_reason9 {
    return Intl.message(
      'Other reasons',
      name: 'report_reason9',
      desc: '',
      args: [],
    );
  }

  /// `Report submitted.`
  String get report_res_title {
    return Intl.message(
      'Report submitted.',
      name: 'report_res_title',
      desc: '',
      args: [],
    );
  }

  /// `Thanks for your report, your report has been submitted.`
  String get report_res_content {
    return Intl.message(
      'Thanks for your report, your report has been submitted.',
      name: 'report_res_content',
      desc: '',
      args: [],
    );
  }

  /// `I'm trying to find travel buddies`
  String get personal_become_guilder_want_companion {
    return Intl.message(
      'I\'m trying to find travel buddies',
      name: 'personal_become_guilder_want_companion',
      desc: '',
      args: [],
    );
  }

  /// `I have my own transport, and I can carry my travel buddies.`
  String get personal_become_guilder_have_car {
    return Intl.message(
      'I have my own transport, and I can carry my travel buddies.',
      name: 'personal_become_guilder_have_car',
      desc: '',
      args: [],
    );
  }

  /// `Don't show me on CTour`
  String get personal_become_guilder_visible {
    return Intl.message(
      'Don\'t show me on CTour',
      name: 'personal_become_guilder_visible',
      desc: '',
      args: [],
    );
  }

  /// `Max. 64 characters`
  String get personal_become_guilder_name_hint {
    return Intl.message(
      'Max. 64 characters',
      name: 'personal_become_guilder_name_hint',
      desc: '',
      args: [],
    );
  }

  /// `Find travel buddies`
  String get gilder_filter_looking_companion {
    return Intl.message(
      'Find travel buddies',
      name: 'gilder_filter_looking_companion',
      desc: '',
      args: [],
    );
  }

  /// `Vehicle`
  String get gilder_filter_looking_companion_traffic {
    return Intl.message(
      'Vehicle',
      name: 'gilder_filter_looking_companion_traffic',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get new_register_name {
    return Intl.message(
      'Name',
      name: 'new_register_name',
      desc: '',
      args: [],
    );
  }

  /// `I am a`
  String get new_register_my_gender {
    return Intl.message(
      'I am a',
      name: 'new_register_my_gender',
      desc: '',
      args: [],
    );
  }

  /// `WOMAN`
  String get new_register_my_gender_female {
    return Intl.message(
      'WOMAN',
      name: 'new_register_my_gender_female',
      desc: '',
      args: [],
    );
  }

  /// `MAN`
  String get new_register_my_gender_male {
    return Intl.message(
      'MAN',
      name: 'new_register_my_gender_male',
      desc: '',
      args: [],
    );
  }

  /// `This won't appear on your profile`
  String get new_register_my_gender_hint {
    return Intl.message(
      'This won\'t appear on your profile',
      name: 'new_register_my_gender_hint',
      desc: '',
      args: [],
    );
  }

  /// `Languages`
  String get new_register_my_lan {
    return Intl.message(
      'Languages',
      name: 'new_register_my_lan',
      desc: '',
      args: [],
    );
  }

  /// `Let people who speak other languages to feel free to communicate with you`
  String get new_register_my_lan_hint {
    return Intl.message(
      'Let people who speak other languages to feel free to communicate with you',
      name: 'new_register_my_lan_hint',
      desc: '',
      args: [],
    );
  }

  /// `Interests`
  String get new_register_my_trip {
    return Intl.message(
      'Interests',
      name: 'new_register_my_trip',
      desc: '',
      args: [],
    );
  }

  /// `Let everyone know what you're interested in by adding it to your profile`
  String get new_register_my_trip_hint {
    return Intl.message(
      'Let everyone know what you\'re interested in by adding it to your profile',
      name: 'new_register_my_trip_hint',
      desc: '',
      args: [],
    );
  }

  /// `Add photos`
  String get new_register_photo {
    return Intl.message(
      'Add photos',
      name: 'new_register_photo',
      desc: '',
      args: [],
    );
  }

  /// `Add at least 2 photos to continue`
  String get new_register_photo_error {
    return Intl.message(
      'Add at least 2 photos to continue',
      name: 'new_register_photo_error',
      desc: '',
      args: [],
    );
  }

  /// `Add at least 2 photos to continue`
  String get new_register_photo_hint {
    return Intl.message(
      'Add at least 2 photos to continue',
      name: 'new_register_photo_hint',
      desc: '',
      args: [],
    );
  }

  /// `Max. 64 characters`
  String get new_register_name_hint {
    return Intl.message(
      'Max. 64 characters',
      name: 'new_register_name_hint',
      desc: '',
      args: [],
    );
  }

  /// `This is how it will appear in CTour, and you can edit it after registration.`
  String get new_register_name_hint2 {
    return Intl.message(
      'This is how it will appear in CTour, and you can edit it after registration.',
      name: 'new_register_name_hint2',
      desc: '',
      args: [],
    );
  }

  /// `NEXT`
  String get new_register_next {
    return Intl.message(
      'NEXT',
      name: 'new_register_next',
      desc: '',
      args: [],
    );
  }

  /// `START`
  String get new_register_start {
    return Intl.message(
      'START',
      name: 'new_register_start',
      desc: '',
      args: [],
    );
  }

  /// `Rate CTour`
  String get rate_title {
    return Intl.message(
      'Rate CTour',
      name: 'rate_title',
      desc: '',
      args: [],
    );
  }

  /// `Would you please leave us a review if you like this app?`
  String get rate_hint {
    return Intl.message(
      'Would you please leave us a review if you like this app?',
      name: 'rate_hint',
      desc: '',
      args: [],
    );
  }

  /// `Rate now`
  String get rate_btn {
    return Intl.message(
      'Rate now',
      name: 'rate_btn',
      desc: '',
      args: [],
    );
  }

  /// `Maybe later`
  String get rate_btn_2 {
    return Intl.message(
      'Maybe later',
      name: 'rate_btn_2',
      desc: '',
      args: [],
    );
  }

  /// `No thanks`
  String get rate_btn_3 {
    return Intl.message(
      'No thanks',
      name: 'rate_btn_3',
      desc: '',
      args: [],
    );
  }

  /// `Travel and Food Community`
  String get dynamic_title {
    return Intl.message(
      'Travel and Food Community',
      name: 'dynamic_title',
      desc: '',
      args: [],
    );
  }

  /// `Find Travel Buddies`
  String get find_companion {
    return Intl.message(
      'Find Travel Buddies',
      name: 'find_companion',
      desc: '',
      args: [],
    );
  }

  /// `照片最多三張`
  String get travel_img_max_title {
    return Intl.message(
      '照片最多三張',
      name: 'travel_img_max_title',
      desc: '',
      args: [],
    );
  }

  /// `請先刪除照片後再新增`
  String get travel_img_max_hint {
    return Intl.message(
      '請先刪除照片後再新增',
      name: 'travel_img_max_hint',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'zh', scriptCode: 'Hant'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}