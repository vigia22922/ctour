import 'dart:io';
import 'dart:ui';

import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'generated/l10n.dart';

class GlobalTask {

  BuildContext contextAll;
  Future<GuildClass> getGuild(BuildContext context, String id
      ) async {
    contextAll = context;
    GuildClass guildClass;
    print("getGuild");
    Loading.show(context);
    ResultData resultData = await AppApi.getInstance().getTourist(context, true, SharePre.prefs.getString(GDefine.user_id), id);
    Loading.dismiss(context);
    if(resultData.isSuccess()) {
      if (resultData.data['score'] == null) {
        resultData.data['score'] = "0.0";
        guildClass = GuildClass.fromJson(resultData.data);

      } else {

        guildClass = GuildClass.fromJson(resultData.data);

      }
    }else {
      WarningDialog.showIOSAlertDialogWithFunc(context, S().travel_guilder_not_found_title, S().travel_guilder_not_found_hint, S().confirm,backClick);
    }

    return guildClass;

  }

  void backClick( ) {
    print("back");

    if (Navigator.canPop(contextAll)) {
      Navigator.pop(contextAll);
    } else {
      SystemNavigator.pop();
    }
  }


}