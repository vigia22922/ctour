import 'dart:async';

import 'dart:io';
import 'dart:typed_data';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/BuildTitleEditText.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/OrangeXCross.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Lobby/LobbyPage.dart';
import 'package:ctour/Login/RegisterStatic.dart';
import 'package:ctour/Login/RegisterTripPage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/CropSample.dart';
import 'package:ctour/tool/CropTool.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_button/group_button.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:permission_handler/permission_handler.dart';

import 'dart:convert';
import 'package:dio/dio.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';

import '../res.dart';
import 'dart:math' as math; // import this
class RegisterLanPage extends StatefulWidget {





  // final String pwd;

  const RegisterLanPage(
      {Key key,


      })
      : super(key: key);


  @override
  State createState() {
    return _RegisterLanPageState();
  }
}

class _RegisterLanPageState extends State<RegisterLanPage> {



  GroupButtonController lansController;
  List<String> lansList = ["中文","English","日本語","한국어","Français","Deutsch","Bahasa Indonesia"];
  List<bool> lansSelList = [true,true,false,false,false,false,false];
  @override
  void initState() {
    lansController = GroupButtonController(

      // onDisablePressed: (index) => ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(content: Text('${_checkboxButtons[index]} is disabled')),
      // ),
    );
    lansController.selectIndex(0);
    lansController.selectIndex(1);

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.white,
        body: Scaffold(
            backgroundColor: OwnColors.white,
            body: GestureDetector(
              onTap: () {
                Tools().dismissK(context);
              },
              child:
              Align(
                alignment: Alignment.topCenter,
                child: SingleChildScrollView(
                  physics: ClampingScrollPhysics(),
                  child: Container(
                    width: ScreenSize().getWidth(context) * 0.92,
                    height: 1.sh,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ScreenSize().createTopPad(context),
                        HorizontalSpace().create(context, 0.01),
                        Container(
                          height: 0.06.sh,
                          child: Topbar().noTitleCreate(
                              context, ScreenSize().getHeight(context) * 0.073,
                              backClick),
                        ),
                        HorizontalSpace().create(context, 0.186),
                        Text(
                          S().new_register_my_lan,
                          style: GoogleFonts.inter(
                            fontSize: 24.nsp,
                            color: OwnColors.black,
                            fontWeight: FontWeight.w600,

                          ),
                        ),

                        HorizontalSpace().create(context, 0.076),
                        Align(
                          alignment: Alignment.center,
                          child: GroupButton(
                            controller: lansController,
                            isRadio: false,
                            onSelected: (val,index, isSelected) {
                              print("onSelected");
                              int check = 0;
                              for(bool value in lansSelList){
                                if(value){
                                  check++;
                                }
                              }

                              if(check == 1){
                                if(isSelected){

                                  lansSelList[index] = isSelected;
                                  print(lansSelList);
                                }
                              }else if(check > 1){

                                lansSelList[index] = !lansSelList[index];
                                print(lansSelList);
                              }
                              setState(() {

                              });

                            },

                            buttons: lansList,
                            buttonIndexedBuilder: (selected, index, context) {
                              return  lansSelList[index]?Padding(
                                padding: const EdgeInsets.only(left:8.0,right: 8),
                                child: Container(

                                    decoration: BoxDecoration(
                                        color: OwnColors.white,
                                        border: Border.all(color: OwnColors.CC8C8C8,width:0.5),
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(12),
                                            topRight: Radius.circular(12),
                                            bottomLeft: Radius.circular(12),
                                            bottomRight: Radius.circular(12)),
                                        boxShadow: [
                                          BoxShadow(
                                            color: OwnColors.primaryText.withOpacity(0.4),
                                            spreadRadius: 1,
                                            blurRadius: 4,
                                            offset: Offset(0, 3), // changes position of shadow
                                          ),
                                        ]),
                                    child: Padding(
                                      padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                      child: Text(lansList[index],
                                        textAlign: TextAlign.center,
                                        style: GoogleFonts.inter(
                                            fontWeight: FontWeight.w400,
                                            height: 1.3,
                                            fontSize: 14.nsp,
                                            color: OwnColors.black
                                        ),
                                      ),
                                    )),
                              ):
                              Padding(
                                padding: const EdgeInsets.only(left:8.0,right: 8),
                                child: Container(

                                    decoration: BoxDecoration(
                                      color: OwnColors.white,
                                      border: Border.all(color: OwnColors.CC8C8C8,width: 0.5),
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(12),
                                          topRight: Radius.circular(12),
                                          bottomLeft: Radius.circular(12),
                                          bottomRight: Radius.circular(12)),),
                                    child: Padding(
                                      padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                      child: Text(lansList[index],
                                        textAlign: TextAlign.center,
                                        style: GoogleFonts.inter(
                                            fontWeight: FontWeight.w400,
                                            height: 1.3,
                                            fontSize: 14.nsp,
                                            color: OwnColors.C989898
                                        ),
                                      ),
                                    )),
                              );

                            },
                            options: GroupButtonOptions(


                              borderRadius: BorderRadius.circular(8),
                              spacing: 0,
                              runSpacing: 16,
                              groupingType: GroupingType.wrap,
                              direction: Axis.horizontal,
                              buttonHeight: 60,
                              buttonWidth: 60,

                              mainGroupAlignment: MainGroupAlignment.center,
                              crossGroupAlignment: CrossGroupAlignment.center,
                              groupRunAlignment: GroupRunAlignment.center,
                              textAlign: TextAlign.center,
                              textPadding: EdgeInsets.only(left: 8,right: 8),
                              alignment: Alignment.centerLeft,
                              elevation: 0,
                            ),
                          ),
                        ),



                        HorizontalSpace().create(context, 0.047),
                        Container(
                          // height: 0.04.sh,
                          width: 0.68.sw,
                          child: Center(
                            child: Text(
                              S().new_register_my_lan_hint,
                              style: GoogleFonts.inter(
                                fontSize: 12.nsp,
                                color: OwnColors.C989898,
                                fontWeight: FontWeight.w400,

                              ),
                            ),
                          ),
                        ),


                        Spacer(),
                        Container(

                          width: ScreenSize().getWidth(context) * 0.7,
                          height: 0.045.sh,
                          child: Container(
                            // height : ScreenSize().getHeight(context)*0.08,
                            width: double.infinity,
                            child: FlatButton(
                              color: OwnColors.CTourOrange,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              textColor: OwnColors.white,
                              // elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: AutoSizeText(
                                  S().new_register_next,
                                  minFontSize: 10,
                                  maxFontSize: 30,
                                  style: GoogleFonts.inter(
                                    fontSize: 16.nsp,
                                    color: OwnColors.white,
                                    fontWeight: FontWeight.w300,
                                  ),
                                  maxLines: 1,
                                ),
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                                // side: BorderSide(color: Colors.red)
                              ),
                              onPressed: start,
                            ),
                          ),
                        ),

                        HorizontalSpace().create(context, 0.061),
                        Container(
                          // height: 0.04.sh,
                          width: 0.68.sw,
                          child: Center(
                            child: Text(
                              "4 / 5",
                              style: GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.C989898,
                                fontWeight: FontWeight.w700,

                              ),
                            ),
                          ),
                        ),
                        HorizontalSpace().create(context, 0.076),
                      ],
                    ),
                  ),
                ),
              ),
            )));
  }




  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }


  void start() {
    RegisterStatic.langs.clear();
    for (int i = 0 ; i< lansSelList.length ; i++){
      if(lansSelList[i]){
        RegisterStatic.langs.add(lansList[i]);
      }
    }

    print(RegisterStatic.langs);
    PushNewScreen().normalPush(context, RegisterTripPage());
  }


}
