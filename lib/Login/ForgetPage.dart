import 'dart:async';

import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:ctour/CreateWidget/BuildTitleEditText.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Login/LoginChoosePage.dart';
import 'package:ctour/Login/RegisterPage.dart';
import 'package:ctour/Login/RegisterPhotoPage.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:permission_handler/permission_handler.dart';

import 'dart:convert';
import 'package:dio/dio.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';

import '../res.dart';

class ForgetPage extends StatefulWidget {
  @override
  State createState() {
    return _ForgetPageState();
  }
}

class _ForgetPageState extends State {

  TextEditingController accountEdit, pwdEdit;

  Color borderColor = OwnColors.C989898;
  bool secure = true;
  int registerIndex = 0;

  @override
  void initState() {
    accountEdit = TextEditingController();
    pwdEdit = TextEditingController();

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.white,
        body: GestureDetector(
          onTap: (){
            Tools().dismissK(context);
          },
          child: Stack(
            children: [

              Align(
                alignment: Alignment.topCenter,
                child: SingleChildScrollView(
                  child: Container(
                    width: ScreenSize().getWidth(context) * 0.92,
                    height: 1.sh,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        //HorizontalSpace().create(context, 0.01),
                        ScreenSize().createTopPad(context),
                        Container(
                          height:  0.06.sh,
                          child: Topbar().noTitleCreate(context, ScreenSize().getHeight(context) * 0.073,  backClick,  ),
                        ),
                        Container(
                          height: 0.19.sh,
                          child: Container(
                            child: Image.asset(Res.forget_icon),
                          ),
                        ),
                        HorizontalSpace().create(context, 0.064),
                        Container(
                          width: 0.7 * ScreenSize().getWidth(context),
                          height: 0.1.sh,
                          child: BuildTitleEditText().create(
                            context,
                            S().email,
                            OwnColors.black,
                            1,
                            "example@mail.com",
                            false,
                            accountEdit,
                            ScreenSize().getHeight(context)*0.06,
                            TextInputType.text,
                            borderColor,

                          ),
                        ),
                        HorizontalSpace().create(context, 0.02),

                        HorizontalSpace().create(context, 0.006),

                        HorizontalSpace().create(context, 0.0237),
                        Container(
                          height: 0.05.sh,
                          width: ScreenSize().getWidth(context) * 0.7,
                          child: Column(
                            children: [
                              Expanded(
                                flex: 5,
                                child: Container(
                                  // height : ScreenSize().getHeight(context)*0.08,
                                  width: double.infinity,
                                  child: FlatButton(
                                    color: OwnColors.CTourOrange,
                                    splashColor: Colors.transparent,
                                    highlightColor: Colors.transparent,
                                    textColor: OwnColors.white,
                                    // elevation: 10,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: AutoSizeText(
                                        S().reset_pwd,
                                        minFontSize: 10,
                                        maxFontSize: 30,
                                        style:GoogleFonts.inter(
                                          fontSize: 16.nsp,
                                          color: OwnColors.white,
                                          fontWeight: FontWeight.w300,
                                        ),
                                        maxLines: 1,
                                      ),
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0),
                                      // side: BorderSide(color: Colors.red)
                                    ),
                                    onPressed: reset,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),

                       ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }


  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }




  Future<void> reset() async {
    if(accountEdit.text.contains("@")){
      Loading.show(context);
      try {
        List<String > a = await FirebaseAuth.instance.fetchSignInMethodsForEmail(  accountEdit.text);
        await FirebaseAuth.instance.sendPasswordResetEmail(email:  accountEdit.text,);
        Loading.dismiss(context);
        // WarningDialog.showIOSAlertDialog(context, "已發送Email", "請前往電子信箱重設密碼(請檢察垃圾郵件)", "確定");
        if(a.isEmpty){
          WarningDialog.showIOSAlertDialog(context, S().login_fail_no_user_hint, S().login_fail_no_user_hint, S().confirm);
        }else {
          //Loading.dismiss(context);
          if(a.contains("password")){
            WarningDialog.showIOSAlertDialog(context, S().forget_password_success_title, S().forget_password_success_content, S().confirm);
          }else if(a.contains("facebook.com")){
            WarningDialog.showIOSAlertDialog(context, S().the_email_used_social, S().the_email_used_social_fb, S().confirm);
          }else if(a.contains("apple.com")){
            WarningDialog.showIOSAlertDialog(context, S().the_email_used_social, S().the_email_used_social_apple, S().confirm);
          }else if(a.contains("google.com")){
            WarningDialog.showIOSAlertDialog(context, S().the_email_used_social, S().the_email_used_social_google, S().confirm);
          }
        }
      } on FirebaseAuthException catch (e) {
        Loading.dismiss(context);
        if (e.code == 'user-not-found') {
          WarningDialog.showIOSAlertDialog3(context, S().forget_password_email_noUser_fail_title, S().forget_password_email_noUser_fail_content, S().cancel, S().register, null, goRegister);
        } else if (e.code == 'email-already-in-use') {
          WarningDialog.showIOSAlertDialog(context, S().register_exit_content, "", S().confirm);
        }else if (e.code == 'invalid-email') {
          WarningDialog.showIOSAlertDialog(context, S().forget_password_email_format_fail_title, S().forget_password_email_format_fail_content, S().confirm);
        }



      }
    }else{
      WarningDialog.showIOSAlertDialog(context, S().forget_password_email_format_fail_title, S().forget_password_email_format_fail_content, S().confirm);
    }
    //
  //


  }

  void goRegister() {
    PushNewScreen().normalPush(context, RegisterPage(emailStr: accountEdit.text.toString(),));
  }

}
