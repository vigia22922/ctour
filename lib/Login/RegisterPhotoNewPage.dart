import 'dart:async';

import 'dart:io';
import 'dart:typed_data';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/BuildTitleEditText.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/OrangeXCross.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Lobby/LobbyPage.dart';
import 'package:ctour/Login/RegisterGenderPage.dart';
import 'package:ctour/Login/RegisterStatic.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/CropSample.dart';
import 'package:ctour/tool/CropTool.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:permission_handler/permission_handler.dart';

import 'dart:convert';
import 'package:dio/dio.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';

import '../res.dart';
import 'dart:math' as math; // import this
class RegisterPhotoNewPage extends StatefulWidget {





  // final String pwd;

  const RegisterPhotoNewPage(
      {Key key,


      })
      : super(key: key);


  @override
  State createState() {
    return _RegisterPhotoNewPageState();
  }
}

class _RegisterPhotoNewPageState extends State<RegisterPhotoNewPage> {



  Color borderColor = OwnColors.C989898;
  bool secure = true;
  int registerIndex = 1;

  String avatar = "";
  Uint8List avatarUnit8 ;
  String avatarUid = "";
  String avatarPath = "";
  String avatarFileName = "";

  String avatar_url="";
  List<String> imgList = [
    "",
    "",
    "",
    "",
    "",

  ];
  int imgIndex = 0;
  int currentIndexPage = 0;
  @override
  void initState() {


    super.initState();
  }

  @override
  void dispose() {
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.white,
        body: Scaffold(
            backgroundColor: OwnColors.white,
            body: GestureDetector(
              onTap: (){
                Tools().dismissK(context);
              },
              child:
              Align(
                alignment: Alignment.topCenter,
                child: SingleChildScrollView(
                  physics: ClampingScrollPhysics(),
                  child: Container(
                    width: ScreenSize().getWidth(context) * 0.92,
                    height: 1.sh,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ScreenSize().createTopPad(context),
                        HorizontalSpace().create(context, 0.01),
                        Container(
                          height: 0.06.sh,
                          child: Topbar().noTitleCreate(context, ScreenSize().getHeight(context) * 0.073,  backClick ),
                        ),
                        HorizontalSpace().create(context, 0.06),
                        Text(
                          S().new_register_photo,
                          style: GoogleFonts.inter(
                            fontSize: 24.nsp,
                            color: OwnColors.black,
                            fontWeight: FontWeight.w600,

                          ),
                        ),
                        HorizontalSpace().create(context, 0.01),
                        Text(
                          imgIndex . toString() +" / 2",
                          style: GoogleFonts.inter(
                            fontSize: 16.nsp,
                            color: OwnColors.C989898,
                            fontWeight: FontWeight.w700,

                          ),
                        ),
                        HorizontalSpace().create(context, 0.03),
                        Container(
                          width: 0.77 * ScreenSize().getWidth(context),
                          height: 0.77 * ScreenSize().getWidth(context),
                          child: Swiper(
                            loop: false,
                            itemBuilder: (BuildContext context, int index) {
                              return imgList[index].contains("http")
                                  ? Stack(
                                children: [
                                  GestureDetector(
                                    onTap: (){
                                      if(index != 0) {
                                        showBottomMenu();
                                        // imgIndex = index;
                                      }
                                    },
                                    child: Padding(
                                      padding:  EdgeInsets.only(top:0.01.sh,right: 0.01.sh),
                                      child: CachedNetworkImage(
                                        imageUrl: imgList[index],
                                        fit: BoxFit.fill,
                                        placeholder: (context, url) => AspectRatio(
                                          aspectRatio: 1,
                                          child: FractionallySizedBox(
                                            heightFactor: 0.5,
                                            widthFactor: 0.5,
                                            child: SpinKitRing(
                                              lineWidth:5,
                                              color:
                                              OwnColors.tran ,
                                            ),
                                          ),
                                        ),
                                        errorWidget: (context, url, error) =>
                                            Icon(
                                              Icons.error,
                                              color: OwnColors.black.withOpacity(0.5),
                                            ),
                                        imageBuilder: (context, imageProvider) =>
                                            Container(
                                              // width:0.92 * ScreenSize().getWidth(context),
                                              // height: 0.92 * ScreenSize().getWidth(context),
                                              decoration: BoxDecoration(
                                                color: OwnColors.tran,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(14)),
                                                image: DecorationImage(
                                                    image: imageProvider,
                                                    fit: BoxFit.cover),
                                              ),
                                            ),
                                        // width: MediaQuery.of(context).size.width,
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    top:0,
                                    right: 0,
                                    child: Opacity(
                                      opacity:  1.0,
                                      child: GestureDetector(
                                        onTap: (){

                                            imgList[index] = "";
                                            if(imgIndex != 0){
                                              imgIndex = imgIndex-1;
                                            }
                                            setState(() {

                                            });


                                        },
                                        child: OrangeXCross().create(context),
                                      ),
                                    ),
                                  ),


                                ],
                              )
                                  : GestureDetector(
                                onTap: ()  {
                                  print("onTap");
                                  showBottomMenu();
                                  // imgIndex = index ;
                                },
                                child: Container(
                                  width:0.9 * ScreenSize().getWidth(context),
                                  height: 0.9 * ScreenSize().getWidth(context),
                                  decoration: BoxDecoration(
                                    color: OwnColors.CF7F7F7,
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(14)),
                                  ),
                                  child:
                                  // bestTourList.length==0?

                                  Padding(
                                    padding: const EdgeInsets.only(left:0.0,right: 8),
                                    child: Column(
                                      children: [
                                        Spacer(),
                                        Expanded(
                                          child: Center(
                                            child: Stack(
                                                alignment: Alignment.topLeft,
                                                children:[
                                                  Transform(
                                                    alignment: Alignment.center,
                                                    transform: Matrix4.rotationY(math.pi),
                                                    child: Padding(
                                                      padding: const EdgeInsets.only(right: 6),
                                                      child: Icon(SFSymbols.photo_fill, color: OwnColors.CE6E1E1,size: ScreenSize().getWidth(context)*0.3,),
                                                    ),
                                                  ),
                                                  Container(
                                                    height: 0.10 * ScreenSize().getWidth(context),
                                                    width: 0.10 * ScreenSize().getWidth(context),
                                                    child: Padding(
                                                      padding: const EdgeInsets.only(top:10.0,right: 20),
                                                      child:Stack(
                                                        children: [
                                                          Image.asset(Res.add_fill),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ]
                                            ),
                                          ),
                                        ),
                                        Spacer(),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                            itemCount: imgList.length,
                            itemWidth: 300.0,
                            itemHeight: 200.0,
                            onIndexChanged: (index) {
                              currentIndexPage = index;
                              setState(() {});
                            },
//                             pagination: new SwiperPagination(
//                                 builder: new DotSwiperPaginationBuilder(
//                                   color: Colors.white,
//                                   activeColor: OwnColors.CTourOrange,
//                                 ),
// ),
                            // control: new SwiperControl(),
                          ),
                        ),


                        HorizontalSpace().create(context, 0.019),
                        DotsIndicator(
                          dotsCount: imgList.length,
                          position: currentIndexPage.toDouble(),
                          decorator: DotsDecorator(
                            color: OwnColors.CC8C8C8,
                            activeColor: OwnColors.CTourOrange,
                          ),
                        ),
                        HorizontalSpace().create(context, 0.047),
                        Container(
                          // height: 0.04.sh,
                          width: 0.68.sw,
                          child: Center(
                            child: Text(
                              S().new_register_photo_hint,
                              style: GoogleFonts.inter(
                                fontSize: 12.nsp,
                                color: OwnColors.C989898,
                                fontWeight: FontWeight.w400,

                              ),
                            ),
                          ),
                        ),


                        Spacer(),
                        Container(

                          width: ScreenSize().getWidth(context) * 0.7,
                          height: 0.045.sh,
                          child: Container(
                            // height : ScreenSize().getHeight(context)*0.08,
                            width: double.infinity,
                            child: FlatButton(
                              color: imgIndex < 2?  OwnColors.CC8C8C8 : OwnColors.CTourOrange,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              textColor: OwnColors.white,
                              // elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: AutoSizeText(
                                  S().new_register_next,
                                  minFontSize: 10,
                                  maxFontSize: 30,
                                  style:GoogleFonts.inter(
                                    fontSize: 16.nsp,
                                    color: OwnColors.white,
                                    fontWeight: FontWeight.w300,
                                  ),
                                  maxLines: 1,
                                ),
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                                // side: BorderSide(color: Colors.red)
                              ),
                              onPressed: start,
                            ),
                          ),
                        ),

                        HorizontalSpace().create(context, 0.061),
                        Container(
                          // height: 0.04.sh,
                          width: 0.68.sw,
                          child: Center(
                            child: Text(
                              "2 / 5",
                              style: GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.C989898,
                                fontWeight: FontWeight.w700,

                              ),
                            ),
                          ),
                        ),
                        HorizontalSpace().create(context, 0.076),
                      ],
                    ),
                  ),
                ),
              ),
            )));
  }

  void showBottomMenu() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 150,
              child: Column(children: <Widget>[
                ListTile(
                  leading: Icon(Icons.photo_camera),
                  title: Text(S().take_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromCamera();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.photo_library),
                  title: Text(S().choose_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromPhoto();
                  },
                )
              ]));
        });
  }
  final picker = ImagePicker();
  void pcikImageFromCamera( ) async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);

    File img = File(pickedFile.path);
    Uint8List imageBytes = await pickedFile.readAsBytes();

    CroppedFile  croppedFile = await CropTool().crop(1, 1, CropAspectRatioPreset.square, false, img.path);
    if(croppedFile != null) {
      imageBytes = await croppedFile.readAsBytes();
      avatar = img.path;
      avatarFileName = avatar
          .split("/")
          .last;
      avatarUnit8 = imageBytes;
      String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + base64Image);
      _uploadImage(base64Image);

      print(croppedFile);
    }



  }

  // void pcikImageFromCamera( ) async {
  //   PickedFile pickedFile = await picker.getImage(
  //       source: ImageSource.camera,
  //       maxHeight: 1440,
  //       maxWidth: 1440,
  //       imageQuality: 80);
  //
  //   File img = File(pickedFile.path);
  //   List<int> imageBytes = await img.readAsBytes();
  //   // img22.Image capturedImage =
  //   // img22.decodeImage(await File(pickedFile.path).readAsBytes());
  //   // img22.Image orientedImage = img22.bakeOrientation(capturedImage);
  //   // img = await File(pickedFile.path)
  //   //     .writeAsBytes(img22.encodeJpg(orientedImage));
  //   avatar = img.path;
  //   avatarFileName = avatar
  //       .split("/")
  //       .last;
  //   String base64Image = "data:image/jpg;base64," +base64Encode(imageBytes);
  //   print("base64Image: " + base64Image);
  //   _uploadImage(base64Image);
  //
  //   setState(() {
  //
  //   });
  //
  // }
  void pcikImageFromPhoto() async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);
    File img = File(pickedFile.path);
    Uint8List imageBytes = await pickedFile.readAsBytes();
    CroppedFile  croppedFile = await CropTool().crop(1, 1, CropAspectRatioPreset.square, false, img.path);
    if(croppedFile != null) {
      imageBytes = await croppedFile.readAsBytes();
      avatar = img.path;
      avatarFileName = avatar
          .split("/")
          .last;
      avatarUnit8 = imageBytes;
      String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + base64Image);
      _uploadImage(base64Image);

      print(croppedFile);
    }

  }

  // void pcikImageFromPhoto() async {
  //   PickedFile pickedFile = await picker.getImage(
  //       source: ImageSource.gallery,
  //       maxHeight: 1440,
  //       maxWidth: 1440,
  //       imageQuality: 80);
  //   File img = File(pickedFile.path);
  //   List<int> imageBytes = await img.readAsBytes();
  //   avatar = img.path;
  //   avatarFileName = avatar
  //       .split("/")
  //       .last;
  //
  //   String base64Image = "data:image/jpg;base64," +base64Encode(imageBytes);
  //   print("base64Image: " + base64Image);
  //   _uploadImage(base64Image);
  //
  //   setState(() {
  //
  //   });
  //
  // }

  Future<void> _uploadImage(String data)  async {
    // Loading.show(context);

    ResultData resultData = await AppApi.getInstance().setFileBase64(
        context,
        true,
        data
    );

    if (resultData.isSuccess()) {
      // Loading.dismiss(context);
      // Notice how you have to call body from the response if you are using http to retrieve json
        print(resultData.data);
        avatar_url = GDefine.imageUrl + resultData.data["path"];
        print("avatar_url " + avatar_url);
        print("imgIndex " + imgIndex.toString());
        imgList[currentIndexPage] = avatar_url;
        if(imgIndex < 5 ){
          imgIndex = imgIndex + 1;
        }
        setState(() {

        });

    } else {
      // Loading.dismiss(context);
      WarningDialog.showIOSAlertDialog(context, S().upload_fail_title, S().upload_fail_hint, S().confirm);

    }

  }





  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }



  void editAvatar() {
    showBottomMenu();
  }
  void start() {
    if (imgIndex >= 2){
      List<String> ii = new List.from(imgList);
      ii.removeWhere((element) => element == "");
      RegisterStatic.img = ii.toList();

      PushNewScreen().normalPush(context, RegisterGenderPage());
    }
  }


}
