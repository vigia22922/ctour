import 'dart:async';

import 'dart:io';
import 'dart:typed_data';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/BuildTitleEditText.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/OrangeXCross.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Lobby/LobbyPage.dart';
import 'package:ctour/Login/RegisterStatic.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/CropSample.dart';
import 'package:ctour/tool/CropTool.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:fading_edge_scrollview/fading_edge_scrollview.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_button/group_button.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:permission_handler/permission_handler.dart';

import 'dart:convert';
import 'package:dio/dio.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';

import '../res.dart';
import 'dart:math' as math; // import this
class RegisterTripPage extends StatefulWidget {





  // final String pwd;

  const RegisterTripPage(
      {Key key,


      })
      : super(key: key);


  @override
  State createState() {
    return _RegisterTripPageState();
  }
}

class _RegisterTripPageState extends State<RegisterTripPage> {


  final controller = ScrollController();
  GroupButtonController tripController;
  TextEditingController locationEdit = new TextEditingController();

  List<String> tripList = ["台北","台中","台南","單車旅行","紐約","巴黎","非洲""中南美洲","潛水","爬山",
    "咖啡館","美食","澎湖","SUP立槳","金門","夜市","宜蘭","墾丁","日本自由行","夜景行程","秋紅谷","日月潭","親山步道","海邊","公路旅行"
    ,"麻辣鍋","酒吧","拉麵","衝浪","展覽","露營","花蓮","溯溪","其他"];
  List<bool> tripSelList = [false,false,false,
    false,false,false,false,false,false,false,false,false,
    false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false
   ,false,false,false];
  @override
  void initState() {
    tripController = GroupButtonController(

      // onDisablePressed: (index) => ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(content: Text('${_checkboxButtons[index]} is disabled')),
      // ),
    );


    super.initState();
  }

  @override
  void dispose() {
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.white,
        body: Scaffold(
            backgroundColor: OwnColors.white,
            body: GestureDetector(
              onTap: () {
                Tools().dismissK(context);
              },
              child:
              Align(
                alignment: Alignment.topCenter,
                child: SingleChildScrollView(
                  physics: ClampingScrollPhysics(),
                  child: Container(
                    width: ScreenSize().getWidth(context) * 0.92,
                    height: 1.sh,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ScreenSize().createTopPad(context),
                        HorizontalSpace().create(context, 0.01),
                        Container(
                          height: 0.06.sh,
                          child: Topbar().noTitleCreate(
                              context, ScreenSize().getHeight(context) * 0.073,
                              backClick),
                        ),
                        HorizontalSpace().create(context, 0.047),
                        Text(
                          S().new_register_my_trip,
                          style: GoogleFonts.inter(
                            fontSize: 24.nsp,
                            color: OwnColors.black,
                            fontWeight: FontWeight.w600,

                          ),
                        ),
                        Container(height: 8,),
                        Container(
                          // height: 0.04.sh,
                          width: 0.68.sw,
                          child: Center(
                            child: Text(
                              getTrue().toString() + " / 2",
                              style: GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: getTrue()  >= 2? OwnColors.CTourOrange:OwnColors.C989898,
                                fontWeight: FontWeight.w700,

                              ),
                            ),
                          ),
                        ),

                        HorizontalSpace().create(context, 0.05),
                        Align(
                          alignment: Alignment.center,
                          child:  Container(
                            height: 0.306.sh,
                            width: 0.88.sw,
                            child: FadingEdgeScrollView.fromSingleChildScrollView(
                              child: SingleChildScrollView(
                                controller: controller,
                                physics: BouncingScrollPhysics(),
                                child: Padding(
                                  padding: const EdgeInsets.only(bottom: 8.0,top: 8.0),
                                  child: GroupButton(
                                    controller: tripController,
                                    isRadio: false,
                                    onSelected: (val,index, isSelected) {
                                      if(index == tripList.length-1){

                                        WarningDialog.showTextFieldDialog(context, S().personal_become_add_location_title,
                                            S().personal_become_add_location_title, S().personal_become_add_location_hint, locationEdit
                                            , addLocation);
                                      }else {
                                        print("onSelected");
                                        int check = 0;
                                        for (bool value in tripSelList) {
                                          if (value) {
                                            check++;
                                          }
                                        }

                                        tripSelList[index] = !tripSelList[index];
                                        print(tripSelList);

                                        setState(() {

                                        });
                                      }

                                    },

                                    buttons: tripList,
                                    buttonIndexedBuilder: (selected, index, context) {
                                      return
                                        index == tripList.length-1?
                                        Padding(
                                          padding: const EdgeInsets.only(left:8.0,right: 8),
                                          child: Container(

                                              decoration: BoxDecoration(
                                                  color: OwnColors.white,
                                                  border: Border.all(color: OwnColors.CTourOrange,width:0.5),
                                                  borderRadius: BorderRadius.only(
                                                      topLeft: Radius.circular(12),
                                                      topRight: Radius.circular(12),
                                                      bottomLeft: Radius.circular(12),
                                                      bottomRight: Radius.circular(12)), ),
                                              child: Padding(
                                                padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                                child: Wrap(
                                                  direction: Axis.horizontal,
                                                  alignment: WrapAlignment.center,
                                                  runAlignment : WrapAlignment.center,
                                                  crossAxisAlignment : WrapCrossAlignment.center,
                                                  children: [
                                                    Icon(Icons.add_rounded,color: OwnColors.CTourOrange,size: 17,),
                                                    Text(tripList[index],
                                                      textAlign: TextAlign.center,
                                                      style: GoogleFonts.inter(
                                                          fontWeight: FontWeight.w400,
                                                          height: 1.3,
                                                          fontSize: 14.nsp,
                                                          color: OwnColors.CTourOrange
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )),
                                        )

                                            :
                                        tripSelList[index]?
                                      Padding(
                                        padding: const EdgeInsets.only(left:8.0,right: 8),
                                        child: Container(

                                            decoration: BoxDecoration(
                                                color: OwnColors.white,
                                                border: Border.all(color: OwnColors.CC8C8C8,width:0.5),
                                                borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(12),
                                                    topRight: Radius.circular(12),
                                                    bottomLeft: Radius.circular(12),
                                                    bottomRight: Radius.circular(12)),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: OwnColors.primaryText.withOpacity(0.4),
                                                    spreadRadius: 1,
                                                    blurRadius: 4,
                                                    offset: Offset(0, 3), // changes position of shadow
                                                  ),
                                                ]),
                                            child: Padding(
                                              padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                              child: Text(tripList[index],
                                                textAlign: TextAlign.center,
                                                style: GoogleFonts.inter(
                                                    fontWeight: FontWeight.w400,
                                                    height: 1.3,
                                                    fontSize: 14.nsp,
                                                    color: OwnColors.black
                                                ),
                                              ),
                                            )),
                                      ):
                                      Padding(
                                        padding: const EdgeInsets.only(left:8.0,right: 8),
                                        child: Container(

                                            decoration: BoxDecoration(
                                              color: OwnColors.white,
                                              border: Border.all(color: OwnColors.CC8C8C8,width: 0.5),
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(12),
                                                  topRight: Radius.circular(12),
                                                  bottomLeft: Radius.circular(12),
                                                  bottomRight: Radius.circular(12)),),
                                            child: Padding(
                                              padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                              child: Text(tripList[index],
                                                textAlign: TextAlign.center,
                                                style: GoogleFonts.inter(
                                                    fontWeight: FontWeight.w400,
                                                    height: 1.3,
                                                    fontSize: 14.nsp,
                                                    color: OwnColors.C989898
                                                ),
                                              ),
                                            )),
                                      );

                                    },
                                    options: GroupButtonOptions(


                                      borderRadius: BorderRadius.circular(8),
                                      spacing: 0,
                                      runSpacing: 16,
                                      groupingType: GroupingType.wrap,
                                      direction: Axis.horizontal,
                                      buttonHeight: 60,
                                      buttonWidth: 60,

                                      mainGroupAlignment: MainGroupAlignment.center,
                                      crossGroupAlignment: CrossGroupAlignment.center,
                                      groupRunAlignment: GroupRunAlignment.center,
                                      textAlign: TextAlign.center,
                                      textPadding: EdgeInsets.only(left: 8,right: 8),
                                      alignment: Alignment.centerLeft,
                                      elevation: 0,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),



                        HorizontalSpace().create(context, 0.047),
                        Container(
                          // height: 0.04.sh,
                          width: 0.68.sw,
                          child: Center(
                            child: Text(
                              S().new_register_my_trip_hint,
                              style: GoogleFonts.inter(
                                fontSize: 12.nsp,
                                color: OwnColors.C989898,
                                fontWeight: FontWeight.w400,

                              ),
                            ),
                          ),
                        ),


                        Spacer(),
                        Container(

                          width: ScreenSize().getWidth(context) * 0.7,
                          height: 0.045.sh,
                          child: Container(
                            // height : ScreenSize().getHeight(context)*0.08,
                            width: double.infinity,
                            child: FlatButton(
                              color: getTrue() < 2?  OwnColors.CC8C8C8 : OwnColors.CTourOrange,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              textColor: OwnColors.white,
                              // elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: AutoSizeText(
                                  S().new_register_start,
                                  minFontSize: 10,
                                  maxFontSize: 30,
                                  style: GoogleFonts.inter(
                                    fontSize: 16.nsp,
                                    color: OwnColors.white,
                                    fontWeight: FontWeight.w300,
                                  ),
                                  maxLines: 1,
                                ),
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                                // side: BorderSide(color: Colors.red)
                              ),
                              onPressed: start,
                            ),
                          ),
                        ),

                        HorizontalSpace().create(context, 0.061),
                        Container(
                          // height: 0.04.sh,
                          width: 0.68.sw,
                          child: Center(
                            child: Text(
                              "5 / 5",
                              style: GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.C989898,
                                fontWeight: FontWeight.w700,

                              ),
                            ),
                          ),
                        ),
                        HorizontalSpace().create(context, 0.076),
                      ],
                    ),
                  ),
                ),
              ),
            )));
  }




  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }
  void addLocation() {
    tripSelList.insert(tripList.length-1, true );
    tripList.insert(tripList.length-1, locationEdit.text );

    locationEdit.text = "";

    print(tripSelList);
    setState(() {

    });

  }

  int getTrue(){
    return tripSelList.where((c) => c == true).toList().length;
  }
  void start() {

    if(getTrue() >=2) {
      RegisterStatic.locations.clear();
      for (int i = 0; i < tripList.length - 1; i++) {
        if (tripSelList[i]) {
          RegisterStatic.locations.add(tripList[i]);
        }
      }


      print(RegisterStatic.locations);


      setUserInfo( );
    }
  }
  Future<void> setUserInfo( )  async {
    Loading.show(context);

    ResultData resultData = await AppApi.getInstance().registerSet(
        context,
        true,

    );

    if (resultData.isSuccess()) {

      loginAPI();
    } else {
      Loading.dismiss(context);
      WarningDialog.showIOSAlertDialog(context, S().update+S().fail, S().update+S().fail, S().confirm);

    }

  }
  void loginAPI( ) async {
    print("loginAPI");

    ResultData resultData = await AppApi.getInstance().login(context, true,  SharePre.prefs.getString(GDefine.firebase_id), SharePre.prefs.getString(GDefine.login_type));
    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data.toString());

      SharePre.prefs.setString(GDefine.name, resultData.data["name"]);
      SharePre.prefs.setString(GDefine.user_id, resultData.data["user_id"].toString());
      SharePre.prefs.setString(GDefine.avatar, resultData.data["avatar"].toString());
      SharePre.prefs.setString(GDefine.gender, resultData.data["gender"].toString());

      SharePre.prefs.setString(GDefine.is_fcm_open, resultData.data["is_fcm_open"].toString());
      SharePre.prefs.setString(GDefine.description, resultData.data["description"].toString());
      SharePre.prefs.setString(GDefine.invite_code, resultData.data["invite_code"].toString());
      SharePre.prefs.setString(GDefine.want_companion, resultData.data["want_companion"].toString());

      List<dynamic> trans =  resultData.data["trans"];
      List<dynamic> location =  resultData.data["location"];
      List<dynamic> tourist_img =  resultData.data["tourist_img"];
      List<dynamic> lang =  resultData.data["lang"];
      SharePre.prefs.setString(GDefine.trans, trans.join(","));
      SharePre.prefs.setString(GDefine.location, location.join(","));
      SharePre.prefs.setString(GDefine.tourist_img, tourist_img.join(","));
      SharePre.prefs.setString(GDefine.lang, lang.join(","));

      String role_slug  = resultData.data["role_slug"].toString();
      if(role_slug.contains("guide")){
        SharePre.prefs.setString(GDefine.isGuilder, "true");
      }else {
        SharePre.prefs.setString(GDefine.isGuilder, "false");
      }

      Loading.dismiss(context);

      PushNewScreen().popAllAndPush(context, LobbyPage(articleID: "",guilderID: "",));

    }
  }


}
