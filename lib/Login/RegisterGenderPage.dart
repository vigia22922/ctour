import 'dart:async';

import 'dart:io';
import 'dart:typed_data';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/BuildTitleEditText.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/OrangeXCross.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Lobby/LobbyPage.dart';
import 'package:ctour/Login/RegisterLanPage.dart';
import 'package:ctour/Login/RegisterStatic.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/CropSample.dart';
import 'package:ctour/tool/CropTool.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:permission_handler/permission_handler.dart';

import 'dart:convert';
import 'package:dio/dio.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';

import '../res.dart';
import 'dart:math' as math; // import this
class RegisterGenderPage extends StatefulWidget {





  // final String pwd;

  const RegisterGenderPage(
      {Key key,


      })
      : super(key: key);


  @override
  State createState() {
    return _RegisterGenderPageState();
  }
}

class _RegisterGenderPageState extends State<RegisterGenderPage> {




  @override
  void initState() {


    super.initState();
  }

  @override
  void dispose() {
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.white,
        body: Scaffold(
            backgroundColor: OwnColors.white,
            body: GestureDetector(
              onTap: () {
                Tools().dismissK(context);
              },
              child:
              Align(
                alignment: Alignment.topCenter,
                child: SingleChildScrollView(
                  physics: ClampingScrollPhysics(),
                  child: Container(
                    width: ScreenSize().getWidth(context) * 0.92,
                    height: 1.sh,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ScreenSize().createTopPad(context),
                        HorizontalSpace().create(context, 0.01),
                        Container(
                          height: 0.06.sh,
                          child: Topbar().noTitleCreate(
                              context, ScreenSize().getHeight(context) * 0.073,
                              backClick),
                        ),
                        HorizontalSpace().create(context, 0.186),
                        Text(
                          S().new_register_my_gender,
                          style: GoogleFonts.inter(
                            fontSize: 24.nsp,
                            color: OwnColors.black,
                            fontWeight: FontWeight.w600,

                          ),
                        ),

                        HorizontalSpace().create(context, 0.076),


                        Container(

                          width: ScreenSize().getWidth(context) * 0.7,
                          height: 0.045.sh,
                          child: Container(
                            // height : ScreenSize().getHeight(context)*0.08,
                            width: double.infinity,
                            child: FlatButton(
                              color: OwnColors.white,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              textColor: OwnColors.white,

                              // elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: AutoSizeText(
                                  S().new_register_my_gender_male,
                                  minFontSize: 10,
                                  maxFontSize: 30,
                                  style: GoogleFonts.inter(
                                    fontSize: 16.nsp,
                                    color: RegisterStatic.gender.contains("m")?OwnColors.CTourOrange : OwnColors.C989898,
                                    fontWeight: RegisterStatic.gender.contains("m")?FontWeight.w700:FontWeight.w400,
                                  ),
                                  maxLines: 1,
                                ),
                              ),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0),
                                  side: BorderSide(color: RegisterStatic.gender.contains("m")?OwnColors.CTourOrange : OwnColors.C989898
                                      ,width: RegisterStatic.gender.contains("m")?2 : 1
                                  )
                              ),
                              onPressed: (){
                                RegisterStatic.gender = "m";
                                setState(() {

                                });
                              },
                            ),
                          ),
                        ),

                        HorizontalSpace().create(context, 0.019),

                        Container(

                          width: ScreenSize().getWidth(context) * 0.7,
                          height: 0.045.sh,
                          child: Container(
                            // height : ScreenSize().getHeight(context)*0.08,
                            width: double.infinity,
                            child: FlatButton(
                              color: OwnColors.white,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              textColor: OwnColors.white,

                              // elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: AutoSizeText(
                                  S().new_register_my_gender_female,
                                  minFontSize: 10,
                                  maxFontSize: 30,
                                  style: GoogleFonts.inter(
                                    fontSize: 16.nsp,
                                    color: RegisterStatic.gender.contains("f")?OwnColors.CTourOrange : OwnColors.C989898,
                                    fontWeight: RegisterStatic.gender.contains("f")?FontWeight.w700:FontWeight.w400,
                                  ),
                                  maxLines: 1,
                                ),
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                                side: BorderSide(color: RegisterStatic.gender.contains("f")?OwnColors.CTourOrange : OwnColors.C989898
                                    ,width: RegisterStatic.gender.contains("f")?2 : 1
                                ),

                              ),
                              onPressed: (){
                                RegisterStatic.gender = "f";
                                setState(() {

                                });
                              },
                            ),
                          ),
                        ),
                        HorizontalSpace().create(context, 0.047),
                        Container(
                          // height: 0.04.sh,
                          width: 0.68.sw,
                          child: Center(
                            child: Text(
                              S().new_register_my_gender_hint,
                              style: GoogleFonts.inter(
                                fontSize: 12.nsp,
                                color: OwnColors.C989898,
                                fontWeight: FontWeight.w400,

                              ),
                            ),
                          ),
                        ),


                        Spacer(),
                        Container(

                          width: ScreenSize().getWidth(context) * 0.7,
                          height: 0.045.sh,
                          child: Container(
                            // height : ScreenSize().getHeight(context)*0.08,
                            width: double.infinity,
                            child: FlatButton(
                              color: OwnColors.CTourOrange,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              textColor: OwnColors.white,
                              // elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: AutoSizeText(
                                  S().new_register_next,
                                  minFontSize: 10,
                                  maxFontSize: 30,
                                  style: GoogleFonts.inter(
                                    fontSize: 16.nsp,
                                    color: OwnColors.white,
                                    fontWeight: FontWeight.w300,
                                  ),
                                  maxLines: 1,
                                ),
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                                // side: BorderSide(color: Colors.red)
                              ),
                              onPressed: start,
                            ),
                          ),
                        ),

                        HorizontalSpace().create(context, 0.061),
                        Container(
                          // height: 0.04.sh,
                          width: 0.68.sw,
                          child: Center(
                            child: Text(
                              "3 / 5",
                              style: GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.C989898,
                                fontWeight: FontWeight.w700,

                              ),
                            ),
                          ),
                        ),
                        HorizontalSpace().create(context, 0.076),
                      ],
                    ),
                  ),
                ),
              ),
            )));
  }




  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }


  void start() {
    PushNewScreen().normalPush(context, RegisterLanPage());
  }


}
