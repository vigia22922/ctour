import 'package:auto_size_text/auto_size_text.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/tool/StatusTools.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/VerticalSpace.dart';
import 'RegisterPage.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';

class PrivacyPage extends StatelessWidget{



  @override
  Widget build(BuildContext context) {
    void backClick() {
      print("back");
      if (Navigator.canPop(context)) {
        Navigator.pop(context);
      } else {
        SystemNavigator.pop();
      }
    }
    return Scaffold(
      backgroundColor: OwnColors.white,
      body:Column(
        children: [
          ScreenSize().createTopPad(context),

          Topbar().noTitleBorderCreate(context, ScreenSize().getHeight(context) * 0.073,  backClick ),

          Flexible(

            child:SingleChildScrollView(

              child: SizedBox(
                width:ScreenSize().getWidth(context) * 0.95,
                child:Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      HorizontalSpace().create(context, 0.005),
                      ServiceTermsTextStyle().createTitle("CTour隱私權政策"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("""「CTour」是由「嘻途資訊有限公司」（下稱本公司）所經營之應用程式(下稱「CTour」)。以下是本公司的隱私權保護政策，幫助您瞭解CTour所蒐集的個人資料之運用及保護方式。"""),
                      ),
                      ServiceTermsTextStyle().createTitle("\n一、隱私權保護政策的適用範圍"),

                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("""(1)請您在於使用CTour服務前，確認您已審閱並同意本隱私權政策所列全部條款，若您不同意全部或部份者，則請勿使用CTour服務。
\n(2)隱私權保護政策內容，包括本公司如何處理您在使用CTour服務時蒐集到的個人識別資料。
\n(3)隱私權保護政策不適用於CTour以外的相關連結網站與應用程式，亦不適用於非本公司所委託或參與管理之人員。
"""),
                      ),
                      ServiceTermsTextStyle().createTitle("二、個人資料的蒐集及使用"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)在使用本公司的服務時，本公司可能會要求您向本公司提供可用於聯繫或識別您的某些個人資料，包括："),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(a)C001辨識個人者： 如姓名、地址、電話、電子郵件等資訊。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(b)C002辨識財務者： 如信用卡或金融機構帳戶資訊。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(c)C011個人描述：例如：性別、出生年月日等。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)CTour將蒐集的數據用於各種目的："),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(a)提供和維護系統所提供讀服務。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(b)提供用戶支持。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(c)提供分析或有價值訊息，以便本公司改進服務。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(d)監控服務的使用情況。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(e)檢測，預防和解決技術問題。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(3)CTour針對蒐集數據的利用期間、地區、對象及方式："),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(a)期間：當事人要求停止使用或本服務停止提供服務之日為止。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(b)地區：個人資料將用於台灣地區。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(c)利用對象及方式：所蒐集到的資料將利用於各項業務之執行，利用方式為因業務執行所必須進行之各項分析、聯繫及通知。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(4)為提升服務品質，CTour會於您使用時蒐集特定資訊，例如註冊資訊、裝置/定位/瀏覽資訊等等。以上資訊僅供CTour內部使用，除非依照相關法律規定，CTour不會將用戶註冊時所填寫的個人資料提供給第三方。"),
                      ),

                      ServiceTermsTextStyle().createTitle("\n三、資料的保護與安全"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)CTour將以合理技術盡力保障所有使用者的個人資料安全。只有經過授權的人員才能接觸您的個人資料。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)如因業務需要有必要委託CTour相關單位提供服務時，本公司會要求其遵守保密義務，並採取相當之檢查程序以確定其將確實遵守。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(3)請您妥善保管您的密碼與個人資料，並提醒您使用完畢CTour相關服務後，務必關閉CTour，以免您的資料遭人盜用。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(4)您同意在使用CTour服務時，所留存的資料與事實相符。若您發現您的個人資料遭他人非法使用或有任何異常時，應即時通知本公司。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(5)當您同意使用CTour服務時，所提供及使用之資料皆為合法，並無侵害第三人權利、違反第三方協議或涉及任何違法行為。如因使用CTour服務而致第三人損害，本公司並不負擔相關賠償責任。"),
                      ),
                      ServiceTermsTextStyle().createTitle("\n四、對外的相關連結"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("Tour上有可能包含其他合作網站或應用程式之連結，該網站或應用程式也有可能會蒐集您的個人資料，不論其內容或隱私權政策為何，皆與CTour無關，您應自行參考該連結網站或應用程式中的隱私權保護政策，本公司不負任何連帶責任。"),
                      ),
                      ServiceTermsTextStyle().createTitle("\n五、與第三人共用個人資料之政策"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)CTour絕不會提供、交換、出租或出售任何您的個人資料給其他個人、團體、私人企業或公務機關，但有法律依據或合約義務者，不在此限。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)前項但書之情形包括但不限於："),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(a)經由您書面同意。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(b)法律明文規定。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(c)為維護國家安全或增進公共利益。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(d)為免除您生命、身體、自由或財產上之危險。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(e)與公務機關或學術研究機構合作，基於公共利益為統計或學術研究而有必要，且資料經過提供者處理或蒐集者依其揭露方式無從識別特定之當事人。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(f)當您在CTour的行為，違反服務條款或可能損害或妨礙CTour與其他使用者權益或導致任何人遭受損害時，經CTour管理單位研析揭露您的個人資料是為了辨識、聯絡或採取法律行動所必要者。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(g)有利於您的權益。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(3)CTour委託廠商協助蒐集、處理或利用您的個人資料時，將對委外廠商或個人善盡監督管理之責。"),
                      ),

                      ServiceTermsTextStyle().createTitle("\n六、未成年人保護"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("未成年人於註冊或使用本服務而同意本公司蒐集、利用其個人資訊時，應按其年齡由其法定代理人代為或在法定代理人之同意下為之。"),
                      ),

                      ServiceTermsTextStyle().createTitle("\n七、隱私權政策的修訂"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("本公司將因應需求擁有隨時修改本隱私權保護政策的權利，當本公司做出修改時，會於CTour中的隱私權條款公告，且自公告日起生效，不再另行通知。若您於本政策修改後仍繼續使用本網站，視為您已同意接受本政策之修改及變更。"),
                      ),

                      ServiceTermsTextStyle().createDate("""\n訂定日：2022年7月8日
上一次修訂於：2022年7月8日"""),
                      HorizontalSpace().create(context, 0.05),
                      ScreenSize().createBottomPad(context),
                    ]),
              ),),
          ),
        ],
      ),
    );
  }

}
class ServiceTermsTextStyle{
  Text createTitle(text) {
    return Text("""$text""",
      textAlign: TextAlign.left,
      style: TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.bold,
      ),
    );
  }
  Text createContent(text) {
    return Text("""$text""",
      textAlign: TextAlign.left,
      style: TextStyle(
        fontSize: 14,
      ),
    );
  }

  Text createDate(text) {
    return Text("""$text""",
      textAlign: TextAlign.left,
      style: TextStyle(
        fontSize: 12,
        color: OwnColors.secondaryText
      ),
    );
  }


}