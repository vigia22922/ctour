import 'dart:async';

import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Login/LoginPage.dart';
import 'package:ctour/Login/RegisterPage.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/StatusTools.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../res.dart';

class LoginChoosePage extends StatefulWidget {


  @override
  State createState() {
    return _LoginChoosePageState();
  }
}

class _LoginChoosePageState extends State {


  TextEditingController accountEdit, pwdEdit;

  Color borderColor = OwnColors.tran;
  bool secure = true;

  bool go2register = false;

  String loginTypeString = "";

  @override
  void initState() {

    accountEdit = TextEditingController();
    pwdEdit = TextEditingController();

    if(SharePre.prefs.getString(GDefine.login_type) != null) {

      loginTypeString = SharePre.prefs.getString(GDefine.login_type) == "normal"?"Email ":SharePre.prefs.getString(GDefine.login_type) == "FB"?"Facebook ":SharePre.prefs.getString(GDefine.login_type) + " ";

  }

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    StatusTools().setTranBGWhiteText();
  }

  @override
  Widget build(BuildContext context) {
    return FocusDetector(
      onFocusGained: (){
        StatusTools().setTranBGWhiteText();
      },
      child: Scaffold(
          backgroundColor: OwnColors.white,
          body: Stack(
            children: [
              new Positioned.fill(
                  child: Container(
                      height: double.infinity,
                      width: double.infinity,
                      child: Image.asset(Res.bg,fit: BoxFit.cover,))),
              // new Positioned.fill(
              //     child: Container(
              //   height: double.infinity,
              //   width: double.infinity,
              //   color: OwnColors.white_a70,
              // )),
              Center(
                child: SingleChildScrollView(
                  child: Container(
                    width: ScreenSize().getWidth(context) * 0.92,
                    height: ScreenSize().getHeight(context),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        HorizontalSpace().create(context, 0.28),
                        Container(
                          height: ScreenSize().getHeight(context) * 0.1,
                          child: Container(
                            child: Image.asset(Res.orange_icon),

                          ),
                        ),
                        Container(
                          height: ScreenSize().getHeight(context) * 0.03,
                          child: AutoSizeText(
                            "Chat & Tour",
                            minFontSize: 10,
                            maxFontSize: 30,
                            style:TextStyle (
                              fontFamily : 'NotoSansTC',
                              fontSize: 16.nsp,
                              color: OwnColors.white,
                              fontWeight: FontWeight.w500,
                            ),
                            /*GoogleFonts.NotoSansTC(
                                fontSize: 16.nsp,
                                color: OwnColors.white,
                                textStyle: Theme.of(context).textTheme.displayMedium,
                                fontWeight: FontWeight.w500,
                                //fontStyle: FontStyle.normal
                            )*/
                            maxLines: 1,
                          ),
                        ),
                        HorizontalSpace().create(context, 0.043),
                        Container(

                          width: ScreenSize().getWidth(context) * 0.5,
                          child: Container(
                            height : ScreenSize().getHeight(context)*0.05,
                            width: double.infinity,
                            child: FlatButton(
                              color: OwnColors.CTourOrange,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,


                              textColor: OwnColors.white,
                              // elevation: 10,
                              child: AutoSizeText(
                                  S().register,
                                  minFontSize: 10,
                                  maxFontSize: 30,
                                  style:TextStyle (
                                    fontFamily : 'NotoSansTC',
                                    fontSize: 16.nsp,
                                    color: OwnColors.white,
                                    fontWeight: FontWeight.w500,
                                  ),
                                  maxLines: 1,
                                ),

                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                                // side: BorderSide(color: Colors.red)
                              ),
                              onPressed: goRegister,
                            ),
                          ),
                        ),
                        HorizontalSpace().create(context, 0.019),
                        Container(
                          height : ScreenSize().getHeight(context)*0.05,

                          width: ScreenSize().getWidth(context) * 0.5,
                          child: Container(
                            // height : ScreenSize().getHeight(context)*0.08,
                            width: double.infinity,
                            child: FlatButton(
                              color: OwnColors.white,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              textColor: OwnColors.white,
                              // elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: AutoSizeText(
                                  S().login,
                                  minFontSize: 10,
                                  maxFontSize: 30,
                                  style:GoogleFonts.inter (

                                    fontSize: 16.nsp,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                  ),
                                  maxLines: 1,
                                ),
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                                // side: BorderSide(color: Colors.red)
                              ),
                              onPressed: goLogin,
                            ),
                          ),

                        ),
                        HorizontalSpace().create(context, 0.019),
                      Visibility(
                        visible:  loginTypeString .length>0,
                        child: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            //crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              new Baseline(
                                baseline: 10.0,
                                baselineType: TextBaseline.alphabetic,
                                child: AutoSizeText(
                                  S().login_choose_lasttime_type,
                                  minFontSize: 10,
                                  maxFontSize: 30,
                                  style:GoogleFonts.inter (

                                    fontSize: 14.nsp,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500,
                                  ),
                                  maxLines: 1,
                                ),
                              ),
                              new Baseline(
                                baseline: 10.0,
                                baselineType: TextBaseline.alphabetic,
                                child: AutoSizeText(
                                  loginTypeString,
                                  minFontSize: 10,
                                  maxFontSize: 30,
                                  style:GoogleFonts.inter (

                                    fontSize: 14.nsp,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500,
                                  ),
                                  maxLines: 1,
                                ),
                              ),

                              new Baseline(
                                  baseline: 10.0,
                                  baselineType: TextBaseline.alphabetic,
                                  child: Container(
                                    height : ScreenSize().getHeight(context)*0.017,
                                    width: ScreenSize().getHeight(context)*0.017,
                                    child: Image.asset(Res.key),
                                  )
                              ),
                            ],
                          ),
                        ),
                      ),

                      ScreenSize().createBottomPad(context)
                    ],

                  ),
                ),
              ),
              )
            ],
          )),
    );
  }

  void goLogin() {
    PushNewScreen().normalPush(context, LoginPage());
  }

  void goRegister() {
    PushNewScreen().normalPush(context, RegisterPage());
  }



}

