import 'dart:async';

import 'dart:io';
import 'dart:typed_data';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:ctour/CreateWidget/BuildTitleEditText.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Lobby/LobbyPage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/CropSample.dart';
import 'package:ctour/tool/CropTool.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:permission_handler/permission_handler.dart';

import 'dart:convert';
import 'package:dio/dio.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';

import '../res.dart';

class RegisterPhotoPage extends StatefulWidget {

  final String user_id;
  final String firebase_uid;
  final String name;




  // final String pwd;

  const RegisterPhotoPage(
      {Key key,
        this.user_id,
        this.firebase_uid,
       this.name,

      })
      : super(key: key);


  @override
  State createState() {
    return _RegisterPhotoPageState();
  }
}

class _RegisterPhotoPageState extends State<RegisterPhotoPage> {


  TextEditingController nameEdit;

  Color borderColor = OwnColors.C989898;
  bool secure = true;
  int registerIndex = 1;

  String avatar = "";
  Uint8List avatarUnit8 ;
  String avatarUid = "";
  String avatarPath = "";
  String avatarFileName = "";

  String avatar_url="";
  @override
  void initState() {
    nameEdit = TextEditingController();
    nameEdit.text = widget.name;

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.white,
        body: GestureDetector(
          onTap: (){
            Tools().dismissK(context);
          },
          child:
          Align(
            alignment: Alignment.topCenter,
            child: SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: Container(
                width: ScreenSize().getWidth(context) * 0.92,
                height: 1.sh,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ScreenSize().createTopPad(context),
                    HorizontalSpace().create(context, 0.01),
                    Container(
                      height: 0.06.sh,
                      child: Topbar().noTitleCreate(context, ScreenSize().getHeight(context) * 0.073,  backClick ),
                    ),
                    HorizontalSpace().create(context, 0.12),
                    Container(
                      height: 0.19.sh,
                      child: Container(
                        child:


                        avatar.length == 0
                            ?  Image.asset(Res.choose_img)
                            :  AspectRatio(
                          aspectRatio: 1,
                          child: Container(
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(color: OwnColors.CTourOrange.withOpacity(0.5),width: 0
                                ),
                                // boxShadow: [
                                //   BoxShadow(
                                //     color: OwnColors.black.withOpacity(0.1),
                                //     spreadRadius: 1,
                                //     blurRadius: 10,
                                //     offset: Offset(0, 2),
                                //   )
                                // ]
                            ),
                            child: ClipOval(
                              child: Material(
                                child: Image.memory(avatarUnit8) ,
                              ),
                            ),
                          ),
                        )

                      ),
                    ),
                    HorizontalSpace().create(context, 0.015),
                    Container(
                      width:  ScreenSize().getWidth(context) * 0.3,
                      height: 0.03.sh,
                      child: Stack(alignment: Alignment.center, children: [
                        Center(
                            child: AutoSizeText(
                              S().personal_edit_avatar,
                              minFontSize: 10,
                              maxFontSize: 30,
                              style:GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.black,
                                fontWeight: FontWeight.w700,
                                decoration: TextDecoration.underline,
                              ),
                              maxLines: 1,
                            )),
                        new Positioned.fill(
                            child: new Material(
                              color: Colors.transparent,
                              child: new GestureDetector(
                                onTap: () => {editAvatar()},
                              ),
                            )),
                      ]),
                    )

,

                    HorizontalSpace().create(context, 0.02),
                    Container(
                      width: 0.7 * ScreenSize().getWidth(context),
                      height:0.1.sh,
                      child: BuildTitleEditText().create(
                        context,
                        S().name,
                        OwnColors.black,
                        1,
                        S().name_hint,
                        false,
                        nameEdit,
                        ScreenSize().getHeight(context)*0.06,
                        TextInputType.text,
                        borderColor,

                      ),
                    ),
                    HorizontalSpace().create(context, 0.04),
                    Container(

                      width: ScreenSize().getWidth(context) * 0.7,
                      child: Container(
                        // height : ScreenSize().getHeight(context)*0.08,
                        width: double.infinity,
                        child: FlatButton(
                          color: OwnColors.CTourOrange,
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          textColor: OwnColors.white,
                          // elevation: 10,
                          child: Padding(
                            padding: const EdgeInsets.all(6.0),
                            child: AutoSizeText(
                              S().start,
                              minFontSize: 10,
                              maxFontSize: 30,
                              style:GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.white,
                                fontWeight: FontWeight.w300,
                              ),
                              maxLines: 1,
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                            // side: BorderSide(color: Colors.red)
                          ),
                          onPressed: start,
                        ),
                      ),
                    ),
                    HorizontalSpace().create(context, 0.033),
                    Container(
                      height: 0.056.sh,
                      width: ScreenSize().getWidth(context) * 0.8,
                      child: Row(
                        children: [

                         ],
                      ),
                    ),

                    HorizontalSpace().create(context, 0.011),

                    HorizontalSpace().create(context, 0.1),
                    Container(
                      height:0.01.sh,
                      width: ScreenSize().getWidth(context) * 0.6,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          AspectRatio(
                            aspectRatio: 1,
                            child: Container(

                              height: double.infinity,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color:  registerIndex == 0? OwnColors.CTourOrange:OwnColors.CC8C8C8,)
                            ),
                          ),
                          VerticalSpace().create(context, 0.02),
                          AspectRatio(
                            aspectRatio: 1,
                            child: Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color:  registerIndex == 1? OwnColors.CTourOrange:OwnColors.CC8C8C8,),

                              height: double.infinity,

                            ),
                          )

                        ],
                      ),
                    ),
                   ],
                ),
              ),
            ),
          ),
        ));
  }

  void showBottomMenu() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 150,
              child: Column(children: <Widget>[
                ListTile(
                  leading: Icon(Icons.photo_camera),
                  title: Text(S().take_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromCamera();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.photo_library),
                  title: Text(S().choose_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromPhoto();
                  },
                )
              ]));
        });
  }
  final picker = ImagePicker();
  void pcikImageFromCamera( ) async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);

    File img = File(pickedFile.path);
    Uint8List imageBytes = await pickedFile.readAsBytes();

    CroppedFile  croppedFile = await CropTool().crop(1, 1, CropAspectRatioPreset.square, true, img.path);
    if(croppedFile != null) {
      imageBytes = await croppedFile.readAsBytes();
      avatar = img.path;
      avatarFileName = avatar
          .split("/")
          .last;
      avatarUnit8 = imageBytes;
      String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + base64Image);
      _uploadImage(base64Image);

      print(croppedFile);
    }



  }

  // void pcikImageFromCamera( ) async {
  //   PickedFile pickedFile = await picker.getImage(
  //       source: ImageSource.camera,
  //       maxHeight: 1440,
  //       maxWidth: 1440,
  //       imageQuality: 80);
  //
  //   File img = File(pickedFile.path);
  //   List<int> imageBytes = await img.readAsBytes();
  //   // img22.Image capturedImage =
  //   // img22.decodeImage(await File(pickedFile.path).readAsBytes());
  //   // img22.Image orientedImage = img22.bakeOrientation(capturedImage);
  //   // img = await File(pickedFile.path)
  //   //     .writeAsBytes(img22.encodeJpg(orientedImage));
  //   avatar = img.path;
  //   avatarFileName = avatar
  //       .split("/")
  //       .last;
  //   String base64Image = "data:image/jpg;base64," +base64Encode(imageBytes);
  //   print("base64Image: " + base64Image);
  //   _uploadImage(base64Image);
  //
  //   setState(() {
  //
  //   });
  //
  // }
  void pcikImageFromPhoto() async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);
    File img = File(pickedFile.path);
    Uint8List imageBytes = await pickedFile.readAsBytes();
    CroppedFile  croppedFile = await CropTool().crop(1, 1, CropAspectRatioPreset.square, true, img.path);
    if(croppedFile != null) {
      imageBytes = await croppedFile.readAsBytes();
      avatar = img.path;
      avatarFileName = avatar
          .split("/")
          .last;
      avatarUnit8 = imageBytes;
      String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + base64Image);
      _uploadImage(base64Image);

      print(croppedFile);
    }

  }

  // void pcikImageFromPhoto() async {
  //   PickedFile pickedFile = await picker.getImage(
  //       source: ImageSource.gallery,
  //       maxHeight: 1440,
  //       maxWidth: 1440,
  //       imageQuality: 80);
  //   File img = File(pickedFile.path);
  //   List<int> imageBytes = await img.readAsBytes();
  //   avatar = img.path;
  //   avatarFileName = avatar
  //       .split("/")
  //       .last;
  //
  //   String base64Image = "data:image/jpg;base64," +base64Encode(imageBytes);
  //   print("base64Image: " + base64Image);
  //   _uploadImage(base64Image);
  //
  //   setState(() {
  //
  //   });
  //
  // }

  Future<void> _uploadImage(String data)  async {
    // Loading.show(context);

    ResultData resultData = await AppApi.getInstance().setFileBase64(
        context,
        true,
        data
    );

    if (resultData.isSuccess()) {
      // Loading.dismiss(context);
      // Notice how you have to call body from the response if you are using http to retrieve json
        print(resultData.data);
        avatar_url = GDefine.imageUrl + resultData.data["path"];
        print("avatar_url " + avatar_url);
        setState(() {

        });
    } else {
      // Loading.dismiss(context);
      WarningDialog.showIOSAlertDialog(context, S().upload_fail_title, S().upload_fail_hint, S().confirm);

    }

  }


  void updateUser( ) async {
    print("updateUserAPI");
    Loading.show(context);

    ResultData resultData = await AppApi.getInstance().setUserInfoAvatarName(context, true,widget.user_id, nameEdit.text,  avatar_url);

    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      // Notice how you have to call body from the response if you are using http to retrieve json

      print(resultData.data.toString());
      loginAPI();

    } else {

      Loading.dismiss(context);
      WarningDialog.showIOSAlertDialog(context, S().register_fail_title, S().register_fail_hint, S().confirm);
    }
  }



  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }



  void editAvatar() {
    showBottomMenu();
  }
  void start() {
    if (nameEdit.text.isEmpty){
      WarningDialog.showIOSAlertDialog(context,S().no_name_title,S().no_name_title_hint, S().confirm);
    }else {
      updateUser();
    }
  }

  void loginAPI() async {
    print("loginAPI");


    ResultData resultData = await AppApi.getInstance()
        .login(context, true,  widget.firebase_uid, "normal");

    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data.toString());

      SharePre.prefs.setString(GDefine.name, resultData.data["name"]);
      SharePre.prefs.setString(GDefine.user_id, resultData.data["user_id"].toString());
      SharePre.prefs.setString(GDefine.avatar, resultData.data["avatar"].toString());
      SharePre.prefs.setString(GDefine.gender, resultData.data["gender"].toString());
      SharePre.prefs.setString(GDefine.firebase_id, widget.firebase_uid);
      SharePre.prefs.setString(GDefine.is_fcm_open, resultData.data["is_fcm_open"].toString());
      SharePre.prefs.setString(GDefine.description, resultData.data["description"].toString());
      SharePre.prefs.setString(GDefine.invite_code, resultData.data["invite_code"].toString());
      // SharePre.prefs.setString(GDefine.login_type, "normal");
      SharePre.prefs.setString(GDefine.want_companion, resultData.data["want_companion"].toString());
      List<dynamic> trans =  resultData.data["trans"];
      List<dynamic> location =  resultData.data["location"];
      List<dynamic> tourist_img =  resultData.data["tourist_img"];
      List<dynamic> lang =  resultData.data["lang"];
      SharePre.prefs.setString(GDefine.trans, trans.join(","));
      SharePre.prefs.setString(GDefine.location, location.join(","));
      SharePre.prefs.setString(GDefine.tourist_img, tourist_img.join(","));
      SharePre.prefs.setString(GDefine.lang, lang.join(","));

      String role_slug  = resultData.data["role_slug"].toString();
      if(role_slug.contains("guide")){
        SharePre.prefs.setString(GDefine.isGuilder, "true");
      }else {
        SharePre.prefs.setString(GDefine.isGuilder, "false");
      }
      Loading.dismiss(context);
      PushNewScreen().popAllAndPush(context, LobbyPage(articleID: "",guilderID: "",));

    } else {

    }
  }

}
