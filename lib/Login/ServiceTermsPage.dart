import 'package:auto_size_text/auto_size_text.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/VerticalSpace.dart';
import 'RegisterPage.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';

class ServiceTermsPage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    void backClick() {
      print("back");
      if (Navigator.canPop(context)) {
        Navigator.pop(context);
      } else {
        SystemNavigator.pop();
      }
    }
    return Scaffold(
        backgroundColor: OwnColors.white,
        body:Column(
          children: [
            ScreenSize().createTopPad(context),

            Topbar().noTitleBorderCreate(context, ScreenSize().getHeight(context) * 0.073,  backClick ),

            Flexible(

              child:SingleChildScrollView(

                child: SizedBox(
                    width:ScreenSize().getWidth(context) * 0.95,
                    child:Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      HorizontalSpace().create(context, 0.005),
                      ServiceTermsTextStyle().createTitle("CTour服務條款"),
                      ServiceTermsTextStyle().createContent("CTour服務條款（下稱「本條款」）是由「嘻途資訊有限公司」（下稱本公司）所經營之應用程式(下稱「CTour」)之用戶（下稱「用戶」）與本公司之間所約定之使用條件。"),

                      ServiceTermsTextStyle().createTitle("\n一、服務條款之認知與同意"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)用戶應遵守本條款規定，當用戶開始使用本服務時，即視為用戶已詳細閱讀、瞭解、並不可撤銷地同意接受並願意遵守本條款。 若用戶繼續使用本服務，視為已同意並接受修改或變更之內容，建議用戶隨時注意服務條款之修改或變更。若不同意本條款之內容，或其修改或變更之內容者，用戶應立即停止使用本服務。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)若用戶為未成年人，應於其法定代理人已詳細閱讀、瞭解並同意本條款後，始得使用本服務。若用戶為業者，應於該業者之公司負責人或法定代表同意本條款後，始得使用本服務。當前述用戶開始使用或繼續使用本服務，即視為用戶已取得前述法定代理人、公司負責人或法定代表之同意。"),
                      ),

                      ServiceTermsTextStyle().createTitle("\n二、服務條款之變更"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("本公司認為有必要時，得不事先通知用戶，而隨時變更本條款。變更後之本條款，經本公司於所經營之CTour適當公告後生效；用戶於本條款變更後仍繼續使用本服務者，視為對變更後之本條款做出有效且無法撤銷之同意。用戶瞭解由於本公司不會個別通知用戶相關變更內容，故在使用本服務時，應隨時參考本公司公告最新之條款。"),
                      ),

                      ServiceTermsTextStyle().createTitle("\n三、用戶資格與帳號"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)使用本服務需要帳號。若無使用電話號碼及電子郵件信箱申請帳號或是使用第三方帳號（Facebook、Apple、Google）註冊，將無法登入本服務。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)用戶有義務於註冊時提供正確的資料，若提供的資料不正確，本公司有權暫停或終止用戶帳號，並拒絕使用本服務之全部或一部份功能。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(3)帳號刪除時，用戶於本服務的所有使用權及使用紀錄不論任何理由均為消滅。即便用戶不慎刪除帳號，原有的帳號及使用歷程與紀錄亦無法回復，敬請留意。"),
                      ),

                      ServiceTermsTextStyle().createTitle("\n四、用戶之義務"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)無論是否為CTour之用戶，皆應遵守下列事項："),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(a)不得修改本服務之內容或修改本服務所提供之任何軟體。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(b)不得利用本服務非法獲取他人之個人資訊。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(c)不得以任何方式干擾本服務之安全功能，及阻止或限制本服務內容之功能。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)用戶應遵守您所屬國家或所在地域之相關法律、法規及網際網路之相關國際慣例，且不得以任何非法目的或以任何非法方式使用本服務。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(3)用戶應同意並擔保不利用本服務從事侵害他人權益或違法之行為（包括但不限於侵害他人隱私權及智慧財產權、濫發廣告信、追蹤他人或其他干擾他人，或為前述目的蒐集或儲存他人之個人資訊等）。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(4)用戶對於公開、發佈，或傳送於CTour之任何內容，包括但不限於資訊、資料、文字、軟體、音樂、音訊、照片、圖形、視訊、信息等，應自行負擔法律責任。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(5)CTour並未對於用戶所公開、發佈，或傳送於CTour上之任何內容，事前加以審查，但若有發現違反本公司服務宗旨及超出所規範之主題之言論，本公司有權不經事先告知，編輯、拒絕或移除用戶所公開、發佈，或傳送於CTour上之任何內容。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(6)用戶應擔保所公開、發佈或傳送於CTour上之任何內容為自己所擁有或已取得權利人之著作權、專利權或其相關智慧財產權之授權。"),
                      ),

                      ServiceTermsTextStyle().createTitle("\n五、隱私權"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)本公司尊重用戶之隱私權。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)用戶瞭解並同意本公司得基於調查不當使用本服務之目的，於必要時向您所在之當地司法調查單位，揭露用戶的註冊資訊、聊天紀錄資訊及其他為調查不當使用情事所需之資訊。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(3)用戶瞭解，於使用本服務時，適用CTour隱私權政策。當您使用本服務時，視為您已同意依據CTour隱私權政策進行您個人資料的蒐集與利用。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(4)除本公司以外，其他公司和組織將在本服務上投放廣告。本公司使用其他公司的廣告服務平台來優化廣告投放。其他公司將透過這些廣告服務平台蒐集用戶資料。"),
                      ),
                      ServiceTermsTextStyle().createTitle("\n六、服務"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)本服務是提供用戶於本公司之CTour應用程式上發佈個人文章、留言、評論、訊息與個人相關資料，故本服務本身不包括其他用戶或是合作夥伴於平台上所銷售與旅遊或美食相關之商品或服務。用戶如欲購買該類商品或服務時，係透過本服務連結至本公司外之其他用戶或合作夥伴，而直接向該對方購買商品或服務，並非向本公司購買商品或服務。用戶瞭解本公司亦無義務向用戶配送或提供任何平台上之其他用戶所銷售之商品或服務。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)本服務中所提供的內容及本服務相關之一切權利（包含但不限於著作權、商標權、專利權等之智慧財產權）均歸屬於本公司或授權予本公司使用之第三者（包含合作夥伴）。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(3)本公司授權用戶依照本條款及本服務內記載之使用條件使用本服務。用戶不得將本服務的使用權轉讓予第三者或使其繼承。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(4)用戶使用本服務時，須自費及自主備置所需之個人電腦、手機、通信設備、作業系統、通信能力及電力等。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(5)本公司得隨時按用戶年齡、用戶有無完成身份確認作業、用戶有無註冊或更新資料，以及其他本公司判斷為必要的條件，將本服務的全部或部分提供給符合條件之用戶。用戶不得因本公司就上述條件判斷後所為是否提供服務之決定，對本公司主張任何損害或損失賠償。"),
                      ),
                      ServiceTermsTextStyle().createTitle("\n七、合作夥伴之服務"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("本公司合作的合作夥伴或其他業者透過本服務所提供之服務、產品或內容所生相關責任，概由提供之合作夥伴或其他業者負責，本公司不負任何責任。"),
                      ),

                      ServiceTermsTextStyle().createTitle("\n八、禁止行為"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)本公司於本服務中禁止以下之行為（下稱「禁止行為」）："),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(a)干擾、妨害或限制本公司或本服務運作之行為（包含但不限於非法存取、有害程式之傳送）或毀損本公司信用之行為；"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(b)出於不法目的而使用或使第三人使用本服務；"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(c)未經本公司同意竄改本公司於本服務中所提供之資訊及內容之行為；"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(d)對本公司或合作夥伴做出暴力行為、威脅性言行舉止、或不當要求之行為；"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(e)侵害本公司或合作夥伴之智慧（知識）財產權等其他權利之行為；"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(f)違反任何應適用之法律或規定、網際網路相關慣例、或公序良俗之行為或有違反之虞之行為；"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(g)其他違反本條款任一條款之行為；或其他本公司判斷為不當之行為。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)本公司在用戶有以下各款任一情況時，應依照禁止行為(4)之規定進行處理。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(a)因滯納稅金等公課受行政扣押處分時"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(b)受票據交換所通報拒絕往來時"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(c)遭扣押、假扣押、假處分、欠稅處分、強制執行時；"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(d)開始破產程序、申請民事債務清理程序等法律上清理程序、或開始此等程序時；或無法進行用戶身分確認、進行身分確認有顯著困難時、或進行身分確認後無法確認是本人時。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(3)用戶同意本公司為確認用戶有無本條規定之禁止行為，得檢視用戶所揭示或傳送之資訊及內容。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(4)本公司認為用戶有(1)所定之禁止行為、或有(2)之任一情況時，得採取以下處置而無須於事前通知用戶；本公司對用戶因此等處置所生之損害或不利益不負任何責任："),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(a)警示、警告"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(b)刪除資訊"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(c)停止提供本服務之全部或一部"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(d)停止使用或刪除帳號"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(e)其他本公司認為有必要且適當之處置。"),
                      ),

                      ServiceTermsTextStyle().createTitle("\n九、智慧財產權"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)本公司所使用之軟體或程式、網站上所有內容，包括但不限於著作、圖片、檔案、資訊、資料、網站架構、網站畫面的安排、網頁設計等，均由本公司或其他權利人依法擁有其智慧財產權，包括但不限於商標權、專利權、著作權、營業秘密與專有技術等。未經本公司或其他權利人同意，任何人不得逕自使用、修改、重製、公開播送、公開傳輸、公開演出、改作、散布、發行、公開發表、進行還原工程、解編或反向組譯。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)若用戶欲引用或轉載前述軟體、程式或網站內容，除明確為法律所許可者外，必須依法取得本公司或其他權利人的事前書面同意。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(3)用戶若違反本條規定，應對本公司負損害賠償責任。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(4)用戶同意在使用本服務時，對於所公開、發佈、或傳送於CTour上之任何內容，應擔保其未違反著作權法等相關智慧財產權法規及其他法規。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(5)若用戶有涉及侵權之情事，本公司得暫停全部或部份之服務，或隨時刪除用戶之帳號。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(6)若用戶發現著作權遭侵害之情事，請將所遭侵權之情形及聯絡方式，並附具真實陳述及擁有合法智慧財產權之聲明，請至CTour中聯絡本公司客服信箱：ctourtw@gmail.com。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(7)本公司認為用戶提供之內容有違反相關法令或本條款規定或有違反之虞時，或有其他業務上需求時，本公司及其指定人有權在不事先通知用戶的情形下，移除該等內容。"),
                      ),
                      ServiceTermsTextStyle().createTitle("\n十、著作權之授權"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)對於用戶公開、發佈、或傳送於CTour上之任何內容或其相關之智慧財產權，歸屬於用戶或用戶之授權人所有，但用戶同意授予本公司及本公司之其他用戶為全球性、無償、可轉讓、不可撤銷之使用、重製、發行、製作衍生作品、展示、公開展示、出版、改編、公開傳輸、公開上映、公開發表、翻譯、提供/製作線上版本或電子傳輸該內容，包括但不限於以任何媒體格式，或任何媒體管道推廣和重新發佈服務（及其衍生作品）的全部或一部份，且在前述授權範圍內，得再授權予第三人。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)若用戶移除或刪除在CTour上所公開、發佈、或傳送之任何內容，則前述之授權，自移除或刪除之日起，於商業慣例上之合理期間內終止。然而對於已移除或刪除之內容，本公司可以在其資料庫中保留副本。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(3)用戶應擔保本公司及本公司之其他用戶，於使用、修改、重製、公開播送、改作、散布、發行、公開發表、公開傳輸、公開上映、翻譯、再授權用戶公開、發佈，或傳送於CTour上之任何內容，不致侵害任何第三人之智慧財產權，倘有因前述侵害而導致本公司及本公司之其他用戶受有損害，用戶同意負賠償責任。"),
                      ),
                      ServiceTermsTextStyle().createTitle("\n十一、資訊之處理"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("用戶同意若本公司認為用戶有違反法令或本條款之行為時（包含但不限於本公司基於禁止行為(4)介入問題時）或本公司認為有必要對本服務之不當使用之調查、犯罪搜查時，本公司得依其必要程度，對您所在地區之司法單位、合作夥伴、遭違法使用之被害者及搜查機關揭露用戶的註冊資訊、交易紀錄資訊及其他必要資訊。"),
                      ),

                      ServiceTermsTextStyle().createTitle("\n十二、免則聲明"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)本服務所呈現的內容，除由本公司所提供、彙整或分析之內容外，包括但不限於其他用戶或合作夥伴相關旅遊商品或服務的內容及其所刊載的廣告，均由其他用戶或是合作夥伴所提供。在法律許可範圍內，本公司對於所有其內容的真實性、合法性、完整性、即時性等，不負任何明示或默示的承諾或擔保。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)用戶得於CTour上公開、發佈，或傳送內容，但本公司對您所公開、發佈，或傳送之內容，不負保密義務。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(3)用戶可能因為使用本服務而接觸或連結不準確、攻擊性、不雅或不愉快的內容或其他網站與應用程式，若有此情事發生，用戶同意放棄對本公司主張任何法律上或基於平衡原則之權利或救濟。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(4)任何用戶於CTour上所公開、發佈，或傳送內容，或者表達任何意見、提議或建議，本公司不作任何審查，且明確聲明免除承擔有關該內容、意見等之法律責任。但若有發現違反本公司服務宗旨及超出所規範之主題之言論，本公司有權不經事先告知，編輯、拒絕或移除用戶所公開、發佈，或傳送於CTour上之任何內容。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(5)本公司不保證，包括但不限於以下事項:"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(a)本服務之內容及所提供之軟體為正確、完整"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(b)本服務的不中斷或不停止傳輸"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(c)第三人透過本服務傳輸任何錯誤軟體、病毒、木馬或類似性質的程式"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 16),
                        child: ServiceTermsTextStyle().createContent("(d)第三人透過本服務、任何超鏈結服務，或橫幅功能發佈或提供的商業廣告"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(6)不論明示或默示，本公司均未保證本服務不具有事實上或法律上之瑕疵（包括安全性、可靠性、正確性、完整性、有效性、特定目的之適用性、資訊安全等相關缺失、錯誤或程式漏洞、權利侵害等）。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(7)除因本公司故意或重大過失致本服務對用戶造成之損害外，本公司對本服務造成用戶之損害不負任何責任。"),
                      ),

                      ServiceTermsTextStyle().createTitle("\n十三、本服務之暫停、變更、終止"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)用戶同意因本公司或合作夥伴提供本服務之維護或障礙處理、商業判斷、天災等不可抗力因素，或其他本公司認為有必要之情事，得不經通知用戶，隨時修改或停止全部或一部份本服務之提供。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)因前項造成之本服務的暫停、變更、終止而對用戶產生任何損害或不利益，本公司不負任何責任。"),
                      ),

                      ServiceTermsTextStyle().createTitle("\n十四、聯絡方式"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)經本公司判斷有通知或聯絡用戶之必要時，本公司得於本服務內通知，或藉由電子郵件、電話等本公司認為適當之方法進行聯絡。用戶同意本公司出於通知或聯絡用戶之目的，使用用戶已註冊之電子郵件信箱、電話號碼等。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)當用戶同意本服務條款而存取本服務時，即表示同意CTour以您所留存之帳號資料傳送CTour之相關通知，且一經寄發通知，無論您是否閱讀接收，均視為送達。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(3)用戶欲與本公司聯絡或諮詢時，應使用本服務中之客服信箱。針對合作夥伴等本公司以外第三者之諮詢內容，本公司有可能無法回答。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(4)若用戶未從合作夥伴或其他用戶收到商品或服務，或所收到的商品或服務有瑕疵或與所提供的內容不一致時，用戶應直接聯繫對方。本公司不代表用戶與合作夥伴調解、爭論或解決該問題。"),
                      ),
                      ServiceTermsTextStyle().createTitle("\n十五、準據法與管轄法院"),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("(1)本條款之準據法為中華民國法律。用戶與本公司間因本服務或與本服務有關所發生的紛爭，應以中華民國法律為準據法，並同意以台灣台北地方法院為第一審管轄法院，但若法律對於管轄法院另有強制規定者，仍應依其規定。"),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 6),
                        child: ServiceTermsTextStyle().createContent("\n(2)本服務條款之部分條款依法被認為無效時，其他條款仍應繼續有效。"),
                      ),

                      ServiceTermsTextStyle().createDate("""\n訂定日：2022年7月8日
上一次修訂於：2022年7月8日"""),
                      HorizontalSpace().create(context, 0.05),
                      ScreenSize().createBottomPad(context),

                ]),
              ),),
            ),
          ],
        ),
    );
  }
}
class ServiceTermsTextStyle{
  Text createTitle(text) {
    return Text("""$text""",
      textAlign: TextAlign.left,
      style: TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.bold,
      ),
    );
  }
  Text createContent(text) {
    return Text("""$text""",
      textAlign: TextAlign.left,
      style: TextStyle(
        fontSize: 14,
      ),
    );
  }
  Text createDate(text) {
    return Text("""$text""",
      textAlign: TextAlign.left,
      style: TextStyle(
          fontSize: 12,
          color: OwnColors.secondaryText
      ),
    );
  }
}