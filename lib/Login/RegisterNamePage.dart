import 'dart:async';

import 'dart:io';
import 'dart:typed_data';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:ctour/CreateWidget/BuildTitleEditText.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Lobby/LobbyPage.dart';
import 'package:ctour/Login/RegisterPhotoNewPage.dart';
import 'package:ctour/Login/RegisterStatic.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/CropSample.dart';
import 'package:ctour/tool/CropTool.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:permission_handler/permission_handler.dart';

import 'dart:convert';
import 'package:dio/dio.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';

import '../res.dart';

class RegisterNamePage extends StatefulWidget {

  final String user_id;
  final String firebase_uid;
  final String name;




  // final String pwd;

  const RegisterNamePage(
      {Key key,
        this.user_id,
        this.firebase_uid,
       this.name,

      })
      : super(key: key);


  @override
  State createState() {
    return _RegisterNamePageState();
  }
}

class _RegisterNamePageState extends State<RegisterNamePage> {


  TextEditingController nameEdit;

  Color borderColor = OwnColors.C989898;
  bool secure = true;
  int registerIndex = 1;

  String avatar = "";
  Uint8List avatarUnit8 ;
  String avatarUid = "";
  String avatarPath = "";
  String avatarFileName = "";

  String avatar_url="";
  @override
  void initState() {
    nameEdit = TextEditingController();
    nameEdit.text = widget.name;

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.white,
        body: GestureDetector(
          onTap: (){
            Tools().dismissK(context);
          },
          child:
          Align(
            alignment: Alignment.topCenter,
            child: SingleChildScrollView(
              physics: ClampingScrollPhysics(),
              child: Container(
                width: ScreenSize().getWidth(context) * 0.92,
                height: 1.sh,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ScreenSize().createTopPad(context),
                    HorizontalSpace().create(context, 0.01),
                    Container(
                      height: 0.06.sh,
                      child: Topbar().noTitleCreate(context, ScreenSize().getHeight(context) * 0.073,  backClick ),
                    ),
                    HorizontalSpace().create(context, 0.06),
                    Text(
                      S().new_register_name,
                      style: GoogleFonts.inter(
                        fontSize: 24.nsp,
                        color: OwnColors.black,
                        fontWeight: FontWeight.w600,

                      ),
                    ),

                    HorizontalSpace().create(context, 0.04),

                    Container(
                      height: 0.04.sh,
                      width: 0.68.sw,
                      decoration: BoxDecoration(
                        borderRadius:
                        BorderRadius.all(Radius.circular(10)),
                        border: Border.all(color: OwnColors.C989898,width: 1),

                      ),
                      child: Center(
                        child: TextField(
                            controller: nameEdit,

                            style: GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.black,
                                fontWeight: FontWeight.w500
                            ),

                            onChanged: (S){
                                setState(() {

                                });

                            },

                            maxLength:64,
                            maxLines: null,

                            buildCounter: null,
                            decoration: InputDecoration(
                                hintStyle: GoogleFonts.inter(
                                    fontSize: 12.nsp,
                                    color: OwnColors.C989898,
                                    fontWeight: FontWeight.w500
                                ),
                                counterText: "",
                                hintText: S().personal_become_guilder_name_hint,
                                fillColor: OwnColors.white,

                                counterStyle: TextStyle(color: Colors.transparent,fontSize: 0),
                                contentPadding: EdgeInsets.only(left: 8,right: 8,top: 0,bottom: 0),

                                //Change this value to custom as you like
                                isDense: true,
                                filled: true,
                                border: OutlineInputBorder(
                                    borderRadius:
                                    new BorderRadius.circular(10.0),
                                    borderSide: BorderSide.none)


                            )
                        ),
                      ),
                    ),
                    HorizontalSpace().create(context, 0.019),
                    Container(
                      // height: 0.04.sh,
                      width: 0.68.sw,
                      child: Text(
                        S().new_register_name_hint2,
                        style: GoogleFonts.inter(
                          fontSize: 12.nsp,
                          color: OwnColors.C989898,
                          fontWeight: FontWeight.w400,

                        ),
                      ),
                    ),

                    HorizontalSpace().create(context, 0.02),

                    HorizontalSpace().create(context, 0.04),

                    Container(

                      width: ScreenSize().getWidth(context) * 0.7,
                      height: 0.045.sh,
                      child: Container(
                        // height : ScreenSize().getHeight(context)*0.08,
                        width: double.infinity,
                        child: FlatButton(
                          color: nameEdit.text.isEmpty?  OwnColors.CC8C8C8 : OwnColors.CTourOrange,
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          textColor: OwnColors.white,
                          // elevation: 10,
                          child: Padding(
                            padding: const EdgeInsets.all(6.0),
                            child: AutoSizeText(
                              S().new_register_next,
                              minFontSize: 10,
                              maxFontSize: 30,
                              style:GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.white,
                                fontWeight: FontWeight.w300,
                              ),
                              maxLines: 1,
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                            // side: BorderSide(color: Colors.red)
                          ),
                          onPressed: start,
                        ),
                      ),
                    ),

                    HorizontalSpace().create(context, 0.061),
                    Container(
                      // height: 0.04.sh,
                      width: 0.68.sw,
                      child: Center(
                        child: Text(
                          "1 / 5",
                          style: GoogleFonts.inter(
                            fontSize: 16.nsp,
                            color: OwnColors.C989898,
                            fontWeight: FontWeight.w700,

                          ),
                        ),
                      ),
                    ),
                    HorizontalSpace().create(context, 0.076),

                   ],
                ),
              ),
            ),
          ),
        ));
  }


  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }


  void start() {
    if (nameEdit.text.isEmpty){
      // WarningDialog.showIOSAlertDialog(context,S().no_name_title,S().no_name_title_hint, S().confirm);
    }else {
      RegisterStatic.name = nameEdit.text;
      PushNewScreen().normalPush(context, RegisterPhotoNewPage());
      // updateUser();
    }
  }


}
