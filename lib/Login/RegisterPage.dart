import 'dart:async';

import 'dart:io';
import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:crypto/crypto.dart';
import 'package:ctour/CreateWidget/BuildTitleEditText.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Lobby/LobbyPage.dart';
import 'package:ctour/Login/LoginPage.dart';
import 'package:ctour/Login/RegisterNamePage.dart';
import 'package:ctour/Login/RegisterPhotoPage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/StatusTools.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:permission_handler/permission_handler.dart';

import 'dart:convert';
import 'package:dio/dio.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

import 'PrivacyPage.dart';
import 'ServiceTermsPage.dart';
import '../res.dart';

class RegisterPage extends StatefulWidget {

  final String emailStr;
  const RegisterPage({
    Key key,
    this.emailStr

  }) : super(key: key);

  @override
  State createState() {
    return _RegisterPageState();
  }
}

class _RegisterPageState extends State<RegisterPage> {

  TextEditingController accountEdit, pwdEdit;

  Color borderColor = OwnColors.C989898;
  bool secure = true;
  int registerIndex = 0;
  GoogleSignInAccount user;
  @override
  void initState() {
    accountEdit = TextEditingController();
    pwdEdit = TextEditingController();
    StatusTools().setWhitBGDarkText();

    if(widget.emailStr != null){
      accountEdit.text = widget.emailStr;
    }

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap:(){
        Tools().dismissK(context);
        print("onTap");
      },
      child: Scaffold(
          backgroundColor: OwnColors.white,
          body: Align(
            alignment: Alignment.topCenter,
            child: SingleChildScrollView(
              child: Container(
                width: ScreenSize().getWidth(context) * 0.92,
                height: 1.sh,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    //HorizontalSpace().create(context, 0.01),
                    ScreenSize().createTopPad(context),
                    Container(
                      height: 0.06.sh,
                      child: Topbar().noTitleCreate(context, ScreenSize().getHeight(context) * 0.073,  backClick ),
                    ),
                    Container(
                      height: 0.24.sh,
                      child: Container(
                        child: Image.asset(Res.register_icon),
                      ),
                    ),
                    HorizontalSpace().create(context, 0.015),
                    Container(
                      width: 0.7 * ScreenSize().getWidth(context),
                      height:0.1.sh,
                      child: BuildTitleEditText().create(
                        context,
                        S().email,
                        OwnColors.black,
                        1,
                        "example@mail.com",
                        false,
                        accountEdit,
                        ScreenSize().getHeight(context)*0.06,
                        TextInputType.text,
                        borderColor,
                      ),
                    ),
                    HorizontalSpace().create(context, 0.017),
                    Container(
                      width: 0.7 * ScreenSize().getWidth(context),
                      height: 0.1.sh,
                      child: BuildTitleEditText().create(
                        context,
                        S().pwd,
                        OwnColors.black,
                        1,
                        S().pwd_hint,
                        true,
                        pwdEdit,
                        ScreenSize().getHeight(context)*0.06,
                        TextInputType.text,
                        borderColor,

                      ),
                    ),
                    HorizontalSpace().create(context, 0.006),
                    HorizontalSpace().create(context, 0.0237),
                    Container(

                      width: ScreenSize().getWidth(context) * 0.7,
                      child: Container(
                        // height : ScreenSize().getHeight(context)*0.08,
                        width: double.infinity,
                        child: FlatButton(
                          color: OwnColors.CTourOrange,
                          splashColor: Colors.transparent,
                          highlightColor: Colors.transparent,
                          textColor: OwnColors.white,
                          // elevation: 10,
                          child: Padding(
                            padding: const EdgeInsets.all(6.0),
                            child: AutoSizeText(
                              S().register,
                              minFontSize: 10,
                              maxFontSize: 30,
                              style:GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.white,
                                //textStyle: Theme.of(context).textTheme.b,
                                fontWeight: FontWeight.w300,
                                //fontStyle: FontStyle.normal
                              ),
                              maxLines: 1,
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                            // side: BorderSide(color: Colors.red)
                          ),
                          onPressed: register,
                        ),
                      ),
                    ),
                    HorizontalSpace().create(context, 0.033),
                    Container(
                      height:  0.056.sh,
                      width: ScreenSize().getWidth(context) * 0.8,
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              height: 1,
                              color: OwnColors.black,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left:4.0,right: 4.0),
                            child: AutoSizeText(
                              S().or,
                              minFontSize: 10,
                              maxFontSize: 30,
                              style:GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.black,
                                //textStyle: Theme.of(context).textTheme.b,
                                fontWeight: FontWeight.w300,
                                //fontStyle: FontStyle.normal
                              ),
                              maxLines: 1,
                            ),
                          ),
                          Expanded(
                            child: Container(
                              height: 1,
                              color: OwnColors.black,
                            ),
                          )
                        ],
                      ),
                    ),

                    HorizontalSpace().create(context, 0.018),
                    Container(
                      height: 0.05.sh,
                      width: ScreenSize().getWidth(context) * 0.6,
                      child: Platform.isAndroid? androidRow():iosRow(),
                    ),
                    //Spacer(),
                    // HorizontalSpace().create(context, 0.08),
                    HorizontalSpace().create(context, 0.054),
                    Container(
                      height:   0.01.sh,
                      width: ScreenSize().getWidth(context) * 0.6,
                      // child: Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   crossAxisAlignment: CrossAxisAlignment.center,
                      //   children: [
                      //     AspectRatio(
                      //       aspectRatio: 1,
                      //       child: Container(
                      //
                      //           height: double.infinity,
                      //           decoration: BoxDecoration(
                      //             shape: BoxShape.circle,
                      //             color:  registerIndex == 0? OwnColors.CTourOrange:OwnColors.CC8C8C8,)
                      //       ),
                      //     ),
                      //     VerticalSpace().create(context, 0.02),
                      //     AspectRatio(
                      //       aspectRatio: 1,
                      //       child: Container(
                      //         decoration: BoxDecoration(
                      //           shape: BoxShape.circle,
                      //           color:  registerIndex == 1? OwnColors.CTourOrange:OwnColors.CC8C8C8,),
                      //
                      //         height: double.infinity,
                      //
                      //       ),
                      //     )
                      //
                      //   ],
                      // ),
                    ),
                    HorizontalSpace().create(context, 0.03),
                    Platform.localeName.toLowerCase().contains("zh")?

                    terms():
                    terms_eng()
                    // Align(
                    //   alignment: Alignment.bottomCenter,
                    //   child: Padding(
                    //     padding: const EdgeInsets.only(top:14,right: 10,bottom: 10,left: 8),
                    //     child: Platform.localeName.toLowerCase().contains("zh")?
                    //     terms():
                    //     terms_eng(),
                    //   ),
                    // ),
                    //HorizontalSpace().create(context, 0.05),
                  ],
                ),
              ),
            ),
          )),
    );
  }
  Widget terms(){
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children:[

          AutoSizeText(
            S().register_agree,
            minFontSize: 10,
            maxFontSize: 15,
            style:GoogleFonts.inter(
              fontSize: 12.nsp,
              color: OwnColors.black,
              fontWeight: FontWeight.w500,
            ),
          ),
          TextButton(
            onPressed: (){
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ServiceTermsPage(), maintainState: false));
            },
            style: TextButton.styleFrom(
                padding: EdgeInsets.zero,
                minimumSize: Size(20, 30)
            ),
            child:AutoSizeText(
              S().register_service,
              minFontSize: 10,
              maxFontSize: 15,
              style:GoogleFonts.inter(
                fontSize: 12.nsp,
                color: OwnColors.black,
                decoration: TextDecoration.underline,
                fontWeight: FontWeight.w500,
              ),
            ),),
          AutoSizeText(
            S().register_and,
            minFontSize: 10,
            maxFontSize: 15,
            style:GoogleFonts.inter(
              fontSize: 12.nsp,
              color: OwnColors.black,
              fontWeight: FontWeight.w500,
            ),
          ),
          TextButton(
            onPressed: (){
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PrivacyPage(), maintainState: false));
            },
            style: TextButton.styleFrom(
                padding: EdgeInsets.zero,
                minimumSize: Size(20, 30)
            ),
            child:AutoSizeText(
              S().register_privacy,
              minFontSize: 10,
              maxFontSize: 15,
              style:GoogleFonts.inter(
                fontSize: 12.nsp,
                color: OwnColors.black,
                decoration: TextDecoration.underline,
                fontWeight: FontWeight.w500,
              ),
            ),),
        ]

    );
  }

  Widget terms_eng(){
    return  Column(
      children: [
        AutoSizeText(
          S().register_agree,
          minFontSize: 10,
          maxFontSize: 15,
          style:GoogleFonts.inter(
            fontSize: 12.nsp,
            color: OwnColors.black,
            fontWeight: FontWeight.w500,
          ),
        ),
        Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children:[


              TextButton(

                onPressed: (){
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ServiceTermsPage(), maintainState: false));
                },
                style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                    minimumSize: Size(0, 0),
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    alignment: Alignment.centerLeft),
                child:AutoSizeText(
                  S().register_service,
                  minFontSize: 10,
                  maxFontSize: 15,
                  style:GoogleFonts.inter(
                    fontSize: 12.nsp,
                    color: OwnColors.black,
                    decoration: TextDecoration.underline,
                    fontWeight: FontWeight.w500,
                  ),
                ),),
              AutoSizeText(
                " "+S().register_and+" ",
                minFontSize: 10,
                maxFontSize: 15,
                style:GoogleFonts.inter(
                  fontSize: 12.nsp,
                  color: OwnColors.black,
                  fontWeight: FontWeight.w500,
                ),
              ),
              TextButton(
                onPressed: (){
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PrivacyPage(), maintainState: false));
                },
                style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                    minimumSize: Size(0, 0),
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    alignment: Alignment.centerLeft),
                child:AutoSizeText(
                  S().register_privacy,
                  minFontSize: 10,
                  maxFontSize: 15,
                  style:GoogleFonts.inter(
                    fontSize: 12.nsp,
                    color: OwnColors.black,
                    decoration: TextDecoration.underline,
                    fontWeight: FontWeight.w500,
                  ),
                ),),
            ]

        ),
      ],
    );
  }


  Widget iosRow(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Stack(alignment: Alignment.center, children: [
          Center(
            child:            Padding(
              padding: const EdgeInsets.only(left:8.0,right: 8.0),
              child: Image.asset(Res.icon_apple_sign),
            ),),
          new Positioned.fill(
              child: new Material(
                color: Colors.transparent,
                child: new GestureDetector(
                  onTap: () => {appleRegister()},
                ),
              )),
        ]),
        Stack(alignment: Alignment.center, children: [
          Center(
            child:            Padding(
              padding: const EdgeInsets.only(left:8.0,right: 8.0),
              child: Image.asset(Res.icon_google_sign),
            ),),
          new Positioned.fill(
              child: new Material(
                color: Colors.transparent,
                child: new GestureDetector(
                  onTap: () => {googleRegister()},
                ),
              )),
        ]),

        Stack(alignment: Alignment.center, children: [
          Center(
            child:            Padding(
              padding: const EdgeInsets.only(left:8.0,right: 8.0),
              child: Image.asset(Res.icon_fb_sign),
            ),),
          new Positioned.fill(
              child: new Material(
                color: Colors.transparent,
                child: new GestureDetector(
                  onTap: () => {fbRegister()},
                ),
              )),
        ])


      ],
    );
  }

  Widget androidRow(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [

        Stack(alignment: Alignment.center, children: [
          Center(
            child:Padding(
              padding: const EdgeInsets.only(left:8.0,right: 8.0),
              child: Image.asset(Res.icon_google_sign),
            ),),
          new Positioned.fill(
              child: new Material(
                color: Colors.transparent,
                child: new GestureDetector(
                  onTap: () => {googleRegister()},
                ),
              )),
        ]),

        Stack(alignment: Alignment.center, children: [
          Center(
            child:Padding(
              padding: const EdgeInsets.only(left:8.0,right: 8.0),
              child: Image.asset(Res.icon_fb_sign),
            ),),
          new Positioned.fill(
              child: new Material(
                color: Colors.transparent,
                child: new GestureDetector(
                  onTap: () => {fbRegister()},
                ),
              )),
        ])


      ],
    );
  }

  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      if(widget.emailStr != null){
        print("widget.emailStr.isNotEmpty");
        Navigator.of(context)
          ..pop();
      }else{
        Navigator.pop(context);
      }

    } else {
      if(widget.emailStr.isNotEmpty){
        SystemNavigator.pop();
      }
      SystemNavigator.pop();
    }
  }


  void goForget() {}

  void register() {



    // PushNewScreen().normalPush(context, RegisterNamePage());


    Loading.show(context);
    registerFirebase();

    // PushNewScreen().normalPush(context, RegisterPhotoPage());
  }

  Future<void> registerFirebase() async {
    print("registerFirebase");
    try {
      UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: accountEdit.text,
          password: pwdEdit.text
      );
      final firebaseUser = await FirebaseAuth.instance.signInWithEmailAndPassword(email:  accountEdit.text, password:  pwdEdit.text);
      // FirebaseAuth.instance.signOut();

      registerToAvatarAPI("User",accountEdit.text,firebaseUser.user.uid,"normal","");
    } on FirebaseAuthException catch (e) {
      print("FirebaseAuthExc eption : " + e.code);
      Loading.dismiss(context);
      if (e.code == 'weak-password') {
        WarningDialog.showIOSAlertDialog(context, S().register_password_easy_title, S().register_password_easy_content, S().confirm);
      } else if (e.code == 'email-already-in-use') {

        WarningDialog.showIOSAlertDialog2(context,S().register_exit_title, S().register_exit_content,S().keep_going,go2Login);
      }  else if (e.code == 'wrong-password') {
        WarningDialog.showIOSAlertDialog2(context,S().remind, S().personal_logout_content,S().register_exit_content,go2Login);
      }  else if (e.code == 'user-not-found') {

        // WarningDialog.showIOSAlertDialog(context, "已有此帳號密碼", "已有此帳號密碼，請進行登入", "確定");
      }else if (e.code == 'invalid-email') {
        WarningDialog.showIOSAlertDialog(context, S().forget_password_email_format_fail_title, S().forget_password_email_format_fail_content, S().confirm);
      }
      // else if(){
      //   PushNewScreen().normalPush(context, RegisterPhotoPage(email: accountEdit.text,password:));
      // }
    } catch (e) {
      print(e);
    }

    // final firebaseUser = await FirebaseAuth.instance
    //     .signInWithEmailAndPassword(email: email, password: password);
    // firebaseUser.user.emailVerified
  }

  Future<void> go2Login() async {
    PushNewScreen().normalPush(context, LoginPage(email: accountEdit.text,));
  }

  // void registerAPI(String name, String email, String firebase_id,
  //     String login_type, String avatar) async {
  //   print("registerAPI");
  //
  //
  //   ResultData resultData = await AppApi.getInstance()
  //       .register(context, true, name, email, firebase_id, login_type, avatar);
  //
  //   // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
  //   if (resultData.isSuccess()) {
  //     // Notice how you have to call body from the response if you are using http to retrieve json
  //     print(resultData.data.toString());
  //     loginAPI(false, name, email, firebase_id, login_type, avatar);
  //
  //
  //   } else {
  //     Loading.dismiss(context);
  //     WarningDialog.showIOSAlertDialog(context, S().register_fail_title, S().register_fail_hint, S().confirm);
  //   }
  // }
  void registerToAvatarAPI(String name, String email, String firebase_id,
      String login_type, String avatar) async {
    print("registerToAvatarAPI");


    ResultData resultData = await AppApi.getInstance().register(context, true, name, email, firebase_id, login_type, avatar);

    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data.toString());
      loginRegisterAPI(firebase_id, login_type);
      // Loading.dismiss(context);
      // PushNewScreen().normalPush(context, RegisterPhotoPage(user_id: resultData.data["user_id"].toString(),firebase_uid: firebase_id,));
    } else {
      Loading.dismiss(context);

      WarningDialog.showIOSAlertDialog(context, S().register_fail_title, S().register_fail_hint, S().confirm);
    }
  }


  void loginAPI(bool isRegister, String name, String email, String firebase_id, String login_type, String avatar) async {
    print("loginAPI");


    ResultData resultData = await AppApi.getInstance()
        .login(context, true,   firebase_id, login_type);

    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      if(!isRegister){
        print(resultData.data.toString());
        SharePre.prefs.setString(GDefine.name, resultData.data["name"]);
        SharePre.prefs.setString(GDefine.user_id, resultData.data["user_id"].toString());
        SharePre.prefs.setString(GDefine.avatar, resultData.data["avatar"].toString());
        SharePre.prefs.setString(GDefine.gender, resultData.data["gender"].toString());
        SharePre.prefs.setString(GDefine.firebase_id, firebase_id);
        SharePre.prefs.setString(GDefine.is_fcm_open, resultData.data["is_fcm_open"].toString());
        SharePre.prefs.setString(GDefine.description, resultData.data["description"].toString());
        SharePre.prefs.setString(GDefine.invite_code, resultData.data["invite_code"].toString());
        SharePre.prefs.setString(GDefine.login_type, login_type);
        SharePre.prefs.setInt(GDefine.isLogin, 1);
        SharePre.prefs.setString(GDefine.want_companion, resultData.data["want_companion"].toString());
        List<dynamic> trans =  resultData.data["trans"];
        List<dynamic> location =  resultData.data["location"];
        List<dynamic> tourist_img =  resultData.data["tourist_img"];
        List<dynamic> lang =  resultData.data["lang"];
        SharePre.prefs.setString(GDefine.trans, trans.join(","));
        SharePre.prefs.setString(GDefine.location, location.join(","));
        SharePre.prefs.setString(GDefine.tourist_img, tourist_img.join(","));
        SharePre.prefs.setString(GDefine.lang, lang.join(","));

        String role_slug  = resultData.data["role_slug"].toString();
        if(role_slug.contains("guide")){
          SharePre.prefs.setString(GDefine.isGuilder, "true");
        }else {
          SharePre.prefs.setString(GDefine.isGuilder, "false");
        }

        Loading.dismiss(context);
        PushNewScreen().popAllAndPush(context, LobbyPage(articleID: "",guilderID: "",));
      }else{

      }
    } else {
      Loading.dismiss(context);
      print(resultData.data.toString());
      if(resultData.data.toString().contains("查無資料")){
        // registerAPI(name,email,firebase_id,login_type,avatar);
        registerToAvatarAPI(name,email,firebase_id,login_type,"");

      }else if(resultData.data.toString().contains("停用")){
        WarningDialog.showIOSAlertDialog(context,S().account_unactivated_title, S().account_unactivated_hint,S().confirm);

      }else {
        Loading.dismiss(context);
        WarningDialog.showIOSAlertDialog(context, S().login_fail_title, S().login_fail_hint, S().confirm);

      }
      //
      // WarningDialog.show(context, "登入失敗", "註冊失敗");
      // if(resultData.data.toString())
      // registerAPI(name,email,firebase_id,login_type,avatar);
    }
  }

  void loginRegisterAPI( String firebase_id,
      String login_type ) async {
    print("loginRegisterAPI");

    ResultData resultData = await AppApi.getInstance().login(context, true,  firebase_id, login_type);

    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data.toString());



      SharePre.prefs.setString(GDefine.name, resultData.data["name"]);
      SharePre.prefs.setString(GDefine.user_id, resultData.data["user_id"].toString());
      SharePre.prefs.setString(GDefine.avatar, resultData.data["avatar"].toString());
      SharePre.prefs.setString(GDefine.gender, resultData.data["gender"].toString());
      SharePre.prefs.setString(GDefine.firebase_id, firebase_id);
      SharePre.prefs.setString(GDefine.is_fcm_open, resultData.data["is_fcm_open"].toString());
      SharePre.prefs.setString(GDefine.description, resultData.data["description"].toString());
      SharePre.prefs.setString(GDefine.invite_code, resultData.data["invite_code"].toString());
      SharePre.prefs.setString(GDefine.login_type, login_type);
      SharePre.prefs.setInt(GDefine.isLogin, 1);
      SharePre.prefs.setString(GDefine.want_companion, resultData.data["want_companion"].toString());
      List<dynamic> trans =  resultData.data["trans"];
      List<dynamic> location =  resultData.data["location"];
      List<dynamic> tourist_img =  resultData.data["tourist_img"];
      List<dynamic> lang =  resultData.data["lang"];
      SharePre.prefs.setString(GDefine.trans, trans.join(","));
      SharePre.prefs.setString(GDefine.location, location.join(","));
      SharePre.prefs.setString(GDefine.tourist_img, tourist_img.join(","));
      SharePre.prefs.setString(GDefine.lang, lang.join(","));

      String role_slug  = resultData.data["role_slug"].toString();
      if(role_slug.contains("guide")){
        SharePre.prefs.setString(GDefine.isGuilder, "true");
      }else {
        SharePre.prefs.setString(GDefine.isGuilder, "false");
      }




      Loading.dismiss(context);

      PushNewScreen().normalPush(context, RegisterNamePage());
      // PushNewScreen().normalPush(context, RegisterPhotoPage(user_id: resultData.data["user_id"].toString(),firebase_uid: firebase_id,
      //   name :
      //   resultData.data["name"].toString()
      // ));
    } else {
      Loading.dismiss(context);
    if(resultData.data.toString().contains("停用")){
    WarningDialog.showIOSAlertDialog(context,S().account_unactivated_title, S().account_unactivated_hint,S().confirm);

    }else {

      WarningDialog.showIOSAlertDialog(
          context, S().register_fail_title, S().register_fail_hint,
          S().confirm);
    }
    }
  }



  String generateNonce([int length = 32]) {
    final charset =
        '0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._';
    final random = Random.secure();
    return List.generate(length, (_) => charset[random.nextInt(charset.length)])
        .join();
  }

  String sha256ofString(String input) {
    final bytes = utf8.encode(input);
    final digest = sha256.convert(bytes);
    return digest.toString();
  }

  Future<void> appleRegister() async {
    final rawNonce = generateNonce();
    final nonce = sha256ofString(rawNonce);
    final credential = await SignInWithApple.getAppleIDCredential(scopes: [
      AppleIDAuthorizationScopes.email,
      AppleIDAuthorizationScopes.fullName,
    ], nonce: nonce, webAuthenticationOptions: WebAuthenticationOptions());
    print(credential.identityToken);
    print(credential.email);
    print(credential.familyName);
    print(credential.givenName);

    final oauthCredential = OAuthProvider("apple.com").credential(
      idToken: credential.identityToken,
      rawNonce: rawNonce,
    );

    // ignore: avoid_print

    UserCredential userCredential = await FirebaseAuth.instance.signInWithCredential(oauthCredential);

    print("appleLogin ");
    // print(facebookAuthCredential.u) ;
    print(userCredential.user.email);
    print("uid " + userCredential.user.uid);
    print(userCredential.user.displayName);
    print(userCredential.user.photoURL);

    // FirebaseAuth.instance.signOut();
    loginAPI(false,userCredential.user.displayName,userCredential.user.email,userCredential.user.uid,"Apple","");
    // Loading.dismiss(context);
  }

  Future<void> googleRegister() async {
    Loading.show(context);
    GoogleSignIn _googleSignIn = GoogleSignIn(
      scopes: [
        'email',
        'profile',
      ],
    );
    try {
      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();

      user = _googleSignIn.currentUser;
      print(user.email);
      print(user.photoUrl);
      print(user.displayName);
      print(user.id);
      print(user.authentication);
      final GoogleSignInAuthentication googleAuth =
      await googleUser.authentication;

      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      UserCredential userCredential =
      await FirebaseAuth.instance.signInWithCredential(credential);
      print("google ");
      // print(facebookAuthCredential.u) ;
      print(userCredential.user.email);
      print(userCredential.user.uid);
      print("uid " + userCredential.user.uid);
      print(userCredential.user.displayName);
      print(userCredential.user.photoURL);
      // FirebaseAuth.instance.signOut();

      // Loading.dismiss(context);
      _googleSignIn.disconnect();
      loginAPI(false,userCredential.user.displayName,userCredential.user.email,userCredential.user.uid,"Google","");

    } catch (error) {
      print(error);
      Loading.dismiss(context);
    }
  }

  Future<void> fbRegister() async {
    Loading.show(context);
    // Trigger the sign-in flow
    try {
      final LoginResult loginResult = await FacebookAuth.instance.login();
      print("loginResult " + loginResult.toString());
      print("loginResult " + loginResult.accessToken.toString());
      // Create a credential from the access token
      final OAuthCredential facebookAuthCredential =
      FacebookAuthProvider.credential(loginResult.accessToken.token);

      // Once signed in, return the UserCredential
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithCredential(facebookAuthCredential);
      print("fbLogin ");
      // print(facebookAuthCredential.u) ;
      print(userCredential.user.email);
      print(userCredential.user.uid);
      print("uid " + userCredential.user.uid);
      print(userCredential.user.displayName);
      print(userCredential.user.photoURL);
      // FirebaseAuth.instance.signOut();
      loginAPI(false,userCredential.user.displayName,userCredential.user.email,userCredential.user.uid,"FB","");



    } catch (error) {
      print(error);
      Loading.dismiss(context);
    }

    // await FirebaseAuth.instance.signOut();
    //
    // await FacebookAuth.instance.logOut();
  }



}

