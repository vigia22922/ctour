import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Firebase/FirebaseRealtime.dart';
import 'package:ctour/Firebase/message.dart';
import 'package:ctour/Lobby/LobbyPage.dart';
import 'package:ctour/Login/LoginChoosePage.dart';
import 'package:ctour/Login/RegisterNamePage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/Travel/TravelClass.dart';
import 'package:ctour/Travel/TravelPage.dart';
import 'package:ctour/Travel/TravelerPage.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/StatusTools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:cupertino_back_gesture/cupertino_back_gesture.dart';
// import 'package:device_preview/device_preview.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_screenutil/screenutil_init.dart';
import 'package:google_fonts/google_fonts.dart';
import 'Travel/EditParagraphPage.dart';


import 'Guild/GuildReviewPage.dart';
import 'Login/RegisterPage.dart';
import 'Travel/AddTagPage.dart';
import 'Personal/PersonalPage.dart';
import 'Login/ServiceTermsPage.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  await setupFlutterNotifications();
  // showFlutterNotification(message);

  print("Handling a background message ${message.messageId}");
}
/// Create a [AndroidNotificationChannel] for heads up notifications
const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'high_importance_channel', // id
    'High Importance Notifications', // title
    description: 'This channel is used for important notifications.', // description
    importance: Importance.high,
    enableVibration: true,
    playSound: true,
    showBadge: true

);

const IOSNotificationDetails iOSPlatformChannelSpecifics =
IOSNotificationDetails(
  presentAlert: true,  // Present an alert when the notification is displayed and the application is in the foreground (only from iOS 10 onwards)
  presentBadge: true,  // Present the badge number when the notification is displayed and the application is in the foreground (only from iOS 10 onwards)
  presentSound: true,  // Play a sound when the notification is displayed and the application is in the foreground (only from iOS 10 onwards)


);

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();



bool isFlutterLocalNotificationsInitialized = false;

Future<void> setupFlutterNotifications() async {
  // if (isFlutterLocalNotificationsInitialized) {
  //   return;
  // }

  flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  const AndroidInitializationSettings initializationSettingsAndroid =
  AndroidInitializationSettings('ic_stat_login_logo',);
  final IOSInitializationSettings initializationSettingsIOS =
  IOSInitializationSettings(
    requestAlertPermission: true,
    requestBadgePermission: true,
    requestSoundPermission: true,
  );


  final InitializationSettings initializationSettings = InitializationSettings(
    android: initializationSettingsAndroid,
    iOS: initializationSettingsIOS,
  );

  flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<
      AndroidFlutterLocalNotificationsPlugin>().requestPermission();

  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
      AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);


  final bool result = await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
      IOSFlutterLocalNotificationsPlugin>()
      ?.requestPermissions(
    alert: true,
    badge: true,
    sound: true,
  );


  /// Update the iOS foreground notification presentation options to allow
  /// heads up notifications.
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: (String payload) async {
        if (payload != null) {
          debugPrint('notification payload: $payload');
        }

      });

  isFlutterLocalNotificationsInitialized = true;
}

void showFlutterNotification(RemoteMessage message){
  RemoteNotification notification = message.notification;
  if (notification != null   && !kIsWeb) {
    flutterLocalNotificationsPlugin.show(
        notification.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(
            android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channelDescription:channel.description,
                icon: 'ic_stat_login_logo',
                channelShowBadge: true
            ),
            iOS:IOSNotificationDetails(
              presentAlert: true,
              presentBadge: true,
              presentSound: true,
            )
        ));
  }

}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  if (Platform.isAndroid) {
    SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(
      statusBarColor: OwnColors.BGGray, //设置为透明
    );
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
    // SystemChrome.setEnabledSystemUIOverlays([]);

  }
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  await Firebase.initializeApp();

  /// Create an Android Notification Channel.
  ///
  /// We use this channel in the `AndroidManifest.xml` file to override the
  /// default FCM channel to enable heads up notifications.
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  /// Update the iOS foreground notification presentation options to allow
  /// heads up notifications.
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  SharePre.init();
  FirebaseRealtime.init();
  //
  runApp(
      const MyApp()
  );

  // runApp(
  //   DevicePreview(
  //     enabled: true,
  //     // tools: [
  //     //   ...DevicePreview.defaultTools,
  //     //   const CustomPlugin(),
  //     // ],
  //     builder: (context) => const MyApp(),
  //   ),
  // );
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    final textTheme = Theme.of(context).textTheme;

    return ScreenUtilInit(
      designSize: Size(390, 844),
      builder: () => BackGestureWidthTheme(
        backGestureWidth: BackGestureWidth.fraction(1),
        child: MaterialApp(
            localizationsDelegates: [
              S.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales:S.delegate.supportedLocales,
            title: 'Ctour',
            builder: (context, widget) {
              return MediaQuery(
                ///设置文字大小不随系统设置改变
                data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0,boldText: false),
                child: widget,
              );
            },
            theme: ThemeData(
              // This is the theme of your application.
              //
              // Try running your application with "flutter run". You'll see the
              // application has a blue toolbar. Then, without quitting the app, try
              // changing the primarySwatch below to Colors.green and then invoke
              // "hot reload" (press "r" in the console where you ran "flutter run",
              // or simply save your changes to "hot reload" in a Flutter IDE).
              // Notice that the counter didn't reset back to zero; the application
              // is not restarted.
              highlightColor: Colors.transparent,
              splashColor: Colors.transparent,
              primarySwatch: Colors.blue,
              textTheme: GoogleFonts.interTextTheme(textTheme).copyWith(
                bodyText1: GoogleFonts.inter(textStyle: textTheme.bodyText1),
              ),
              pageTransitionsTheme: PageTransitionsTheme(
                builders: {
                  TargetPlatform.android: CupertinoPageTransitionsBuilderCustomBackGestureWidth(),
                  TargetPlatform.iOS: CupertinoPageTransitionsBuilderCustomBackGestureWidth(),
                },
              ),
            ),
            // locale: DevicePreview.locale(context), // Add the locale here
            // builder: DevicePreview.appBuilder, // Add the builder here
            home: MyHomePage(),
            debugShowCheckedModeBanner: false,
            routes: {}),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;
  StreamSubscription link ;
  String articleID = "";
  String guilderID = "";
  @override
  void initState() {
    super.initState();

    StatusTools().setTranBGWhiteText();
    initFCM();
    initDynamicLinks();
    initDynamicNotActiveLinks();
    print("main initState");

    //
    // Timer(Duration(seconds: 1), () {
    //
    //   PushNewScreen().normalPush(context, LoginChoosePage());
    //
    //   // }
    // });
  }

  Future<void> initFCM() async {
    print("initFCM");
    FirebaseMessaging messaging = FirebaseMessaging.instance;
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage message) {
      if (message != null) {
        Navigator.pushNamed(context, '/message',
            arguments: MessageArguments(message, true));
      }
    });
    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    print('User granted permission: ${settings.authorizationStatus}');


    FirebaseMessaging.instance
        .getToken()
        .then((token){
      print("token!!!!" + token);
      SharePre.setString(GDefine.FCMtoken, token);
    });



    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;
      AppleNotification ios = message.notification?.apple;
      print(ios.toString());
      print(android.toString());
      showFlutterNotification(message);

    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published!');
      // PushNewScreen().normalPush(context, LobbyPage(fromFCM: true,));
    });
  }

  Future<void> initDynamicNotActiveLinks() async {
    /// 當 app 處於非活動狀態
    final PendingDynamicLinkData data = await FirebaseDynamicLinks.instance.getInitialLink();
    final Uri deepLink = data?.link;

    if (deepLink != null) {
      if (deepLink.queryParameters.containsKey('articleID')) {
        print("articleID");
        String id = deepLink.queryParameters['articleID'];
        print(id);
        articleID = id ;

        if(SharePre.prefs.getString(GDefine.user_id)!=null){

          Timer(Duration(milliseconds: 800), () {
            loginAPI(SharePre.prefs.getString(GDefine.firebase_id), SharePre.prefs.getString(GDefine.login_type));

          });
        } else {
          WarningDialog.showIOSAlertDialogWithFunc(context,S().login_first_title,  S().login_first_hint, S().confirm,goLogin);
        }
      }else if (deepLink.queryParameters.containsKey('guilderID')) {
        print("guilderID");
        String id = deepLink.queryParameters['guilderID'];
        print(id);
        guilderID = id ;

        if(SharePre.prefs.getString(GDefine.user_id)!=null){
          Timer(Duration(milliseconds: 800), () {
            loginAPI(SharePre.prefs.getString(GDefine.firebase_id), SharePre.prefs.getString(GDefine.login_type));

          });
        } else {
          WarningDialog.showIOSAlertDialogWithFunc(context,S().login_first_title,  S().login_first_hint, S().confirm,goLogin);
        }
      }
    }else {

      if(SharePre.prefs.getString(GDefine.user_id)!=null &&SharePre.prefs.getInt(GDefine.isLogin) != null ){
        if(    SharePre.prefs.getInt(GDefine.isLogin) == 1){
          Timer(Duration(milliseconds: 800), () {
            loginAPI(SharePre.prefs.getString(GDefine.firebase_id), SharePre.prefs.getString(GDefine.login_type));

          });
        }else {
        Timer(Duration(seconds: 1), () {

        PushNewScreen().popAllAndPushFade(context, LoginChoosePage());
        });
        }

      } else {
        Timer(Duration(seconds: 1), () {

          PushNewScreen().popAllAndPushFade(context, LoginChoosePage());
        });
      }


    }

  }

  Future<void> initDynamicLinks() async {
    /// 當 app 處於非活動狀態

    link = dynamicLinks.onLink.listen((dynamicLinkData) {
      print("onLink");
      print(dynamicLinkData.toString());
      print(dynamicLinkData.link.queryParameters.toString());
      print(dynamicLinkData.link.path.toString());
      if (dynamicLinkData.link.queryParameters.containsKey('articleID')) {
        print("articleID");
        String id = dynamicLinkData.link.queryParameters['articleID'];
        print(id);
        articleID = id ;

        if(SharePre.prefs.getString(GDefine.user_id)!=null){


          Future.delayed(Duration(milliseconds: 200), () {
            // 5s over, navigate to a new page
            loginAPI(SharePre.prefs.getString(GDefine.firebase_id), SharePre.prefs.getString(GDefine.login_type));
            // getMyArticleAPI(id);
          });

        }
        else {
          WarningDialog.showIOSAlertDialog(context, S().login_first_title,  S().login_first_hint, S().confirm);
        }
      }else if (dynamicLinkData.link.queryParameters.containsKey('guilderID')) {
        print("guilderID");
        String id = dynamicLinkData.link.queryParameters['guilderID'];
        print(id);
        guilderID = id ;

        if(SharePre.prefs.getString(GDefine.user_id)!=null){

          loginAPI(SharePre.prefs.getString(GDefine.firebase_id), SharePre.prefs.getString(GDefine.login_type));

          // PushNewScreen().normalPushFade(context, TravelerPage(id:id ,));
        }
        else {
          WarningDialog.showIOSAlertDialog(context, S().login_first_title,  S().login_first_hint, S().confirm);
        }
      }

    }) ;
  }


  void goLogin(){
    PushNewScreen().popAllAndPushFade(context, LoginChoosePage());
  }
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      backgroundColor: OwnColors.CTourOrange,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 0.518 * ScreenSize().getWidth(context),
              child: Image.asset(Res.splash_icon),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    print("main dispse");
    if(link != null){
      link.cancel();
      link =null;
    }
    super.dispose();


  }



  // void getMyArticleAPI(String id ) async {
  //   // Loading.show(context);
  //   print("getMyArticleAPI");
  //
  //   ResultData resultData = await AppApi.getInstance().getArticle(context, true,
  //     SharePre.prefs.getString(GDefine.user_id),
  //     id,);
  //
  //   //
  //   //
  //   // Loading.dismiss(context);
  //   if (resultData.isSuccess()) {
  //     print(resultData.data.toString());
  //     var a = resultData.data;
  //
  //     TravelClass travelClass = TravelClass.fromJson(a);
  //
  //     print("name : " + TravelClass
  //         .fromJson(a)
  //         .title
  //         .toString());
  //     print("contentList : " + TravelClass
  //         .fromJson(a)
  //         .contentList
  //         .toString());
  //     print("contentList : " + a["content_list"].toString());
  //     print("DDDDDD" + jsonDecode(a["content_list"].toString()).toString());
  //     if (a["content_list"] != null) {
  //       List<dynamic> content_list = jsonDecode(a["content_list"].toString());
  //       travelClass.contentListDynamic = new List();
  //
  //
  //       List<TravelSubClass> subList = new List();
  //       for (Map<String, dynamic> sub in content_list) {
  //         TravelSubClass travelSubClass = new TravelSubClass();
  //         List<dynamic> content_list2 = sub["contentList"];
  //         List<TravelContentClass> travelContentClassList = new List();
  //
  //         Map<String, dynamic> dy = new Map();
  //         dy["title"] =
  //         sub["title"].toString() == "null" ? "" : sub["title"].toString();
  //         dy["type"] = "0";
  //         dy["description"] = "";
  //         dy["google"] = "";
  //         dy["image"] = [];
  //         travelClass.contentListDynamic.add(dy);
  //
  //
  //         for (Map<String, dynamic> subsub in content_list2) {
  //           TravelContentClass travelContentClass = new TravelContentClass();
  //           travelContentClass.image = List<String>.from(subsub["image"]);
  //           travelContentClass.image.removeWhere((element) =>
  //           element == "" || element == "null");
  //           travelContentClass.title = subsub["title"];
  //           travelContentClass.description =
  //           subsub["description"].toString() == "null"
  //               ? ""
  //               : subsub["description"].toString();
  //           travelContentClass.id = subsub["id"].toString();
  //           travelContentClass.google =
  //           subsub["google"].toString() == "null" ? "" : subsub["google"]
  //               .toString();
  //           travelContentClass.key = new GlobalKey();
  //
  //           travelContentClassList.add(travelContentClass);
  //
  //           Map<String, dynamic> dy2 = new Map();
  //           dy2["title"] = travelContentClass.title;
  //           dy2["type"] = "1";
  //           dy2["description"] = travelContentClass.description;
  //           dy2["google"] = travelContentClass.google;
  //           dy2["image"] = List<String>.from(subsub["image"]);
  //
  //           travelClass.contentListDynamic.add(dy2);
  //         }
  //         travelSubClass.title =
  //         sub["title"].toString() == "null" ? "" : sub["title"].toString();
  //         travelSubClass.id = sub["id"].toString();
  //         travelSubClass.key = new GlobalKey();
  //
  //
  //         if (travelContentClassList.isNotEmpty) {
  //           travelSubClass.content = travelContentClassList;
  //         }
  //
  //         subList.add(travelSubClass);
  //       }
  //
  //       travelClass.contentList = subList;
  //
  //       PushNewScreen().normalPushFade(context, TravelPage(travelClass: travelClass,edit: false,));
  //
  //     }
  //   }
  //
  //   else {
  //     if(resultData.data.toString().contains("取消")){
  //       WarningDialog.showIOSAlertDialog(context, S().travel_article_not_found_title  , S().travel_article_not_found_hint2,S().confirm);
  //     }
  //     else {
  //       WarningDialog.showIOSAlertDialog(
  //           context, S().travel_article_not_found_title,
  //           S().travel_article_not_found_hint1, S().confirm);
  //     }
  //   }
  //
  //
  //
  // }


  void loginAPI(String firebase_id, String login_type) async {
    print("loginAPI");
    // Loading.show(context);

    ResultData resultData = await AppApi.getInstance().login(context, true,  firebase_id, login_type);
    // Loading.dismiss(context);
    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data.toString());

      SharePre.prefs.setString(GDefine.name, resultData.data["name"]);
      SharePre.prefs.setString(
          GDefine.user_id, resultData.data["user_id"].toString());
      SharePre.prefs.setString(
          GDefine.avatar, resultData.data["avatar"].toString());
      SharePre.prefs.setString(
          GDefine.gender, resultData.data["gender"].toString());
      SharePre.prefs.setString(GDefine.firebase_id, firebase_id);
      SharePre.prefs.setString(
          GDefine.is_fcm_open, resultData.data["is_fcm_open"].toString());
      SharePre.prefs.setString(
          GDefine.description, resultData.data["description"].toString());
      SharePre.prefs.setString(
          GDefine.invite_code, resultData.data["invite_code"].toString());
      SharePre.prefs.setString(GDefine.want_companion, resultData.data["want_companion"].toString());
      SharePre.prefs.setInt(GDefine.isLogin, 1);

      List<dynamic> trans = resultData.data["trans"];
      List<dynamic> location = resultData.data["location"];
      List<dynamic> tourist_img = resultData.data["tourist_img"];
      List<dynamic> lang = resultData.data["lang"];
      SharePre.prefs.setString(GDefine.trans, trans.join("," ));
      SharePre.prefs.setString(GDefine.location, location.join(","));
      SharePre.prefs.setString(GDefine.tourist_img, tourist_img.join(","));
      SharePre.prefs.setString(GDefine.lang, lang.join(","));

      String role_slug = resultData.data["role_slug"].toString();
      if (role_slug.contains("guide")) {
        SharePre.prefs.setString(GDefine.isGuilder, "true");
      } else {
        SharePre.prefs.setString(GDefine.isGuilder, "false");
      }
      await getBlockList();
      if(resultData.data["status"].toString()  == "5"){
        PushNewScreen().normalPush(context, RegisterNamePage());
      }else {
        PushNewScreen().popAllAndPushFade(
            context, LobbyPage(articleID: articleID, guilderID: guilderID,));
      }

    }else {
      if (resultData.data.toString().contains("停用")) {
        SharePre.prefs.setInt(GDefine.isLogin, 0);
        WarningDialog.showIOSAlertDialog2(
            context, S().account_unactivated_title,
            S().account_unactivated_hint, S().confirm, goChoose);
      } else if(resultData.data.toString().contains("查無")){
    print(resultData.data.toString());
    SharePre.prefs.setInt(GDefine.isLogin, 0);
    WarningDialog.showIOSAlertDialog2(context,S().login_fail_title, S().login_fail_no_user_hint,S().confirm,goChoose);

    }else {
        print(resultData.data.toString());
        SharePre.prefs.setInt(GDefine.isLogin, 0);
        WarningDialog.showIOSAlertDialog2(
            context, S().login_fail_title, resultData.data.toString(), S().confirm,
            goChoose);
      }

    }
  }

  goChoose(){


      PushNewScreen().popAllAndPushFade(context, LoginChoosePage());

  }
  void getBlockList( ) async {
    print("getBlockList");


    ResultData resultData = await AppApi.getInstance()
        .getUserBlockList(context, true);

    print(resultData.data.toString());
    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      GDefine.blockUserId.clear();
      for (Map<String, dynamic> a in resultData.data) {
        if(!GDefine.blockUserId.contains(a ["block_user_id"].toString())) {
          GDefine.blockUserId.add(a ["block_user_id"].toString());
        }
      }

    }
  }

}
