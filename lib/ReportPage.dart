import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/OrangeXCross.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Firebase/FirebaseDynamicLink.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Guild/GuildReviewPage.dart';
import 'package:ctour/Guild/GuilderTravelPage.dart';
import 'package:ctour/Home/HomeFilterPage.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/Travel/NewTravelPage.dart';
import 'package:ctour/Travel/TravelCommentClass.dart';
import 'package:ctour/Travel/TravelMenuPage.dart';
import 'package:ctour/Travel/TravelerPage.dart';
import 'package:ctour/tool/CropSample.dart';
import 'package:ctour/tool/CropTool.dart';
import 'package:ctour/tool/DateTools.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/StatusTools.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icons.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:sliver_tools/sliver_tools.dart';
import 'package:url_launcher/url_launcher.dart';
import '../Network/API/app_api.dart';

import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/TopBar.dart';
import '../res.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_star/flutter_star.dart';
import 'package:flutter/material.dart';

class ReportPage extends StatefulWidget {

  int type; // 1-> traveler 2 article  3 文章留言





  ReportPage(
      {Key key,
        this.type,



      })
      : super(key: key);

  @override
  State createState() {
    return _ReportPageState();
  }
}

class _ReportPageState extends State<ReportPage>with TickerProviderStateMixin {



  @override
  void initState() {
    super.initState();

    // StatusTools().setWhitBGDarkText();

  }



  @override
  void dispose() {
    super.dispose();
    // StatusTools().setTra();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.white,
        extendBodyBehindAppBar: true,

        body:  Column(
          children: [
            ScreenSize().createTopPad(context),
            HorizontalSpace().create(context, 0.028),
            Container(
              width: 0.9.sw,
              child: Row(
                children: [
                  GestureDetector(
                    onTap: (){
                      backClick();
                    },
                    child: SizedBox(
                      height: 0.019.sh,
                      width: 0.019.sh,
                      child: Image.asset(Res.xmark),
                    ),
                  ),
                  Expanded(
                    child: Center(
                      child: Text(
                        S().report_reason,
                        style: GoogleFonts.inter(
                          fontSize: 19.nsp,
                          color: OwnColors.black,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Opacity(
                    opacity: 0.0,
                    child: SizedBox(
                      height: 0.019.sh,
                      width: 0.019.sh,
                      child: Image.asset(Res.xmark),
                    ),
                  )

                ],
              ),
            ),

            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      width: 0.9.sw,
                      height: 0.02.sh /2 ,
                    ),
                    item(S().report_reason1),
                    item(S().report_reason2),
                    item(S().report_reason3),
                    item(S().report_reason4),
                    item(S().report_reason5),
                    item(S().report_reason6),
                    item(S().report_reason7),
                    item(S().report_reason8),
                    item(S().report_reason9),

                  ],
                ),
              ),
            )

          ],
        ));
  }


  Widget item(String title){
    return GestureDetector(
      onTap: (){
        print("tap" + title) ;
        report(title);
      },
      child: Column(
        children: [
          Container(
            width: 0.9.sw,
            height: 0.04.sh /2 ,
          ),
          Container(
            width: 0.9.sw,
            child: Text(
              title,
              style: GoogleFonts.inter(
                fontSize: 17.nsp,
                color: OwnColors.black,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          Container(
            width: 0.9.sw,
            height: 0.04.sh/2,
          ),


        ],
      ),
    );
  }

  Future<void> report(String content) async {
    Loading.show(context);

    ResultData resultData =
    await AppApi.getInstance().setUserArticleAppeal(context, true, widget.type.toString(),content);

    if (resultData.isSuccess()) {
      Loading.dismiss(context);
      WarningDialog.showIOSAlertDialogWithFunc(context, S().report_res_title, S().report_res_content, S().confirm,backClick);
    } else {
      Loading.dismiss(context);
      WarningDialog.showIOSAlertDialogWithFunc(context, S().report_res_title, S().report_res_content, S().confirm,backClick);

    }
  }


  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }



}

