import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

class PushNewScreen {



  void normalPush(context,Widget statefulWidget){
    FocusScope.of(context).unfocus();
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return statefulWidget;
        },
      ),
    );

    // Navigator.push(context,
    //     MaterialPageRoute(builder: (_) => statefulWidget)
    // );
    // pushNewScreen(
    //   context,
    //   screen: statefulWidget,
    //   withNavBar: false, // OPTIONAL VALUE. True by default.
    //
    //   // pageTransitionAnimation: PageTransitionAnimation.cupertino,
    // );
  }
  void normalPushFade(context,Widget statefulWidget){
    FocusScope.of(context).unfocus();


    Navigator.push(
      context,
      PageRouteBuilder(
        pageBuilder: (c, a1, a2) => statefulWidget,
        transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
        transitionDuration: Duration(milliseconds: 800),

      )

    );

  }
  void normalPushThen(context,Widget statefulWidge,Function then){

    Navigator.push(context,
        MaterialPageRoute(builder: (_) => statefulWidge)
    ).then((value) => then());


  }

  void popAndPush(context,Widget statefulWidget){
    FocusScope.of(context).unfocus();
    // Navigator.pushReplacement(context,
    //     MaterialPageRoute(builder: (_) => statefulWidget)
    // );

    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) {
          return statefulWidget;
        },
      ),
    );
    // pushNewScreen(
    //   context,
    //   screen: statefulWidget,
    //   withNavBar: false, // OPTIONAL VALUE. True by default.
    //   // pageTransitionAnimation: PageTransitionAnimation.cupertino,
    // );


  }
  void popAllAndPush(context,Widget statefulWidget){
    FocusScope.of(context).unfocus();

    Navigator.of(context).pushAndRemoveUntil(
      MaterialPageRoute(builder: (context) => statefulWidget), (Route<dynamic> route) => false,

    );
      //
      // Navigator.pushAndRemoveUntil(
      // context,
      // MaterialPageRoute(builder: (context) => statefulWidget), (Route<dynamic> route) => false,
      //
      // );
  }

  void popAllAndPushFade(context,Widget statefulWidget){
    FocusScope.of(context).unfocus();

    Navigator.of(context).pushAndRemoveUntil(
      PageRouteBuilder(
        pageBuilder: (c, a1, a2) => statefulWidget,
        transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
        transitionDuration: Duration(milliseconds: 300),

      )
      , (Route<dynamic> route) => false,
    );

    // Navigator.pushAndRemoveUntil(
    //   context,
    //   PageRouteBuilder(
    //     pageBuilder: (c, a1, a2) => statefulWidget,
    //     transitionsBuilder: (c, anim, a2, child) => FadeTransition(opacity: anim, child: child),
    //     transitionDuration: Duration(milliseconds: 300),
    //
    //   )
    //   , (Route<dynamic> route) => false,
    // );

  }



}