import 'dart:convert';
import 'dart:io';

import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class PickAndUploadImg {

  static Future<String>  showBottomMenu(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 150,
              child: Column(children: <Widget>[
                ListTile(


                  leading: Icon(Icons.photo_camera),
                  title: Text(S().take_pic),
                  onTap: () async {
                    Navigator.pop(context);
                    return await pcikImageFromCamera(context);
                  },
                ),
                ListTile(
                  leading: Icon(Icons.photo_library),
                  title: Text(S().choose_pic),
                  onTap: () async {
                    Navigator.pop(context);
                    return await pcikImageFromPhoto(context);
                  },
                )
              ]));
        });
  }

  static Future<String> pcikImageFromCamera(BuildContext context) async {
    final picker = ImagePicker();
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);

    File img = File(pickedFile.path);
    List<int> imageBytes = await img.readAsBytes();

    String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
    print("base64Image: " + base64Image);
    String url = await  PickAndUploadImg.uploadImage(base64Image,context);
    return url;
  }

  static Future<String> pcikImageFromPhoto(BuildContext context) async {
    final picker = ImagePicker();
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 720,
        maxWidth: 720,
        imageQuality: 50);
    File img = File(pickedFile.path);
    List<int> imageBytes = await img.readAsBytes();
    String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
    print("base64Image: " + base64Image);
    String url = await  PickAndUploadImg.uploadImage(base64Image,context);
    return url;

  }

  static Future<String> uploadImage(String data,BuildContext context) async {
    // Loading.show(context);

    ResultData resultData =
    await AppApi.getInstance().setFileBase64(context, true, data);

    if (resultData.isSuccess()) {
      // Loading.dismiss(context);
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data);
      String img_url = GDefine.imageUrl + resultData.data["path"];
      print("img_url " + img_url);
      return img_url;

    } else {
      // Loading.dismiss(context);
      WarningDialog.showIOSAlertDialog(context, S().upload_fail_title, S().upload_fail_hint, S().confirm);
      return "";
    }
  }


}