import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:ctour/tool/OwnColors.dart';

class Loading extends StatelessWidget {


  static void show(BuildContext context) {

    showDialog(
      barrierDismissible: true,
      context: context,
      barrierColor: OwnColors.tran,
      builder: (ctx) => Theme(
        data: Theme.of(ctx).copyWith(dialogBackgroundColor: Colors.white),
        child: Loading(),
      ),
    );
  }

  static void dismiss(context) {
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Center(
        child: Container(
          decoration: BoxDecoration(
            color: OwnColors.tran.withOpacity(0.0),
            borderRadius: BorderRadius.circular(5),
          ),
          width: 75,
          height: 75,
          alignment: Alignment.center,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SpinKitRing(
                lineWidth: 5,
                color: OwnColors.CTourOrange,
                size: 60.0,
              )
            ],
          ),
        ),
      ),
    );
  }
}