import 'package:shared_preferences/shared_preferences.dart';

class SharePre {
  static SharedPreferences prefs;
  static Future<void> init() async {
     prefs = await SharedPreferences.getInstance();
  }

  static getString(String id)  {
    return prefs.getString(id);
  }

  static setString(String id,String content) async {
    return prefs.setString(id,content);
  }

}