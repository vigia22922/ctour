import 'package:auto_size_text/auto_size_text.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:just_the_tooltip/just_the_tooltip.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/ScreenSize.dart';

import 'OwnColors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WarningDialog {

  static void showIOSAlertDialogPercentage(
      BuildContext context, String title, String content, String btnText,double per) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context)
        {return WillPopScope(
          onWillPop: () => Future.value(false),
          child: CupertinoAlertDialog(
            title: Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: Text(
                title,
                style: TextStyle(fontSize: 18.sp,fontWeight: FontWeight.bold),
              ),
            ),
            content: Container(
              child:
              new CircularPercentIndicator(
                radius: 100.0,
                lineWidth: 10.0,
                percent: per,
                header: new Text("Icon header"),
                center: new Icon(
                  Icons.person_pin,
                  size: 50.0,
                  color: Colors.blue,
                ),
                backgroundColor: Colors.grey,
                progressColor: Colors.blue,
              ),
            ),
            actions: <Widget>[
              CupertinoDialogAction(
                  isDefaultAction: true,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(btnText)),
            ],
          ),
        );});
  }

  // static void showIOSAlertDialog(
  //
  //     BuildContext context, String title, String content, String btnText) {
  //   Tools().dismissK(context);
  //   showDialog(
  //       context: context,
  //       barrierDismissible: false,
  //       builder: (context)
  //       {return WillPopScope(
  //         onWillPop: () => Future.value(false),
  //         child:
  //
  //         CupertinoAlertDialog(
  //
  //           title: Padding(
  //             padding: const EdgeInsets.only(bottom: 16.0),
  //             child: Text(
  //               title,
  //               style: GoogleFonts.inter(
  //                   fontSize: 16.nsp,
  //                 fontWeight: FontWeight.w300
  //               ),
  //             ),
  //           ),
  //           content: Text(
  //             content,
  //             style: TextStyle(fontSize: ScreenUtil().setSp(16)),
  //           ),
  //           actions: <Widget>[
  //             CupertinoDialogAction(
  //                 isDefaultAction: true,
  //                 onPressed: () {
  //                   Navigator.pop(context);
  //                 },
  //                 child: Text(btnText)),
  //           ],
  //         ),
  //       );});
  // }


  static void showIOSAlertDialog(

      BuildContext context, String title, String content, String btnText) {
    Tools().dismissK(context);
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context)
        {return WillPopScope(
          onWillPop: (){
            Tools().dismissK(context);
          },
          child: GestureDetector(
            onTap: (){
              Tools().dismissK(context);
            },
            child: AnimatedContainer(
              padding: EdgeInsets.only(bottom:
              MediaQuery.of(context).viewInsets.bottom == 0?
              0:
              0.2.sh ),
              duration: const Duration(milliseconds: 300),
              child: Center(
                child: Material(
                  type: MaterialType.transparency,
                  child: Container(
                    height: 1.0.sh,
                    child: Column(
                      children: [
                        Spacer(),
                        Center(
                          child: Container(

                            width: ScreenSize().getWidth(context)*0.63,
                            decoration: BoxDecoration(
                              color: OwnColors.white,
                              borderRadius:
                              BorderRadius.all(Radius.circular(12)),

                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                //HorizontalSpace().create(context, 0.005),
                                Container(
                                  constraints: BoxConstraints(
                                    minHeight: ScreenSize().getHeight(context)*0.053,

                                  ),

                                  // width: ScreenSize().getWidth(context)*0.6,
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left:8.0,right: 8.0, top: 14, bottom: 14),
                                      child: Text(title,textAlign: TextAlign.center,
                                        style: GoogleFonts.inter(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold,
                                          color: OwnColors.black
                                      ),),
                                    ),
                                  ),
                                ),

                                Container(
                                  // constraints: BoxConstraints(
                                  //   minHeight: ScreenSize().getHeight(context)*0.053,
                                  //
                                  // ),

                                  // width: ScreenSize().getWidth(context)*0.6,
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left:20.0,right: 20.0, bottom: 14.0),
                                      child: Text(content,textAlign: TextAlign.center,
                                        style: GoogleFonts.inter(
                                          fontSize: 14.sp,

                                          fontWeight: FontWeight.normal,
                                          color: OwnColors.black
                                      ),),
                                    ),
                                  ),
                                ),



                                //HorizontalSpace().create(context, 0.01),


                                Container(
                                  height: ScreenSize().getHeight(context)*0.052,
                                  decoration: BoxDecoration(
                                    border: Border(
                                      top: BorderSide(width:1.0, color: OwnColors.CC8C8C8),

                                    ),
                                    color: Colors.transparent,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: (){
                                            Navigator.pop(context);
                                          },
                                          child: Container(
                                            height: ScreenSize().getHeight(context)*0.052,
                                            width: double.infinity,

                                            decoration: BoxDecoration(

                                                color: Colors.white,
                                                borderRadius: BorderRadius.only(
                                                    bottomRight: Radius.circular(12),
                                                    bottomLeft: Radius.circular(12))
                                            ),
                                            child: Center(
                                              child: Text(
                                                S().confirm,
                                                style: GoogleFonts.inter(color: OwnColors.BtnBlue, fontSize: 16.nsp),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),

                                     ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );});
  }

  static void showIOSAlertDialogWithFunc(

      BuildContext context, String title, String content, String btnText,Function function) {
    Tools().dismissK(context);
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context)
        {return WillPopScope(
          onWillPop: (){
            Tools().dismissK(context);
          },
          child: GestureDetector(
            onTap: (){
              Tools().dismissK(context);
            },
            child: AnimatedContainer(
              padding: EdgeInsets.only(bottom:
              MediaQuery.of(context).viewInsets.bottom == 0?
              0:
              0.2.sh ),
              duration: const Duration(milliseconds: 300),
              child: Center(
                child: Material(
                  type: MaterialType.transparency,
                  child: Container(
                    height: 1.0.sh,
                    child: Column(
                      children: [
                        Spacer(),
                        Center(
                          child: Container(

                            width: ScreenSize().getWidth(context)*0.63,
                            decoration: BoxDecoration(
                              color: OwnColors.white,
                              borderRadius:
                              BorderRadius.all(Radius.circular(12)),

                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                //HorizontalSpace().create(context, 0.005),
                                Container(
                                  // constraints: BoxConstraints(
                                  //   minHeight: ScreenSize().getHeight(context)*0.053,
                                  //
                                  // ),

                                  // width: ScreenSize().getWidth(context)*0.6,
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left:8.0,right: 8.0, top: 14, bottom: 14),
                                      child: Text(title,
                                        textAlign: TextAlign.center,style: GoogleFonts.inter(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold,
                                          color: OwnColors.black
                                      ),),
                                    ),
                                  ),
                                ),

                                Container(
                                  // constraints: BoxConstraints(
                                  //   minHeight: ScreenSize().getHeight(context)*0.053,
                                  //
                                  // ),

                                  // width: ScreenSize().getWidth(context)*0.6,
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left:20.0,right: 20.0),
                                      child: Text(content,textAlign: TextAlign.center,
                                        style: GoogleFonts.inter(
                                            fontSize: 14.sp,

                                            fontWeight: FontWeight.normal,
                                            color: OwnColors.black
                                        ),),
                                    ),
                                  ),
                                ),



                                HorizontalSpace().create(context, 0.01),


                                Container(
                                  height: ScreenSize().getHeight(context)*0.052,
                                  decoration: BoxDecoration(
                                    border: Border(
                                      top: BorderSide(width:1.0, color: OwnColors.CC8C8C8),

                                    ),
                                    color: Colors.transparent,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: (){
                                            Navigator.pop(context);
                                            function();
                                          },
                                          child: Container(
                                            height: ScreenSize().getHeight(context)*0.052,
                                            width: double.infinity,

                                            decoration: BoxDecoration(

                                                color: Colors.white,
                                                borderRadius: BorderRadius.only(
                                                    bottomRight: Radius.circular(12),
                                                    bottomLeft: Radius.circular(12))
                                            ),
                                            child: Center(
                                              child: Text(
                                                S().confirm,
                                                style: GoogleFonts.inter(color: OwnColors.BtnBlue, fontSize: 16.nsp),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),

                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );});
  }


  static void showIOSAlertDialog2(BuildContext context, String title,
      String content, String btnText, Function function) {
    Tools().dismissK(context);
    showDialog(

        context: context,
        barrierDismissible: false,
        builder: (context)
        {return
          WillPopScope(
            onWillPop: (){
              Tools().dismissK(context);
            },
            child: GestureDetector(
              onTap: (){
                Tools().dismissK(context);
              },
              child: AnimatedContainer(
                padding: EdgeInsets.only(bottom:
                MediaQuery.of(context).viewInsets.bottom == 0?
                0:
                0.2.sh ),
                duration: const Duration(milliseconds: 300),
                child: Center(
                  child: Material(
                    type: MaterialType.transparency,
                    child: Container(
                      height: 1.0.sh,
                      child: Column(
                        children: [
                          Spacer(),
                          Center(
                            child: Container(

                              width: ScreenSize().getWidth(context)*0.63,
                              decoration: BoxDecoration(
                                color: OwnColors.white,
                                borderRadius:
                                BorderRadius.all(Radius.circular(12)),

                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  //orizontalSpace().create(context, 0.005),
                                  Container(
                                    // constraints: BoxConstraints(
                                    //   minHeight: ScreenSize().getHeight(context)*0.053,
                                    //
                                    // ),

                                    // width: ScreenSize().getWidth(context)*0.6,
                                    child: Center(
                                      child: Padding(
                                        padding: const EdgeInsets.only(left:8.0,right: 8.0, top: 14, bottom: 14),
                                        child: Text(
                                          title,
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.inter(
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.w700,
                                            color: OwnColors.black
                                        ),),
                                      ),
                                    ),
                                  ),

                                  Container(
                                    // constraints: BoxConstraints(
                                    //   minHeight: ScreenSize().getHeight(context)*0.053,
                                    //
                                    // ),

                                    // width: ScreenSize().getWidth(context)*0.6,
                                    child: Center(
                                      child: Padding(
                                        padding: const EdgeInsets.only(left:20.0,right: 20.0),
                                        child: Text(content,textAlign: TextAlign.center,
                                          style: GoogleFonts.inter(
                                              fontSize: 14.sp,

                                              fontWeight: FontWeight.normal,
                                              color: OwnColors.black
                                          ),),
                                      ),
                                    ),
                                  ),



                                  HorizontalSpace().create(context, 0.01),


                                  Container(
                                    height: ScreenSize().getHeight(context)*0.052,
                                    decoration: BoxDecoration(
                                      border: Border(
                                        top: BorderSide(width:1.0, color: OwnColors.CC8C8C8),

                                      ),
                                      color: Colors.transparent,
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: GestureDetector(
                                            onTap: (){
                                              Navigator.pop(context);
                                            },
                                            child: Container(
                                              height: ScreenSize().getHeight(context)*0.052,
                                              width: double.infinity,

                                              decoration: BoxDecoration(

                                                  color: Colors.white,
                                                  borderRadius: BorderRadius.only(

                                                      bottomLeft: Radius.circular(12))
                                              ),
                                              child: Center(
                                                child: Text(
                                                  S().cancel,
                                                  style: GoogleFonts.inter(color: OwnColors.BtnBlue, fontSize: 16.nsp),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: ScreenSize().getHeight(context)*0.052,
                                          width: 1,
                                          color: OwnColors.CC8C8C8,
                                        ),
                                        Expanded(
                                          child: GestureDetector(
                                            onTap: (){
                                              Navigator.pop(context);
                                              function();
                                            },
                                            child: Container(

                                              height: ScreenSize().getHeight(context)*0.052,
                                              width: double.infinity,
                                              decoration: BoxDecoration(

                                                borderRadius: BorderRadius.only(

                                                    bottomRight: Radius.circular(12)),
                                                color: Colors.white,
                                              ),
                                              child: Center(
                                                child: Text(
                                                  btnText,
                                                  style: TextStyle(color: btnText==S().delete?OwnColors.TextRed:btnText==S().personal_logout?OwnColors.TextRed:OwnColors.BtnBlue, fontSize: 16.nsp),
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Spacer(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );


          WillPopScope(
          onWillPop: () => Future.value(false),
          child: CupertinoAlertDialog(
            title: Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: Text(
                title,
                style: TextStyle(fontSize: 18.sp),
              ),
            ),
            content: Text(content),
            actions: <Widget>[
              CupertinoDialogAction(
                  isDefaultAction: true,
                  onPressed: () {
                    Navigator.pop(context);

                  },
                  child: Text(S().cancel)),
              CupertinoDialogAction(
                  isDefaultAction: true,
                  onPressed: () {
                    Navigator.pop(context);
                    function();
                  },
                  child: Text(btnText)),
            ],
          ),
        );});
  }


  // static void showIOSAlertDialog2(BuildContext context, String title,
  //     String content, String btnText, Function function) {
  //   Tools().dismissK(context);
  //   showDialog(
  //
  //       context: context,
  //       barrierDismissible: false,
  //       builder: (context)
  //       {return WillPopScope(
  //         onWillPop: () => Future.value(false),
  //         child: CupertinoAlertDialog(
  //           title: Padding(
  //             padding: const EdgeInsets.only(bottom: 16.0),
  //             child: Text(
  //               title,
  //               style: TextStyle(fontSize: 18.sp),
  //             ),
  //           ),
  //           content: Text(content),
  //           actions: <Widget>[
  //             CupertinoDialogAction(
  //                 isDefaultAction: true,
  //                 onPressed: () {
  //                   Navigator.pop(context);
  //
  //                 },
  //                 child: Text(S().cancel)),
  //             CupertinoDialogAction(
  //                 isDefaultAction: true,
  //                 onPressed: () {
  //                   Navigator.pop(context);
  //                   function();
  //                 },
  //                 child: Text(btnText)),
  //           ],
  //         ),
  //       );});
  // }

  static void showIOSAlertDialog3(BuildContext context, String title,
      String content, String btnText1, String btnText2, Function function1, Function function2) {
    Tools().dismissK(context);
    showDialog(

        context: context,
        barrierDismissible: false,
        builder: (context)
        {return           WillPopScope(
          onWillPop: (){
            Tools().dismissK(context);
          },
          child: GestureDetector(
            onTap: (){
              Tools().dismissK(context);
            },
            child: AnimatedContainer(
              padding: EdgeInsets.only(bottom:
              MediaQuery.of(context).viewInsets.bottom == 0?
              0:
              0.2.sh ),
              duration: const Duration(milliseconds: 300),
              child: Center(
                child: Material(
                  type: MaterialType.transparency,
                  child: Container(
                    height: 1.0.sh,
                    child: Column(
                      children: [
                        Spacer(),
                        Center(
                          child: Container(

                            width: ScreenSize().getWidth(context)*0.63,
                            decoration: BoxDecoration(
                              color: OwnColors.white,
                              borderRadius:
                              BorderRadius.all(Radius.circular(12)),

                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                //HorizontalSpace().create(context, 0.005),
                                Container(
                                  // constraints: BoxConstraints(
                                  //   minHeight: ScreenSize().getHeight(context)*0.053,
                                  //
                                  // ),

                                  // width: ScreenSize().getWidth(context)*0.6,
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left:8.0,right: 8.0, top: 14, bottom: 14),
                                      child: Text(title,textAlign: TextAlign.center,style: GoogleFonts.inter(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold,
                                          color: OwnColors.black
                                      ),),
                                    ),
                                  ),
                                ),

                                Container(
                                  // constraints: BoxConstraints(
                                  //   minHeight: ScreenSize().getHeight(context)*0.053,
                                  //
                                  // ),

                                  // width: ScreenSize().getWidth(context)*0.6,
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left:20.0,right: 20.0),
                                      child: Text(content,textAlign: TextAlign.center,
                                        style: GoogleFonts.inter(
                                            fontSize: 14.sp,

                                            fontWeight: FontWeight.normal,
                                            color: OwnColors.black
                                        ),),
                                    ),
                                  ),
                                ),



                                HorizontalSpace().create(context, 0.01),


                                Container(
                                  height: ScreenSize().getHeight(context)*0.052,
                                  decoration: BoxDecoration(
                                    border: Border(
                                      top: BorderSide(width:1.0, color: OwnColors.CC8C8C8),

                                    ),
                                    color: Colors.transparent,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: (){
                                            Navigator.pop(context);
                                            function1();
                                          },
                                          child: Container(
                                            height: ScreenSize().getHeight(context)*0.052,
                                            width: double.infinity,

                                            decoration: BoxDecoration(

                                                color: Colors.white,
                                                borderRadius: BorderRadius.only(

                                                    bottomLeft: Radius.circular(12))
                                            ),
                                            child: Center(
                                              child: Text(
                                                btnText1,
                                                style: GoogleFonts.inter(color: OwnColors.BtnBlue, fontSize: 16.nsp),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        height: ScreenSize().getHeight(context)*0.052,
                                        width: 1,
                                        color: OwnColors.CC8C8C8,
                                      ),
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: (){
                                            Navigator.pop(context);
                                            function2();
                                          },
                                          child: Container(

                                            height: ScreenSize().getHeight(context)*0.052,
                                            width: double.infinity,
                                            decoration: BoxDecoration(

                                              borderRadius: BorderRadius.only(

                                                  bottomRight: Radius.circular(12)),
                                              color: Colors.white,
                                            ),
                                            child: Center(
                                              child: Text(
                                                btnText2,
                                                style: GoogleFonts.inter(color: OwnColors.BtnBlue, fontSize: 16.nsp),
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );



          WillPopScope(
          onWillPop: () => Future.value(false),
          child: CupertinoAlertDialog(
            title: Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: Text(
                title,
                style: GoogleFonts.inter(
                    fontSize: 16.nsp,
                    fontWeight: FontWeight.w600
                ),
              ),
            ),
            content: Text(content,
              style: GoogleFonts.inter(
                  fontSize: 14.nsp,
                  fontWeight: FontWeight.w200
              ),),
            actions: <Widget>[
              CupertinoDialogAction(
                  isDefaultAction: true,
                  onPressed: () {
                    Navigator.pop(context);
                    function1();
                  },
                  child: Text(btnText1,style:GoogleFonts.inter(
                    color: OwnColors.TextRed,
                    fontWeight: FontWeight.w300,
                  ),)),
              CupertinoDialogAction(
                  isDefaultAction: true,
                  onPressed: () {
                    Navigator.pop(context);
                    function2();
                  },
                  child: Text(btnText2,style:GoogleFonts.inter(
                    color: OwnColors.BtnBlue,
                    fontWeight: FontWeight.w300,))
              ),
            ],
          ),
        );});
  }

  static void showIOSAlertDialog2BtnFunction(
      BuildContext context, String title, String content, Function function) {
    Tools().dismissK(context);
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context)
        {return WillPopScope(
          onWillPop: (){
            Tools().dismissK(context);
          },
          child: GestureDetector(
            onTap: (){
              Tools().dismissK(context);
            },
            child: AnimatedContainer(
              padding: EdgeInsets.only(bottom:
              MediaQuery.of(context).viewInsets.bottom == 0?
              0:
              0.2.sh ),
              duration: const Duration(milliseconds: 300),
              child: Center(
                child: Material(
                  type: MaterialType.transparency,
                  child: Container(
                    height: 1.0.sh,
                    child: Column(
                      children: [
                        Spacer(),
                        Center(
                          child: Container(

                            width: ScreenSize().getWidth(context)*0.63,
                            decoration: BoxDecoration(
                              color: OwnColors.white,
                              borderRadius:
                              BorderRadius.all(Radius.circular(12)),

                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                //HorizontalSpace().create(context, 0.005),
                                Container(
                                  // constraints: BoxConstraints(
                                  //   minHeight: ScreenSize().getHeight(context)*0.053,
                                  //
                                  // ),

                                  // width: ScreenSize().getWidth(context)*0.6,
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left:8.0,right: 8.0, top: 14, bottom: 14),
                                      child: Text(title,textAlign: TextAlign.center,style: GoogleFonts.inter(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold,
                                          color: OwnColors.black
                                      ),),
                                    ),
                                  ),
                                ),

                                Container(
                                  // constraints: BoxConstraints(
                                  //   minHeight: ScreenSize().getHeight(context)*0.053,
                                  //
                                  // ),

                                  // width: ScreenSize().getWidth(context)*0.6,
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left:20.0,right: 20.0),
                                      child: Text(content,textAlign: TextAlign.center,
                                        style: GoogleFonts.inter(
                                            fontSize: 14.sp,

                                            fontWeight: FontWeight.normal,
                                            color: OwnColors.black
                                        ),),
                                    ),
                                  ),
                                ),



                                HorizontalSpace().create(context, 0.01),


                                Container(
                                  height: ScreenSize().getHeight(context)*0.052,
                                  decoration: BoxDecoration(
                                    border: Border(
                                      top: BorderSide(width:1.0, color: OwnColors.CC8C8C8),

                                    ),
                                    color: Colors.transparent,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: (){
                                            Navigator.pop(context);

                                          },
                                          child: Container(
                                            height: ScreenSize().getHeight(context)*0.052,
                                            width: double.infinity,

                                            decoration: BoxDecoration(

                                                color: Colors.white,
                                                borderRadius: BorderRadius.only(

                                                    bottomLeft: Radius.circular(12))
                                            ),
                                            child: Center(
                                              child: Text(
                                                S().cancel,
                                                style: GoogleFonts.inter(color:OwnColors.BtnBlue, fontSize: 16.nsp),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        height: ScreenSize().getHeight(context)*0.052,
                                        width: 1,
                                        color: OwnColors.CC8C8C8,
                                      ),
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: (){
                                            Navigator.pop(context);
                                            function();
                                          },
                                          child: Container(

                                            height: ScreenSize().getHeight(context)*0.052,
                                            width: double.infinity,
                                            decoration: BoxDecoration(

                                              borderRadius: BorderRadius.only(

                                                  bottomRight: Radius.circular(12)),
                                              color: Colors.white,
                                            ),
                                            child: Center(
                                              child: Text(
                                                S().confirm,
                                                style: GoogleFonts.inter(color: OwnColors.BtnBlue, fontSize: 16.nsp),
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );


          WillPopScope(
          onWillPop: () => Future.value(false),
          child: CupertinoAlertDialog(
            title: Padding(
              padding: const EdgeInsets.only(bottom: 16.0),
              child: Text(
                title,
                style: TextStyle(fontSize: 18.sp),
              ),
            ),
            content: Text(content),
            actions: <Widget>[
              CupertinoDialogAction(
                  isDefaultAction: true,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(S().cancel)),
              CupertinoDialogAction(
                  isDefaultAction: true,
                  onPressed: () async {
                    Navigator.pop(context);
                    function();
                  },
                  child: Text(S().confirm))
            ],
          ),
        );});
  }




  static void showTextFieldDialog(BuildContext context,
      String title,String content,String hint, TextEditingController textEditingController,Function click) {
    Tools().dismissK(context);
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context)
        {
          return WillPopScope(
            onWillPop: (){
              Tools().dismissK(context);
            },
            child: GestureDetector(
              onTap: (){
                Tools().dismissK(context);
              },
              child: AnimatedContainer(
                padding: EdgeInsets.only(bottom:
                MediaQuery.of(context).viewInsets.bottom == 0?
                0:
                0.2.sh ),
                duration: const Duration(milliseconds: 300),
                child: Center(
                  child: Material(
                    type: MaterialType.transparency,
                    child: Container(
                      height: 1.0.sh,
                      child: Column(
                        children: [
                          Spacer(),
                          Center(
                            child: Container(
                              height: ScreenSize().getHeight(context)*0.18,
                              width: ScreenSize().getWidth(context)*0.63,
                              decoration: BoxDecoration(
                                color: OwnColors.white,
                                borderRadius:
                                BorderRadius.all(Radius.circular(12)),

                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  //HorizontalSpace().create(context, 0.005),
                                  Container(
                                    // constraints: BoxConstraints(
                                    //   minHeight: ScreenSize().getHeight(context)*0.053,
                                    //
                                    // ),

                                    // width: ScreenSize().getWidth(context)*0.6,
                                    child: Center(
                                      child: Padding(
                                        padding: const EdgeInsets.only(left:8.0,right: 8.0, top: 14, bottom: 14),
                                        child: Text(title,textAlign: TextAlign.center,style: GoogleFonts.inter(
                                            fontSize: 16.sp,
                                            fontWeight: FontWeight.bold,
                                            color: OwnColors.black
                                        ),),
                                      ),
                                    ),
                                  ),

                                  Container(
                                    width: ScreenSize().getWidth(context)*0.48,
                                    child: TextField(
                                        controller: textEditingController,
                                        style: TextStyle(
                                            fontSize: 16.nsp,
                                            color: OwnColors.black
                                        ),
                                        onChanged: (S){

                                        },


                                        maxLines: 1,
                                        // maxLength: 4,

                                        decoration: InputDecoration(
                                            hintText: hint,
                                            fillColor: OwnColors.CF7F7F7,
                                            counterStyle: TextStyle(color: Colors.transparent),
                                            contentPadding: EdgeInsets.symmetric(horizontal: 10,vertical:4),

                                            //Change this value to custom as you like
                                            isDense: true,
                                            filled: true,
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                new BorderRadius.circular(8.0),
                                                borderSide: BorderSide.none)


                                        )
                                    ),
                                  ),


                                  HorizontalSpace().create(context, 0.01),


                                  Container(
                                    height: ScreenSize().getHeight(context)*0.052,
                                    decoration: BoxDecoration(
                                      border: Border(
                                        top: BorderSide(width:1.0, color: OwnColors.CC8C8C8),

                                      ),
                                      color: Colors.transparent,
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: GestureDetector(
                                            onTap: (){
                                              Navigator.pop(context);
                                            },
                                            child: Container(
                                              height: ScreenSize().getHeight(context)*0.052,
                                              width: double.infinity,

                                              decoration: BoxDecoration(

                                                color: Colors.white,
                                                  borderRadius: BorderRadius.only(

                                                      bottomLeft: Radius.circular(12))
                                              ),
                                              child: Center(
                                                child: Text(
                                                  S().cancel,
                                                  style: TextStyle(color: OwnColors.C0094FF, fontSize: 16.nsp),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: ScreenSize().getHeight(context)*0.052,
                                          width: 1,
                                            color: OwnColors.CC8C8C8,
                                        ),
                                        Expanded(
                                          child: GestureDetector(
                                            onTap: (){
                                              click();
                                              Navigator.pop(context);
                                            },
                                            child: Container(

                                                height: ScreenSize().getHeight(context)*0.052,
                                                width: double.infinity,
                                                decoration: BoxDecoration(

                                                  borderRadius: BorderRadius.only(

                                                      bottomRight: Radius.circular(12)),
                                                  color: Colors.white,
                                                ),
                                              child: Center(
                                                child: Text(
                                                  S().finish,
                                                  style: TextStyle(color: OwnColors.C0094FF, fontSize: 16.nsp),
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Spacer(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        }
    );
  }

  static void showTextFieldSaveDeleteDialog(BuildContext context,
      String title,String content,String hint, TextEditingController textEditingController,Function del,Function save) {
    Tools().dismissK(context);
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context)
        {
          return WillPopScope(
            onWillPop: (){
              Tools().dismissK(context);
            },
            child: GestureDetector(
              onTap: (){
                Tools().dismissK(context);
              },
              child: AnimatedContainer(
                padding: EdgeInsets.only(bottom:
                MediaQuery.of(context).viewInsets.bottom == 0?
                0:0.2.sh ),
                duration: const Duration(milliseconds: 300),
                child: Center(
                  child: Material(
                    type: MaterialType.transparency,
                    child: Container(
                      height: 1.0.sh,
                      child: Column(
                        children: [
                          Spacer(),
                          Center(
                            child: Container(
                              height: ScreenSize().getHeight(context)*0.18,
                              width: ScreenSize().getWidth(context)*0.63,
                              decoration: BoxDecoration(
                                color: OwnColors.white,
                                borderRadius:
                                BorderRadius.all(Radius.circular(12)),

                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  HorizontalSpace().create(context, 0.005),
                                  Container(
                                    constraints: BoxConstraints(
                                      maxHeight: ScreenSize().getHeight(context)*0.053,

                                    ),

                                    // width: ScreenSize().getWidth(context)*0.6,
                                    child: Center(
                                      child: AutoSizeText(title,style: GoogleFonts.inter(
                                          fontSize: 16.sp,
                                          fontWeight: FontWeight.bold,
                                          color: OwnColors.black
                                      ),),
                                    ),
                                  ),

                                  Container(
                                    width: ScreenSize().getWidth(context)*0.48,
                                    child: TextField(
                                        controller: textEditingController,
                                        style: TextStyle(
                                            fontSize: 16.nsp,
                                            color: OwnColors.black
                                        ),
                                        onChanged: (S){

                                        },


                                        maxLines: 1,
                                        // maxLength: 4,

                                        decoration: InputDecoration(
                                            hintText: hint,
                                            fillColor: OwnColors.CF7F7F7,
                                            counterStyle: TextStyle(color: Colors.transparent),
                                            contentPadding: EdgeInsets.symmetric(horizontal: 10,vertical:4),

                                            //Change this value to custom as you like
                                            isDense: true,
                                            filled: true,
                                            border: OutlineInputBorder(
                                                borderRadius:
                                                new BorderRadius.circular(8.0),
                                                borderSide: BorderSide.none)


                                        )
                                    ),
                                  ),


                                  HorizontalSpace().create(context, 0.01),


                                  Container(
                                    height: ScreenSize().getHeight(context)*0.052,
                                    decoration: BoxDecoration(
                                      border: Border(
                                        top: BorderSide(width:1.0, color: OwnColors.CC8C8C8),

                                      ),
                                      color: Colors.transparent,
                                    ),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: GestureDetector(
                                            onTap: (){
                                              Navigator.pop(context);
                                              del();
                                            },
                                            child: Container(
                                              height: ScreenSize().getHeight(context)*0.052,
                                              width: double.infinity,

                                              decoration: BoxDecoration(

                                                  color: Colors.white,
                                                  borderRadius: BorderRadius.only(

                                                      bottomLeft: Radius.circular(12))
                                              ),
                                              child: Center(
                                                child: Text(
                                                  S().delete,
                                                  style: GoogleFonts.inter(color: OwnColors.CFF644E, fontSize: 16.nsp),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Container(
                                          height: ScreenSize().getHeight(context)*0.052,
                                          width: 1,
                                          color: OwnColors.CC8C8C8,
                                        ),
                                        Expanded(
                                          child: GestureDetector(
                                            onTap: (){
                                              save();
                                              Navigator.pop(context);
                                            },
                                            child: Container(

                                              height: ScreenSize().getHeight(context)*0.052,
                                              width: double.infinity,
                                              decoration: BoxDecoration(

                                                borderRadius: BorderRadius.only(

                                                    bottomRight: Radius.circular(12)),
                                                color: Colors.white,
                                              ),
                                              child: Center(
                                                child: Text(
                                                  S().save,
                                                  style: GoogleFonts.inter(color: OwnColors.C0094FF, fontSize: 16.nsp,fontWeight: FontWeight.w400),
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Spacer(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        }
    );
  }


  static void showTextFieldInviteCodeDialog(BuildContext context,
      String title,String content,String hint, TextEditingController textEditingController,String inviteCode,setState,Function sendAuth) {
    Tools().dismissK(context);
    bool showInfo = false;
    final tooltipController = JustTheController();
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context)
        {
          return GestureDetector(
            onTap: (){
              Tools().dismissK(context);
            },
            child: WillPopScope(
              onWillPop: (){
                Tools().dismissK(context);
              },
              child: AnimatedContainer(
                padding: EdgeInsets.only(bottom:
                MediaQuery.of(context).viewInsets.bottom == 0?
                0:0.2.sh),
                duration: const Duration(milliseconds: 300),
                child: Material(
                  type: MaterialType.transparency,
                  child: Center(
                    child: Container(
                      height: 1.0.sh,
                      child: Column(
                        children: [
                          Spacer(),
                          Container(
                            height: ScreenSize().getWidth(context)*0.33+20,
                            width: ScreenSize().getWidth(context)*0.628,
                            decoration: BoxDecoration(
                              color: OwnColors.white,
                              borderRadius:
                              BorderRadius.all(Radius.circular(12)),

                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                //HorizontalSpace().create(context, 0.005),
                                Container(height: 5,),
                                Container(
                                  constraints: BoxConstraints(
                                    maxHeight: ScreenSize().getHeight(context)*0.053,

                                  ),

                                  // width: ScreenSize().getWidth(context)*0.6,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Opacity(
                                        opacity: 0.0,
                                        child: JustTheTooltip(

                                          controller: tooltipController,
                                          borderRadius: BorderRadius.all(Radius.circular(11)),
                                          backgroundColor: OwnColors.CEEEEEE,

                                          onDismiss: (){
                                            // showInfo = false;
                                          },
                                          onShow: (){
                                            // showInfo = true;
                                          },
                                          isModal: true,
                                          preferredDirection: AxisDirection.up,
                                          elevation: 0,


                                          child: GestureDetector(
                                            child:Padding(
                                                padding: const EdgeInsets.only(left: 0, top: 2, bottom: 1.0),
                                                child: Icon(SFSymbols.question_circle_fill, color: OwnColors.CTourOrange,size: 20,)
                                            ),
                                            onTap: () {
                                              setState(() {
                                                // if(showInfo){
                                                //   tooltipController.hideTooltip();
                                                // }else {
                                                //   tooltipController.showTooltip();
                                                // }


                                              });
                                            },
                                          ),
                                          content: Padding(
                                              padding: EdgeInsets.all(8.0),
                                              child: Container(
                                                width: 0.57.sw,

                                                child: Text(
                                                  S().guilder_add_comment_invite_tool_hint
                                                  ,style:
                                                GoogleFonts.inter(fontSize: 14.nsp,color: OwnColors.black, fontWeight: FontWeight.w300

                                                ),),
                                              )
                                          ),

                                        ),
                                      ),
                                      Flexible(
                                        child: Text(title,
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.inter(
                                            fontSize: 16.nsp,
                                            fontWeight: FontWeight.bold,
                                            color: OwnColors.black,

                                        ),),
                                      ),
                                      Container(width: 5,),
                                      JustTheTooltip(

                                        controller: tooltipController,
                                        borderRadius: BorderRadius.all(Radius.circular(11)),
                                        backgroundColor: OwnColors.CEEEEEE,

                                        onDismiss: (){
                                          // showInfo = false;
                                        },
                                        onShow: (){
                                          // showInfo = true;
                                        },
                                        isModal: true,
                                        preferredDirection: AxisDirection.up,
                                        elevation: 0,


                                        child: GestureDetector(
                                          child:Padding(
                                              padding: const EdgeInsets.only(left: 0, top: 2, bottom: 1.0),
                                              child: Icon(SFSymbols.question_circle_fill, color: OwnColors.CTourOrange,size: 20,)
                                          ),
                                          onTap: () {
                                            setState(() {
                                              if(showInfo){
                                                tooltipController.hideTooltip();
                                              }else {
                                                tooltipController.showTooltip();
                                              }


                                            });
                                          },
                                        ),
                                        content: Padding(
                                            padding: EdgeInsets.all(8.0),
                                            child: Container(
                                              width: 0.57.sw,

                                              child: Text(
                                                S().guilder_add_comment_invite_tool_hint
                                                ,style:
                                              GoogleFonts.inter(fontSize: 14.nsp,color: OwnColors.black, fontWeight: FontWeight.w300

                                              ),),
                                            )
                                        ),

                                      ),
                                    ],
                                  ),
                                ),
                                Container(height: 5,),

                                Container(
                                  width: ScreenSize().getWidth(context)*0.46,
                                  child: TextField(
                                      controller: textEditingController,
                                      keyboardType: TextInputType.phone,
                                      style: GoogleFonts.inter(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14.nsp,
                                          color: OwnColors.black
                                      ),
                                      onChanged: (S){

                                      },


                                      maxLines: 1,
                                      // maxLength: 4,

                                      decoration: InputDecoration(
                                        hintStyle:           GoogleFonts.inter(
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14.nsp,
                                          color: OwnColors.CC8C8C8
                                      ),
                                          hintText: hint,
                                          fillColor: OwnColors.CF7F7F7,
                                          counterStyle: TextStyle(color: Colors.transparent),
                                          contentPadding: EdgeInsets.symmetric(horizontal: 10,vertical:4),

                                          //Change this value to custom as you like
                                          isDense: true,
                                          filled: true,
                                          border: OutlineInputBorder(
                                              borderRadius:
                                              new BorderRadius.circular(8.0),
                                              borderSide: BorderSide.none)


                                      )
                                  ),
                                ),


                                HorizontalSpace().create(context, 0.01),


                                Container(
                                  height: ScreenSize().getHeight(context)*0.052,
                                  decoration: BoxDecoration(
                                    border: Border(
                                      top: BorderSide(width:1.0, color: OwnColors.CC8C8C8),

                                    ),
                                    color: Colors.transparent,
                                  ),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: (){
                                            Navigator.pop(context);
                                            Navigator.pop(context);
                                          },
                                          child: Container(
                                            height: ScreenSize().getHeight(context)*0.052,
                                            width: double.infinity,

                                            decoration: BoxDecoration(

                                                color: Colors.white,
                                                borderRadius: BorderRadius.only(

                                                    bottomLeft: Radius.circular(12))
                                            ),
                                            child: Center(
                                              child: Text(
                                                S().cancel,
                                                style: TextStyle(color: OwnColors.C0094FF, fontSize: 16.nsp),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Container(
                                        height: ScreenSize().getHeight(context)*0.052,
                                        width: 1,
                                        color: OwnColors.CC8C8C8,
                                      ),
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: (){
                                            sendAuth();
                                            // if(inviteCode == textEditingController.text){
                                            //   Navigator.pop(context);
                                            // }else {
                                            //
                                            // }

                                          },
                                          child: Container(

                                            height: ScreenSize().getHeight(context)*0.052,
                                            width: double.infinity,
                                            decoration: BoxDecoration(

                                              borderRadius: BorderRadius.only(

                                                  bottomRight: Radius.circular(12)),
                                              color: Colors.white,
                                            ),
                                            child: Center(
                                              child: Text(
                                                S().finish,

                                                style: TextStyle(color: OwnColors.C0094FF, fontSize: 16.nsp),
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        }
    );
  }

}
