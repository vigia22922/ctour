import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';

class Tools {

  void dismissK(context) {
    print("dismissK");
    FocusScope.of(context).unfocus();
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  bool stringNotNullOrSpace(String i ) {
    return i.isNotEmpty && i.trim() != "";
  }

  String getLocal( ) {
    final List<Locale> systemLocales = window.locales;
    String local =  systemLocales.first.languageCode;// Platform.localeName;

    if(local.toLowerCase().contains("en")){
      local = "en";
    }else if(local.toLowerCase().contains("zh")){
      local = "zh";
    }
    print("local " + local);
    return local;
  }
}