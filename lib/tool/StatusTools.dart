import 'package:ctour/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:status_bar_control/status_bar_control.dart';

class StatusTools {


  Future<void> setWhitBGDarkText() async {
    await StatusBarControl.setColor(Colors.white, animated:false);
    await StatusBarControl.setStyle(StatusBarStyle.DARK_CONTENT);
  }
  Future<void> setTranBGDarkText() async {
    await StatusBarControl.setColor(Colors.transparent, animated:false);
    await StatusBarControl.setStyle(StatusBarStyle.DARK_CONTENT);
  }
  Future<void> setTranBGWhiteText() async {
    await StatusBarControl.setColor(Colors.transparent, animated:false);
    await StatusBarControl.setStyle(StatusBarStyle.LIGHT_CONTENT);
  }

}