import 'package:ctour/generated/l10n.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

class FilterData{
  static int travel_sort_type = 0;
  static int travel_date_choose = 0;
  static String travel_start_date = "";
  static String travel_end_date = "";
  static int travel_type = 0;
  static bool travel_isDay = false;
  static double travel_end_duration = -1;
  static double travel_start_duration = 0.0;
  static SfRangeValues travel_duration =  SfRangeValues(0.0, 30.0);


  static num guild_rating = 5;
  static int guild_want = 0;
  static double guild_startNum = 0, guild_endNum = -1;
  static bool guild_trans = false;
  static List<bool> guild_transSelList = [false,false,false];
  static String guild_gender = S().guild_gender1;
  static List<bool> guild_lansSelList = [false,false,false,false,false,false,false];
  static List<String> lansList = ["中文","English","日本語","한국어","Français","Deutsch","Bahasa Indonesia"];
}