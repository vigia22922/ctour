import 'package:flutter/material.dart';

class OwnColors {
  static const Color colorPrimary = Color(0xff4caf50);
  static const Color colorPrimaryDark = Color(0xff388E3C);
  static const Color colorAccent = Color(0xff8BC34A);
  static const Color colorPrimaryLight = Color(0xffC8E6C9);

  static const Color primaryText = Color(0xff212121);
  static const Color secondaryText = Color(0xff757575);


  static const Color deepBlue = Color(0xff001942);
  static const Color deepPink = Color(0xffBF6766);

  static const Color skyBlue = Color(0xffB9F7FF);
  static const Color blue = Color(0xff123cdb);

  static const Color brown = Color(0xff64363C);

  static const Color white = Color(0xffffffff);

  static const Color tran = Colors.transparent;

  static const Color inkColor = Color(0x26000000);

  static const Color green = Color(0xff8CEA00);
  static const Color yellow = Color(0xffFFDC35);

  static const Color black = Color(0xff000000);



  static const Color TextBLue = Color(0xff1A1F63);
  static const Color LogoOrange = Color(0xffEAA622);








  static const Color  purple_200= Color(0xFFBB86FC);
  static const Color  purple_500= Color(0xFF6200EE);
  static const Color  purple_700= Color(0xFF3700B3);
  static const Color  teal_200= Color(0xFF03DAC5);
  static const Color  teal_700= Color(0xFF018786);

  static const Color  TextPurple= Color(0xffAC70FF);
  static const Color  Textgray= Color(0xff828282);
  static const Color  TextRed= Color(0xffFF3B30);

  static const Color  DividerGray= Color(0xffdfe1e7);
  static const Color  BtnBlue= Color(0xff0094FF);
  static const Color  BtnGreen= Color(0xff34C759);
  static const Color  TextBlue= Color(0xff007AFF);
  static const Color  PressBtnBlue= Color(0xff1883BA);
  static const Color  Peach= Color(0xffFF70CE);
  static const Color  transparent= Color(0x00FFFFFF);

  static const Color  Black30= Color(0x4D000000);
  static const Color  BGGray= Color(0xffeff1f3);


  static const Color  LightRed= Color(0xffeb5757);
  static const Color  LightOrange= Color(0xfff2994a);
  static const Color  LightYellow= Color(0xfff2c94c);
  static const Color  LightGreen= Color(0xff27ae60);
  static const Color  LightBlue= Color(0xff2f80ed);
  static const Color  LightSky= Color(0xff56ccf2);
  static const Color  LightPurple= Color(0xff9b51e0);





  static const Color  IronBlue= Color(0xff7789b5);


  static const Color  CTourOrange= Color(0xffFF644E);
  static const Color  CFF644E= Color(0xffFF644E);
  static const Color  CFFAE4E= Color(0xffFFAE4E);
  static const Color  Gray= Color(0xffC8C8C8);
  static const Color ironGray = Color(0xff989898);
  static const Color C7A7A7A_a36= Color(0x5c7a7a7a);
  static const Color white_a70= Color(0xb3ffffff);
  static const Color C989898= Color(0xff989898);
  static const Color CC8C8C8= Color(0xffC8C8C8);
  static const Color CE6E1E1= Color(0xffE6E1E1);
  static const Color CF2F2F2 =  Color(0xffF2F2F2);
  static const Color C797979 =  Color(0xff797979);
  static const Color C787878 =  Color(0xff787878);
  static const Color C3E3E3E =  Color(0xff3E3E3E);
  static const Color CCCCCCC =  Color(0xffCCCCCC);
  static const Color CF4F4F4 =  Color(0xffF4F4F4);
  static const Color C888888 =  Color(0xff888888);
  static const Color CF7F7F7 =  Color(0xffF7F7F7);
  static const Color CEEEEEE =  Color(0xffEEEEEE);
  static const Color CC4C4C4 =  Color(0xffC4C4C4);
  static const Color C0094FF =  Color(0xff0094FF);
  static const Color C00B900 =  Color(0xff00B900);

  static const Color black_a4 = Color(0xa000000);
  static const Color black_a38 = Color(0x61000000);
  static const Color black_a87 = Color(0xde000000);

  static const Color CE75735 = Color(0xffE75735);
  static const Color CFE3040 = Color(0xffFE3040);

}