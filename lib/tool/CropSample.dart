// import 'dart:io';
// import 'dart:typed_data';
//
// import 'package:crop_your_image/crop_your_image.dart';
// import 'package:ctour/generated/l10n.dart';
// import 'package:ctour/tool/Loading.dart';
// import 'package:ctour/tool/StatusTools.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_spinkit/flutter_spinkit.dart';
// import 'package:ctour/tool/OwnColors.dart';
//
// class CropSample extends StatefulWidget {
//
//   final Uint8List origImg;
//   final double ratio;
//   final bool circle;
//
//
//
//   const CropSample(
//       {Key key,
//         this.origImg,
//         this.ratio,
//         this.circle,
//
//
//
//       })
//       : super(key: key);
//
//   @override
//   _CropSampleState createState() => _CropSampleState();
// }
//
// class _CropSampleState extends State<CropSample> {
//   final _controller = CropController();
//
//   var _isProcessing = false;
//   set isProcessing(bool value) {
//     setState(() {
//       _isProcessing = value;
//     });
//   }
//
//   Uint8List _croppedData;
//   Uint8List _origData;
//   set croppedData(Uint8List value) {
//     setState(() {
//       _croppedData = value;
//     });
//   }
//
//   bool _isPreviewing = false;
//   set isPreviewing(bool value) {
//     setState(() {
//       _isPreviewing = value;
//     });
//   }
//
//   @override
//   void initState()  {
//     super.initState();
//     StatusTools().setTranBGWhiteText();
//     print( widget.origImg );
//     _origData = widget.origImg ;
//
//     // final ByteData bytes = await rootBundle.load('assets/logo.jpg');
//     // final Uint8List list = bytes.buffer.asUint8List();
//
//     //
//     // _controller.withCircleUi = false;
//     // _controller.aspectRatio = shapeData.aspectRatio;
//   }
//
//
//   @override
//   Widget build(BuildContext context) {
//
//     return Scaffold(
//       backgroundColor: OwnColors.black,
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//
//         actions: [
//           if (_croppedData != null)
//             IconButton(
//               icon: Icon(Icons.redo),
//               onPressed: () => croppedData = null,
//             ),
//         ],
//         iconTheme: IconThemeData(
//           color: Colors.white,
//         ),
//
//       ),
//       body: SafeArea(
//         child: Visibility(
//           visible: _origData.length > 2 && !_isProcessing,
//           child: _origData.length > 2
//               ? Visibility(
//             visible: _croppedData == null,
//             child: Column(
//               children: [
//                 Expanded(
//                   child: Stack(
//                     children: [
//                       Crop(
//                         baseColor: OwnColors.black,
//                         controller: _controller,
//                         image: _origData ,
//                         onCropped: (cropped) {
//                           croppedData = cropped;
//                           isProcessing = false;
//                           Loading.dismiss(context);
//                           Navigator.pop(context,_croppedData);
//                         },
//                         initialSize: 1,
//                         withCircleUi: widget.circle,
//                         aspectRatio: widget.ratio,
//                         interactive: false,
//                         cornerDotBuilder: (size, cornerIndex) {
//                           return _isPreviewing
//                               ? const SizedBox.shrink()
//                               : const DotControl();
//                         },
//                         maskColor: _isPreviewing ? Colors.black : null,
//                       ),
//                       Positioned(
//                         bottom: 16,
//                         left: 16,
//                         child: InkWell(
//                           splashColor: OwnColors.tran,
//                           onTap: (){
//                             _isPreviewing = ! _isPreviewing;
//                             setState(() {
//
//                             });
//                           },
//                           child: Padding(
//                             padding: const EdgeInsets.symmetric(
//                                 horizontal: 16, vertical: 8),
//                             child: Text(
//                               S().preview,
//                               style: TextStyle(color: _isPreviewing? Colors.blueAccent : Colors.white, fontWeight: FontWeight.bold),
//                             ),
//                           ),
//                         ),
//                       ),
//
//                       Positioned(
//                         bottom: 16,
//                         right: 16,
//                         child: InkWell(
//                           splashColor: OwnColors.tran,
//                           onTap: (){
//                             Loading.show(context);
//                             if(widget.circle){
//                               _controller.cropCircle();
//                             }else {
//                               _controller.crop();
//                             }
//                           },
//                           child: Padding(
//                             padding: const EdgeInsets.symmetric(
//                                 horizontal: 16, vertical: 8),
//                             child: Text(
//                               S().finish,
//                               style: TextStyle(color: _isPreviewing? Colors.blueAccent : Colors.white, fontWeight: FontWeight.bold),
//                             ),
//                           ),
//                         ),
//                       )
//                     ],
//                   ),
//                 ),
//                 // Padding(
//                 //   padding: const EdgeInsets.all(16),
//                 //   child: Wrap(
//                 //     spacing: 16,
//                 //     children: const [
//                 //       _ShapeSelection('16 : 9', 16 / 9, Icons.crop_7_5),
//                 //       _ShapeSelection('8 : 5', 8 / 5, Icons.crop_16_9),
//                 //       _ShapeSelection('7 : 5', 7 / 5, Icons.crop_5_4),
//                 //       _ShapeSelection('4 : 3', 4 / 3, Icons.crop_3_2),
//                 //     ].map((shapeData) {
//                 //       return InkWell(
//                 //         onTap: () {
//                 //           _controller.withCircleUi = false;
//                 //           _controller.aspectRatio = shapeData.aspectRatio;
//                 //         },
//                 //         child: Padding(
//                 //           padding: const EdgeInsets.all(8),
//                 //           child: Column(
//                 //             children: [
//                 //               Icon(shapeData.icon),
//                 //               const SizedBox(height: 4),
//                 //               Text(shapeData.label)
//                 //             ],
//                 //           ),
//                 //         ),
//                 //       );
//                 //     }).toList()
//                 //       ..add(
//                 //         InkWell(
//                 //           onTap: () => _controller.withCircleUi = true,
//                 //           child: Padding(
//                 //             padding: const EdgeInsets.all(8),
//                 //             child: Column(
//                 //               children: [
//                 //                 Icon(Icons.fiber_manual_record_outlined),
//                 //                 const SizedBox(height: 4),
//                 //                 Text('Circle'),
//                 //               ],
//                 //             ),
//                 //           ),
//                 //         ),
//                 //       ),
//                 //   ),
//                 // ),
//
//               ],
//             ),
//             // replacement: _croppedData != null
//             //     ? SizedBox(
//             //   height: double.infinity,
//             //   width: double.infinity,
//             //
//             //   child: Image.memory(
//             //     _croppedData,
//             //     fit: BoxFit.contain,
//             //   ),
//             // )
//             //     : const SizedBox.shrink(),
//           )
//               : const SizedBox.shrink(),
//           replacement: Center(child:Container(
//             decoration: BoxDecoration(
//               color: OwnColors.tran.withOpacity(0.0),
//               borderRadius: BorderRadius.circular(5),
//             ),
//             width: 75,
//             height: 75,
//             alignment: Alignment.center,
//             child: Column(
//               mainAxisSize: MainAxisSize.min,
//               children: <Widget>[
//                 SpinKitRing(
//                   lineWidth: 5,
//                   color: OwnColors.CTourOrange,
//                   size: 60.0,
//                 )
//               ],
//             ),
//           )),
//         ),
//       ),
//     );
//   }
// }
