import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
class ScreenSize{

  double getHeight(BuildContext context){
    // return MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    return MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top - MediaQuery.of(context).padding.bottom;
  }
  double getHeightWTop(BuildContext context){
    // return MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    return MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top  ;
  }

  double getWidth(BuildContext context){
    // return MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    return MediaQuery.of(context).size.width - MediaQuery.of(context).padding.left - MediaQuery.of(context).padding.right;
  }

  double getBottomPad(BuildContext context){
    // return MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    return  MediaQuery.of(context).padding.bottom;
  }
  double getTopPad(BuildContext context){
    // return MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    return  MediaQuery.of(context).padding.top;
  }
  Widget createBottomPad(BuildContext context){
    // return MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    return
      Container(height:MediaQuery.of(context).padding.bottom);
  }
  Widget createTopPad(BuildContext context){
    // return MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top;
    return
      Container(height:MediaQuery.of(context).padding.top);
  }
}