import 'dart:async';

import 'package:ctour/generated/l10n.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class MyPercentageProgress extends StatefulWidget {
  final Stream<double> stream;
  String title;
  double total;
  double uploaded;
  Function refreshParent;
  MyPercentageProgress(this.stream,this.title,this.total,this.uploaded,this.refreshParent);
  @override
  _MyPercentageProgress createState() => _MyPercentageProgress();
}

class _MyPercentageProgress extends State<MyPercentageProgress> {
  double progress = 0.0;

  StreamSubscription<double> subscribe;

  @override
  void initState() {
    super.initState();
    subscribe = widget.stream.listen((uploaded) {
      setState(() {
        print("uploaded " + uploaded.toString());
        if (uploaded >= widget.total){
          progress = 1;
        }else {
          progress = uploaded / widget.total;
        }
        widget.uploaded = uploaded;
      });
    });
  }

  @override
  void dispose() {
    subscribe.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context){
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: CupertinoAlertDialog(
        title: Padding(
          padding: const EdgeInsets.only(bottom: 16.0),
          child: Text(
            widget.uploaded.round() != widget.total? widget.title : "上傳成功",
            style: TextStyle(fontSize: 18.sp),
          ),
        ),
        content: Container(
          child:
          new CircularPercentIndicator(
            radius: 100.0,
            lineWidth: 10.0,
            percent: progress,

            center: Text(
              (progress*100).round().toString() + "%",
              style: TextStyle(fontSize: 18.sp),
            ),
            footer :
            Text(
              widget.uploaded.round().toString() + "/" + widget.total.round().toString(),
              style: TextStyle(fontSize: 18.sp),
            ),

            backgroundColor: Colors.grey,
            progressColor: Colors.blue,
          ),
        ),
        actions: <Widget>[
          CupertinoDialogAction(
              isDefaultAction: true,
              onPressed: () {
                Navigator.pop(context);
                widget.refreshParent();
              },
              child: Text(
                  widget.uploaded.round() != widget.total? S().cancel : S().confirm,
                  )),
        ],
      ),
    );

  }

}