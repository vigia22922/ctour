import 'dart:convert';
import 'dart:typed_data';

import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:intl/intl.dart';
import 'package:status_bar_control/status_bar_control.dart';

class CropTool {


  Future<CroppedFile > crop(double x, double y,CropAspectRatioPreset ratio ,bool circle, String path ) async {
    CroppedFile  croppedFile = await ImageCropper().cropImage(
      sourcePath: path,
      aspectRatio:  CropAspectRatio(ratioX: x, ratioY: y),
      cropStyle: circle ? CropStyle.circle:CropStyle.rectangle,
      aspectRatioPresets: [

        ratio,
      ],
      uiSettings: [
        AndroidUiSettings(
            toolbarTitle: '',
            toolbarColor: OwnColors.CTourOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: true),
        IOSUiSettings(
            title: '',
            aspectRatioLockEnabled: true,
            resetAspectRatioEnabled : false
        )],


    );



    return croppedFile;
  }


}