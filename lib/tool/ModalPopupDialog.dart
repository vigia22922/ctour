
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'OwnColors.dart';

class ModalPopupDialog{

  static Future<dynamic> showIOSDialog(BuildContext context,String content,   String btnText) async {


      var result = await showCupertinoModalPopup(
          context: context,
          builder: (context) {
            return CupertinoActionSheet(
              // title: Text('提示'),
              message: Text(content),
              actions: <Widget>[
                CupertinoActionSheetAction(
                  child: Text(btnText,style: TextStyle(
                    fontWeight: FontWeight.normal
                  ),),
                  onPressed: () {
                    Navigator.of(context).pop('delete');
                  },
                  isDefaultAction: true,
                ),
              ],
              cancelButton: CupertinoActionSheetAction(
                child: Text(S().cancel,style: TextStyle(
                  fontWeight: FontWeight.bold
                ),),
                onPressed: () {
                  Navigator.of(context).pop('cancel');
                },
              ),
            );
          });

      return result;



  }






  // static Future<dynamic> showIOSDatePickerDialog(BuildContext context) async {
  //
  //   String title =S().coursesAddCalendar;
  //   DateTime  d =DateTime.now();
  //   var result = await showCupertinoModalPopup(
  //       context: context,
  //       builder: (context) {
  //         return CupertinoActionSheet(
  //           // title: Text('提示'),
  //           message: Container(
  //             height: 300,
  //             child: CupertinoDatePicker(
  //                 mode: CupertinoDatePickerMode.dateAndTime,
  //                 onDateTimeChanged: (dateTime) {
  //                   d = dateTime;
  //                 }
  //             ),
  //           ),
  //           actions: <Widget>[
  //             CupertinoActionSheetAction(
  //               child: Text(title,style: TextStyle(
  //                   fontWeight: FontWeight.normal
  //               ),),
  //               onPressed: () {
  //
  //
  //                 Navigator.of(context).pop( DateFormat('yyyy-MM-dd HH:mm:ss').format((d)).toString());
  //               },
  //               isDefaultAction: true,
  //             ),
  //           ],
  //           cancelButton: CupertinoActionSheetAction(
  //             child: Text(S().settingDeviceSetNameCancel,style: TextStyle(
  //                 fontWeight: FontWeight.bold
  //             ),),
  //             onPressed: () {
  //               Navigator.of(context).pop('cancel');
  //             },
  //           ),
  //         );
  //       });
  //
  //   return result;
  //
  //
  //
  // }
  // static Future<dynamic> showIOSTimePickerDialog(BuildContext context) async {
  //
  //   String title =S().ok;
  //   DateTime  d = DateTime.now();
  //   String dS= DateFormat('HH:mm').format((d))   ;
  //
  //   var result = await showCupertinoModalPopup(
  //       context: context,
  //       builder: (context) {
  //         return CupertinoActionSheet(
  //           // title: Text('提示'),
  //           message: Container(
  //             height: 300,
  //             child: CupertinoDatePicker(
  //                 mode: CupertinoDatePickerMode.time,
  //                 onDateTimeChanged: (dateTime) {
  //                   d = dateTime;
  //                   dS= DateFormat('HH:mm').format((d))   ;
  //                 }
  //             ),
  //           ),
  //           actions: <Widget>[
  //             CupertinoActionSheetAction(
  //               child: Text(title,style: TextStyle(
  //                   fontWeight: FontWeight.normal
  //               ),),
  //               onPressed: () {
  //                 Navigator.of(context).pop(dS);
  //               },
  //               isDefaultAction: true,
  //             ),
  //           ],
  //           cancelButton: CupertinoActionSheetAction(
  //             child: Text(S().settingDeviceSetNameCancel,style: TextStyle(
  //                 fontWeight: FontWeight.bold
  //             ),),
  //             onPressed: () {
  //               Navigator.of(context).pop('cancel');
  //             },
  //           ),
  //         );
  //       });
  //
  //   return result;
  //
  //
  //
  // }



  static Future<dynamic> showPicker(BuildContext context,List<int> itemList) async {

    int item =itemList[0];
    int index  =0;

    List<Widget> itemText = new List();
    for (int i in itemList){
      itemText.add(Center(
        child: Text(

          i.toString(),textScaleFactor: 1.0,style: TextStyle(fontSize: 24.sp
        )
        ),
      ));
    }
    var result = await showCupertinoModalPopup(

        context: context,
        builder: (context) {
          return CupertinoActionSheet(
            // title: Text('提示'),
            message: Container(
              height: 100,
              child: CupertinoPicker(

                onSelectedItemChanged: (value) {
                  item = itemList[value];
                  index = value;
                },
                itemExtent: 50.0,
                children: itemText
              ),
            ),
            actions: <Widget>[
              CupertinoActionSheetAction(
                child: Text(S().confirm,style: TextStyle(
                    fontWeight: FontWeight.normal
                ),),
                onPressed: () {
                  Navigator.of(context).pop(index.toString()+"," + item.toString());
                },
                isDefaultAction: true,
              ),
            ],
            cancelButton: CupertinoActionSheetAction(
              child: Text(S().cancel,style: TextStyle(
                  fontWeight: FontWeight.bold
              ),),
              onPressed: () {
                Navigator.of(context).pop("cancel");
              },
            ),
          );
        });

    return result;



  }






}