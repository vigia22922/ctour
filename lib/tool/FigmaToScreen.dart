import 'package:flutter/cupertino.dart';

class FigmaToScreen{
  double figmaScreenWidth = 390;
  double getWidthRatio(double figmaWidth){
    double ratio = figmaWidth/figmaScreenWidth;
    return ratio;
  }
  double getHeightRatio(BuildContext context,double figmaWidth,double figmaHeight){
    //test null
    figmaWidth ??= figmaScreenWidth;
    double widthRatio = figmaHeight/figmaWidth*figmaWidth/figmaScreenWidth;
    double heightRatio = widthRatio * MediaQuery.of(context).size.width / MediaQuery.of(context).size.height;
    return heightRatio;
  }
  double getPixelWidth(BuildContext context,double figmaWidth){
    return MediaQuery.of(context).size.width * getWidthRatio(figmaWidth);
  }
  double getPixelHeight(BuildContext context,double figmaHeight){
    return MediaQuery.of(context).size.height * getHeightRatio(context, null, figmaHeight);
  }
}