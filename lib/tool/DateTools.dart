import 'package:ctour/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateTools {

  DateTime findFirstDateOfTheWeek(DateTime dateTime) {
    return dateTime.subtract(Duration(days: dateTime.weekday - 1));
  }
  DateTime findLastDateOfTheWeek(DateTime dateTime) {
    return dateTime.add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  }

  DateTime findLastDateByDateTime(DateTime dateTime, int num) {
    return dateTime.add(Duration(days: num));
  }

  String getWeekName(DateTime dateTime) {
    return DateFormat('EEEE').format(dateTime);
  }

  bool checkSameWeek(DateTime dateTime) {
    print("checkSameWeek " + dateTime.toString());
    DateTime first = findFirstDateOfTheWeek(DateTime.now());
    first = DateTime(first.year,first.month,first.day);
    DateTime last = findLastDateOfTheWeek(DateTime.now());
    last = DateTime(last.year,last.month,last.day);
    print("checkSameWeek first" + first.toString());
    print("checkSameWeek last" + last.toString());
    if((dateTime.isBefore(last) || dateTime == last) &&( dateTime.isAfter(first) || dateTime == first)){
      return true;
    }else {
      return false;
    }

  }

  String intlYMD(String datetime) {
    String result = DateFormat('yyyy' + S().year + 'MM' +S().month + 'dd'+S().day).format(DateTime.parse(datetime));
    return result ;
   
  

  }

  String dateLineFormate(DateTime d){
    if(DateTime.now().difference(d).inDays<1){
      return DateFormat('hh:mm a').format((d));
    }else if(DateTime.now().difference(d).inDays<2){
      return S().one_day_ago;
    }else if(DateTime.now().difference(d).inDays<3){
      return S().two_day_ago;
    }else if(DateTime.now().difference(d).inDays<4){
      return S().three_day_ago;
    }else {
      return d.year.toString() + S().year + d.month.toString() + S().month + d.day.toString() + S().day;
    }
  }

  String dateLineFormate2(DateTime d){
    if(DateTime.now().difference(d).inDays<1){
      return DateFormat('a hh:mm').format((d));
    }else if(DateTime.now().difference(d).inDays<2){
      return S().one_day_ago;
    }else if(DateTime.now().difference(d).inDays<3){
      return S().two_day_ago;
    }else if(DateTime.now().difference(d).inDays<4){
      return S().three_day_ago;
    }else {
      return d.year.toString() + S().year + d.month.toString() + S().month + d.day.toString() + S().day;
    }
  }
}