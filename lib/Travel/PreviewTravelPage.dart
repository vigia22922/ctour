import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Guild/GuildReviewPage.dart';
import 'package:ctour/Guild/GuilderTravelPage.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/Travel/TravelCommentClass.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/StatusTools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icons.dart';
import 'package:selectable_autolink_text/selectable_autolink_text.dart';
import 'package:sliver_tools/sliver_tools.dart';
import 'package:url_launcher/url_launcher.dart';
import '../Network/API/app_api.dart';
import 'TravelClass.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/TopBar.dart';
import '../res.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_star/flutter_star.dart';

class PreviewTravelPage extends StatefulWidget {

  final TravelClass travelClass;

  const PreviewTravelPage(
      {Key key,
        this.travelClass,


      })
      : super(key: key);

  @override
  State createState() {
    return _PreviewTravelPageState();
  }
}

class _PreviewTravelPageState extends State<PreviewTravelPage>with TickerProviderStateMixin {



  bool more  = false;
  TabController _tabFirstController;
  int currentFirstTab = 0;
  String imgBase64 = "";
  String imgExt = "";
  String imgPath = "";
  String imgUrl = "";
  TextEditingController commentEdit = new TextEditingController();

  List<TravelCommentClass> commentList = new List();
  double topShirkOffset = 0;
  double maxTop = 0.0;

  GlobalKey tabKey = new  GlobalKey();
  List<double> posList = [];
  List<int> scrollToList = [];
  GlobalKey titleKey = new  GlobalKey();
  bool showTitle = false ;

  @override
  void initState() {
    super.initState();
    StatusTools().setTranBGWhiteText();
    print (widget.travelClass.contentList.toString());


    _tabFirstController = new TabController(vsync: this, length: widget.travelClass.sectionTag.length);
    //
    // commentList.add(new TravelCommentClass(name: "WWW",avatar: "https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg",
    // img: "https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg",content:"非常完美的在地達人，跟著他一起玩絕對Happy。推推推！！",update_time: "2022年5月10日",is_liked: "1",liked_num: "132"));
    // commentList.add(new TravelCommentClass(name: "WWW",avatar: "https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg",
    //     img: "https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg",content:"非常完美的在地達人，跟著他一起玩絕對Happy。推推推",update_time: "2022年5月10日",is_liked: "0",liked_num: "132"));
    // commentList.add(new TravelCommentClass(name: "WWW",avatar: "https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg",
    //     img: "https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg",content:"非常完美的在地達人，跟著他一起玩絕對Happy。推推推",update_time: "2022年5月10日",is_liked: "0",liked_num: "132"));

    // print("tag " + widget.travelClass.tag.toString());



  }

  @override
  void dispose() {
    super.dispose();
    StatusTools().setWhitBGDarkText();
  }
   @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.white,
        extendBodyBehindAppBar: true,
        body: GestureDetector(
            child: Stack(
          children: [
            Container(
              width: ScreenSize().getWidth(context) * 1.0,
              color: OwnColors.white,
              height: MediaQuery.of(context).size.height,
              child: Container(
                width: ScreenSize().getWidth(context) * 1.0,
                height: MediaQuery.of(context).size.height,
                color: Colors.white,
                child:
                NotificationListener<ScrollUpdateNotification>(
                  onNotification: (notification) {
                    //How many pixels scrolled from pervious frame
                    // print("position " + notification.scrollDelta.toString());

                    //List scroll position
                    // print("position " + notification.metrics.pixels.toString());
                    // final targetContext = widget.travelClass.contentList[currentFirstTab].key.currentContext;

                    RenderBox box2 = tabKey.currentContext.findRenderObject() as RenderBox;
                    double topH = 0.07 * ScreenSize().getHeight(context) + box2.size.height + MediaQuery.of(context).padding.top;

                    posList = [];
                    scrollToList = [];
                    for (int ii = 0 ; ii < widget.travelClass.contentList.length ; ii++ ){
                      TravelSubClass t = widget.travelClass.contentList[ii];
                      final targetContext =  t.key.currentContext;
                      RenderBox box = targetContext.findRenderObject() as RenderBox;
                      var position = box.getTransformTo(null).getTranslation(); //this is global position
                      double y = position.y;
                      print("ii " + ii .toString());
                      print(" position.dy " + y .toString());
                      posList.add(position.y);

                    }

                    for (int i = 0; i<posList.length;i++){
                      if(posList[i]<= topH+30){
                        scrollToList.add(i);
                      }

                    }
                    print("scrollToList " + scrollToList.toString());

                    if(scrollToList.isNotEmpty) {
                      _tabFirstController.animateTo(scrollToList.last);
                    }


                    RenderBox titleBox = titleKey.currentContext.findRenderObject() as RenderBox;
                    double topH2 = 0.07 * ScreenSize().getHeight(context)  + MediaQuery.of(context).padding.top;
                    print ("titlebox " + titleBox.getTransformTo(null).getTranslation().y.toString());
                    if(titleBox.getTransformTo(null).getTranslation().y <= topH2) {
                      StatusTools().setWhitBGDarkText();
                      if(showTitle == false ){
                        setState(() {
                          showTitle = true;
                        });
                      }
                    }else {
                      StatusTools().setTranBGWhiteText();
                      if(showTitle == true ){
                        setState(() {
                          showTitle = false;
                        });
                      }
                    }
                  },
                    child: CustomScrollView(
                      physics: const BouncingScrollPhysics(),
                      slivers:[
                        SliverAppBar(
                            backgroundColor: OwnColors.white,
                            expandedHeight:1.sw / 16 * 9   - ScreenSize().getTopPad(context) ,
                            shadowColor: OwnColors.white,
                            elevation: 0,
                            // title: Text(
                            //   "WWWWWWWWWWW"
                            // ),
                            // collapsedHeight:   0.07 * ScreenSize().getHeight(context),
                            centerTitle: true,
                            stretch: true,

                            pinned: true,


                            flexibleSpace:
                            LayoutBuilder(
                                builder: (BuildContext context, BoxConstraints constraints) {
                                  if(maxTop == 0){
                                    maxTop = constraints.biggest.height;
                                  }
                                  topShirkOffset = constraints.biggest.height;
                                  print (topShirkOffset /( MediaQuery.of(context).padding.top + kToolbarHeight));
                                  print (constraints.biggest.height);
                                  print (0.07 * ScreenSize().getHeight(context) + MediaQuery.of(context).padding.top  );

                                  // if((topShirkOffset  - (0.07 * ScreenSize().getHeight(context) + MediaQuery.of(context).padding.top  )) <0.1){
                                  //   StatusTools().setWhitBGDarkText();
                                  // }else {
                                  //   StatusTools().setTranBGWhiteText();
                                  // }

                                  return Stack(
                                    children: [


                                      FlexibleSpaceBar(
                                        titlePadding:  EdgeInsetsDirectional.only(
                                            start: 0.04 * ScreenSize().getHeight(context) +  0.05.sw,
                                            end:0.04 * ScreenSize().getHeight(context) +  0.05.sw,
                                            bottom: 0
                                        ),
                                        // title: Text(
                                        //  widget.travelClass.title,
                                        //   maxLines: ( topShirkOffset ==(0.07 * ScreenSize().getHeight(context) + MediaQuery.of(context).padding.top  ))?1:5,
                                        //   overflow: TextOverflow.ellipsis,
                                        //   style: TextStyle(
                                        //   fontSize: 12.nsp,
                                        //     color: OwnColors.black
                                        //
                                        // ),
                                        // ),

                                        background: CachedNetworkImage(
                                          imageUrl: widget.travelClass.cover_image,
                                          fit: BoxFit.fill,
                                          placeholder: (context, url) =>
                                              AspectRatio(
                                                aspectRatio: 1,
                                                child: FractionallySizedBox(
                                                  heightFactor: 0.5,
                                                  widthFactor: 0.5,
                                                  child: SpinKitRing(
                                                    lineWidth: 5,
                                                    color:
                                                    OwnColors.tran ,
                                                  ),
                                                ),
                                              ),
                                          errorWidget: (context, url, error) =>
                                              Icon(
                                                Icons.error,
                                                color: OwnColors.black.withOpacity(
                                                    0.5),
                                              ),
                                          imageBuilder: (context, imageProvider) =>
                                              Container(
                                                decoration: BoxDecoration(
                                                  color: OwnColors.tran,
                                                  image: DecorationImage(
                                                      image: imageProvider,
                                                      fit: BoxFit.cover),
                                                ),
                                              ),
                                          // width: MediaQuery.of(context).size.width,
                                        ),

                                      ),

                                      Positioned(
                                        child: Container(
                                          height: 0.04.sw  ,
                                          decoration: const BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.vertical(
                                              top: Radius.circular(80),
                                            ),
                                          ),
                                        ),
                                        bottom: -3,
                                        left: 0,
                                        right: 0,
                                      ),

                                      // Align(
                                      //   alignment: Alignment.centerLeft,
                                      //   child: Padding(
                                      //     padding: EdgeInsets.only(
                                      //         top: MediaQuery.of(context).padding.top,
                                      //         left: 0.04 * ScreenSize().getHeight(context)+ 0.05.sw,
                                      //         right: 0.04 * ScreenSize().getHeight(context)*2 + 0.05.sw +10),
                                      //     child: Text(
                                      //       widget.travelClass.title,
                                      //       maxLines: 1,
                                      //       overflow: TextOverflow.ellipsis,
                                      //       style: TextStyle(
                                      //         color: showTitle? OwnColors.black:
                                      //         OwnColors.tran,
                                      //         fontSize: 20.nsp,
                                      //       ),
                                      //     ),
                                      //   ),
                                      //
                                      // )
                                      // Positioned(
                                      //   child: Container(
                                      //     height: 0.035.sh,
                                      //     decoration: BoxDecoration(
                                      //       color: OwnColors.white,
                                      //       borderRadius: BorderRadius.vertical(
                                      //         top: Radius.circular(50),
                                      //       ),
                                      //         boxShadow: [
                                      //           BoxShadow(
                                      //             color: OwnColors.white.withOpacity(1),
                                      //             spreadRadius: 1,
                                      //             blurRadius: 10,
                                      //             offset: Offset(0, 8),
                                      //           )
                                      //         ]
                                      //     ),
                                      //   ),
                                      //   bottom: -1,
                                      //   left: 0,
                                      //   right: 0,
                                      // ),
                                    ],
                                  );
                                })

                        ),
                        SliverToBoxAdapter(
                          child: Column(
                            children: [

                              Container(
                                width: double.infinity,

                                decoration: BoxDecoration(
                                    color: OwnColors.white,
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(35)),border: Border.all(width: 0,color: OwnColors.white)),
                                child: Center(
                                  child: Container(
                                    width: 0.92.sw,
                                    child: Column(
                                      children: [

                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            widget.travelClass.title,
                                            key: titleKey,
                                            style: TextStyle(
                                                fontSize: 20.nsp,
                                                fontWeight: FontWeight.bold
                                            ),
                                          ),
                                        ),
                                        HorizontalSpace().create(context, 0.02),
                                        Container(
                                          width: 0.92 *
                                              ScreenSize().getWidth(context),
                                          child: Row(
                                            children: [
                                              Container(
                                                height: 0.05.sh,
                                                width: 0.05.sh,
                                                child: CachedNetworkImage(
                                                  imageUrl: widget.travelClass.guilder_avatar,
                                                  fit: BoxFit.fill,
                                                  placeholder: (context, url) => AspectRatio(
                                                    aspectRatio: 1,
                                                    child: FractionallySizedBox(
                                                      heightFactor: 0.5,
                                                      widthFactor: 0.5,
                                                      child: SpinKitRing(
                                                        lineWidth:5,
                                                        color:
                                                        OwnColors.tran ,
                                                      ),
                                                    ),
                                                  ),
                                                  errorWidget: (context, url, error) => Icon(
                                                    Icons.error,
                                                    color: OwnColors.black.withOpacity(0.5),
                                                  ),
                                                  imageBuilder: (context, imageProvider) =>
                                                      Container(
                                                        // width: 80.0,
                                                        // height: 80.0,
                                                        decoration: BoxDecoration(
                                                          color: OwnColors.tran,
                                                          shape: BoxShape.circle,
                                                          image: DecorationImage(
                                                              image: imageProvider,
                                                              fit: BoxFit.cover),
                                                        ),
                                                      ),
                                                  // width: MediaQuery.of(context).size.width,
                                                ),
                                              ),
                                              Container(width: 8,),
                                              Expanded(
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [

                                                    Text(
                                                      widget.travelClass.guilder_name,
                                                      maxLines: 1,
                                                      overflow: TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                        fontSize: 16.nsp,
                                                        color: OwnColors.black,
                                                      ),
                                                    ),
                                                    Row(
                                                      children: [
                                                        Padding(
                                                          padding: const EdgeInsets.only(left:0.0,right: 4),
                                                          child: Container(
                                                              height: 0.021 *
                                                                  ScreenSize()
                                                                      .getHeight(context),
                                                              child: Image.asset(Res.star)),
                                                        ),
                                                        Text(widget.travelClass.guilder_rate,
                                                            style: TextStyle(
                                                              fontSize: 16.nsp,
                                                              color: OwnColors.black,
                                                            )),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              Container(
                                                decoration: BoxDecoration(
                                                  border: Border.all(color: OwnColors.black,width: 1),
                                                  borderRadius:
                                                  BorderRadius.all(Radius.circular(14)),),
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left:16.0,right: 16.0,top: 4,bottom: 4),
                                                  child: Center(
                                                    child: Text(
                                                        widget.travelClass.type == "1"?
                                                        widget.travelClass.duration.toString() + S().home_filter_type2_string :
                                                        widget.travelClass.type == "2"? S().home_filter_type3 :
                                                        S(). home_filter_type4
                                                        ,
                                                        textAlign: TextAlign.center,
                                                        style: TextStyle(
                                                          fontSize: 12.nsp,
                                                          color: OwnColors.black,
                                                          fontWeight: FontWeight.bold,

                                                        )),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),//作者
                                        HorizontalSpace().create(context, 0.01),
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            widget.travelClass.description,
                                            style: TextStyle(
                                                fontSize: 16.nsp,
                                                color: OwnColors.black
                                            ),

                                          ),
                                        ),
                                        HorizontalSpace().create(context, 0.015),

                                        Container(
                                          width: 0.88 * ScreenSize().getWidth(context),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Wrap(

                                              spacing: 10,
                                              runSpacing : 10.0,
                                              children: List.generate(
                                                widget.travelClass.tag.length,

                                                    (i) {
                                                  return Container(

                                                      decoration: BoxDecoration(
                                                          color: OwnColors.white,
                                                          borderRadius: BorderRadius.only(
                                                              topLeft: Radius.circular(13),
                                                              topRight: Radius.circular(13),
                                                              bottomLeft: Radius.circular(13),
                                                              bottomRight: Radius.circular(13)),
                                                          border: Border.all(
                                                              width: 0.5,color: OwnColors.CC8C8C8
                                                          ),
                                                          boxShadow: [
                                                            BoxShadow(
                                                              color: OwnColors.primaryText.withOpacity(0.4),
                                                              spreadRadius: 1,
                                                              blurRadius: 4,
                                                              offset: Offset(0, 3), // changes position of shadow
                                                            ),
                                                          ]),
                                                      child: Padding(
                                                        padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                                        child: Text(widget.travelClass.tag[i],
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                              height: 1.3,
                                                              fontSize: 14.nsp,
                                                              color: OwnColors.black
                                                          ),
                                                        ),
                                                      ));
                                                },
                                              ),
                                            ),
                                          ),
                                        ),
                                        HorizontalSpace().create(context, 0.02),

                                        HorizontalSpace().create(context, 0.01),




                                      ],
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        SliverPinnedHeader(
                          child: Container(
                            key: tabKey,
                            color: OwnColors.white,
                            child: Column(
                              children: [
                                Container(

                                  color: OwnColors.CF4F4F4,
                                  height: 1,
                                  width: 0.92 * ScreenSize().getWidth(context),
                                ),
                                // HorizontalSpace().create(context, 0.01),
                                widget.travelClass.sectionTag.length!= 0 ? _tabFirst() : Container(width: 1,),
                              ],
                            ),
                          ),
                        ),
                        SliverToBoxAdapter(
                          child: Column(
                            children: [

                              Container(
                                width: double.infinity,

                                decoration: BoxDecoration(
                                    color: OwnColors.white,
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(35)),border: Border.all(width: 0,color: OwnColors.white)),
                                child: Center(
                                  child: Container(
                                    width: 0.92.sw,
                                    child: Column(
                                      children: [

                                        ListView.builder(
                                            shrinkWrap: true,
                                            itemCount: widget.travelClass.contentList.length,
                                            scrollDirection: Axis.vertical,
                                            padding: const EdgeInsets.all(0.0),
                                            physics: const NeverScrollableScrollPhysics(),
                                            itemBuilder: (context, index) {

                                              print ("index  " + index.toString() ) ;
                                              return       SectionWidget(
                                                key: widget.travelClass.contentList[index].key,
                                                travelSubClass: widget.travelClass.contentList[index],
                                              );
                                            })   ,

                                        Visibility(
                                          visible: widget.travelClass.sectionTag.length!= 0  ,//|| widget.travelClass.contentList.length != 0,
                                          child: Column(
                                            children: [

                                              HorizontalSpace().create(context, 0.027),
                                            ],
                                          ),
                                        ),



                                      ],
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )

                      ]
                    ),
                  ),

              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
              child: Container(
                height: 0.07 * ScreenSize().getHeight(context),
                child: Center(
                  child: Container(
                    width: 0.92 * ScreenSize().getWidth(context),
                    child: Row(
                      children: [
                        ClipOval(
                          child: Material(
                            color: Colors.white, // button color
                            child: GestureDetector(
                              // inkwell color
                              child: SizedBox(
                                  width: 0.04 * ScreenSize().getHeight(context),
                                  height:
                                      0.04 * ScreenSize().getHeight(context),
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 0.0,left: 0.0,right: 0.0),
                                    // child: Icon(Icons.chevron_left_rounded, color: OwnColors.black,size:  0.04.sh,),
                                    // child: Center(child: Icon(Ionicons.chevron_back_outline, color: OwnColors.black,size:  0.03.sh,)),
                                    child: Center(child: Icon(LineIcons.angleLeft, color: OwnColors.black,size:  0.03.sh,)),

                                  )),
                              onTap: () {
                                backClick();
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left:8.0,right: 8),
                            child: Text(
                              widget.travelClass.title,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                color: showTitle? OwnColors.black:
                                OwnColors.tran,
                                fontSize: 20.nsp,
                              ),
                            ),
                          ),
                        ),
                        // ClipOval(
                        //   child: Material(
                        //     color: Colors.white, // button color
                        //     child: InkWell(
                        //       splashColor: OwnColors.inkColor,
                        //       // inkwell color
                        //       child: SizedBox(
                        //           width: 0.04 * ScreenSize().getHeight(context),
                        //           height:
                        //               0.04 * ScreenSize().getHeight(context),
                        //           child: Padding(
                        //             padding: const EdgeInsets.all(4.0),
                        //             child: Image.asset(Res.share_black),
                        //           )),
                        //       onTap: () {},
                        //     ),
                        //   ),
                        // ),
                        Container(
                          width: 10,
                        ),
                        // ClipOval(
                        //   child: Material(
                        //     color: Colors.white, // button color
                        //     child: InkWell(
                        //       splashColor: OwnColors.inkColor,
                        //       // inkwell color
                        //       child: SizedBox(
                        //           width: 0.04 * ScreenSize().getHeight(context),
                        //           height:
                        //               0.04 * ScreenSize().getHeight(context),
                        //           child: Padding(
                        //             padding: const EdgeInsets.all(4.0),
                        //             child: Image.asset(Res.black_heart),
                        //           )),
                        //       onTap: () {},
                        //     ),
                        //   ),
                        // )
                      ],
                    ),
                  ),
                ),
              ),
            ),

           ],
        )));
  }

  Widget _tabFirst() {
    print(widget.travelClass.contentList.length);
    return Container(
      width: 0.92.sw,
      child: DefaultTabController(
        length: widget.travelClass.sectionTag.length,
        child: TabBar(
          controller: _tabFirstController,
          isScrollable: widget.travelClass.sectionTag.length >3? true:false,
          indicatorColor: OwnColors.CTourOrange,
          labelColor: OwnColors.CTourOrange,
          unselectedLabelColor: OwnColors.C989898,
          // indicatorPadding: EdgeInsets.all(8.0),
          indicatorWeight: 2.0,

          onTap: (index) {
            currentFirstTab = index ;

            setState(() {

            });
            final targetContext = widget.travelClass.sectionTag[currentFirstTab].key.currentContext;
            if (targetContext != null) {
              Scrollable.ensureVisible(
                targetContext,
                duration: const Duration(milliseconds: 400),
                curve: Curves.easeInOut,
              );
            }

          },


          tabs:
            List.generate(
              widget.travelClass.sectionTag.length,
                  (i) {
                print(i);
                return widget.travelClass.sectionTag.length >3? Container(
                  width: 0.23.sw,
                  child: Tab(child: Center(
                    child: Text(widget.travelClass.sectionTag[i].title,
                      style: TextStyle(
                          fontSize: 16.nsp
                      ),
                    ),
                  ),),
                ) :
                Container(
                  // width: 0.2.sw,
                  child: Tab(child: Center(
                    child: Text(widget.travelClass.sectionTag[i].title,
                      style: TextStyle(
                          fontSize: 16.nsp
                      ),
                    ),
                  ),),
                );
              },
            )

            // Container(
            //   width: 0.306.sw,
            //   child: Tab(child: Text(S().personal_collection_tab2,
            //     style: TextStyle(
            //         fontSize: 16.nsp
            //     ),
            //   ),),
            // ),

          ,
        ),
      ),
    );
  }
  Widget commentCard(TravelCommentClass travelCommentClass, int index) {
    return Container(
      width: 0.92 * ScreenSize().getWidth(context),
      // height: 0.154 *  ScreenSize().getHeight(context),

      child: Column(
        children: [

          Container(
            width: 0.92 * ScreenSize().getWidth(context),

            child: Row(
              children: [
                Container(
                  height: ScreenSize().getHeight(context) * 0.05,
                  child:    AspectRatio(
                    aspectRatio: 1,
                    child: CachedNetworkImage(
                      errorWidget:
                          (context, url, error) =>
                          Icon(
                            Icons.error,
                            color: Colors.red,
                          ),
                      imageUrl: travelCommentClass.user_img,
                      fit: BoxFit.fitHeight,
                      imageBuilder:
                          (context, imageProvider) =>
                          Container(
                            // width: 80.0,
                            // height: 80.0,
                            decoration: BoxDecoration(
                              color: OwnColors.tran,
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.cover),
                            ),
                          ),
                        placeholder: (context, url) => AspectRatio(
                          aspectRatio: 1,
                          child: FractionallySizedBox(
                            heightFactor: 0.5,
                            widthFactor: 0.5,
                            child: SpinKitRing(
                              lineWidth:5,
                              color:
                              OwnColors.tran ,
                            ),
                          ),
                        )
                    ),
                  )
                  ,
                ),
                VerticalSpace().create(context, 0.01),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                          travelCommentClass.name,
                        style: TextStyle(
                          fontSize: 16.nsp,
                          fontWeight: FontWeight.bold,
                          color: OwnColors.black
                        ),
                      ),
                      Text(
                        travelCommentClass.updated_at,
                        style: TextStyle(
                            fontSize: 12.nsp,
                            fontWeight: FontWeight.normal,
                            color: OwnColors.C989898
                        ),
                      )

                    ],
                  ),
                ),
                Row(
                  children: [
                    travelCommentClass.is_liked == "1" ? SizedBox(height: 0.03.sh,child: Icon(SFSymbols.heart_fill, color: OwnColors.CTourOrange,)): SizedBox(height: 0.03.sh,child: Image.asset(Res.black_heart)),
                    Text(
                        travelCommentClass.liked_num,
                      style: TextStyle(
                        fontSize: 16.nsp,
                        color: OwnColors.black,
                        
                      ),
                    ),

                  ],
                )
              ],

            ),
          ),
          HorizontalSpace().create(context, 0.01),
          Text(
              travelCommentClass.content,
            style: TextStyle(
              fontSize: 16.nsp,
              fontWeight: FontWeight.w500,
              color: OwnColors.black
            ),
          ),
          HorizontalSpace().create(context, 0.02),
          travelCommentClass.img.contains("http")?
          Center(
            child: Container(
              // height: 0.205.sh,
              width: 0.78.sw,
 
              child: ClipRRect (
                borderRadius: BorderRadius.all(Radius.circular(15)),
                child: CachedNetworkImage(
                  imageUrl: travelCommentClass.img,
                  fit: BoxFit.fill,
                  placeholder: (context, url) => AspectRatio(
                    aspectRatio: 1,
                    child: FractionallySizedBox(
                      heightFactor: 0.5,
                      widthFactor: 0.5,
                      child: SpinKitRing(
                        lineWidth:5,
                        color:
                        OwnColors.tran ,
                      ),
                    ),
                  ),
                  errorWidget: (context, url, error) => Icon(
                    Icons.error,
                    color: OwnColors.black.withOpacity(0.5),
                  ),

                  // imageBuilder: (context, imageProvider) =>
                  //     Container(
                  //       decoration: BoxDecoration(
                  //         color: OwnColors.black,
                  //         borderRadius:
                  //         BorderRadius.all(Radius.circular(15.0)),
                  //         image: DecorationImage(
                  //             image: imageProvider,
                  //             fit: BoxFit.cover),
                  //       ),
                  //     ),
                  // width: MediaQuery.of(context).size.width,
                ),
              ),
            ),
          ):Container(width: 0,),
          HorizontalSpace().create(context, 0.02),
          Container(
            color: index ==commentList.length-1? OwnColors.tran:OwnColors.CC8C8C8,
            height: 1,
            width: 0.92 * ScreenSize().getWidth(context),
          ),
          HorizontalSpace().create(context, 0.02),

        ],
      ),
    );
  }





  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }


}

class SectionWidget extends StatelessWidget {
  final TravelSubClass travelSubClass;
  final int index;

  const SectionWidget({Key key,  this.travelSubClass,  this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int currentIndexPage = 0;


    return Container(


        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [


            travelSubClass.title!= null && travelSubClass.title != ""?
            SelectableAutoLinkText(
              index!= 0? "\n\n"+ travelSubClass.title : travelSubClass.title,
              linkStyle: TextStyle(color: Colors.blueAccent,decoration: TextDecoration.underline,),
              highlightedLinkStyle: TextStyle(
                color: Colors.blueAccent,
                backgroundColor: Colors.blueAccent.withAlpha(0x33),
              ),
              textAlign: TextAlign.center,
          moreBool: false,

        style:GoogleFonts.inter(
                fontSize: 20.nsp,
                color: OwnColors.CTourOrange,
                fontWeight: FontWeight.w700,
              ),
            ) :       Container(width: 1,),

            travelSubClass.content!= null?
            ListView.builder(
                shrinkWrap: true,
                itemCount: travelSubClass.content.length,
                scrollDirection: Axis.vertical,
                physics: const NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.all(0.0),
                itemBuilder: (context, index) {
                  bool containImg = false;
                  for (String a  in travelSubClass.content[index].image){
                    if (a.contains("http")){
                      containImg = true;
                    }
                  }

                  return      Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      HorizontalSpace().create(context, 0.026),
                      Row(
                        children: [
                          Flexible(
                            child: SelectableAutoLinkText(
                              travelSubClass.content[index].title,
                              linkStyle: TextStyle(color: Colors.blueAccent,decoration: TextDecoration.underline,),
                              highlightedLinkStyle: TextStyle(
                                color: Colors.blueAccent,
                                backgroundColor: Colors.blueAccent.withAlpha(0x33),
                              ),
                              key: travelSubClass.content[index].key,
                              textAlign: TextAlign.left,
                              style:GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.black,
                                fontWeight: FontWeight.w700,
                              ),
                              moreBool: false,

                            ),
                          ),
                          travelSubClass.content[index].google.contains("goo")?Padding(
                            padding: const EdgeInsets.only(left:8.0),
                            child: Container(
                              height: 0.018.sh,
                              child: GestureDetector(onTap: () async {
                                print("tap");
                                if (await canLaunch(travelSubClass.content[index].google)) {
                                  await launch(travelSubClass.content[index].google);
                                } else {
                                  throw 'Could not open the map.';
                                }

                              },child: Image.asset(Res.mapping)),
                            ),
                          ) : Container(width: 1,)


                        ],
                      ),
                      HorizontalSpace().create(context, 0.026),
                      Visibility(
                        visible: travelSubClass.content[index].description.length>0 && travelSubClass.content[index].description.toString() != "null",
                        child: SelectableAutoLinkText(
                          travelSubClass.content[index].description,
                          linkStyle: TextStyle(color: Colors.blueAccent,decoration: TextDecoration.underline,),
                          highlightedLinkStyle: TextStyle(
                            color: Colors.blueAccent,
                            backgroundColor: Colors.blueAccent.withAlpha(0x33),
                          ),
                          onTap: (url) => launch(url, forceSafariVC: false),
                          style:GoogleFonts.inter(
                            fontSize: 15.nsp,
                            height: 1.7,
                            color: OwnColors.black,
                            fontWeight: FontWeight.w400,


                          ),
                          moreBool: false,

                        ),
                      ),
                      containImg?HorizontalSpace().create(context, 0.026):Container(width: 1,),
                      containImg?Container(
                          width: 0.92 * ScreenSize().getWidth(context),

                          child:
                          ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: travelSubClass.content[index].image.length,
                              scrollDirection: Axis.vertical,
                              itemBuilder: (context, i) {
                                return

                                  Padding(
                                      padding: EdgeInsets.only(top: i != 0 ? 12.0:0.0),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(Radius.circular(14)),
                                        child: CachedNetworkImage(
                                          imageUrl: travelSubClass.content[index].image[i],
                                          fit: BoxFit.fitWidth,
                                          width: 200,
                                          placeholder: (context, url) => AspectRatio(
                                            aspectRatio: 1,
                                            child: FractionallySizedBox(
                                              heightFactor: 0.5,
                                              widthFactor: 0.5,
                                              child: SpinKitRing(
                                                lineWidth:5,
                                                color:
                                                OwnColors.tran ,
                                              ),
                                            ),
                                          ),
                                          errorWidget: (context, url, error) =>
                                              Icon(
                                                Icons.error,
                                                color: OwnColors.black.withOpacity(0.5),
                                              ),
                                          // imageBuilder: (context, imageProvider) =>
                                          //     Center(
                                          //       child: Container(
                                          //         // width:0.92 * ScreenSize().getWidth(context),
                                          //
                                          //         // height: 0.92 * ScreenSize().getWidth(context),
                                          //         decoration: BoxDecoration(
                                          //           color: OwnColors.tran,
                                          //           borderRadius: BorderRadius.all(
                                          //               Radius.circular(14)),
                                          //           image: DecorationImage(
                                          //               image: imageProvider,
                                          //               fit: BoxFit.fitWidth),
                                          //         ),
                                          //       ),
                                          //     ),
                                        ),
                                      )
                                  );
                              })

//                       Swiper(
//
//                         itemBuilder: (BuildContext context, int ii) {
//                           return  CachedNetworkImage(
//                             imageUrl: travelSubClass.content[index].image[ii],
//                             fit: BoxFit.cover,
//                             placeholder: (context, url) => AspectRatio(
//                               aspectRatio: 1,
//                               child: FractionallySizedBox(
//                                 heightFactor: 0.5,
//                                 widthFactor: 0.5,
//                                 child: SpinKitRing(
//                                   lineWidth:5,
//                                   color:
//                                   OwnColors.tran ,
//                                 ),
//                               ),
//                             ),
//                             errorWidget: (context, url, error) =>
//                                 Icon(
//                                   Icons.error,
//                                   color: OwnColors.black.withOpacity(0.5),
//                                 ),
//                             imageBuilder: (context, imageProvider) =>
//                                 Container(
//                                   // width:0.92 * ScreenSize().getWidth(context),
//                                   // height: 0.92 * ScreenSize().getWidth(context),
//                                   decoration: BoxDecoration(
//                                     color: OwnColors.tran,
//                                     borderRadius: BorderRadius.all(
//                                         Radius.circular(14)),
//                                     image: DecorationImage(
//                                         image: imageProvider,
//                                         fit: BoxFit.cover),
//                                   ),
//                                 ),
//                             // width: MediaQuery.of(context).size.width,
//                           );
//
//                         },
//                         outer:true,
//                         autoplay: false,
//                         itemCount: travelSubClass.content[index].image.length,
//                         itemWidth: 300.0,
//                         itemHeight: 200.0,
//                         // onIndexChanged: (index) {
//                         //   currentIndexPage = index;
//                         //   setState(() {});
//                         // },
//                         loop:false,
//                         pagination:
//
//                         travelSubClass.content[index].image.length == 1? null :new SwiperPagination(
//                           builder: new DotSwiperPaginationBuilder(
//
//                             size: travelSubClass.content[index].image.length == 1? 0:8,
//                             activeSize: travelSubClass.content[index].image.length == 1? 0 :8,
//                             color: OwnColors.CF4F4F4,
//                             activeColor: travelSubClass.content[index].image.length == 1? OwnColors.tran:OwnColors.CTourOrange,
//                           ),
// // ),
//                           // control: new SwiperControl(),
//                         ),
//                       )
                      ):Container(width: 1,),
                      // HorizontalSpace().create(context, 0.01),
                      // DotsIndicator(
                      //   dotsCount: travelSubClass.content[index].image.length,
                      //   position: currentIndexPage.toDouble(),
                      //   decorator: DotsDecorator(
                      //     color: OwnColors.CC8C8C8,
                      //     activeColor: OwnColors.CTourOrange,
                      //   ),
                      // ),

                    ],
                  );
                }):
            Container(width: 1,)

          ],
        ));
  }








}
