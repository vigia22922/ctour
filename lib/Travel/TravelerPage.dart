import 'dart:async';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/Chat/ChattingPage.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Firebase/FirebaseDynamicLink.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Guild/GuildReviewPage.dart';
import 'package:ctour/Guild/GuilderTravelPage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/StatusTools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icons.dart';
import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/TopBar.dart';
import '../res.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_star/flutter_star.dart';

class TravelerPage extends StatefulWidget {

  final String id;

  const TravelerPage(
      {Key key,
        this.id,


      })
      : super(key: key);

  @override
  State createState() {
    return _TravelerPageState();
  }
}

class _TravelerPageState extends State<TravelerPage> {
  List<String> imageList = [];
  bool more  = false;
  final ScrollController _controller = ScrollController();
  double topShirkOffset = 0;
  double maxTop = 0.0;


  GlobalKey titleKey = new  GlobalKey();
  bool showTitle = false ;
  GuildClass guildClass = new GuildClass(name:"", score:"0.0", comment_num: "0", lang: [], trans: [], location: [], is_collect: "0", description: "");

  String link = "";
  @override
  void initState() {
    super.initState();
    StatusTools().setTranBGWhiteText();


    Future.delayed(Duration(milliseconds: 200), () {
      // 5s over, navigate to a new page
      getGuild();
    });
    // getGuild();

  }

  @override
  void dispose() {
    // StatusTools().setWhitBGDarkText();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.white,

        body: Stack(
          children: [
            Container(
              width: ScreenSize().getWidth(context) * 1.0,
              color: OwnColors.white,
              height: MediaQuery.of(context).size.height,
              child: SingleChildScrollView(
                child: Container(
                  width: ScreenSize().getWidth(context) * 1.0,
                  height: MediaQuery.of(context).size.height,
                  child: Stack(
                    children: [
                      NotificationListener<ScrollUpdateNotification>(
                        onNotification: (notification) {

                          RenderBox titleBox = titleKey.currentContext.findRenderObject() as RenderBox;
                          double topH2 = 0.07 * ScreenSize().getHeight(context)  + MediaQuery.of(context).padding.top;
                          print ("titlebox " + titleBox.getTransformTo(null).getTranslation().y.toString());
                          if(titleBox.getTransformTo(null).getTranslation().y <= topH2) {
                            StatusTools().setWhitBGDarkText();
                            if(showTitle == false ){
                              setState(() {
                                showTitle = true;
                              });
                            }
                          }else {
                            StatusTools().setTranBGWhiteText();
                            if(showTitle == true ){
                              setState(() {
                                showTitle = false;
                              });
                            }
                          }


                        },
                        child: CustomScrollView(
                          controller: _controller,
                          physics: const BouncingScrollPhysics(),
                          slivers: [
                            SliverAppBar(
                                backgroundColor: OwnColors.white,
                                expandedHeight:0.5 * ScreenSize().getHeight(context),
                                shadowColor: OwnColors.white,
                                elevation: 0,
                                stretch: true,
                                bottom: PreferredSize(
                                  child: Container(
                                    color: Colors.orange,
                                  ),
                                  preferredSize: Size(0, 0),
                                ),
                                pinned: true,


                                flexibleSpace:

                                LayoutBuilder(
                                    builder: (BuildContext context, BoxConstraints constraints) {
                                      if(maxTop == 0){
                                        maxTop = constraints.biggest.height;
                                      }
                                      topShirkOffset = constraints.biggest.height;

                                      // if((topShirkOffset  - (0.07 * ScreenSize().getHeight(context) + MediaQuery.of(context).padding.top  )) <0.1){
                                      //   StatusTools().setWhitBGDarkText();
                                      // }else {
                                      //   StatusTools().setTranBGWhiteText();
                                      // }

                                      return Stack(
                                        children: [
                                          FlexibleSpaceBar(

                                            background:Swiper(

                                              itemBuilder: (BuildContext context, int index) {
                                                return CachedNetworkImage(
                                                  imageUrl: imageList[index],
                                                  fit: BoxFit.fill,
                                                  placeholder: (context, url) => AspectRatio(
                                                    aspectRatio: 1,
                                                    child: FractionallySizedBox(
                                                      heightFactor: 0.5,
                                                      widthFactor: 0.5,
                                                      child: SpinKitRing(
                                                        lineWidth:5,
                                                        color:
                                                        OwnColors.tran ,
                                                      ),
                                                    ),
                                                  ),
                                                  errorWidget: (context, url, error) => Icon(
                                                    Icons.error,
                                                    color: OwnColors.black.withOpacity(0.5),
                                                  ),
                                                  imageBuilder: (context, imageProvider) =>
                                                      Container(
                                                        decoration: BoxDecoration(
                                                          color: OwnColors.tran,
                                                          image: DecorationImage(
                                                              image: imageProvider,
                                                              fit: BoxFit.cover),
                                                        ),
                                                      ),
                                                  // width: MediaQuery.of(context).size.width,
                                                );
                                              },
                                              itemCount: imageList.length,
                                              autoplay: false,
                                              loop: false,

                                              pagination: new SwiperPagination(
                                                  builder: new DotSwiperPaginationBuilder(
                                                    color: Colors.white,
                                                    size: 8,
                                                    activeSize: 8,
                                                    activeColor: imageList.length == 1? OwnColors.tran:OwnColors.CTourOrange,

                                                  ),
                                                  margin: EdgeInsets.only(
                                                      bottom: 0.05 *
                                                          ScreenSize()
                                                              .getHeight(context) +
                                                          10)),
                                              // control: new SwiperControl(),
                                            ) ,
                                          ),
                                          Positioned(
                                            child: Container(
                                              height: 33.h,
                                              decoration: const BoxDecoration(
                                                color: Colors.white,
                                                borderRadius: BorderRadius.vertical(
                                                  top: Radius.circular(40),
                                                ),
                                              ),
                                            ),

                                            bottom: -7,
                                            left: 0,
                                            right: 0,
                                          )                 ,
                                          // Center(
                                          //   child: Padding(
                                          //     padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top,
                                          //         left: 0.04 * ScreenSize().getHeight(context)+ 0.05.sw,
                                          //         right: 0.04 * ScreenSize().getHeight(context)*2 + 0.05.sw +10),
                                          //     child: Text(
                                          //       widget.guildClass.name,
                                          //       maxLines: 1,
                                          //       overflow: TextOverflow.ellipsis,
                                          //       style:TextStyle (
                                          //         fontFamily : 'NotoSansTC',
                                          //         fontSize: 20.nsp,
                                          //         color:  showTitle? OwnColors.black:OwnColors.tran,
                                          //         fontWeight: FontWeight.w700,
                                          //       ),
                                          //
                                          // ),
                                          //       )
                                          //
                                          //
                                          //
                                          //
                                          // )
// Positioned(

                                        ],
                                      );
                                    }
                                )

                            ),
                            SliverToBoxAdapter(
                              child: Column(
                                children: [
                                  Container(
                                    width: double.infinity,
                                    color: OwnColors.white,
                                    child: Column(
                                      children: [

                                        Container(
                                          width: 0.88 *
                                              ScreenSize().getWidth(context),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Flexible(
                                                child: Text(
                                                  guildClass.name,
                                                  key: titleKey,
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis,
                                                  style:GoogleFonts.inter(
                                                    fontSize: 25.nsp,
                                                    color: OwnColors.black,
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(top: 4.0, left:4.0,right: 4),
                                                child: Container(
                                                    height: 0.024 * ScreenSize().getHeight(context),
                                                    child: Image.asset(Res.star)),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(top: 4.0),
                                                child:  Text(guildClass.score,
                                                  style:GoogleFonts.inter(
                                                    fontSize: 20.nsp,
                                                    color: OwnColors.black,
                                                    fontWeight: FontWeight.w600,
                                                  ),),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(top: 4.0, left:4.0,right: 4),
                                                child: Center(
                                                  child: Text("("+guildClass.comment_num + " " + S().home_rate_number + ")",
                                                      textAlign: TextAlign.center,
                                                      style:GoogleFonts.inter(
                                                        fontSize: 18.nsp,
                                                        height: 1.2,
                                                        color: OwnColors.black,
                                                        fontWeight: FontWeight.w300,
                                                      )
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        HorizontalSpace().create(context, 0.008),
                                        Container(
                                          width: 0.88*ScreenSize().getWidth(context),
                                          child: Row(

                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [

                                              Container(
                                                height: 0.022* ScreenSize().getHeight(context),
                                                width: 0.046 * ScreenSize().getWidth(context),
                                                child: Padding(
                                                  padding: const EdgeInsets.only(top:2.0),
                                                  child: Icon(SFSymbols.globe, size: 20,),
                                                ),

                                              ),
                                              Flexible(
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left:7.0),
                                                  child: Text(
                                                    guildClass.lang.join(" | ") ,

                                                    style:GoogleFonts.inter(
                                                      fontSize: 18.nsp,
                                                      color: OwnColors.black,
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        Visibility(
                                          visible:! guildClass.trans.contains("3"),
                                          child: Container(
                                            width: 0.88*ScreenSize().getWidth(context),
                                            child: Row(

                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [

                                                Container(
                                                    height: 0.022* ScreenSize().getHeight(context),
                                                    width: 0.046 * ScreenSize().getWidth(context),
                                                    child: Icon(SFSymbols.car_fill, size: 19,)
                                                ),
                                                Flexible(
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(left:8.0),
                                                    child: AutoSizeText(
                                                      !guildClass.trans.contains("3") ? S().home_have_car:S().home_have_no_car,

                                                      minFontSize: 1,
                                                      style:GoogleFonts.inter(
                                                        fontSize: 18.nsp,
                                                        color: OwnColors.black,
                                                        fontWeight: FontWeight.w500,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(height: 10,),
                                        Container(
                                          width: 0.88 * ScreenSize().getWidth(context),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Wrap(

                                              spacing: 16,
                                              runSpacing : 5.0,
                                              children: List.generate(
                                                guildClass.location.length,
                                                    (i) {
                                                  return Container(

                                                      decoration: BoxDecoration(
                                                          color: OwnColors.white,
                                                          borderRadius: BorderRadius.only(
                                                              topLeft: Radius.circular(13),
                                                              topRight: Radius.circular(13),
                                                              bottomLeft: Radius.circular(13),
                                                              bottomRight: Radius.circular(13)),
                                                          border: Border.all(
                                                              width: 1,color: OwnColors.CF4F4F4
                                                          ),
                                                          boxShadow: [
                                                            BoxShadow(
                                                              color: OwnColors.primaryText.withOpacity(0.4),
                                                              spreadRadius: 1,
                                                              blurRadius: 4,
                                                              offset: Offset(0, 3), // changes position of shadow
                                                            ),
                                                          ]),
                                                      child: Padding(
                                                        padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                                        child: Text(guildClass.location[i],
                                                          textAlign: TextAlign.center,
                                                          style:GoogleFonts.inter(
                                                            height: 1.3,
                                                            fontSize: 14.nsp,
                                                            color: OwnColors.black,
                                                            fontWeight: FontWeight.w300,
                                                          ),
                                                        ),
                                                      ));
                                                },
                                              ),
                                            ),
                                          ),
                                        ),
                                        HorizontalSpace().create(context, 0.027),

                                        Container(
                                          color: OwnColors.CF4F4F4,
                                          height: 1,
                                          width: 0.92 * ScreenSize().getWidth(context),
                                        ),
                                        HorizontalSpace().create(context, 0.027),
                                        Container(
                                          width: 0.84 *
                                              ScreenSize().getWidth(context),

                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [

                                                    Align(
                                                      alignment: Alignment.centerLeft,
                                                      child: AutoSizeText(
                                                        S().guilder_abountme,
                                                        minFontSize: 1,
                                                        style:GoogleFonts.inter(
                                                          fontSize: 18.nsp,
                                                          color: OwnColors.black,
                                                          fontWeight: FontWeight.w600,
                                                        ),
                                                      ),
                                                    ),
                                                    LayoutBuilder(
                                                        builder:(context, size) {
                                                          var span = TextSpan(
                                                            text: guildClass.description == "null"?
                                                            "":guildClass.description,
                                                            style: TextStyle(fontSize: 16.nsp),
                                                          );

                                                          // Use a textpainter to determine if it will exceed max lines
                                                          var tp = TextPainter(
                                                            maxLines: 5,
                                                            textAlign: TextAlign.left,
                                                            textDirection: TextDirection.ltr,
                                                            text: span,
                                                          );

                                                          // trigger it to layout
                                                          tp.layout(maxWidth: size.maxWidth);

                                                          // whether the text overflowed or not
                                                          var exceeded = tp.didExceedMaxLines;
                                                          print("exceeded" + exceeded.toString());
                                                          return Padding(
                                                            padding: const EdgeInsets.only(left:8.0),
                                                            child: Row(
                                                              mainAxisAlignment: MainAxisAlignment.end,
                                                              crossAxisAlignment: CrossAxisAlignment.end,
                                                              children: [

                                                                Expanded(
                                                                    child:   Container(


                                                                      child: Text(
                                                                        guildClass.description == "null"?
                                                                        "":guildClass.description,
                                                                        overflow: TextOverflow.ellipsis,

                                                                        maxLines:!more ? 5:100000,
                                                                        style:GoogleFonts.inter(
                                                                          height: 1.3,
                                                                          fontSize: 16.nsp,
                                                                          color: OwnColors.CC8C8C8,
                                                                          fontWeight: FontWeight.w500,
                                                                        ),
                                                                      ),
                                                                    )

                                                                ),

                                                                Visibility(
                                                                  visible: !more && exceeded,
                                                                  child: Padding(
                                                                    padding: const EdgeInsets.only(left:2.0),
                                                                    child: GestureDetector(
                                                                      onTap: (){
                                                                        more = !more;
                                                                        setState(() {

                                                                        });
                                                                        Future.delayed(Duration(milliseconds: 100), () {
                                                                          // 5s over, navigate to a new page
                                                                          _controller.animateTo(_controller.position.maxScrollExtent,duration: Duration(milliseconds: 400),    curve: Curves.fastOutSlowIn,);
                                                                        });
                                                                      },
                                                                      child: Text(
                                                                        S().guilder_more,
                                                                        style:GoogleFonts.inter(
                                                                          fontSize: 18.nsp,
                                                                          color: OwnColors.black,
                                                                          fontWeight: FontWeight.w600,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                                Visibility(
                                                                  child:
                                                                  VerticalSpace().create(context, 0.08),
                                                                )
                                                              ],
                                                            ),
                                                          );
                                                        }
                                                    ) ,
                                                  ],
                                                ),
                                              ),
                                              Opacity(
                                                opacity: 0,
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    HorizontalSpace().create(context, 0.03),
                                                    GestureDetector(
                                                      onTap:(){
                                                        // goMess();
                                                      },
                                                      child: Column(
                                                        children: [
                                                          Container(height: 0.035.sh,child: Image.asset(Res.guilder_mes)),
                                                          Text(
                                                            S().guilder_mess,
                                                            style:GoogleFonts.inter(
                                                              fontSize: 15.nsp,
                                                              color: OwnColors.black,
                                                              fontWeight: FontWeight.w300,
                                                            ),
                                                          )
                                                        ],),
                                                    ),
                                                    HorizontalSpace().create(context, 0.01),
                                                    GestureDetector(
                                                      onTap:(){
                                                        goComment(guildClass);
                                                      },
                                                      child: Column(
                                                        children: [
                                                          Container(height: 0.035.sh,
                                                              // child: Image.asset(Res.guilder_comment)
                                                              child: Icon(SFSymbols.square_pencil)
                                                          ),
                                                          Text(
                                                            S().guilder_comment,
                                                            style:GoogleFonts.inter(
                                                              fontSize: 15.nsp,
                                                              color: OwnColors.black,
                                                              fontWeight: FontWeight.w300,
                                                            ),
                                                          )
                                                        ],),
                                                    )

                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        HorizontalSpace().create(context, 0.027),
                                        HorizontalSpace().create(context, 0.02),
                                        HorizontalSpace().create(context, 0.09),
                                        HorizontalSpace().create(context, 0.02),
                                        // Container(
                                        //   color: OwnColors.CC8C8C8,
                                        //   height: 1,
                                        //   width: 0.92 * ScreenSize().getWidth(context),
                                        // ),
                                        // HorizontalSpace().create(context, 0.024),
                                        // Container(
                                        //   height :0.054.sh,
                                        //   width: 0.315.sw,
                                        //   child: FlatButton(
                                        //     color: OwnColors.CTourOrange,
                                        //     splashColor: Colors.transparent,
                                        //     highlightColor: Colors.transparent,
                                        //     textColor: OwnColors.white,
                                        //     // elevation: 10,
                                        //     child: Padding(
                                        //       padding: const EdgeInsets.all(8.0),
                                        //       child: AutoSizeText(
                                        //         S().guilder_travel,
                                        //         minFontSize: 10,
                                        //         maxFontSize: 30,
                                        //         style:GoogleFonts.inter(
                                        //           fontSize: 18.nsp,
                                        //           color: OwnColors.white,
                                        //           fontWeight: FontWeight.w600,
                                        //         ),
                                        //         maxLines: 1,
                                        //       ),
                                        //     ),
                                        //     shape: RoundedRectangleBorder(
                                        //       borderRadius: BorderRadius.circular(20.0),
                                        //       // side: BorderSide(color: Colors.red)
                                        //     ),
                                        //     onPressed: goTravel,
                                        //   ),
                                        // ),
                                        // HorizontalSpace().create(context, 0.024),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),





                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
              child: Container(
                height: 0.07 * ScreenSize().getHeight(context),
                child: Center(
                  child: Container(
                    width: 0.92 * ScreenSize().getWidth(context),
                    child: Row(
                      children: [
                        ClipOval(
                          child: Material(
                            color: Colors.white, // button color
                            child: GestureDetector(

                              child: SizedBox(
                                  width: 0.04 * ScreenSize().getHeight(context),
                                  height:
                                  0.04 * ScreenSize().getHeight(context),
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 0.0,left: 0.0,right: 0.0),
                                    // child: Icon(Icons.chevron_left_rounded, color: OwnColors.black,size:  0.04.sh,),
                                    // child: Center(child: Icon(Ionicons.chevron_back_outline, color: OwnColors.black,size:  0.03.sh,)),
                                    child: Center(child: Icon(LineIcons.angleLeft, color: OwnColors.black,size:  0.03.sh,)),

                                  )),
                              onTap: () {
                                backClick();
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left:8.0,right: 8),
                            child: Text(
                              guildClass.name,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: GoogleFonts.inter(
                                color: showTitle? OwnColors.black:
                                OwnColors.tran,
                                fontSize: 20.nsp,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ),
                        ClipOval(
                          child: Material(
                            color: Colors.white, // button color
                            child: GestureDetector(

                              child: SizedBox(
                                  width: 0.043 * ScreenSize().getHeight(context),
                                  height:
                                  0.043 * ScreenSize().getHeight(context),
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 3.5,left: 3.5,right: 4.0),
                                    child: Icon(SFSymbols.square_arrow_up,),
                                  )),
                              onTap: () async {

                                await FlutterShare.share(
                                    title: S().travel_share_title,
                                    text: guildClass.name,
                                    linkUrl: link,
                                    chooserTitle: ''
                                );

                              },
                            ),
                          ),
                        ),
                        Container(
                          width: 10,
                        ),
                        ClipOval(
                          child: Material(
                            color: Colors.white, // button color
                            child: GestureDetector(

                              child: SizedBox(
                                  width: 0.043 * ScreenSize().getHeight(context),
                                  height:
                                  0.043 * ScreenSize().getHeight(context),
                                  child: Padding(
                                      padding: const EdgeInsets.only(top:2.0, left: 3.8,right: 4.0),
                                      //child: Image.asset(Res.collected_heart),
                                      child: guildClass.is_collect.contains("0")? Icon(SFSymbols.heart, color: OwnColors.black,size: 23,):
                                      Icon(SFSymbols.heart_fill, color: OwnColors.CTourOrange,size: 23,)
                                  )),
                              onTap: () {
                                sendFavorite(guildClass);
                              },
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),

            Positioned(
              right: 0.08.sw,
              bottom: 0.13.sh,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  HorizontalSpace().create(context, 0.03),
                  Container(
                    decoration: BoxDecoration(
                      color: OwnColors.white,
                      borderRadius:
                      BorderRadius.all(Radius.circular(8)),),
                    child: GestureDetector(
                      onTap:(){
                        goMess();
                      },
                      child: Column(
                        children: [
                          Container(
                              height: 0.035.sh,
                              child: Icon(SFSymbols.ellipses_bubble)//Image.asset(Res.guilder_mes)
                          ),
                          Text(
                            S().guilder_mess,
                            style: GoogleFonts.inter(
                                fontSize: 15.nsp,
                                fontWeight: FontWeight.w400
                            ),
                          )
                        ],),
                    ),
                  ),
                  HorizontalSpace().create(context, 0.02),
                  Container(
                    decoration: BoxDecoration(
                      color: OwnColors.white,
                      borderRadius:
                      BorderRadius.all(Radius.circular(8)),),
                    child: GestureDetector(
                      onTap:(){
                        goComment(guildClass);
                      },
                      child: Column(
                        children: [
                          Container(height: 0.035.sh,
                              // child: Image.asset(Res.guilder_comment)
                              child:Icon(SFSymbols.square_pencil)
                          ),
                          Text(
                            S().guilder_comment,
                            style:GoogleFonts.inter(
                                fontSize: 15.nsp,
                                fontWeight: FontWeight.w400
                            ),
                          )
                        ],),
                    ),
                  )

                ],
              ),
            ),
            Positioned(
                bottom: 0,child: Container(
              color: OwnColors.white,
              width: 1.sw,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    color: OwnColors.CF4F4F4,
                    height: 1,
                    width: 0.92 * ScreenSize().getWidth(context),
                  ),
                  HorizontalSpace().create(context, 0.024),
                  Container(
                    height :0.054.sh,
                    width: 0.315.sw,
                    child: FlatButton(
                      color: OwnColors.CTourOrange,
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      textColor: OwnColors.white,
                      // elevation: 10,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: AutoSizeText(
                          S().guilder_travel,
                          minFontSize: 10,
                          maxFontSize: 30,
                          style:GoogleFonts.inter(
                            fontSize: 18.nsp,
                            color: OwnColors.white,
                            fontWeight: FontWeight.w600,
                          ),
                          maxLines: 1,
                        ),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(28.0),
                        // side: BorderSide(color: Colors.red)
                      ),
                      onPressed: goTravel,
                    ),
                  ),
                  HorizontalSpace().create(context, 0.024),
                ],),
            ))
          ],
        ));
  }





  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }

  void goMess(){
    print("goMess");
    PushNewScreen().normalPush(context, ChattingPage(chat_title: guildClass.name,chat_avatar: guildClass.avatar,send_to_id: guildClass.id,send_to_fcm: "",));
  }
  void goTravel(){
    print("goTravel");
    PushNewScreen().normalPush(context, GuilderTravelPage(guildClass:guildClass));
  }
  void goComment(GuildClass guildClass){
    print("goComment");
    PushNewScreen().normalPush(context, GuildReviewPage(guildClass: guildClass,));
  }

  void sendFavorite(GuildClass guildClass) async {
    // Loading.show(context);
    ResultData resultData = await AppApi.getInstance().setCollect(context, true,  SharePre.prefs.getString(GDefine.user_id),guildClass.id);
    // Loading.dismiss(context);
    if (resultData.isSuccess()) {
      if(guildClass.is_collect.contains("1")){
        guildClass.is_collect = "0";
      }else{
        guildClass.is_collect = "1";
      }
      setState(() {

      });
    } else {
      WarningDialog.showIOSAlertDialog(context, S().send_like_fail_title,  S().send_like_fail_hint,S().confirm);
    }
  }
  Future<void> getLink() async {

    link = await FirebaseDynamicLink.createDynamicLinkGuilder(guildClass.id,imageList[0],guildClass.name,guildClass.description);
    print("link : " + link);
  }
  Future<void> getGuild() async {
    Loading.show(context);
    ResultData resultData = await AppApi.getInstance().getTourist(context, true, SharePre.prefs.getString(GDefine.user_id), widget.id);
    Loading.dismiss(context);
    if(resultData.isSuccess()) {
      if (resultData.data['score'] == null) {
        resultData.data['score'] = "0.0";
        guildClass = GuildClass.fromJson(resultData.data);
        print("001");
        imageList.add(guildClass.avatar);
        for (dynamic c in guildClass.tourist_img) {
          if (c.toString().contains("http")) {
            imageList.add(c.toString());
          }
        }
      } else {

        guildClass = GuildClass.fromJson(resultData.data);
        imageList.add(guildClass.avatar);
        print("002");
        for (dynamic c in guildClass.tourist_img) {
          if (c.toString().contains("http")) {
            imageList.add(c.toString());
          }
        }
      }
    }else {
      WarningDialog.showIOSAlertDialogWithFunc(context, S().travel_guilder_not_found_title, S().travel_guilder_not_found_hint, S().confirm,backClick);
    }
    link =guildClass.id;


    setState(() {});
  }


}
