import 'package:flutter/widgets.dart';

class TravelCommentClass {
  String id;
  String user_id;
  String name;
  String user_img;
  String img;
  String content;
  String updated_at;
  String created_at;
  DateTime created_at_time;
  String is_liked;
  String liked_num;
  String created_at_raw;




  TravelCommentClass({this.id,this.user_id,this.name,this.user_img,this.img,this.content,this.updated_at,this. created_at,this.created_at_time
    ,this.is_liked, this.liked_num,this.created_at_raw });



  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'user_id': user_id,
      'name': name,
      'user_img': user_img,
      'img': img,
      'content': content,
      'updated_at': updated_at,
      'is_liked': is_liked,
      'liked_num': liked_num,
      'created_at': created_at,
      'created_at_raw': created_at_raw,



    };
  }

  factory TravelCommentClass.fromJson(Map<String, dynamic> json) => TravelCommentClass(
    id: json["article_id"].toString(),
    user_id: json["user_id"].toString(),
    name: json["name"].toString(),
    user_img: json["user_img"].toString(),
    img: json["img"].toString(),
    content: json["content"].toString() ,
    updated_at: json["updated_at"].toString() ,
    is_liked: json["is_liked"].toString() ,
    liked_num: json["liked_num"].toString(),
    created_at: json["created_at"].toString(),
    created_at_raw: json["created_at"].toString(),

  );


}
