import 'package:auto_size_text/auto_size_text.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/TopBarWithShadow.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../tool/FigmaToScreen.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';

class AddTagPage extends StatefulWidget {



  final String title;
  final int index;
  final bool canDelete;
  final bool dragEnable;

  const AddTagPage(
      {Key key,
        this.title,
        this.index,
        this.canDelete,
        this.dragEnable,


      })
      : super(key: key);

  @override
  State createState() {
    return _AddTagPageState();
  }
}

class _AddTagPageState extends State<AddTagPage> {

  TextEditingController travelTagEdit = new TextEditingController();

  bool isCheck = false;

  @override
  void initState() {
    super.initState();
    travelTagEdit.text = widget.title;
    if(travelTagEdit.text.length > 0){
      isCheck = true;
    }else{
      isCheck = false;
    }
  }


  @override
  Widget build(BuildContext context) {
    void backClick() {
      Navigator.pop(context,[travelTagEdit.text,"nothing",widget.index.toString()]);
    }

    void save(){

      if(Tools().stringNotNullOrSpace(travelTagEdit.text)) {
        if (widget.canDelete) {
          Navigator.pop(
              context, [travelTagEdit.text, "save", widget.index.toString()]);
        }
        else if (!widget.dragEnable) {
          Navigator.pop(
              context, [travelTagEdit.text, "save", widget.index.toString()]);
        }

        else {
          Navigator.pop(
              context, [travelTagEdit.text, "new", widget.index.toString()]);
        }
      }else {
        WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_no_tag_title, S().personal_my_travel_no_tag_hint,
            S().confirm);
      }
    }
    void delete(){
      if(widget.canDelete){
        Navigator.pop(context,[travelTagEdit.text,"delete",widget.index.toString()]);
      }else {
        WarningDialog.showIOSAlertDialog(context, S().warning, S().travel_cant_delete,
            S().confirm);
        //Navigator.pop(context,[travelTagEdit.text,"nothing",widget.index.toString()]);
      }

    }

    void checkIfSave(){
      if(widget.title != null){
        if(travelTagEdit.text.toString() != widget.title){
          WarningDialog.showIOSAlertDialog3(context, S().save_not_yet, S().save_not_yet_hint2, S().cancel, S().personal_edit_save,backClick, save);
        }else{
          backClick();
        }
      }else{
        if(travelTagEdit.text.toString().length > 0){
          WarningDialog.showIOSAlertDialog3(context, S().save_not_yet, S().save_not_yet_hint2, S().cancel, S().personal_edit_save,backClick, save);
        }else{
          backClick();
        }
      }
    }
    return Scaffold(
      backgroundColor: Colors.white,
      body:GestureDetector(
        onTap:(){
          Tools().dismissK(context);

        },
        child: SafeArea(
            child:Column(
              children: [
                Topbar().saveDeleteCreateTextLength(context, checkIfSave, save, delete, isCheck),

                // TopbarWithShadow().create(context, backClick, save, delete),
                Expanded(child: tagTextField(context)),

                // HorizontalSpace().create(context, 0.)
              ],
            )
        ),
      ),
    );
  }
  Widget tagTextField(BuildContext context) {
    return SingleChildScrollView(
      keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
      child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  HorizontalSpace().create(context, 0.02),
                  Container(
                    width: 0.92 * ScreenSize().getWidth(context),
                    child: Text(
                      S().personal_my_travel_tag,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: OwnColors.black,
                          fontSize: 16.nsp),
                    ),
                  ),
                  HorizontalSpace().create(context, 0.01),
                  Container(
                    width: 0.92 * ScreenSize().getWidth(context),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      border:
                      Border.all(color: OwnColors.C989898, width: 1),
                    ),
                    child: Center(
                      child: TextField(
                          controller: travelTagEdit,
                          style: TextStyle(
                              fontSize: 16.nsp, color: OwnColors.black),
                          onChanged: (S) {
                            setState(() {
                              if(S.length > 0){
                                isCheck = true;
                              }else{
                                isCheck = false;
                              }
                            });
                          },
                          maxLines: 1,
                          decoration: InputDecoration(
                              hintText: S().personal_my_travel_tag,
                              fillColor: OwnColors.white,
                              counterStyle:
                              TextStyle(color: Colors.transparent),
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 4),

                              //Change this value to custom as you like
                              isDense: true,
                              filled: true,
                              border: OutlineInputBorder(
                                  borderRadius:
                                  new BorderRadius.circular(20.0),
                                  borderSide: BorderSide.none))),
                    ),
                  ),
                  Container(
                    width: 0.92 * ScreenSize().getWidth(context),
                    height: 0.8.sh,

                  ),
                ]
              ),
    );
  }
}