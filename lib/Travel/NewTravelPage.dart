import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/OrangeXCross.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Guild/GuildReviewPage.dart';
import 'package:ctour/Guild/GuilderTravelPage.dart';
import 'package:ctour/SQLite/Database.dart';
import 'package:ctour/SQLite/model/TravelModel.dart';
import 'package:ctour/Travel/PreviewTravelPage.dart';
import 'package:ctour/tool/CropSample.dart';
import 'package:ctour/tool/CropTool.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:ntp/ntp.dart';
import 'EditParagraphPage.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'TravelClass.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'AddTagPage.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/ModalPopupDialog.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:drag_and_drop_lists/drag_and_drop_lists.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:group_button/group_button.dart';
import 'package:image_picker/image_picker.dart';
import 'package:just_the_tooltip/just_the_tooltip.dart';
import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/TopBar.dart';
import '../res.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_star/flutter_star.dart';
import 'dart:math' as math; // import this

class NewTravelPage extends StatefulWidget {

  final TravelClass travelClass;
  final bool editOnline;

  const NewTravelPage(
      {Key key,
        this.travelClass,
        this.editOnline,


      })
      : super(key: key);

  @override
  State createState() {
    return _NewTravelPageState();
  }
}

class _NewTravelPageState extends State<NewTravelPage> {
  String imgUrl = "";

  List<String> themeList = [
    S().personal_my_travel_foody,
    S().personal_my_travel_foody,
    S().personal_my_travel_other
  ];
  List<bool> themeSelList = [true, false, false];
  int themeIndex = 0;

  List<String> travelTagList = [];
  int currentIndexPage = 0;
  TextEditingController travelTagEdit = new TextEditingController();
  TextEditingController descriptionEdit = new TextEditingController();
  TextEditingController titleEdit = new TextEditingController();

  int durationInt = 30;

  // List<DragAndDropList> _contents;

  List<TravelArticleClass> travelList = [];
  int travelTagIndex = 0;

  bool isCheckTitle = false;
  bool isCheckContent = false;
  bool isCheckImage = false;

  bool travelChange = false;

  @override
  void initState() {
    super.initState();
    // if (Platform.isAndroid) {
    SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(
        statusBarColor: OwnColors.tran, //设置为透明
        statusBarIconBrightness: Brightness.dark);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
    // }


    // travelList.add(new TravelArticleClass(
    //     id: "0", type: "0", title: "第一天", description: "DDFAFAF", image: []));
    // travelList.add(new TravelArticleClass(
    //     id: "0", type: "0", title: "第二天", description: "DDFAFAF", image: []));

    // _contents = List.generate(10, (index) {
    //   return DragAndDropList(
    //     children: <DragAndDropItem>[
    //       DragAndDropItem(
    //         child: Row(
    //           children: [
    //             Text('$index.1'),
    //           ],
    //         ),
    //       ),
    //     ],
    //   );
    // });
    if(widget.travelClass != null){
      print(widget.travelClass.id);
      print("travelList " + travelList.toString());
      titleEdit.text = widget.travelClass.title;
      imgUrl = widget.travelClass.cover_image;
      descriptionEdit.text = widget.travelClass.description;
      durationInt = int.parse(widget.travelClass.duration);
      themeIndex = int.parse(widget.travelClass.type) - 1 ;
      themeSelList = [false, false, false];
      themeSelList[themeIndex] = true;
      travelTagList = List<String>.from( widget.travelClass.tag);

      if(widget.travelClass.contentListDynamic.length==0){
        // travelList.add(new TravelArticleClass(id: "0", type: "0", title: S().personal_travel_first_chapter, description: "", image: [],google: "",dragEnable: false));
        // travelList.add(new TravelArticleClass(id: "0", type: "1", title: S().personal_travel_first_section, description:S().personal_travel_first_section, image: ["","","",],google: "",dragEnable: false));
      }

      print("contentListDynamic  " + widget.travelClass.contentListDynamic.toString());
      bool tagOpen = true;
      bool paraOpen = true;
      for (Map<String,dynamic> c in widget.travelClass.contentListDynamic){
        TravelArticleClass t = new TravelArticleClass();
        t.title = c["title"];
        t.type = c["type"];
        t.description = c["description"];
        t.google = c["google"];
        print( c["image"]);
        t.image = List<String>.from( c["image"]);
        if(t.type == "0") {
          if (tagOpen ) {
            t.dragEnable = true;
            tagOpen = false;
          } else {
            t.dragEnable = true;
          }
        }else {
          if (paraOpen ) {
            t.dragEnable = true;
            paraOpen = false;
          } else {
            t.dragEnable = true;
          }
        }
        if(t.title != "") {
          travelList.add(t);
        }
      }
      if(titleEdit.text.length > 0){
        isCheckTitle = true;
      }

      if(descriptionEdit.text.length > 0){
        isCheckContent = true;
      }

     if(imgUrl.length > 0){
       isCheckImage = true;
     }

      print(widget.travelClass.contentListDynamic);
    }else {
      // travelList.add(new TravelArticleClass(id: "0", type: "0", title: S().personal_travel_first_chapter, description: "", image: [],google: "",dragEnable: false));
      // travelList.add(new TravelArticleClass(id: "0", type: "1", title: S().personal_travel_first_section, description: S().personal_travel_first_section, image: ["","","",],google: "",dragEnable: false));
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.white,
        body: WillPopScope(
          onWillPop: (){
            backPressClick();
          },
          child: GestureDetector(
            onTap:(){
              Tools().dismissK(context);

            },
            child: Column(
              children: [
                ScreenSize().createTopPad(context),
                Visibility( //新增
                  visible: widget.travelClass == null,
                  child: Topbar().savePreviewCreateTextLength3_2(context, checkIfSave, saveClick, saveToDB, previewClick, isCheckTitle&isCheckContent&isCheckImage),
                ),
                Visibility(//草稿
                  visible: widget.travelClass != null && widget.editOnline == null,
                  child: Topbar().savePreviewCreateTextLength3(context, checkDraftIfSave, saveClick, saveToDB, previewClick, isCheckTitle&isCheckContent&isCheckImage),
                ),

                Visibility(//線上編輯
                  visible: widget.editOnline != null,
                  child: Topbar().savePreviewCreateTextLength4(context, checkIfSave, saveClick,  previewClick, isCheckTitle&isCheckContent&isCheckImage),
                ),

                Expanded(
                    child: SingleChildScrollView(
                      keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
                      child: Container(
                        width: 0.92 * ScreenSize().getWidth(context),
                        child: Column(
                          children: [
                            HorizontalSpace().create(context, 0.02),
                            Container(
                              width: 0.92 * ScreenSize().getWidth(context),
                              child: Text(
                                S().personal_my_travel_title,
                                style: GoogleFonts.inter(
                                    fontWeight: FontWeight.w700,
                                    color: OwnColors.black,
                                    fontSize: 16.nsp),
                              ),
                            ),
                            HorizontalSpace().create(context, 0.01),
                            Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(10)),
                                border:
                                Border.all(color: OwnColors.C989898, width: 1),
                              ),
                              child: Center(
                                child: TextField(
                                    controller: titleEdit,
                                    style: GoogleFonts.inter(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16.nsp, color: OwnColors.black),
                                    onChanged: (S) {
                                      setState(() {
                                        if(S.length > 0){
                                          isCheckTitle = true;
                                        }else{
                                          isCheckTitle = false;
                                        }
                                      });
                                    },
                                    maxLines: 1,
                                    decoration: InputDecoration(
                                        hintText: S().personal_my_travel_title_hint,
                                        fillColor: OwnColors.white,
                                        hintStyle: GoogleFonts.inter(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16.nsp, color: OwnColors.C989898),
                                        counterStyle:
                                        GoogleFonts.inter(color: Colors.transparent),
                                        contentPadding: EdgeInsets.symmetric(
                                            horizontal: 10, vertical: 4),

                                        //Change this value to custom as you like
                                        isDense: true,
                                        filled: true,
                                        border: OutlineInputBorder(
                                            borderRadius:
                                            new BorderRadius.circular(20.0),
                                            borderSide: BorderSide.none))),
                              ),
                            ),
                            HorizontalSpace().create(context, 0.02),
                            Container(
                              width: 0.92 * ScreenSize().getWidth(context),
                              child: Text(
                                S().personal_my_travel_title_img,
                                style: GoogleFonts.inter(
                                    fontWeight: FontWeight.w700,
                                    color: OwnColors.black,
                                    fontSize: 16.nsp),
                              ),
                            ),
                            HorizontalSpace().create(context, 0.02),
                            Container(
                              width: 0.92 * ScreenSize().getWidth(context),
                              height:  0.92 * ScreenSize().getWidth(context)/16*9,
                              child: imgUrl.contains("http")
                                  ? Stack(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      showBottomMenu();
                                    },
                                    child: Padding(
                                      padding:  EdgeInsets.only(top:0.01.sh,right:0.01.sh),
                                      child: CachedNetworkImage(
                                        imageUrl: imgUrl,
                                        fit: BoxFit.fill,
                                        placeholder: (context, url) => AspectRatio(
                                          aspectRatio: 1,
                                          child: FractionallySizedBox(
                                            heightFactor: 0.5,
                                            widthFactor: 0.5,
                                            child: SpinKitRing(
                                              lineWidth:5,
                                              color:
                                              OwnColors.tran ,
                                            ),
                                          ),
                                        ),
                                        errorWidget: (context, url, error) => Icon(
                                          Icons.error,
                                          color: OwnColors.black.withOpacity(0.5),
                                        ),
                                        imageBuilder: (context, imageProvider) =>
                                            Container(
                                              // width:0.92 * ScreenSize().getWidth(context),
                                              // height: 0.92 * ScreenSize().getWidth(context),
                                              decoration: BoxDecoration(
                                                color: OwnColors.tran,
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(14)),
                                                image: DecorationImage(
                                                    image: imageProvider,
                                                    fit: BoxFit.cover),
                                              ),
                                            ),
                                        // width: MediaQuery.of(context).size.width,
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    top:0,
                                    right: 0,
                                    child: GestureDetector(
                                      onTap: (){
                                        imgUrl = "";
                                        setState(() {
                                          isCheckImage = false;
                                        });
                                      },
                                      child: OrangeXCross().create(context),
                                    ),
                                  )
                                ],
                              )
                                  : GestureDetector (
                                onTap: () {
                                  print("onTap");
                                  showBottomMenu();
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: OwnColors.CF7F7F7,
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(14)),
                                  ),
                                  child:
                                  // bestTourList.length==0?

                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 0.0, right: 8),
                                    child: Column(
                                      children: [
                                        Expanded( child: Container(),),
                                        Expanded(
                                            child: Center(
                                              child: Stack(
                                                  alignment: Alignment.topLeft,
                                                  children:[
                                                    Transform(
                                                      alignment: Alignment.center,
                                                      transform: Matrix4.rotationY(math.pi),
                                                      child: Padding(
                                                        padding: const EdgeInsets.only(right: 6),
                                                        child: Icon(SFSymbols.photo_fill, color: OwnColors.CE6E1E1,size: ScreenSize().getWidth(context)*0.2,),
                                                      ),
                                                    ),
                                                    Container(
                                                      height: 0.10 * ScreenSize().getWidth(context),
                                                      width: 0.10 * ScreenSize().getWidth(context),
                                                      child: Padding(
                                                        padding: const EdgeInsets.only(top:5.0,right: 20),
                                                        child:Stack(
                                                          children: [

                                                            Image.asset(Res.add_fill),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ]
                                              ),
                                            ),
                                        ),
                                        Expanded( child: Container(),),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            HorizontalSpace().create(context, 0.02),
                            Container(
                              width: 0.92 * ScreenSize().getWidth(context),
                              child: Text(
                                S().personal_my_travel_theme,
                                style: GoogleFonts.inter(
                                    fontWeight: FontWeight.w700,
                                    color: OwnColors.black,
                                    fontSize: 16.nsp),
                              ),
                            ),
                            HorizontalSpace().create(context, 0.01),
                            Container(
                              child: Row(
                                children: [
                                  GestureDetector(
                                      onTap: () async {
                                        themeSelList[themeIndex] = false;
                                        themeSelList[0] = true;
                                        themeIndex = 0;
                                        setState(() {});

                                        String a =
                                        await ModalPopupDialog.showPicker(
                                            context, [
                                          1,
                                          2,
                                          3,
                                          4,
                                          5,
                                          6,
                                          7,
                                          8,
                                          9,
                                          10,
                                          11,
                                          12,
                                          13,
                                          14,
                                          15,
                                          16,
                                          17,
                                          18,
                                          19,
                                          20,
                                          21,
                                          22,
                                          23,
                                          24,
                                          25,
                                          26,
                                          27,
                                          28,
                                          29,
                                          30
                                        ]);
                                        if (!a.contains("cancel")) {
                                          a = a.split(",")[0];
                                          durationInt = int.parse(a) + 1;

                                          setState(() {});
                                        }
                                        print(a.toString());

                                      },
                                      child: dayButton()),
                                  GestureDetector(
                                      onTap: () async {
                                        themeSelList[themeIndex] = false;
                                        themeSelList[1] = true;
                                        themeIndex = 1;
                                        durationInt = 0;
                                        setState(() {});
                                      },
                                      child: otherButton(1)),
                                  GestureDetector(
                                      onTap: () {
                                        themeSelList[themeIndex] = false;
                                        themeSelList[2] = true;
                                        themeIndex = 2;
                                        setState(() {});
                                        durationInt = 0;
                                      },
                                      child: otherButton(2)),
                                ],
                              ),
                            ),

                          HorizontalSpace().create(context, 0.01),


                                HorizontalSpace().create(context, 0.03),
                                Row(
                                  children: [
                                    Text(
                                      S().personal_my_travel_description,
                                      style: GoogleFonts.inter(
                                          fontWeight: FontWeight.w600,
                                          color: OwnColors.black,
                                          fontSize: 16.nsp),
                                    ),



                                  ],

                                ),
                                HorizontalSpace().create(context, 0.01),
                                Container(
                                  height: 0.18.sh,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                    border:
                                    Border.all(color: OwnColors.C989898, width: 1),
                                  ),
                                  child: TextField(
                                      controller: descriptionEdit,
                                      style: GoogleFonts.inter(
                                          fontWeight: FontWeight.w400,
                                          fontSize: 16.nsp, color: OwnColors.black),
                                      onChanged: (S) {
                                        setState(() {
                                          if(S.length > 0){
                                            isCheckContent = true;
                                          }else{
                                            isCheckContent = false;
                                          }
                                        });
                                      },
                                      maxLength: 500,
                                      maxLines: null,
                                      decoration: InputDecoration(
                                          hintText:
                                          S().personal_my_travel_description_hint,
                                          fillColor: OwnColors.white,
                                          hintStyle:GoogleFonts.inter(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 16.nsp, color: OwnColors.C989898) ,
                                          counterStyle:
                                          GoogleFonts.inter(color: Colors.transparent),
                                          contentPadding: EdgeInsets.symmetric(horizontal: 8, vertical: 0),
                                          counterText: "",
                                          //Change this value to custom as you like
                                          isDense: true,
                                          filled: true,
                                          border: OutlineInputBorder(
                                              borderRadius:
                                              new BorderRadius.circular(20.0),
                                              borderSide: BorderSide.none))),
                                ),
                                HorizontalSpace().create(context, 0.005),
                                Row(
                                  children: [
                                    Spacer(),
                                    Padding(
                                      padding: const EdgeInsets.only(right: 8.0),
                                      child: Text(
                                        descriptionEdit.text.length.toString() + "/500",
                                        style: GoogleFonts.inter(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12.nsp, color:  descriptionEdit.text.length <= 500 ? OwnColors.C989898: Colors.red),
                                      ),
                                    )
                                  ],
                                ),
                                HorizontalSpace().create(context, 0.02),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      S().personal_my_travel_travel_tag,
                                      style: GoogleFonts.inter(
                                          fontWeight: FontWeight.w600,
                                          color: OwnColors.black,
                                          fontSize: 16.nsp),
                                    ),
                                    ClipOval(
                                      child: Material(
                                        color: Colors.transparent, // button color
                                        child: GestureDetector(

                                          // inkwell color
                                          child: SizedBox(
                                              width: 0.035.sh,
                                              height: 0.035.sh,
                                              child: Icon(SFSymbols.plus, color: OwnColors.CTourOrange,)),

                                          onTap: () {
                                            WarningDialog.showTextFieldDialog(
                                                context,
                                                S().personal_my_travel_tag_title,
                                                S().personal_my_travel_tag_title2,
                                                S().personal_my_travel_tag_title_hint,
                                                travelTagEdit,
                                                addTravelTag);
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                HorizontalSpace().create(context, 0.01),
                            Align(
                                alignment: Alignment.topLeft,
                                child: Wrap(
                                    spacing: 4,
                                    runSpacing: 15.0,
                                    children: List.generate(
                                      travelTagList.length,
                                          (i) {
                                        return GestureDetector(
                                          onTap: (){
                                            travelTagEdit.text = travelTagList[i];

                                            travelTagIndex = i;
                                            WarningDialog.showTextFieldSaveDeleteDialog(context,  S().personal_my_travel_tag_title,
                                                S().personal_my_travel_tag_title2,
                                                S().personal_my_travel_tag_title_hint, travelTagEdit
                                                ,delLocation, changeLocation);
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 8.0, right: 8),
                                            child: Container(
                                                decoration: BoxDecoration(
                                                    color: OwnColors.white,
                                                    border: Border.all(
                                                        color: OwnColors.CC8C8C8,
                                                        width: 0.5),
                                                    borderRadius: BorderRadius.only(
                                                        topLeft: Radius.circular(12),
                                                        topRight: Radius.circular(12),
                                                        bottomLeft: Radius.circular(12),
                                                        bottomRight: Radius.circular(12)),

                                                    boxShadow: [
                                                      BoxShadow(
                                                        color: OwnColors.primaryText
                                                            .withOpacity(0.4),
                                                        spreadRadius: 1,
                                                        blurRadius: 4,
                                                        offset: Offset(0,
                                                            3), // changes position of shadow
                                                      ),
                                                    ]),
                                                child: Padding(
                                                  padding: const EdgeInsets.only(
                                                      left: 12.0,
                                                      right: 12,
                                                      bottom: 2,
                                                      top: 2),
                                                  child: Text(
                                                    travelTagList[i],
                                                    textAlign: TextAlign.center,
                                                    style: GoogleFonts.inter(
                                                        height: 1.3,
                                                        fontSize: 14.nsp,
                                                        fontWeight: FontWeight.w400,
                                                        color: OwnColors.black),
                                                  ),
                                                )),
                                          ),
                                        );
                                      },
                                    ))),
                                HorizontalSpace().create(context, 0.03),
                                Row(
                                  children: [
                                    Text(
                                      S().personal_my_travel_article,
                                      style: GoogleFonts.inter(
                                          fontWeight: FontWeight.w600,
                                          color: OwnColors.black,
                                          fontSize: 16.nsp),
                                    ),
                                    // Spacer(),
                                    // Stack(
                                    //   children: [
                                    //     Container(
                                    //       decoration: BoxDecoration(
                                    //         color: OwnColors.white,
                                    //         border: Border.all(
                                    //             width: 1, color: OwnColors.CFF644E),
                                    //         borderRadius: BorderRadius.only(
                                    //             topLeft: Radius.circular(18),
                                    //             topRight: Radius.circular(18),
                                    //             bottomLeft: Radius.circular(18),
                                    //             bottomRight: Radius.circular(18)),
                                    //       ),
                                    //       child: Padding(
                                    //         padding: const EdgeInsets.only(
                                    //             left: 8.0, right: 8),
                                    //         child: Row(
                                    //           children: [
                                    //             Container(
                                    //                 height: 0.0237.sh,
                                    //                 child: Image.asset(Res.add_orange)),
                                    //             Text(
                                    //               S().personal_my_travel_tag,
                                    //               style: GoogleFonts.inter(
                                    //                   fontSize: 14.nsp,
                                    //                   fontWeight: FontWeight.w600,
                                    //                   color: OwnColors.CFF644E),
                                    //             )
                                    //           ],
                                    //         ),
                                    //       ),
                                    //     ),
                                    //     new Positioned.fill(
                                    //         child: new Material(
                                    //           color: Colors.transparent,
                                    //           child: GestureDetector(
                                    //             onTap: () async {
                                    //
                                    //
                                    //               showTagDetail("",false,0,true);
                                    //             },
                                    //
                                    //           ),
                                    //         ))
                                    //   ],
                                    // ),
                                    // Container(
                                    //   width: 8,
                                    // ),
                                    // Stack(
                                    //   children: [
                                    //     Container(
                                    //       decoration: BoxDecoration(
                                    //         color: OwnColors.white,
                                    //         border: Border.all(
                                    //             width: 1, color: OwnColors.black),
                                    //         borderRadius: BorderRadius.only(
                                    //             topLeft: Radius.circular(18),
                                    //             topRight: Radius.circular(18),
                                    //             bottomLeft: Radius.circular(18),
                                    //             bottomRight: Radius.circular(18)),
                                    //       ),
                                    //       child: Padding(
                                    //         padding: const EdgeInsets.only(
                                    //             left: 8.0, right: 8),
                                    //         child: Row(
                                    //           children: [
                                    //             Container(
                                    //                 height: 0.0237.sh,
                                    //                 child: Image.asset(Res.black_add)),
                                    //             Text(
                                    //               S().personal_my_travel_paragraph,
                                    //               style: GoogleFonts.inter(
                                    //                   fontSize: 14.nsp,
                                    //                   fontWeight: FontWeight.w600,
                                    //                   color: OwnColors.black),
                                    //             )
                                    //           ],
                                    //         ),
                                    //       ),
                                    //     ),
                                    //     new Positioned.fill(
                                    //         child: new Material(
                                    //           color: Colors.transparent,
                                    //           child: GestureDetector(
                                    //             onTap: () {
                                    //
                                    //               showParaDetail("","",[],"",false,0,true);
                                    //             },
                                    //           ),
                                    //         ))
                                    //   ],
                                    // )
                                  ],
                                ),
                                HorizontalSpace().create(context, 0.02),
                                DragAndDropLists(
                                  lastItemTargetHeight: 20,
                                  lastListTargetSize: 0,
                                  contentsWhenEmpty: Container(
                                    width: 0,
                                  ),
                                  children: List.generate(travelList.length, (index) {
                                    return DragAndDropList(

                                      canDrag: false,
                                      children: <DragAndDropItem>[
                                        DragAndDropItem(
                                          canDrag:travelList[index].dragEnable ,
                                          child: GestureDetector(
                                            onTap: (){
                                              if(travelList[index].type == "0") {
                                                showTagDetail(
                                                    travelList[index].title, travelList[index].dragEnable,
                                                    index,travelList[index].dragEnable);
                                              }else {
                                                print("title " +travelList[index].title );
                                                showParaDetail(
                                                    travelList[index].title,travelList[index].description,travelList[index].image,travelList[index].google, travelList[index].dragEnable,
                                                    index,travelList[index].dragEnable);

                                              }
                                            },
                                            child: Container(
                                              width: 0.92 * ScreenSize().getWidth(context),
                                              child: Row(

                                                children: [
                                                  Visibility(
                                                    visible: !travelList[index].dragEnable,
                                                    child: Container(
                                                        width: 0.035.sh
                                                    ),
                                                  ),
                                                  Visibility(
                                                    visible: travelList[index].dragEnable  ,
                                                    child: Container(
                                                        width: 0.035.sh,child: Icon(SFSymbols.line_horizontal_3)
                                                    ),
                                                  ),
                                                  Container(
                                                    width: 8,
                                                  ),
                                                  travelList[index].type == "0"
                                                      ? Expanded(
                                                    child: Stack(
                                                      children: [
                                                        Row(
                                                          children: [
                                                            Flexible(

                                                              child: Container(
                                                                decoration: BoxDecoration(
                                                                  border: Border.all(
                                                                      width: 1,
                                                                      color: OwnColors
                                                                          .CTourOrange),
                                                                  borderRadius:
                                                                  BorderRadius.all(
                                                                      Radius.circular(14)),
                                                                ),
                                                                child: Padding(
                                                                  padding: const EdgeInsets.only(left:8.0,right: 8),
                                                                  child: Text(
                                                                    travelList[index].title,
                                                                    style: GoogleFonts.inter(
                                                                        fontSize: 16.nsp,
                                                                        // height: ,
                                                                        fontWeight: FontWeight.w600,
                                                                        color: OwnColors.CTourOrange
                                                                    ),),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        // Container(
                                                        //   width: double.infinity,
                                                        //   height: 0.033.sh,
                                                        //   color: OwnColors.tran,
                                                        // )
                                                      ],
                                                    ),
                                                  )
                                                      : Expanded(
                                                    child: Stack(
                                                      children: [
                                                        Row(
                                                          children: [
                                                            Flexible(
                                                              child: Container(
                                                                decoration: BoxDecoration(
                                                                  border: Border.all(
                                                                      width: 1,
                                                                      color: OwnColors
                                                                          .tran),
                                                                  borderRadius:
                                                                  BorderRadius.all(
                                                                      Radius.circular(14)),
                                                                ),
                                                                child: Padding(
                                                                  padding: const EdgeInsets.only(left:8.0,right: 8),
                                                                  child: Text(
                                                                    travelList[index].title,
                                                                    style: GoogleFonts.inter(
                                                                        fontSize: 16.nsp,
                                                                        fontWeight: FontWeight.w600,
                                                                        color: OwnColors.black
                                                                    ),),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        // Container(
                                                        //   width: double.infinity,
                                                        //   height: 0.033.sh,
                                                        //   color: OwnColors.tran,
                                                        // )
                                                      ],
                                                    ),
                                                  )
                                                  ,

                                                  Container(height: 0.033.sh,child: Icon(SFSymbols.chevron_right)),

                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    );
                                  }),
                                  onItemReorder: _onItemReorder,
                                  // onListReorder: _onListReorder,
                                  disableScrolling: true,
                                ),
                            Row(
                              children: [
                 
                                Stack(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        color: OwnColors.white,
                                        border: Border.all(
                                            width: 1, color: OwnColors.CFF644E),
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(18),
                                            topRight: Radius.circular(18),
                                            bottomLeft: Radius.circular(18),
                                            bottomRight: Radius.circular(18)),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 8.0, right: 8),
                                        child: Row(
                                          children: [
                                            Container(
                                                height: 0.0237.sh,
                                                child: Image.asset(Res.add_orange)),
                                            Text(
                                              S().personal_my_travel_tag,
                                              style: GoogleFonts.inter(
                                                  fontSize: 14.nsp,
                                                  fontWeight: FontWeight.w600,
                                                  color: OwnColors.CFF644E),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    new Positioned.fill(
                                        child: new Material(
                                          color: Colors.transparent,
                                          child: GestureDetector(
                                            onTap: () async {


                                              showTagDetail("",false,0,true);
                                            },

                                          ),
                                        ))
                                  ],
                                ),
                                Container(
                                  width: 8,
                                ),
                                Stack(
                                  children: [
                                    Container(
                                      decoration: BoxDecoration(
                                        color: OwnColors.white,
                                        border: Border.all(
                                            width: 1, color: OwnColors.black),
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(18),
                                            topRight: Radius.circular(18),
                                            bottomLeft: Radius.circular(18),
                                            bottomRight: Radius.circular(18)),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 8.0, right: 8),
                                        child: Row(
                                          children: [
                                            Container(
                                                height: 0.0237.sh,
                                                child: Image.asset(Res.black_add)),
                                            Text(
                                              S().personal_my_travel_paragraph,
                                              style: GoogleFonts.inter(
                                                  fontSize: 14.nsp,
                                                  fontWeight: FontWeight.w600,
                                                  color: OwnColors.black),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                    new Positioned.fill(
                                        child: new Material(
                                          color: Colors.transparent,
                                          child: GestureDetector(
                                            onTap: () {

                                              showParaDetail("","",[],"",false,0,true);
                                            },
                                          ),
                                        ))
                                  ],
                                )
                              ],
                            ),
                                HorizontalSpace().create(context, 0.04),
                            ScreenSize().createBottomPad(context),

                          ],
                              ),
                            ),
                          ))
                          ],
                        ),

                    ),

        ));
  }

  _buildList(int outerIndex) {
    return DragAndDropItem(
        child: Row(
          children: [
            Image.asset(Res.drag),
          ],
        ));
  }

  _onItemReorder(
      int oldItemIndex, int oldListIndex, int newItemIndex, int newListIndex) {
    print("_onItemReorder");
    print("oldItemIndex" + oldItemIndex.toString());
    print("oldListIndex" + oldListIndex.toString());
    print("newItemIndex" + newItemIndex.toString());
    print("newListIndex" + newListIndex.toString());
    setState(() {
      var movedItem = travelList .removeAt(oldListIndex);
      travelList.insert(newListIndex, movedItem);
    });
    for (TravelArticleClass travelArticleClass in travelList){
      print(travelArticleClass.title);
    }
  }

  // _onListReorder(int oldListIndex, int newListIndex) {
  //   setState(() {
  //     var movedList = _contents.removeAt(oldListIndex);
  //     _contents.insert(newListIndex, movedList);
  //   });
  // }

  void checkIfSave(){
    print("checkIfSave");
    if(widget.editOnline != null){
      bool check = false;

      if(titleEdit.text != widget.travelClass.title){
        print("titleEdit");
        check = true;
      }

      if(descriptionEdit.text != widget.travelClass.description){
        print("descriptionEdit");
        check = true;
      }

      if(travelTagList.length != List<String>.from(widget.travelClass.tag).length){
        check = true;
      }else{
        for(int index = 0;index < travelTagList.length; index++){
          if(travelTagList[index] != List<String>.from(widget.travelClass.tag)[index]){
            check = true;
          }
        }
      }

      if(themeIndex != (int.parse(widget.travelClass.type) - 1)){
        print("themeIndex");
        check = true;
      }

      if(imgUrl != widget.travelClass.cover_image){
        print("imgUrl");
        check = true;
      }

      if(      travelChange ){
        check = true;
      }

      if(widget.editOnline){
        if(check){
          WarningDialog.showIOSAlertDialog3(
              context,
              S().save_not_yet,
              S().save_not_yet_hint2,
              S().cancel,
              S().personal_edit_save,
              backClick,
              saveClick);
        }else{
          backClick();
        }

      }else {
        if(check){
          WarningDialog.showIOSAlertDialog3(
              context,
              S().save_not_yet,
              S().save_not_yet_hint,
              S().cancel,
              S().personal_edit_save,
              backClick,
              saveToDB);
        }else{
          backClick();
        }

      }
    }else {
      bool check = false;

      if(titleEdit.text.toString().length > 0){
        check = true;
      }

      if(descriptionEdit.text.toString().length > 0){
        check = true;
      }

      if(travelTagEdit.text.toString().length > 0){
        check = true;
      }

      if(themeIndex != 0){
        check = true;
      }

      if(imgUrl.length > 0){
        check = true;
      }
      if(check){
        WarningDialog.showIOSAlertDialog3(context, S().save_not_yet, S().save_not_yet_hint, S().cancel, S().personal_edit_save,backClick, saveToDB);
      }else{
        backClick();
      }

    }
  }
  void checkDraftIfSave(){
    print("checkDraftIfSave");

      bool check = false;

      if(titleEdit.text != widget.travelClass.title){
        print("titleEdit");
        check = true;
      }

      if(descriptionEdit.text != widget.travelClass.description){
        print("descriptionEdit");
        check = true;
      }

      if(travelTagList.length != List<String>.from(widget.travelClass.tag).length){
        check = true;
      }else{
        for(int index = 0;index < travelTagList.length; index++){
          if(travelTagList[index] != List<String>.from(widget.travelClass.tag)[index]){
            check = true;
          }
        }
      }

      if(themeIndex != (int.parse(widget.travelClass.type) - 1)){
        print("themeIndex");
        check = true;
      }

      if(imgUrl != widget.travelClass.cover_image){
        print("imgUrl");
        check = true;
      }
    if(      travelChange ){
      check = true;
    }
    if(check){
      WarningDialog.showIOSAlertDialog3(
          context,
          S().save_not_yet,
          S().save_not_yet_hint,
          S().cancel,
          S().personal_edit_save,
          backClick,
          saveToDB);
    }else{
      backClick();
    }



  }

  void backClick() {
    print("back");

    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }
  void backPressClick() {
    checkIfSave();
    // if(widget.editOnline != null){
    //   if(widget.editOnline){
    //
    //   }else {
    //
    //     if(widget.travelClass != null){
    //
    //       bool update =false;
    //
    //       if(widget.travelClass.title != titleEdit.text){
    //         update = true;
    //       }
    //       if(widget.travelClass.description != descriptionEdit.text){
    //         update = true;
    //       }
    //       if(widget.travelClass.duration != durationInt.toString()){
    //         update = true;
    //       }
    //       if(widget.travelClass.type !=  ){
    //         update = true;
    //       }
    //
    //
    //       if(update){
    //         WarningDialog.showIOSAlertDialog3(
    //             context,
    //             S().save_not_yet,
    //             S().save_not_yet_hint,
    //             S().delete,
    //             S().personal_edit_save,
    //             backClick,
    //             saveToDB);
    //       }else {
    //         backClick();
    //       }
    //     }else {
    //       WarningDialog.showIOSAlertDialog3(
    //           context,
    //           S().save_not_yet,
    //           S().save_not_yet_hint,
    //           S().delete,
    //           S().personal_edit_save,
    //           backClick,
    //           saveToDB);
    //     }
    //   }
    // }else {
    //   WarningDialog.showIOSAlertDialog3(context, S().save_not_yet, S().save_not_yet_hint, S().delete, S().personal_edit_save,backClick, saveToDB);
    // }

  }
  Future<void> showTagDetail(String title, bool canDelete,int index,bool dragEnable) async {
    FocusScope.of(context).unfocus();
    final result = await Navigator.push(
      context,
      // Create the SelectionScreen in the next step.
      MaterialPageRoute(builder: (context) =>  AddTagPage(title: title,canDelete: canDelete,index: index,dragEnable: dragEnable,)),
    );
    List<String> a = result;
    String title2 = result[0];
    String type = result[1];
    int index2 = int.parse (result[2]);
    if(type == "save"){
      travelChange = true;
      travelList[index2].title = title2;
    }else if(type == "delete"){
      travelChange = true;
      travelList.removeAt(index2);
    }else if(type == "new"){
      travelChange = true;
      travelList.add(new TravelArticleClass(
          id: "0", type: "0", title: title2, description: "DDFAFAF", image: [],dragEnable: true));
    }
    setState(() {

    });
  }
  Future<void> showParaDetail(String title,String desctiption,List<String> img,String google1, bool canDelete,int index,bool dragEnable) async {
    FocusScope.of(context).unfocus();
    print("img " + img.toString());
    print("title " + title.toString());
    final result = await Navigator.push(
      context,
      // Create the SelectionScreen in the next step.
      MaterialPageRoute(builder: (context) =>  EditParagraphPage(title: title,canDelete: canDelete,index: index,description: desctiption, img: img,
          google:google1,dragEnable:dragEnable)),
    );
    List<String> a = result;
    String title2 = result[0];
    String des = result[1];
    String google = result[2];
    String img1 = result[3];
    String img2 = result[4];
    String img3 = result[5];
    String type = result[6];
    int index2 = int.parse (result[7]);
    print("google " + google);
    if(type == "save"){
      travelChange = true;
      travelList[index2].title = title2;
      travelList[index2].description = des;
      travelList[index2].google = google;
      travelList[index2].image =  [img1,img2,img3];
    }else if(type == "delete"){
      travelChange = true;
      travelList.removeAt(index2);
    }else if(type == "new"){
      travelChange = true;
      travelList.add(new TravelArticleClass(
          id: "0", type: "1", title: title2, description: des,google: google, image: [img1,img2,img3],dragEnable: true));
    }
    setState(() {

    });
  }

  Widget dayButton() {
    if (themeSelList[0]) {
      return Padding(
        padding: const EdgeInsets.only(left: 4.0, right: 4),
        child: Container(
            decoration: BoxDecoration(
                color: OwnColors.white,
                border: Border.all(color: OwnColors.CC8C8C8, width: 0.5),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12),
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12)),
                boxShadow: [
                  BoxShadow(
                    color: OwnColors.primaryText.withOpacity(0.4),
                    spreadRadius: 1,
                    blurRadius: 4,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ]),
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 12.0, right: 12, bottom: 3, top: 3),
              child: Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: OwnColors.white,
                      border: Border.all(width: 1, color: OwnColors.CC8C8C8),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(5),
                          topRight: Radius.circular(5),
                          bottomLeft: Radius.circular(5),
                          bottomRight: Radius.circular(5)),
                    ),
                    child: Row(
                      children: [
                        Container(
                          width: 4,
                        ),
                        Stack(
                          alignment: Alignment.center,
                          children: [
                            Text(
                              30.toString(),
                              textAlign: TextAlign.center,
                              style: GoogleFonts.inter(
                                  height: 1.2,
                                  fontSize: 14.nsp,
                                  fontWeight: FontWeight.w600,
                                  color: OwnColors.tran),
                            ),

                            Center(
                              child: Text(
                                durationInt.toString(),
                                textAlign: TextAlign.center,
                                style: GoogleFonts.inter(
                                    height: 1.2,
                                    fontSize: 14.nsp,
                                    fontWeight: FontWeight.w600,
                                    color: OwnColors.black),
                              ),
                            ),

                          ],
                        ),
                        Container(
                          width: 4,
                        ),
                        Container(
                          height: 0.018.sh,
                          width: 0.03.sw,
                          decoration: BoxDecoration(
                            color: OwnColors.CTourOrange,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(3),
                                topRight: Radius.circular(3),
                                bottomLeft: Radius.circular(3),
                                bottomRight: Radius.circular(3)),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 1.0, bottom: 1, left:2, right: 2),
                            child: Column(
                              children: [
                                Expanded(child: Image.asset(Res.up_white)),
                                Container(
                                  height: 4,
                                ),
                                Expanded(child: Image.asset(Res.down_white))


                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 2,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: 4,
                  ),
                  Text(

                    S().personal_my_travel_day + S().personal_my_travel_journey,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.inter(
                        fontWeight: FontWeight.w400,
                        height: 1.2, fontSize: 14.nsp, color: OwnColors.black),
                  )
                ],
              ),
            )),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.only(left: 8.0, right: 8),
        child: Container(
            decoration: BoxDecoration(
              color: OwnColors.white,
              border: Border.all(color: OwnColors.CC8C8C8, width: 0.5),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(12),
                  topRight: Radius.circular(12),
                  bottomLeft: Radius.circular(12),
                  bottomRight: Radius.circular(12)),
            ),
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 12.0, right: 12, bottom: 3, top:3),
              child: Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: OwnColors.white,
                      border: Border.all(width: 1, color: OwnColors.CC8C8C8),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(5),
                          topRight: Radius.circular(5),
                          bottomLeft: Radius.circular(5),
                          bottomRight: Radius.circular(5)),
                    ),
                    child: Row(
                      children: [
                        Container(
                          width: 4,
                        ),
                        Text(
                          durationInt.toString(),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.inter(
                              height: 1.2,
                              fontSize: 14.nsp,
                              fontWeight: FontWeight.w600,
                              color: OwnColors.C989898),
                        ),
                        Container(
                          width: 4,
                        ),
                        Container(
                          height: 0.018.sh,
                          width: 0.03.sw,
                          decoration: BoxDecoration(
                            color: OwnColors.CTourOrange,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(3),
                                topRight: Radius.circular(3),
                                bottomLeft: Radius.circular(3),
                                bottomRight: Radius.circular(3)),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 1.0, bottom: 1, left:2, right: 2),
                            child: Column(
                              children: [
                                Expanded(child: Image.asset(Res.up_white)),
                                Container(
                                  height: 4,
                                ),
                                Expanded(child: Image.asset(Res.down_white))


                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 2,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width:4,
                  ),
                  Text(
                    S().personal_my_travel_day + S().personal_my_travel_journey,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.inter(
                        height: 1.2,
                        fontSize: 14.nsp,
                        fontWeight: FontWeight.w400,
                        color: OwnColors.C989898),
                  )
                ],
              ),
            )),
      );
    }
  }

  Widget otherButton(int index) {
    if (themeSelList[index]) {
      return Padding(
        padding: const EdgeInsets.only(left: 8.0, right: 8),
        child: Container(
            decoration: BoxDecoration(
                color: OwnColors.white,
                border: Border.all(color: OwnColors.CC8C8C8, width: 0.5),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12),
                    bottomLeft: Radius.circular(12),
                    bottomRight: Radius.circular(12)),
                boxShadow: [
                  BoxShadow(
                    color: OwnColors.primaryText.withOpacity(0.4),
                    spreadRadius: 1,
                    blurRadius: 4,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ]),
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 12.0, right: 12, bottom: 2, top: 2),
              child:                  Container(
                decoration: BoxDecoration(
                  color: OwnColors.white,
                  border: Border.all(width: 1, color: OwnColors.tran),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5),
                      bottomLeft: Radius.circular(5),
                      bottomRight: Radius.circular(5)),
                ),
                child: Text(
                  themeList[index],
                  textAlign: TextAlign.center,
                  style: GoogleFonts.inter(
                      fontWeight: FontWeight.w400,
                      height: 1.3, fontSize: 14.nsp, color: OwnColors.black),
                ),
              ),
            )),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.only(left: 8.0, right: 8),
        child: Container(
            decoration: BoxDecoration(
              color: OwnColors.white,
              border: Border.all(color: OwnColors.CC8C8C8, width: 0.5),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(12),
                  topRight: Radius.circular(12),
                  bottomLeft: Radius.circular(12),
                  bottomRight: Radius.circular(12)),
            ),
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 12.0, right: 12, bottom: 2, top: 2),
              child:      Container(
                decoration: BoxDecoration(
                  color: OwnColors.white,
                  border: Border.all(width: 1, color: OwnColors.tran),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5),
                      bottomLeft: Radius.circular(5),
                      bottomRight: Radius.circular(5)),
                ),
                child: Text(
                  themeList[index],
                  textAlign: TextAlign.center,
                  style: GoogleFonts.inter(
                      height: 1.3, fontSize: 14.nsp, color: OwnColors.C989898),
                ),
              ),
            )),
      );
    }
  }

  void addTravelTag() {
    print("addTravelTag");
    travelTagList.add(travelTagEdit.text);
    travelTagEdit.text = "";
    setState(() {});
  }

  void saveClick() {
    if (    !Tools().stringNotNullOrSpace( titleEdit.text)) {
      WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_no_title_title, S().personal_my_travel_no_title_hint, S().confirm);
    }
    else if (imgUrl.length == 0) {
      WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_no_img_title, S().personal_my_travel_no_img_hint, S().confirm);
    } else if ( !Tools().stringNotNullOrSpace(descriptionEdit.text)) {
      WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_no_description_title, S().personal_my_travel_no_description_hint, S().confirm);
    } else if (!themeSelList.contains(true)) {
      WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_no_theme_title, S().personal_my_travel_no_theme_hint, S().confirm);
    // } else if (travelList.isEmpty) {
    //   WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_no_para_title, S().personal_my_travel_no_para_hint, S().confirm);
    //
    } else if (descriptionEdit.text.length>500) {
      WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_content_length_title, S().personal_my_travel_content_length_hint, S().confirm);
    }


    else {
      setArticle();
    }
    print("saveClick");
  }

  void previewClick() {
    if (    !Tools().stringNotNullOrSpace( titleEdit.text)) {
    WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_no_title_title, S().personal_my_travel_no_title_hint, S().confirm);
    }
    else if (imgUrl.length == 0) {
      WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_no_img_title, S().personal_my_travel_no_img_hint, S().confirm);
    } else if ( !Tools().stringNotNullOrSpace(descriptionEdit.text)) {
      WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_no_description_title, S().personal_my_travel_no_description_hint, S().confirm);
    } else if (!themeSelList.contains(true)) {
      WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_no_theme_title, S().personal_my_travel_no_theme_hint, S().confirm);
    // } else if (travelList.isEmpty) {
    //   WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_no_para_title, S().personal_my_travel_no_para_hint, S().confirm);
    //
    } else if (descriptionEdit.text.length>500) {
      WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_content_length_title, S().personal_my_travel_content_length_hint, S().confirm);
    }


    else {
      goPreview();
    }
    print("previewClick");
  }


  void changeLocation() {
    print("changeLocation");
    travelTagList[travelTagIndex] = travelTagEdit.text;
    travelTagEdit.text = "";
    setState(() {

    });

  }

  void delLocation() {
    print("delLocation");
    travelTagList.removeAt(travelTagIndex);
    travelTagEdit.text = "";
    setState(() {

    });

  }


  void  showBottomMenu( ) {
    Tools().dismissK(context);
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 150,
              child: Column(children: <Widget>[
                ListTile(


                  leading: Icon(Icons.photo_camera),
                  title: Text(S().take_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromCamera();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.photo_library),
                  title: Text(S().choose_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromPhoto();
                  },
                )
              ]));
        });
  }

  final picker = ImagePicker();
  void pcikImageFromCamera( ) async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);

    File img = File(pickedFile.path);
    Uint8List imageBytes = await pickedFile.readAsBytes();
    CroppedFile  croppedFile = await CropTool().crop(16, 9, CropAspectRatioPreset.ratio16x9, false, img.path);
    if(croppedFile != null) {
      imageBytes = await croppedFile.readAsBytes();

      String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + base64Image);
      _uploadImage(base64Image);

      print(croppedFile);
    }
  }


  void pcikImageFromPhoto() async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);
    File img = File(pickedFile.path);
    Uint8List imageBytes = await pickedFile.readAsBytes();

    CroppedFile  croppedFile = await CropTool().crop(16, 9, CropAspectRatioPreset.ratio16x9, false, img.path);
    if(croppedFile != null) {
      imageBytes = await croppedFile.readAsBytes();

      String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + base64Image);
      _uploadImage(base64Image);

      print(croppedFile);
    }

  }

  // void pcikImageFromPhoto() async {
  //   PickedFile pickedFile = await picker.getImage(
  //       source: ImageSource.gallery,
  //       maxHeight: 1440,
  //       maxWidth: 1440,
  //       imageQuality: 80);
  //   File img = File(pickedFile.path);
  //   List<int> imageBytes = await img.readAsBytes();
  //   String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
  //   print("base64Image: " + base64Image);
  //   _uploadImage(base64Image);
  //
  //   setState(() {});
  // }

  Future<void> _uploadImage(String data) async {
    // Loading.show(context);

    ResultData resultData =
    await AppApi.getInstance().setFileBase64(context, true, data);

    if (resultData.isSuccess()) {
      // Loading.dismiss(context);
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data);
      String img_url = GDefine.imageUrl + resultData.data["path"];
      print("img_url " + img_url);
      imgUrl = img_url;
      isCheckImage = true;
      setState(() {});
    } else {
      // Loading.dismiss(context);
      WarningDialog.showIOSAlertDialog(context, S().upload_fail_title, S().upload_fail_hint, S().confirm);
    }
  }



  Future<void> setArticle( )  async {
    print("setArticle");
    // Loading.show(context);
    DateTime startDate;
    try {
      startDate = await NTP.now();
    }catch(e){

    }
    print('NTP DateTime: ${startDate}');


    String type = (themeIndex + 1 ).toString();

    bool chapter = false;
    List<dynamic> contentList = new List();
    List<dynamic> contentSubList = new List();
    Map<String,dynamic> contentChapter =  <String,dynamic>{};
    bool setAPI = true;
    for(TravelArticleClass t in travelList){
      print(t.title);
      print(t.type);
      print(chapter);
      print("------");
      Map<String,dynamic> contentSub=  <String,dynamic>{};
      if(t.type == "0"){
        if(chapter){
          contentChapter["contentList"]  =  new List.from(contentSubList);
          contentList.add(new Map.from(contentChapter));
          contentSubList.clear();
          contentChapter.clear();
          contentChapter["title"] = t.title;
          chapter = true;
        }else {
          if(contentSubList.length != 0 && contentChapter.length!= 0  ){
            contentChapter["contentList"]  =  new List.from(contentSubList);
            contentList.add(new Map.from(contentChapter));
          }
          contentSubList.clear();
          contentChapter.clear();
          contentChapter["title"] = t.title;
          chapter = true;

        }
      } else {
        if(chapter){
          contentSub["title"] = t.title;
          contentSub["description"] = t.description;
          contentSub["google"] = t.google;
          contentSub["image"] = t.image;
          contentSubList.add(new Map.from(contentSub));
        }else {
          contentChapter.clear();
          contentChapter["title"] = "";
          contentSub["title"] = t.title;
          contentSub["description"] = t.description;
          contentSub["google"] = t.google;
          contentSub["image"] = t.image;
          contentSubList.add(new Map.from(contentSub));

          // setAPI = false;
          // WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_warning_structure_title,
          //     S().personal_my_travel_warning_structure_hint, S().confirm);
        }
      }
    }

    print("contentSubList " + contentSubList.toString());
    if(setAPI) {
      // if (contentSubList.isNotEmpty) {
        contentChapter["contentList"] = contentSubList;
        contentList.add(new Map.from(contentChapter));
      // }
      //
      // if (contentList.length < 1) {
      //   WarningDialog.showIOSAlertDialog(
      //       context, S().personal_my_travel_warning_structure_title,
      //       S().personal_my_travel_warning_structure_hint, S().confirm);
      // } else {
        Loading.show(context);
        print(contentList.toString());
        print(titleEdit.text);
        print(durationInt.toString());
        print(type);


        ResultData resultData;
        if (widget.editOnline != null) {
          if (widget.editOnline) {
            resultData = await AppApi.getInstance().setArticle(
                context,
                true,
                SharePre.prefs.getString(GDefine.user_id),
                widget.travelClass.id,
                imgUrl,
                titleEdit.text,
                durationInt.toString(),
                widget.travelClass.date,


                // DateFormat('yyyy-MM-dd').format((DateTime.now())),
                type,
                descriptionEdit.text,
                SharePre.prefs.getString(GDefine.user_id),
                travelTagList,
                contentList
            );
          }
        } else {
          resultData = await AppApi.getInstance().setArticle(
              context,
              true,
              SharePre.prefs.getString(GDefine.user_id),
              "0",
              imgUrl,
              titleEdit.text,
              durationInt.toString(),
              // DateFormat('yyyy-MM-dd').format((DateTime.now())),
              startDate != null?
              DateFormat('yyyy-MM-dd').format((startDate)) :
              DateFormat('yyyy-MM-dd').format((DateTime.now())),
              type,
              descriptionEdit.text,
              SharePre.prefs.getString(GDefine.user_id),
              travelTagList,
              contentList
          );
        }


        if (resultData.isSuccess()) {
          Loading.dismiss(context);
          if (widget.editOnline != null) {
            if (widget.editOnline) {
              Navigator.pop(context, "true");
            }
          } else {
            backClick();
          }

          if (widget.travelClass.id != null) {
            var db = DatabaseHelper();
            db.deleteTravel(int.parse(widget.travelClass.id));
          }
        } else {
          Loading.dismiss(context);
          WarningDialog.showIOSAlertDialog(
              context, S().upload_fail_title, S().upload_fail_title,
              S().confirm);
        }
      // }
    }
  }
  void goPreview() {
    String type = (themeIndex + 1 ).toString();

    TravelClass travelClass = new TravelClass();
    travelClass.cover_image = imgUrl;
    travelClass.title = titleEdit.text;
    travelClass.duration = durationInt.toString();
    travelClass.date = DateFormat('yyyy-MM-dd').format((DateTime.now()));
    travelClass.views = "0";
    travelClass.comment_num = "0";
    travelClass.type = type;
    travelClass.description = descriptionEdit.text;
    travelClass.guilder_name = SharePre.prefs.getString(GDefine.name);
    travelClass.guilder_rate = "";
    travelClass.guilder_comment = "";
    travelClass.guilder_avatar = SharePre.prefs.getString(GDefine.avatar);
    travelClass.is_collect = "0";
    travelClass.tag = travelTagList;

    print("previewClick");
    bool chapter = false;
    List<TravelSubClass> contentList = new List();
    List<TravelContentClass> contentSubList = new List();
    TravelSubClass travelSubClass =  new TravelSubClass();

    for(TravelArticleClass t in travelList){
      print(t.title);
      print(t.type);
      print(chapter);
      print("------");
      TravelContentClass travelContentClass= new TravelContentClass();
      if(t.type == "0"){
        if(chapter){
          travelSubClass.content  =  new List.from(contentSubList);
          contentList.add(new TravelSubClass(title: travelSubClass.title,content: travelSubClass.content,key: new GlobalKey()));
          contentSubList.clear();
          travelSubClass  =  new TravelSubClass();
          travelSubClass.title = t.title;
          chapter = true;
        }else {
          if(contentSubList.length != 0    ){
            travelSubClass.content  =  new List.from(contentSubList);
            contentList.add(new TravelSubClass(title: travelSubClass.title,content: travelSubClass.content,key: new GlobalKey()));
          }
          contentSubList.clear();
          travelSubClass  =  new TravelSubClass();
          travelSubClass.title = t.title;
          chapter = true;
        }
      } else {
        if(chapter){
          travelContentClass.title = t.title;
          travelContentClass.description = t.description;
          travelContentClass.google = t.google;
          travelContentClass.image =List.from(t.image) ;
          travelContentClass.image.removeWhere((element) =>
          element == "" || element == "null");
          travelContentClass.key = new GlobalKey();
          contentSubList.add(new TravelContentClass(title: t.title, description: t.description,google: t.google,image: travelContentClass.image ));
        }else {
          travelSubClass.title ="";
          travelContentClass.title = t.title;
          travelContentClass.description = t.description;
          travelContentClass.google = t.google;
          travelContentClass.image =List.from(t.image) ;
          travelContentClass.image.removeWhere((element) =>
          element == "" || element == "null");
          travelContentClass.key = new GlobalKey();
          contentSubList.add(new TravelContentClass(title: t.title, description: t.description,google: t.google,image: travelContentClass.image ));

          // WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_warning_structure_title,
          //     S().personal_my_travel_warning_structure_hint, S().confirm);
        }
      }
    }

    if(contentSubList.isNotEmpty) {
      travelSubClass.content=  new List.from(contentSubList);
      contentList.add(new TravelSubClass(title: travelSubClass.title,content: travelSubClass.content,key: new GlobalKey()));
    }else {
      if(travelSubClass.title!=null){
        if(travelSubClass.title.isNotEmpty) {
          travelSubClass.content = new List();
          contentList.add(new TravelSubClass(title: travelSubClass.title,
              content: travelSubClass.content,
              key: new GlobalKey()));
        }
      }
    }
    travelClass.contentList = contentList;
    travelClass.sectionTag = new List();
    for (TravelSubClass t in travelClass.contentList){
      if(t.title != ""){
        print("t.title" + t.title);
        travelClass.sectionTag.add(new SectionClass( title: t.title , key: t.key) );
      }
    }


    print(travelClass.contentList.length);

        PushNewScreen().normalPush(context, PreviewTravelPage(travelClass: travelClass,));
  }


  Future<void> saveToDB() async {

    // if(
    // Tools().stringNotNullOrSpace( titleEdit.text)
    //     ||  Tools().stringNotNullOrSpace( imgUrl)  ||
    //     Tools().stringNotNullOrSpace( descriptionEdit.text)   ||
    //     travelTagList.isNotEmpty || travelList.length>1) {
      TravelModel travelModel = new TravelModel();
      if(widget.travelClass != null){
        travelModel.id = int.parse(widget.travelClass.id);
      }
      travelModel.title = Tools().stringNotNullOrSpace( titleEdit.text) ? titleEdit.text: S().personal_my_travel_default_title;
      print(descriptionEdit.text);
      travelModel.description = descriptionEdit.text;
      travelModel.type = (themeIndex + 1).toString();

      travelModel.duration = durationInt.toString();

      travelModel.tag = jsonEncode(travelTagList);
      travelModel.cover_image = imgUrl;
      travelModel.tourist_id = SharePre.prefs.getString(GDefine.user_id);


      List<dynamic> cList = new List();
      for (TravelArticleClass t in travelList) {
        Map<String, dynamic> c = {};
        c["title"] = t.title;
        c["type"] = t.type;
        c["description"] = t.description;
        c["google"] = t.google;
        c["image"] = t.image;
        cList.add(c);
      }
      travelModel.contentList = jsonEncode(cList);
      print("travelModel.contentList " + travelModel.contentList);
      var db = new DatabaseHelper();
      await db.insertTravel(travelModel);
      backClick();
    }
  // }


}
