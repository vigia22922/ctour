
import 'dart:async';
import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Personal/BecomeGuilderPage.dart';
import 'package:ctour/Travel/TravelClass.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_star/custom_rating.dart';
import 'package:flutter_star/flutter_star.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_button/group_button.dart';


import 'package:intl/intl.dart';

import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:url_launcher/url_launcher.dart';

class TravelMenuPage extends StatefulWidget {


  TravelClass travelClass;



  TravelMenuPage(
      {Key key,
        this.travelClass,


      })
      : super(key: key);




  @override
  _TravelMenuPageState createState() => _TravelMenuPageState();
}

class _TravelMenuPageState extends State<TravelMenuPage> {

  List<String> lansList = ["中文","English","日本語","한국어","français","Deutsch","bahasa Indonesia"];
  List<bool> lansSelList = [false,false,false,false,false,false,false];
  @override
  Future<void> initState() {
    super.initState();
    _checkboxesController = GroupButtonController(

      // onDisablePressed: (index) => ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(content: Text('${_checkboxButtons[index]} is disabled')),
      // ),
    );
    lansController = GroupButtonController(

      // onDisablePressed: (index) => ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(content: Text('${_checkboxButtons[index]} is disabled')),
      // ),
    );

    List<String> lang = SharePre.getString(GDefine.lang).toString().split(",") ;
    print(lang);
    for (int i = 0 ; i<lang.length; i++){
      int idex = lansList.indexWhere((element) => element == lang[i]);
      if(idex >=0) {
        lansSelList[idex] = true;
        lansController.selectIndex(idex);
      }
    }
    _checkboxesController.disableIndexes([0,1,2]);

  }
  List<String> trafficList = [S().guild_traffic1,S().guild_traffic2, S().guild_traffic3];
  List<String> genderList = [S().guild_gender1,S().guild_gender2, S().guild_gender3];

  String traffic = S().guild_traffic1;
  String gender = S().guild_gender1;
  // double duration = 3;
  bool transBool = false;
  GroupButtonController _checkboxesController;
  GroupButtonController lansController;
  List<bool> transSelList = [false,false,false];
  SfRangeValues duration = SfRangeValues(0.0, 50.0);
  num rating = 5;
  String startNum = "0", endNum = "50";
  @override
  Widget build(BuildContext context) {
    return Material(
        color: OwnColors.white,
        child: Container(
          height:  ScreenSize().getHeightWTop(context),
          child: Column(
            children: <Widget>[

              Container(
                  height: 0.007 * ScreenSize().getHeight(context),
                  width: 0.2 * ScreenSize().getWidth(context),
                  decoration: const BoxDecoration(
                    color: OwnColors.white,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  )),
              HorizontalSpace().create(context, 0.02),
              Topbar().menuCreate(context, backClick, ),
               Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      HorizontalSpace().create(context, 0.02),
                      ListView.builder(
                          shrinkWrap: true,
                          itemCount: widget.travelClass.contentList.length,
                          scrollDirection: Axis.vertical,
                          physics: const NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {

                            // print ("index  " + index.toString() ) ;
                            return       SectionMenuWidget(
                              // key: widget.travelClass.contentList[index].key,
                              travelSubClass: widget.travelClass.contentList[index],
                              index : index,
                            );
                          })   ,
                      HorizontalSpace().create(context, 0.005),

                      ScreenSize().createBottomPad(context)
                    ],
                  ),
                ),
              ),





            ],
          ),
        ));
  }


  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }

  void setRating(num) {
    rating = num;
  }
  void updateTraffic(String item ){
    print(item);
    setState(() {
      traffic = item;
    });
  }

  void updateGender(String item ){
    print(item);
    setState(() {
      gender = item;
    });
  }

  void closeBtn() {
    Navigator.of(context).pop();
  }

  void filterClick(){
    String result_gender = "-1";
    if(gender.contains(genderList[0])){
      result_gender = "-1";
    }else if(gender.contains(genderList[1])){
      result_gender = "m";
    }else if(gender.contains(genderList[2])){
      result_gender = "f";
    }

    List<String> result_lang = new List();
    for(int count = 0; count < lansSelList.length;count++){
      if(lansSelList.elementAt(count)){
        result_lang.add(lansList.elementAt(count));
      }
    }

    List<String> result_trans = new List();
    for(int count = 0;count<transSelList.length;count++){
      if(transSelList.elementAt(count)){
        result_trans.add(count.toString());
      }
    }

    Navigator.of(context).pop([rating.toString(),duration.start.toString(),duration.end.toString(),result_trans.toString(), result_gender, result_lang.toString()]);
    // Navigator.of(context).pop([rating.toString(),duration.start.toString(),duration.end.toString(),transSelList,result_gender,result_lang]);
  }
  void cleanClick(){
    setState(() {
      traffic = S().home_filter_order_method1;
       gender = S().home_filter_date1;
      // double duration = 3;
      duration = SfRangeValues(0.0, 30.0);
    });
  }

  @override
  void dispose() {
    print("dispose");

    super.dispose();
  }

  @override
  void deactivate() {
    print("deactivate");

    super.deactivate();
  }
}


class SectionMenuWidget extends StatelessWidget {
  final TravelSubClass travelSubClass;
  final int index;

  const SectionMenuWidget({Key key,  this.travelSubClass,  this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int currentIndexPage = 0;


    return Container(


        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [

            GestureDetector(
              onTap: (){
                Navigator.of(context).pop(travelSubClass.key);
              },
              child: Container(
                width: 0.9 * ScreenSize().getWidth(context),
                child: Text(
                   travelSubClass.title,
                  textAlign: TextAlign.left,
                  style:GoogleFonts.inter(
                    fontSize: 16.nsp,
                    color: OwnColors.CTourOrange,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),

            travelSubClass.content!= null?
            Container(
              width: 0.9 * ScreenSize().getWidth(context),
              child: Center(
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: travelSubClass.content.length,
                    scrollDirection: Axis.vertical,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      bool containImg = false;
                      for (String a  in travelSubClass.content[index].image){
                        if (a.contains("http")){
                          containImg = true;
                        }
                      }

                      return      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          HorizontalSpace().create(context, 0.0154),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: GestureDetector(
                                  onTap: (){
                                    Navigator.of(context).pop(travelSubClass.content[index].key);
                                  },
                                  child: Text(
                                    travelSubClass.content[index].title,
                                    textAlign: TextAlign.left,
                                    style:GoogleFonts.inter(
                                      fontSize: 14.nsp,
                                      color: OwnColors.black,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ),
                              ),
                              travelSubClass.content[index].google.contains("goo")?Padding(
                                padding: const EdgeInsets.only(left:8.0),
                                child: Container(
                                  height: 0.018.sh,
                                  child: GestureDetector(onTap: () async {
                                    print("tap");
                                    if (await canLaunch(travelSubClass.content[index].google)) {
                                      await launch(travelSubClass.content[index].google);
                                    } else {
                                      throw 'Could not open the map.';
                                    }

                                  },child: Image.asset(Res.mapping)),
                                ),
                              ) : Padding(
                                padding: const EdgeInsets.only(left:8.0),
                                child: Container(
                                  height: 0.018.sh,
                                  child: Opacity(opacity: 0.0,child: Image.asset(Res.mapping)),
                                ),
                              )


                            ],
                          ),




                        ],
                      );
                    }),
              ),
            ):
                Container(width: 1,),
            HorizontalSpace().create(context, 0.033),
            Container(
              height: 1,
              width: 0.9 * ScreenSize().getWidth(context),
              color: OwnColors.CF4F4F4,
            ),
            HorizontalSpace().create(context, 0.033),

          ],
        ));
  }








}
