import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/OrangeXCross.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_pickers/image_pickers.dart';



import 'package:url_launcher/url_launcher.dart';
import '../CreateWidget/TopBarWithShadow.dart';
import '../Network/API/app_api.dart';
import '../tool/FigmaToScreen.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'dart:math' as math; // import this
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
class EditParagraphPage extends StatefulWidget {
  final String title;
  final String description;
  final String google;
  final List<String> img;
  final int index;
  final bool canDelete;
  final bool dragEnable;
  const EditParagraphPage(
      {Key key,
        this.title,
        this.description,
        this.img,
        this.google,
        this.index,
        this.canDelete,
        this.dragEnable,


      })
      : super(key: key);
  @override
  State createState() {
    return _EditParagraphPage();
  }
}

class _EditParagraphPage extends State<EditParagraphPage> {
  num contentLength = 0;
  String title;
  String content;
  bool showPic = false;
  bool showMap = false;

  TextEditingController titleEdit = new TextEditingController();
  TextEditingController contentEdit = new TextEditingController();
  TextEditingController googleEdit = new TextEditingController();
  List<String> imgUrlList = ["","","'"];
  List<Uint8List> img64List = [Uint8List(0),Uint8List(0),Uint8List(0)];
  // List<Uint8List> img64List = new List();

  bool isCheckTitle = false;
  bool isCheckContent = false;

  List<bool> uploadLoading = [false,false,false] ;

  @override
  void initState() {
    FocusManager.instance.primaryFocus?.unfocus();

    if( widget.title!=null){
      titleEdit.text = widget.title;
      isCheckTitle = true;
    }
    if( widget.description!=null){
      contentEdit.text = widget.description;
      isCheckContent = true;
    }
    if(widget.canDelete || !widget.dragEnable){
      titleEdit.text = widget.title;
      contentEdit.text = widget.description;
      googleEdit.text = widget.google.toString();
      imgUrlList = widget.img.toList();
      print(widget.img);

      if(contentEdit.text.length > 0){
        isCheckContent = true;
      }else{
        isCheckContent = false;
      }

      if(titleEdit.text.length > 0){
        isCheckTitle = true;
      }else{
        isCheckTitle = false;
      }

      if(imgUrlList.indexWhere((element) => element.contains("http") )!= -1){
        setState(() {
          if (showPic) {
            showPic = false;
          } else {
            showPic = true;
          }
          print(showPic);
        });
      }

      if(googleEdit.text.length > 0){
        setState(() {
          if (showMap) {
            showMap = false;
          } else {
            showMap = true;
          }
        });
      }
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    void setTitle(String text) {
      title = text;
    }

    void setContent(String text) {
      content = text;
    }

    void setContentLength(num length) {
      setState(() {
        contentLength = length;
      });
    }

    void setShowPic() {
      setState(() {
        if (showPic) {
          showPic = false;
          imgUrlList [0] = "";
          imgUrlList [1] = "";
          imgUrlList [2] = "";
          img64List = [Uint8List(0),Uint8List(0),Uint8List(0)];
        } else {
          pcikImageFromPhoto(0);
          showPic = true;
        }
        print(showPic);
      });
    }

    void setShowMap() {
      setState(() {
        if (showMap) {
          showMap = false;
          googleEdit.text = "";
        } else {
          showMap = true;
        }
      });
    }

    void save() async {
      if (!Tools().stringNotNullOrSpace(titleEdit.text)) {
        WarningDialog.showIOSAlertDialog(
            context,
            S().personal_my_travel_no_title_title,
            S().personal_my_travel_no_title_hint,
            S().confirm);
      // } else if (!Tools().stringNotNullOrSpace(contentEdit.text)) {
      //   WarningDialog.showIOSAlertDialog(
      //       context,
      //       S().personal_my_travel_no_content_title,
      //       S().personal_my_travel_no_content_hint,
      //       S().confirm);
      }
      else {
        if (googleEdit.text.isNotEmpty) {
          // ignore: deprecated_member_use
          if (await canLaunch(googleEdit.text)) {
            if (widget.canDelete) {
              Navigator.pop(context, [
                titleEdit.text,
                contentEdit.text,
                googleEdit.text,
                imgUrlList[0],
                imgUrlList[1],
                imgUrlList[2],
                "save",
                widget.index.toString()
              ]);
            } else if (!widget.dragEnable) {
              Navigator.pop(context, [
                titleEdit.text,
                contentEdit.text,
                googleEdit.text,
                imgUrlList[0],
                imgUrlList[1],
                imgUrlList[2],
                "save",
                widget.index.toString()
              ]);
            } else {
              Navigator.pop(context, [
                titleEdit.text,
                contentEdit.text,
                googleEdit.text,
                imgUrlList[0],
                imgUrlList[1],
                imgUrlList[2],
                "new",
                widget.index.toString()
              ]);
            }
          } else {
            WarningDialog.showIOSAlertDialog(
                context,
                S().personal_my_travel_google_error,
                S().personal_my_travel_google_error_hint,
                S().confirm);
          }
        } else {
          if (widget.canDelete) {
            Navigator.pop(context, [
              titleEdit.text,
              contentEdit.text,
              googleEdit.text,
              imgUrlList[0],
              imgUrlList[1],
              imgUrlList[2],
              "save",
              widget.index.toString()
            ]);
          } else if (!widget.dragEnable) {
            Navigator.pop(context, [
              titleEdit.text,
              contentEdit.text,
              googleEdit.text,
              imgUrlList[0],
              imgUrlList[1],
              imgUrlList[2],
              "save",
              widget.index.toString()
            ]);
          } else {
            Navigator.pop(context, [
              titleEdit.text,
              contentEdit.text,
              googleEdit.text,
              imgUrlList[0],
              imgUrlList[1],
              imgUrlList[2],
              "new",
              widget.index.toString()
            ]);
          }
        }
      }
    }

    void delete(){
      if(widget.canDelete){
        Navigator.pop(context,[titleEdit.text,contentEdit.text,googleEdit.text,imgUrlList[0],imgUrlList[1],imgUrlList[2],"delete",widget.index.toString()]);
      }else {
        WarningDialog.showIOSAlertDialog(context, S().warning, S().travel_cant_delete,
            S().confirm);
        //Navigator.pop(context,[titleEdit.text,contentEdit.text,googleEdit.text,imgUrlList[0],imgUrlList[1],imgUrlList[2],"nothing",widget.index.toString()]);
      }

    }


    void backClick() {

      Navigator.pop(context,[titleEdit.text,contentEdit.text,googleEdit.text,imgUrlList[0].toString(),imgUrlList[1].toString(),imgUrlList[2].toString(),"nothing",widget.index.toString()]);
    }

    bool checkIsChange(){
      bool result = false;
      if(widget.title != titleEdit.text.toString()){
        result = true;
      }

      if(widget.description != contentEdit.text.toString()){
        result = true;
      }

      if(widget.img.length != imgUrlList.length){
        for(int index = 0; index < imgUrlList.length; index++){
          if(imgUrlList[index].length > 0){
            result = true;
          }
        }
      }else{
        for(int index = 0; index < imgUrlList.length; index++){
          if(widget.img[index] != imgUrlList[index]){
            result = true;
          }
        }
      }

      if(widget.google != googleEdit.text.toString()){
        result = true;
      }

      return result;
    }

    void checkIfSave(){
      print("checkIfSave");
      if(widget.title != null){
        print("widget.title != null");
        if(checkIsChange()){
          print("checkIsChange");
          WarningDialog.showIOSAlertDialog3(context, S().save_not_yet, S().save_not_yet_hint2, S().cancel, S().personal_edit_save,backClick, save);
        }else{
          print("backClick");
          backClick();
        }
      }else{
        WarningDialog.showIOSAlertDialog3(context, S().save_not_yet, S().save_not_yet_hint2, S().cancel, S().personal_edit_save,backClick, save);
      }

      //WarningDialog.showIOSAlertDialog3(context, S().save_not_yet, S().save_not_yet_hint2, S().cancel, S().personal_edit_save,backClick, save);
    }

    return Scaffold(
      backgroundColor: Colors.white,
      body: GestureDetector(
        onTap:(){
          Tools().dismissK(context);

        },
        child: Column(

          children: [
            ScreenSize().createTopPad(context),
        Topbar().saveDeleteCreateTextLength(context, checkIfSave, save, delete, isCheckTitle),

        Expanded(
          flex: 1,
          child: SingleChildScrollView(
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
            child: Column(children: [
              // TopbarWithShadow().create(context, backClick, save, delete),
              titleTextField(context, setTitle),
              inputForm(context, contentLength, setContentLength,
                  setContent, setShowPic, setShowMap, showPic, showMap),
              ScreenSize().createBottomPad(context),
            ]),
          ),
        )
          ],
        ),
      ),
    );
  }

  Column titleTextField(BuildContext context, Function setTitle) {
    return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
      HorizontalSpace().create(context, 0.02),
      Container(
        width: 0.92 * ScreenSize().getWidth(context),
        child: Text(
          S().personal_my_travel_title,
          style: GoogleFonts.inter(
              fontWeight: FontWeight.w700,
              color: OwnColors.black,
              fontSize: 16.nsp),
        ),
      ),
      HorizontalSpace().create(context, 0.01),
      Container(
        width: 0.92 * ScreenSize().getWidth(context),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          border:
          Border.all(color: OwnColors.C989898, width: 1),
        ),
        child: Center(
          child: TextField(
              controller: titleEdit,
              style: GoogleFonts.inter(
                fontWeight: FontWeight.w500,
                  fontSize: 16.nsp, color: OwnColors.black),
              onChanged: (S) {
                setState(() {
                  if(S.length > 0){
                    isCheckTitle = true;
                  }else{
                    isCheckTitle = false;
                  }
                });
              },
              maxLines: 1,
              decoration: InputDecoration(
                  hintText: S().personal_my_travel_title_hint,
                  hintStyle:  GoogleFonts.inter(
                      fontWeight: FontWeight.w500,
                      fontSize: 16.nsp, color: OwnColors.C989898),
                  fillColor: OwnColors.white,
                  counterStyle:
                  TextStyle(color: Colors.transparent),
                  contentPadding: EdgeInsets.symmetric(
                      horizontal: 10, vertical: 4),

                  //Change this value to custom as you like
                  isDense: true,
                  filled: true,
                  border: OutlineInputBorder(
                      borderRadius:
                      new BorderRadius.circular(20.0),
                      borderSide: BorderSide.none))),
        ),
      ),
    ]);
  }

  Column inputForm(
      BuildContext context,
      num commentLength,
      Function setCommentLength,
      Function setContent,
      Function setShowPic,
      Function setShowMap,
      bool showPic,
      bool showMap) {
    return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
      HorizontalSpace().create(context, 0.02),
      Container(
        width: ScreenSize().getWidth(context)*0.92,

        child: Text(
          S().personal_my_travel_content_title,
          style: GoogleFonts.inter(
              fontWeight: FontWeight.w700,
              color: OwnColors.black,
              fontSize: 16.nsp),
        ),
      ),
      HorizontalSpace().create(context, 0.01),
      Container(
        width: 0.92 * ScreenSize().getWidth(context),
        height: 0.278.sh,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          border:
          Border.all(color: OwnColors.C989898, width: 1),
        ),
        child: TextField(
          maxLines: 50, //or null
          controller: contentEdit,
          style:GoogleFonts.inter(
            fontSize: 16.nsp,
            color: OwnColors.black,
            fontWeight: FontWeight.w500
          ),
          maxLength: 1000,
          decoration: InputDecoration(
              hintText: S().personal_my_travel_content_hint,
              fillColor: OwnColors.white,
              counterStyle:
              TextStyle(color: Colors.transparent),
              contentPadding: EdgeInsets.symmetric(
                  horizontal: 8, vertical: 0),
              counterText: "",
              //Change this value to custom as you like
              isDense: true,
              filled: true,

              hintStyle: GoogleFonts.inter(
                fontSize: 16.nsp,
                color: OwnColors.C989898,
                fontWeight: FontWeight.w500
              ),

              border: OutlineInputBorder(
                  borderRadius:
                  new BorderRadius.circular(10.0),
                  borderSide: BorderSide.none)),
              onChanged: (text) {
                setContent(text);
                setCommentLength(text.length);
                if(text.length > 0){
                  isCheckContent = true;
                }else{
                  isCheckContent = false;
                }
              },
        ),
      ),
      HorizontalSpace().create(context, 0.005),
      Container(
        width: ScreenSize().getWidth(context) * 0.92,

        child: Padding(
          padding: const EdgeInsets.only(right: 8.0),
          child: Text("$commentLength/1000",

              textAlign: TextAlign.right,
              style: GoogleFonts.inter(
                fontWeight: FontWeight.w500,
                color: Color.fromRGBO(152, 152, 152, 1),
                fontSize: 17.nsp,
              )),
        ),
      ),
      HorizontalSpace().create(context, 0.015),
      Container(
        width: 0.92 * ScreenSize().getWidth(context),
        child: Row(
          children: [

            if (!showPic) showButtons(context, S().personal_my_travel_image, FigmaToScreen().getPixelWidth(context, 75),
                FigmaToScreen().getPixelHeight(context, 27), setShowPic),
            VerticalSpace().create(context, 0.01),
            if (!showMap)showButtons(
                context,
                S().personal_my_travel_google,
                FigmaToScreen().getPixelWidth(context, 128),
                FigmaToScreen().getPixelHeight(context, 27),
                setShowMap),
          ],
        ),
      ),
      if (showPic)
        Column(children: [
          HorizontalSpace().create(context, 0.02),
          GestureDetector(
            onTap: (){
              print("WWW");
              setShowPic();
            },
            child: Container(
              width: 0.92 * ScreenSize().getWidth(context),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3),
                    child: SizedBox(height: 0.024.sh ,child: Icon(SFSymbols.minus_circle)),//Image.asset(Res.inactive)),,
                  ),

                  VerticalSpace().create(context, 0.018),
                  Text(
                    S().personal_my_travel_image,
                    textAlign: TextAlign.left,
                    style: GoogleFonts.inter(

                        fontWeight: FontWeight.w700,
                        color: OwnColors.black,
                        fontSize: 16.nsp),
                  ),
                ],
              ),
            ),
          ),

          HorizontalSpace().create(
              context, 0.02),
          picBlock(context)
        ]),
      if (showMap)
        Column(children: [
          HorizontalSpace().create(context, 0.02),
          GestureDetector(
            onTap: (){
              print("WWW");
              setShowMap();
            },
            child: Container(
              width: 0.92 * ScreenSize().getWidth(context),
              child: Row(

                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 3),
                    child: SizedBox(height: 0.024.sh ,child: Icon(SFSymbols.minus_circle)),//Image.asset(Res.inactive)),,
                  ),
                  VerticalSpace().create(context, 0.018),
                  Text(
                    S().personal_my_travel_google_link,
                    textAlign: TextAlign.left,
                    style: GoogleFonts.inter(

                        fontWeight: FontWeight.w700,
                        color: OwnColors.black,
                        fontSize: 16.nsp),
                  ),
                ],
              ),
            ),
          ),
          HorizontalSpace().create(context, 0.02),
          googleURLInput(context)
        ]),
      HorizontalSpace().create(context, 0.1),
    ]);
  }

  Widget picItem (int index){
    return Container(
        child: Container(
          width: 0.285.sw,
          child:

          imgUrlList[index]. contains("http")?
          AspectRatio(
            aspectRatio: 111/90,
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top:8.0,right: 8),
                  child: CachedNetworkImage(
                    imageUrl: imgUrlList[index],
                    fit: BoxFit.fill,
                    placeholder: (context, url) => AspectRatio(
                      aspectRatio: 1,
                      child: FractionallySizedBox(
                        heightFactor: 0.5,
                        widthFactor: 0.5,
                        child: SpinKitRing(
                          lineWidth:5,
                          color:
                          OwnColors.tran ,
                        ),
                      ),
                    ),
                    errorWidget: (context, url, error) => Icon(
                      Icons.error,
                      color: OwnColors.black.withOpacity(0.5),
                    ),
                    imageBuilder: (context, imageProvider) =>
                        Container(
                          width: 0.285.sw,
                          height: 0.11.sh,
                          decoration: BoxDecoration(
                            color: OwnColors.tran,
                            borderRadius: BorderRadius.all(
                                Radius.circular(14)),
                            image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover),
                          ),
                        ),
                    // width: MediaQuery.of(context).size.width,
                  ),
                ),
                new Positioned.fill(child: GestureDetector(
                  onTap: (){
                    print("WWW");
                    // showBottomMenu(0);
                    pcikImageFromPhoto(0);
                  },
                  child: Container(

                  ),
                )),
                Positioned(
                  top:0,
                  right: 0,
                  child: GestureDetector(
                    onTap: (){
                      // imgUrlList[0] = "";
                      // img64List[0] = Uint8List(0);
                      imgUrlList.removeAt(index);
                      img64List.removeAt(index);
                      img64List.add(Uint8List(0));
                      imgUrlList.add("");
                      setState(() {

                      });
                    },
                    child: OrangeXCross().create(context),
                  ),
                )
              ],
            ),
          ):

          img64List[index].length>=2?
          AspectRatio(
            aspectRatio: 111/90,
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top:8.0,right: 8),
                  child: Stack(
                    children: [
                      Container(
                        width: 0.285.sw,
                        height: 0.11.sh,
                        decoration: BoxDecoration(
                          color: OwnColors.tran,
                          borderRadius: BorderRadius.all(
                              Radius.circular(14)),
                          image: DecorationImage(
                              image: Image.memory(img64List[index]).image,
                              fit: BoxFit.cover),
                        ),
                      ),
                      new Positioned.fill(
                        child: Visibility(
                          visible: uploadLoading[index],
                          child:  SpinKitRing(
                            lineWidth: 3,
                            color: OwnColors.CTourOrange,
                            size: 0.285.sw/111*40,
                          ),
                        ),
                      )
                    ],
                  ),
                ),

              ],
            ),
          ):
          GestureDetector(

            onTap: (){
              // showBottomMenu(index);
              pcikImageFromPhoto(index);
            },
            child: Padding(
              padding: EdgeInsets.only(top: 0.01.sh,right: 0.01.sh),
              child: AspectRatio(
                aspectRatio: 111/90,
                child: Container(
                  decoration: BoxDecoration(
                    color: OwnColors.CF7F7F7,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  child: Center(
                    child: Stack(
                        alignment: Alignment.topLeft,
                        children:[
                          Transform(
                            alignment: Alignment.center,
                            transform: Matrix4.rotationY(math.pi),
                            child: Padding(
                              padding: const EdgeInsets.only(right: 6),
                              child: Icon(SFSymbols.photo_fill, color: OwnColors.CE6E1E1,size: ScreenSize().getWidth(context)*0.15,),
                            ),
                          ),
                          Container(
                            height: 0.10 * ScreenSize().getWidth(context),
                            width: 0.10 * ScreenSize().getWidth(context),
                            child: Padding(
                              padding: const EdgeInsets.only(top:0.0,right: 20),
                              child:Stack(
                                children: [
                                  Image.asset(Res.add_fill),
                                ],
                              ),
                            ),
                          ),
                        ]
                    ),
                  ),
                ),
              ),
            ),
          ),
        ));

  }
  Container  picBlock(BuildContext context) {
    return Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // VerticalSpace().create(context, FigmaToScreen().getWidthRatio(5)),
            picItem(0),
            picItem(1),
            picItem(2),


          ],
        ));
  }
  //
  // Container  picBlock(BuildContext context) {
  //   return Container(
  //       child: Row(
  //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //         crossAxisAlignment: CrossAxisAlignment.center,
  //     children: [
  //       // VerticalSpace().create(context, FigmaToScreen().getWidthRatio(5)),
  //       Container(
  //         width: 0.285.sw,
  //         child: imgUrlList[0]. contains("http")?
  //         AspectRatio(
  //           aspectRatio: 111/90,
  //           child: Stack(
  //             children: [
  //               Padding(
  //                 padding: const EdgeInsets.only(top:8.0,right: 8),
  //                 child: CachedNetworkImage(
  //                   imageUrl: imgUrlList[0],
  //                   fit: BoxFit.fill,
  //                   placeholder: (context, url) => AspectRatio(
  //                     aspectRatio: 1,
  //                     child: FractionallySizedBox(
  //                       heightFactor: 0.5,
  //                       widthFactor: 0.5,
  //                       child: SpinKitRing(
  //                         lineWidth:5,
  //                         color:
  //                         OwnColors.tran ,
  //                       ),
  //                     ),
  //                   ),
  //                   errorWidget: (context, url, error) => Icon(
  //                     Icons.error,
  //                     color: OwnColors.black.withOpacity(0.5),
  //                   ),
  //                   imageBuilder: (context, imageProvider) =>
  //                       Container(
  //                         width: 0.285.sw,
  //                         height: 0.11.sh,
  //                         decoration: BoxDecoration(
  //                           color: OwnColors.tran,
  //                           borderRadius: BorderRadius.all(
  //                               Radius.circular(14)),
  //                           image: DecorationImage(
  //                               image: imageProvider,
  //                               fit: BoxFit.cover),
  //                         ),
  //                       ),
  //                   // width: MediaQuery.of(context).size.width,
  //                 ),
  //               ),
  //               new Positioned.fill(child: GestureDetector(
  //                 onTap: (){
  //                   print("WWW");
  //                   showBottomMenu(0);
  //                 },
  //                 child: Container(
  //
  //                 ),
  //               )),
  //               Positioned(
  //                 top:0,
  //                 right: 0,
  //                 child: GestureDetector(
  //                   onTap: (){
  //                   // imgUrlList[0] = "";
  //                   // img64List[0] = Uint8List(0);
  //                   imgUrlList.removeAt(0);
  //                   img64List.removeAt(0);
  //                   imgUrlList.add("");
  //                   img64List.add(Uint8List(0));
  //                   setState(() {
  //
  //                   });
  //                 },
  //                   child: OrangeXCross().create(context),
  //                 ),
  //               )
  //             ],
  //           ),
  //         ):
  //
  //             img64List[0].length>0?
  //             AspectRatio(
  //               aspectRatio: 111/90,
  //               child: Stack(
  //                 children: [
  //                   Padding(
  //                     padding: const EdgeInsets.only(top:8.0,right: 8),
  //                     child: Stack(
  //                       children: [
  //                         Container(
  //                           width: 0.285.sw,
  //                           height: 0.11.sh,
  //                           decoration: BoxDecoration(
  //                             color: OwnColors.tran,
  //                             borderRadius: BorderRadius.all(
  //                                 Radius.circular(14)),
  //                             image: DecorationImage(
  //                                 image: Image.memory(img64List[0]).image,
  //                                 fit: BoxFit.cover),
  //                           ),
  //                         ),
  //                         new Positioned.fill(
  //                           child: Visibility(
  //                             visible: uploadLoading[0],
  //                             child:  SpinKitRing(
  //                               lineWidth: 3,
  //                               color: OwnColors.CTourOrange,
  //                               size: 0.285.sw/111*40,
  //                             ),
  //                           ),
  //                         )
  //                       ],
  //                     ),
  //                   ),
  //
  //                 ],
  //               ),
  //             ):
  //         GestureDetector(
  //
  //           onTap: (){
  //             showBottomMenu(0);
  //           },
  //           child: Padding(
  //             padding: EdgeInsets.only(top: 0.01.sh,right: 0.01.sh),
  //             child: AspectRatio(
  //               aspectRatio: 111/90,
  //               child: Container(
  //                 decoration: BoxDecoration(
  //                   color: OwnColors.CF7F7F7,
  //                   borderRadius: BorderRadius.all(Radius.circular(10)),
  //                 ),
  //                 child: Center(
  //                   child: Stack(
  //                       alignment: Alignment.topLeft,
  //                       children:[
  //                         Transform(
  //                           alignment: Alignment.center,
  //                           transform: Matrix4.rotationY(math.pi),
  //                           child: Padding(
  //                             padding: const EdgeInsets.only(right: 6),
  //                             child: Icon(SFSymbols.photo_fill, color: OwnColors.CE6E1E1,size: ScreenSize().getWidth(context)*0.15,),
  //                           ),
  //                         ),
  //                         Container(
  //                           height: 0.10 * ScreenSize().getWidth(context),
  //                           width: 0.10 * ScreenSize().getWidth(context),
  //                           child: Padding(
  //                             padding: const EdgeInsets.only(top:0.0,right: 20),
  //                             child:Stack(
  //                               children: [
  //                                 Image.asset(Res.add_fill),
  //                               ],
  //                             ),
  //                           ),
  //                         ),
  //                       ]
  //                   ),
  //                 ),
  //               ),
  //             ),
  //           ),
  //         ),
  //       ),
  //
  //
  //
  //
  //
  //       Container(
  //         width: 0.285.sw,
  //         child: imgUrlList[1]. contains("http")? AspectRatio(
  //           aspectRatio: 111/90,
  //           child: Stack(
  //             children: [
  //               Padding(
  //                 padding: EdgeInsets.only(top:0.01.sh,right: 0.01.sh),
  //                 child: CachedNetworkImage(
  //                   imageUrl: imgUrlList[1],
  //                   fit: BoxFit.fill,
  //                   placeholder: (context, url) => AspectRatio(
  //                     aspectRatio: 1,
  //                     child: FractionallySizedBox(
  //                       heightFactor: 0.5,
  //                       widthFactor: 0.5,
  //                       child: SpinKitRing(
  //                         lineWidth:5,
  //                         color:
  //                         OwnColors.tran ,
  //                       ),
  //                     ),
  //                   ),
  //                   errorWidget: (context, url, error) => Icon(
  //                     Icons.error,
  //                     color: OwnColors.black.withOpacity(0.5),
  //                   ),
  //                   imageBuilder: (context, imageProvider) =>
  //                       Container(
  //                         width: 0.285.sw,
  //                         height: 0.11.sh,
  //                         decoration: BoxDecoration(
  //                           color: OwnColors.tran,
  //                           borderRadius: BorderRadius.all(
  //                               Radius.circular(14)),
  //                           image: DecorationImage(
  //                               image: imageProvider,
  //                               fit: BoxFit.cover),
  //                         ),
  //                       ),
  //                   // width: MediaQuery.of(context).size.width,
  //                 ),
  //               ),
  //               new Positioned.fill(child: GestureDetector(
  //                 onTap: (){
  //                   showBottomMenu(1);
  //                 },
  //                 child: Container(
  //
  //                 ),
  //               )),
  //               Positioned(
  //                 top:0,
  //                 right: 0,
  //                 child: GestureDetector(
  //                   onTap: (){
  //                     imgUrlList.removeAt(1);
  //                     img64List.removeAt(1);
  //                     imgUrlList.add("");
  //                     img64List.add(Uint8List(0));
  //                     // img64List[1] = Uint8List(0);
  //                     setState(() {
  //
  //                     });
  //                   },
  //                   child: OrangeXCross().create(context),
  //                 ),
  //               )
  //             ],
  //           ),
  //         ):
  //         img64List[1].length>0?
  //         AspectRatio(
  //           aspectRatio: 111/90,
  //           child: Stack(
  //             children: [
  //               Padding(
  //                 padding: const EdgeInsets.only(top:8.0,right: 8),
  //                 child: Stack(
  //                   children: [
  //                     Container(
  //                       width: 0.285.sw,
  //                       height: 0.11.sh,
  //                       decoration: BoxDecoration(
  //                         color: OwnColors.tran,
  //                         borderRadius: BorderRadius.all(
  //                             Radius.circular(14)),
  //                         image: DecorationImage(
  //                             image: Image.memory(img64List[1]).image,
  //                             fit: BoxFit.cover),
  //                       ),
  //                     ),
  //                     new Positioned.fill(
  //                       child: Visibility(
  //                         visible: uploadLoading[1],
  //                         child:  SpinKitRing(
  //                           lineWidth: 3,
  //                           color: OwnColors.CTourOrange,
  //                           size: 0.285.sw/111*40,
  //                         ),
  //                       ),
  //                     )
  //                   ],
  //                 ),
  //               ),
  //             ],
  //           ),
  //         ):
  //
  //         GestureDetector(
  //
  //           onTap: (){
  //             showBottomMenu(1);
  //           },
  //           child: Padding(
  //             padding: const EdgeInsets.only(top:8.0,right: 8),
  //             child: AspectRatio(
  //               aspectRatio: 111/90,
  //               child: Container(
  //                 decoration: BoxDecoration(
  //                   color: OwnColors.CF7F7F7,
  //                   borderRadius: BorderRadius.all(Radius.circular(10)),
  //                 ),
  //                 child: Center(
  //                   child: Stack(
  //                       alignment: Alignment.topLeft,
  //                       children:[
  //                         Transform(
  //                           alignment: Alignment.center,
  //                           transform: Matrix4.rotationY(math.pi),
  //                           child: Padding(
  //                             padding: const EdgeInsets.only(right: 6),
  //                             child: Icon(SFSymbols.photo_fill, color: OwnColors.CE6E1E1,size: ScreenSize().getWidth(context)*0.15,),
  //                           ),
  //                         ),
  //                         Container(
  //                           height: 0.10 * ScreenSize().getWidth(context),
  //                           width: 0.10 * ScreenSize().getWidth(context),
  //                           child: Padding(
  //                             padding: const EdgeInsets.only(top:0.0,right: 20),
  //                             child:Stack(
  //                               children: [
  //                                 Image.asset(Res.add_fill),
  //                               ],
  //                             ),
  //                           ),
  //                         ),
  //                       ]
  //                   ),
  //                 ),
  //               ),
  //             ),
  //           ),
  //         ),
  //       ),
  //
  //       // VerticalSpace().create(context, FigmaToScreen().getWidthRatio(17)),
  //       Container(
  //         width: 0.285.sw,
  //         child: imgUrlList[2]. contains("http")? AspectRatio(
  //           aspectRatio: 111/90,
  //           child: Stack(
  //             children: [
  //               Padding(
  //                 padding: EdgeInsets.only(top:0.01.sh,right: 0.01.sh),
  //                 child: CachedNetworkImage(
  //                   imageUrl: imgUrlList[2],
  //                   fit: BoxFit.fill,
  //                   placeholder: (context, url) => AspectRatio(
  //                     aspectRatio: 1,
  //                     child: FractionallySizedBox(
  //                       heightFactor: 0.5,
  //                       widthFactor: 0.5,
  //                       child: SpinKitRing(
  //                         lineWidth:5,
  //                         color:
  //                         OwnColors.tran ,
  //                       ),
  //                     ),
  //                   ),
  //                   errorWidget: (context, url, error) => Icon(
  //                     Icons.error,
  //                     color: OwnColors.black.withOpacity(0.5),
  //                   ),
  //                   imageBuilder: (context, imageProvider) =>
  //                       Container(
  //                         width: 0.285.sw,
  //                         height: 0.11.sh,
  //                         decoration: BoxDecoration(
  //                           color: OwnColors.tran,
  //                           borderRadius: BorderRadius.all(
  //                               Radius.circular(14)),
  //                           image: DecorationImage(
  //                               image: imageProvider,
  //                               fit: BoxFit.cover),
  //                         ),
  //                       ),
  //                   // width: MediaQuery.of(context).size.width,
  //                 ),
  //               ),
  //               new Positioned.fill(child: GestureDetector(
  //                 onTap: (){
  //                   showBottomMenu(2);
  //                 },
  //                 child: Container(
  //
  //                 ),
  //               )),
  //               Positioned(
  //                 top:0,
  //                 right: 0,
  //                 child: GestureDetector(
  //                   onTap: (){
  //                     // imgUrlList[2] = "";
  //                     // img64List[2] = Uint8List(0);
  //                     imgUrlList.removeAt(2);
  //                     img64List.removeAt(2);
  //                     imgUrlList.add("");
  //                     img64List.add(Uint8List(0));
  //                     setState(() {
  //
  //                     });
  //                   },
  //                   child: OrangeXCross().create(context),
  //                 ),
  //               )
  //             ],
  //           ),
  //         ):
  //         img64List[2]  .length>=0?
  //         AspectRatio(
  //           aspectRatio: 111/90,
  //           child: Stack(
  //             children: [
  //               Padding(
  //                 padding: const EdgeInsets.only(top:8.0,right: 8),
  //                 child: Stack(
  //                   children: [
  //                     Container(
  //                       width: 0.285.sw,
  //                       height: 0.11.sh,
  //                       decoration: BoxDecoration(
  //                         color: OwnColors.tran,
  //                         borderRadius: BorderRadius.all(
  //                             Radius.circular(14)),
  //                         image: DecorationImage(
  //                             image: Image.memory(img64List[2]).image,
  //                             fit: BoxFit.cover),
  //                       ),
  //                     ),
  //                     new Positioned.fill(
  //                       child: Visibility(
  //                         visible: uploadLoading[2],
  //                         child:  SpinKitRing(
  //                           lineWidth: 3,
  //                           color: OwnColors.CTourOrange,
  //                           size: 0.285.sw/111*40,
  //                         ),
  //                       ),
  //                     )
  //                   ],
  //                 ),
  //               ),
  //
  //             ],
  //           ),
  //         ):
  //         GestureDetector(
  //
  //           onTap: (){
  //             showBottomMenu(2);
  //           },
  //           child: Padding(
  //             padding: const EdgeInsets.only(top:8.0,right: 8),
  //             child: AspectRatio(
  //               aspectRatio: 111/90,
  //               child: Container(
  //                 decoration: BoxDecoration(
  //                   color: OwnColors.CF7F7F7,
  //                   borderRadius: BorderRadius.all(Radius.circular(10)),
  //                 ),
  //                 child: Center(
  //                   child: Stack(
  //                       alignment: Alignment.topLeft,
  //                       children:[
  //                         Transform(
  //                           alignment: Alignment.center,
  //                           transform: Matrix4.rotationY(math.pi),
  //                           child: Padding(
  //                             padding: const EdgeInsets.only(right: 6),
  //                             child: Icon(SFSymbols.photo_fill, color: OwnColors.CE6E1E1,size: ScreenSize().getWidth(context)*0.15,),
  //                           ),
  //                         ),
  //                         Container(
  //                           height: 0.10 * ScreenSize().getWidth(context),
  //                           width: 0.10 * ScreenSize().getWidth(context),
  //                           child: Padding(
  //                             padding: const EdgeInsets.only(top:0.0,right: 20),
  //                             child:Stack(
  //                               children: [
  //                                 Image.asset(Res.add_fill),
  //                               ],
  //                             ),
  //                           ),
  //                         ),
  //                       ]
  //                   ),
  //                 ),
  //               ),
  //             ),
  //           ),
  //         ),
  //       ),
  //
  //     ],
  //   ));
  // }


  Container googleURLInput(BuildContext context) {
    return       Container(
      width: 0.92 * ScreenSize().getWidth(context),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        border:
        Border.all(color: OwnColors.C989898, width: 1),
      ),
      child: TextField(
        maxLines: 1, //or null
        controller: googleEdit,

        style:              GoogleFonts.inter(
          color: OwnColors.black,
          fontSize: 16.nsp,
          fontWeight: FontWeight.w500
      ),
        decoration: InputDecoration(
            // hintText: S().personal_my_travel_title_hint,
            fillColor: OwnColors.white,
            hintStyle: GoogleFonts.inter(
              color: OwnColors.C989898,
              fontSize: 16.nsp,
              fontWeight: FontWeight.w500
            ),

            hintText: "https://goo.gl/maps/example",//maps.aa,
            counterStyle:
            TextStyle(color: Colors.transparent),
            contentPadding: EdgeInsets.symmetric(
                horizontal: 10, vertical: 4),

            //Change this value to custom as you like
            isDense: true,
            filled: true,
            border: OutlineInputBorder(
                borderRadius:
                new BorderRadius.circular(20.0),
                borderSide: BorderSide.none)),
        onChanged: (text) {

        },
      ),
    );
  }

  GestureDetector showButtons(BuildContext context, String buttonTitle, num width,
      num height, Function setShow) {
    return GestureDetector(
      onTap: () {
        //increaseClick();
        print("setshow");
        setShow();
      },
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: OwnColors.white,
          border: Border.all(color: Color.fromARGB(255, 1, 1, 1), width: 1),
          borderRadius: BorderRadius.all(Radius.circular(17.5)),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.add,
              color: OwnColors.black,
            ),
            AutoSizeText(
              buttonTitle,
              style: GoogleFonts.inter(
                  fontSize: 14,
                  color: Color.fromARGB(255, 5, 5, 5),
                  fontWeight: FontWeight.w600),
            ),
          ],
        ),
      ),
    );
  }


  void showBottomMenu(int index) {
    Tools().dismissK(context);
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 150,
              child: Column(children: <Widget>[
                ListTile(
                  leading: Icon(Icons.photo_camera),
                  title: Text(S().take_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromCamera(index);
                  },
                ),
                ListTile(
                  leading: Icon(Icons.photo_library),
                  title: Text(S().choose_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromPhoto(index);
                  },
                )
              ]));
        });
  }

  final picker = ImagePicker();

  void pcikImageFromCamera(int index ) async {


    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);

    File img ;
    List<int> imageBytes;
    if(pickedFile.path.split(".").last == "png"){

      imageBytes = await FlutterImageCompress.compressWithFile(
          pickedFile.path,
          minWidth: 1440,
          minHeight: 1440,
          quality: 80,
        );
    }else {
      img = File(pickedFile.path);
      imageBytes = await img.readAsBytes();
    }

    // img22.Image capturedImage =
    // img22.decodeImage(await File(pickedFile.path).readAsBytes());
    // img22.Image orientedImage = img22.bakeOrientation(capturedImage);
    // img = await File(pickedFile.path)
    //     .writeAsBytes(img22.encodeJpg(orientedImage));
    // avatar = img.path;
    // avatarFileName = avatar
    //     .split("/")
    //     .last;
    img64List[index] = imageBytes;
    setState(() {

    });
    String base64Image = "data:image/jpg;base64," +base64Encode(imageBytes);
    print("base64Image: " + base64Image);

    _uploadImage(base64Image,index);

    setState(() {

    });


  }

  void pcikImageFromPhoto(int index) async {
// Get max 5 images
//     int count = img64List.where((c) => c.length<2).length;
    int count = imgUrlList.where((c) => !c.contains("http")).length;
    print("count " + count.toString());
    int index = 3-count;
    if(count == 0){
      
      WarningDialog.showIOSAlertDialog(context, S().travel_img_max_title, S().travel_img_max_hint, S().confirm);
    }else {
      // Setup image picker configs (global settings for app)
      List<Media> _listImagePaths = await ImagePickers.pickerPaths(
        galleryMode: GalleryMode.image,
        selectCount: count,
        showGif: false,
        showCamera: true,
        compressSize: 2500,
        uiConfig: UIConfig(uiThemeColor: OwnColors.white),
      );

      // List<PickedFile> pickedFileList = await picker.getMultiImage(
      //     // source: ImageSource.gallery,
      //     maxHeight: 1440,
      //     maxWidth: 1440,
      //     imageQuality: 80);
      print("_listImagePaths222");
      print(_listImagePaths.length.toString());
      Loading.show(context);
      if (_listImagePaths.length > 0) {
        for (Media pickedFile in _listImagePaths) {
          File img;
          List<int> imageBytes;
          img = File(pickedFile.path);
          imageBytes = await img.readAsBytes();
          print("imageBytes1 " + imageBytes.length.toString());
          print("imageBytes " + pickedFile.path
              .split(".")
              .last
              .toString());
          print("imageBytes " + pickedFile.path.toString());

          if (pickedFile.path
              .split(".")
              .last == "png") {
            imageBytes = await FlutterImageCompress.compressWithFile(
              pickedFile.path,
              minWidth: 1440,
              minHeight: 1440,
              quality: 80,
            );

            print("imageBytes 2" + imageBytes.length.toString());
          } else {
            img = File(pickedFile.path);
            imageBytes = await img.readAsBytes();
          }

          // File img = File(pickedFile.path);
          // List<int> imageBytes = await img.readAsBytes();
          // avatar = img.path;
          // avatarFileName = avatar
          //     .split("/")
          //     .last;
          img64List[index] = imageBytes;
          index = index + 1;
          setState(() {

          });
        }


        for (int i = 0; i < 3; i++) {
          if (!imgUrlList[i].contains("http")) {
            print("img64List[i] " + img64List[i].toString());
            if( img64List[i].toString().length>2) {
              String base64Image = "data:image/jpg;base64," +
                  base64Encode(img64List[i]);
              print("base64Image: " + base64Image);
              _uploadImage(base64Image, i);
            }
          }
        }
        setState(() {

        });
      }
    }
    Loading.dismiss(context);



  }

  Future<void> _uploadImage(String data,int index)  async {
    setState(() {
      uploadLoading[index] = true;
    });

    // Loading.show(context);

    ResultData resultData = await AppApi.getInstance().setFileBase64(
        context,
        true,
        data
    );

    if (resultData.isSuccess()) {
      // Loading.dismiss(context);
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data);
      String img_url = GDefine.imageUrl + resultData.data["path"];
      print("img_url " + img_url);
      imgUrlList[index] = img_url;
      // imgUrlList.add(img_url);
      setState(() {
        uploadLoading[index] = false;
      });
      setState(() {

      });
    } else {
      setState(() {
        uploadLoading[index] = false;

        img64List [index] = Uint8List(0);
      });
      //TODO
      // Loading.dismiss(context);
      // WarningDialog.showIOSAlertDialog(context, S().upload_fail_title, S().upload_fail_hint, S().confirm);
      WarningDialog.showIOSAlertDialog(context, S().upload_fail_title,resultData.data.toString(), S().confirm);

    }

  }



}
