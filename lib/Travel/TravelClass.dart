import 'dart:convert';

import 'package:flutter/widgets.dart';

class TravelClass {
  String id;
  String cover_image;
  String title;
  String duration;
  String date;
  String views;
  String comment_num;
  String type;
  String description;

  String tourist_id;
  String guilder_name;
  String guilder_rate;
  String guilder_comment;
  String guilder_avatar;
  String is_collect;
  List<SectionClass> sectionTag;
  List<dynamic> tag;

  List<TravelSubClass> contentList ;
  List<dynamic> contentListDynamic ;


  TravelClass({this.id,this.cover_image,this.type,this.tag,this.description,this.sectionTag,
    this.title, this.duration, this.date, this.views,this.comment_num,this.guilder_name,this.guilder_avatar,
    this.guilder_rate,this.guilder_comment,this.contentList,this.is_collect,this.tourist_id,this.contentListDynamic});


  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'cover_image': cover_image,
      'title': title,
      'duration': duration,
      'date': date,
      'views': views,
      'comment_num': comment_num,
      'type': type,
      'description': description,
      'tourist_id': tourist_id,

      'guilder_name': guilder_name,
      'guilder_rate': guilder_rate,
      'guilder_comment': guilder_comment,
      'guilder_avatar': guilder_avatar,
      'is_collect': is_collect,
      'tag': tag,
      'contentList': contentList,
      'contentListDynamic': contentListDynamic,
      'sectionTag': sectionTag,



    };
  }

  factory TravelClass.fromJson(Map<String, dynamic> json) => TravelClass(
    id: json["id"].toString(),
    cover_image: json["cover_image"].toString(),
    title: json["title"].toString(),
    duration: json["duration"].toString(),
    date: json["date"].toString() ,
    views: json["views"].toString() ,
    comment_num: json["comment_num"].toString() ,
    type: json["type"].toString() ,
    description: json["description"].toString(),
    tourist_id: json["tourist_id"].toString(),
    guilder_name: json["guilder_name"].toString(),
    guilder_rate: json["guilder_score"].toString()=="null"? "0.0": double.parse(json["guilder_score"].toString()).toStringAsFixed(1),
    guilder_comment: json["guilder_comment"] .toString()=="null"? "0.0": json["guilder_comment"].toString(),
    guilder_avatar: json["guilder_avatar"].toString() ,
    is_collect: json["is_collect"].toString() ,
    tag:  json["tag"].toString() == "null" ? []:json["tag"],
    // contentList:
    //
    // json["content_list"] ,

  );


}
class SectionClass {

  String title;
  GlobalKey key;

  SectionClass({this.title,this.key, });
}

class TravelSubClass {
  String id;
  String title;
  GlobalKey key;
  List<TravelContentClass> content;



  TravelSubClass({this.id,this.title,this.content,this.key });
}
class TravelContentClass {
  String id;
  String title;
  String description;
  String google;

  List<String> image;
  GlobalKey key;


  TravelContentClass({this.id,this.title,this.description,this.image,this.google });
}

class TravelArticleClass {
  String id;
  String title;
  String type;
  String description;
  String google;
  bool dragEnable;
  List<String> image;




  TravelArticleClass({this.id,this.title,this.type,this.description,this.image,this.google,this.dragEnable });
}