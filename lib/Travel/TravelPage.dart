import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/OrangeXCross.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Define/GlobalData.dart';
import 'package:ctour/Firebase/FirebaseDynamicLink.dart';
import 'package:ctour/GlobalTask.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Guild/GuildReviewPage.dart';
import 'package:ctour/Guild/GuilderPage.dart';
import 'package:ctour/Guild/GuilderTravelPage.dart';
import 'package:ctour/Home/HomeFilterPage.dart';
import 'package:ctour/ImgPage.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/ReportPage.dart';
import 'package:ctour/Travel/NewTravelPage.dart';
import 'package:ctour/Travel/TravelCommentClass.dart';
import 'package:ctour/Travel/TravelMenuPage.dart';
import 'package:ctour/Travel/TravelerPage.dart';
import 'package:ctour/tool/CropSample.dart';
import 'package:ctour/tool/CropTool.dart';
import 'package:ctour/tool/DateTools.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/StatusTools.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter_mentions/flutter_mentions.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart' as intl;
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icons.dart';

import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:rich_text_view/rich_text_view.dart' as rText;
import 'package:selectable_autolink_text/selectable_autolink_text.dart';
import 'package:sliver_tools/sliver_tools.dart';
import 'package:social_share/social_share.dart';
import 'package:url_launcher/url_launcher.dart';
import '../Network/API/app_api.dart';
import 'TravelClass.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/TopBar.dart';
import '../res.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_star/flutter_star.dart';
import 'package:flutter/material.dart';
import 'package:appinio_social_share/appinio_social_share.dart';

class TravelPage extends StatefulWidget {

  TravelClass travelClass;

  final bool edit;



  TravelPage(
      {Key key,
        this.travelClass,
        this.edit,


      })
      : super(key: key);

  @override
  State createState() {
    return _TravelPageState();
  }
}

class _TravelPageState extends State<TravelPage>with TickerProviderStateMixin {



  bool more  = false;
  TabController _tabFirstController;
  int currentFirstTab = 0;
  String imgBase64 = "";
  String imgExt = "";
  String imgPath = "";
  String imgUrl = "";
  Uint8List imgUnit8  ;
  TextEditingController commentEdit = new TextEditingController();

  List<TravelCommentClass> commentList = new List();

  double topShirkOffset = 0;
  double maxTop = 0.0;
  GlobalKey tabKey = new  GlobalKey();
  List<double> posList = [];
  List<int> scrollToList = [];

  GlobalKey commentKey = new  GlobalKey();

  String editCommentID = "-1";
  String editCreated = "";


  String filterValue = S().travel_comment_filter_hot;
  final ScrollController _controller = ScrollController();

  GlobalKey titleKey = new  GlobalKey();
  bool showTitle = false ;

  bool bottomVisible = false;

  String link = "";


  GlobalKey<FlutterMentionsState> mentionKey = GlobalKey<FlutterMentionsState>();

  List<Map<String,dynamic>> mentionList = new List();
  List<Map<String,dynamic>> allMentionList = new List();
  List<int> mentionID = new List();



  @override
  void initState() {
    super.initState();


    StatusTools().setTranBGWhiteText();
    print("toool " +  AppBar().preferredSize.height.toString());
    print(" widget.travelClass.contentList " +   widget.travelClass.contentList.length.toString());
    print(" widget.travelClass.contentList " +   widget.travelClass.contentList.first.content.toString());
    print(" widget.travelClass.sectionTag " +   widget.travelClass.sectionTag.toString());
    _tabFirstController = new TabController(vsync: this, length: widget.travelClass.sectionTag.length);
    setArticleViews();
    getArticleCommentList();

    getLink();
    GlobalData.curernt_travel = widget.travelClass;

    if( widget.travelClass.sectionTag.length!= 0  || widget.travelClass.contentList.length != 0){
      if(widget.travelClass.contentList.length != 0){
        if(widget.travelClass.contentList.first.content!= null){
          bottomVisible = true;
        }

      }
    }
    

  }

  Future<void> getLink() async {
    link = await FirebaseDynamicLink.createDynamicLinkArticle(widget.travelClass.id,widget.travelClass.cover_image,widget.travelClass.title,widget.travelClass.description);
    print("link : " + link);
  }

  @override
  void dispose() {
    super.dispose();
    StatusTools().setWhitBGDarkText();
  }

  @override
  Widget build(BuildContext context) {
    return Portal(
      child: Scaffold(
          backgroundColor: OwnColors.white,
          extendBodyBehindAppBar: true,

          body: GestureDetector(
            onTap: (){
              // Tools().dismissK(context);
              // FocusScopeNode currentFocus = FocusScope.of(context);
              FocusScope.of(context).unfocus();
              // if(!currentFocus.hasPrimaryFocus) {
              //   currentFocus.focusedChild.unfocus();
              // }
            },
              child: Stack(
            children: [
              Container(
                width: ScreenSize().getWidth(context) * 1.0,
                color: OwnColors.white,
                height: MediaQuery.of(context).size.height,
                child: Container(
                  width: ScreenSize().getWidth(context) * 1.0,
                  height: MediaQuery.of(context).size.height,
                  color: Colors.white,
                  child:
                  NotificationListener<ScrollUpdateNotification>(
                    onNotification: (notification) {
                      //How many pixels scrolled from pervious frame
                      // print("position " + notification.scrollDelta.toString());

                      //List scroll position
                      // print("position " + notification.metrics.pixels.toString());
                      // final targetContext = widget.travelClass.contentList[currentFirstTab].key.currentContext;

                      RenderBox box2 = tabKey.currentContext.findRenderObject() as RenderBox;
                      double topH = 0.07 .sh + box2.size.height + MediaQuery.of(context).padding.top;

                       posList = [];
                       scrollToList = [];
                      for (int ii = 0 ; ii < widget.travelClass.contentList.length ; ii++ ){
                        TravelSubClass t = widget.travelClass.contentList[ii];
                        final targetContext =  t.key.currentContext;
                        RenderBox box = targetContext.findRenderObject() as RenderBox;
                        var position = box.getTransformTo(null).getTranslation(); //this is global position
                        double y = position.y;
                        // print("ii " + ii .toString());
                        // print(" position.dy " + y .toString());
                        posList.add(position.y);

                      }

                      for (int i = 0; i<posList.length;i++){
                        if(posList[i]<= topH+30){
                          scrollToList.add(i);
                        }

                      }
                      // print("scrollToList " + scrollToList.toString());

                      if(scrollToList.isNotEmpty) {
                        _tabFirstController.animateTo(scrollToList.last);
                      }


                      RenderBox titleBox = titleKey.currentContext.findRenderObject() as RenderBox;
                      double topH2 = 0.07 .sh  + MediaQuery.of(context).padding.top;
                      // print ("titlebox " + titleBox.getTransformTo(null).getTranslation().y.toString());
                      if(titleBox.getTransformTo(null).getTranslation().y <= topH2) {
                        StatusTools().setWhitBGDarkText();
                        if(showTitle == false ){
                          setState(() {
                            showTitle = true;
                          });
                        }
                      }else {
                        StatusTools().setTranBGWhiteText();
                        if(showTitle == true ){
                          setState(() {
                            showTitle = false;
                          });
                        }
                      }


                    },
                      child: CustomScrollView(
                        controller: _controller,
                        physics: const BouncingScrollPhysics(),
                        slivers:[
                          SliverAppBar(
                            backgroundColor: OwnColors.white,
                            expandedHeight:1.sw / 16 * 9   - ScreenSize().getTopPad(context) ,
                            toolbarHeight:  AppBar().preferredSize.height,


                            shadowColor: OwnColors.white,
                            elevation: 0,
                            // title: Text(
                            //   "WWWWWWWWWWW"
                            // ),
                            // collapsedHeight:   0.07 * ScreenSize().getHeight(context),
                            centerTitle: true,
                            stretch: true,
                            pinned: true,
                            flexibleSpace:
                            LayoutBuilder(
                                builder: (BuildContext context, BoxConstraints constraints) {
                                  print("LayoutBuilder ") ;
                                  if(maxTop == 0){
                                    maxTop = constraints.biggest.height;
                                  }
                                  topShirkOffset = constraints.biggest.height;

                                  return Stack(
                                    children: [


                                      FlexibleSpaceBar(
                                        titlePadding:  EdgeInsetsDirectional.only(
                                  start: 0.04 .sh +  0.05.sw,
                                   end:0.04 .sh +  0.05.sw,
                                          bottom: 0
                                  ),
                                        // title: Text(
                                        //  widget.travelClass.title,
                                        //   maxLines: ( topShirkOffset ==(0.07 * ScreenSize().getHeight(context) + MediaQuery.of(context).padding.top  ))?1:5,
                                        //   overflow: TextOverflow.ellipsis,
                                        //   style: TextStyle(
                                        //   fontSize: 12.nsp,
                                        //     color: OwnColors.black
                                        //
                                        // ),
                                        // ),

                                        background: CachedNetworkImage(
                                          imageUrl: widget.travelClass.cover_image,
                                          fit: BoxFit.fitWidth,
                                          placeholder: (context, url) =>
                                              AspectRatio(
                                                aspectRatio: 1,
                                                child: FractionallySizedBox(
                                                  heightFactor: 0.5,
                                                  widthFactor: 0.5,
                                                  child: SpinKitRing(
                                                    lineWidth: 5,
                                                    color:
                                                    OwnColors.tran ,
                                                  ),
                                                ),
                                              ),
                                          errorWidget: (context, url, error) =>
                                              Icon(
                                                Icons.error,
                                                color: OwnColors.black.withOpacity(
                                                    0.5),
                                              ),
                                          imageBuilder: (context, imageProvider) =>
                                              Container(
                                                decoration: BoxDecoration(
                                                  color: OwnColors.tran,
                                                  image: DecorationImage(
                                                      image: imageProvider,
                                                      fit: BoxFit.cover),
                                                ),
                                              ),
                                          // width: MediaQuery.of(context).size.width,
                                        ),

                                      ),

                                      Positioned(
                                        child: Container(
                                          height: 0.04.sw  ,
                                          decoration: const BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.vertical(
                                              top: Radius.circular(40),
                                            ),
                                          ),
                                        ),
                                        bottom: -3,
                                        left: 0,
                                        right: 0,
                                      ),


                                    ],
                                  );
                                })

                          ),

                          SliverToBoxAdapter(
                            child: Column(
                              children: [

                                Container(
                                  width: double.infinity,

                                  decoration: BoxDecoration(
                                      color: OwnColors.white,
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(35)),border: Border.all(width: 0,color: OwnColors.white)),
                                  child: Center(
                                    child: Container(
                                      width: 0.92.sw,
                                      child: Column(
                                        children: [

                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: SelectableText(widget.travelClass.title,
                                              key: titleKey,
                                              style:GoogleFonts.inter (
                                                fontSize: 20.nsp,
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                          ),
                                          HorizontalSpace().create(context, 0.02),
                                          Container(
                                            width: 0.92 *
                                                ScreenSize().getWidth(context),
                                            child: Row(
                                              children: [
                                                GestureDetector(
                                                  onTap:() async {
                                                    GuildClass guildclass = await GlobalTask().getGuild(context, widget.travelClass.tourist_id);
                                                    if(guildclass!=null) {
                                                      PushNewScreen().normalPush(context,
                                                          GuilderPage(guildClass: guildclass, preview: false,));
                                                    }else {
                                                      PushNewScreen().normalPush(context,
                                                          GuilderPage(id:widget.travelClass.tourist_id, preview: false,));
                                                    }

                                                    // PushNewScreen().normalPush(context, GuilderPage(id: widget.travelClass.tourist_id,preview: false));
                                                  },
                                                  child: Container(
                                                    height: 0.05.sh,
                                                    width: 0.05.sh,
                                                    child: CachedNetworkImage(
                                                      imageUrl: widget.travelClass.guilder_avatar,
                                                      fit: BoxFit.fill,
                                                      placeholder: (context, url) => AspectRatio(
                                                        aspectRatio: 1,
                                                        child: FractionallySizedBox(
                                                          heightFactor: 0.5,
                                                          widthFactor: 0.5,
                                                          child: SpinKitRing(
                                                            lineWidth:5,
                                                            color:
                                                            OwnColors.tran ,
                                                          ),
                                                        ),
                                                      ),
                                                      errorWidget: (context, url, error) => Icon(
                                                        Icons.error,
                                                        color: OwnColors.black.withOpacity(0.5),
                                                      ),
                                                      imageBuilder: (context, imageProvider) =>
                                                          Container(
                                                            // width: 80.0,
                                                            // height: 80.0,
                                                            decoration: BoxDecoration(
                                                              color: OwnColors.tran,
                                                              shape: BoxShape.circle,
                                                              image: DecorationImage(
                                                                  image: imageProvider,
                                                                  fit: BoxFit.cover),
                                                            ),
                                                          ),
                                                      // width: MediaQuery.of(context).size.width,
                                                    ),
                                                  ),
                                                ),
                                                Container(width: 8,),
                                                Expanded(
                                                  child: GestureDetector(
                                                    onTap: () async {
                                                      // PushNewScreen().normalPush(context, GuilderPage(id: widget.travelClass.tourist_id,preview: false));
                                                      GuildClass guildclass = await GlobalTask().getGuild(context, widget.travelClass.tourist_id);
                                                      if(guildclass!=null) {
                                                        PushNewScreen().normalPush(context,
                                                            GuilderPage(guildClass: guildclass, preview: false,));
                                                      }else {
                                                        PushNewScreen().normalPush(context,
                                                            GuilderPage(id: widget.travelClass.tourist_id, preview: false,));
                                                      }
                                                    },
                                                    child:                Container(
                                                      height: 0.05.sh,
                                                      child: Column(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          Padding(
                                                            padding: const EdgeInsets.only(left: 3.0),
                                                            child: Text(
                                                              widget.travelClass.guilder_name,
                                                              maxLines: 1,
                                                              overflow: TextOverflow.ellipsis,
                                                              style: GoogleFonts.inter (

                                                                fontSize: 16.nsp,
                                                                height: 1.2,
                                                                color: OwnColors.black,
                                                                fontWeight: FontWeight.w500,
                                                                //fontStyle: FontStyle.normal
                                                              ),
                                                            ),
                                                          ),
                                                          HorizontalSpace().create(context, 0.001),
                                                          Row(
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            crossAxisAlignment: CrossAxisAlignment.end,
                                                            children: [
                                                              Padding(
                                                                padding: const EdgeInsets.only(left:0.0,right: 4,bottom: 2),
                                                                child: Container(
                                                                    height: 0.022.sh,
                                                                    child: Image.asset(Res.star)),
                                                              ),
                                                              Text(widget.travelClass.guilder_rate,
                                                                style:GoogleFonts.inter (
                                                                  fontSize: 16.nsp,
                                                                  height: 1.2,
                                                                  color: OwnColors.black,
                                                                  fontWeight: FontWeight.w600,
                                                                  //fontStyle: FontStyle.normal
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),

                                                GestureDetector(
                                                  onTap: (){
                                                    showArticleMenu();
                                                  },
                                                  child: Container(

                                                    decoration: BoxDecoration(
                                                      color: OwnColors.CFF644E,
                                                      // border: Border.all(color: OwnColors.black,width: 1),
                                                      borderRadius:
                                                      BorderRadius.all(Radius.circular(14)),),
                                                    child: Padding(
                                                      padding: const EdgeInsets.only(left:12.0,right: 12.0,top: 8,bottom: 8),
                                                      child: Row(
                                                        children: [
                                                          SizedBox(width: 0.04.sw,height: 0.014.sh,child: Image.asset(Res.list_triangle)),
                                                          Padding(
                                                            padding: const EdgeInsets.only(left:3.0),
                                                            child: Center(
                                                              child: Text(
                                                                  widget.travelClass.type == "1"?
                                                                  widget.travelClass.duration.toString() + S().home_filter_type2_string :
                                                                  widget.travelClass.type == "2"? S().home_filter_type3 :
                                                                  S(). home_filter_type4
                                                                  ,
                                                                  textAlign: TextAlign.center,
                                                                style:GoogleFonts.inter (
                                                                    fontSize: 12.nsp,
                                                                    height: 1.2,
                                                                    color: OwnColors.white,
                                                                    fontWeight: FontWeight.w600,
                                                                  ),
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),//作者
                                          HorizontalSpace().create(context, 0.01),
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: SelectableAutoLinkText(
                                              widget.travelClass.description,
                                              linkStyle: TextStyle(color: Colors.blueAccent,decoration: TextDecoration.underline,),
                                              onTap: (url) => launch(url, forceSafariVC: false),
                                              highlightedLinkStyle: TextStyle(
                                                color: Colors.blueAccent,
                                                backgroundColor: Colors.blueAccent.withAlpha(0x33),
                                              ),
                                              style:GoogleFonts.inter (
                                                fontSize: 16.nsp,
                                                color: OwnColors.black,
                                                fontWeight: FontWeight.w400,
                                              ),
                                              moreBool: false,

                                            ),
                                          ),
                                          HorizontalSpace().create(context, 0.015),

                                          Container(
                                            width: 0.92 * ScreenSize().getWidth(context),
                                            child: Align(
                                              alignment: Alignment.topLeft,
                                              child: Wrap(

                                                spacing: 16,
                                                runSpacing : 5.0,
                                                children: List.generate(
                                                  widget.travelClass.tag.length,

                                                      (i) {
                                                    return Container(

                                                        decoration: BoxDecoration(
                                                            color: OwnColors.white,
                                                            borderRadius: BorderRadius.only(
                                                                topLeft: Radius.circular(13),
                                                                topRight: Radius.circular(13),
                                                                bottomLeft: Radius.circular(13),
                                                                bottomRight: Radius.circular(13)),
                                                            border: Border.all(
                                                                width: 0.5,color: OwnColors.CC8C8C8
                                                            ),
                                                            boxShadow: [
                                                              BoxShadow(
                                                                color: OwnColors.primaryText.withOpacity(0.4),
                                                                spreadRadius: 1,
                                                                blurRadius: 4,
                                                                offset: Offset(0, 3), // changes position of shadow
                                                              ),
                                                            ]),
                                                        child: Padding(
                                                          padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                                          child: Text(widget.travelClass.tag[i],
                                                            textAlign: TextAlign.center,
                                                            style:GoogleFonts.inter (
                                                              height: 1.2,
                                                              fontSize: 14.nsp,
                                                              color: OwnColors.black,
                                                              fontWeight: FontWeight.w400,
                                                            ),

                                                          ),
                                                        ));
                                                  },
                                                ),
                                              ),
                                            ),
                                          ),
                                          HorizontalSpace().create(context, 0.02),
                                          Container(
                                            width: 0.92 * ScreenSize().getWidth(context),
                                            height: 0.03.sh,
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Expanded(
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      Padding(
                                                        padding: const EdgeInsets.only(bottom: 4.0,top: 4.0, right: 4),
                                                        child: Image.asset(Res.icon_people2)
                                                        // Icon(SFSymbols.person_2),
                                                      ),
                                                      Flexible(
                                                        child: Text(
                                                          widget.travelClass.views + " " +  (int.parse(widget.travelClass.views)>1? S().travel_views :  S().travel_views_single) + "",
                                                          maxLines: 1,
                                                          overflow: TextOverflow.ellipsis,
                                                          style:GoogleFonts.inter(
                                                            fontSize: 16.nsp,
                                                            height: 1.2,
                                                            color: OwnColors.black,
                                                            fontWeight: FontWeight.w500,
                                                          ),
                                                        ),
                                                      ),
                                                      GestureDetector(
                                                        onTap: (){
                                                          final targetContext = commentKey.currentContext;
                                                          if (targetContext != null) {
                                                            Scrollable.ensureVisible(
                                                              targetContext,
                                                              duration: const Duration(milliseconds: 400),
                                                              curve: Curves.easeInOut,
                                                            );
                                                          }
                                                        },
                                                        child: Padding(
                                                          padding: const EdgeInsets.only(
                                                              top: 4.0, bottom: 4.0, right: 4, left: 8),

                                                            child: Image.asset(Res.icon_comment2)

                                                        ),
                                                      ),
                                                      Flexible(
                                                        child: GestureDetector(
                                                          onTap: (){
                                                            final targetContext = commentKey.currentContext;
                                                            if (targetContext != null) {
                                                              Scrollable.ensureVisible(
                                                                targetContext,
                                                                duration: const Duration(milliseconds: 400),
                                                                curve: Curves.easeInOut,
                                                              );
                                                            }
                                                          },
                                                          child: Text(
                                                            widget.travelClass.comment_num +
                                                                " " +
                                                                (int.parse(widget.travelClass.comment_num) > 1 ? S().travel_comment : S().travel_comment_single),
                                                            maxLines: 1,
                                                            overflow: TextOverflow.ellipsis,

                                                            style:GoogleFonts.inter (
                                                              fontSize: 16.nsp,
                                                              height: 1.2,
                                                              color: OwnColors.black,
                                                              fontWeight: FontWeight.w500,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Text(
                                                  DateTools().intlYMD(widget.travelClass.date),
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis,
                                                  style:GoogleFonts.inter  (

                                                    fontSize: 12.nsp,
                                                    color: OwnColors.CCCCCCC,
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                          Container(height: 16,)




                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          SliverPinnedHeader(
                            child: Container(
                              key: tabKey,
                              color: OwnColors.white,
                              child: Column(
                                children: [
                                  Container(

                                    color: OwnColors.CF4F4F4,
                                    height: 1,
                                    width: 0.92 * ScreenSize().getWidth(context),
                                  ),
                                  // HorizontalSpace().create(context, 0.01),
                                  widget.travelClass.sectionTag.length!= 0 ? _tabFirst() : Container(width: 1,),
                                ],
                              ),
                            ),
                          ),
                          SliverToBoxAdapter(

                            child: Column(
                              children: [

                                Container(
                                  width: double.infinity,

                                  decoration: BoxDecoration(
                                      color: OwnColors.white,
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(35)),border: Border.all(width: 0,color: OwnColors.white)),
                                  child: Center(
                                    child: Container(
                                      width: 0.92.sw,
                                      child: Column(
                                        children: [

                                          ListView.builder(
                                              shrinkWrap: true,
                                              itemCount: widget.travelClass.contentList.length,
                                              scrollDirection: Axis.vertical,
                                              physics: const NeverScrollableScrollPhysics(),
                                              padding: const EdgeInsets.all(0.0),
                                              itemBuilder: (context, index) {

                                                // print ("index  " + index.toString() ) ;
                                                return

                                                  SectionWidget(
                                                  key: widget.travelClass.contentList[index].key,
                                                  travelSubClass: widget.travelClass.contentList[index],
                                                  index : index,
                                                );
                                              })   ,



                                          Visibility(
                                            visible: bottomVisible,
                                            child: Column(
                                              children: [
                                                // HorizontalSpace().create(context, 0.027),
                                                Container(height: 16,),
                                                Container(
                                                  color: OwnColors.CF4F4F4,
                                                  height: 1,
                                                  width: 0.92 * ScreenSize().getWidth(context),
                                                ),
                                                Container(height: 16,)
                                                // HorizontalSpace().create(context, 0.027),
                                              ],
                                            ),
                                          ),

                                          Row(
                                            children: [
                                              Text(
                                                S().travel_comment_title,
                                                key: commentKey,
                                                style:GoogleFonts.inter (
                                                      fontSize: 20.nsp,
                                                    color: OwnColors.black,
                                                    fontWeight: FontWeight.bold
                                                ),

                                              ),
                                              Spacer(),
                                              Expanded(
                                                child: Stack(
                                                  children: [
                                                    Row(
                                                      children: [
                                                        Spacer(),
                                                        Container(height: 0.021.sh,child: Image.asset(Res.filter_left)),
                                                        Container(width: 5,),
                                                        Text(
                                                          filterValue,
                                                          style:GoogleFonts.inter  (

                                                              fontSize: 14.nsp,
                                                              color: OwnColors.black
                                                          ),

                                                        ),


                                                      ],
                                                    ),
                                                    Positioned(
                                                      top: 0,
                                                      right: 0,
                                                      child: DropdownButton2(

                                                        dropdownPadding: EdgeInsets.symmetric(horizontal: 0.0),

                                                        offset: const Offset(-0, 20),
                                                        items:  [S().travel_comment_filter_hot,S().travel_comment_filter_new].map((String items) {
                                                          return DropdownMenuItem(
                                                            value: items,
                                                            child: Text(items,
                                                                maxLines: 1,
                                                                style: GoogleFonts.inter (

                                                                  fontSize: 14.nsp,
                                                                  color: filterValue == items? OwnColors.CTourOrange : Colors.black
                                                                )),
                                                          );
                                                        }).toList(),
                                                        underline: SizedBox(),
                                                        value: filterValue,

                                                        selectedItemBuilder: (BuildContext context) {
                                                          return [S().travel_comment_filter_hot,S().travel_comment_filter_new].map<Widget>((String item) {
                                                            return Text(item, textAlign: TextAlign.end,
                                                            style: GoogleFonts.inter (
                                                              color: OwnColors.tran,
                                                              fontWeight: FontWeight.w500,
                                                              fontSize: 16.nsp,
                                                            ),);
                                                          }).toList();
                                                        },
                                                        onChanged: (String newValue) {
                                                          filterValue = newValue;
                                                          sortComment();
                                                          setState(() {

                                                          });

                                                        },
                                                        dropdownDecoration: BoxDecoration(
                                                          borderRadius:
                                                          BorderRadius.all(Radius.circular(8)),
                                                        ),
                                                      ),
                                                    )

                                                  ],
                                                ),
                                              )
                                            ],
                                          ),

                                          ListView.builder(
                                              shrinkWrap: true,
                                              itemCount: commentList.length,
                                              scrollDirection: Axis.vertical,
                                              physics: const NeverScrollableScrollPhysics(),
                                              itemBuilder: (context, index) {
                                                return       commentCard(commentList[index],index);
                                              }),




                                          HorizontalSpace().create(context, 0.1),

                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )


                        ]
                      ),
                    ),

                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                child: Container(
                  height: 0.07  .sh,
                  child: Center(
                    child: Container(
                      width: 0.92 * ScreenSize().getWidth(context),
                      child: Row(
                        children: [
                          ClipOval(
                            child: Material(
                              color: Colors.white, // button color
                              child: GestureDetector(

                                child: SizedBox(
                                    width: 0.04 .sh,
                                    height:
                                        0.04 .sh,
                                    child: Padding(
                                      padding: const EdgeInsets.only(bottom: 0.0,left: 0.0,right: 0.0),
                                      // child: Center(child: Icon(Icons.chevron_left_rounded, color: OwnColors.black,size:  0.04.sh,)),
                                      // child: Center(child: Icon(SFSymbols.chevron_left, color: OwnColors.black,size:  0.03.sh,)),
                                      // child: Center(child: Icon(Ionicons.chevron_back_outline, color: OwnColors.black,size:  0.03.sh,)),
                                      child: Center(child: Icon(LineIcons.angleLeft, color: OwnColors.black,size:  0.03.sh,)),
                                    )),
                                onTap: () {
                                  backClick();
                                },
                              ),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left:8.0,right: 8),
                              child: Text(
                                widget.travelClass.title,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: GoogleFonts.inter(
                                  color: showTitle? OwnColors.black:
                                  OwnColors.tran,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 20.nsp,
                                ),
                              ),
                            ),
                          ),

                          ClipOval(
                            child: Material(
                              color: Colors.white, // button color
                              child: GestureDetector(

                                child: SizedBox(
                                    width: 0.043 .sh,
                                    height: 0.043 .sh,
                                    child: Padding(
                                      padding: const EdgeInsets.only(bottom: 4.0,left: 0,right: 0),
                                      child: Center(child: Icon(SFSymbols.square_arrow_up ,size: 0.03.sh,)),
                                    )),
                                onTap: () async {
                                  // SocialShare.shareOptions("Hello world");

                                  await FlutterShare.share(
                                      title: S().travel_share_title,
                                      text: widget.travelClass.title,
                                      linkUrl: link,
                                      chooserTitle: ''
                                  );

                                },
                              ),
                            ),
                          ),
                          Container(
                            width: 10,
                          ),
                          // ClipOval(
                          //   child: Material(
                          //     color: Colors.white, // button color
                          //     child: GestureDetector(
                          //
                          //       child: SizedBox(
                          //           width: 0.043 .sh,
                          //           height:
                          //               0.043.sh,
                          //           child: Center(
                          //             child: Padding(
                          //                 padding: const EdgeInsets.only(top:0.0, left: 0.0,right: 0.0),
                          //               child: widget.travelClass.is_collect == "0" ?
                          //                   Icon(SFSymbols.heart, color: OwnColors.black,size: 0.03.sh,):
                          //               Icon(SFSymbols.heart_fill, color: OwnColors.CTourOrange,size:  0.03.sh,)
                          //               // Image.asset(Res.black_heart):
                          //               // Image.asset(Res.collected_heart)
                          //             ),
                          //           )),
                          //       onTap: () {
                          //         sendArticalFavorite(widget.travelClass);
                          //       },
                          //     ),
                          //   ),
                          // ),
                          ClipOval(
                            child: Material(
                              color: Colors.white, // button color
                              child: GestureDetector(

                                child: SizedBox(
                                    width: 0.043 .sh,
                                    height:
                                    0.043.sh,
                                    child: Center(
                                      child: Padding(
                                          padding: const EdgeInsets.only(top:0.0, left: 4.0,right: 4.0),
                                          child:
                                              Image.asset(Res.icon_menu)
                                          // Icon(SFSymbols.menu, color: OwnColors.black,size: 0.03.sh,)
                                        // Image.asset(Res.black_heart):。
                                        // Image.asset(Res.collected_heart)
                                      ),
                                    )),
                                onTap: () {
                                 showMenu();
                                },
                              ),
                            ),
                          )

                        ],
                      ),
                    ),
                  ),
                ),
              ),

              Positioned(
                bottom: 0,
                child: Container(
                    width: ScreenSize().getWidth(context),
                    height:  0.097.sh,
                    decoration: BoxDecoration(
                      color: OwnColors.white,

                    ),
                    child: sendMess()),
              ),
              widget.edit ? Positioned(
                right: 0.06.sw,
                bottom:  0.097.sh + 0.016.sh,
                child: FloatingActionButton(
                  splashColor: OwnColors.tran,
                  focusElevation: 0,
                  focusColor: OwnColors.tran,
                  backgroundColor: OwnColors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Image.asset(Res.highlighter)
                    // child: Icon(SFSymbols.pencil, color: OwnColors.black, size: 28, )
                  ),
                  onPressed: () {
                      goEdit();
                  },
                ),
              ):Container(),


             ],
          ))),
    );
  }

  Widget _tabFirst() {

    return Container(
      width: 0.92.sw,

      color: OwnColors.white,
      child: DefaultTabController(
        length: widget.travelClass.contentList.length,
        child: TabBar(
          controller: _tabFirstController,
          isScrollable: widget.travelClass.contentList.length >3? true:false,
          indicatorColor: OwnColors.CTourOrange,
          labelColor: OwnColors.CTourOrange,
          unselectedLabelColor: OwnColors.C989898,
          unselectedLabelStyle: GoogleFonts.inter(
            fontWeight: FontWeight.w500,
            fontSize: 16.nsp,
            color: OwnColors.C989898,
        ),
          labelStyle: GoogleFonts.inter(
            fontWeight: FontWeight.w700,
            fontSize: 16.nsp,
            color: OwnColors.CTourOrange,
          ),
          // indicatorPadding: EdgeInsets.all(8.0),
          indicatorWeight: 2.0,

          onTap: (index) {
            currentFirstTab = index ;
            setState(() {

            });
            final targetContext = widget.travelClass.sectionTag[currentFirstTab].key.currentContext;
            if (targetContext != null) {
              Scrollable.ensureVisible(
                targetContext,
                duration: const Duration(milliseconds: 400),
                curve: Curves.easeInOut,
              );
            }

          },
          tabs:
            List.generate(
              widget.travelClass.sectionTag.length,
                  (i) {
                print(i);





                return  LayoutBuilder(
                    builder:(context, size) {
                      print("max " + size.maxWidth.toString());
                      //
                      //
                      // int maxLines = ((0.185.sh / 16.nsp) * 0.8).floor();
                      //
                      // maxLines = maxLines > 0 ? maxLines : 1;
                      // print ("maxLines " + maxLines.toString());
                      var span = TextSpan(
                        text: widget.travelClass.sectionTag[i].title,
                        style: TextStyle(fontSize: 16.nsp),

                      );
                      //
                      // // Use a textpainter to determine if it will exceed max lines
                      var tp = TextPainter(
                        maxLines: 2,
                        textAlign: TextAlign.left,
                        textDirection: TextDirection.ltr,
                        text: span,
                      );
                      // print("tp.size.width " + tp.size.width.toString());
                      // //
                      // // // trigger it to layout
                      tp.layout(maxWidth: 0.23.sw);
                      print("tp.size.width " + tp.size.width.toString());
                      print("0.23.sw " + 0.23.sw.toString());

                      // //
                      // // // whether the text overflowed or not
                      var exceeded = tp.didExceedMaxLines;
                      double halfWidth = 0.23.sw;
                      if(exceeded){
                        var tp = TextPainter(
                          maxLines: 2,
                          textAlign: TextAlign.left,
                          textDirection: TextDirection.ltr,
                          text: span,
                        );
                        tp.layout(minWidth: 0.23.sw);
                        print("tp2.size.width " + tp.size.width.toString());
                        halfWidth = tp.size.width / 2;
                      }

                      return  widget.travelClass.sectionTag.length >3  ? Container(
                        width:  0.23.sw,
                        child: Tab(child: Center(
                          child: Text(widget.travelClass.sectionTag[i].title ,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,

                            style:GoogleFonts.inter (

                              // fontSize: 16.nsp,
                              // fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),),
                      ) :
                      Container(
                        // width: 0.2.sw,
                        child: Tab(
                          child: Center(
                            child: Text(widget.travelClass.sectionTag[i] .title,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style:GoogleFonts.inter (

                                // fontSize: 16.nsp,
                                // fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      );
                    }
                );





              },
            ),
        ),
      ),
    );
  }
  Widget commentCard(TravelCommentClass travelCommentClass, int index) {
    return Container(
      width: 0.92 * ScreenSize().getWidth(context),
      // height: 0.154 *  ScreenSize().getHeight(context),

      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          Container(
            width: 0.92 * ScreenSize().getWidth(context),
            height: 0.05.sh,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      Row(
                        children: [
                          Container(
                            height: 0.05.sh,
                            child:    AspectRatio(
                              aspectRatio: 1,
                              child: CachedNetworkImage(
                                errorWidget:
                                    (context, url, error) =>
                                    Icon(
                                      Icons.error,
                                      color: Colors.red,
                                    ),
                                imageUrl: travelCommentClass.user_img,
                                fit: BoxFit.fitHeight,
                                imageBuilder:
                                    (context, imageProvider) =>
                                    Container(
                                      // width: 80.0,
                                      // height: 80.0,
                                      decoration: BoxDecoration(
                                        color: OwnColors.tran,
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            image: imageProvider,
                                            fit: BoxFit.cover),
                                      ),
                                    ),
                                  placeholder: (context, url) => AspectRatio(
                                    aspectRatio: 1,
                                    child: FractionallySizedBox(
                                      heightFactor: 0.5,
                                      widthFactor: 0.5,
                                      child: SpinKitRing(
                                        lineWidth:5,
                                        color:
                                        OwnColors.tran ,
                                      ),
                                    ),
                                  )
                              ),
                            )
                            ,
                          ),
                          VerticalSpace().create(context, 0.013),
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    travelCommentClass.name,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style:GoogleFonts.inter (

                                    fontSize: 16.nsp,
                                    height: 1.2,

                                    fontWeight: FontWeight.w700,
                                    color: OwnColors.black
                                  ),
                                ),
                                Text(
                                  travelCommentClass.created_at,
                                  style:GoogleFonts.inter (
                                      fontSize: 12.nsp,
                                      height: 1.2,
                                      fontWeight: FontWeight.w400,
                                      color: OwnColors.C989898
                                  ),
                                )

                              ],
                            ),
                          ),
                        ],
                      ),
                      new Positioned.fill(
                        child: InkWell(
                          onTap: () async {
                            // PushNewScreen().normalPush(context, GuilderPage(id: travelCommentClass.user_id,preview: false));
                            GuildClass guildclass = await GlobalTask().getGuild(context, travelCommentClass.user_id);
                            if(guildclass!=null) {
                              PushNewScreen().normalPush(context,
                                  GuilderPage(guildClass: guildclass, preview: false,));
                            }else {
                              PushNewScreen().normalPush(context,
                                  GuilderPage(id: travelCommentClass.user_id, preview: false,));
                            }
                          },
                          onLongPress: (){
                            // showMenuCommend(travelCommentClass);
                          },
                        ),
                      )
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    sendCommentFavorite(travelCommentClass);
                  },
                  child: Row(
                    children: [
                      ClipOval(
                        child: Material(
                          color: Colors.white, // button color
                          child: SizedBox(
                              width: 0.04.sh,
                              height:
                              0.04 .sh,
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                //child: Image.asset(Res.collected_heart),
                                child: travelCommentClass.is_liked.contains("1")? Image.asset(Res.collected_heart): Image.asset(Res.black_heart),
                              )),
                        ),
                      ),
                      Text(
                          travelCommentClass.liked_num,
                        style:TextStyle (
                          fontFamily : 'NotoSansTC',
                          fontSize: 16.nsp,
                          color: OwnColors.black,

                        ),
                      ),

                    ],
                  ),
                )
              ],

            ),
          ),
          GestureDetector(
            onLongPress: (){
              showMenuCommend(travelCommentClass);
            },
            behavior: HitTestBehavior.opaque,
            child: Column(
              children: [
                HorizontalSpace().create(context, 0.01),
                Row(
                  children: [
                    Container(
                      width: 0.05.sh,
                    ),
                    VerticalSpace().create(context, 0.013),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Visibility(
                            visible: travelCommentClass.content !="",
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child:
                              SelectableAutoLinkText(
                                travelCommentClass.content,
                                linkRegExpPattern: '(@[^\u0000-\u007F]+|@[\\w]+|${AutoLinkUtils.urlRegExpPattern})',
                                linkStyle: TextStyle(color: Colors.blueAccent ,),
                                // onTap: (url) => launch(url, forceSafariVC: false),
                                highlightedLinkStyle: TextStyle(
                                  color: Colors.blueAccent,
                                  backgroundColor: Colors.blueAccent.withAlpha(0x33),
                                ),
                                style:GoogleFonts.inter (
                                  fontSize: 16.nsp,
                                  color: OwnColors.black,
                                  fontWeight: FontWeight.w400,
                                ),
                                moreBool: false,

                              )

                              // Text(
                              //     travelCommentClass.content,
                              //   style:GoogleFonts.inter (
                              //
                              //     fontSize: 16.nsp,
                              //     fontWeight: FontWeight.w500,
                              //     color: OwnColors.black
                              //   ),
                              // ),
                            ),
                          ),
                          HorizontalSpace().create(context, 0.01),
                          travelCommentClass.img.contains("http")?
                          Column(
                            children: [
                              GestureDetector(
                                onTap: (){
                                  PushNewScreen().normalPush(context, ImgPage(url:travelCommentClass.img ,));
                                },
                                child: Container(
                                  // height: 0.205.sh,
                                  width: double.infinity,

                                  child: CachedNetworkImage(
                                    imageUrl: travelCommentClass.img,
                                    fit: BoxFit.fill,
                                    placeholder: (context, url) => AspectRatio(
                                      aspectRatio: 4/3,
                                      child: FractionallySizedBox(
                                        heightFactor: 0.5,
                                        widthFactor: 0.5,
                                        child: SpinKitRing(
                                          lineWidth:5,
                                          color:
                                          OwnColors.tran ,
                                        ),
                                      ),
                                    ),
                                    errorWidget: (context, url, error) => Icon(
                                      Icons.error,
                                      color: OwnColors.black.withOpacity(0.5),
                                    ),
                                    imageBuilder: (context, imageProvider) => AspectRatio(
                                      aspectRatio: 4/3,
                                      child: Container(

                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(15)),
                                          image: DecorationImage(
                                              image: imageProvider,

                                              fit: BoxFit.cover,
                                               ),
                                        ),
                                      ),
                                    ),




                                  ),
                                ),
                              ),
                              Container(height: 12,),
                            ],
                          ):Container(width: 0,),
                          Align(
                            alignment: Alignment.centerRight,
                            child: GestureDetector(onTap:(){
                              showMenuCommend(travelCommentClass);
                            },child: Container(width: 0.021.sh,height: 0.021.sh,child: Image.asset(Res.three_dots))),
                          )
                        ],
                      ),
                    ),
                  ],
                ),

                HorizontalSpace().create(context, 0.02),
                Container(
                  color: index ==commentList.length-1? OwnColors.tran:OwnColors.CF4F4F4,
                  height: 1,
                  width: 0.92 * ScreenSize().getWidth(context),
                ),
                HorizontalSpace().create(context, 0.02),
              ],
            ),
          ),


        ],
      ),
    );
  }


  Container sendMess() {
    return Container(
      decoration: BoxDecoration(color: OwnColors.tran,          border: Border(
        top: BorderSide(width: 1, color: OwnColors.CF4F4F4),

      ), ),
      child: Row(children: [

 
        Expanded(
          child: Container(
            height:  0.05.sh,
            decoration: BoxDecoration(
              // color: OwnColors.Gray,
              // border: Border.all(width: 1,color: OwnColors.C989898),
              borderRadius: BorderRadius.all(Radius.circular(80)),

            ),
            alignment: Alignment.center,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                maxHeight: 300.0,
              ),
              child:


              FlutterMentions(
                key: mentionKey,
                suggestionPosition: SuggestionPosition.Top,

                // maxLines: 5,
                leading: [
                  VerticalSpace().create(context, 0.032),
                  Container(
                    height: 0.05.sh,
                    child:    AspectRatio(
                      aspectRatio: 1,
                      child: CachedNetworkImage(
                          errorWidget:
                              (context, url, error) =>
                              Icon(
                                Icons.error,
                                color: Colors.red,
                              ),
                          imageUrl: SharePre.prefs.getString(GDefine.avatar),
                          fit: BoxFit.fitHeight,
                          imageBuilder:
                              (context, imageProvider) =>
                              Container(
                                // width: 80.0,
                                // height: 80.0,
                                decoration: BoxDecoration(
                                  color: OwnColors.tran,
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.cover),
                                ),
                              ),
                          placeholder: (context, url) => AspectRatio(
                            aspectRatio: 1,
                            child: FractionallySizedBox(
                              heightFactor: 0.5,
                              widthFactor: 0.5,
                              child: SpinKitRing(
                                lineWidth:5,
                                color:
                                OwnColors.tran ,
                              ),
                            ),
                          )
                      ),
                    )
                    ,
                  ),
                  VerticalSpace().create(context, 0.02),
                ],
                  trailing: [
                    VerticalSpace().create(context, 0.02),
                    Container(
                      height: 0.05.sh,
                      child: AspectRatio(
                        aspectRatio: 1,
                        child:
                        imgUnit8==null && imgUrl == "" ?
                        Material(
                          color: Colors.transparent, // button color
                          child: GestureDetector(

                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: SizedBox(
                                  width: 0.03.sh,  height: 0.02.sh, child: Icon(SFSymbols.photo, size: 26,)),
                            ),
                            onTap: () {
                              showBottomMenuVideo();
                            },
                          ),
                        ): Stack(
                          children: [
                            GestureDetector(onTap:(){
                              showBottomMenuVideo();
                            },child: Padding(
                              padding: EdgeInsets.only(top:0.007.sh,right:0.007.sh),
                              child: AspectRatio(
                                  aspectRatio: 4/3,
                                  child:ClipRRect(
                                    borderRadius: BorderRadius.circular(5), // Image border
                                    child: SizedBox.fromSize(
                                      size: Size.fromRadius(5), // Image radius
                                      child:
                                      imgUnit8 != null?
                                      Image.memory(imgUnit8,fit: BoxFit.cover,):
                                      CachedNetworkImage(
                                        imageUrl: imgUrl,
                                        fit: BoxFit.fitWidth,

                                        placeholder: (context, url) => AspectRatio(
                                          aspectRatio: 1,
                                          child: FractionallySizedBox(
                                            heightFactor: 0.5,
                                            widthFactor: 0.5,
                                            child: SpinKitRing(
                                              lineWidth:5,
                                              color:
                                              OwnColors.tran ,
                                            ),
                                          ),
                                        ),
                                        errorWidget: (context, url, error) =>
                                            Icon(
                                              Icons.error,
                                              color: OwnColors.black.withOpacity(0.5),
                                            ),

                                      )


                                      ,
                                    ),
                                  )
                              ),
                            )),

                            Positioned(
                              top:0,
                              right: 0,
                              child: GestureDetector(
                                onTap: (){
                                  imgUnit8 = null;
                                  imgUrl = "";
                                  setState(() {

                                  });
                                },
                                child: OrangeXCross().createWH(context,0.018.sh),
                              ),
                            )

                          ],
                        ),
                      ),
                    ),
                    VerticalSpace().create(context, 0.01),
                    ClipOval(
                      child: Material(
                        color: Colors.transparent, // button color
                        child: GestureDetector(

                          child: SizedBox(
                              width:  0.05.sh,
                              height:  0.05.sh,
                              child: Icon(SFSymbols.arrow_up_circle_fill, color: (imgUrl.length > 0 || commentEdit.text.toString().length > 0)?OwnColors.CTourOrange:OwnColors.CC8C8C8,size: 33,)
                          ),
                          onTap: () {
                            if((imgUrl.length > 0 ||

                                commentEdit.text.toString().length > 0)){
                              sendComment();
                            }
                          },
                        ),
                      ),
                    ),
                    VerticalSpace().create(context, 0.032),
                  ],
                  // suggestionListDecoration :
                  //     BoxDecoration(
                  //       color: Colors.white,
                  //       border: Border(
                  //         top: BorderSide(width: 1.0, color: OwnColors.ironGray),
                  //         bottom: BorderSide(width: 1.0, color:OwnColors.ironGray),
                  //         left:BorderSide(width: 1.0, color: OwnColors.ironGray),
                  //         right: BorderSide(width: 1.0, color: OwnColors.ironGray),
                  //       ),
                  //     ),
                scrollPadding : const EdgeInsets.all(0.0),
                decoration: InputDecoration(
                    fillColor: OwnColors.white,
                    contentPadding: EdgeInsets.symmetric(horizontal: 15),
                    //Change this value to custom as you like
                    // isDense: true,
                    filled: true,

                    hintText: S().travel_comment_edit_hint,
                    hintStyle: TextStyle (
                        fontFamily : 'NotoSansTC',
                        color: OwnColors.C989898, fontSize: 15.nsp),
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(20.0),

                        borderSide: BorderSide(width: 1, color: OwnColors.C989898))
                ),
                style: TextStyle (
                    fontFamily : 'NotoSansTC',
                    color: Colors.black.withOpacity(1), fontSize: 15.nsp),
                maxLines: null,
                expands: true,
                keyboardType: TextInputType.multiline,
                onMarkupChanged: (value){
                  print(value);
                },
                onSearchChanged: (tri,val){
                  // print(tri);
                  print("search : " + val);

                  searchUser(val);
                },
                onMentionAdd: (v){
                  print("vv " + mentionKey.currentState.controller.markupText.toString());
                  allMentionList.add(v);

                  print("allMentionList " +allMentionList.toString());

                },

                onChanged: (v){
                  commentEdit.text = mentionKey.currentState.controller.text;
                  print("commentEdit.text " +commentEdit.text.toString());

                },

                mentions: [
                  Mention(

                      trigger: '@',

                      style: TextStyle(
                        color: Colors.blueAccent,
                      ),
                      data:mentionList,


                      matchAll: false,
                      suggestionBuilder: (data) {
                        return Container(
                          padding: EdgeInsets.only(top: 10.0,bottom: 10),
                          decoration: BoxDecoration(
                            // border: Border(
                            //   top: BorderSide(width: 1.0, color: OwnColors.ironGray),
                            //   bottom: BorderSide(width: 0.5, color: OwnColors.ironGray),
                            //
                            // ),
                            color: Colors.white,
                          ),
                          child: Row(
                            children: <Widget>[
                              VerticalSpace().create(context, 0.032),
                              CircleAvatar(
                                backgroundImage: NetworkImage(
                                  data['avatar'],
                                ),
                              ),
                              VerticalSpace().create(context, 0.02),
                              Column(
                                children: <Widget>[
                                  Text(data['name'],
                                  maxLines: 1,
                                    style: TextStyle (
                                        fontFamily : 'NotoSansTC',
                                        color: Colors.black.withOpacity(1), fontSize: 15.nsp),)
                                ],
                              )
                            ],
                          ),
                        );
                      }),
                  Mention(
                    trigger: '@',
                    data: allMentionList,  // Just make sure your data items are present here to identify mentioned item.
                    matchAll: false,
                    style: TextStyle(
                      color: Colors.blueAccent,
                    ),
                    // suggestionBuilder: _buildSuggessionBuilderUI,
                    markupBuilder: (trigger, mention, value) {
                      return '[mentionId:$mention]<end>456';
                    },
                  ),
                 ],
              ),



              // TextField(
              //   autofocus: false,
              //   controller: commentEdit,
              //   decoration: InputDecoration(
              //       fillColor: OwnColors.white,
              //       contentPadding: EdgeInsets.symmetric(horizontal: 15),
              //       //Change this value to custom as you like
              //       // isDense: true,
              //       filled: true,
              //       hintText: S().travel_comment_edit_hint,
              //       hintStyle: TextStyle (
              //       fontFamily : 'NotoSansTC',
              //           color: OwnColors.C989898, fontSize: 15.nsp),
              //       border: OutlineInputBorder(
              //           borderRadius: new BorderRadius.circular(20.0),
              //           borderSide: BorderSide.none)),
              //   style: TextStyle (
              //     fontFamily : 'NotoSansTC',
              //       color: Colors.black.withOpacity(1), fontSize: 15.nsp),
              //   maxLines: null,
              //   expands: true,
              //   keyboardType: TextInputType.multiline,
              //   obscureText: false,
              //   onChanged: (S) {
              //     setState(() {
              //
              //     });
              //   },
              // ),
            ),
          ),
        ),

      ]),
    );
  }

  Future<void> showArticleMenu() async {
    Tools().dismissK(context);
    GlobalKey a = await  showCupertinoModalBottomSheet(
      barrierColor:Colors.black54,
      topRadius: Radius.circular(30),
      expand: false,
      context: context,
      enableDrag: false,
      backgroundColor: Colors.transparent,
      builder: (context) => TravelMenuPage(travelClass: widget.travelClass,),
    ).whenComplete(() {

    });

    if(a!=null){

      final targetContext = a.currentContext;


      Future.delayed(Duration(milliseconds: 500), () {
        Scrollable.ensureVisible(
          targetContext,
          duration: const Duration(milliseconds: 400),
          curve: Curves.easeInOut,
        );
      });

    }

  }


  void showBottomMenuVideo() {
    Tools().dismissK(context);
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 150,
              child: Column(children: <Widget>[
                ListTile(
                  leading: Icon(Icons.photo_camera),
                  title: Text(S().take_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromCamera();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.photo_library),
                  title: Text(S().choose_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromPhoto();
                  },
                )
              ]));
        });
  }

  final picker = ImagePicker();

  void pcikImageFromCamera( ) async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);

    File img = File(pickedFile.path);
    Uint8List imageBytes = await pickedFile.readAsBytes();

    CroppedFile  croppedFile = await CropTool().crop(4, 3, CropAspectRatioPreset.ratio4x3, false, img.path);

    if(croppedFile != null) {
      imageBytes = await croppedFile.readAsBytes();
      imgPath = img.path;
      imgUnit8 = imageBytes;
      imgBase64 = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + imgBase64);

      _uploadImage(imgBase64);
      setState(() {

      });
    }

  }

  Future<void> showMenu() async {
    Tools().dismissK(context);
    int result = await showCupertinoModalBottomSheet(

        barrierColor:Colors.black54,
      topRadius: Radius.circular(30),
      expand: false,
      context: context,
      enableDrag: true,
      backgroundColor: Colors.transparent,
      builder: (context) => GestureDetector(
        onTap:(){
          Tools().dismissK(context);
          print("onTap");
        },
        child: Material(
            color: OwnColors.white,
            child: Container(
              // height:  ScreenSize().getHeightWTop(context),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[

                  Container(
                      height: 0.007 * ScreenSize().getHeight(context),
                      width: 0.2 * ScreenSize().getWidth(context),
                      decoration: const BoxDecoration(
                        color: OwnColors.white,
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                      )),
                  HorizontalSpace().create(context, 0.01),
                  Visibility(
                    visible: widget.travelClass.tourist_id == SharePre.prefs.getString(GDefine.user_id),
                    child: Column(
                      children: [
                        Stack(
                          children: [
                            Container(
                              width: 0.88.sw,
                              child: Row(
                                children: [
                                  SizedBox(
                                      width: 0.043 .sh,
                                      height:
                                      0.043.sh,
                                      child: Center(
                                        child: Padding(
                                            padding: const EdgeInsets.only(top:0.0, left: 0.0,right: 0.0),
                                            child: Image.asset(Res.highlighter,height: 0.03.sh,)
                                            // Icon(SFSymbols., color: OwnColors.CTourOrange,size:  0.03.sh,)
                                          // Image.asset(Res.black_heart):
                                          // Image.asset(Res.collected_heart)
                                        ),
                                      )),
                                  VerticalSpace().create(context, 0.01),
                                  Text(
                                    S().edit,
                                    style: GoogleFonts.inter(
                                      fontSize: 16.nsp,
                                      color: OwnColors.black,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            new Positioned.fill(child: InkWell(
                              splashColor: OwnColors.transparent,
                              onTap: (){
                                goEdit();


                              },
                            ))
                          ],
                        ),
                        HorizontalSpace().create(context, 0.02),
                      ],
                    ),
                  ),
                  Stack(
                    children: [
                      Container(
                        width: 0.88.sw,
                        child: Row(
                          children: [
                            SizedBox(
                                width: 0.043 .sh,
                                height:
                                0.043.sh,
                                child: Center(
                                  child: Padding(
                                      padding: const EdgeInsets.only(top:0.0, left: 0.0,right: 0.0),
                                      child: widget.travelClass.is_collect == "0" ?
                                      Icon(SFSymbols.heart, color: OwnColors.black,size: 0.03.sh,):
                                      Icon(SFSymbols.heart_fill, color: OwnColors.CTourOrange,size:  0.03.sh,)
                                    // Image.asset(Res.black_heart):
                                    // Image.asset(Res.collected_heart)
                                  ),
                                )),
                            VerticalSpace().create(context, 0.01),
                            Text(
                              S().collect,
                              style: GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.black,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Positioned.fill(child: InkWell(
                        splashColor: OwnColors.transparent,
                        onTap: (){
                          sendArticalFavorite(widget.travelClass);
                          backClick();
                        },
                      ))
                    ],
                  ),
                  HorizontalSpace().create(context, 0.02),
                  Stack(
                    children: [
                      Container(
                        width: 0.88.sw,
                        child: Row(
                          children: [

                            SizedBox(
                                width: 0.043 .sh,
                                height: 0.043 .sh,
                                child: Padding(
                                  padding: const EdgeInsets.only(bottom: 0.0,left: 0,right: 0),
                                  child: Center(child: Icon(SFSymbols.square_arrow_up ,size: 0.03.sh,)),
                                )),
                            VerticalSpace().create(context, 0.01),
                            Text(
                              S().share,
                              style: GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.black,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Positioned.fill(child: InkWell(
                        splashColor: OwnColors.transparent,
                        onTap: () async {
                          await FlutterShare.share(
                              title: S().travel_share_title,
                              text: widget.travelClass.title,
                              linkUrl: link,
                              chooserTitle: ''
                          );
                          backClick();
                        },
                      ))
                    ],
                  ),
                  HorizontalSpace().create(context, 0.02),
                  Stack(
                    children: [
                      Container(
                        width: 0.88.sw,
                        child: Row(
                          children: [

                            SizedBox(
                                width: 0.043 .sh,
                                height: 0.043 .sh,
                                child: Padding(
                                  padding: const EdgeInsets.only(bottom: 0.0,left: 0,right: 0),
                                  child: Center(child:SizedBox(height: 0.03.sh,child: Image.asset(Res.icon_report))),
                                )),
                            VerticalSpace().create(context, 0.01),
                            Text(
                              S().report,
                              style: GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.CFE3040,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Positioned.fill(child: InkWell(
                        splashColor: OwnColors.transparent,
                        onTap: () {
                          print("report");
                          backClick();
                        PushNewScreen().normalPush(context,ReportPage(type: 2,) );

                        },
                      ))
                    ],
                  ),
                  HorizontalSpace().create(context, 0.02),
                  Stack(
                    children: [
                      Container(
                        width: 0.88.sw,
                        child: Row(
                          children: [

                            SizedBox(
                                width: 0.043 .sh,
                                height: 0.043 .sh,
                                child: Padding(
                                  padding: const EdgeInsets.only(bottom: 0.0,left: 0,right: 0),
                                  child: Center(child:SizedBox(height: 0.03.sh,child: Image.asset(Res.icon_block))),
                                )),
                            VerticalSpace().create(context, 0.01),
                            Text(
                              S().block,
                              style: GoogleFonts.inter(
                                fontSize: 16.nsp,
                                color: OwnColors.CFE3040,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Positioned.fill(child: InkWell(
                        splashColor: OwnColors.transparent,
                        onTap: () {
                          print("block");
                          sendBlockWarning();
                          // backClick();
                          // PushNewScreen().normalPush(context,ReportPage(type: 1,) );

                        },
                      ))
                    ],
                  ),
                  HorizontalSpace().create(context, 0.04),

                ],
              ),
            )),
      )
    ).whenComplete(() {});


  }

  TravelCommentClass travelCommentClassBlock;
  Future<void> showMenuCommend(TravelCommentClass travelCommentClass) async {
    Tools().dismissK(context);
    travelCommentClassBlock = travelCommentClass;
    int result = await showCupertinoModalBottomSheet(
        barrierColor:Colors.black54,
        topRadius: Radius.circular(30),
        expand: false,
        context: context,
        enableDrag: true,
        backgroundColor: Colors.transparent,
        builder: (context) => GestureDetector(
          onTap:(){
            Tools().dismissK(context);
            print("onTap");
          },
          child: Material(
              color: OwnColors.white,
              child: Container(
                // height:  ScreenSize().getHeightWTop(context),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[

                    Container(
                        height: 0.007 * ScreenSize().getHeight(context),
                        width: 0.2 * ScreenSize().getWidth(context),
                        decoration: const BoxDecoration(
                          color: OwnColors.white,
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                        )),
                    HorizontalSpace().create(context, 0.01),
                    Stack(
                      children: [
                        Container(
                          width: 0.88.sw,
                          child: Row(
                            children: [
                              SizedBox(
                                  width: 0.043 .sh,
                                  height:
                                  0.043.sh,
                                  child: Center(
                                    child: Padding(
                                        padding: const EdgeInsets.only(top:0.0, left: 0.0,right: 0.0),
                                        child: travelCommentClass.is_liked.contains("0")?
                                        Icon(SFSymbols.heart, color: OwnColors.black,size: 0.03.sh,):
                                        Icon(SFSymbols.heart_fill, color: OwnColors.CTourOrange,size:  0.03.sh,)
                                      // Image.asset(Res.black_heart):
                                      // Image.asset(Res.collected_heart)
                                    ),
                                  )),
                              VerticalSpace().create(context, 0.01),
                              Text(
                                S().like,
                                style: GoogleFonts.inter(
                                  fontSize: 16.nsp,
                                  color: OwnColors.black,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Positioned.fill(child: InkWell(
                          splashColor: OwnColors.transparent,
                          onTap: (){
                            sendCommentFavorite(travelCommentClass);
                            backClick();
                          },
                        ))
                      ],
                    ),
                    HorizontalSpace().create(context, 0.02),

                    Visibility(
                      visible: travelCommentClass.user_id == SharePre.prefs.getString(GDefine.user_id),
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Container(
                                width: 0.88.sw,
                                child: Row(
                                  children: [
                                    SizedBox(
                                        width: 0.043 .sh,
                                        height:
                                        0.043.sh,
                                        child: Center(
                                          child: Padding(
                                              padding: const EdgeInsets.only(top:0.0, left: 0.0,right: 0.0),
                                              child: Image.asset(Res.highlighter,height: 0.03.sh,)
                                            // Icon(SFSymbols., color: OwnColors.CTourOrange,size:  0.03.sh,)
                                            // Image.asset(Res.black_heart):
                                            // Image.asset(Res.collected_heart)
                                          ),
                                        )),
                                    VerticalSpace().create(context, 0.01),
                                    Text(
                                      S().edit,
                                      style: GoogleFonts.inter(
                                        fontSize: 16.nsp,
                                        color: OwnColors.black,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              new Positioned.fill(child: InkWell(
                                splashColor: OwnColors.transparent,
                                onTap: (){
                                  // goEdit();
                                  goEditComment(travelCommentClass);


                                },
                              ))
                            ],
                          ),
                          HorizontalSpace().create(context, 0.02),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: travelCommentClass.user_id == SharePre.prefs.getString(GDefine.user_id),
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Container(
                                width: 0.88.sw,
                                child: Row(
                                  children: [
                                    SizedBox(
                                        width: 0.043 .sh,
                                        height:
                                        0.043.sh,
                                        child: Center(
                                          child: Padding(
                                              padding: const EdgeInsets.only(top:0.0, left: 0.0,right: 0.0),
                                              child: Image.asset(Res.orange_trash,height: 0.03.sh,)
                                            // Icon(SFSymbols., color: OwnColors.CTourOrange,size:  0.03.sh,)
                                            // Image.asset(Res.black_heart):
                                            // Image.asset(Res.collected_heart)
                                          ),
                                        )),
                                    VerticalSpace().create(context, 0.01),
                                    Text(
                                      S().delete,
                                      style: GoogleFonts.inter(
                                        fontSize: 16.nsp,
                                        color: OwnColors.CFE3040,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              new Positioned.fill(child: InkWell(
                                splashColor: OwnColors.transparent,
                                onTap: (){

                                  goDelComment(travelCommentClass);

                                },
                              ))
                            ],
                          ),
                          HorizontalSpace().create(context, 0.02),
                        ],
                      ),
                    ),

                    Stack(
                      children: [
                        Container(
                          width: 0.88.sw,
                          child: Row(
                            children: [

                              SizedBox(
                                  width: 0.043 .sh,
                                  height: 0.043 .sh,
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 0.0,left: 0,right: 0),
                                    child: Center(child:SizedBox(height: 0.03.sh,child: Image.asset(Res.icon_report))),
                                  )),
                              VerticalSpace().create(context, 0.01),
                              Text(
                                S().report,
                                style: GoogleFonts.inter(
                                  fontSize: 16.nsp,
                                  color: OwnColors.CFE3040,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Positioned.fill(child: InkWell(
                          splashColor: OwnColors.transparent,
                          onTap: () {
                            print("report");
                            backClick();
                            PushNewScreen().normalPush(context,ReportPage(type: 3,) );

                          },
                        ))
                      ],
                    ),
                    HorizontalSpace().create(context, 0.02),
                    Stack(
                      children: [
                        Container(
                          width: 0.88.sw,
                          child: Row(
                            children: [

                              SizedBox(
                                  width: 0.043 .sh,
                                  height: 0.043 .sh,
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 0.0,left: 0,right: 0),
                                    child: Center(child:SizedBox(height: 0.03.sh,child: Image.asset(Res.icon_block))),
                                  )),
                              VerticalSpace().create(context, 0.01),
                              Text(
                                S().block,
                                style: GoogleFonts.inter(
                                  fontSize: 16.nsp,
                                  color: OwnColors.CFE3040,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Positioned.fill(child: InkWell(
                          splashColor: OwnColors.transparent,
                          onTap: () {
                            print("block");
                            sendBlockWarning2();
                            // backClick();
                            // PushNewScreen().normalPush(context,ReportPage(type: 1,) );

                          },
                        ))
                      ],
                    ),

                    HorizontalSpace().create(context, 0.04),

                  ],
                ),
              )),
        )
    ).whenComplete(() {});


  }


  void pcikImageFromPhoto() async {


    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);
    File img = File(pickedFile.path);
    print (img.path);

    // SocialShare.shareInstagramStory(imagePath: img.path );
    // AppinioSocialShare appinioSocialShare = AppinioSocialShare();
    // String response = await appinioSocialShare.shareToFacebookStory("798744054865435",stickerImage: img.path);


    //
    Uint8List imageBytes = await pickedFile.readAsBytes();
    CroppedFile  croppedFile = await CropTool().crop(4, 3, CropAspectRatioPreset.ratio4x3, false, img.path);
    if(croppedFile != null) {
      imageBytes = await croppedFile.readAsBytes();
      imgPath = img.path;
      imgUnit8 = imageBytes;
      imgBase64 = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + imgBase64);

      _uploadImage(imgBase64);
      setState(() {

      });
    }

  }


  void getArticleCommentList( ) async {
    print("getArticleCommentList");



    ResultData resultData = await AppApi.getInstance().getArticleCommentList(context, true,
        widget.travelClass.id);
    if (resultData.isSuccess()) {
      print(resultData.data.toString());

      commentList.clear();
      for (Map<String,dynamic> c in resultData.data){
        TravelCommentClass travelCommentClass = TravelCommentClass.fromJson(c);

        print(travelCommentClass.user_img);
        print(travelCommentClass.updated_at);
        print(travelCommentClass.created_at);

        DateTime d = DateTime.parse(travelCommentClass.created_at);
        travelCommentClass.created_at_time = DateTime.parse(travelCommentClass.created_at);

        travelCommentClass.created_at = DateTools().dateLineFormate(d);


        print(" travelCommentClass.user_id " +  travelCommentClass.user_id);
        print(" travelCommentClass.name" +  travelCommentClass.name);
        print(" travelCommentClass.img " +  travelCommentClass.user_img);
        print(" travelCommentClass.content " +  travelCommentClass.content);

        commentList.add(travelCommentClass);
      }
      sortComment();
    } else {
      WarningDialog.showIOSAlertDialog(context, S().download_fail_title,S().download_fail_hint,S().confirm);
    }
  }

  void getArticleCommentListAndToBottom( ) async {
    print("getArticleCommentListAndToBottom");



    ResultData resultData = await AppApi.getInstance().getArticleCommentList(context, true,
        widget.travelClass.id);
    if (resultData.isSuccess()) {
      print(resultData.data.toString());

      commentList.clear();
      for (Map<String,dynamic> c in resultData.data){
        TravelCommentClass travelCommentClass = TravelCommentClass.fromJson(c);

        print(travelCommentClass.user_img);
        print(travelCommentClass.updated_at);
        print(travelCommentClass.created_at);

        DateTime d = DateTime.parse(travelCommentClass.created_at);
        travelCommentClass.created_at_time = DateTime.parse(travelCommentClass.created_at);

        travelCommentClass.created_at = DateTools().dateLineFormate(d);

        print(" travelCommentClass.created_at " +  travelCommentClass.created_at);
        commentList.add(travelCommentClass);
      }

      sortComment();
      Future.delayed(Duration(milliseconds: 100), () {
        // 5s over, navigate to a new page
        _controller.animateTo(_controller.position.maxScrollExtent,duration: Duration(milliseconds: 400),    curve: Curves.fastOutSlowIn,);
      });

    } else {
      WarningDialog.showIOSAlertDialog(context, S().download_fail_title,S().download_fail_hint,S().confirm);
    }
  }


  Future<void> _uploadImage(String data)  async {
    // Loading.show(context);

    ResultData resultData = await AppApi.getInstance().setFileBase64(
        context,
        true,
        data
    );

    if (resultData.isSuccess()) {
      // Loading.dismiss(context);
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data);
      imgUrl = GDefine.imageUrl + resultData.data["path"];
      setState(() {

      });
      print("imgUrl " + imgUrl);
    } else {
      // Loading.dismiss(context);
      WarningDialog.showIOSAlertDialog(context, S().upload_fail_title, S().upload_fail_title,S().confirm);

    }

  }


  Future<void> sendComment() async {
    Tools().dismissK(context);


    if(!Tools().stringNotNullOrSpace(commentEdit.text) &&  imgUrl.length == 0
    ){

      //Loading.dismiss(context);
      //WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_comment_no_content_title, S().personal_my_travel_comment_no_content_hint, S().confirm);
    }else {
      Loading.show(context);
      print("commentEdit.text " + commentEdit.text.toString());
      ResultData resultData;
      if(editCommentID == "-1") {
        resultData = await AppApi.getInstance().setArticleComment(
            context, true, widget.travelClass.id,
            imgUrl, commentEdit.text);
      } else {

        resultData = await AppApi.getInstance().editArticleComment(
            context, true, editCommentID,
            imgUrl, commentEdit.text,editCreated,1);
        editCommentID = "-1";
      }
      Loading.dismiss(context);
      if (resultData.isSuccess()) {
        commentEdit.text = "";


        imgUrl = "";
        imgPath = "";
        imgUnit8 = null;
        sendTag();

        getArticleCommentListAndToBottom();

        getMyArticleAPI();

      } else {
        WarningDialog.showIOSAlertDialog(
            context, S().upload_fail_title, S().upload_fail_hint, S().confirm);
      }
    }

  }
  bool isNumeric(String s) {
    if(s == null) {
      return false;
    }
    return double.parse(s, (e) => null) != null;
  }
  Future<void> sendTag() async {
    ResultData resultData;

    List<String> mentionTextList =  mentionKey.currentState.controller.markupText.replaceAll("[mentionId:", "").replaceAll("]<end>", "").split(" ");

    for (String a in mentionTextList) {
      if(isNumeric(a)) {
        resultData = await AppApi.getInstance().setMessage1(
            context, true,
            a );
      }
    }

    mentionKey.currentState.controller.text  = "";


    if (resultData.isSuccess()) {

    } else {

    }



  }


  Future<void> searchUser(String name)  async {
    // Loading.show(context);
    print("searchUser");
    mentionList.clear();
    ResultData resultData = await AppApi.getInstance().getUserInfo(
        context,
        true,
        name
    );

    if (resultData.isSuccess()) {
      // Loading.dismiss(context);
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data);

      for (Map a in resultData.data){
        mentionList.add(
          {
            "id" : a["user_id"].toString(),
            "avatar" : a["avatar"],
            "name" : a["name"].toString().replaceAll(" ", " "),
            "display" : a["name"].toString().replaceAll(" ", "_"),
          }
        );
      }

      setState(() {

      });
    } else {

    }

  }

  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }


  void setArticleViews( ) async {
    print("setArticleViews");

    ResultData resultData = await AppApi.getInstance().setArticleViews(context, true,  widget.travelClass.id);
    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});

  }
  void sendArticalFavorite(TravelClass travelClass) async {
    // Loading.show(context);
    String isCollect = "0";

    ResultData resultData = await AppApi.getInstance().setArticleCollect(context, true,  SharePre.prefs.getString(GDefine.user_id),travelClass.id, isCollect);
    // Loading.dismiss(context);
    if (resultData.isSuccess()) {
      if(travelClass.is_collect.contains("1")){
        travelClass.is_collect = "0";
      }else{
        travelClass.is_collect = "1";
      }
      setState(() {

      });
    } else {
      WarningDialog.showIOSAlertDialog(context, S().travel_collect_fail_title, S().travel_collect_fail_hint,S().confirm);
    }
  }
  void sendCommentFavorite(TravelCommentClass travelCommentClass) async {
      // Loading.show(context);
      String isCollect = "0";

      ResultData resultData = await AppApi.getInstance().setArticleScoreLiked(context, true,  travelCommentClass.id,intl.DateFormat('yyyy-MM-dd HH:mm:ss').
      format((travelCommentClass.created_at_time)));
      // Loading.dismiss(context);
      if (resultData.isSuccess()) {
        if(travelCommentClass.is_liked.contains("1")){
          travelCommentClass.is_liked= "0";
          travelCommentClass.liked_num = (int.parse(travelCommentClass.liked_num) - 1).toString() ;
        }else{
          travelCommentClass.is_liked = "1";
          travelCommentClass.liked_num = (int.parse(travelCommentClass.liked_num) +1).toString() ;
        }
        // getArticleCommentList();

        setState(() {

        });
      } else {
        // WarningDialog.showIOSAlertDialog(context, "喜愛失敗", resultData.msg,"確認");
      }
    }

    void sortComment(){
      print("sortComment");
      if(filterValue == S().travel_comment_filter_hot){


        commentList.sort((a,b) {
          int nameComp = b.liked_num.compareTo(a.liked_num);
          if (nameComp == 0) {
            return a.created_at_time.compareTo(b.created_at_time); // '-' for descending
          }
          return nameComp;
        });

      }else {
        commentList.sort((a,b) {
          return a.created_at_time.compareTo(b.created_at_time);
        });
      }
      setState(() {

      });
      //
      // print("commentList")
    }


  Future<void> goEdit() async {
    print("goEdit");
    print(widget.travelClass.contentListDynamic.toString());
    String update = await Navigator.push(context,
        MaterialPageRoute(builder: (_) => NewTravelPage(travelClass:widget.travelClass,editOnline:true))
    );
    if(update.contains("true")){
      getMyArticleAPI();
    }else {

    }

  }


  void getMyArticleAPI( ) async {
    Loading.show(context);
    print("getMyArticleAPI");

    ResultData resultData = await AppApi.getInstance().getArticle(context, true,
      SharePre.prefs.getString(GDefine.user_id),
      widget.travelClass.id,);



    Loading.dismiss(context);
    if (resultData.isSuccess()) {
      print(resultData.data.toString());
      var a = resultData.data;

      TravelClass travelClass = TravelClass.fromJson(a);
      travelClass.sectionTag = new List();
      print("name : " + TravelClass
          .fromJson(a)
          .title
          .toString());
      print("contentList : " + TravelClass
          .fromJson(a)
          .contentList
          .toString());
      print("contentList : " + a["content_list"].toString());
      print("DDDDDD" + jsonDecode(a["content_list"].toString()).toString());
      if (a["content_list"] != null) {
        List<dynamic> content_list = jsonDecode(a["content_list"].toString());
        travelClass.contentListDynamic = new List();


        List<TravelSubClass> subList = new List();
        for (Map<String, dynamic> sub in content_list) {
          TravelSubClass travelSubClass = new TravelSubClass();
          List<dynamic> content_list2 = sub["contentList"];
          List<TravelContentClass> travelContentClassList = new List();

          // if(content_list2.length>0) {
            Map<String, dynamic> dy = new Map();
            dy["title"] =
            sub["title"].toString() == "null" ? "" : sub["title"].toString();
            dy["type"] = "0";
            dy["description"] = "";
            dy["google"] = "";
            dy["image"] = [];
            travelClass.contentListDynamic.add(dy);
          // }


          for (Map<String, dynamic> subsub in content_list2) {
            TravelContentClass travelContentClass = new TravelContentClass();
            travelContentClass.image = List<String>.from(subsub["image"]);
            travelContentClass.image.removeWhere((element) =>
            element == "" || element == "null");
            travelContentClass.title = subsub["title"];
            travelContentClass.description =
            subsub["description"].toString() == "null"
                ? ""
                : subsub["description"].toString();
            travelContentClass.id = subsub["id"].toString();
            travelContentClass.google =
            subsub["google"].toString() == "null" ? "" : subsub["google"]
                .toString();
            travelContentClass.key = new GlobalKey();

            travelContentClassList.add(travelContentClass);

            Map<String, dynamic> dy2 = new Map();
            dy2["title"] = travelContentClass.title;
            dy2["type"] = "1";
            dy2["description"] = travelContentClass.description;
            dy2["google"] = travelContentClass.google;
            dy2["image"] = List<String>.from(subsub["image"]);

            travelClass.contentListDynamic.add(dy2);
          }
          travelSubClass.title =
          sub["title"].toString() == "null" ? "" : sub["title"].toString();
          travelSubClass.id = sub["id"].toString();
          travelSubClass.key = new GlobalKey();
          if(sub["title"]!="" && sub["title"].toString() != "null") {
            travelClass.sectionTag.add(new SectionClass(title: sub["title"], key:travelSubClass.key  ));
          }

          if (travelContentClassList.isNotEmpty) {
            travelSubClass.content = travelContentClassList;
          }

          subList.add(travelSubClass);
        }

        travelClass.contentList = subList;

        widget.travelClass = travelClass;
        GlobalData.curernt_travel = widget.travelClass;
        _tabFirstController = new TabController(vsync: this, length: widget.travelClass.sectionTag.length);
        bottomVisible = false;
        if( widget.travelClass.sectionTag.length!= 0  || widget.travelClass.contentList.length != 0){
          if(widget.travelClass.contentList.length != 0){
            if(widget.travelClass.contentList.first.content!= null){
              bottomVisible = true;
            }

          }
        }
        setState(() {


        });
      }
    }

    else {
      WarningDialog.showIOSAlertDialog(context, S().download_fail  , S().please_wait,S().confirm);
    }



  }


  void sendBlockWarning(){
    WarningDialog.showIOSAlertDialog2(context, S().block_title, S().block_hint, S().confirm, sendBlock);
  }
  void sendBlock( ) async {


    Loading.show(context);
    ResultData resultData = await AppApi.getInstance().setUserBlock(context, true,  widget.travelClass.tourist_id);
    await getBlockList();
    Loading.dismiss(context);
    backClick();
    backClick();
    // if (resultData.isSuccess()) {
    //
    //   Loading.show(context);
    //
    //
    // } else {
    //   WarningDialog.showIOSAlertDialog(context, S().send_like_fail_title,S().send_like_fail_hint,S().confirm);
    // }
  }

  void sendBlockWarning2(){
    WarningDialog.showIOSAlertDialog2(context, S().block_title, S().block_hint, S().confirm, sendBlock2);
  }
  void sendBlock2( ) async {


    Loading.show(context);
    ResultData resultData = await AppApi.getInstance().setUserBlock(context, true,  travelCommentClassBlock.user_id);
    await getBlockList();
    getArticleCommentListAndToBottom();
    Loading.dismiss(context);
    backClick();
 
    // if (resultData.isSuccess()) {
    //
    //   Loading.show(context);
    //
    //
    // } else {
    //   WarningDialog.showIOSAlertDialog(context, S().send_like_fail_title,S().send_like_fail_hint,S().confirm);
    // }
  }
  void getBlockList( ) async {
    print("getBlockList");


    ResultData resultData = await AppApi.getInstance()
        .getUserBlockList(context, true);

    print(resultData.data.toString());
    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      GDefine.blockUserId.clear();
      for (Map<String, dynamic> a in resultData.data) {
        if(!GDefine.blockUserId.contains(a ["block_user_id"].toString())) {
          GDefine.blockUserId.add(a ["block_user_id"].toString());
        }
      }

    }
  }

  void goEditComment(TravelCommentClass travelCommentClass){
    editCommentID = travelCommentClass.id;
    editCreated = travelCommentClass.created_at_raw;

    commentEdit.text = travelCommentClass.content;
    imgUrl = travelCommentClass.img;
    imgPath = "";
    imgUnit8 = null;

    backClick();
    setState(() {

    });

  }

  Future<void> goDelComment(TravelCommentClass travelCommentClass) async {

    backClick();
    Tools().dismissK(context);

      Loading.show(context);



      ResultData resultData = await AppApi.getInstance().delArticleComment(
          context, true, travelCommentClass.id,
          travelCommentClass.created_at_raw);


      Loading.dismiss(context);
      if (resultData.isSuccess()) {

        getArticleCommentListAndToBottom();

        getMyArticleAPI();


      } else {
        WarningDialog.showIOSAlertDialog(
            context, S().delete_fail_title, S().delete_fail_hint, S().confirm);
      }


  }


}

class SectionWidget extends StatelessWidget {
  final TravelSubClass travelSubClass;
  final int index;

  const SectionWidget({Key key,  this.travelSubClass,  this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int currentIndexPage = 0;


    return Container(


        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [


            travelSubClass.title!= null && travelSubClass.title != ""?
            Padding(
              padding: EdgeInsets.only(top:0.026.sh),
              child: SelectableAutoLinkText(
                index!= 0? "\n\n"+ travelSubClass.title : travelSubClass.title,
                linkStyle: TextStyle(color: Colors.blueAccent,decoration: TextDecoration.underline,),
                onTap: (url) => launch(url, forceSafariVC: false),
                highlightedLinkStyle: TextStyle(
                  color: Colors.blueAccent,
                  backgroundColor: Colors.blueAccent.withAlpha(0x33),
                ),
                textAlign: TextAlign.center,
                style:GoogleFonts.inter(
                  fontSize: 20.nsp,
                  color: OwnColors.CTourOrange,
                  fontWeight: FontWeight.w700,
                ),
                moreBool: false,

              ),
            ) :       Container(width: 1,),

            travelSubClass.content!= null?
            ListView.builder(
                shrinkWrap: true,
                itemCount: travelSubClass.content.length,
                scrollDirection: Axis.vertical,
                physics: const NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.all(0.0),
                itemBuilder: (context, index) {
                  bool containImg = false;
                  for (String a  in travelSubClass.content[index].image){
                    if (a.contains("http")){
                      containImg = true;
                    }
                  }
                  travelSubClass.content[index].image.removeWhere((value) => !value.contains("http"));

                  return      Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  HorizontalSpace().create(context, 0.026),
                  Row(
                    children: [
                      Flexible(
                        child: SelectableAutoLinkText(
                          travelSubClass.content[index].title,
                          linkStyle: TextStyle(color: Colors.blueAccent,decoration: TextDecoration.underline,),
                          highlightedLinkStyle: TextStyle(
                            color: Colors.blueAccent,
                            backgroundColor: Colors.blueAccent.withAlpha(0x33),
                          ),
                          onTap: (url) => launch(url, forceSafariVC: false),
                          key: travelSubClass.content[index].key,
                          textAlign: TextAlign.left,
                          style:GoogleFonts.inter(
                            fontSize: 16.nsp,
                            color: OwnColors.black,
                            fontWeight: FontWeight.w700,
                          ),
                          moreBool: false,

                        ),
                      ),
                      travelSubClass.content[index].google.contains("goo")?Padding(
                        padding: const EdgeInsets.only(left:8.0),
                        child: Container(
                          height: 0.018.sh,
                          child: GestureDetector(onTap: () async {
                            print("tap");
                            if (await canLaunch(travelSubClass.content[index].google)) {
                            await launch(travelSubClass.content[index].google);
                            } else {
                            throw 'Could not open the map.';
                            }

                          },child: Image.asset(Res.mapping)),
                        ),
                      ) : Container(width: 1,)
           

                    ],
                  ),
                  HorizontalSpace().create(context, 0.026),
                  Visibility(
                    visible: travelSubClass.content[index].description.length>0 && travelSubClass.content[index].description.toString() != "null",
                    child: SelectableAutoLinkText(
                      travelSubClass.content[index].description,
                      linkStyle: TextStyle(color: Colors.blueAccent,decoration: TextDecoration.underline,),
                      highlightedLinkStyle: TextStyle(
                        color: Colors.blueAccent,
                        backgroundColor: Colors.blueAccent.withAlpha(0x33),
                      ),
                      onTap: (url) => launch(url, forceSafariVC: false),
                      style:GoogleFonts.inter(
                        fontSize: 15.nsp,
                        height: 1.7,
                        color: OwnColors.black,
                        fontWeight: FontWeight.w400,


                      ),
                      moreBool: false,

                    ),
                  ),
                  containImg?HorizontalSpace().create(context, 0.026):Container(width: 1,),
                  containImg?Container(
                      width: 0.92 * ScreenSize().getWidth(context),

                      child:
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: travelSubClass.content[index].image.length,
                          scrollDirection: Axis.vertical,
                          itemBuilder: (context, i) {
                            return 
                              
                              Padding(
                              padding: EdgeInsets.only(top: i != 0 ? 12.0:0.0),
                              child: ClipRRect(
                                borderRadius: BorderRadius.all(Radius.circular(14)),
                                child: GestureDetector(
                                  onTap: (){
                                    PushNewScreen().normalPush(context, ImgPage(url:travelSubClass.content[index].image[i] ,));
                                  },
                                  child: CachedNetworkImage(
                            imageUrl: travelSubClass.content[index].image[i],
                            fit: BoxFit.fitWidth,
                            width: 200,
                            placeholder: (context, url) => AspectRatio(
                                  aspectRatio: 1,
                                  child: FractionallySizedBox(
                                  heightFactor: 0.5,
                                  widthFactor: 0.5,
                                  child: SpinKitRing(
                                    lineWidth:5,
                                    color:
                                    OwnColors.tran ,
                                  ),
                                  ),
                            ),
                            errorWidget: (context, url, error) =>
                                  Icon(
                                    Icons.error,
                                    color: OwnColors.black.withOpacity(0.5),
                                  ),
                            // imageBuilder: (context, imageProvider) =>
                            //     Center(
                            //       child: Container(
                            //         // width:0.92 * ScreenSize().getWidth(context),
                            //
                            //         // height: 0.92 * ScreenSize().getWidth(context),
                            //         decoration: BoxDecoration(
                            //           color: OwnColors.tran,
                            //           borderRadius: BorderRadius.all(
                            //               Radius.circular(14)),
                            //           image: DecorationImage(
                            //               image: imageProvider,
                            //               fit: BoxFit.fitWidth),
                            //         ),
                            //       ),
                            //     ),
                                  ),
                                ),
                              )
                            );
                          })

//                       Swiper(
//
//                         itemBuilder: (BuildContext context, int ii) {
//                           return  CachedNetworkImage(
//                             imageUrl: travelSubClass.content[index].image[ii],
//                             fit: BoxFit.cover,
//                             placeholder: (context, url) => AspectRatio(
//                               aspectRatio: 1,
//                               child: FractionallySizedBox(
//                                 heightFactor: 0.5,
//                                 widthFactor: 0.5,
//                                 child: SpinKitRing(
//                                   lineWidth:5,
//                                   color:
//                                   OwnColors.tran ,
//                                 ),
//                               ),
//                             ),
//                             errorWidget: (context, url, error) =>
//                                 Icon(
//                                   Icons.error,
//                                   color: OwnColors.black.withOpacity(0.5),
//                                 ),
//                             imageBuilder: (context, imageProvider) =>
//                                 Container(
//                                   // width:0.92 * ScreenSize().getWidth(context),
//                                   // height: 0.92 * ScreenSize().getWidth(context),
//                                   decoration: BoxDecoration(
//                                     color: OwnColors.tran,
//                                     borderRadius: BorderRadius.all(
//                                         Radius.circular(14)),
//                                     image: DecorationImage(
//                                         image: imageProvider,
//                                         fit: BoxFit.cover),
//                                   ),
//                                 ),
//                             // width: MediaQuery.of(context).size.width,
//                           );
//
//                         },
//                         outer:true,
//                         autoplay: false,
//                         itemCount: travelSubClass.content[index].image.length,
//                         itemWidth: 300.0,
//                         itemHeight: 200.0,
//                         // onIndexChanged: (index) {
//                         //   currentIndexPage = index;
//                         //   setState(() {});
//                         // },
//                         loop:false,
//                         pagination:
//
//                         travelSubClass.content[index].image.length == 1? null :new SwiperPagination(
//                           builder: new DotSwiperPaginationBuilder(
//
//                             size: travelSubClass.content[index].image.length == 1? 0:8,
//                             activeSize: travelSubClass.content[index].image.length == 1? 0 :8,
//                             color: OwnColors.CF4F4F4,
//                             activeColor: travelSubClass.content[index].image.length == 1? OwnColors.tran:OwnColors.CTourOrange,
//                           ),
// // ),
//                           // control: new SwiperControl(),
//                         ),
//                       )
                  ):Container(width: 1,),
                  // HorizontalSpace().create(context, 0.01),
                  // DotsIndicator(
                  //   dotsCount: travelSubClass.content[index].image.length,
                  //   position: currentIndexPage.toDouble(),
                  //   decorator: DotsDecorator(
                  //     color: OwnColors.CC8C8C8,
                  //     activeColor: OwnColors.CTourOrange,
                  //   ),
                  // ),

                ],
                  );
                }):
                Container(width: 1,)

           ],
        ));
  }








}



typedef SliverHeaderBuilder = Widget Function(
    BuildContext context, double shrinkOffset, bool overlapsContent);

class SliverHeaderDelegate extends SliverPersistentHeaderDelegate {
  final PreferredSize child;

  SliverHeaderDelegate({this.child});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(child: child);
  }


  @override
  double get maxExtent => child.preferredSize.height;

  @override
  double get minExtent => child.preferredSize.height;

  @override
  bool shouldRebuild(SliverHeaderDelegate old) {
    return old.maxExtent != maxExtent || old.minExtent != minExtent;
  }

}


class UserModel {
  int id;
  String avatar;
  String name ;




  UserModel({this.id,this.avatar, this.name });
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'avatar': avatar,
      'name': name,
    };
  }

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    id: json["id"],
    avatar: json["avatar"].toString(),
    name: json["name"].toString(),


  );

}