library basicnetservice;

import 'dart:convert';
import 'dart:io';

import 'package:ctour/Login/RegisterStatic.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:flutter/widgets.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Network/service/result_data.dart';

import 'app_net_service.dart';

class AppApi extends AppNetService {
  /// 获取天气的接口
  static String API = "/enterPort";

  AppApi._();

  static AppApi _instance;

  static AppApi getInstance() {
    if (_instance == null) {
      _instance = AppApi._();
    }
    return _instance;
  }

  Future<ResultData> register(BuildContext context, bool showProgress, String name, String email,
      String firebase_id,String login_type,String avatar ) async {
    Map<String, dynamic> param = {};
    API = "/Register";
    param["name"] = name;
    if(email != null){
      param["email"] = email;
    }else {
      param["email"] = "";
    }

    param["firebase_id"] = firebase_id;
    param["login_type"] = login_type;
    param["avatar"] = avatar;
    param["country"] = Tools().getLocal();

    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }


  Future<ResultData> login(BuildContext context, bool showProgress,   String firebase_id,String login_type) async {
    Map<String, dynamic> param = {};
    API = "/login";
    param["firebase_id"] = firebase_id;
    param["login_type"] = login_type;
    param["phone_lang"] = Tools().getLocal();
    param["device_token"] = SharePre.prefs.getString(GDefine.FCMtoken);


    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> getScoreCount(BuildContext context, bool showProgress,   String user_id) async {
    Map<String, dynamic> param = {};
    API = "/getScoreCount";
    param["user_id"] = user_id;



    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }



  Future<ResultData> setFileBase64(BuildContext context, bool showProgress,String base64 ) async {
    Map<String, dynamic> param = {};
    API = "/setFileBase64";
    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["type"] = "image";
    param["Base64"] = base64;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }
  Future<ResultData> getUserInfo(BuildContext context, bool showProgress,String name ) async {
    Map<String, dynamic> param = {};
    API = "/getUserInfo";
    param["name"] = name;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }
  Future<ResultData> setMessage1(BuildContext context, bool showProgress,String tag_user_id ) async {
    Map<String, dynamic> param = {};
    API = "/setMessage1";
    print("setMessage1 " + tag_user_id);
    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["tag_user_id"] = tag_user_id;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }



  Future<ResultData> setFileBase64Avatar(BuildContext context, bool showProgress,String base64 ) async {
    Map<String, dynamic> param = {};
    API = "/setFileBase64";
    param["user_id"] = "";
    param["type"] = "image";
    param["fileName"] = SharePre.prefs.getString(GDefine.firebase_id);
    param["Base64"] = base64;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }



  Future<ResultData> setUserInfoAvatarName(BuildContext context, bool showProgress,String user_id,String name,String avatar) async {
    Map<String, dynamic> param = {};
    API = "/setUserInfo";
    param["user_id"] = user_id;
    param["name"] = name;
    param["avatar"] = avatar;
    if(SharePre.prefs.getString(GDefine.isGuilder) == null){
      param["is_tourist"] = 0;
    }else {
      if (SharePre.prefs.getString(GDefine.isGuilder).contains("false")) {
        param["is_tourist"] = 0;
      } else {
        param["is_tourist"] = 1;
      }
    }
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }




  Future<ResultData> getTouristList(BuildContext context, bool showProgress ,String user_id,
      String start_date,String end_date,int count,String lang,String is_rank ) async {
    Map<String, dynamic> param = {};
    API = "/getTouristList";

    param["user_id"] = user_id;
    param["start_date"] = start_date;
    param["end_date"] = end_date;
    param["count"] = count;
    param["country"] = Tools().getLocal();
    param["is_rank"] = is_rank;
    // param["lang"] = lang;



    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> getTouristListPage(BuildContext context, bool showProgress ,String user_id,
      String start_date,String end_date,int count,String lang,String is_rank,int page ) async {
    Map<String, dynamic> param = {};
    API = "/getTouristListPage";

    param["user_id"] = user_id;
    param["start_date"] = start_date;
    param["end_date"] = end_date;
    param["count"] = count;
    param["country"] = Tools().getLocal();
    param["is_rank"] = is_rank;
    param["page"] = page;

    // param["lang"] = lang;



    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> getTouristFilterList(BuildContext context, bool showProgress ,
      String user_id,String start_date,String end_date,int count,List<String> lang,
      String score, String gender, List<String> trans, String comment_num_min, String comment_num_max,String is_rank) async {
    Map<String, dynamic> param = {};
    API = "/getTouristListPage";

    param["user_id"] = user_id;
    // param["start_date"] = start_date;
    // param["end_date"] = end_date;
    param["count"] = count;
    param["score"] = score;
    if(lang.length > 0){
      param["lang"] = jsonEncode(lang);
    }
    if(gender == S().guild_gender1){

    }else if(gender == S().guild_gender2){
      param["gender"] = "m";
    }else{
      param["gender"] = "f";
    }

    if(trans.length > 0){
      param["trans"] = trans.toString();
    }

    param["comment_num_min"] = comment_num_min;
    param["comment_num_max"] = comment_num_max;
    param["country"] = Tools().getLocal();
    param["is_rank"] = is_rank;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }


  Future<ResultData> getTouristSearchList(BuildContext context, bool showProgress ,
      String user_id,String start_date,String end_date,int count,List<String> lang,
      String score, String gender, List<String> trans, String comment_num_min,
      String comment_num_max,String is_rank,int want_companion,int page,String keyword) async {
    Map<String, dynamic> param = {};
    API = "/getTouristListPage";

    param["user_id"] = user_id;
    // param["start_date"] = start_date;
    // param["end_date"] = end_date;
    param["count"] = count;
    param["score"] = score;
    if(lang.length > 0){
      param["lang"] = jsonEncode(lang);
    }
    if(gender == S().guild_gender1){

    }else if(gender == S().guild_gender2){
      param["gender"] = "m";
    }else if (gender == S().guild_gender3){
      param["gender"] = "f";
    }

    if(trans.length > 0){
      param["trans"] = trans.toString();
    }
    if(want_companion != 3){
      param["want_companion"] = want_companion;
    }

    param["comment_num_min"] = comment_num_min;
    param["comment_num_max"] = comment_num_max;
    param["country"] = Tools().getLocal();
    param["is_rank"] = is_rank;

    param["page"] = page;
    param["keyword"] = keyword;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }


  Future<ResultData> getTouristFilterPageList(BuildContext context, bool showProgress ,
      String user_id,String start_date,String end_date,int count,List<String> lang,
      String score, String gender, List<String> trans, String comment_num_min, String comment_num_max,String is_rank,int want_companion,int page) async {
    Map<String, dynamic> param = {};
    API = "/getTouristListPage";

    param["user_id"] = user_id;
    // param["start_date"] = start_date;
    // param["end_date"] = end_date;
    param["count"] = count;
    param["score"] = score;
    if(lang.length > 0){
      param["lang"] = jsonEncode(lang);
    }
    if(gender == S().guild_gender1){

    }else if(gender == S().guild_gender2){
      param["gender"] = "m";
    }else if (gender == S().guild_gender3){
      param["gender"] = "f";
    }

    if(trans.length > 0){
      param["trans"] = trans.toString();
    }
    if(want_companion != 3){
      param["want_companion"] = want_companion;
    }

    param["comment_num_min"] = comment_num_min;
    param["comment_num_max"] = comment_num_max;
    param["country"] = Tools().getLocal();
    param["is_rank"] = is_rank;

    param["page"] = page;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }


  Future<ResultData> getTourist(BuildContext context, bool showProgress ,String user_id,String tourist_id ) async {
    Map<String, dynamic> param = {};
    API = "/getTourist";

    param["user_id"] = user_id;
    param["tourist_id"] = tourist_id;



    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }


  Future<ResultData> setUserInfo(BuildContext context, bool showProgress,List<String> tourist_img,List<String> lang, String gender, String is_toursit,
  List<String> trans, List<String> location,String invite_code,String description,String name,String want_companion,String avatar) async {
    Map<String, dynamic> param = {};
    API = "/setUserInfo";
    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["tourist_img"] = jsonEncode(tourist_img);
    param["lang"] =  jsonEncode(lang);
    param["gender"] = gender;
    param["is_tourist"] = is_toursit;
    param["trans"] = jsonEncode(trans);
    param["location"] = jsonEncode(location);
    param["invite_code"] = invite_code;
    param["description"] = description;
    param["name"] = name;
    param["want_companion"] = want_companion;
    param["avatar"] = avatar;


    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> registerSet(BuildContext context, bool showProgress, ) async {
    Map<String, dynamic> param = {};
    API = "/setUserInfo";
    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["avatar"] = RegisterStatic.img[0];
    param["tourist_img"] = jsonEncode(RegisterStatic.img.sublist(1));
    param["lang"] =  jsonEncode(RegisterStatic.langs);
    param["gender"] = RegisterStatic.gender;
    param["is_tourist"] = "1";
    param["name"] = RegisterStatic.name;
    param["trans"] = jsonEncode(["3"]);
    param["remark"] = "FullRG";
    param["location"] = jsonEncode(RegisterStatic.locations);



    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }
  Future<ResultData> setFCMOpen(BuildContext context, bool showProgress,String open) async {
    Map<String, dynamic> param = {};
    API = "/setUserInfo";

    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["is_fcm_open"] = open;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }
  Future<ResultData> sendSingeFCM(BuildContext context, bool showProgress,String user_id,String title,String content) async {
    Map<String, dynamic> param = {};
    API = "/sendSingeFCM";

    param["user_id"] = user_id;
    param["from_user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["title"] = title;
    param["content"] = content;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }





  Future<ResultData> sendFCM(BuildContext context, bool showProgress ,String fcm, String title,String mes ) async {
    Map<String, dynamic> param = {};

    String api = "http://manage-carryforward.com/app/main/api/fcmAPI.php?token=" + fcm +
        "&title=" + title + "&message=" + mes;


    ResultData resultData = await postRaw(api, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> setCollect(BuildContext context, bool showProgress, String user_id,String tourist_id) async {
    Map<String, dynamic> param = {};
    API = "/setCollect";

    param["user_id"] = user_id;
    param["tourist_id"] = tourist_id;
    // param["lang"] = lang;



    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> getTouristCommentList(BuildContext context, bool showProgress, String user_id,String tourist_id) async {
    Map<String, dynamic> param = {};
    API = "/getTouristCommentList";

    param["user_id"] = user_id;
    param["tourist_id"] = tourist_id;
    // param["lang"] = lang;

    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> setAuthComment(BuildContext context, bool showProgress, String user_id,String tourist_id, String invite_code) async {
    Map<String, dynamic> param = {};
    API = "/setAuthComment";

    param["user_id"] = user_id;
    param["tourist_id"] = tourist_id;
    param["invite_code"] = invite_code;
    // param["lang"] = lang;

    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> getTouristCollectList(BuildContext context, bool showProgress ,String user_id) async {
    Map<String, dynamic> param = {};
    API = "/getTouristCollectList";
    param["user_id"] = user_id;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> getArticleCollectList(BuildContext context, bool showProgress ,String user_id) async {
    Map<String, dynamic> param = {};
    API = "/getArticleCollectList";
    param["user_id"] = user_id;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }
  Future<ResultData> getArticleCommentList(BuildContext context, bool showProgress ,String article_id) async {
    Map<String, dynamic> param = {};
    API = "/getArticleCommentList";
    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["article_id"] = article_id;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> setComment(BuildContext context, bool showProgress ,String user_id,String tourist_id, int is_public, String score, String comment, List imgPath) async {
    Map<String, dynamic> param = {};
    API = "/setComment";
    param["user_id"] = user_id;
    param["tourist_id"] = tourist_id;
    param["is_public"] = is_public;
    param["score"] = score;
    param["comment"] = comment;
    Map<String,dynamic> imgMap = {};
    for(var i=0;i<imgPath.length;i++){
      imgMap[i.toString()]=[imgPath[i]];
    }
    param["img"]=imgMap;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }


  Future<ResultData> getArticleList(BuildContext context, bool showProgress ,String user_id,String tourist_id,String start_date
      ,String end_date,String duration,String sort_type,String type,String lang,String count,String is_rank) async {
    Map<String, dynamic> param = {};
    API = "/getArticleList";
    param["user_id"] = user_id;
    param["tourist_id"] = tourist_id;
    param["start_date"] = start_date;
    param["end_date"] = end_date;
    param["duration"] = duration;
    param["sort_type"] = sort_type;
    param["type"] = type;
    param["lang"] = lang;
    param["count"] = count;
    param["country"] = Tools().getLocal();
    param["is_rank"] = is_rank;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }
  Future<ResultData> getArticleListPage(BuildContext context, bool showProgress ,String user_id,String tourist_id,String start_date
      ,String end_date ,String sort_type,String type,String lang,String count,String duration_start,
      String duration_end,String is_rank,int page)  async {
    Map<String, dynamic> param = {};
    API = "/getArticleListPage";
    print("getArticleListPagegetArticleListPage");
    print("getArticleListPagegetArticleListPage");
    param["user_id"] = user_id;
    param["tourist_id"] = tourist_id;
    param["start_date"] = start_date;
    param["end_date"] = end_date;
    param["duration_start"] = duration_start;
    param["duration_end"] = duration_end;
    param["sort_type"] = sort_type;
    param["type"] = type;
    param["lang"] = lang;
    param["count"] = count;
    param["country"] = Tools().getLocal();
    param["is_rank"] = is_rank;
    param["page"] = page;

    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }


  Future<ResultData> getArticleListFilter(BuildContext context, bool showProgress ,String user_id,String tourist_id,String start_date
      ,String end_date ,String sort_type,String type,String lang,
      String count,String duration_start,
      String duration_end,String is_rank,int page) async {
    Map<String, dynamic> param = {};
    API = "/getArticleList";
    param["user_id"] = user_id;
    param["tourist_id"] = tourist_id;
    param["start_date"] = start_date;
    param["end_date"] = end_date;
    param["duration_start"] = duration_start;
    param["duration_end"] = duration_end;
    param["sort_type"] = sort_type;
    param["type"] = type;
    param["lang"] = lang;
    param["count"] = count;
    param["country"] = Tools().getLocal();
    param["is_rank"] = is_rank;
    param["page"] = page;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }


  Future<ResultData> getArticleListSearch(BuildContext context, bool showProgress ,String user_id,String tourist_id,String start_date
      ,String end_date ,String sort_type,String type,String lang,
      String count,String duration_start,
      String duration_end,String is_rank,int page,String keyword) async {
    Map<String, dynamic> param = {};
    API = "/getArticleListPage";
    param["user_id"] = user_id;
    param["tourist_id"] = tourist_id;
    param["start_date"] = start_date;
    param["end_date"] = end_date;
    param["duration_start"] = duration_start;
    param["duration_end"] = duration_end;
    param["sort_type"] = sort_type;
    param["type"] = type;
    param["lang"] = lang;
    param["count"] = count;
    param["country"] = Tools().getLocal();
    param["is_rank"] = is_rank;
    param["page"] = page;
    param["keyword"] = keyword;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> getArticle(BuildContext context, bool showProgress ,String user_id,String article_id ) async {
    Map<String, dynamic> param = {};
    API = "/getArticle";
    param["user_id"] = user_id;
    param["article_id"] = article_id;

    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> setArticle(BuildContext context, bool showProgress ,String user_id,String article_id,String cover_image
      ,String title,String duration,String date,String type,String description,String tourist_id,List<String> tag,dynamic contentList) async {
    Map<String, dynamic> param = {};
    API = "/setArticle";
    param["user_id"] = user_id;
    param["article_id"] = article_id;
    param["cover_image"] = cover_image;
    param["title"] = title;
    param["duration"] = duration;
    param["date"] = date;
    param["type"] = type;
    param["description"] = description;
    param["tourist_id"] = tourist_id;
    param["tag"] = jsonEncode(tag);
    param["contentList"] = jsonEncode(contentList);
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> setArticleViews(BuildContext context, bool showProgress ,String article_id) async {
    Map<String, dynamic> param = {};
    API = "/setArticleViews";
    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["article_id"] = article_id;
     ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> setArticleComment(BuildContext context, bool showProgress ,String article_id,String img, String content) async {
    Map<String, dynamic> param = {};
    API = "/setArticleComment";
    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["article_id"] = article_id;
    param["img"] = img;
    param["content"] = content;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> editArticleComment(BuildContext context, bool showProgress, String article_id,String img, String content,String created_at,
      int is_public) async {
    Map<String, dynamic> param = {};
    API = "/editArticleComment";

    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["article_id"] = article_id;
    param["img"] = img;
    param["content"] = content;
    param["created_at"] = created_at;
    param["is_public"] = is_public;
    // param["is_collect"] = is_collect;
    // param["lang"] = lang;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> delArticleComment(BuildContext context, bool showProgress, String article_id, String created_at,
       ) async {
    Map<String, dynamic> param = {};
    API = "/delArticleComment";

    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["article_id"] = article_id;

    param["created_at"] = created_at;

    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> delTouristComment(BuildContext context, bool showProgress, String tourist_id, String created_at,
      ) async {
    Map<String, dynamic> param = {};
    API = "/delTouristComment";

    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["tourist_id"] = tourist_id;

    param["created_at"] = created_at;

    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> editTouristComment(BuildContext context, bool showProgress ,String user_id,String tourist_id, int is_public, String score, String comment, List imgPath,String created_at
      ) async {
    Map<String, dynamic> param = {};
    API = "/editTouristComment";
    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["tourist_id"] = tourist_id;
    param["is_public"] = is_public;
    param["score"] = score;
    param["comment"] = comment;
    Map<String,dynamic> imgMap = {};
    for(var i=0;i<imgPath.length;i++){
      imgMap[i.toString()]=[imgPath[i]];
    }
    param["img"]=imgMap;
    param["created_at"]=created_at;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }


  Future<ResultData> setArticleScoreLiked(BuildContext context, bool showProgress ,String article_id ,String created_at) async {
    Map<String, dynamic> param = {};
    API = "/setArticleScoreLiked";
    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["article_id"] = article_id;
    param["created_at"] = created_at;

    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> delete_user(BuildContext context, bool showProgress ,String user_id) async {

    Map<String, dynamic> param = {};
    API = "/delete_user";
    param["user_id"] = user_id;

    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> setArticleCollect(BuildContext context, bool showProgress, String user_id,String article_id, String is_collect) async {
    Map<String, dynamic> param = {};
    API = "/setArticleCollect";

    param["user_id"] = user_id;
    param["article_id"] = article_id;
    // param["is_collect"] = is_collect;
    // param["lang"] = lang;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }


  Future<ResultData> delArticle(BuildContext context, bool showProgress, String user_id,String article_id) async {
    Map<String, dynamic> param = {};
    API = "/delArticle";

    param["user_id"] = user_id;
    param["article_id"] = article_id;
    // param["lang"] = lang;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> setUserArticleAppeal(BuildContext context, bool showProgress, String type,String content) async {
    Map<String, dynamic> param = {};
    API = "/setUserArticleAppeal";

    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["type"] = type;
    param["content"] = content;
    // param["lang"] = lang;
    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> setUserBlock(BuildContext context, bool showProgress, String block_user_id) async {
    Map<String, dynamic> param = {};
    API = "/setUserBlock";

    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);
    param["block_user_id"] = block_user_id;

    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

  Future<ResultData> getUserBlockList(BuildContext context, bool showProgress ) async {
    Map<String, dynamic> param = {};
    API = "/getUserBlockList";

    param["user_id"] = SharePre.prefs.getString(GDefine.user_id);


    ResultData resultData = await post(API, params: param, context: context, showLoad: showProgress);
    return resultData;
  }

}