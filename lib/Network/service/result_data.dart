import 'dart:convert';

import 'package:ctour/Network/API/net_config.dart';
import 'package:flutter/material.dart';


class ResultData {
  Map<String, dynamic> response; // 所有返回值
  dynamic data; // 请求回来的data, 可能是list也可能是map
  var code; // 服务器的状态码
  String msg; // 服务器给的提示信息
  /// true 请求成功 false 请求失败
  bool result = true; // 客户端是否请求成功false: HTTP错误
  String url = "";

  ResultData(this.msg, this.result, {this.url = ""});

  ResultData.response(this.response, {this.url = ""}) {
    this.code = this.response["retCode"];
    this.msg = this.response["retMsg"];
    this.data = this.response["retVal"];
  }

  bool isFail() {
    bool success = isSuccess();
    return !success;
  }

  bool isSuccess() {
    print("code：${code}");
    if(result){
      print("result = true");
    }
    bool success = (result) && (code.toString().contains("1"));
    if (!success) {
      mDebugPrint("Not success for $url:$result,code:$code,msg:$msg");
    }
    return success;
  }

  /// 失败情况下弹提示
  bool toast() {
    if (isFail()) {
      //Fluttertoast.showToast(msg: msg);
      return true;
    } else {
      return false;
    }
  }

  mDebugPrint(String log) {
    if (NetConfig.DEBUG) {
      debugPrint(log);
    }
  }
}