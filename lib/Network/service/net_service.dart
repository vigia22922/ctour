library msnetservice;

export 'result_data.dart';
import 'dart:convert';
import 'dart:io';

import 'package:ctour/generated/l10n.dart';
import 'package:dio/dio.dart';
import 'package:ctour/Network/API/net_config.dart';
import 'package:ctour/tool/APIHeader.dart';
import 'result_data.dart';
import 'dio_instance.dart';
import 'package:flutter/material.dart';
import 'dart:convert';


enum Method {
  GET,
  POST
}

class NetService {
  static const String _TAG = "NetService";

  /// get请求
  get(String url, {Map<String, dynamic> params, BuildContext context, bool showLoad}) async {
    return await getR(url,
        method: Method.GET,
        params: params,
        context: context,
        showLoad: showLoad);
  }

  /// post请求
  post(String url, {Map<String, dynamic> params, BuildContext context, bool showLoad}) async {
    return await request(url, method: Method.POST, params: params, context: context, showLoad: showLoad);
  }
  postRaw(String url, {Map<String, dynamic> params, BuildContext context, bool showLoad}) async {
    return await requestRaw(url, method: Method.POST, params: params, context: context, showLoad: showLoad);
  }

  Map<String, String> get headers => {
    "Content-Type": "application/json",
    "Accept": "application/json",
    //"Auth-Key": "Bearer $_token",
    // "Timestamp":APIHeader.currentTimeMillis().toString(),
    // "Auth-Key":APIHeader.getAuth().toString()
  };

  getR(String url, {Method method, Map<String, dynamic> params, String fileName, String fileSavePath, BuildContext context,
    bool showLoad = false}) async {
    try {
      Response response;
      Dio dioInstance = DioInstance.createInstance();

      switch (method) {
        case Method.GET:
          response = await dioInstance.get(url, queryParameters: params);
          break;
        case Method.POST:
          response = await dioInstance.post(url, data: params, options: Options(contentType: "application/x-www-form-urlencoded"));
          break;
      }
      return await handleDataSource(response, method, url: url);
    } catch (exception) {
      printLog("$_TAG net exception= " + exception.toString());
      return ResultData(S().network_fail, false, url: url);
    }
  }
  ///  请求部分
  request(String url, {Method method, Map<String, dynamic> params, String fileName, String fileSavePath, BuildContext context,
        bool showLoad = false}) async {
    try {
      Response response;
      Dio dioInstance = DioInstance.createInstance();
      //var headers = await headers;
      print("headers : " + headers.toString());
      if (headers != null) {
        dioInstance.options.headers = headers;
      }
      var baseUrl = await getBasicUrl();
      dioInstance.options.baseUrl = baseUrl;

      // 打印网络日志
      StringBuffer requestParamLog = new StringBuffer();
      requestParamLog.write("$_TAG ");
      requestParamLog.write("Url:");
      requestParamLog.write(baseUrl);
      requestParamLog.write(url);
      requestParamLog.write("\n");
      requestParamLog.write("$_TAG ");
      requestParamLog.write("params:");
      requestParamLog.write(json.encode(params));
      printLog(requestParamLog.toString());


      switch (method) {
        case Method.GET:
          response = await dioInstance.get(url, queryParameters: params);
          break;
        case Method.POST:
          print("params " + params.toString());
          response = await dioInstance.post(url, data: params, options: Options(contentType: "application/x-www-form-urlencoded"));
          print("response " + response.toString());
          break;
      }
      return await handleDataSource(response, method, url: url);
    } catch (exception) {
      printLog("$_TAG net exception= " + exception.toString());
      return ResultData(S().network_fail, false, url: url);
    }
  }

  requestRaw(String url, {Method method, Map<String, dynamic> params, String fileName, String fileSavePath, BuildContext context,
    bool showLoad = false}) async {
    try {
      Response response;
      Dio dioInstance = DioInstance.createInstance();
      //var headers = await headers;
      print("headers : " + headers.toString());
      if (headers != null) {
        dioInstance.options.headers = headers;
      }
      var baseUrl = await getBasicUrl();
      // dioInstance.options.baseUrl = baseUrl;

      // 打印网络日志
      StringBuffer requestParamLog = new StringBuffer();
      requestParamLog.write("$_TAG ");
      // requestParamLog.write("Url:");
      // requestParamLog.write(baseUrl);
      requestParamLog.write(url);
      requestParamLog.write("\n");
      requestParamLog.write("$_TAG ");
      requestParamLog.write("params:");
      requestParamLog.write(json.encode(params));
      printLog(requestParamLog.toString());


      switch (method) {
        case Method.GET:
          response = await dioInstance.get(url, queryParameters: params);
          break;
        case Method.POST:
          response = await dioInstance.post(url, data: params, options: Options(contentType: "application/x-www-form-urlencoded"));
          break;
      }
      return await handleDataSource(response, method, url: url);
    } catch (exception) {
      printLog("$_TAG net exception= " + exception.toString());
      return ResultData(S().network_fail, false, url: url);
    }
  }


  /// 数据处理
  static handleDataSource(Response response, Method method, {String url = ""}) {
    ResultData resultData;
    String errorMsg = "";
    int statusCode;
    const HTTP_OK = 200;
    statusCode = response.statusCode;
    printLog("$_TAG statusCode:" + statusCode.toString());
    Map<String, dynamic> data;
    if (response.data is Map) {
      data = response.data;
    } else {
      data = json.decode(response.data);
    }
    if (isPrint()) {
      printBigLog("$_TAG data: ", json.encode(data));
    }

    //处理错误部分
    if (statusCode != HTTP_OK) {
      errorMsg = "网络请求错误,状态码:" + statusCode.toString();
      resultData = ResultData(errorMsg, false, url: url);
    } else {
      try {
        resultData = ResultData.response(data);
      } catch (exception) {
        resultData = ResultData(exception.toString(), true, url: url);
      }
    }
    return resultData;
  }

  getHeaders() {

    return null;
  }

  getBasicUrl() {
    return null;
  }

  static void printLog(String log, {tag}) {
    bool print = isPrint();
    if (print) {
      String tagLog;
      if (tag != null) {
        tagLog = tag + log;
      } else {
        tagLog = log;
      }
      debugPrint(tagLog);
    }
  }

  static void printBigLog(String tag, String log) {
    //log = TEST_POEM;
    bool print = isPrint();
    const MAX_COUNT = 800;
    if (print) {
      if (log != null && log.length > MAX_COUNT) {
        // 超过1000就分次打印
        int len = log.length;
        int paragraphCount = ((len / MAX_COUNT) + 1).toInt();
        for (int i = 0; i < paragraphCount; i++) {
          int printCount = MAX_COUNT;
          if (i == paragraphCount -1) {
            printCount = len - (MAX_COUNT * (paragraphCount -1));
          }
          String finalTag = "" + tag + "\n";
          printLog(log.substring(i * MAX_COUNT, i * MAX_COUNT + printCount) + "\n", tag: finalTag);
        }
      } else {
        String tagLog;
        if (tag == null) {
          tagLog = tag + log;
        } else {
          tagLog = log;
        }
        printLog(tagLog);
      }
    }
  }

  static bool isPrint() {
    return NetConfig.DEBUG;
  }
}