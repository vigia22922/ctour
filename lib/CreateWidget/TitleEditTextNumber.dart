import 'package:auto_size_text/auto_size_text.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/size_extension.dart';

class TitleEditTextNumber {

  Widget create(
      BuildContext context,
      String title,
      TextEditingController editingController,
      Color borderColor,
      String image,
      Function edit,
      Function  update,

      ) {
    Color color = Theme.of(context).primaryColor;

    return Container(
      height:ScreenSize().getHeight(context)*0.07,
      decoration: BoxDecoration(
        color: OwnColors.black_a4,
        border: Border(
          top: BorderSide(width: 1.0, color: borderColor),
          bottom: BorderSide(width: 1.0, color:borderColor),
          left:BorderSide(width: 1.0, color: borderColor),
          right: BorderSide(width: 1.0, color: borderColor),
        ),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8),
            topRight: Radius.circular(8),
            bottomLeft: Radius.circular(8),
            bottomRight: Radius.circular(8)),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top:4.0,bottom: 4.0,left: 12,right: 12.0),
        child: Row(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                //列布局
                children: <Widget>[

                  Align(
                    alignment: Alignment.centerLeft,
                    child: AutoSizeText(

                      title,

                      minFontSize: 10,
                      maxFontSize: 14,
                      style: TextStyle(
                        color: OwnColors.black_a38,
                        fontWeight: FontWeight.normal,
                        fontSize: 12.nsp,

                      ),
                      maxLines: 1,
                    ),
                  ),

                  Expanded(
                    child: Container(
                      // height: height,
                      alignment: Alignment.center,
                      child: TextField(
                        controller: editingController,
                        onChanged: (text) {
                          update() ;
                        },
                        decoration: InputDecoration(
                            fillColor: OwnColors.tran,
                            contentPadding: EdgeInsets.symmetric(horizontal: 0,vertical: 0),
                            //Change this value to custom as you like
                            // isDense: true,
                            filled: true,
                            border: OutlineInputBorder(
                                borderRadius:
                                new BorderRadius.circular(20.0),
                                borderSide: BorderSide.none)


                        ),

                        style:
                        TextStyle(color:OwnColors.black_a87 , fontSize: 19.nsp),
                        maxLines: 1,
                        keyboardType: TextInputType.number,

                      ),
                    ),
                  ),


                ],
              ),
            ),

            Container(
              height: ScreenSize().getHeight(context)*0.04,
              child: AspectRatio(
                aspectRatio: 1,
                child: Ink.image( image: AssetImage(image),
                  fit: BoxFit.fitWidth,
                  child: GestureDetector(onTap: edit,),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }


  Widget createNeeded(
      BuildContext context,
      String title,
      TextEditingController editingController,
      Color borderColor,
      String image,
      Function edit,
      Function  update,
      ) {
    Color color = Theme.of(context).primaryColor;

    return Container(
      height:ScreenSize().getHeight(context)*0.07,
      decoration: BoxDecoration(
        color: OwnColors.black_a4,
        border: Border(
          top: BorderSide(width: 1.0, color: borderColor),
          bottom: BorderSide(width: 1.0, color:borderColor),
          left:BorderSide(width: 1.0, color: borderColor),
          right: BorderSide(width: 1.0, color: borderColor),
        ),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8),
            topRight: Radius.circular(8),
            bottomLeft: Radius.circular(8),
            bottomRight: Radius.circular(8)),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top:4.0,bottom: 4.0,left: 12,right: 12.0),
        child: Row(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                //列布局
                children: <Widget>[

                  Row(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: AutoSizeText(

                          title,

                          minFontSize: 10,
                          maxFontSize: 14,
                          style: TextStyle(
                            color: OwnColors.black_a38,
                            fontWeight: FontWeight.normal,
                            fontSize: 12.nsp,

                          ),
                          maxLines: 1,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left:8.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: AutoSizeText(

                            "*必填",

                            minFontSize: 10,
                            maxFontSize: 14,
                            style: TextStyle(
                              color: OwnColors.CE75735,
                              fontWeight: FontWeight.normal,
                              fontSize: 10.nsp,

                            ),
                            maxLines: 1,
                          ),
                        ),
                      )
                    ],
                  ),

                  Expanded(
                    child: Container(
                      // height: height,
                      alignment: Alignment.center,
                      child:


                      TextField(
                        controller: editingController,
                        onChanged: (text) {
                          update() ;
                        },
                        decoration: InputDecoration(
                            fillColor: OwnColors.tran,
                            contentPadding: EdgeInsets.symmetric(horizontal: 0,vertical: 0),
                            //Change this value to custom as you like
                            // isDense: true,
                            filled: true,
                            border: OutlineInputBorder(
                                borderRadius:
                                new BorderRadius.circular(20.0),
                                borderSide: BorderSide.none)


                        ),

                        style:
                        TextStyle(color:OwnColors.black_a87 , fontSize: 19.nsp),
                        maxLines: 1,
                        keyboardType: TextInputType.number,

                      ),
                    ),
                  ),


                ],
              ),
            ),

            Container(
              height: ScreenSize().getHeight(context)*0.04,
              child: AspectRatio(
                aspectRatio: 1,
                child: Ink.image( image: AssetImage(image),
                  fit: BoxFit.fitWidth,
                  child: GestureDetector(onTap: edit,),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

}
