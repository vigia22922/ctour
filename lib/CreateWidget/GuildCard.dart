import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Travel/TravelClass.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/DateTools.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:extended_wrap/extended_wrap.dart';

class GuildCard {
  Widget tourCard(BuildContext context,GuildClass guildClass,int index, Function tourClick) {

    return GestureDetector(
      onTap: () => {tourClick(guildClass)},
      child: Container(
        width: double.infinity,



        child: Container(
          decoration: BoxDecoration(
            color: OwnColors.white,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(15),
              topLeft: Radius.circular(15),
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15),
            ),

          ),
          child: Column(
            children: [
              HorizontalSpace().create(context, 0.01),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      height: 0.174 *  ScreenSize().getHeight(context),
                      width: 0.46.sw,
                      child: Center(
                        child: AspectRatio(
                          aspectRatio: 180/130,//1.384
                          child: Stack(
                            children: [
                              CachedNetworkImage(
                                imageUrl:
                                guildClass.avatar.contains("choose_img.png") ? GDefine.guilderDefault : guildClass.avatar,
                                // guildClass.avatar,//guildClass.tourist_img.length>0?  guildClass.tourist_img[0]:guildClass.avatar,
                                fit: BoxFit.fill ,
                                placeholder: (context, url) => AspectRatio(
                                  aspectRatio: 1,
                                  child: FractionallySizedBox(
                                    heightFactor: 0.5,
                                    widthFactor: 0.5,
                                    child: SpinKitRing(
                                      lineWidth:5,
                                      color:
                                      OwnColors.tran ,
                                    ),
                                  ),
                                ),
                                errorWidget: (context, url, error) => Icon(Icons.error,color: OwnColors.black.withOpacity(0.5),),
                                imageBuilder: (context, imageProvider) => Container(

                                  decoration: BoxDecoration(
                                    color: OwnColors.tran,
                                    borderRadius: BorderRadius.all(Radius.circular(15.0)),

                                    image: DecorationImage(

                                        image: imageProvider, fit: BoxFit.cover),
                                  ),
                                ),
                                // width: MediaQuery.of(context).size.width,

                              ),
                              Positioned.fill(
                                bottom: 0,
                                left: 0,
                                child: Column(
                                  children: [
                                    Spacer(),
                                    Container(


                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(15),
                                              bottomRight: Radius.circular(15)),
                                          gradient: LinearGradient(
                                            begin: Alignment.topCenter,
                                            end: Alignment.bottomCenter,
                                            colors: [
                                              OwnColors.C797979
                                                  .withOpacity(0.0),
                                              OwnColors.C787878
                                                  .withOpacity(0.0073),
                                              OwnColors.C3E3E3E
                                                  .withOpacity(0.3391),
                                              Colors.black.withOpacity(0.7),
                                            ],
                                          )),
                                      child: Row(
                                        children: [
                                          Expanded(

                                            child: Padding(
                                              padding: const EdgeInsets.only(top:14,right: 0,bottom: 8,left: 8),
                                              child: Text(
                                                guildClass.name,

                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                                style:GoogleFonts.inter (
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 16.nsp,
                                                    color: OwnColors.white),
                                              ),
                                            ),
                                          ),
                                          Visibility(
                                            visible: guildClass.want_companion == "1",
                                            child: Padding(
                                              padding: const EdgeInsets.only(top:14,right: 10,bottom: 8,left: 8),
                                              child: SizedBox(height:0.025.sh,child: Image.asset(Res.icon_companion)),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),

                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Spacer(flex: 15,),
                  Expanded(
                    flex: 150,
                    child: Center(
                      child: Padding(
                        padding: const EdgeInsets.only(top:8.0),
                        child: Column(

                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [

                                // Container(
                                //   height: 0.02 * ScreenSize().getHeight(context),
                                //   child: Padding(
                                //     padding: const EdgeInsets.only(top:2.0,bottom: 2.0,left: 4),
                                //     child: Image.asset(Res.star),
                                //   ),
                                // ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 4.0,left: 3),
                                  child: Container(width:0.04.sw,height: 0.017.sh,child: Icon(SFSymbols.star_fill, color: OwnColors.CTourOrange,size: 17,)),
                                ),
                                Container(width: 5,),

                                Expanded(
                                  child: Row(
                                    
                                          children:[

                                            Flexible(
                                              child: Text(
                                                 guildClass.score,
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                                style: GoogleFonts.inter(
                                                    fontSize: 12.nsp,
                                                    color: OwnColors.black,


                                                ),
                                              ),
                                            ),


                                            Flexible(
                                              child: Text(
                                                " (" +guildClass.comment_num+

                                                    (int.parse(guildClass.comment_num)>1? S().home_rate_number:S().home_rate_number_single)

                                                    +")",
                                                overflow: TextOverflow.ellipsis,
                                                maxLines: 1,
                                                textAlign: TextAlign.center,
                                                style: GoogleFonts.inter(
                                                    fontSize: 10.nsp,
                                                    height: 1.2,
                                                    color: OwnColors.black,
                                                    fontWeight: FontWeight.w300

                                                ),
                                              ),

                                            )

                                          ]


                                  ),
                                )
                              ],
                            ),
                            HorizontalSpace().create(context, 0.005),
                            Row(

                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                //
                                // Container(
                                //   height: 0.02* ScreenSize().getHeight(context),
                                //   child: Padding(
                                //     padding: const EdgeInsets.only(top:2.0,bottom: 2.0,right: 4),
                                //     child: Image.asset(Res.location),
                                //   ),
                                // ),
                                Padding(
                                  padding: const EdgeInsets.only(bottom: 0.0,left: 4,top: 2.0),
                                  child: Container(width:0.04.sw,height: 0.017.sh,child:Image.asset(Res.icon_global)),
                                ),
                                Container(width: 2,),
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left:2.0),
                                    child: Text(
                                      guildClass.lang.join(" | ")+ "",
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: GoogleFonts.inter(
                                        fontSize: 12.nsp,
                                        fontWeight: FontWeight.w400,
                                        color: OwnColors.black,

                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),

                            Visibility(
                              visible:  !guildClass.trans.contains("3"),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  HorizontalSpace().create(context, 0.004),
                                  Row(

                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [

                                      // Container(
                                      //   height: 0.02* ScreenSize().getHeight(context),
                                      //   child: Padding(
                                      //     padding: const EdgeInsets.only(top:2.0,bottom: 2.0,right: 4),
                                      //     child: Image.asset(Res.car),
                                      //   ),
                                      // ),
                                      Padding(
                                        padding: const EdgeInsets.only(top:2.0,bottom: 2.0,left: 4),
                                        child: Container(width:0.04.sw,height: 0.017.sh,child: Image.asset(Res.icon_car)),
                                      ),
                                      Container(width: 2,),
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.only(left:2.0),
                                          child: Text(
                                            !guildClass.trans.contains("3") ?
                                            S().home_have_car:S().home_have_no_car,

                                            style: GoogleFonts.inter(
                                              fontWeight: FontWeight.w400,
                                              fontSize: 12.nsp,
                                              color: OwnColors.black,

                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            HorizontalSpace().create(context, 0.008),
                            Align(
                              alignment: Alignment.topLeft,
                              child: Visibility(
                                visible: guildClass.lang.length > 0,
                                child: ExtendedWrap(

                                  spacing: 8,
                                  maxLines: 2,
                                  runSpacing : 5.0,
                                  children: List.generate(
                                    // guildClass.location.length
                                    guildClass.location.length
                                    ,

                                        (i) {
                                      return Container(
                                          height: 0.023*ScreenSize().getHeight(context),
                                          decoration: BoxDecoration(
                                              color: OwnColors.white,
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(10),
                                                  topRight: Radius.circular(10),
                                                  bottomLeft: Radius.circular(10),
                                                  bottomRight: Radius.circular(10)),
                                              border: Border.all(
                                                  width: 0.5,color: OwnColors.CC8C8C8
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: OwnColors.primaryText.withOpacity(0.4),
                                                  spreadRadius: 1,
                                                  blurRadius: 4,
                                                  offset: Offset(0, 3), // changes position of shadow
                                                ),
                                              ]),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(left:8.0,right: 8,bottom: 0),
                                                child: Text(guildClass.location[i],
                                                  textAlign: TextAlign.center,
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis,
                                                  style: GoogleFonts.inter(
                                                      height: 1.2,
                                                      fontSize: 12.nsp,
                                                      color: OwnColors.black,
                                                    fontWeight: FontWeight.w400
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ));
                                    },
                                  ),
                                ),
                              ),
                            )

                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
              HorizontalSpace().create(context, 0.01),
            ],
          ),
        ),
      ),
    );
  }



}
