import 'package:flutter/cupertino.dart';

import '../tool/ScreenSize.dart';
import 'HorizontalSpace.dart';
import 'TopBar.dart';

class TopbarWithShadow {
  Column create(BuildContext context, void backClick(), void save(), void delete()){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        HorizontalSpace().create(context, 0.02),
        Container(
          height: ScreenSize().getHeight(context) * 0.05,
          child: Topbar().noTitleCreateWithSaveAndDelete(context,ScreenSize().getHeight(context) * 0.073,  backClick ,save ,delete),

        ),
        HorizontalSpace().create(context, 0.02),
        //Divider(height: 1.0,indent: 0,color: OwnColors.Black30,),
        Container(
          child: Image.asset("images/boxshadow.png"),
        ),
        //HorizontalSpace().create(context, FigmaToScreen().getHeightRatio(context,null, 23)),


      ],
    );
  }
}