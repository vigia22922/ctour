
import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:flutter/material.dart';
import 'package:ctour/tool/OwnColors.dart';

class BuildTitleText {




  Container  create(BuildContext context, String title, int maxline, double height ,TextEditingController editingController) {
    Color color = Theme.of(context).primaryColor;

    return Container(
      decoration: BoxDecoration(
          color: OwnColors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(13),
              topRight: Radius.circular(13),
              bottomLeft: Radius.circular(13),
              bottomRight: Radius.circular(13)),
          boxShadow: [
            BoxShadow(
              color: OwnColors.primaryText.withOpacity(0.4),
              spreadRadius: 0.5,
              blurRadius: 5,
              offset: Offset(3, 4), // changes position of shadow
            ),
          ]),
      child: Row(

        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,

        //列布局
        children: <Widget>[

          Expanded(
            flex: 5, //比例

            child: Container(
              height: height*0.8,
              padding: EdgeInsets.only(left: 2,right: 2),
              width: double.infinity,
              decoration: BoxDecoration(

                border: Border(
                  right: BorderSide(color: OwnColors.ironGray,width: 2)
                )
              ),
              child: Center(
                child: AutoSizeText(
                  title,
                  minFontSize: 10,
                  maxFontSize: 30,
                  style: TextStyle(
                    color: OwnColors.ironGray.withOpacity(1),
                    fontWeight: FontWeight.bold,
                    fontSize: 30,
                  ),
                  maxLines: maxline,
                ),
              ),
            ),
          ),
          Spacer(
            flex: 1,
          ),
          Expanded(
            flex: 8, //比例
            child: Container(
              height: height,

              alignment: Alignment.center,
              child: AutoSizeTextField(
                controller: editingController,

                decoration: InputDecoration(

                    fillColor: OwnColors.tran,
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                    //Change this value to custom as you like
                    // isDense: true,
                    filled: true,
                    enabledBorder: OutlineInputBorder(
                      /*边角*/
                      borderRadius: BorderRadius.all(
                        Radius.circular(15), //边角为30
                      ),
                      borderSide: BorderSide(
                        color: Colors.grey, //边线颜色为黄色
                        width: 2, //边线宽度为2
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(15), //边角为30
                      ),
                      borderSide: BorderSide(
                        color: Colors.grey, //边线颜色为黄色
                        width: 2, //边线宽度为2
                      ),
                    )),
                minFontSize: 10,
                maxFontSize: 20,
                style: TextStyle(color: Colors.black.withOpacity(1), fontSize: 30),
                maxLines: 1,
                enabled: false,

              ),
              // child: AutoSizeTextField(
              //   controller: editingController,
              //
              //   decoration: InputDecoration(
              //
              //       hintText: hint,
              //       fillColor: OwnColors.white,
              //       contentPadding: EdgeInsets.symmetric(horizontal: 10),
              //       //Change this value to custom as you like
              //       // isDense: true,
              //       filled: true,
              //       enabledBorder: OutlineInputBorder(
              //         /*边角*/
              //         borderRadius: BorderRadius.all(
              //           Radius.circular(15), //边角为30
              //         ),
              //         borderSide: BorderSide(
              //           color: Colors.grey, //边线颜色为黄色
              //           width: 2, //边线宽度为2
              //         ),
              //       ),
              //       focusedBorder: OutlineInputBorder(
              //         borderRadius: BorderRadius.all(
              //           Radius.circular(15), //边角为30
              //         ),
              //         borderSide: BorderSide(
              //           color: Colors.grey, //边线颜色为黄色
              //           width: 2, //边线宽度为2
              //         ),
              //       )),
              //   minFontSize: 10,
              //   maxFontSize: 30,
              //   style: TextStyle(color: Colors.black.withOpacity(1), fontSize: 30),
              //   maxLines: 1,
              //   keyboardType: inputType,
              //   obscureText: secure,
              //
              // ),
            ),
          ),
        ],
      ),
    );
  }

}