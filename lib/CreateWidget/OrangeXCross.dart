import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/tool/FigmaToScreen.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:google_fonts/google_fonts.dart';

class OrangeXCross {
  Container create(BuildContext context) {

      return Container(
        width: 0.024.sh,
        height: 0.024.sh,

        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: OwnColors.white
              ),
            ),
            Image.asset(Res.xmark_fill),
          ],
        ),
      );

  }
  Container createWH(BuildContext context, double a) {

    return Container(
      width:a,
      height: a,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: OwnColors.CTourOrange
      ),

      child:  Icon(Icons.close,color: OwnColors.white,size:a/0.024 * 0.02,),
    );

  }



}
