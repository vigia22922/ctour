import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Travel/TravelClass.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/DateTools.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';


class TravelCard {
  Widget recommendCard(BuildContext context,TravelClass travelClass, int index, Function recommendClick,
      double shadowWidth) {
    return GestureDetector(
      onTap: () => {recommendClick(travelClass)},
      child: Container(
        width: 0.9 * ScreenSize().getWidth(context),


        child: Container(
          decoration: BoxDecoration(
            color: OwnColors.white,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(15),
              topLeft: Radius.circular(15),
              bottomLeft: Radius.circular(15),
              bottomRight: Radius.circular(15),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              HorizontalSpace().create(context, 0.015),
              AspectRatio(
                aspectRatio: 16/9,
                child: Container(
                  width: 0.9 * ScreenSize().getWidth(context),
                  child: Stack(
                    children: [
                      CachedNetworkImage(
                        imageUrl: travelClass.cover_image,
                        fit: BoxFit.fill,
                        placeholder: (context, url) => AspectRatio(
                          aspectRatio: 2.16,
                          child: FractionallySizedBox(
                            heightFactor: 0.5,
                            widthFactor: 0.5,
                            child: SpinKitRing(
                              color:
                              OwnColors.tran ,
                            ),
                          ),
                        ),
                        errorWidget: (context, url, error) => Icon(
                          Icons.error,
                          color: OwnColors.black.withOpacity(0.5),
                        ),
                        imageBuilder: (context, imageProvider) =>
                            Container(
                              width: 0.9 * ScreenSize().getWidth(context),
                              decoration: BoxDecoration(
                                color: OwnColors.tran,
                                borderRadius: BorderRadius.all(
                                    Radius.circular(15.0)),
                                image: DecorationImage(
                                    image: imageProvider,
                                    fit: BoxFit.cover),
                              ),
                            ),
                        // width: MediaQuery.of(context).size.width,
                      ),
                      // Positioned(
                      //   right: 10,
                      //   top: 10,
                      //   child: Container(
                      //       height: 0.03 *
                      //           ScreenSize().getHeight(context),
                      //       child: Image.asset(Res.favorite)),
                      // ),
                      Positioned(
                        bottom: 0,
                        // right: 5,
                        child: Container(
                          height: 0.13.sh,
                          width: shadowWidth,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(15),
                                  bottomRight: Radius.circular(15)),
                              gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                                  OwnColors.C797979
                                      .withOpacity(0.0),
                                  OwnColors.C787878
                                      .withOpacity(0.0073),
                                  OwnColors.C3E3E3E
                                      .withOpacity(0.3391),
                                  Colors.black.withOpacity(0.7),
                                ],
                              )),
                          child: Center(
                            child: Container(
                              width: 0.80 *
                                  ScreenSize().getWidth(context),
                              child: Column(
                                mainAxisAlignment:
                                MainAxisAlignment.start,
                                crossAxisAlignment:
                                CrossAxisAlignment.start,
                                children: [
                                  Spacer(
                                    flex: 10,
                                  ),
                                  Text(
                                    travelClass.type == "1"?
                                    travelClass.duration.toString() + S().home_filter_type2_string :
                                    travelClass.type == "2"? S().home_filter_type3 :
                                    S(). home_filter_type4,

                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style:GoogleFonts.inter (
                                        fontSize: 12.nsp,
                                        fontWeight: FontWeight.w300,
                                        color: OwnColors.white),
                                  ),
                                  Text(
                                    travelClass.title,

                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style:GoogleFonts.inter (
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16.nsp,
                                        color: OwnColors.white),
                                  ),
                                  HorizontalSpace()
                                      .create(context, 0.008),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),

                      Container(
                        height: 0.1 .sh,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15),
                                topRight: Radius.circular(15)),
                            gradient: LinearGradient(
                              begin: Alignment.bottomCenter ,
                              end: Alignment.topCenter,
                              colors: [
                                OwnColors.C797979
                                    .withOpacity(0.0),
                                OwnColors.C787878
                                    .withOpacity(0.0073),
                                OwnColors.C3E3E3E
                                    .withOpacity(0.3391),
                                Colors.black.withOpacity(0.7),
                              ],
                            )),
                        child: Padding(
                          padding: const EdgeInsets.only(top:10,left: 10),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: 0.045 .sh,
                                child: AspectRatio(
                                  aspectRatio: 1,
                                  child: CachedNetworkImage(
                                    placeholder: (context, url) => AspectRatio(
                                      aspectRatio: 2.16,
                                      child: FractionallySizedBox(
                                        heightFactor: 0.5,
                                        widthFactor: 0.5,
                                        child: SpinKitRing(
                                          color:
                                          OwnColors.tran ,
                                        ),
                                      ),
                                    ),
                                    errorWidget:
                                        (context, url, error) =>
                                        Icon(
                                          Icons.error,
                                          color: Colors.red,
                                        ),
                                    imageUrl: travelClass.guilder_avatar,
                                    fit: BoxFit.fitHeight,
                                    imageBuilder: (context,
                                        imageProvider) =>
                                        Container(
                                          // width: 80.0,
                                          // height: 80.0,
                                          decoration: BoxDecoration(
                                            color: OwnColors.tran,
                                            shape: BoxShape.circle,
                                            border: Border.all(width: 1,color: OwnColors.CC8C8C8

                                            ),
                                            image: DecorationImage(
                                                image: imageProvider,
                                                fit: BoxFit.cover),
                                          ),
                                        ),
                                    // width: MediaQuery.of(context).size.width,
                                    // placeholder: (context, url) => new CircularProgressIndicator(),
                                    // placeholder: new CircularProgressIndicator(),
                                  ),
                                ),
                              ),

                              VerticalSpace().create(context, 0.017),
                              Expanded(
                                child: Container(
                                  height: 0.045 .sh,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Text(travelClass.guilder_name,
                                        maxLines: 1,

                                        overflow: TextOverflow.ellipsis,
                                        style:GoogleFonts.inter (
                                            fontSize: 16.nsp,
                                            color: OwnColors.white,
                                            height: 1.2,
                                            fontWeight: FontWeight.w500
                                        ),),
                                      // Row(
                                      //   mainAxisAlignment: MainAxisAlignment.start,
                                      //   crossAxisAlignment: CrossAxisAlignment.center,
                                      //   children: [
                                      //     Container(
                                      //       height: 0.02 .sh,
                                      //       child: Image.asset(Res.star),
                                      //     ),
                                      //     Container(width: 2,),
                                      //     Text(
                                      //       travelClass.guilder_rate  ,
                                      //
                                      //       style:GoogleFonts.inter (
                                      //           fontSize: 12.nsp,
                                      //           height: 1.2,
                                      //           color: OwnColors.white,
                                      //           fontWeight: FontWeight.w600
                                      //       ),
                                      //     ),
                                      //     Text(
                                      //       " (" + double.parse(travelClass.guilder_comment).toStringAsFixed(0)+S().home_rate_number+")",
                                      //
                                      //       style:GoogleFonts.inter (
                                      //           height: 1.2,
                                      //           fontSize: 12.nsp,
                                      //           color: OwnColors.white,
                                      //           fontWeight: FontWeight.w300
                                      //
                                      //       ),
                                      //     ),
                                      //   ],
                                      // ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              HorizontalSpace().create(context, 0.005),
              Container(
                width: 0.83 * ScreenSize().getWidth(context),
                height: 0.025 .sh,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [

                    Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 4.0, bottom: 4.0, right: 4),
                            child: Image.asset(Res.icon_people2),
                          ),
                          Flexible(
                            child: Text(
                              travelClass.views + " " + (int.parse(travelClass.views)>1? S().travel_views :  S().travel_views_single) + "",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style:GoogleFonts.inter (
                                fontSize: 12.nsp,
                                color: OwnColors.black,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 4.0, bottom: 4.0, right: 4, left: 8),
                            child: Image.asset(Res.icon_comment2),
                          ),
                          Flexible(
                            fit: FlexFit.loose,
                            child: Text(
                              travelClass.comment_num +
                                  " " +
                                  (int.parse(travelClass.comment_num) > 1 ? S().travel_comment : S().travel_comment_single)                                  ,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style:GoogleFonts.inter (
                                fontSize: 12.nsp,
                                color: OwnColors.black,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        DateTools().intlYMD( travelClass.date),
                        maxLines: 1,

                        style:GoogleFonts.inter (
                            fontSize: 12.nsp,
                            color: OwnColors.CCCCCCC,
                            fontWeight: FontWeight.w400
                        ),
                      ),
                    )
                  ],
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }



}
