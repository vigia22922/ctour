import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:flutter/material.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';


class BuildTitleEditText {
  Column create(
      BuildContext context,
      String title,
      Color titleColor,
      int maxline,
      String hint,
      bool secure,
      TextEditingController editingController,
      double height,
      TextInputType inputType,
      Color borderColor) {
    Color color = Theme.of(context).primaryColor;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      //列布局
      children: <Widget>[

        Expanded(
          flex: 19,
          child: Align(
            alignment: Alignment.centerLeft,
            child: AutoSizeText(
              title,
              minFontSize: 10,
              maxFontSize: 50,
              style:TextStyle (
                fontFamily : 'NotoSansTC',
                fontSize: 16.nsp,
                color: OwnColors.black,
                //textStyle: Theme.of(context).textTheme.b,
                fontWeight: FontWeight.w600,
                //fontStyle: FontStyle.normal
              ),
              maxLines: maxline,
            ),
          ),
        ),
        Spacer(flex: 10,),

        Expanded(
          flex: 30,
          child: Container(
            // height: height,
            decoration: BoxDecoration(
                color: OwnColors.white,
                border: Border(
                  top: BorderSide(width: 1.0, color: borderColor),
                  bottom: BorderSide(width: 1.0, color:borderColor),
                  left:BorderSide(width: 1.0, color: borderColor),
                  right: BorderSide(width: 1.0, color: borderColor),
                ),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10),
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10)),
 ),

            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.only(left: 4.0, top:6.0,bottom: 6),
              child: AutoSizeTextField(
                controller: editingController,

                decoration: InputDecoration(
                    hintText: hint,
                    fillColor: OwnColors.white,
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),

                    //Change this value to custom as you like
                    // isDense: true,
                    filled: true,
                    border: OutlineInputBorder(
                    borderRadius:
                    new BorderRadius.circular(20.0),
                    borderSide: BorderSide.none)


                ),
                minFontSize: 10,
                maxFontSize: 30,
                style:GoogleFonts.inter(
                  fontSize: 12.nsp,
                  color: Colors.black.withOpacity(1),
                  fontWeight: FontWeight.w300,
                  //fontStyle: FontStyle.normal
                ),
                maxLines: 1,
                keyboardType: inputType,
                obscureText: secure,
              ),
            ),
          ),
        ),
      ],
    );
  }



}
