import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
class VerticalSpace{

  Container create(BuildContext context, num weight) {
    return Container(
      // width: MediaQuery.of(context).size.width * 0.80,
      height: 1,

      width: MediaQuery
          .of(context)
          .size
          .width * weight,

    );
  }

  Container createWhite(BuildContext context, num weight) {
    return Container(
      // width: MediaQuery.of(context).size.width * 0.80,
      height: 1.sh,
      width: MediaQuery
          .of(context)
          .size
          .width * weight,

    );
  }

}
