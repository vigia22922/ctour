import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/tool/FigmaToScreen.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:google_fonts/google_fonts.dart';

class Topbar {
  Container create(BuildContext context, double height, String Title,
      Function back, bool whiteBorder) {
    if (whiteBorder) {
      return Container(
        width: 1.0 * ScreenSize().getWidth(context),
        decoration: BoxDecoration(
            color: OwnColors.LogoOrange,
            border: Border(
                bottom: BorderSide(color: OwnColors.TextBLue, width: 3))),
        child: Stack(
          alignment: Alignment.centerLeft,
          children: <Widget>[
            // child:
            // Stack(
            // alignment: Alignment.centerLeft,
            // children: [

            Container(
              width: 1.0 * ScreenSize().getWidth(context),
              height: 0.07 * ScreenSize().getHeight(context),
              color: OwnColors.tran,
              child: Center(
                child: AutoSizeText(
                  Title,
                  minFontSize: 10,
                  maxFontSize: 40,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: OwnColors.TextBLue.withOpacity(1),
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                  ),
                  maxLines: 1,
                ),
              ),
            ),

            Container(
              width: 0.15 * ScreenSize().getWidth(context),
              height: 0.04 * ScreenSize().getHeight(context),
              child: FlatButton(
                padding: EdgeInsets.all(0.0),
                color: OwnColors.tran,
                textColor: OwnColors.white,
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  // side: BorderSide(color: Colors.red)
                ),
                child: Container(
                  width: 1.5 * ScreenSize().getWidth(context),
                  // height: MediaQuery.of(context).size.height * 0.07,
                  // child: Image.asset(Res.back_arrow),
                ),
                onPressed: back,
              ),
            ),
          ],
        ),
      );
    } else {
      return Container(
          width: 0.5 * ScreenSize().getWidth(context),
          height: height,
          color: OwnColors.LogoOrange,
          alignment: Alignment.center,
          // color: Colors.blue,
          child: Container(
            width: 1.0 * ScreenSize().getWidth(context),
            child: Stack(
              alignment: Alignment.centerLeft,
              children: <Widget>[
                // child:
                // Stack(
                // alignment: Alignment.centerLeft,
                // children: [

                Container(
                  width: 1.0 * ScreenSize().getWidth(context),
                  height: 0.07 * ScreenSize().getHeight(context),
                  color: OwnColors.LogoOrange,
                  child: Center(
                    child: AutoSizeText(
                      Title,
                      minFontSize: 10,
                      maxFontSize: 40,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black.withOpacity(1),
                        fontWeight: FontWeight.bold,
                        fontSize: 40,
                      ),
                      maxLines: 1,
                    ),
                  ),
                ),

                Container(
                  width: 0.15 * ScreenSize().getWidth(context),
                  height: 0.08 * ScreenSize().getHeight(context),
                  child: FlatButton(
                    splashColor: Colors.transparent,
                    highlightColor: Colors.transparent,
                    padding: EdgeInsets.all(0.0),
                    color: OwnColors.white,
                    textColor: OwnColors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                      // side: BorderSide(color: Colors.red)
                    ),
                    child: Container(
                      width: 1.5 * ScreenSize().getWidth(context),
                      // height: MediaQuery.of(context).size.height * 0.07,
                      // child: Image.asset(Res.back_arrow),
                    ),
                    onPressed: back,
                  ),
                ),
              ],
            ),
          ));
    }
  }

  Container createAdd(BuildContext context, String title, double height,
      Function back, Function add) {
    return Container(
      width: 0.95.sw,
      child: Row(
        children: <Widget>[
          // child:
          // Stack(
          // alignment: Alignment.centerLeft,
          // children: [

          AspectRatio(
            aspectRatio: 1,
            child: Container(
              // width: 0.1025 * ScreenSize().getWidth(context),
              height: double.infinity,
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                padding: EdgeInsets.all(0.0),
                color: OwnColors.tran,
                textColor: OwnColors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  // side: BorderSide(color: Colors.red)
                ),
                child: Container(
                  width: 1.5 * ScreenSize().getWidth(context),
                  // height: MediaQuery.of(context).size.height * 0.07,
                  child: Icon(SFSymbols.chevron_left, color: OwnColors.black),
                ),
                onPressed: back,
              ),
            ),
          ),
          VerticalSpace().create(context, 0.04),
          Expanded(
              child: Text(
            title,

            textAlign: TextAlign.left,
            style: GoogleFonts.inter(
              color: OwnColors.black.withOpacity(1),
              fontWeight: FontWeight.w700,
              fontSize: 24.nsp,
            ),
            maxLines: 1,
          )),
          AspectRatio(
            aspectRatio: 1,
            child: Container(
              // width: 0.1025 * ScreenSize().getWidth(context),
              height: double.infinity,
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                padding: EdgeInsets.all(0.0),
                color: OwnColors.tran,
                textColor: OwnColors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  // side: BorderSide(color: Colors.red)
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top: 4.0, bottom: 4.0),
                  child: Container(
                    width: 1.5 * ScreenSize().getWidth(context),
                    // height: MediaQuery.of(context).size.height * 0.07,
                    child: Icon(SFSymbols.plus, color: OwnColors.black,),
                  ),
                ),
                onPressed: add,
              ),
            ),
          )
        ],
      ),
    );
  }

  Container noTitleCreate(BuildContext context, double height, Function back) {
    return Container(
      width: 0.95.sw,
      child: Row(
        children: <Widget>[
          // child:
          // Stack(
          // alignment: Alignment.centerLeft,
          // children: [
          AspectRatio(
            aspectRatio: 1,
            child: Container(
              // width: 0.1025 * ScreenSize().getWidth(context),
              height: double.infinity,
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                padding: EdgeInsets.all(0.0),
                color: OwnColors.tran,
                textColor: OwnColors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  // side: BorderSide(color: Colors.red)
                ),
                child: Container(
                  height: 0.048.sh,
                  // height: MediaQuery.of(context).size.height * 0.07,
                  child: Icon(SFSymbols.chevron_left, color: OwnColors.black),
                ),
                onPressed: back,
              ),
            ),
          ),
          Spacer(
            flex: 1,
          )
        ],
      ),
    );
  }

  Container noTitleBorderCreate(
      BuildContext context, double height, Function back) {
    return Container(
      width: 1.sw,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: OwnColors.CF4F4F4),
        ),
      ),
      child: Center(
        child: Container(
          width: 0.95.sw,
          height: 0.067.sh,
          // decoration: BoxDecoration(
          //   border: Border(
          //     bottom: BorderSide(width: 1.0, color: OwnColors.CF4F4F4),
          //   ),
          // ),
          child: Row(
            children: <Widget>[
              // child:
              // Stack(
              // alignment: Alignment.centerLeft,
              // children: [
              Container(
                width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    height: 0.048.sh,
                    child: Icon(SFSymbols.chevron_left, color: OwnColors.black),
                  ),
                  onPressed: back,
                ),
              ),
              Spacer(
                flex: 1,
              )
            ],
          ),
        ),
      ),
    );
  }

  Container noTitleCreateWithSaveAndDelete(BuildContext context, double height,
      Function back, Function save, Function delete) {
    return Container(
      width: ScreenSize().getWidth(context),
      child: Row(
        children: <Widget>[
          // child:
          // Stack(
          // alignment: Alignment.centerLeft,
          // children: [
          VerticalSpace().create(context, FigmaToScreen().getWidthRatio(9.5)),
          AspectRatio(
              aspectRatio: 1,
              child: Container(
                // width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                    child: Image.asset(Res.back_arrow),
                  ),
                  onPressed: back,
                ),
              )),
          Spacer(
            flex: 1,
          ),
          SizedBox(
            width: 32,
            child: TextButton(
                style: ButtonStyle(
                  padding: MaterialStateProperty.all(EdgeInsets.zero),
                ),
                onPressed: () {
                  delete();
                },
                child: Text("刪除",
                    style: TextStyle(
                      fontSize: 16,
                      decoration: TextDecoration.underline,
                      color: OwnColors.black,
                    ))),
          ),
          VerticalSpace().create(context, FigmaToScreen().getWidthRatio(12)),
          SizedBox(
            width: FigmaToScreen().getPixelWidth(context, 65),
            child: TextButton(
                style: ButtonStyle(
                    padding: MaterialStateProperty.all(EdgeInsets.zero),
                    shape: MaterialStateProperty.all(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.67))),
                    backgroundColor:
                        MaterialStateProperty.all(OwnColors.CTourOrange)),
                child: Text(S().personal_edit_save,
                    style: TextStyle(
                      fontSize: 16,
                      color: OwnColors.white,
                    )),
                onPressed: () {
                  save();
                }),
          ),
          VerticalSpace().create(context, FigmaToScreen().getWidthRatio(14)),
        ],
      ),
    );
  }

  Container titleCreate(
      BuildContext context, String title, double height, Function back, bool border ) {
    return Container(
      width: 1.sw,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: border ? OwnColors.CF4F4F4 : OwnColors.tran),
        ),
      ),
      child: Center(
        child: Container(
          width: 0.95.sw,
          height: 0.067.sh,

          child: Row(
            children: <Widget>[
              // child:
              // Stack(
              // alignment: Alignment.centerLeft,
              // children: [

              Container(
                width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    height: 0.048.sh,
                    child: Icon(SFSymbols.chevron_left, color: OwnColors.black),
                  ),
                  onPressed: back,
                ),
              ),
              VerticalSpace().create(context, 0.04),
              Expanded(
                  child: AutoSizeText(
                title,
                minFontSize: 10,
                maxFontSize: 40,
                textAlign: TextAlign.left,
                style: GoogleFonts.inter(
                  color: OwnColors.black.withOpacity(1),
                  fontWeight: FontWeight.w700,
                  fontSize: 24.nsp,
                ),
                maxLines: 1,
              ))
            ],
          ),
        ),
      ),
    );
  }

  Container titleCreateMenu(
      BuildContext context, String title, double height, Function back, bool border,Function showMenu ) {
    return Container(
      width: 1.sw,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: border ? OwnColors.CF4F4F4 : OwnColors.tran),
        ),
      ),
      child: Center(
        child: Container(
          width: 0.95.sw,
          height: 0.067.sh,

          child: Row(
            children: <Widget>[
              // child:
              // Stack(
              // alignment: Alignment.centerLeft,
              // children: [

              Container(
                width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    height: 0.048.sh,
                    child: Icon(SFSymbols.chevron_left, color: OwnColors.black),
                  ),
                  onPressed: back,
                ),
              ),
              VerticalSpace().create(context, 0.04),
              Expanded(
                  child: AutoSizeText(
                    title,
                    minFontSize: 10,
                    maxFontSize: 40,
                    textAlign: TextAlign.left,
                    style: GoogleFonts.inter(
                      color: OwnColors.black.withOpacity(1),
                      fontWeight: FontWeight.w700,
                      fontSize: 24.nsp,
                    ),
                    maxLines: 1,
                  )),
              Container(
                width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: Material(
                  color: Colors.white, // button color
                  child: GestureDetector(

                    child: SizedBox(
                        width: 0.043 .sh,
                        height:
                        0.043.sh,
                        child: Center(
                          child: Padding(
                              padding: const EdgeInsets.only(top:0.0, left: 4.0,right: 4.0),
                              child:
                              Image.asset(Res.icon_menu)
                            // Icon(SFSymbols.menu, color: OwnColors.black,size: 0.03.sh,)
                            // Image.asset(Res.black_heart):。
                            // Image.asset(Res.collected_heart)
                          ),
                        )),
                    onTap: () {
                      showMenu();
                    },
                  ),
                ),
              )

            ],
          ),
        ),
      ),
    );
  }


  Container titleCreateMenuChat(
      BuildContext context, String title, double height, Function back, bool border,Function showMenu,Function goPerson ) {
    return Container(
      width: 1.sw,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: border ? OwnColors.CF4F4F4 : OwnColors.tran),
        ),
      ),
      child: Center(
        child: Container(
          width: 0.95.sw,
          height: 0.067.sh,

          child: Row(
            children: <Widget>[
              // child:
              // Stack(
              // alignment: Alignment.centerLeft,
              // children: [

              Container(
                width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    height: 0.048.sh,
                    child: Icon(SFSymbols.chevron_left, color: OwnColors.black),
                  ),
                  onPressed: back,
                ),
              ),
              VerticalSpace().create(context, 0.04),
              Expanded(
                  child: GestureDetector(
                    onTap: (){
                      goPerson();
                    },
                    child: AutoSizeText(
                      title,
                      minFontSize: 10,
                      maxFontSize: 40,
                      textAlign: TextAlign.left,
                      style: GoogleFonts.inter(
                        color: OwnColors.black.withOpacity(1),
                        fontWeight: FontWeight.w700,
                        fontSize: 24.nsp,
                      ),
                      maxLines: 1,
                    ),
                  )),
              Container(
                width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: Material(
                  color: Colors.white, // button color
                  child: GestureDetector(

                    child: SizedBox(
                        width: 0.043 .sh,
                        height:
                        0.043.sh,
                        child: Center(
                          child: Padding(
                              padding: const EdgeInsets.only(top:0.0, left: 4.0,right: 4.0),
                              child:
                              Image.asset(Res.icon_menu)
                            // Icon(SFSymbols.menu, color: OwnColors.black,size: 0.03.sh,)
                            // Image.asset(Res.black_heart):。
                            // Image.asset(Res.collected_heart)
                          ),
                        )),
                    onTap: () {
                      showMenu();
                    },
                  ),
                ),
              )

            ],
          ),
        ),
      ),
    );
  }


  Container saveCreate(
      BuildContext context, Function back, Function save, Function preview) {
    return Container(
      width: 1.sw,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: OwnColors.CF4F4F4),
        ),
      ),
      child: Center(
        child: Container(
          width: 0.95.sw,
          height: 0.067.sh,

          child: Row(
            children: <Widget>[
              // child:
              // Stack(
              // alignment: Alignment.centerLeft,
              // children: [

              Container(
                width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    height: 0.048.sh,
                    child: Icon(SFSymbols.chevron_left, color: OwnColors.black),
                  ),
                  onPressed: back,
                ),
              ),
              Spacer(),

              GestureDetector(
                onTap: () {
                  preview();
                },
                child: Text(
                  S().personal_become_guilder_preview,
                  textAlign: TextAlign.left,
                  style: GoogleFonts.inter(
                    fontSize: 16,
                    color: OwnColors.black,
                    fontWeight: FontWeight.w700,
                    decoration: TextDecoration.underline,
                  ) /*TextStyle(
                      color: OwnColors.black.withOpacity(1),
                      fontWeight: FontWeight.bold,
                      fontSize: 16.nsp,
                      decoration: TextDecoration.underline)*/
                  ,
                  maxLines: 1,
                ),
              ),
              Container(
                width: 14,
              ),
              GestureDetector(
                onTap: () {
                  save();
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: OwnColors.CTourOrange,
                    borderRadius: BorderRadius.all(Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 15.5, right: 15.5, top: 4.5, bottom: 4.5),
                    child: Text(
                      S().personal_become_guilder_save,
                      textAlign: TextAlign.left,
                      style: GoogleFonts.inter(
                        color: OwnColors.white.withOpacity(1),
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                      ),
                      maxLines: 1,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Container saveDeleteCreate(
      BuildContext context, Function back, Function save, Function delete) {
    return Container(
      width: 1.sw,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: OwnColors.CF4F4F4),
        ),
      ),
      child: Center(
        child: Container(
          width: 0.95.sw,
          height: 0.067.sh,
          child: Row(
            children: <Widget>[
              // child:
              // Stack(
              // alignment: Alignment.centerLeft,
              // children: [

              Container(
                width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    height: 0.048.sh,
                    child: Icon(SFSymbols.chevron_left, color: OwnColors.black),
                  ),
                  onPressed: back,
                ),
              ),
              Spacer(),
              GestureDetector(
                onTap: () {
                  delete();
                },
                child: Text(
                  S().delete,
                  textAlign: TextAlign.left,
                  style: GoogleFonts.inter(
                      color: OwnColors.black.withOpacity(1),
                      fontWeight: FontWeight.w700,
                      fontSize: 16.nsp,
                      decoration: TextDecoration.underline),
                  maxLines: 1,
                ),
              ),
              Container(
                width: 14,
              ),
              GestureDetector(
                onTap: () {
                  save();
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: OwnColors.CTourOrange,
                    borderRadius: BorderRadius.all(Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 15.5, right: 15.5, top: 4.5, bottom: 4.5),
                    child: Text(
                      S().personal_become_guilder_save,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: OwnColors.white.withOpacity(1),
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                      maxLines: 1,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Container saveDeleteCreateTextLength(
      BuildContext context, Function back, Function save, Function delete, bool value) {
    return Container(
      width: 1.sw,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: OwnColors.CF4F4F4),
        ),
      ),
      child: Center(
        child: Container(
          width: 0.95.sw,
          height: 0.067.sh,
          child: Row(
            children: <Widget>[
              // child:
              // Stack(
              // alignment: Alignment.centerLeft,
              // children: [

              Container(
                width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    height: 0.048.sh,
                    child: Icon(SFSymbols.chevron_left, color: OwnColors.black),
                  ),
                  onPressed: back,
                ),
              ),
              Spacer(),
              GestureDetector(
                onTap: () {
                  delete();
                },
                child: Text(
                  S().delete,
                  textAlign: TextAlign.left,
                  style: GoogleFonts.inter(
                      color: OwnColors.black.withOpacity(1),
                      fontWeight: FontWeight.w700,
                      fontSize: 16.nsp,
                      decoration: TextDecoration.underline),
                  maxLines: 1,
                ),
              ),
              Container(
                width: 14,
              ),
              GestureDetector(
                onTap: () {
                  save();
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: value?OwnColors.CTourOrange:OwnColors.CC8C8C8,
                    borderRadius: BorderRadius.all(Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 15.5, right: 15.5, top: 4.5, bottom: 4.5),
                    child: Text(
                      S().personal_become_guilder_save,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: OwnColors.white.withOpacity(1),
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                      maxLines: 1,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }


  Container savePreviewCreateTextLength(
      BuildContext context, Function back, Function save, Function delete, bool value) {
    return Container(
      width: 1.sw,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: OwnColors.CF4F4F4),
        ),
      ),
      child: Center(
        child: Container(
          width: 0.95.sw,
          height: 0.067.sh,
          child: Row(
            children: <Widget>[
              // child:
              // Stack(
              // alignment: Alignment.centerLeft,
              // children: [

              Container(
                width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    height: 0.048.sh,
                    child: Icon(SFSymbols.chevron_left, color: OwnColors.black),
                  ),
                  onPressed: back,
                ),
              ),
              Spacer(),
              GestureDetector(
                onTap: () {
                  delete();
                },
                child: Text(
                  S().preview,
                  textAlign: TextAlign.left,
                  style: GoogleFonts.inter(
                      color: OwnColors.black.withOpacity(1),
                      fontWeight: FontWeight.w700,
                      fontSize: 16.nsp,
                      decoration: TextDecoration.underline),
                  maxLines: 1,
                ),
              ),
              Container(
                width: 14,
              ),
              GestureDetector(
                onTap: () {
                  save();
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: value?OwnColors.CTourOrange:OwnColors.CC8C8C8,
                    borderRadius: BorderRadius.all(Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 15.5, right: 15.5, top: 4.5, bottom: 4.5),
                    child: Text(
                      S().personal_become_guilder_save,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: OwnColors.white.withOpacity(1),
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                      maxLines: 1,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Container savePreviewCreateTextLength3_2(
      BuildContext context, Function back, Function upload, Function save, Function delete, bool value) {
    return Container(
      width: 1.sw,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: OwnColors.CF4F4F4),
        ),
      ),
      child: Center(
        child: Container(
          width: 0.95.sw,
          height: 0.067.sh,
          child: Row(
            children: <Widget>[
              // child:
              // Stack(
              // alignment: Alignment.centerLeft,
              // children: [

              Container(
                width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    height: 0.048.sh,
                    child: Icon(SFSymbols.chevron_left, color: OwnColors.black),
                  ),
                  onPressed: back,
                ),
              ),
              Spacer(),
              GestureDetector(
                onTap: () {
                  delete();
                },
                child: Text(
                  S().preview,
                  textAlign: TextAlign.left,
                  style: GoogleFonts.inter(
                      color: OwnColors.black.withOpacity(1),
                      fontWeight: FontWeight.w700,
                      fontSize: 16.nsp,
                      decoration: TextDecoration.underline),
                  maxLines: 1,
                ),
              ),
              Container(
                width: 14,
              ),
              GestureDetector(
                onTap: () {
                  save();
                },
                child: Text(
                  S().save_draft,
                  textAlign: TextAlign.left,
                  style: GoogleFonts.inter(
                      color: OwnColors.black.withOpacity(1),
                      fontWeight: FontWeight.w700,
                      fontSize: 16.nsp,
                      decoration: TextDecoration.underline),
                  maxLines: 1,
                ),
              ),
              Container(
                width: 14,
              ),
              GestureDetector(
                onTap: () {
                  upload();
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: value?OwnColors.CTourOrange:OwnColors.CC8C8C8,
                    borderRadius: BorderRadius.all(Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 15.5, right: 15.5, top: 4.5, bottom: 4.5),
                    child: Text(
                      S().personal_become_guilder_upload,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: OwnColors.white.withOpacity(1),
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                      maxLines: 1,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Container savePreviewCreateTextLength3(
      BuildContext context, Function back, Function upload, Function save, Function delete, bool value) {
    return Container(
      width: 1.sw,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: OwnColors.CF4F4F4),
        ),
      ),
      child: Center(
        child: Container(
          width: 0.95.sw,
          height: 0.067.sh,
          child: Row(
            children: <Widget>[
              // child:
              // Stack(
              // alignment: Alignment.centerLeft,
              // children: [

              Container(
                width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    height: 0.048.sh,
                    child: Icon(SFSymbols.chevron_left, color: OwnColors.black),
                  ),
                  onPressed: back,
                ),
              ),
              Spacer(),
              GestureDetector(
                onTap: () {
                  delete();
                },
                child: Text(
                  S().preview,
                  textAlign: TextAlign.left,
                  style: GoogleFonts.inter(
                      color: OwnColors.black.withOpacity(1),
                      fontWeight: FontWeight.w700,
                      fontSize: 16.nsp,
                      decoration: TextDecoration.underline),
                  maxLines: 1,
                ),
              ),
              Container(
                width: 14,
              ),
              GestureDetector(
                onTap: () {
                  save();
                },
                child: Text(
                  S().personal_become_guilder_save,
                  textAlign: TextAlign.left,
                  style: GoogleFonts.inter(
                      color: OwnColors.black.withOpacity(1),
                      fontWeight: FontWeight.w700,
                      fontSize: 16.nsp,
                      decoration: TextDecoration.underline),
                  maxLines: 1,
                ),
              ),
              Container(
                width: 14,
              ),
              GestureDetector(
                onTap: () {
                  upload();
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: value?OwnColors.CTourOrange:OwnColors.CC8C8C8,
                    borderRadius: BorderRadius.all(Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 15.5, right: 15.5, top: 4.5, bottom: 4.5),
                    child: Text(
                      S().personal_become_guilder_upload,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: OwnColors.white.withOpacity(1),
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                      maxLines: 1,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Container savePreviewCreateTextLength4(
      BuildContext context, Function back, Function upload , Function preview, bool value) {
    return Container(
      width: 1.sw,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: OwnColors.CF4F4F4),
        ),
      ),
      child: Center(
        child: Container(
          width: 0.95.sw,
          height: 0.067.sh,
          child: Row(
            children: <Widget>[
              // child:
              // Stack(
              // alignment: Alignment.centerLeft,
              // children: [

              Container(
                width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    height: 0.048.sh,
                    child: Icon(SFSymbols.chevron_left, color: OwnColors.black),
                  ),
                  onPressed: back,
                ),
              ),
              Spacer(),
              GestureDetector(
                onTap: () {
                  preview();
                },
                child: Text(
                  S().preview,
                  textAlign: TextAlign.left,
                  style: GoogleFonts.inter(
                      color: OwnColors.black.withOpacity(1),
                      fontWeight: FontWeight.w700,
                      fontSize: 16.nsp,
                      decoration: TextDecoration.underline),
                  maxLines: 1,
                ),
              ),
              Container(
                width: 14,
              ),

              GestureDetector(
                onTap: () {
                  upload();
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: value?OwnColors.CTourOrange:OwnColors.CC8C8C8,
                    borderRadius: BorderRadius.all(Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 15.5, right: 15.5, top: 4.5, bottom: 4.5),
                    child: Text(
                      S().personal_become_guilder_save,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: OwnColors.white.withOpacity(1),
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                      maxLines: 1,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }


  Container starCreate(BuildContext context, String title, String title2,
      Function back, Function add) {
    return Container(
      width: 1.sw,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1.0, color: OwnColors.CF4F4F4),
        ),
      ),
      child: Center(
        child: Container(
          width: 0.95.sw,
          height: 0.067.sh,

          child: Row(
            children: <Widget>[

              Container(
                width: 0.1025 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    height: 0.048.sh,
                    child: Icon(SFSymbols.chevron_left, color: OwnColors.black),
                  ),
                  onPressed: back,
                ),
              ),

              Expanded(
                  child: Row(
                children: [
                  Icon(
                    Icons.star_rate,
                    color: Color.fromRGBO(255, 100, 78, 1),
                    size: 0.03.sh,
                    semanticLabel: 'Text to announce in accessibility modes',
                  ),
                  Expanded(
                    child: RichText(
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        text: TextSpan(
                          text: ' ',
                          style: TextStyle(color: Colors.black, fontSize: 20.nsp),
                          children: <TextSpan>[
                            TextSpan(
                                text: title,
                                style: GoogleFonts.inter(
                                  fontSize: 20.nsp,
                                  fontWeight: FontWeight.w800,
                                )),
                            TextSpan(
                                text: "  (" + title2 +           ( int.parse(title2) > 1 ?
                                S().home_rate_number :  S().home_rate_number_single) + ")",
                                style: GoogleFonts.inter(
                                    fontWeight: FontWeight.w500,
                                  fontSize: 20.nsp,)),
                          ],
                        )),
                  ),
                ],
              )),
              Container(
                width: 0.08 * ScreenSize().getWidth(context),
                height: double.infinity,
                child: FlatButton(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  padding: EdgeInsets.all(0.0),
                  color: OwnColors.tran,
                  textColor: OwnColors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                    // side: BorderSide(color: Colors.red)
                  ),
                  child: Container(
                    height: 0.048.sh,
                    child: Icon(SFSymbols.plus, color: OwnColors.black,size: 25,),
                  ),
                  onPressed: add,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget filterCleanCreate(BuildContext context, Function back,
      Function cleanClick, Function filterClick) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: 0.038 * ScreenSize().getHeight(context),
          width: 0.9 * ScreenSize().getWidth(context),
          child: Row(
            children: [
              InkWell(
                splashColor: OwnColors.tran,
                  onTap: () {
                    back();
                  },
                  child: Container(
                    height: double.infinity,
                    width: 0.1* ScreenSize().getWidth(context),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                            height: 0.02 * ScreenSize().getHeight(context),
                            child:  Icon(SFSymbols.xmark, color: OwnColors.black)),
                      ],
                    ),
                  )),
              Spacer(
                flex: 1,
              ),
              GestureDetector(
                onTap: () {
                  cleanClick();
                },
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(

                      bottom: BorderSide(width: 1.0, color:OwnColors.black),

                    ),
                    color: Colors.white,
                  ),
                  child: AutoSizeText(
                    S().home_filter_clean,
                    style: GoogleFonts.inter(
                        color: OwnColors.black,
                        fontSize: 16.nsp,
                        fontWeight: FontWeight.w700,
                       ),
                  ),
                ),
              ),
              VerticalSpace().create(context, 0.03),
              GestureDetector(
                onTap: () {
                  filterClick();
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: OwnColors.CTourOrange,
                    borderRadius: BorderRadius.all(Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 2.0, left: 10, right: 10, bottom: 2),
                    child: AutoSizeText(
                      S().home_filter_filter,
                      style: GoogleFonts.inter(
                          color: OwnColors.white,
                          fontSize: 16.nsp,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        HorizontalSpace().create(context, 0.02),
        Container(
          height: 1,
          width: 1 * ScreenSize().getWidth(context),
          color: OwnColors.CF4F4F4,
        ),
      ],
    );
  }

  Widget menuCreate(BuildContext context, Function back,
       ) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: 0.038 * ScreenSize().getHeight(context),
          width: 0.9 * ScreenSize().getWidth(context),
          child: Row(
            children: [
              InkWell(
                  splashColor: OwnColors.tran,
                  onTap: () {
                    back();
                  },
                  child: Container(
                    height: double.infinity,
                    width: 0.1* ScreenSize().getWidth(context),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                            height: 0.02 * ScreenSize().getHeight(context),
                            child:  Icon(SFSymbols.xmark, color: OwnColors.black)),
                      ],
                    ),
                  )),
              Spacer(
                flex: 1,
              ),

             ],
          ),
        ),
        HorizontalSpace().create(context, 0.02),
        Container(
          height: 1,
          width: 1 * ScreenSize().getWidth(context),
          color: OwnColors.CF4F4F4,
        ),
      ],
    );
  }


  Widget saveModalCreate(
      BuildContext context, Function back, Function filterClick) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: 0.038 * ScreenSize().getHeight(context),
          width: 0.9 * ScreenSize().getWidth(context),
          child: Row(
            children: [
              GestureDetector(
                  onTap: () {
                    back();
                  },
                  child: Container(
                      height: 0.02 * ScreenSize().getHeight(context),
                      child: Icon(SFSymbols.xmark))),
              Spacer(
                flex: 1,
              ),
              VerticalSpace().create(context, 0.03),
              GestureDetector(
                onTap: () {
                  filterClick();
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: OwnColors.tran,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 2.0, left: 10, right: 10, bottom: 2),
                    child: Container(
                      decoration: BoxDecoration(
                          color: OwnColors.tran,
                          border: Border(
                            bottom:
                                BorderSide(width: 1.0, color: OwnColors.black),
                          )),
                      child: AutoSizeText(
                        S().personal_edit_save,
                        style: GoogleFonts.inter(
                            color: OwnColors.black,
                            fontSize: 16.nsp,
                            fontWeight: FontWeight.w700,
                             ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        HorizontalSpace().create(context, 0.02),
        Container(
          height: 1,
          width: 1 * ScreenSize().getWidth(context),
          color: OwnColors.CF4F4F4,
        ),
      ],
    );
  }
}
