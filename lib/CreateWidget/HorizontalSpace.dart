import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';

import 'package:ctour/tool/ScreenSize.dart';
class HorizontalSpace{

  Container create(BuildContext context, num weight) {
    return Container(
    //   width: MediaQuery.of(context).size.width * 0.80,
    //   height: ScreenSize().getHeight(context) * weight,
      height: MediaQuery.of(context).size.height * weight,

    );
  }

  Container createWhite(BuildContext context, num weight) {
    return Container(
        width: MediaQuery.of(context).size.width  ,
      //   height: ScreenSize().getHeight(context) * weight,
      color: Colors.white,
      height: MediaQuery.of(context).size.height * weight,

    );
  }
}
