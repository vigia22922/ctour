class GDefine{

  static const String ip = "";
  // static const String apiUrl = "http://103.17.9.162:8888/api";
  // static const String apiUrl = "http://140.128.132.80/checkhouse/public/api";
  static const String apiUrl = "http://ctourapp.com/api";
  static const String imageUrl = "http://ctourapp.com/uploads/";
  // static const String imageUrl = "http://140.128.132.80/checkhouse/public/";
  // static const String imageUrl = "http://103.17.9.162:8888/";
  static const String deskey = "VJ2yMFF4N32khmkyVol1OBJhz2LRQfLje7v2kyZcQM8=";

  static const String login_account = apiUrl + "login.php";
  static const String guilderDefault = "https://ctourapp.com/uploads/images/50D4E1A4-67E9-69FE-A935-66E6B28E1921.jpg";

  //Main Data


  static const String FCMtoken = "FCMtoken";
  static const String version = "version";
  static const String buildNumber = "buildNumber";
  static const String account = "account";
  static const String user_id = "user_id";
  static const String avatar = "avatar";
  static const String login_type = "login_type";
  static const String pwd = "pwd";
  static const String name = "name";
  static const String email = "email";
  static const String firebase_id = "firebase_id";
  static const String gender = "gender";
  static const String isGuilder = "isGuilder";
  static const String trans = "trans";
  static const String is_fcm_open = "is_fcm_open";
  static const String invite_code = "invite_code";
  static const String description = "description";
  static const String location = "location";
  static const String tourist_img = "tourist_img";
  static const String lang = "lang";
  static const String isLogin = "isLogin";
  static const String want_companion = "want_companion";

  static const String device = "device";
  static const String role = "role";
  static const String updateTime = "updateTime";

  static const String notifyOnOff = "notifyOnOff";
  static const String guilderFilter = "guilderFilter";
  static const String articleFilter = "articleFilter";
  static const String commentMax = "commentMax";
  static const String durationMax = "durationMax";


  static List<String> blockUserId  = new List();




}