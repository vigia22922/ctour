import 'dart:io';


import 'package:ctour/SQLite/model/CCModel.dart';
import 'package:ctour/SQLite/model/ChatModel.dart';
import 'package:ctour/SQLite/model/TravelModel.dart';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';


class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get database async {
    print("get database async");
    if (_db != null) {
      return _db;
    }
    // if _database is null we instantiate it
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    print("initDb()");
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, "LSC.db");
    var ourDb = await openDatabase(path, version: 8, onCreate: _onCreate, onUpgrade: _onUpgrade);
    return ourDb;
  }

  void _onCreate(Database db, int version) async {

    await db.execute("CREATE TABLE cc_table ("
        "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
        "nowID TEXT,"
        "userId TEXT,"
        "realName TEXT,"
        "type TEXT,"
        "address TEXT,"
        "FCMToken TEXT,"
        "userPic TEXT"
        ")");
    await db.execute("CREATE TABLE chat ("
        "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
        "now_id TEXT,"
        "chat_group_id TEXT,"
        "chat_id TEXT,"
        "sender_id TEXT,"
        "sender_name TEXT,"
        "sender_avatar TEXT,"
        "t TEXT,"
        "time TEXT,"
        "timeStamp TEXT,"
        "message TEXT,"
        "suc TEXT,"
        "read TEXT"
        ")");

    await db.execute("CREATE TABLE travel_table ("
        "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
        "cover_image TEXT,"
        "title TEXT,"
        "duration TEXT,"

        "type TEXT,"
        "description TEXT,"
        "tourist_id TEXT,"

        "tag TEXT,"
        "contentList TEXT"
        ")");


    print("Table is created");
  }
  Future<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {

    print("_onUpgrade old: ${oldVersion} new: ${newVersion}");
    if (oldVersion  < 5   ) {

      db.execute("ALTER TABLE chat ADD COLUMN suc TEXT;");

    }
    if (oldVersion <7){
      if (oldVersion < newVersion) {
        db.execute("ALTER TABLE cc_table ADD COLUMN FCMToken TEXT;");

      }
    }
    if (oldVersion <8){
      if (oldVersion < newVersion) {
        db.execute("ALTER TABLE cc_table ADD COLUMN address TEXT;");

      }
    }
  }

  // initDB() async {
  //   Directory documentsDirectory = await getApplicationDocumentsDirectory();
  //   String path = join(documentsDirectory.path, "LSC.db");
  //   return await openDatabase(path, version: 1, onOpen: (db) {},
  //       onCreate: (Database db, int version) async {
  //         await db.execute("CREATE TABLE doctor ("
  //             "id INTEGER PRIMARY KEY,"
  //             "name TEXT,"
  //             "avatar TEXT"
  //             ")");
  //         // await db.execute("CREATE TABLE water ("
  //         //     "id INTEGER PRIMARY KEY,"
  //         //     "date TEXT,"
  //         //     "volume TEXT"
  //         //     ")");
  //         // await db.execute("CREATE TABLE temp ("
  //         //     "id INTEGER PRIMARY KEY,"
  //         //     "date TEXT,"
  //         //     "temp TEXT"
  //         //     ")");
  //       });
  //   }
  Future<void> insertCC(CCModel ccModel) async {
    //引用資料庫
    final Database db = await database;
    // 插入狗狗資料庫語法
    await db.insert(
      'cc_table',
      ccModel.toMap(),
      //當資料發生衝突，定義將會採用 replace 覆蓋之前的資料
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertChat(ChatModel chatModel) async {
    print("insertChat");
    //引用資料庫
    final Database db = await database;
    // 插入狗狗資料庫語法
    await db.insert(
      'chat',
      chatModel.toMap(),
      //當資料發生衝突，定義將會採用 replace 覆蓋之前的資料
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }
  Future<int> insertChatReturnID(ChatModel chatModel) async {
    print("insertChat");
    //引用資料庫
    final Database db = await database;
    // 插入狗狗資料庫語法
    return  await db.insert(
      'chat',
      chatModel.toMap(),
      //當資料發生衝突，定義將會採用 replace 覆蓋之前的資料
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertTravel(TravelModel travelModel) async {
    print("inserTravel");
    //引用資料庫
    final Database db = await database;
    // 插入狗狗資料庫語法
    await db.insert(
      'travel_table',
      travelModel.toMap(),
      //當資料發生衝突，定義將會採用 replace 覆蓋之前的資料
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<CCModel>> getCC() async {
    //引用資料庫
    final Database db = await database;
    List<CCModel> listCC;
    listCC = new List();
    // 查詢狗狗資料庫語法
    final List<Map<String, dynamic>> maps = await db.query('cc_table');

    //   return List.generate(maps.length, (i) {
    //     return waterVolumeModel(
    //       id: maps[i]['id'],
    //       date: maps[i]['date'],
    //       volume: maps[i]['volume'],
    //     );

    for(Map i in maps){
      listCC.add(CCModel.fromJson(i));
    }
    return listCC;

  }
  Future<List<CCModel>> getCCByNowID(String nowID ) async {
    //引用資料庫
    final Database db = await database;
    List<CCModel> listCC;
    listCC = new List();
    // 查詢狗狗資料庫語法
    // final List<Map<String, dynamic>> maps = await db.query('check');

    final List<Map<String, dynamic>> maps = await db.query('cc_table',
        where: 'nowID = ?  ',
        whereArgs: [nowID]);
    print ("getCCByNowID" + maps.length.toString());
    for (Map i in maps) {
      listCC.add(CCModel.fromJson(i));
    }
    return listCC;

  }
  Future<List<CCModel>> getCCByNowIDName(String nowID,String type,String name) async {
    //引用資料庫
    final Database db = await database;
    List<CCModel> listCC;
    listCC = new List();
    // 查詢狗狗資料庫語法
    // final List<Map<String, dynamic>> maps = await db.query('check');

    final List<Map<String, dynamic>> maps = await db.query('cc_table',
        where: 'nowID = ? AND type = ? AND ( realName LIKE ? OR address LIKE ? )',
        whereArgs: [nowID,type, '%$name%' , '%$name%']);
    print ("getCCByNowID" + maps.length.toString());
    for (Map i in maps) {
      listCC.add(CCModel.fromJson(i));
    }
    return listCC;

  }

  Future<List<ChatModel>> getChat() async {
    //引用資料庫
    final Database db = await database;
    List<ChatModel> listDoctor;
    listDoctor = new List();
    // 查詢狗狗資料庫語法
    final List<Map<String, dynamic>> maps = await db.query('chat');

    //   return List.generate(maps.length, (i) {
    //     return waterVolumeModel(
    //       id: maps[i]['id'],
    //       date: maps[i]['date'],
    //       volume: maps[i]['volume'],
    //     );

    for(Map i in maps){
      listDoctor.add(ChatModel.fromJson(i));
    }
    return listDoctor;

  }


  Future<List<ChatModel>> getChatByID(int id ) async {
    //引用資料庫
    final Database db = await database;
    List<ChatModel> listCC;
    listCC = new List();
    // 查詢狗狗資料庫語法
    // final List<Map<String, dynamic>> maps = await db.query('check');

    final List<Map<String, dynamic>> maps = await db.query('chat',
        where: 'id = ?',
        whereArgs: [id]);
    print ("getChatByID" + maps.length.toString());
    for (Map i in maps) {
      listCC.add(ChatModel.fromJson(i));
    }
    return listCC;

  }

  Future<List<ChatModel>> getChatByNowID(String nowID) async {
    //引用資料庫
    final Database db = await database;
    List<ChatModel> listCC;
    listCC = new List();
    // 查詢狗狗資料庫語法
    // final List<Map<String, dynamic>> maps = await db.query('check');

    final List<Map<String, dynamic>> maps = await db.query('chat',
        where: 'nowID = ?',
        whereArgs: [nowID]);
    // print ("chat" + maps.length.toString());
    for (Map i in maps) {
      listCC.add(ChatModel.fromJson(i));
    }
    return listCC;

  }

  Future<List<ChatModel>> getChatByNowIDGroupID(String nowID, String groupID) async {
    //引用資料庫
    final Database db = await database;
    List<ChatModel> listCC;
    listCC = new List();
    // 查詢狗狗資料庫語法
    // final List<Map<String, dynamic>> maps = await db.query('check');

    final List<Map<String, dynamic>> maps = await db.query('chat',
        where: 'now_id = ? AND chat_group_id = ?',
        whereArgs: [nowID, groupID]);
    // print ("chat" + maps.length.toString());
    for (Map i in maps) {
      listCC.add(ChatModel.fromJson(i));
    }
    return listCC;

  }
  Future<List<ChatModel>> getChatByNowIDGroupIDChatID(String nowID, String groupID, String chatID) async {
    //引用資料庫
    final Database db = await database;
    List<ChatModel> listCC;
    listCC = new List();
    // 查詢狗狗資料庫語法
    // final List<Map<String, dynamic>> maps = await db.query('check');

    final List<Map<String, dynamic>> maps = await db.query('chat',
        where: 'now_id = ? AND chat_group_id = ? AND chat_id = ?',
        whereArgs: [nowID, groupID, chatID]);
    // print ("chat" + maps.length.toString());
    for (Map i in maps) {
      listCC.add(ChatModel.fromJson(i));
    }
    return listCC;

  }


  Future<List<ChatModel>> getChatByNowIDGroupIDLast(String nowID, String groupID) async {

    //引用資料庫
    final Database db = await database;
    List<ChatModel> listCC;
    listCC = new List();
    // 查詢狗狗資料庫語法
    // final List<Map<String, dynamic>> maps = await db.query('check');

    final List<Map<String, dynamic>> maps = await db.query('chat',
        where: 'now_id = ? AND chat_group_id = ? AND suc = ?',
        whereArgs: [nowID, groupID,"1"],
        orderBy: "id DESC",
        limit: 1
     );
    // print ("chat" + maps.length.toString());

    for (Map i in maps) {
      print ("chat time" +ChatModel.fromJson(i).time);
      listCC.add(ChatModel.fromJson(i));

      print("chat time" + listCC[0].time);
    }

    return listCC;

  }
  Future<List<ChatModel>> getChatByNowIDGroupIDUnread(String nowID, String groupID) async {
    //引用資料庫
    final Database db = await database;
    List<ChatModel> listCC;
    listCC = new List();
    // 查詢狗狗資料庫語法
    // final List<Map<String, dynamic>> maps = await db.query('check');

    final List<Map<String, dynamic>> maps = await db.query('chat',
        where: 'now_id = ? AND chat_group_id = ? AND read = 0',
        whereArgs: [nowID, groupID],

    );
    // print ("chat" + maps.length.toString());
    for (Map i in maps) {
      listCC.add(ChatModel.fromJson(i));
    }
    return listCC;

  }

  Future<List<TravelModel>> getTravelByTouristID(String touristID) async {
    //引用資料庫
    final Database db = await database;
    List<TravelModel> listCC;
    listCC = new List();
    // 查詢狗狗資料庫語法
    // final List<Map<String, dynamic>> maps = await db.query('check');

    final List<Map<String, dynamic>> maps = await db.query('travel_table',
        where: 'tourist_id = ?',
        whereArgs: [touristID]);
    // print ("chat" + maps.length.toString());
    for (Map i in maps) {
      listCC.add(TravelModel.fromJson(i));
    }
    return listCC;

  }


  Future<void> deleteCC(int id) async {
      //引用資料庫
      final db = await database;
      //刪除語法
      await db.delete(
        'cc_table',
        where: "id = ?",
        whereArgs: [id],
      );
    }

    Future<void> deleteAllCC() async {
      //引用資料庫
      final db = await database;
      //刪除語法
      await db.delete(
        'cc_table',
      );
    }



    Future<void> deleteChat(int id) async {
      //引用資料庫
      final db = await database;
      //刪除語法
      await db.delete(
        'chat',
        where: "id = ?",
        whereArgs: [id],
      );
    }

  Future<void> deleteTravel(int id) async {
    //引用資料庫
    final db = await database;
    //刪除語法
    await db.delete(
      'travel_table',
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<void> deleteChatByNowIDGroupID(String nowID,String groupID) async {
    //引用資料庫
    final db = await database;
    //刪除語法



    await db.delete(
      'chat',
        where: 'now_id = ? AND chat_group_id = ?',
        whereArgs: [nowID, groupID]
    );
  }

    Future<void> deleteAllChat() async {
      //引用資料庫
      final db = await database;
      //刪除語法
      await db.delete(
        'chat',
      );
    }


  }
