import 'dart:convert';

import 'package:flutter/widgets.dart';

class TravelModel {
  int id;
  String cover_image;
  String title;
  String duration;


  String type;
  String description;

  String tourist_id;

  String tag;

  String contentList ;


  TravelModel({this.id,this.cover_image,this.type,this.tag,this.description,
    this.title, this.duration, this.contentList, this.tourist_id});


  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'cover_image': cover_image,
      'title': title,
      'duration': duration,

      'type': type,
      'description': description,
      'tourist_id': tourist_id,

      'tag': tag,
      'contentList': contentList,

    };
  }

  factory TravelModel.fromJson(Map<String, dynamic> json) => TravelModel(
    id: json["id"],
    cover_image: json["cover_image"].toString(),
    title: json["title"].toString(),
    duration: json["duration"].toString(),


    type: json["type"].toString() ,
    description: json["description"].toString(),
    tourist_id: json["tourist_id"].toString(),


    tag:json["tag"].toString(),
    contentList:json["contentList"].toString(),


  );


}
