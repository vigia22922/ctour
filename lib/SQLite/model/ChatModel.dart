class ChatModel {
  int id;
  String now_id;
  String chat_group_id;//分辨是哪個聊天室 一個人就是該user_id
  String chat_id;//獨立的，以免重複插入
  String sender_id;
  String sender_name;
  String sender_avatar;
  String t;
  String time;
  String timeStamp;
  String message;
  String read;//0 unread 1 read
  String suc;////0 正在上傳,1成功,2失敗



  ChatModel({this.id,this.now_id, this.sender_id, this.sender_name, this.sender_avatar
    , this.t, this.time, this.timeStamp,this.chat_group_id,this.message,this.read,this.chat_id,this.suc});
Map<String, dynamic> toMap() {
  return {
    'id': id,
    'now_id': now_id,
    'chat_group_id': chat_group_id,
    'sender_id': sender_id,
    'sender_name': sender_name,
    'sender_avatar': sender_avatar,
    't': t,
    'time': time,
    'timeStamp': timeStamp,
    'message': message,
    'read': read,
    'chat_id': chat_id,
    'suc': suc,


  };
}

factory ChatModel.fromJson(Map<String, dynamic> json) => ChatModel(
  now_id: json["now_id"],
  id: json["id"],
  chat_group_id: json["chat_group_id"].toString(),
  sender_id: json["sender_id"].toString(),
  sender_name: json["sender_name"].toString(),
  sender_avatar: json["sender_avatar"].toString(),
  t: json["t"].toString(),
  time: json["time"].toString(),
  timeStamp: json["timeStamp"].toString(),
  message: json["message"].toString(),
  read: json["read"].toString(),
  chat_id: json["chat_id"].toString(),
  suc: json["suc"].toString(),

);

}