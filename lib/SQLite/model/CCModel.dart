class CCModel {
    int id;
   String nowID;

   String userId;
   String realName;
   String userPic;
   String FCMToken;



  CCModel({this.id,this.nowID, this.userId, this.realName, this.userPic,this.FCMToken});
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'nowID': nowID,
      'userId': userId,
      'realName': realName,
      'userPic': userPic,

      'FCMToken': FCMToken,



    };
  }

  factory CCModel.fromJson(Map<String, dynamic> json) => CCModel(
    id: json["id"],
    nowID: json["nowID"],
    userId: json["userId"].toString(),
    realName: json["realName"].toString(),
    userPic: json["userPic"].toString(),

    FCMToken: json["FCMToken"].toString(),


  );

}