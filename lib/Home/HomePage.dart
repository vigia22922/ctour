import 'dart:async';
import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/TravelCard.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Define/GlobalData.dart';
import 'package:ctour/Firebase/FirebaseRealtime.dart';
import 'package:ctour/Home/HomeFilterPage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/tool/DateTools.dart';
import 'package:ctour/tool/FilterData.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

// import 'package:pull_to_refresh/pull_to_refresh.dart';
import '../Travel/TravelClass.dart';
import 'package:ctour/Travel/TravelPage.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:permission_handler/permission_handler.dart';

import 'dart:convert';
import 'package:dio/dio.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shimmer/shimmer.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomePageState();
  }
}

class HomePageState extends State<HomePage>
    with AutomaticKeepAliveClientMixin, TickerProviderStateMixin {
  StreamSubscription<String> fireBaseDBSubScription;

  String searchText1 = "";
  TextEditingController searchEdit;
  double x = 0;

  List<TravelClass> hotTravelList = new List();
  List<TravelClass> recommendTravelList = new List();
  List<TravelClass> searchList = new List();

  EasyRefreshController refreshController = new EasyRefreshController();

  bool loadingBool = true;
  FocusNode searchFocus = new FocusNode();

  @override
  bool get wantKeepAlive => true;

  List<String> rankImg = [
    Res.num1,
    Res.num2,
    Res.num3,
    Res.num4,
    Res.num5,
    Res.num6,
    Res.num7,
    Res.num8,
    Res.num9,
    Res.num10,
  ];

  AnimationController searchAnimationController,
      spacerAnimationController,
      logoAnimationController,
      logoSizeAnimationController;
  AnimationController _TextAnimationController;
  Animation _searchTween, _spacerTween;

  Animation<Offset> offset;
  Animation<double> sizeOffset,heightOffset,heightSpaceOffset,searchOffset;
  Animation _curve1, _curve2, _curve3, _curve4;

bool showSearch = true;

int durationMax = -1;

double per = 0.05;

  int pageCnt = 1;
  int totalCnt = 10;
  int pageCntSearch = 1;
  int totalCntSEarch = 10;
  int cntPerPage = 10;
  bool searchLoading = false;

  @override
  void initState() {

    SharePre.prefs.setString(GDefine.guilderFilter, "false");
    SharePre.prefs.setString(GDefine.articleFilter, "false");
    searchEdit = TextEditingController();
    refreshController = EasyRefreshController(
      controlFinishRefresh: true,
      controlFinishLoad: true,
    );
    print("init");
    Future.delayed(Duration(milliseconds: 100), () {
      // 5s over, navigate to a new page
      refreshController.callRefresh();
    });

    super.initState();

    searchAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    _curve1 = CurvedAnimation(
        parent: searchAnimationController, curve: Curves.fastOutSlowIn);
    // _searchTween = Tween(begin: 0.0, end: 1.0).animate(_curve1);
    searchOffset = Tween<double>(begin: 0, end: 1)        .animate(_curve1);

    double logoEndWidth = ( per.sh-0.017.sh)/140 * 450;

    spacerAnimationController =
        AnimationController(vsync: this, duration: Duration(seconds: 0));
    // _curve2 = CurvedAnimation(parent: spacerAnimationController, curve: Curves.fastOutSlowIn);
    _spacerTween =
        Tween(begin: 0.0, end: 1.0).animate(spacerAnimationController);

    logoAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    _curve3 = CurvedAnimation(
        parent: logoAnimationController, curve: Curves.fastOutSlowIn);
    offset = Tween<Offset>(begin: Offset(0.0, 0.0), end: Offset(
       - (0.44.sw - logoEndWidth/2) /logoEndWidth
    , 0))
        .animate(_curve3);

    logoSizeAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    _curve4 = CurvedAnimation(
        parent: logoSizeAnimationController, curve: Curves.fastOutSlowIn);
    // sizeOffset = Tween<double>(begin: 0.065.sh / 140 * 450, end: 0.88.sw / 3)
    //     .animate(_curve4);
    sizeOffset = Tween<double>(begin: per.sh / 140 * 450, end: logoEndWidth)
        .animate(_curve4);

    heightOffset = Tween<double>(begin: (per+ 0.017). sh, end: per.sh-0.017.sh)
        .animate(_curve4);
    heightSpaceOffset = Tween<double>(begin: 0.0  , end:  0.017.sh)
        .animate(_curve4);



    print("init  " + logoSizeAnimationController.value.toString());
    print("init  " + sizeOffset.value.toString());



    if(SharePre.prefs.getInt(GDefine.durationMax )!= null){
      durationMax = SharePre.prefs.getInt(GDefine.durationMax );
      print("durationMax  " + durationMax.toString());
      if(FilterData.travel_end_duration == -1){
        FilterData.travel_end_duration = double.parse(durationMax.toString());
      }
    }else {
      SharePre.prefs.setInt(GDefine.durationMax,1 );
    }

  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    print(" ScreenSize().getWidth(context)  " +
        ScreenSize().getWidth(context).toString());
    print(" ScreenSize().getHeight(context)  " +
        ScreenSize().getHeight(context).toString());
    // x = (1.5*ScreenSize().getWidth(context)-0.35*ScreenSize().getHeight(context))/2 - 0.08*ScreenSize().getWidth(context);
  }

  @override
  Widget build(BuildContext context) {
    return FocusDetector(
      onFocusGained: () {
        if(GlobalData.refreshArticle) {
          pageCnt = 1;
          getRecommentArticleAPI(false);
          getWeekArticleAPI();

          getScoreCount();
        }else {
          GlobalData.refreshArticle = true;
        }
      },
      child: GestureDetector(
        onTap: () {
          Tools().dismissK(context);
          print("onTap");
        },
        child: Scaffold(
          backgroundColor: OwnColors.white,
          body: SafeArea(
            child: Center(
              child: Container(
                height: ScreenSize().getHeight(context) * 0.91,
                width: ScreenSize().getWidth(context) * 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [

                    Expanded(
                        child: Stack(
                      children: [
                        Column(
                          children: [
                            AnimatedBuilder(
                              animation: logoSizeAnimationController,
                              builder: (context, child) => Container(
                                height:
                                (per+0.017).sh ,
                              ),
                            ),

                            AnimatedBuilder(
                              animation: spacerAnimationController,
                              builder: (context, child) => Container(
                                height:
                                0.07 *
                                    (1 - spacerAnimationController.value) *
                                    ScreenSize().getHeight(context)  ,
                              ),
                            ),
                            Expanded(
                              child:



                              searchEdit.text.length > 0 && searchList.length == 0 && !searchLoading ?
                              SingleChildScrollView(
                                keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    HorizontalSpace().create(context, 0.2),
                                    Container(
                                      height: 0.118.sh,
                                      child: Image.asset(Res.not_found),
                                    ),
                                    HorizontalSpace().create(context, 0.01),
                                    Text(
                                      S().search_not_found_title,
                                      style: GoogleFonts.inter(
                                          fontSize: 16.nsp,
                                          fontWeight: FontWeight.w600,
                                          color: OwnColors.black
                                      ),
                                    ),
                                    HorizontalSpace().create(context, 0.01),
                                    Text(
                                      S().search_not_found_hint,
                                      style: GoogleFonts.inter(
                                          fontSize: 12.nsp,
                                          fontWeight: FontWeight.w500,
                                          color: OwnColors.C989898
                                      ),
                                    ),
                                    HorizontalSpace().create(context, 0.1),
                                    HorizontalSpace().create(context, 0.1),
                                    HorizontalSpace().create(context, 0.1),




                                  ],
                                ),
                              )
                                  :
                              EasyRefresh.builder(
                                  controller: refreshController,
                                  // header: ListenerHeader(
                                  //   triggerOffset: 100,
                                  //   // listenable: _listenable,
                                  //   safeArea: false,
                                  // ),
                                  header: ClassicHeader(
                                      dragText: '',
                                      armedText: '',
                                      readyText: '',
                                      processingText: '',
                                      processedText: '',
                                      noMoreText: '',
                                      failedText: '',
                                      messageText: '',
                                      iconTheme: IconThemeData(
                                          color: OwnColors.CTourOrange)),
                                  footer: ClassicFooter(
                                    // position: IndicatorPosition.locator,
                                      dragText: '',
                                      armedText: '',
                                      readyText: '',
                                      processingText: '',
                                      processedText: '',
                                      noMoreText: '',
                                      failedText: '',
                                      messageText: '',
                                      noMoreIcon: Container(
                                        width: 0,
                                        height: 0,
                                      ),

                                      iconTheme: IconThemeData(
                                          color: OwnColors.CTourOrange
                                      )
                                  ),
                                  onRefresh: () async {
                                    print("onRefresh");
                                    if(searchEdit.text == "") {
                                      pageCnt = 1;
                                      getRecommentArticleAPI(false);
                                      getWeekArticleAPI();
                                      getScoreCount();
                                    }else {
                                      refreshController.finishRefresh();
                                      refreshController.resetFooter();
                                    }
                                  },
                                  onLoad: () async {
                                    print("onLoad");
                                    print("totalCnt " + totalCnt.toString());
                                    if(searchEdit.text == "") {
                                      if (pageCnt * cntPerPage < totalCnt) {
                                        pageCnt = pageCnt + 1;
                                        if (SharePre.prefs.getString(
                                            GDefine.guilderFilter) == "true") {
                                          getRecommentArticleAPI(true);
                                        } else {
                                          getRecommentArticleAPI(true);
                                        }
                                        refreshController.finishLoad();
                                      } else {
                                        refreshController.finishLoad(
                                            IndicatorResult.noMore);
                                      }
                                    }else {
                                      if (pageCntSearch * cntPerPage < totalCntSEarch) {
                                        pageCntSearch = pageCntSearch + 1;
                                        if (SharePre.prefs.getString(
                                            GDefine.guilderFilter) == "true") {
                                          getRecommentArticleSearchAPI(true);
                                        } else {
                                          getRecommentArticleSearchAPI(true);
                                        }
                                        refreshController.finishLoad();
                                      } else {
                                        refreshController.finishLoad(
                                            IndicatorResult.noMore);
                                      }
                                    }
                                    // refreshController.finishLoad(IndicatorResult.noMore);

                                  },
                                  childBuilder: (context, physics) {
                                    return NotificationListener<
                                        ScrollNotification>(
                                      onNotification: (scrollNotification) {
                                        print("position " +
                                            scrollNotification.metrics.pixels
                                                .toString());
                                        print("position " +
                                            scrollNotification.metrics.axisDirection
                                                .toString());
                                        print("searchOffset " +
                                            searchOffset.value
                                                .toString());
                                        print("heightSpaceOffset " +
                                            heightSpaceOffset.value
                                                .toString());


                                        if((
                                            scrollNotification.metrics.axisDirection == AxisDirection.down ||
                                                scrollNotification.metrics.axisDirection == AxisDirection.up)) {

                                          spacerAnimationController.animateTo(
                                              scrollNotification.metrics.pixels /
                                                  100);
                                          if (scrollNotification.metrics
                                              .pixels >
                                              100) {
                                            // searchAnimationController.animateTo((
                                            //     scrollNotification.metrics
                                            //         .pixels-100)  / 100);
                                            if (searchAnimationController
                                                .value !=
                                                1 &&
                                                !searchAnimationController
                                                    .isAnimating) {
                                              searchAnimationController
                                                  .animateTo(1);
                                              setState(() {
                                                showSearch = false;
                                                print("showSearch false!");
                                              });

                                              logoAnimationController
                                                  .animateTo(1);
                                              logoSizeAnimationController
                                                  .animateTo(1);
                                            }
                                          } else {
                                            if (searchAnimationController
                                                .value !=
                                                0 &&
                                                !searchAnimationController
                                                    .isAnimating) {
                                              searchAnimationController
                                                  .animateTo(0);
                                              logoAnimationController
                                                  .animateTo(0);
                                              logoSizeAnimationController
                                                  .animateTo(0);
                                              setState(() {
                                                showSearch = true;
                                                print("showSearch true!");
                                              });
                                            }
                                          }
                                        }

                                        //
                                        // else if(scrollNotification.metrics.pixels <100      ){
                                        //   searchAnimationController.animateTo(
                                        //       scrollNotification.metrics
                                        //           .pixels / 100);
                                        // }

                                        // print("_ColorAnimationController " + _ColorAnimationController.value.toString());
                                      },
                                      child:

                                      SingleChildScrollView(
                                        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
                                          physics: searchEdit.text.length == 0? physics:physics,
                                          padding: EdgeInsets.all(0),
                                          child:



                                          searchEdit.text.length > 0
                                              ?
                                          refreshSearchContent()
                                              : refreshContent()),
                                    );
                                  }),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Stack(

                              children: [
                                AnimatedBuilder(
                                    animation: searchAnimationController,
                                    builder: (context, child) =>

                                        Container(

                                          width: 0.88.sw,
                                          // height: sizeOffset.value/450*165,

                                          child: Stack(
                                            alignment: Alignment.centerRight,
                                            children: [
                                              Container(
                                                height : (per + 0.017).sh,
                                                color: OwnColors.tran,
                                              ),
                                              Column(

                                                children: [
                                                  AnimatedBuilder(
                                                      animation:
                                                      logoSizeAnimationController,
                                                      builder: (context, child) =>
                                                          Container(
                                                            width: heightSpaceOffset.value,
                                                            height:  heightSpaceOffset.value,
                                                          )),
                                                  Align(
                                                    alignment: Alignment.bottomCenter,
                                                    child: SlideTransition(
                                                      position: offset,
                                                      child: AnimatedBuilder(
                                                          animation:
                                                          logoSizeAnimationController,
                                                          builder: (context, child) =>
                                                              Container(
                                                                  width: sizeOffset.value,
                                                                  height: heightOffset.value  ,
                                                                  child: Image.asset(
                                                                      Res.top_icon))),
                                                    ),
                                                  ),
                                                  AnimatedBuilder(
                                                      animation:
                                                      logoSizeAnimationController,
                                                      builder: (context, child) =>
                                                          Container(
                                                            width: heightSpaceOffset.value,
                                                            height:  heightSpaceOffset.value,
                                                          )),


                                                ],
                                              ),
                                              Positioned(
                                                bottom: 0.017.sh,
                                                child: AnimatedBuilder(
                                                    animation:
                                                    searchAnimationController,
                                                    builder: (context, child) =>
                                                        Opacity(
                                                          opacity: searchAnimationController.value,
                                                          child: GestureDetector(
                                                            onTap: () {
                                                              if(searchAnimationController.value == 1 && showSearch == false){
                                                                print("onTap");
                                                                searchAnimationController.animateTo(0);
                                                                searchAnimationController
                                                                    .animateTo(0);
                                                                logoAnimationController
                                                                    .animateTo(0);
                                                                logoSizeAnimationController
                                                                    .animateTo(0);
                                                                setState(() {
                                                                  showSearch = true;
                                                                });
                                                              }

                                                            },
                                                            child: Container(
                                                                height:
                                                                0.88.sw / 3 / 450 * 100,
                                                                child: Image.asset(
                                                                    Res.search_black)),
                                                          ),
                                                        )),
                                              )
                                            ],
                                          ),
                                        ))
                              ],
                            ),

                            ExpandedSection(
                              child: Container(
                                width: 1.sw,
                                height: (0.07 ) * ScreenSize().getHeight(context),
                                color: OwnColors.white,
                                child: Column(
                                  children: [
                                    // HorizontalSpace().createWhite(context, 0.019),
                                    Container(
                                      height: 0.05 *
                                          ScreenSize().getHeight(context),
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.center,
                                        crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            height: 0.05 *
                                                ScreenSize().getHeight(context),
                                            width:
                                            ScreenSize().getWidth(context) *
                                                0.88,
                                            // height: height,
                                            decoration: BoxDecoration(
                                              color: OwnColors.white,
                                              border: Border.all(
                                                  color: OwnColors.CC8C8C8),
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(30),
                                                  topRight: Radius.circular(30),
                                                  bottomLeft:
                                                  Radius.circular(30),
                                                  bottomRight:
                                                  Radius.circular(30)),
                                            ),

                                            alignment: Alignment.center,
                                            child: Row(
                                              children: [
                                                GestureDetector(
                                                  onTap: () {
                                                    print("search click");
                                                    FocusScope.of(context)
                                                        .requestFocus(
                                                        searchFocus);
                                                  },
                                                  child: Padding(
                                                    padding:
                                                    const EdgeInsets.only(
                                                        left: 12.0,
                                                        bottom: 4.0),
                                                    child: Container(
                                                        height: 0.027 *
                                                            ScreenSize()
                                                                .getHeight(
                                                                context),
                                                        child: Icon(
                                                            SFSymbols.search,
                                                            color: OwnColors
                                                                .black)),
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Container(
                                                    height: 0.027 *
                                                        ScreenSize()
                                                            .getHeight(context),
                                                    child: TextField(
                                                      controller: searchEdit,
                                                      focusNode: searchFocus,
                                                      onChanged: (text) {
                                                        print("len " +
                                                            searchEdit
                                                                .text.length
                                                                .toString());
                                                        setState(() {});
                                                        print("onChanged");
                                                        pageCntSearch = 1;
                                                        if (searchText1 == "") {
                                                          searchText1 = text;
                                                          searchLoading = true;
                                                          refreshController.callRefresh();
                                                          getRecommentArticleSearchAPI(false);
                                                        } else {
                                                          if (searchText1 !=
                                                              text) {
                                                            searchText1 = text;
                                                            searchLoading = true;
                                                            refreshController.callRefresh();

                                                            getRecommentArticleSearchAPI(false);
                                                          }
                                                        }
                                                      },
                                                      autocorrect: false,
                                                      decoration:
                                                      InputDecoration(
                                                          fillColor:
                                                          OwnColors
                                                              .tran,
                                                          contentPadding:
                                                          EdgeInsets.symmetric(
                                                              horizontal:
                                                              10,
                                                              vertical:
                                                              0),
                                                          //Change this value to custom as you like
                                                          // isDense: true,
                                                          hintText: S()
                                                              .home_search_hint,
                                                          filled: true,
                                                          hintStyle:
                                                          GoogleFonts
                                                              .inter(
                                                            fontSize:
                                                            16.nsp,
                                                            color: OwnColors
                                                                .black,
                                                            fontWeight:
                                                            FontWeight
                                                                .w500,
                                                            //fontStyle: FontStyle.normal
                                                          ),
                                                          border: OutlineInputBorder(
                                                              borderRadius:
                                                              new BorderRadius
                                                                  .circular(
                                                                  20.0),
                                                              borderSide:
                                                              BorderSide
                                                                  .none)),
                                                      style: GoogleFonts.inter(
                                                          color: Colors.black
                                                              .withOpacity(1),
                                                          fontSize: ScreenUtil()
                                                              .setSp(16)),
                                                      maxLines: 1,
                                                      keyboardType:
                                                      TextInputType.text,
                                                      obscureText: false,
                                                    ),
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                  const EdgeInsets.only(
                                                      right: 8.0),
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      setFilter();
                                                    },
                                                    child: Container(
                                                        height: 0.033 *
                                                            ScreenSize()
                                                                .getHeight(
                                                                context),
                                                        width: 0.033 *
                                                            ScreenSize()
                                                                .getHeight(
                                                                context),
                                                        decoration: BoxDecoration(
                                                            color:
                                                            OwnColors.white,
                                                            shape:
                                                            BoxShape.circle,
                                                            border: Border.all(
                                                                color: OwnColors
                                                                    .CC8C8C8)),
                                                        child: Center(
                                                            child: Icon(
                                                              SFSymbols
                                                                  .line_horizontal_3_decrease,
                                                              size: 0.026 *
                                                                  ScreenSize()
                                                                      .getHeight(
                                                                      context),
                                                            ))),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    HorizontalSpace().create(context, 0.02),
                                  ],
                                ),
                              ),
                              expand: showSearch,
                            ),
                          ],
                        ),
                      ],
                    ))
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget refreshContent() {
    return Column(
      children: [
        Container(
          width: ScreenSize().getWidth(context) * 0.88,
          child: Align(
            alignment: Alignment.centerLeft,
            child: AutoSizeText(
              S().travel_title1,
              maxLines: 1,
              style: GoogleFonts.inter(
                fontSize: 20.nsp,
                color: OwnColors.black,
                fontWeight: FontWeight.w600,
                //fontStyle: FontStyle.normal
              ),
            ),
          ),
        ),
        HorizontalSpace().create(context, 0.001),
        Transform.translate(
          offset: Offset(-x, 0),
          child: Container(
            width: 1 * ScreenSize().getWidth(context),
            height: 0.88 * ScreenSize().getWidth(context) / 16 * 9 +
                0.025 * ScreenSize().getHeight(context),
            child: ListView.builder(
                physics: BouncingScrollPhysics(
                    parent: AlwaysScrollableScrollPhysics()),
                shrinkWrap: true,
                itemCount: hotTravelList.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 0.0),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 0.0, right: 8),
                      child: hotCard(hotTravelList[index], index),
                    ),
                  );
                }),
          ),
        ),
        HorizontalSpace().create(context, 0.04),
        Container(
          width: ScreenSize().getWidth(context) * 0.88,
          child: Align(
            alignment: Alignment.centerLeft,
            child: AutoSizeText(
              S().travel_title2,
              maxLines: 1,
              style: GoogleFonts.inter(
                fontSize: 20.nsp,
                color: OwnColors.black,
                fontWeight: FontWeight.w600,
                //fontStyle: FontStyle.normal
              ),
            ),
          ),
        ),
        HorizontalSpace().create(context, 0.002),
        Container(
          width: 0.88 * ScreenSize().getWidth(context),
          child: recommendTravelList.length == 0
              ?     SharePre.prefs.getString(GDefine.articleFilter).contains("true") ?
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              HorizontalSpace().create(context, 0.08),
              Container(
                height: 0.118.sh,
                child: Image.asset(Res.not_found),
              ),
              HorizontalSpace().create(context, 0.01),
              Text(
                S().search_not_found_title,
                style: GoogleFonts.inter(
                    fontSize: 16.nsp,
                    fontWeight: FontWeight.w600,
                    color: OwnColors.black
                ),
              ),
              HorizontalSpace().create(context, 0.01),
              Text(
                S().search_not_found_hint,
                style: GoogleFonts.inter(
                    fontSize: 12.nsp,
                    fontWeight: FontWeight.w500,
                    color: OwnColors.C989898
                ),
              ),
              HorizontalSpace().create(context, 0.1),


            ],
          ):Container(
            height: 0.3.sh,
          )
              :          Container(
            constraints: BoxConstraints(
                minHeight: 0.4.sh
            ),
                child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: recommendTravelList.length,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.only(top: 0.0),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 0.0, right: 0),
                          child: TravelCard().recommendCard(
                            context,
                            recommendTravelList[index],
                            index,
                            recommendClick,
                            0.88 * ScreenSize().getWidth(context),
                          ),
                        ),
                      );
                    }),
              ),
        ),
      ],
    );
  }

  Widget refreshSearchContent() {
    return Column(
      children: [

        Container(
          width: 0.88 * ScreenSize().getWidth(context),
          child: searchList.length == 0
              ? Container(
                  height: 0.119.sh,
                  
                )
              :  Container(
            constraints: BoxConstraints(
                minHeight: 0.8.sh
            ),
                child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: searchList.length,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.only(top: 0.0),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 0.0, right: 0),
                          child: TravelCard().recommendCard(
                            context,
                            searchList[index],
                            index,
                            recommendClick,
                            0.88 * ScreenSize().getWidth(context),
                          ),
                        ),
                      );
                    }),
              ),
        ),
      ],
    );
  }

  Container hotCard(TravelClass travelClass, int index) {
    return Container(
      // width: 0.9 * ScreenSize().getWidth(context),
      // height: 0.154 *  ScreenSize().getHeight(context),

      child: Column(
        children: [
          HorizontalSpace().create(context, 0.02),
          Container(
            decoration: BoxDecoration(
              color: OwnColors.white,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(15),
                topLeft: Radius.circular(15),
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15),
              ),
            ),
            child: Row(
              children: [
                VerticalSpace().create(context, index == 0 ? 0.06 : 0.02),
                GestureDetector(
                  onTap: () => {hotClick(travelClass)},
                  child: Stack(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            height: 2,
                          ),
                          Center(
                            child: Container(
                              width: 0.88 * ScreenSize().getWidth(context),
                              child: AspectRatio(
                                aspectRatio: 1.78,
                                child: Stack(
                                  children: [
                                    CachedNetworkImage(
                                      imageUrl: travelClass.cover_image,
                                      fit: BoxFit.fill,
                                      placeholder: (context, url) =>
                                          AspectRatio(
                                        aspectRatio: 2.16,
                                        child: FractionallySizedBox(
                                          heightFactor: 0.5,
                                          widthFactor: 0.5,
                                          child: SpinKitRing(
                                            lineWidth: 5,
                                            color: OwnColors.tran,
                                          ),
                                        ),
                                      ),
                                      errorWidget: (context, url, error) =>
                                          Icon(
                                        Icons.error,
                                        color: OwnColors.black.withOpacity(0.5),
                                      ),
                                      imageBuilder: (context, imageProvider) =>
                                          Container(
                                        decoration: BoxDecoration(
                                          color: OwnColors.tran,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                          image: DecorationImage(
                                              image: imageProvider,
                                              fit: BoxFit.cover),
                                        ),
                                      ),
                                      // width: MediaQuery.of(context).size.width,
                                    ),
                                    Container(
                                      height:
                                          0.1 * ScreenSize().getHeight(context),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(15),
                                              topRight: Radius.circular(15)),
                                          gradient: LinearGradient(
                                            begin: Alignment.bottomCenter,
                                            end: Alignment.topCenter,
                                            colors: [
                                              OwnColors.C797979
                                                  .withOpacity(0.0),
                                              OwnColors.C787878
                                                  .withOpacity(0.0073),
                                              OwnColors.C3E3E3E
                                                  .withOpacity(0.3391),
                                              Colors.black.withOpacity(0.7),
                                            ],
                                          )),
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            top: 10.0, left: 10),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 0.045 *
                                                  ScreenSize()
                                                      .getHeight(context),
                                              child: AspectRatio(
                                                aspectRatio: 1,
                                                child: CachedNetworkImage(
                                                  placeholder: (context, url) =>
                                                      AspectRatio(
                                                    aspectRatio: 2.16,
                                                    child: FractionallySizedBox(
                                                      heightFactor: 0.5,
                                                      widthFactor: 0.5,
                                                      child: SpinKitRing(
                                                        color: OwnColors.tran,
                                                      ),
                                                    ),
                                                  ),
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          Icon(
                                                    Icons.error,
                                                    color: Colors.red,
                                                  ),
                                                  imageUrl: travelClass
                                                      .guilder_avatar,
                                                  fit: BoxFit.fitHeight,
                                                  imageBuilder: (context,
                                                          imageProvider) =>
                                                      Container(
                                                    // width: 80.0,
                                                    // height: 80.0,
                                                    decoration: BoxDecoration(
                                                      color: OwnColors.tran,
                                                      shape: BoxShape.circle,
                                                      border: Border.all(
                                                          width: 1,
                                                          color: OwnColors
                                                              .CC8C8C8),
                                                      image: DecorationImage(
                                                          image: imageProvider,
                                                          fit: BoxFit.cover),
                                                    ),
                                                  ),
                                                  // width: MediaQuery.of(context).size.width,
                                                  // placeholder: (context, url) => new CircularProgressIndicator(),
                                                  // placeholder: new CircularProgressIndicator(),
                                                ),
                                              ),
                                            ),
                                            VerticalSpace()
                                                .create(context, 0.017),
                                            Expanded(
                                              child: Container(
                                                height: 0.045 *
                                                    ScreenSize()
                                                        .getHeight(context),
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    Text(
                                                      travelClass.guilder_name,
                                                      maxLines: 1,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: GoogleFonts.inter(
                                                          fontSize: 16.nsp,
                                                          height: 1.2,
                                                          color:
                                                              OwnColors.white,
                                                          fontWeight:
                                                              FontWeight.w500),
                                                    ),
                                                    // Row(
                                                    //   mainAxisAlignment: MainAxisAlignment.start,
                                                    //   crossAxisAlignment: CrossAxisAlignment.center,
                                                    //   children: [
                                                    //     Container(
                                                    //       height: 0.02 * ScreenSize().getHeight(context),
                                                    //       child: Image.asset(Res.star),
                                                    //     ),
                                                    //     Container(width: 2,),
                                                    //     Text(
                                                    //       travelClass.guilder_rate  ,
                                                    //
                                                    //       style:GoogleFonts.inter (
                                                    //           fontSize: 12.nsp,
                                                    //           height: 1.2,
                                                    //           color: OwnColors.white,
                                                    //           fontWeight: FontWeight.w600
                                                    //       ),
                                                    //     ),
                                                    //     Text(
                                                    //       " (" + double.parse(travelClass.guilder_comment).toStringAsFixed(0)+S().home_rate_number+")",
                                                    //
                                                    //       style:GoogleFonts.inter (
                                                    //           height: 1.2,
                                                    //           fontSize: 12.nsp,
                                                    //           color: OwnColors.white,
                                                    //           fontWeight: FontWeight.w300
                                                    //
                                                    //       ),
                                                    //     ),
                                                    //   ],
                                                    // ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 0,
                                      child: Container(
                                        height: 0.13 *
                                            ScreenSize().getHeight(context),
                                        width: 0.88 *
                                            ScreenSize().getWidth(context),
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                bottomLeft: Radius.circular(15),
                                                bottomRight:
                                                    Radius.circular(15)),
                                            gradient: LinearGradient(
                                              begin: Alignment.topCenter,
                                              end: Alignment.bottomCenter,
                                              colors: [
                                                OwnColors.C797979
                                                    .withOpacity(0.0),
                                                OwnColors.C787878
                                                    .withOpacity(0.0073),
                                                OwnColors.C3E3E3E
                                                    .withOpacity(0.3391),
                                                Colors.black.withOpacity(0.7),
                                              ],
                                            )),
                                        child: Center(
                                          child: Container(
                                            width: 0.80 *
                                                ScreenSize().getWidth(context),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Spacer(
                                                  flex: 10,
                                                ),
                                                Text(
                                                  travelClass.type == "1"
                                                      ? travelClass.duration
                                                              .toString() +
                                                          S()
                                                              .home_filter_type2_string
                                                      : travelClass.type == "2"
                                                          ? S()
                                                              .home_filter_type3
                                                          : S()
                                                              .home_filter_type4,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                  style: GoogleFonts.inter(
                                                    fontSize: 12.nsp,
                                                    color: OwnColors.white,
                                                    fontWeight: FontWeight.w300,
                                                  ),
                                                ),
                                                Text(
                                                  travelClass.title,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 2,
                                                  style: GoogleFonts.inter(
                                                    fontSize: 16.nsp,
                                                    color: OwnColors.white,
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                ),
                                                HorizontalSpace()
                                                    .create(context, 0.008),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),

                      Positioned(
                        top: 0,
                        right: 10,
                        child: Container(
                          height: ScreenSize().getHeight(context) * 0.04,
                          child: AspectRatio(
                            aspectRatio: 27 / 30,
                            child: Image.asset(
                              rankImg[index],
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      )

                      // Positioned(
                      //   top: 10,
                      //   right: 10,
                      //   child: ClipOval(
                      //   child: Container(
                      //
                      //     color: Colors.transparent, // button color
                      //     child: InkWell(
                      //       borderRadius: BorderRadius.circular(13.0),
                      //       splashColor: OwnColors.inkColor,
                      //       child: SizedBox(width: 56, height: 56, child: Image.asset(Res.audio_on)),
                      //       onTap: () {
                      //         recommendCollectClick(courseClass);
                      //
                      //       },
                      //     ),
                      //   ),
                      // ),)
                    ],
                  ),
                ),
                // VerticalSpace().create(
                //     context, index == hotTravelList.length - 1 ? 0.05 : 0.0),
              ],
            ),
          ),
        ],
      ),
    );
  }

  // Widget recommendCard(TravelClass travelClass, int index) {
  //   return GestureDetector(
  //     onTap: () => {recommendClick(travelClass)},
  //     child: Container(
  //       width: 0.9 * ScreenSize().getWidth(context),
  //
  //
  //       child: Container(
  //         decoration: BoxDecoration(
  //           color: OwnColors.white,
  //           borderRadius: BorderRadius.only(
  //             topRight: Radius.circular(15),
  //             topLeft: Radius.circular(15),
  //             bottomLeft: Radius.circular(15),
  //             bottomRight: Radius.circular(15),
  //           ),
  //         ),
  //         child: Column(
  //           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //           crossAxisAlignment: CrossAxisAlignment.center,
  //           children: [
  //             HorizontalSpace().create(context, 0.015),
  //              AspectRatio(
  //               aspectRatio: 16/9,
  //               child: Container(
  //                 width: 0.9 * ScreenSize().getWidth(context),
  //                 child: Stack(
  //                   children: [
  //                     CachedNetworkImage(
  //                       imageUrl: travelClass.cover_image,
  //                       fit: BoxFit.fill,
  //                       placeholder: (context, url) => AspectRatio(
  //                         aspectRatio: 2.16,
  //                         child: FractionallySizedBox(
  //                           heightFactor: 0.5,
  //                           widthFactor: 0.5,
  //                           child: SpinKitRing(
  //                             color:
  //                             OwnColors.tran ,
  //                           ),
  //                         ),
  //                       ),
  //                       errorWidget: (context, url, error) => Icon(
  //                         Icons.error,
  //                         color: OwnColors.black.withOpacity(0.5),
  //                       ),
  //                       imageBuilder: (context, imageProvider) =>
  //                           Container(
  //                             width: 0.9 * ScreenSize().getWidth(context),
  //                         decoration: BoxDecoration(
  //                           color: OwnColors.black,
  //                           borderRadius: BorderRadius.all(
  //                               Radius.circular(15.0)),
  //                           image: DecorationImage(
  //                               image: imageProvider,
  //                               fit: BoxFit.cover),
  //                         ),
  //                       ),
  //                       // width: MediaQuery.of(context).size.width,
  //                     ),
  //                     // Positioned(
  //                     //   right: 10,
  //                     //   top: 10,
  //                     //   child: Container(
  //                     //       height: 0.03 *
  //                     //           ScreenSize().getHeight(context),
  //                     //       child: Image.asset(Res.favorite)),
  //                     // ),
  //                     Positioned(
  //                       bottom: 0,
  //                       // right: 5,
  //                       child: Container(
  //                         height: 0.1 *
  //                             ScreenSize().getHeight(context),
  //                         width: 0.88 *
  //                             ScreenSize().getWidth(context),
  //                         decoration: BoxDecoration(
  //                             borderRadius: BorderRadius.only(
  //                                 bottomLeft: Radius.circular(15),
  //                                 bottomRight: Radius.circular(15)),
  //                             gradient: LinearGradient(
  //                               begin: Alignment.topCenter,
  //                               end: Alignment.bottomCenter,
  //                               colors: [
  //                                 OwnColors.C797979
  //                                     .withOpacity(0.0),
  //                                 OwnColors.C787878
  //                                     .withOpacity(0.0073),
  //                                 OwnColors.C3E3E3E
  //                                     .withOpacity(0.3391),
  //                                 Colors.black.withOpacity(0.7),
  //                               ],
  //                             )),
  //                         child: Center(
  //                           child: Container(
  //                             width: 0.83 *
  //                                 ScreenSize().getWidth(context),
  //                             child: Column(
  //                               mainAxisAlignment:
  //                                   MainAxisAlignment.start,
  //                               crossAxisAlignment:
  //                                   CrossAxisAlignment.start,
  //                               children: [
  //                                 Spacer(
  //                                   flex: 10,
  //                                 ),
  //                                 AutoSizeText(
  //                                   travelClass.type == "1"?
  //                                   travelClass.duration.toString() + S().home_filter_type2_string :
  //                                   travelClass.type == "2"? S().home_filter_type3 :
  //                                   S(). home_filter_type4,
  //                                   minFontSize: 12,
  //                                   overflow: TextOverflow.ellipsis,
  //                                   maxLines: 1,
  //                                   style:GoogleFonts.inter (
  //                                       fontSize: 12.nsp,
  //                                       fontWeight: FontWeight.w300,
  //                                       color: OwnColors.white),
  //                                 ),
  //                                 AutoSizeText(
  //                                   travelClass.title,
  //                                   minFontSize: 16,
  //                                   overflow: TextOverflow.ellipsis,
  //                                   maxLines: 1,
  //                                   style:GoogleFonts.inter (
  //                                      fontWeight: FontWeight.w500,
  //                                       fontSize: 18.nsp,
  //                                       color: OwnColors.white),
  //                                 ),
  //                                 HorizontalSpace()
  //                                     .create(context, 0.008),
  //                               ],
  //                             ),
  //                           ),
  //                         ),
  //                       ),
  //                     ),
  //
  //                     Container(
  //                       height: 0.1 *
  //                           ScreenSize().getHeight(context),
  //                       decoration: BoxDecoration(
  //                           borderRadius: BorderRadius.only(
  //                               topLeft: Radius.circular(15),
  //                               topRight: Radius.circular(15)),
  //                           gradient: LinearGradient(
  //                             begin: Alignment.bottomCenter ,
  //                             end: Alignment.topCenter,
  //                             colors: [
  //                               OwnColors.C797979
  //                                   .withOpacity(0.0),
  //                               OwnColors.C787878
  //                                   .withOpacity(0.0073),
  //                               OwnColors.C3E3E3E
  //                                   .withOpacity(0.3391),
  //                               Colors.black.withOpacity(0.7),
  //                             ],
  //                           )),
  //                       child: Padding(
  //                         padding: const EdgeInsets.only(top:5.0,left: 10),
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.start,
  //                           crossAxisAlignment: CrossAxisAlignment.start,
  //                           children: [
  //                             SizedBox(
  //                              height: 0.045 *  ScreenSize().getHeight(context),
  //                               child: AspectRatio(
  //                                 aspectRatio: 1,
  //                                 child: CachedNetworkImage(
  //                                   placeholder: (context, url) => AspectRatio(
  //                                     aspectRatio: 2.16,
  //                                     child: FractionallySizedBox(
  //                                       heightFactor: 0.5,
  //                                       widthFactor: 0.5,
  //                                       child: SpinKitRing(
  //                                         color:
  //                                         OwnColors.tran ,
  //                                       ),
  //                                     ),
  //                                   ),
  //                                   errorWidget:
  //                                       (context, url, error) =>
  //                                       Icon(
  //                                         Icons.error,
  //                                         color: Colors.red,
  //                                       ),
  //                                   imageUrl: travelClass.guilder_avatar,
  //                                   fit: BoxFit.fitHeight,
  //                                   imageBuilder: (context,
  //                                       imageProvider) =>
  //                                       Container(
  //                                         // width: 80.0,
  //                                         // height: 80.0,
  //                                         decoration: BoxDecoration(
  //                                           shape: BoxShape.circle,
  //                                           border: Border.all(width: 1,color: OwnColors.CC8C8C8
  //
  //                                           ),
  //                                           image: DecorationImage(
  //                                               image: imageProvider,
  //                                               fit: BoxFit.cover),
  //                                         ),
  //                                       ),
  //                                   // width: MediaQuery.of(context).size.width,
  //                                   // placeholder: (context, url) => new CircularProgressIndicator(),
  //                                   // placeholder: new CircularProgressIndicator(),
  //                                 ),
  //                               ),
  //                             ),
  //
  //                             VerticalSpace().create(context, 0.017),
  //                             Container(
  //                               child: Column(
  //                                 mainAxisAlignment: MainAxisAlignment.start,
  //                                 crossAxisAlignment: CrossAxisAlignment.start,
  //                                 mainAxisSize: MainAxisSize.min,
  //                                 children: [
  //                                   Text(travelClass.guilder_name,
  //                                     maxLines: 1,
  //
  //                                     overflow: TextOverflow.ellipsis,
  //                                     style:GoogleFonts.inter (
  //                                         fontSize: 16.nsp,
  //                                         color: OwnColors.white,
  //                                         fontWeight: FontWeight.w500
  //                                     ),),
  //                                   Row(
  //                                     mainAxisAlignment: MainAxisAlignment.end,
  //                                     crossAxisAlignment: CrossAxisAlignment.end,
  //                                     children: [
  //                                       Container(
  //                                         height: 0.02 * ScreenSize().getHeight(context),
  //                                         child: Image.asset(Res.star),
  //                                       ),
  //                                       Container(width: 2,),
  //                                       Text(
  //                                         travelClass.guilder_rate  ,
  //
  //                                         style:GoogleFonts.inter (
  //                                             fontSize: 12.nsp,
  //                                             color: OwnColors.white,
  //                                             fontWeight: FontWeight.w600
  //                                         ),
  //                                       ),
  //                                       Text(
  //                                         " (" + double.parse(travelClass.guilder_comment).toStringAsFixed(0)+S().home_rate_number+")",
  //
  //                                         style:GoogleFonts.inter (
  //                                           fontSize: 12.nsp,
  //                                           color: OwnColors.white,
  //                                           fontWeight: FontWeight.w300
  //
  //                                         ),
  //                                       ),
  //                                     ],
  //                                   ),
  //                                 ],
  //                               ),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //             ),
  //             HorizontalSpace().create(context, 0.005),
  //             Container(
  //               width: 0.83 * ScreenSize().getWidth(context),
  //               height: 0.025 * ScreenSize().getHeight(context),
  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.start,
  //                 crossAxisAlignment: CrossAxisAlignment.center,
  //                 children: [
  //
  //                   Padding(
  //                     padding: const EdgeInsets.only(
  //                         top: 2.0, bottom: 2.0, right: 4),
  //                     child: Image.asset(Res.peoples),
  //                   ),
  //                   AutoSizeText(
  //                     travelClass.views + " " + S().travel_views,
  //                     minFontSize: 10,
  //                     style:GoogleFonts.inter (
  //                       fontSize: 10.nsp,
  //                       color: OwnColors.black,
  //                       fontWeight: FontWeight.w500,
  //                     ),
  //                   ),
  //                   Padding(
  //                     padding: const EdgeInsets.only(
  //                         top: 2.0, bottom: 2.0, right: 4, left: 8),
  //                     child: Image.asset(Res.chat),
  //                   ),
  //                   Expanded(
  //                     child: AutoSizeText(
  //                       travelClass.comment_num +
  //                           " " +
  //                           S().travel_comment,
  //                       minFontSize: 10,
  //                       style:GoogleFonts.inter (
  //                         fontSize: 10.nsp,
  //                         color: OwnColors.black,
  //                         fontWeight: FontWeight.w500,
  //                       ),
  //                     ),
  //                   ),
  //                   AutoSizeText(
  //                     DateTools().intlYMD( travelClass.date),
  //                     minFontSize: 10,
  //                     style:GoogleFonts.inter (
  //                       fontSize: 12.nsp,
  //                       color: OwnColors.CCCCCCC,
  //                       fontWeight: FontWeight.w400
  //                     ),
  //                   )
  //                 ],
  //               ),
  //             ),
  //
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }

  void hotClick(TravelClass travelClass) {
    GlobalData.refreshArticle = false;
    PushNewScreen()
        .normalPushThen(context, TravelPage(travelClass: travelClass, edit: false),refreshComment);
  }

  void recommendClick(TravelClass travelClass) {
    GlobalData.refreshArticle = false;
    PushNewScreen()
        .normalPushThen(context, TravelPage(travelClass: travelClass, edit: false),refreshComment);
  }
  void refreshComment(){
    print("refreshComment");


    if(GlobalData.curernt_travel !=null) {
      int index = hotTravelList.indexWhere((element) =>
      element.id == GlobalData.curernt_travel.id);
      if(index != -1) {
        hotTravelList[index].views = GlobalData.curernt_travel.views;
        hotTravelList[index].comment_num = GlobalData.curernt_travel.comment_num;
      }
      print("index" +index.toString());
      index = searchList.indexWhere((element) =>
      element.id == GlobalData.curernt_travel.id);
      if(index != -1) {
        searchList[index].views = GlobalData.curernt_travel.views;
        searchList[index].comment_num = GlobalData.curernt_travel.comment_num;
      }
      print("index" +index.toString());

      index = recommendTravelList.indexWhere((element) =>
      element.id == GlobalData.curernt_travel.id);
      if(index != -1) {
        recommendTravelList[index].views = GlobalData.curernt_travel.views;
        recommendTravelList[index].comment_num = GlobalData.curernt_travel.comment_num;
      }
      print("index" +index.toString());

      setState(() {
        print("refreshScore setState");

      });
    }
  }

  Future<void> _pushSaved() async {
    print("_pushSaved");
  }

  Future<void> setFilter() async {
    Tools().dismissK(context);
    int result = await showCupertinoModalBottomSheet(
      barrierColor:Colors.black54,
      topRadius: Radius.circular(30),
      expand: false,
      context: context,
      enableDrag: false,
      backgroundColor: Colors.transparent,
      builder: (context) => HomeFilterPage(),
    ).whenComplete(() {});
    if(result == 1){
      SharePre.prefs.setString(GDefine.articleFilter, "true");
      // getRecommentArticleFilterAPI();
      pageCnt = 1;
      pageCntSearch = 1;
      if(searchEdit.text == "") {
        getRecommentArticleAPI(false);
      }else {
        getRecommentArticleSearchAPI(false);
      }
    }else if(result == 2){
      pageCnt = 1;
      pageCntSearch = 1;
      if(searchEdit.text == "") {
        getRecommentArticleAPI(false);
      }else {
        getRecommentArticleSearchAPI(false);
      }
    }
  }

  // void getRecommentArticleFilterAPI() async {
  //   print("getRecommentArticleAPI");
  //
  //   String startDate = "", endDate = "";
  //   if (FilterData.travel_date_choose == 0) {
  //
  //   } else if (FilterData.travel_date_choose == 1) {
  //     endDate = DateFormat('yyyy/MM/dd').format((DateTime.now()));
  //     startDate = DateFormat('yyyy/MM/dd')
  //         .format(DateTools().findLastDateByDateTime(DateTime.now(), -7));
  //   } else if (FilterData.travel_date_choose == 2) {
  //     endDate = DateFormat('yyyy/MM/dd').format((DateTime.now()));
  //     startDate = DateFormat('yyyy/MM/dd')
  //         .format(DateTools().findLastDateByDateTime(DateTime.now(), -30));
  //   } else if (FilterData.travel_date_choose == 3) {
  //     endDate = DateFormat('yyyy/MM/dd').format((DateTime.now()));
  //     startDate = DateFormat('yyyy/MM/dd')
  //         .format(DateTools().findLastDateByDateTime(DateTime.now(), -365));
  //   }
  //
  //   ResultData resultData = await AppApi.getInstance().getArticleListFilter(
  //       context,
  //       true,
  //       SharePre.prefs.getString(GDefine.user_id),
  //       "",
  //       startDate,
  //       endDate,
  //       FilterData.travel_sort_type.toString(),
  //       FilterData.travel_type.toString(),
  //       "",
  //       cntPerPage.toString(),
  //       FilterData.travel_duration.start.toString(),
  //       FilterData.travel_duration.end.toString(),"false",0);
  //   if (resultData.isSuccess()) {
  //     print(resultData.data.toString());
  //     recommendTravelList.clear();
  //     for (Map<String, dynamic> a in resultData.data) {
  //       TravelClass travelClass = TravelClass.fromJson(a);
  //       travelClass.sectionTag = new List();
  //       if (a["content_list"] != null) {
  //         List<dynamic> content_list = jsonDecode(a["content_list"].toString());
  //         travelClass.contentListDynamic = new List();
  //
  //         List<TravelSubClass> subList = new List();
  //         for (Map<String, dynamic> sub in content_list) {
  //           TravelSubClass travelSubClass = new TravelSubClass();
  //           List<dynamic> content_list2 = sub["contentList"];
  //           List<TravelContentClass> travelContentClassList = new List();
  //           // if(content_list2.length>0) {
  //           Map<String, dynamic> dy = new Map();
  //           dy["title"] =
  //           sub["title"].toString() == "null" ? "" : sub["title"].toString();
  //           dy["type"] = "0";
  //           dy["description"] = "";
  //           dy["google"] = "";
  //           dy["image"] = [];
  //           travelClass.contentListDynamic.add(dy);
  //           // }
  //
  //
  //           for (Map<String, dynamic> subsub in content_list2) {
  //             TravelContentClass travelContentClass = new TravelContentClass();
  //             travelContentClass.image = List<String>.from(subsub["image"]);
  //             travelContentClass.image.removeWhere((element) =>
  //             element == "" || element == "null");
  //             travelContentClass.title = subsub["title"];
  //             travelContentClass.description =
  //             subsub["description"].toString() == "null"
  //                 ? ""
  //                 : subsub["description"].toString();
  //             travelContentClass.id = subsub["id"].toString();
  //             travelContentClass.google =
  //             subsub["google"].toString() == "null" ? "" : subsub["google"]
  //                 .toString();
  //             travelContentClass.key = new GlobalKey();
  //
  //             travelContentClassList.add(travelContentClass);
  //
  //             Map<String,dynamic> dy2 = new Map();
  //             dy2["title"] = travelContentClass.title;
  //             dy2["type"] = "1";
  //             dy2["description"] = travelContentClass.description;
  //             dy2["google"] = travelContentClass.google;
  //             dy2["image"] = List<String>.from(subsub["image"]);
  //
  //             travelClass.contentListDynamic.add(dy2);
  //
  //           }
  //           travelSubClass.title =
  //           sub["title"].toString() == "null" ? "" : sub["title"].toString();
  //           travelSubClass.id = sub["id"].toString();
  //           travelSubClass.key = new GlobalKey();
  //           if(sub["title"]!="" && sub["title"].toString() != "null") {
  //             travelClass.sectionTag.add(new SectionClass(title: sub["title"], key:travelSubClass.key  ));
  //           }
  //
  //           if (travelContentClassList.isNotEmpty) {
  //             travelSubClass.content = travelContentClassList;
  //           }
  //
  //           subList.add(travelSubClass);
  //         }
  //
  //         travelClass.contentList = subList;
  //         recommendTravelList.add(travelClass);
  //       }
  //     }
  //
  //     setState(() {
  //       print("getTouristAPI");
  //       loadingBool = false;
  //     });
  //     if(searchEdit.text.isNotEmpty){
  //       searchTravel();
  //     }
  //     print(refreshController.controlFinishLoad);
  //     refreshController.finishRefresh();
  //     refreshController.resetFooter();
  //   } else {
  //     // WarningDialog.showIOSAlertDialog(context, "下載遊記失敗", resultData.msg, "確認");
  //     refreshController.finishRefresh();
  //     refreshController.resetFooter();
  //   }
  // }

  void getRecommentArticleAPI(bool page) async {
    print("getRecommentArticleAPI");
    print("cntPerPage " + cntPerPage.toString());
    int durationMax = 0;
    String startDate = "", endDate = "";

    if (FilterData.travel_date_choose == 0) {

    } else if (FilterData.travel_date_choose == 1) {
      endDate = DateFormat('yyyy/MM/dd').format((DateTime.now()));
      startDate = DateFormat('yyyy/MM/dd')
          .format(DateTools().findLastDateByDateTime(DateTime.now(), -7));
    } else if (FilterData.travel_date_choose == 2) {
      endDate = DateFormat('yyyy/MM/dd').format((DateTime.now()));
      startDate = DateFormat('yyyy/MM/dd')
          .format(DateTools().findLastDateByDateTime(DateTime.now(), -30));
    } else if (FilterData.travel_date_choose == 3) {
      endDate = DateFormat('yyyy/MM/dd').format((DateTime.now()));
      startDate = DateFormat('yyyy/MM/dd')
          .format(DateTools().findLastDateByDateTime(DateTime.now(), -365));
    }

    ResultData resultData = await AppApi.getInstance().getArticleListPage(
        context,
        true,
        SharePre.prefs.getString(GDefine.user_id),
        "",
        startDate,
        endDate,
        FilterData.travel_sort_type.toString(),
        FilterData.travel_type.toString(),
        "",
        cntPerPage.toString(),
        FilterData.travel_duration.start.toString(),
        FilterData.travel_duration.end.toString(),"false",pageCnt);

    // ResultData resultData = await AppApi.getInstance().getArticleListPage(
    //     context,
    //     true,
    //     SharePre.prefs.getString(GDefine.user_id),
    //     "0",
    //     "",
    //     "",
    //     "",
    //     "",
    //     "0",
    //     "",
    //     cntPerPage.toString(),"false",pageCnt);
    if (resultData.isSuccess()) {
      print(resultData.data.toString());

      if(page){

      }else {
        recommendTravelList.clear();
      }
      totalCnt = resultData.data["total"];
      for (Map<String, dynamic> a in resultData.data["datas"]) {
        TravelClass travelClass = TravelClass.fromJson(a);
        travelClass.sectionTag = new List();
        print("duration " + travelClass.duration);
        if(int.parse(travelClass.duration) > durationMax){

          durationMax = int.parse(travelClass.duration);
        }
        if (a["content_list"] != null) {
          List<dynamic> content_list = jsonDecode(a["content_list"].toString());
          travelClass.contentListDynamic = new List();
          List<TravelSubClass> subList = new List();
          for (Map<String, dynamic> sub in content_list) {
            TravelSubClass travelSubClass = new TravelSubClass();
            List<dynamic> content_list2 = sub["contentList"];
            List<TravelContentClass> travelContentClassList = new List();
            // if(content_list2.length>0) {
            Map<String, dynamic> dy = new Map();
            dy["title"] =
            sub["title"].toString() == "null" ? "" : sub["title"].toString();
            dy["type"] = "0";
            dy["description"] = "";
            dy["google"] = "";
            dy["image"] = [];
            travelClass.contentListDynamic.add(dy);
            // }
            for (Map<String, dynamic> subsub in content_list2) {
              TravelContentClass travelContentClass = new TravelContentClass();
              travelContentClass.image = List<String>.from(subsub["image"]);
              travelContentClass.image.removeWhere((element) =>
              element == "" || element == "null");
              travelContentClass.title = subsub["title"];
              travelContentClass.description =
              subsub["description"].toString() == "null"
                  ? ""
                  : subsub["description"].toString();
              travelContentClass.id = subsub["id"].toString();
              travelContentClass.google =
              subsub["google"].toString() == "null" ? "" : subsub["google"]
                  .toString();
              travelContentClass.key = new GlobalKey();

              travelContentClassList.add(travelContentClass);

              Map<String,dynamic> dy2 = new Map();
              dy2["title"] = travelContentClass.title;
              dy2["type"] = "1";
              dy2["description"] = travelContentClass.description;
              dy2["google"] = travelContentClass.google;
              dy2["image"] = List<String>.from(subsub["image"]);

              travelClass.contentListDynamic.add(dy2);

            }
            travelSubClass.title =
            sub["title"].toString() == "null" ? "" : sub["title"].toString();
            travelSubClass.id = sub["id"].toString();
            travelSubClass.key = new GlobalKey();
            if(sub["title"]!="" && sub["title"].toString() != "null") {
              travelClass.sectionTag.add(new SectionClass(title: sub["title"], key:travelSubClass.key  ));
            }

            if (travelContentClassList.isNotEmpty) {
              travelSubClass.content = travelContentClassList;
            }

            subList.add(travelSubClass);
          }

          travelClass.contentList = subList;
          recommendTravelList.add(travelClass);
        }
      }
      print("durationMax " + durationMax.toString());

      // print("FilterData.travel_end_duration " + FilterData.travel_end_duration.toString());
      // SharePre.prefs.setInt(GDefine.durationMax, durationMax);
      // if(FilterData.travel_end_duration == -1 || FilterData.travel_end_duration > durationMax ){
      //   FilterData.travel_end_duration = double.parse(durationMax.toString());
      // }


      setState(() {
        print("getTouristAPI");
        loadingBool = false;
      });
      print(refreshController.controlFinishLoad);
      refreshController.finishRefresh();
      refreshController.resetFooter();
    } else {
      refreshController.finishRefresh();
      refreshController.resetFooter();
      // WarningDialog.showIOSAlertDialog(context, "下載遊記失敗", resultData.msg, "確認");
    }
  }


  void getRecommentArticleSearchAPI(bool page) async {
    print("getRecommentArticleSearchAPI");
    print("cntPerPage " + cntPerPage.toString());
    int durationMax = 0;
    String startDate = "", endDate = "";


    if (FilterData.travel_date_choose == 0) {

    } else if (FilterData.travel_date_choose == 1) {
      endDate = DateFormat('yyyy/MM/dd').format((DateTime.now()));
      startDate = DateFormat('yyyy/MM/dd')
          .format(DateTools().findLastDateByDateTime(DateTime.now(), -7));
    } else if (FilterData.travel_date_choose == 2) {
      endDate = DateFormat('yyyy/MM/dd').format((DateTime.now()));
      startDate = DateFormat('yyyy/MM/dd')
          .format(DateTools().findLastDateByDateTime(DateTime.now(), -30));
    } else if (FilterData.travel_date_choose == 3) {
      endDate = DateFormat('yyyy/MM/dd').format((DateTime.now()));
      startDate = DateFormat('yyyy/MM/dd')
          .format(DateTools().findLastDateByDateTime(DateTime.now(), -365));
    }

    if (searchEdit.text == "") {
      searchList.addAll(hotTravelList);
      // bottomList.addAll(recommendTravelList);
    } else {

      ResultData resultData = await AppApi.getInstance().getArticleListSearch(
          context,
          true,
          SharePre.prefs.getString(GDefine.user_id),
          "",
          startDate,
          endDate,
          FilterData.travel_sort_type.toString(),
          FilterData.travel_type.toString(),
          "",
          cntPerPage.toString(),
          FilterData.travel_duration.start.toString(),
          FilterData.travel_duration.end.toString(),"false",pageCntSearch,searchEdit.text);

      if (resultData.isSuccess()) {
        print(resultData.data.toString());
        if(page){

        }else {
          searchList.clear();
        }
        totalCntSEarch = resultData.data["total"];
        for (Map<String, dynamic> a in resultData.data["datas"]) {
          TravelClass travelClass = TravelClass.fromJson(a);
          travelClass.sectionTag = new List();
          print("duration " + travelClass.duration);
          if(int.parse(travelClass.duration) > durationMax){

            durationMax = int.parse(travelClass.duration);
          }
          if (a["content_list"] != null) {
            List<dynamic> content_list = jsonDecode(a["content_list"].toString());
            travelClass.contentListDynamic = new List();
            List<TravelSubClass> subList = new List();
            for (Map<String, dynamic> sub in content_list) {
              TravelSubClass travelSubClass = new TravelSubClass();
              List<dynamic> content_list2 = sub["contentList"];
              List<TravelContentClass> travelContentClassList = new List();
              // if(content_list2.length>0) {
              Map<String, dynamic> dy = new Map();
              dy["title"] =
              sub["title"].toString() == "null" ? "" : sub["title"].toString();
              dy["type"] = "0";
              dy["description"] = "";
              dy["google"] = "";
              dy["image"] = [];
              travelClass.contentListDynamic.add(dy);
              // }
              for (Map<String, dynamic> subsub in content_list2) {
                TravelContentClass travelContentClass = new TravelContentClass();
                travelContentClass.image = List<String>.from(subsub["image"]);
                travelContentClass.image.removeWhere((element) =>
                element == "" || element == "null");
                travelContentClass.title = subsub["title"];
                travelContentClass.description =
                subsub["description"].toString() == "null"
                    ? ""
                    : subsub["description"].toString();
                travelContentClass.id = subsub["id"].toString();
                travelContentClass.google =
                subsub["google"].toString() == "null" ? "" : subsub["google"]
                    .toString();
                travelContentClass.key = new GlobalKey();

                travelContentClassList.add(travelContentClass);

                Map<String,dynamic> dy2 = new Map();
                dy2["title"] = travelContentClass.title;
                dy2["type"] = "1";
                dy2["description"] = travelContentClass.description;
                dy2["google"] = travelContentClass.google;
                dy2["image"] = List<String>.from(subsub["image"]);

                travelClass.contentListDynamic.add(dy2);

              }
              travelSubClass.title =
              sub["title"].toString() == "null" ? "" : sub["title"].toString();
              travelSubClass.id = sub["id"].toString();
              travelSubClass.key = new GlobalKey();
              if(sub["title"]!="" && sub["title"].toString() != "null") {
                travelClass.sectionTag.add(new SectionClass(title: sub["title"], key:travelSubClass.key  ));
              }

              if (travelContentClassList.isNotEmpty) {
                travelSubClass.content = travelContentClassList;
              }

              subList.add(travelSubClass);
            }

            travelClass.contentList = subList;
            searchList.add(travelClass);
          }
        }


        print("searchList " + searchList.length.toString());
        setState(() {
          print("getRecommentArticleSearchAPI2");
          loadingBool = false;

        });
        print(refreshController.controlFinishLoad);
        refreshController.finishRefresh();
        refreshController.resetFooter();
      } else {
        refreshController.finishRefresh();
        refreshController.resetFooter();
        // WarningDialog.showIOSAlertDialog(context, "下載遊記失敗", resultData.msg, "確認");
      }
    }
    searchLoading = false;

    setState(() {});

  }

  void getWeekArticleAPI() async {
    print("getWeekArticleAPI");

    int nowyear = DateTime.now().year; //今年
    int nowmonth = DateTime.now().month; //这个月是几月
    print("month=" + nowmonth.toString());
    int nowday = DateTime.now().day; //今天
    int weekday = DateTime.now().weekday; //今天是本周的第几天
    DateTime lastweekstart =
        new DateTime(nowyear, nowmonth, nowday - weekday - 6); //上周一的时间
    String start_string = DateFormat('yyyy/MM/dd')
        .format(DateTools().findFirstDateOfTheWeek(lastweekstart));
    String end_string = DateFormat('yyyy/MM/dd')
        .format(DateTools().findLastDateOfTheWeek(lastweekstart));

    ResultData resultData = await AppApi.getInstance().getArticleList(
        context,
        true,
        SharePre.prefs.getString(GDefine.user_id),
        "0",
        start_string,
        end_string,
        "",
        "0",
        "0",
        "",
        "10","true");
    if (resultData.isSuccess()) {
      print(resultData.data.toString());
      hotTravelList.clear();
      for (Map<String, dynamic> a in resultData.data) {
        TravelClass travelClass = TravelClass.fromJson(a);
        travelClass.sectionTag = new List();
        if (a["content_list"] != null) {
          List<dynamic> content_list = jsonDecode(a["content_list"].toString());
          travelClass.contentListDynamic = new List();
          List<TravelSubClass> subList = new List();
          for (Map<String, dynamic> sub in content_list) {
            TravelSubClass travelSubClass = new TravelSubClass();
            List<dynamic> content_list2 = sub["contentList"];
            List<TravelContentClass> travelContentClassList = new List();

            // if(content_list2.length>0) {
            Map<String, dynamic> dy = new Map();
            dy["title"] =
            sub["title"].toString() == "null" ? "" : sub["title"].toString();
            dy["type"] = "0";
            dy["description"] = "";
            dy["google"] = "";
            dy["image"] = [];
            travelClass.contentListDynamic.add(dy);
            // }

            for (Map<String, dynamic> subsub in content_list2) {
              TravelContentClass travelContentClass = new TravelContentClass();
              travelContentClass.image = List<String>.from(subsub["image"]);
              travelContentClass.image.removeWhere((element) =>
              element == "" || element == "null");
              travelContentClass.title = subsub["title"];
              travelContentClass.description =
              subsub["description"].toString() == "null"
                  ? ""
                  : subsub["description"].toString();
              travelContentClass.id = subsub["id"].toString();
              travelContentClass.google =
              subsub["google"].toString() == "null" ? "" : subsub["google"]
                  .toString();
              travelContentClass.key = new GlobalKey();

              travelContentClassList.add(travelContentClass);

              Map<String,dynamic> dy2 = new Map();
              dy2["title"] = travelContentClass.title;
              dy2["type"] = "1";
              dy2["description"] = travelContentClass.description;
              dy2["google"] = travelContentClass.google;
              dy2["image"] = List<String>.from(subsub["image"]);

              travelClass.contentListDynamic.add(dy2);

            }
            travelSubClass.title =
            sub["title"].toString() == "null" ? "" : sub["title"].toString();
            travelSubClass.id = sub["id"].toString();
            travelSubClass.key = new GlobalKey();
            if(sub["title"]!="" && sub["title"].toString() != "null") {
              travelClass.sectionTag.add(new SectionClass(title: sub["title"], key:travelSubClass.key  ));
            }

            if (travelContentClassList.isNotEmpty) {
              travelSubClass.content = travelContentClassList;
            }

            subList.add(travelSubClass);
          }

          travelClass.contentList = subList;
          hotTravelList.add(travelClass);
        }
      }

      setState(() {
        print("getTouristAPI");
        loadingBool = false;
      });
    } else {
      // WarningDialog.showIOSAlertDialog(context, "下載遊記失敗", resultData.msg,"確認");
    }
  }
  void getScoreCount() async {
    print("getScoreCount");


    ResultData resultData = await AppApi.getInstance().getScoreCount(
        context,
        true,
        SharePre.prefs.getString(GDefine.user_id),
         );
    if (resultData.isSuccess()) {
      print(resultData.data.toString());
      try{
        GlobalData.article_duration_max = double.parse (resultData.data["article_duration_max"].toString());
        GlobalData.tourist_comment_max = double.parse (resultData.data["tourist_count"].toString());
        SharePre.prefs.setInt(GDefine.durationMax,GlobalData.article_duration_max.toInt());
        SharePre.prefs.setInt(GDefine.commentMax,GlobalData.tourist_comment_max.toInt());

        if(FilterData.travel_end_duration == -1 || FilterData.travel_end_duration > GlobalData.tourist_comment_max ){
          FilterData.travel_end_duration = GlobalData.tourist_comment_max;
        }


        if(FilterData.guild_endNum == -1 ||FilterData.guild_endNum > GlobalData.tourist_comment_max){
          FilterData.guild_endNum = GlobalData.tourist_comment_max;
        }


      }catch (e){

      }
      print("article_duration_max " + GlobalData.article_duration_max.toString());
      print("tourist_comment_max " + GlobalData.tourist_comment_max.toString());
      print("SharePre.prefs.getInt(GDefine.durationMax).toString() " + SharePre.prefs.getInt(GDefine.durationMax).toString());

    } else {

    }
  }

  // Future<void> searchTravel() async {
  //   print("searchTravel");
  //
  //   print("searchEdit.text" + searchEdit.text);
  //   print("searchOffset" + searchOffset.value.toString());
  //   searchList.clear();
  //
  //   if (searchEdit.text == "") {
  //     searchList.addAll(hotTravelList);
  //     // bottomList.addAll(recommendTravelList);
  //   } else {
  //     // List<TravelClass> temp = hotTravelList.where((element) =>
  //     //     element.title.contains(searchEdit.text) ||
  //     //     element.guilder_name.contains(searchEdit.text) ||
  //     //     element.tag.indexWhere((cc) => cc.toString().contains(searchEdit.text)) != -1).toList();
  //     // topList.addAll(temp);
  //
  //     List<TravelClass> temp2 = recommendTravelList
  //         .where((element) =>
  //             element.title
  //                 .toLowerCase()
  //                 .contains(searchEdit.text.toLowerCase()) ||
  //             element.guilder_name
  //                 .toLowerCase()
  //                 .contains(searchEdit.text.toLowerCase()) ||
  //             element.tag.indexWhere((cc) => cc
  //                     .toString()
  //                     .toLowerCase()
  //                     .contains(searchEdit.text.toLowerCase())) !=
  //                 -1)
  //         .toList();
  //     searchList.addAll(temp2);
  //   }
  //
  //   setState(() {});
  // }
}
class ExpandedSection extends StatefulWidget {

  final Widget child;
  final bool expand;
  ExpandedSection({this.expand = false, this.child});

  @override
  _ExpandedSectionState createState() => _ExpandedSectionState();
}

class _ExpandedSectionState extends State<ExpandedSection> with SingleTickerProviderStateMixin {
  AnimationController expandController;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    prepareAnimations();
    _runExpandCheck();
  }

  ///Setting up the animation
  void prepareAnimations() {
    expandController = AnimationController(
        vsync: this,
        duration: Duration(milliseconds: 200)
    );
    animation = CurvedAnimation(
      parent: expandController,
      curve: Curves.fastOutSlowIn,
    );
  }

  void _runExpandCheck() {
    print("_runExpandCheck " + widget.expand.toString());
    if(widget.expand) {
      expandController.forward();
    }
    else {
      expandController.reverse();
    }
  }

  @override
  void didUpdateWidget(ExpandedSection oldWidget) {
    super.didUpdateWidget(oldWidget);
    print("didUpdateWidget");
    _runExpandCheck();
  }

  @override
  void dispose() {
    expandController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizeTransition(
        axisAlignment: 1.0,
        sizeFactor: animation,
        child: widget.child
    );
  }
}