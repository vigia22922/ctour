import 'dart:async';
import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/TopBar.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/FilterData.dart';
import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/Tools.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';


import 'package:intl/intl.dart';

import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';

class HomeFilterPage extends StatefulWidget {
  HomeFilterPage({Key key}) : super(key: key);

  @override
  _HomeFilterPageState createState() => _HomeFilterPageState();
}

class _HomeFilterPageState extends State<HomeFilterPage> {
  @override
  Future<void> initState() {
    super.initState();
    if(SharePre.prefs.getInt(GDefine.durationMax)!=null) {
      endNum = SharePre.prefs.getInt(GDefine.durationMax).toString();
    }

    print(endNum);
    print( FilterData.travel_end_duration);


    if(FilterData.travel_duration.end > double.parse(endNum)){
      FilterData.travel_duration = SfRangeValues(FilterData.travel_start_duration,double.parse(endNum));
      FilterData.travel_end_duration = double.parse(endNum);
    }

    if(FilterData.travel_duration.start > double.parse(endNum)){
      FilterData.travel_duration = SfRangeValues(0.0,FilterData.travel_end_duration);
      FilterData.travel_start_duration = 0.0;
    }

    if(FilterData.travel_type !=1 ){
      FilterData.travel_duration = SfRangeValues(0.0,double.parse(endNum)  );
      FilterData.travel_end_duration = double.parse(endNum);
      FilterData.travel_start_duration = 0.0;
    }

  }
  List<String> orderMethodList = [S().home_filter_order_method1,S().home_filter_order_method2,  S().home_filter_order_method4];
  List<String> dateList = [S().home_filter_date1,S().home_filter_date2, S().home_filter_date3,S().home_filter_date4];
  List<String> typeList = [S().home_filter_type1,S().home_filter_type2, S().home_filter_type3,S().home_filter_type4];

  String orderMethod = S().home_filter_order_method1;
  String dateChoosed = S().home_filter_date1;
  String typeChoosed = S().home_filter_type1;
  double durationNow = 3;
  SfRangeValues duration = SfRangeValues(FilterData.travel_start_duration, FilterData.travel_end_duration == -1? 1:FilterData.travel_end_duration);

  bool isClean = false;

  String startNum = "0", endNum = "30";

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap:(){
        Tools().dismissK(context);
        print("onTap");
      },
      child: Material(
          color: OwnColors.white,
          child: Container(
            height:  ScreenSize().getHeightWTop(context),
            child: Column(
              children: <Widget>[

                Container(
                    height: 0.007 * ScreenSize().getHeight(context),
                    width: 0.2 * ScreenSize().getWidth(context),
                    decoration: const BoxDecoration(
                      color: OwnColors.white,
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                    )),
                HorizontalSpace().create(context, 0.02),
                Topbar().filterCleanCreate(context, backClick, cleanClick, filterClick),


                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        HorizontalSpace().create(context, 0.02),
                        Container(
                          // height: 0.023 * ScreenSize().getHeight(context),
                          width: 0.9 * ScreenSize().getWidth(context),
                          child: AutoSizeText(
                            S().home_filter_order_method,
                            textScaleFactor: 1.0,
                            style: GoogleFonts.inter(
                                fontWeight: FontWeight.w700,
                                fontSize: 16.nsp,
                                color: OwnColors.black),
                          ),
                        ),
                        HorizontalSpace().create(context, 0.005),
                        Container(
                          width: 0.9 * ScreenSize().getWidth(context),
                          child: ListView.builder(
                              shrinkWrap: true,
                              itemCount: orderMethodList.length,
                              physics: NeverScrollableScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding:
                                  EdgeInsets.only(left: 0.0, right: 8),
                                  child: radioItem(
                                      orderMethodList[index],FilterData.travel_sort_type == 0?S().home_filter_order_method1:FilterData.travel_sort_type == 1?S().home_filter_order_method2:S().home_filter_order_method4,updateOrder
                                  ),
                                );
                              }),
                        ),
                        HorizontalSpace().create(context, 0.02),
                        Container(
                          height: 1,
                          width: 0.9 * ScreenSize().getWidth(context),
                          color: OwnColors.CF4F4F4,
                        ),
                        HorizontalSpace().create(context, 0.02),
                        Container(
                          // height: 0.023 * ScreenSize().getHeight(context),
                          width: 0.9 * ScreenSize().getWidth(context),
                          child: AutoSizeText(
                            S().home_filter_date,
                            textScaleFactor: 1.0,
                            style: GoogleFonts.inter(
                                fontWeight: FontWeight.w700,
                                fontSize: 16.nsp,
                                color: OwnColors.black),
                          ),
                        ),
                        HorizontalSpace().create(context, 0.005),
                        Container(
                          width: 0.9 * ScreenSize().getWidth(context),
                          child: ListView.builder(
                              shrinkWrap: true,
                              itemCount: dateList.length,
                              physics: NeverScrollableScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding:
                                  EdgeInsets.only(left: 0.0, right: 8),
                                  child: radioItem(
                                      dateList[index],FilterData.travel_date_choose == 0? S().home_filter_date1:FilterData.travel_date_choose == 1?S().home_filter_date2:FilterData.travel_date_choose == 2?S().home_filter_date3:S().home_filter_date4,updateDate
                                  ),
                                );
                              }),
                        ),
                        HorizontalSpace().create(context, 0.02),
                        Container(
                          height: 1,
                          width: 0.9 * ScreenSize().getWidth(context),
                          color: OwnColors.CF4F4F4,
                        ),
                        HorizontalSpace().create(context, 0.02),
                        Container(
                          // height: 0.023 * ScreenSize().getHeight(context),
                          width: 0.9 * ScreenSize().getWidth(context),
                          child: AutoSizeText(
                            S().home_filter_type,
                            textScaleFactor: 1.0,
                            style: GoogleFonts.inter(
                                fontWeight: FontWeight.w700,
                                fontSize: 16.nsp,
                                color: OwnColors.black),
                          ),
                        ),
                        HorizontalSpace().create(context, 0.005),
                        Container(
                          width: 0.9 * ScreenSize().getWidth(context),
                          child: ListView.builder(
                              shrinkWrap: true,
                              itemCount: typeList.length,
                              physics: NeverScrollableScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding:
                                  EdgeInsets.only(left: 0.0, right: 8),
                                  child: radioItem(
                                      typeList[index],FilterData.travel_type == 0?S().home_filter_type1:FilterData.travel_type == 1?S().home_filter_type2:FilterData.travel_type == 2?S().home_filter_type3:S().home_filter_type4,updateType
                                  ),
                                );
                              }),
                        ),
                        HorizontalSpace().create(context, 0.02),
                        Container(
                          height: 1,
                          width: 0.9 * ScreenSize().getWidth(context),
                          color: OwnColors.CF4F4F4,
                        ),
                        HorizontalSpace().create(context, 0.02),
                        Container(
                          // height: 0.023 * ScreenSize().getHeight(context),
                          width: 0.9 * ScreenSize().getWidth(context),
                          child: AutoSizeText(
                            S().home_filter_duration,
                            textScaleFactor: 1.0,
                            style: GoogleFonts.inter(
                                fontWeight: FontWeight.w700,
                                fontSize: 16.nsp,
                                color: FilterData.travel_isDay?OwnColors.black:OwnColors.CC8C8C8),
                          ),
                        ),
                        HorizontalSpace().create(context, 0.02),
                        Container(
                          width: 0.9 * ScreenSize().getWidth(context),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [

                              AutoSizeText( FilterData.travel_start_duration .toStringAsFixed(0)+ S().personal_my_travel_day,style: GoogleFonts.inter(
                                  fontSize: 14.nsp,fontWeight: FontWeight.w500, color: FilterData.travel_isDay?OwnColors.black:OwnColors.CC8C8C8
                              ),),
                              AutoSizeText( FilterData.travel_end_duration .toStringAsFixed(0)+ S().personal_my_travel_day,style: GoogleFonts.inter(
                                  fontSize: 14.nsp,fontWeight: FontWeight.w500,color: FilterData.travel_isDay?OwnColors.black:OwnColors.CC8C8C8
                              ),)
                            ],),
                        ),
                        HorizontalSpace().create(context, 0.02),
                        Container(
                          width: 0.9 * ScreenSize().getWidth(context),

                          child: SfRangeSliderTheme(
                            data: SfRangeSliderThemeData(
                              overlayRadius : 0.0,
                              thumbColor:  OwnColors.white,
                              thumbStrokeWidth:  1.0,
                              activeTrackHeight: 2.0,
                              inactiveTrackHeight: 2.0,
                              thumbStrokeColor:  FilterData.travel_isDay?OwnColors.CC8C8C8:OwnColors.CC8C8C8,
                              activeTrackColor: FilterData.travel_isDay?OwnColors.CTourOrange:OwnColors.CC8C8C8,
                              inactiveTrackColor: OwnColors.CC8C8C8,
                            ),
                            child: SfRangeSlider(

                              min: 0.0,
                              max: double.parse(endNum),
                              // activeColor: OwnColors.CTourOrange,
                              // inactiveColor: OwnColors.CC8C8C8,

                              values: FilterData.travel_duration,
                              stepSize: 1.0,


                              // enableTooltip: true,
                              minorTicksPerInterval: 1,

                              onChanged: (dynamic newValue){
                                setState(() {
                                  if(FilterData.travel_isDay){
                                    duration = newValue;
                                    FilterData.travel_duration = newValue;
                                    FilterData.travel_start_duration = double.parse ((newValue.start.round()).toString() );
                                    FilterData.travel_end_duration =double.parse ((newValue.end.round()).toString() );
                                    //
                                    // FilterData.travel_start_duration = double.parse(duration.start.toString() ) ;
                                    // FilterData.travel_end_duration = double.parse(duration.end.toString() ) ;
                                  }



                                });
                              },
                            ),
                            // child: SfSlider(
                            //
                            //
                            //       min: 0.0,
                            //       max: 1300.0,
                            //       value: lightTime,
                            //       interval: 20,
                            //       showTicks: false,
                            //       showLabels: false,
                            //       enableTooltip: false,
                            //       activeColor:OwnColors.BtnDeepBlue,
                            //       inactiveColor: OwnColors.EBEBEB,
                            //       minorTicksPerInterval: 1,
                            //       onChanged: (dynamic value){
                            //         setState(() {
                            //           lightTime = value;
                            //         });}),
                          ),

                        ),
                        HorizontalSpace().create(context, 0.02),
                        ScreenSize().createBottomPad(context),

                      ],
                    ),
                  ),
                ),



              ],
            ),
          )),
    );
  }

  Widget radioItem(String item,String selected,Function update){
    return Container(
      // height: 0.04 * ScreenSize().getHeight(context),
      child: Column(
        children: [
          HorizontalSpace().create(context, 0.008),
          Row(
            children: [
              AutoSizeText(item,style: GoogleFonts.inter(fontSize : 14.nsp,color: OwnColors.black,fontWeight: FontWeight.w500),),
              Spacer(),
              Theme(
                data: ThemeData(
                  //here change to your color
                  unselectedWidgetColor: OwnColors.CC8C8C8,
                ),
                child: Radio(
                    visualDensity: const VisualDensity(
                        horizontal: VisualDensity.minimumDensity,
                        vertical: VisualDensity.minimumDensity),
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    value: item,
                    groupValue: selected,
                    activeColor: OwnColors.CTourOrange,
                    // focusColor: OwnColors.black,
                    onChanged: (T){
                      update(T);
                    }
                ),
              )
            ],
          ),
          HorizontalSpace().create(context, 0.008),
        ],

      ),
    );
  }
  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      if(isClean){
        print("isClean");
        Navigator.of(context).pop(2);
      }else{
        Navigator.of(context).pop(0);
      }
    } else {
      SystemNavigator.pop();
    }
  }


  void updateOrder(String item ){
    print(item);
    setState(() {
      if(item == S().home_filter_order_method1){
        FilterData.travel_sort_type = 0;
      }else if(item == S().home_filter_order_method2){
        FilterData.travel_sort_type = 1;
      }else if(item == S().home_filter_order_method4){
        FilterData.travel_sort_type = 2;
      }
      orderMethod = item;
    });
  }

  void updateDate(String item ){
    print(item);
    if(item == S().home_filter_date1){
      FilterData.travel_date_choose = 0;
    }else if(item == S().home_filter_date2){
      FilterData.travel_date_choose = 1;
    }else if(item == S().home_filter_date3){
      FilterData.travel_date_choose = 2;
    }else if(item == S().home_filter_date4){
      FilterData.travel_date_choose = 3;
    }
    setState(() {
      dateChoosed = item;
    });
  }

  void updateType(String item ){
    print(item);
    if((item == S().home_filter_type2) || (item == S().home_filter_type)){
      FilterData.travel_isDay = true;
    }else{
      FilterData.travel_isDay = false;
    }
    if(item == S().home_filter_type1){
      FilterData.travel_type = 0;
    }else if(item == S().home_filter_type2){
      FilterData.travel_type = 1;
    }else if(item == S().home_filter_type3){
      FilterData.travel_type = 2;
    }else if(item == S().home_filter_type4){
      FilterData.travel_type = 3;
    }
    setState(() {
      typeChoosed = item;
      print("updateType : " + typeChoosed);

    });
  }

  void closeBtn() {
    Navigator.of(context).pop("false");
  }

  void filterClick(){
    String result_orderMethod = "0";

    if(orderMethod == S().home_filter_order_method1){
      result_orderMethod = "0";
    }else if(orderMethod == S().home_filter_order_method2){
      result_orderMethod = "1";
    }else{
      result_orderMethod = "2";
    }

    typeList = [S().home_filter_type1,S().home_filter_type2, S().home_filter_type3,S().home_filter_type4];
    String result_typeChoosed = "0";
    if(typeChoosed == S().home_filter_type1){
      result_typeChoosed = "0";
    }else if(typeChoosed == S().home_filter_type2){
      result_typeChoosed = "1";
    }else if(typeChoosed == S().home_filter_type3){
      result_typeChoosed = "2";
    }else{
      result_typeChoosed = "3";
    }
    Navigator.of(context).pop(1);
  }
  void cleanClick(){
    print("cleanCLick");
    setState(() {
      FilterData.travel_isDay = false;
      FilterData.travel_type = 0;
      FilterData.travel_date_choose = 0;
      FilterData.travel_sort_type = 0;

      orderMethod = S().home_filter_order_method1;
      dateChoosed = S().home_filter_date1;
      typeChoosed = S().home_filter_type1;
      // double duration = 3;
      duration = SfRangeValues(0.0, double.parse(endNum) );
      FilterData.travel_duration = duration;
      FilterData.travel_start_duration = double.parse(duration.start.toString() ) ;
      FilterData.travel_end_duration = double.parse(duration.end.toString() ) ;

      isClean = true;
    });
  }

  @override
  void dispose() {
    print("dispose");

    super.dispose();
  }

  @override
  void deactivate() {
    print("deactivate");

    super.deactivate();
  }
}
