import 'dart:async';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/Chat/ChattingPage.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Guild/GuildReviewPage.dart';
import 'package:ctour/Guild/GuilderTravelPage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/StatusTools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ionicons/ionicons.dart';
import 'package:line_icons/line_icons.dart';
import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/TopBar.dart';
import '../res.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_star/flutter_star.dart';

class PreviewGuilderPage extends StatefulWidget {

  final GuildClass guildClass;

  const PreviewGuilderPage(
      {Key key,
        this.guildClass,


      })
      : super(key: key);

  @override
  State createState() {
    return _PreviewGuilderPageState();
  }
}

class _PreviewGuilderPageState extends State<PreviewGuilderPage> {
  List<String> imageList = [
    "https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg",
    "https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg",
    "https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg",
  ];

  GuildClass guildClass;
  bool more  = true;
  double topShirkOffset = 0;
  double maxTop = 0.0;

  GlobalKey titleKey = new  GlobalKey();
  bool showTitle = false ;


  @override
  void initState() {
    super.initState();
    StatusTools().setTranBGWhiteText();
    guildClass = widget.guildClass;

    print("guildClass.want_companion " + guildClass.want_companion.toString());

    imageList.clear();
    // imageList.add(widget.guildClass.avatar);
    for (dynamic c in widget.guildClass.tourist_img){

      if(c.toString().contains("http")){
        imageList.add(c.toString());
      }
    }


    // guildClass = new GuildClass(id: "",
    // tourist_img: ["https://memeprod.ap-south-1.linodeobjects.com/user-template-thumbnail/d2254363d8a0d6a7b74a19b2da5902b4.jpg"],
    // name: "Ben",score: "5.0",location: ["台北","桃園"], trans:[0,2],lang: ["中文","英文","英文"],comment_num: "18"
    // ,about_me: "Hi~ 大家好 我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi我是Melissa歡迎大家來找我玩Hi~ 大家好 我是Melissa歡迎大家來找我玩    Hi~ 大家好 我是Melissa    歡迎大家來找我玩Hi~ 大家好 我是Melissa         ");
  }

  @override
  void dispose() {
    super.dispose();
    StatusTools().setWhitBGDarkText();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.tran,

        body: GestureDetector(
            child: Stack(
          children: [
            Container(
              width: ScreenSize().getWidth(context) * 1.0,
              color: OwnColors.white,
              height: MediaQuery.of(context).size.height,
              child: SingleChildScrollView(
                child: Container(
                  width: ScreenSize().getWidth(context) * 1.0,
                  height: MediaQuery.of(context).size.height,
                  child: Stack(
                    children: [
                      NotificationListener<ScrollUpdateNotification>(
                        onNotification: (notification) {

                          RenderBox titleBox = titleKey.currentContext.findRenderObject() as RenderBox;
                          double topH2 = 0.07 * ScreenSize().getHeight(context)  + MediaQuery.of(context).padding.top;
                          print ("titlebox " + titleBox.getTransformTo(null).getTranslation().y.toString());
                          if(titleBox.getTransformTo(null).getTranslation().y <= topH2) {
                            StatusTools().setWhitBGDarkText();
                            if(showTitle == false ){
                              setState(() {
                                showTitle = true;
                              });
                            }
                          }else {
                            StatusTools().setTranBGWhiteText();
                            if(showTitle == true ){
                              setState(() {
                                showTitle = false;
                              });
                            }
                          }


                        },
                        child: CustomScrollView(
                          physics: const BouncingScrollPhysics(),
                          slivers: [
                            SliverAppBar(
                                backgroundColor: OwnColors.white,
                                expandedHeight:0.5 * ScreenSize().getHeight(context),
                                shadowColor: OwnColors.white,
                                elevation: 0,
                                stretch: true,
                                bottom: PreferredSize(
                                  child: Container(
                                    color: Colors.orange,
                                  ),
                                  preferredSize: Size(0, 0),
                                ),
                                pinned: true,


                                flexibleSpace:                               LayoutBuilder(
                                    builder: (BuildContext context, BoxConstraints constraints) {
                                      if(maxTop == 0){
                                        maxTop = constraints.biggest.height;
                                      }
                                      topShirkOffset = constraints.biggest.height;
                                      // print (topShirkOffset /( MediaQuery.of(context).padding.top + kToolbarHeight));
                                      // print (constraints.biggest.height);
                                      // print (0.07 * ScreenSize().getHeight(context) + MediaQuery.of(context).padding.top  );

                                      // if(( topShirkOffset -(0.07 * ScreenSize().getHeight(context) + MediaQuery.of(context).padding.top  )) < 0.1){
                                      //   setState(() {
                                      //
                                      //   });
                                      // }
                                      // if((topShirkOffset  - (0.07 * ScreenSize().getHeight(context) + MediaQuery.of(context).padding.top  )) <0.1){
                                      //   StatusTools().setWhitBGDarkText();
                                      // }else {
                                      //   StatusTools().setTranBGWhiteText();
                                      // }

                                      return Stack(
                                        children: [
                                          FlexibleSpaceBar(

                                            background:Swiper(

                                              itemBuilder: (BuildContext context, int index) {
                                                return CachedNetworkImage(
                                                  imageUrl: imageList[index],
                                                  fit: BoxFit.fill,
                                                  placeholder: (context, url) => AspectRatio(
                                                    aspectRatio: 1,
                                                    child: FractionallySizedBox(
                                                      heightFactor: 0.5,
                                                      widthFactor: 0.5,
                                                      child: SpinKitRing(
                                                        lineWidth:5,
                                                        color:
                                                        OwnColors.tran ,
                                                      ),
                                                    ),
                                                  ),
                                                  errorWidget: (context, url, error) => Icon(
                                                    Icons.error,
                                                    color: OwnColors.black.withOpacity(0.5),
                                                  ),
                                                  imageBuilder: (context, imageProvider) =>
                                                      Container(
                                                        decoration: BoxDecoration(
                                                          color: OwnColors.tran,
                                                          image: DecorationImage(
                                                              image: imageProvider,
                                                              fit: BoxFit.cover),
                                                        ),
                                                      ),
                                                  // width: MediaQuery.of(context).size.width,
                                                );
                                              },
                                              itemCount: imageList.length,
                                              autoplay: false,
                                              loop: false,

                                              pagination: new SwiperPagination(
                                                  builder: new DotSwiperPaginationBuilder(
                                                    color: Colors.white,
                                                    activeColor: imageList.length == 1? OwnColors.tran:OwnColors.CTourOrange,
                                                    size: 8,
                                                    activeSize: 8,
                                                  ),
                                                  margin: EdgeInsets.only(
                                                      bottom: 0.05 *
                                                          ScreenSize()
                                                              .getHeight(context) +
                                                          10)),
                                              // control: new SwiperControl(),
                                            ) ,
                                          ),
                                          Positioned(
                                            child: Container(
                                              height: 0.05.sw  ,
                                              decoration: const BoxDecoration(
                                                color: Colors.white,
                                                borderRadius: BorderRadius.vertical(
                                                  top: Radius.circular(80),
                                                ),
                                              ),
                                            ),
                                            bottom: -3,
                                            left: 0,
                                            right: 0,
                                          )                 ,
                                          // Center(
                                          //   child: Padding(
                                          //     padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top,
                                          //         left: 0.04 * ScreenSize().getHeight(context)+ 0.05.sw,
                                          //         right: 0.04 * ScreenSize().getHeight(context) + 0.05.sw ),
                                          //     child: Text(
                                          //       widget.guildClass.name,
                                          //       key: titleKey,
                                          //       maxLines: 1,
                                          //       overflow: TextOverflow.ellipsis,
                                          //       style:TextStyle (
                                          //         fontFamily : 'NotoSansTC',
                                          //         fontSize: 20.nsp,
                                          //         color: (topShirkOffset  - (0.07 * ScreenSize().getHeight(context) + MediaQuery.of(context).padding.top  )) <0.1? OwnColors.black:OwnColors.tran,
                                          //         fontWeight: FontWeight.w700,
                                          //       ),
                                          //     ),
                                          //   ),
                                          //
                                          // )
// Positioned(

                                        ],
                                      );
                                    }
                                )

                            ),
                            SliverToBoxAdapter(

                              child: Column(
                                children: [

                                  Container(
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      color: OwnColors.white,

                                    ),
                                    child: Column(
                                      children: [
                                        Container(
                                          width: 0.88 *
                                              ScreenSize().getWidth(context),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Flexible(
                                                child: Text(
                                                  guildClass.name,
                                                  key: titleKey,
                                                  maxLines: 1,
                                                  overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                    fontSize: 25.nsp,
                                                    fontWeight: FontWeight.w600,
                                                    color: OwnColors.black,
                                                  ),
                                                ),
                                              ),
                                              // Padding(
                                              //   padding: const EdgeInsets.only(left:4.0,right: 4),
                                              //   child: Container(
                                              //       height: 0.024 *
                                              //           ScreenSize()
                                              //               .getHeight(context),
                                              //       child: Image.asset(Res.star)),
                                              // ),
                                              // Text(guildClass.score,
                                              //     style: TextStyle(
                                              //       fontSize: 20.nsp,
                                              //       color: OwnColors.black,
                                              //     )),
                                              // Padding(
                                              //   padding: const EdgeInsets.only(left:4.0,right: 4),
                                              //   child: Center(
                                              //     child: Text("("+guildClass.comment_num + S().home_rate_number + ")",
                                              //         textAlign: TextAlign.center,
                                              //         style: TextStyle(
                                              //           fontSize: 18.nsp,
                                              //           color: OwnColors.black,
                                              //           height: 1
                                              //         )),
                                              //   ),
                                              // ),
                                            ],
                                          ),
                                        ),
                                        HorizontalSpace().create(context, 0.008),
                                        Visibility(
                                          visible: widget.guildClass.want_companion == "1"?true:false,
                                          child: Container(
                                            width: 0.88*ScreenSize().getWidth(context),
                                            child: Row(

                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [

                                                Container(
                                                  height: 0.022* ScreenSize().getHeight(context),
                                                  width: 0.046 * ScreenSize().getWidth(context),
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(top:2.0),
                                                    child: Image.asset(Res.icon_companion),
                                                  ),

                                                ),
                                                Flexible(
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(left:7.0),
                                                    child: Text(
                                                      S().find_companion ,

                                                      style:GoogleFonts.inter(
                                                        fontSize: 18.nsp,
                                                        color: OwnColors.CFFAE4E,
                                                        fontWeight: FontWeight.w600,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(
                                          width: 0.88*ScreenSize().getWidth(context),
                                          child: Row(

                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [

                                              Container(
                                                height: 0.022* ScreenSize().getHeight(context),
                                                width: 0.046 * ScreenSize().getWidth(context),
                                                child: Padding(
                                                  padding: const EdgeInsets.only(top:2.0),
                                                  child: Icon(SFSymbols.globe, size: 20,),
                                                ),

                                              ),
                                              Flexible(
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left:7.0),
                                                  child: Text(
                                                    guildClass.lang.join(" | ") ,

                                                    style:GoogleFonts.inter(
                                                      fontSize: 18.nsp,
                                                      color: OwnColors.black,
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        Visibility(
                                          visible: !guildClass.trans.contains("3"),
                                          child: Container(
                                            width: 0.88*ScreenSize().getWidth(context),
                                            child: Row(

                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [

                                                Container(
                                                    height: 0.022* ScreenSize().getHeight(context),
                                                    width: 0.046 * ScreenSize().getWidth(context),
                                                    child: Icon(SFSymbols.car_fill, size: 19,)
                                                ),
                                                Flexible(
                                                  child: Padding(
                                                    padding: const EdgeInsets.only(left:8.0),
                                                    child: AutoSizeText(
                                                      !guildClass.trans.contains("3") ? S().home_have_car:S().home_have_no_car,

                                                      minFontSize: 1,
                                                      style:GoogleFonts.inter(
                                                        fontSize: 18.nsp,
                                                        color: OwnColors.black,
                                                        fontWeight: FontWeight.w500,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                        Container(height: 10,),
                                        Container(
                                          width: 0.88 * ScreenSize().getWidth(context),
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Wrap(

                                              spacing: 16,
                                              runSpacing : 5.0,
                                              children: List.generate(
                                                guildClass.location.length,
                                                    (i) {
                                                  return Container(

                                                      decoration: BoxDecoration(
                                                          color: OwnColors.white,
                                                          borderRadius: BorderRadius.only(
                                                              topLeft: Radius.circular(13),
                                                              topRight: Radius.circular(13),
                                                              bottomLeft: Radius.circular(13),
                                                              bottomRight: Radius.circular(13)),
                                                          border: Border.all(
                                                              width: 1,color: OwnColors.CF4F4F4
                                                          ),
                                                          boxShadow: [
                                                            BoxShadow(
                                                              color: OwnColors.primaryText.withOpacity(0.4),
                                                              spreadRadius: 1,
                                                              blurRadius: 4,
                                                              offset: Offset(0, 3), // changes position of shadow
                                                            ),
                                                          ]),
                                                      child: Padding(
                                                        padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                                        child: Text(guildClass.location[i],
                                                          textAlign: TextAlign.center,
                                                          style:GoogleFonts.inter(
                                                            height: 1.2,
                                                            fontSize: 14.nsp,
                                                            color: OwnColors.black,
                                                            fontWeight: FontWeight.w300,
                                                          ),
                                                        ),
                                                      ));
                                                },
                                              ),
                                            ),
                                          ),
                                        ),
                                        HorizontalSpace().create(context, 0.027),

                                        Container(
                                          color: OwnColors.CF4F4F4,
                                          height: 1,
                                          width: 0.92 * ScreenSize().getWidth(context),
                                        ),
                                        HorizontalSpace().create(context, 0.027),
                                        Container(
                                          width: 0.84 *
                                              ScreenSize().getWidth(context),

                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [

                                                    // Align(
                                                    //   alignment: Alignment.centerLeft,
                                                    //   child: AutoSizeText(
                                                    //     S().guilder_abountme,
                                                    //     minFontSize: 1,
                                                    //     style: TextStyle(
                                                    //         fontSize: 18.nsp,
                                                    //         color: OwnColors.black,
                                                    //         fontWeight: FontWeight.w600
                                                    //
                                                    //     ),
                                                    //   ),
                                                    // ),
                                                    Padding(
                                                      padding: const EdgeInsets.only(left:8.0),
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.end,
                                                        crossAxisAlignment: CrossAxisAlignment.end,
                                                        children: [
                                                          Expanded(
                                                              child:   Container(


                                                                child: Text(
                                                                  guildClass.description,
                                                                  overflow: TextOverflow.ellipsis,

                                                                  maxLines:!more ? 5:100000,
                                                                  style: TextStyle(
                                                                      fontSize: 16.nsp,
                                                                      height: 1.3,
                                                                      color: OwnColors.CC8C8C8,
                                                                      fontWeight: FontWeight.w600

                                                                  ),
                                                                ),
                                                              )

                                                          ),

                                                          Visibility(
                                                            visible: !more,
                                                            child: Padding(
                                                              padding: const EdgeInsets.only(left:2.0),
                                                              child: GestureDetector(
                                                                onTap: (){
                                                                  more = !more;
                                                                  setState(() {

                                                                  });
                                                                },
                                                                child: Text(
                                                                  S().guilder_more,
                                                                  style: TextStyle(
                                                                      fontSize: 18.nsp,
                                                                      fontWeight: FontWeight.w600
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          Visibility(
                                                            child:
                                                            VerticalSpace().create(context, 0.08),
                                                          )
                                                        ],
                                                      ),
                                                    ) ,
                                                  ],
                                                ),
                                              ),
                                              Opacity(
                                                opacity: 0,
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    HorizontalSpace().create(context, 0.03),
                                                    GestureDetector(
                                                      onTap:(){
                                                        // goMess();
                                                      },
                                                      child: Column(
                                                        children: [
                                                          Container(height: 0.035.sh,child: Image.asset(Res.guilder_mes)),
                                                          Text(
                                                            S().guilder_mess,
                                                            style: TextStyle(
                                                              fontSize: 15.nsp,
                                                            ),
                                                          )
                                                        ],),
                                                    ),
                                                    HorizontalSpace().create(context, 0.01),
                                                    GestureDetector(
                                                      onTap:(){
                                                        // goComment();
                                                      },
                                                      child: Column(
                                                        children: [
                                                          Container(height: 0.035.sh,child: Image.asset(Res.guilder_comment)),
                                                          Text(
                                                            S().guilder_comment,
                                                            style: TextStyle(
                                                              fontSize: 15.nsp,
                                                            ),
                                                          )
                                                        ],),
                                                    )

                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        HorizontalSpace().create(context, 0.027),
                                        HorizontalSpace().create(context, 0.027),
                                        Container(
                                          color: OwnColors.CF4F4F4,
                                          height: 1,
                                          width: 0.92 * ScreenSize().getWidth(context),
                                        ),
                                        HorizontalSpace().create(context, 0.024),
                                        // Container(
                                        //   height :0.054.sh,
                                        //   width: 0.315.sw,
                                        //   child: RaisedButton(
                                        //     color: OwnColors.CTourOrange,
                                        //     splashColor: OwnColors.inkColor,
                                        //     textColor: OwnColors.white,
                                        //     // elevation: 10,
                                        //     child: Padding(
                                        //       padding: const EdgeInsets.all(8.0),
                                        //       child: AutoSizeText(
                                        //         S().guilder_travel,
                                        //         minFontSize: 10,
                                        //         maxFontSize: 30,
                                        //         style: TextStyle(
                                        //           color: OwnColors.white,
                                        //           fontWeight: FontWeight.bold,
                                        //           fontSize: 30,
                                        //         ),
                                        //         maxLines: 1,
                                        //       ),
                                        //     ),
                                        //     shape: RoundedRectangleBorder(
                                        //       borderRadius: BorderRadius.circular(20.0),
                                        //       // side: BorderSide(color: Colors.red)
                                        //     ),
                                        //     onPressed: goTravel,
                                        //   ),
                                        // ),
                                        HorizontalSpace().create(context, 0.024),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),

                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
              child: Container(
                height: 0.07 * ScreenSize().getHeight(context),
                child: Center(
                  child: Container(
                    width: 0.92 * ScreenSize().getWidth(context),
                    child: Row(
                      children: [
                        ClipOval(
                          child: Material(
                            color: Colors.white, // button color
                            child: GestureDetector(
                              // inkwell color
                              child: SizedBox(
                                  width: 0.04 * ScreenSize().getHeight(context),
                                  height:
                                      0.04 * ScreenSize().getHeight(context),
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 0.0,left: 0.0,right: 0.0),
                                    // child: Icon(Icons.chevron_left_rounded, color: OwnColors.black,size:  0.04.sh,),
                                    // child: Center(child: Icon(Ionicons.chevron_back_outline, color: OwnColors.black,size:  0.03.sh,)),
                                    child: Center(child: Icon(LineIcons.angleLeft, color: OwnColors.black,size:  0.03.sh,)),

                                  )),
                              onTap: () {
                                backClick();
                              },
                            ),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(left:8.0,right: 8),
                            child: Text(
                              widget.guildClass.name,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: GoogleFonts.inter(
                                color: showTitle? OwnColors.black:
                                OwnColors.tran,
                                fontSize: 20.nsp,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        )
                        // ClipOval(
                        //   child: Material(
                        //     color: Colors.white, // button color
                        //     child: InkWell(
                        //       splashColor: OwnColors.inkColor,
                        //       // inkwell color
                        //       child: SizedBox(
                        //           width: 0.04 * ScreenSize().getHeight(context),
                        //           height:
                        //               0.04 * ScreenSize().getHeight(context),
                        //           child: Padding(
                        //             padding: const EdgeInsets.all(4.0),
                        //             child: Image.asset(Res.share_black),
                        //           )),
                        //       onTap: () {},
                        //     ),
                        //   ),
                        // ),
                        ,
                        Container(
                          width: 10,
                        ),
                        // ClipOval(
                        //   child: Material(
                        //     color: Colors.white, // button color
                        //     child: InkWell(
                        //       splashColor: OwnColors.inkColor,
                        //       // inkwell color
                        //       child: SizedBox(
                        //           width: 0.04 * ScreenSize().getHeight(context),
                        //           height:
                        //               0.04 * ScreenSize().getHeight(context),
                        //           child: Padding(
                        //             padding: const EdgeInsets.all(4.0),
                        //             //child: Image.asset(Res.collected_heart),
                        //             child: guildClass.is_collect.contains("1")? Image.asset(Res.collected_heart): Image.asset(Res.black_heart),
                        //           )),
                        //       onTap: () {
                        //         sendFavorite(guildClass);
                        //       },
                        //     ),
                        //   ),
                        // )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            // Positioned(
            //   right: 0.08.sw,
            //   bottom: 0.15.sh,
            //   child: Column(
            //     mainAxisAlignment: MainAxisAlignment.start,
            //     crossAxisAlignment: CrossAxisAlignment.start,
            //     children: [
            //       HorizontalSpace().create(context, 0.03),
            //       InkWell(
            //         onTap:(){
            //           goMess();
            //         },
            //         child: Column(
            //           children: [
            //             Container(height: 0.035.sh,child: Image.asset(Res.guilder_mes)),
            //             Text(
            //               S().guilder_mess,
            //               style: TextStyle(
            //                 fontSize: 15.nsp,
            //               ),
            //             )
            //           ],),
            //       ),
            //       HorizontalSpace().create(context, 0.01),
            //       InkWell(
            //         onTap:(){
            //           goComment();
            //         },
            //         child: Column(
            //           children: [
            //             Container(height: 0.035.sh,child: Image.asset(Res.guilder_comment)),
            //             Text(
            //               S().guilder_comment,
            //               style: TextStyle(
            //                 fontSize: 15.nsp,
            //               ),
            //             )
            //           ],),
            //       )
            //
            //     ],
            //   ),
            // )
          ],
        )));
  }

  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }

  void goMess(){
    PushNewScreen().normalPush(context, ChattingPage(chat_title: widget.guildClass.name,chat_avatar: widget.guildClass.avatar,send_to_id: widget.guildClass.id,send_to_fcm: "",));
  }
  void goTravel(){
    PushNewScreen().normalPush(context, GuilderTravelPage(guildClass:guildClass));
  }
  void goComment(){
    PushNewScreen().normalPush(context, GuildReviewPage());
  }

  void sendFavorite(GuildClass guildClass) async {

    ResultData resultData = await AppApi.getInstance().setCollect(context, true,  SharePre.prefs.getString(GDefine.user_id),guildClass.id);
    if (resultData.isSuccess()) {

    } else {
      WarningDialog.showIOSAlertDialog(context, S().set_favorite_fail, resultData.msg,S().confirm);
    }
  }

}
