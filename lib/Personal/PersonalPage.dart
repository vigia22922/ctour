import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/Chat/ChattingPage.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Firebase/FirebaseRealtime.dart';
import 'package:ctour/Chat/ChatClass.dart';
import 'package:ctour/Login/LoginChoosePage.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:open_mail_app/open_mail_app.dart';
import 'package:rate_my_app/rate_my_app.dart';
import 'BecomeGuilderPage.dart';
import 'package:ctour/Login/PrivacyPage.dart';
import 'package:ctour/Personal/CollectionPage.dart';
import 'package:ctour/Personal/EditPersonalPage.dart';
import 'package:ctour/Personal/MyTravelPage.dart';

import 'package:ctour/SQLite/Database.dart';
import 'dart:math' as math;
import 'package:ctour/SQLite/model/ChatModel.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/res.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';

import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../Login/ServiceTermsPage.dart';

class PersonalPage extends StatefulWidget {
  Function goNews;

  PersonalPage({Key key, this.goNews}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PersonalPageState();
  }
}

class PersonalPageState extends State<PersonalPage> {
  final String TAG = "ChatPageState";
  TextEditingController searchEdit;
  List<ChatClass> itemList = new List();

  int tabState = 0;
  RefreshController refreshController =
      new RefreshController(initialRefresh: false);

  bool go2ChooseMode = false;
  List<int> _selectedItems = List<int>();
  bool selectAll = false;

  String searchText1 = "";

  String avatar = "";
  String avatarFileName = "";

  @override
  void initState() {
    super.initState();


    avatar = SharePre.prefs.getString(GDefine.avatar).toString();


  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print(avatar);
    return normal();
  }

  Widget normal() {
    return FocusDetector(
      onFocusGained: () {
        // setItemList();
        setState(() {

        });
      },
      child: Scaffold(
        backgroundColor: OwnColors.white,
        body: SafeArea(
          child: Container(
              height: ScreenSize().getHeight(context) * 0.9,
              width: ScreenSize().getWidth(context),
              child: Column(
                children: [
                  HorizontalSpace().create(context, 0.04),
                  Container(
                    height: ScreenSize().getHeight(context) * 0.088,
                    width: ScreenSize().getWidth(context) * 0.8,
                    child: Row(
                      children: [
                        AspectRatio(
                          aspectRatio: 0.85,
                          child: Material(
                            color: Colors.transparent, // button color
                            child: Container(
                              height: ScreenSize().getHeight(context) * 0.05,
                              child: avatarFileName.length > 1
                                  ? AspectRatio(
                                      aspectRatio: 1,
                                      child: Container(
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            border: Border.all(
                                                color: OwnColors.CTourOrange.withOpacity(0.5),
                                                width: 0),
                                            boxShadow: [
                                              BoxShadow(
                                                color: OwnColors.black
                                                    .withOpacity(0.1),
                                                spreadRadius: 1,
                                                blurRadius: 10,
                                                offset: Offset(0, 2),
                                              )
                                            ]),
                                        child: ClipOval(
                                          child: Material(
                                            child: Image.file(
                                                File(avatar),
                                                fit: BoxFit.cover),
                                          ),
                                        ),
                                      ),
                                    )
                                  : avatar.length > 5 && !avatar.contains("AdminLTE")
                                      ?

                              Column(

                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  CachedNetworkImage(
                                              errorWidget:
                                                  (context, url, error) =>
                                                      Icon(
                                                Icons.error,
                                                color: Colors.red,
                                              ),
                                              imageUrl: avatar,
                                              fit: BoxFit.fitWidth,
                                              imageBuilder:
                                                  (context, imageProvider) =>
                                                      AspectRatio(
                                                        aspectRatio: 1,
                                                        child: Container(

                                                decoration: BoxDecoration(
                                                  color: OwnColors.green,
                                                  shape: BoxShape.circle,
                                                  image: DecorationImage(
                                                        image: imageProvider,
                                                        fit: BoxFit.cover),
                                                ),
                                              ),
                                                      ),
                                      placeholder: (context, url) => AspectRatio(
                                        aspectRatio: 1,
                                        child: FractionallySizedBox(
                                          heightFactor: 0.5,
                                          widthFactor: 0.5,
                                          child: SpinKitRing(
                                            lineWidth:5,
                                            color:
                                            OwnColors.tran ,
                                          ),
                                        ),
                                      )
                                            ),
                                ],
                              )
                                      : Image.asset(Res.choose_img),
                            ),
                          ),
                        ),
                        VerticalSpace().create(context, 0.023),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(padding: const EdgeInsets.only(bottom:10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                        height: ScreenSize().getHeight(context) * 0.018,
                                        child: SharePre.prefs.getString(GDefine.isGuilder).contains("false")?Image.asset(Res.bi_check):Image.asset(Res.bi_org)),
                                    Align(
                                      alignment: Alignment.center,
                                      child: Padding(
                                        padding:
                                        const EdgeInsets.only(left: 4),
                                        child: Text(
                                          SharePre.getString(GDefine.name).toString(),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style:GoogleFonts.inter(
                                            fontSize: 20.nsp,
                                            color: OwnColors.black,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                    ),
                                    // Expanded(
                                    //   child: Padding(
                                    //     padding:
                                    //     const EdgeInsets.only(left: 4.0),
                                    //     child: Text(
                                    //       SharePre.getString(GDefine.name).toString(),
                                    //       maxLines: 1,
                                    //       overflow: TextOverflow.ellipsis,
                                    //       style:GoogleFonts.inter(
                                    //         fontSize: 20.nsp,
                                    //         color: OwnColors.black,
                                    //         fontWeight: FontWeight.w600,
                                    //       ),
                                    //     ),
                                    //   ),
                                    // )
                                  ],
                                ),
                              ),
                              Row(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      becomeClick();
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(17)),
                                          border: Border.all(
                                              width: 1,
                                              color: OwnColors.black)),
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            top: 4.0,
                                            bottom: 4.0,
                                            left: 12.0,
                                            right: 12.0),
                                        child: Center(
                                          child: AutoSizeText(
                                            S().personal_edit,
                                            maxLines: 1,
                                            style:GoogleFonts.inter(
                                              fontSize: 12.nsp,
                                              color: OwnColors.black,
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  HorizontalSpace().create(context, 0.03),
                  Container(
                    height: ScreenSize().getHeight(context) * 0.045,
                    width: ScreenSize().getWidth(context) * 0.88,
                    child: FlatButton(
                      color: OwnColors.CTourOrange,
                      splashColor: Colors.transparent,
                      highlightColor: Colors.transparent,
                      textColor: OwnColors.white,
                      // elevation: 10,
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: AutoSizeText(
                          S().personal_my_travel,
                          minFontSize: 10,
                          maxFontSize: 30,
                          style:GoogleFonts.inter(
                            fontSize: 16.nsp,
                            color: OwnColors.white,
                            fontWeight: FontWeight.w300,
                          ),
                          maxLines: 1,
                        ),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                        // side: BorderSide(color: Colors.red)
                      ),
                      onPressed: goMyTravel,
                    ),
                  ),
                  HorizontalSpace().create(context, 0.035),
                  Container(
                    height: 1,
                    width: ScreenSize().getWidth(context),
                    color: OwnColors.CF4F4F4,
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [

                          HorizontalSpace().create(context, 0.025),
                          item(Icon(SFSymbols.heart), S().personal_title1, collectClick),
                          // item(Icon(SFSymbols.person_crop_circle_badge_checkmark), S().personal_title2, becomeClick),
                          item2(Icon(SFSymbols.bell), S().personal_title3, notifyClick),
                          item(Icon(SFSymbols.star_circle), S().personal_title4, rateClick),

                          item(Icon(SFSymbols.envelope), S().personal_title5, mailClick),
                          item(Icon(SFSymbols.doc_text), S().personal_title6, ruleClick),
                          item(Icon(SFSymbols.doc_text), S().personal_title7, privacyClick),
                          // item(Res.document, S().personal_title8, deleteClick),

                          HorizontalSpace().create(context, 0.1),


                          GestureDetector(
                            onTap: (){
                              logout();
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(width: 1.0, color:OwnColors.black),

                                ),
                                color: Colors.white,
                              ),

                              child: AutoSizeText(
                                S().personal_logout,
                                maxFontSize: 20,
                                style:GoogleFonts.inter(
                                    fontSize: 16.nsp,
                                    color: OwnColors.black,
                                    fontWeight: FontWeight.w600,

                                ),
                              ),
                            ),
                          ),
                          HorizontalSpace().create(context, 0.08),
                        ],
                      ),
                    ),
                  ),

                ],
              )
          ),
        ),
      ),
    );
  }

  Widget item(Icon icon, String title, Function function) {
    return Stack(alignment: Alignment.center, children: [
      Container(
        height: 0.06 * ScreenSize().getHeight(context),
        width: ScreenSize().getWidth(context) * 1,
        child: Center(
          child: Container(
            width: ScreenSize().getWidth(context) * 0.88,
            child: Column(
              children: [
                Spacer(
                  flex: 1,
                ),
                Expanded(
                  flex: 4,
                  child: Row(
                    children: [
                      icon,
                      // VerticalSpace().create(context, 0.0256),
                      VerticalSpace().create(context, 0.04),
                      AutoSizeText(
                        title,
                        maxFontSize: 20,
                        style:GoogleFonts.inter(
                          fontSize: 16.nsp,
                          color: OwnColors.black,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Spacer(),
                      Image.asset(Res.right_arrow),
                    ],
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
              ],
            ),
          ),
        ),
      ),
      new Positioned.fill(
          child: new Material(
        color: Colors.transparent,
        child: new GestureDetector(

          onTap: () => {function()},
        ),
      )),
    ]);
  }

  Widget item2(Icon icon, String title, Function function) {
    return Stack(alignment: Alignment.center, children: [
      Container(
        height: 0.06 * ScreenSize().getHeight(context),
        width: ScreenSize().getWidth(context) * 1,
        child: Center(
          child: Container(
            width: ScreenSize().getWidth(context) * 0.88,
            child: Column(
              children: [
                Spacer(
                  flex: 1,
                ),
                Expanded(
                  flex: 4,
                  child: Row(
                    children: [
                      icon,
                      VerticalSpace().create(context, 0.04),

                      AutoSizeText(
                        title,
                        maxFontSize: 20,
                        style:GoogleFonts.inter(
                          fontSize: 16.nsp,
                          color: OwnColors.black,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Spacer(),
                      CupertinoSwitch(

                        value: SharePre.getString(GDefine.is_fcm_open) == "1",
                        activeColor: OwnColors.CFF644E,
                        onChanged: (bool value) {
                          // setState(() {
                          //   _lights = value;
                          // });

                        },
                      ),
                    ],
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
              ],
            ),
          ),
        ),
      ),
      new Positioned.fill(
          child: new Material(
        color: Colors.transparent,
        child: new GestureDetector(
          onTap: () => {function()},
        ),
      )),
    ]);
  }

  @override
  void dispose() {
    super.dispose();
    print("dispose");
  }

  void editName() {
    showCupertinoModalBottomSheet(
      barrierColor:Colors.black54,
      topRadius: Radius.circular(30),
      expand: false,
      context: context,
      backgroundColor: Colors.transparent,
      enableDrag : true,
      builder: (context) => EditPersonalPage( ),

    ).whenComplete(() {

      // subjectEdit.text = "";
      // subjectEdit2.text = "";
      // List<String> s = new List();
      // List<String> s2 = new List();
      // for (int a in GDefine.subjectIndexList){
      //   s.add(subjectList[a].trans);
      //   s2.add(subjectList[a].param_value);
      //
      // }
      // subjectEdit.text = s.join(",");
      // subjectEdit2.text = s2.join(",");
      // print(subjectEdit2.text );

      avatar = SharePre.prefs.getString(GDefine.avatar).toString();
      setState(() {});


    });

  }

  void collectClick() {
    PushNewScreen().normalPush(context, CollectionPage());
  }

  void becomeClick() {
    PushNewScreen().normalPushThen(context, BecomeGuilderPage(),refreshAvatar);

  }
  void refreshAvatar(){
  avatar = SharePre.prefs.getString(GDefine.avatar).toString();
  setState(() {

  });

}
  RateMyApp rateMyApp = RateMyApp(
    preferencesPrefix: 'rateMyApp_',
    minDays: 0,
    minLaunches: 0,
    remindDays: 0,
    remindLaunches: 0,
    googlePlayIdentifier: 'com.punchih.ctour',
    appStoreIdentifier: '1635886410',
  );
  void rateClick() {
    print("rateClick");
    rateMyApp.reset();

    rateMyApp.init().then((_) {
      print("then");
      print("rateMyApp.shouldOpenDialog " + rateMyApp.shouldOpenDialog.toString());
      // if (rateMyApp.shouldOpenDialog) {
      //
      //
      //   rateMyApp.showRateDialog(
      //     context,
      //     title: S().rate_title,
      //     // The dialog title.
      //     message: S().rate_hint,
      //     // The dialog message.
      //     rateButton: S().rate_btn,
      //     // The dialog "rate" button text.
      //     noButton: S().rate_btn_3,
      //     // The dialog "no" button text.
      //     laterButton: S().rate_btn_2,
      //     // The dialog "later" button text.
      //     listener: (
      //         button) { // The button click listener (useful if you want to cancel the click event).
      //       switch (button) {
      //         case RateMyAppDialogButton.rate:
      //           print('Clicked on "Rate".');
      //           break;
      //         case RateMyAppDialogButton.later:
      //           print('Clicked on "Later".');
      //           break;
      //         case RateMyAppDialogButton.no:
      //           print('Clicked on "No".');
      //           break;
      //       }
      //
      //       return true; // Return false if you want to cancel the click event.
      //     },
      //     ignoreNativeDialog: Platform.isAndroid,
      //     // Set to false if you want to show the Apple's native app rating dialog on iOS or Google's native app rating dialog (depends on the current Platform).
      //     dialogStyle: const DialogStyle(),
      //     // Custom dialog styles.
      //     onDismissed: () =>
      //         rateMyApp.callEvent(RateMyAppEventType
      //             .laterButtonPressed), // Called when the user dismissed the dialog (either by taping outside or by pressing the "back" button).
      //     // contentBuilder: (context, defaultContent) => content, // This one allows you to change the default dialog content.
      //     // actionsBuilder: (context) => [], // This one allows you to use your own buttons.
      //   );
      // }else {
      //
      // }

      rateMyApp.launchStore().then((result) {
        print('Result $result');
      });

        // Or if you prefer to show a star rating bar (powered by `flutter_rating_bar`) :

        // rateMyApp.showStarRateDialog(
        //   context,
        //   title: 'Rate this app', // The dialog title.
        //   message: 'You like this app ? Then take a little bit of your time to leave a rating :', // The dialog message.
        //   // contentBuilder: (context, defaultContent) => content, // This one allows you to change the default dialog content.
        //   actionsBuilder: (context, stars) { // Triggered when the user updates the star rating.
        //     return [ // Return a list of actions (that will be shown at the bottom of the dialog).
        //       FlatButton(
        //         child: Text('OK'),
        //         onPressed: () async {
        //           print('Thanks for the ' + (stars == null ? '0' : stars.round().toString()) + ' star(s) !');
        //           // You can handle the result as you want (for instance if the user puts 1 star then open your contact page, if he puts more then open the store page, etc...).
        //           // This allows to mimic the behavior of the default "Rate" button. See "Advanced > Broadcasting events" for more information :
        //           await rateMyApp.callEvent(RateMyAppEventType.rateButtonPressed);
        //           Navigator.pop<RateMyAppDialogButton>(context, RateMyAppDialogButton.rate);
        //         },
        //       ),
        //     ];
        //   },
        //   ignoreNativeDialog: Platform.isAndroid, // Set to false if you want to show the Apple's native app rating dialog on iOS or Google's native app rating dialog (depends on the current Platform).
        //   dialogStyle: const DialogStyle( // Custom dialog styles.
        //     titleAlign: TextAlign.center,
        //     messageAlign: TextAlign.center,
        //     messagePadding: EdgeInsets.only(bottom: 20),
        //   ),
        //   starRatingOptions: const StarRatingOptions(), // Custom star bar rating options.
        //   onDismissed: () => rateMyApp.callEvent(RateMyAppEventType.laterButtonPressed), // Called when the user dismissed the dialog (either by taping outside or by pressing the "back" button).
        // );
      // }
    });
  }

  void notifyClick() {
    print("notifyClick");

    if(SharePre.prefs.getString(GDefine.is_fcm_open) == "1"){
      SharePre.prefs.setString(GDefine.is_fcm_open,"0");
    }else {
      SharePre.prefs.setString(GDefine.is_fcm_open,"1");
    }

    String v =  SharePre.prefs.getString(GDefine.is_fcm_open);
    print("onChange");
    setFCM(v);

    setState(() {

    });
  }


  void setFCM(String v) async {
    print("setFCM");

    ResultData resultData = await AppApi.getInstance().setFCMOpen(context, true,  v);
    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data.toString());

    } else {


    }
  }


  void mailClick() async{
    EmailContent email = EmailContent(
      to: ['ctourtw@gmail.com',],
      subject:S().personal_mail_title,
      body: S().personal_mail_hint,
      cc: [],
      bcc: [],
    );

    OpenMailAppResult result = await OpenMailApp.composeNewEmailInMailApp(
        nativePickerTitle: 'Select email app to compose',
        emailContent: email);
    if (!result.didOpen && !result.canOpen) {
      WarningDialog.showIOSAlertDialog(context, S().personal_no_email_title, S().personal_no_email_app,S().confirm);
    } else if (!result.didOpen && result.canOpen) {
      showDialog(
        context: context,
        builder: (_) => MailAppPickerDialog(
          mailApps: result.options,
          emailContent: email,
        ),
      );
    }
  }

  void ruleClick() {
    PushNewScreen().normalPush(context, ServiceTermsPage());

  }

  void privacyClick() {

    PushNewScreen().normalPush(context, PrivacyPage());
  }

  void deleteClick() {}

  void logout() {
    WarningDialog.showIOSAlertDialog2(context,S().remind, S().personal_logout_content,S().personal_logout,sure2logout);

    // FirebaseAuth auth = FirebaseAuth.instance;

    // print(FirebaseAuth.instance.currentUser.toString());
  }

  Future<void> sure2logout() async {
    SharePre.prefs.setInt(GDefine.isLogin, 0);
    if(await FirebaseAuth.instance.currentUser != null){
      await FirebaseAuth.instance.signOut();
    }
    PushNewScreen().popAllAndPush(context, LoginChoosePage());
  }

  void goMyTravel() {
    print(SharePre.prefs.getString(GDefine.isGuilder) );
    if(SharePre.prefs.getString(GDefine.isGuilder).contains("false")){
      WarningDialog.showIOSAlertDialog(context, S().remind, S().not_tourist,S().confirm);
    }else{
      PushNewScreen().normalPush(context, MyTravelPage());
    }
  }

  void showBottomMenu() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 150,
              child: Column(children: <Widget>[
                ListTile(
                  leading: Icon(Icons.photo_camera),
                  title: Text(S().take_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromCamera();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.photo_library),
                  title: Text(S().choose_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromPhoto();
                  },
                )
              ]));
        });
  }

  final picker = ImagePicker();

  void pcikImageFromCamera() async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 720,
        maxWidth: 720,
        imageQuality: 50);

    File img = File(pickedFile.path);
    // img22.Image capturedImage =
    // img22.decodeImage(await File(pickedFile.path).readAsBytes());
    // img22.Image orientedImage = img22.bakeOrientation(capturedImage);
    // img = await File(pickedFile.path)
    //     .writeAsBytes(img22.encodeJpg(orientedImage));
    avatar = img.path;
    avatarFileName = avatar.split("/").last;

    setState(() {});
  }

  void pcikImageFromPhoto() async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 720,
        maxWidth: 720,
        imageQuality: 50);
    File img = File(pickedFile.path);
    List<int> imageBytes = await img.readAsBytes();
    avatar = img.path;
    avatarFileName = avatar.split("/").last;
    setState(() {});
  }



}
