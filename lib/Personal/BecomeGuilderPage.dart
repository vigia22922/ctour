import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/OrangeXCross.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Guild/GuildReviewPage.dart';
import 'package:ctour/Guild/GuilderPage.dart';
import 'package:ctour/Guild/GuilderTravelPage.dart';
import 'package:ctour/Login/LoginChoosePage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/Personal/PreviewGuilderPage.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/CropSample.dart';
import 'package:ctour/tool/CropTool.dart';
import 'package:ctour/tool/Loading.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:group_button/group_button.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:just_the_tooltip/just_the_tooltip.dart';
import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/TopBar.dart';
import '../res.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_star/flutter_star.dart';
import 'dart:math' as math; // import this

class BecomeGuilderPage extends StatefulWidget {
  @override
  State createState() {
    return _BecomeGuilderPageState();
  }
}

class _BecomeGuilderPageState extends State<BecomeGuilderPage> {
  List<String> imgList = [
    SharePre.prefs.getString(GDefine.avatar).contains("choose_img.png") ? GDefine.guilderDefault : SharePre.prefs.getString(GDefine.avatar),
    "",
    "",
    "",
    "",
    ""
  ];

  List<String> lansList = ["中文","English","日本語","한국어","Français","Deutsch","Bahasa Indonesia"];
  List<bool> lansSelList = [false,false,false,false,false,false,false];
  List<String> transList = [S().personal_become_guilder_trans1,S().personal_become_guilder_trans2,S().personal_become_guilder_trans3];
  List<bool> transSelList = [false,false,false];

  List<String> locationList = [ ];
  int currentIndexPage = 0;
  TextEditingController locationEdit = new TextEditingController();
  TextEditingController aboutMeEdit = new TextEditingController();
  TextEditingController nameEdit = new TextEditingController();
  TextEditingController inviteEdit = new TextEditingController();
  bool genderSwitchState = false; //左邊

  Color switch1Color = OwnColors.CTourOrange;
  Color switch2Color = OwnColors.C989898 ;

  bool becomeBool = false;
  bool transBool = false;
  GroupButtonController _checkboxesController;
  GroupButtonController lansController;
  bool showInfo = false;
  final tooltipController = JustTheController();
  final tooltipController2 = JustTheController();

  String avatar = "";
  String avatarUid = "";
  String avatarPath = "";
  String avatarFileName = "";
  String avatar_url="";

  int imgIndex = 0;
  int locationIndex = 0;

  bool imgChange = false ;
  bool nameChange = false ;
  bool locationChange = false ;
  bool aboutChange = false ;
  bool languageChange = false ;
  bool genderChange = false ;
  bool tranChange = false ;
  bool inviteChange = false ;
  // bool profileVisible = false;
  bool wantCompanion = false;



  @override
  void initState() {
    super.initState();
    // if (Platform.isAndroid) {
    SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(
        statusBarColor: OwnColors.tran, //设置为透明
        statusBarIconBrightness: Brightness.dark);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
    // }
    _checkboxesController = GroupButtonController(

      // onDisablePressed: (index) => ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(content: Text('${_checkboxButtons[index]} is disabled')),
      // ),
    );    lansController = GroupButtonController(

      // onDisablePressed: (index) => ScaffoldMessenger.of(context).showSnackBar(
      //   SnackBar(content: Text('${_checkboxButtons[index]} is disabled')),
      // ),
    );

    becomeBool = SharePre.getString(GDefine.isGuilder) != "true";

    List<String> imgTemp =  SharePre.getString(GDefine.tourist_img).toString().split(",") ;
    imgTemp.removeWhere((element) => element == "null");
    // imgTemp.removeWhere((ele) ele == "null");
    print(" SharePre.getString(GDefine.tourist_img).toString() " +  SharePre.getString(GDefine.tourist_img).toString());
    for (int i = 0 ; i<imgTemp.length; i++){
      if(!imgTemp[i].contains("choose_img.png")) {
        imgList[i + 1] = imgTemp[i];
      }
    }

    print(" imgList " +  imgList.toString());

    List<String> lang = SharePre.getString(GDefine.lang).toString().split(",") ;
    print(lang);
    for (int i = 0 ; i<lang.length; i++){
      int idex = lansList.indexWhere((element) => element == lang[i]);
      if(idex >=0) {
        lansSelList[idex] = true;
        lansController.selectIndex(idex);
      }
    }
    print(lansSelList);
    // if(lang.length == 0){
    //   lansSelList[0] = true;
    //   lansController.selectIndex(0);
    // }

    locationList =  SharePre.getString(GDefine.location).toString() .split(",")  ;
    locationList.remove(null);
    locationList.remove("");
    if(locationList.length == 0){

      locationList.add(S().personal_become_taipei);
      locationList.add(S().personal_become_outside);
    }
    print(S().personal_become_taipei);
    print(locationList);
    aboutMeEdit.text = SharePre.getString(GDefine.description).toString();
    nameEdit.text = SharePre.prefs.getString(GDefine.name);

    genderSwitchState = SharePre.getString(GDefine.gender).toString() == "f";

    if(SharePre.getString(GDefine.want_companion) == "1" ){
      wantCompanion = true;
      transBool = true;
      _checkboxesController.enableIndexes([0,1,2]);
    }else {
      wantCompanion = false;
      transBool = false;
      _checkboxesController.disableIndexes([0,1,2]);
    }

    List<String> tt = SharePre.getString(GDefine.trans).toString().contains(",")?   SharePre.getString(GDefine.trans).toString() .split(",") : [SharePre.getString(GDefine.trans).toString()];
    print("tt " + tt.toString());
    for (String c in tt){
      try {
        int a = int.parse(c);
        transSelList[a] = true;
        _checkboxesController.selectIndex(a);
      }catch(e){

      }
    }

    print("transSelList " + SharePre.getString(GDefine.trans).toString().toString());
    print("transSelList " + transSelList.toString());
    // if(transSelList.contains(true)){
    //   transBool = true;
    //   _checkboxesController.enableIndexes([0,1,2]);
    // }else {
    //   _checkboxesController.disableIndexes([0,1,2]);
    // }
    inviteEdit.text = SharePre.prefs.getString(GDefine.invite_code);
    if(int.tryParse( inviteEdit.text) == null ){
      inviteEdit.text = "";
    }
    // Future.delayed(Duration(milliseconds: 100), () {
    //   getTourist();
    // });



  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap:(){
        Tools().dismissK(context);
        print("onTap");
      },
      child: Scaffold(
          backgroundColor: OwnColors.white,
          body: Container(
            width: ScreenSize().getWidth(context),
            height: 1.sh,
            color: OwnColors.white,
            child: Column(
              children: [
                ScreenSize().createTopPad(context),
                Topbar().saveCreate(context, checkIfSave, saveClick, previewClick),

                Expanded(
                    child: SingleChildScrollView(
                      child: Container(
                        width: 0.92 * ScreenSize().getWidth(context),
                        child: Column(
                          children: [
                            HorizontalSpace().create(context, 0.01),
                            // Row(
                            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            //   crossAxisAlignment: CrossAxisAlignment.center,
                            //   children: [
                            //     Text(
                            //       S().personal_become_guilder_title,
                            //       style: GoogleFonts.inter(
                            //             fontSize: 16,
                            //             color: OwnColors.black,
                            //             fontWeight: FontWeight.w700,
                            //           ),
                            //     ),
                            //     Transform.scale(
                            //       scale: 0.8,
                            //       child: GestureDetector(
                            //
                            //         child: CupertinoSwitch(
                            //           value: becomeBool ,
                            //           activeColor: OwnColors.CFF644E,
                            //           onChanged: (bool value) {
                            //
                            //             // if(SharePre.prefs.getString(GDefine.isGuilder) == "true"){
                            //             //   SharePre.prefs.setString(GDefine.isGuilder,"false");
                            //             // }else {
                            //             //   SharePre.prefs.setString(GDefine.isGuilder,"true");
                            //             // }
                            //
                            //             setState(() {
                            //               print("onChanged");
                            //               becomeBool = value;
                            //             });
                            //           },
                            //         ),
                            //         onTap: () {
                            //           print("onTap");
                            //
                            //           setState(() {
                            //
                            //           });
                            //         },
                            //       ),
                            //       // child: CupertinoSwitch(
                            //       //   value: SharePre.getString(GDefine.isGuilder) == "true",
                            //       //   activeColor: OwnColors.CFF644E,
                            //       //   onChanged: (bool value) {
                            //       //     setState(() {
                            //       //       becomeBool = value;
                            //       //     });
                            //       //   },
                            //       // ),
                            //     )
                            //   ],
                            // ),
                            HorizontalSpace().create(context, 0.02),
                            Container(
                              width: 0.92 * ScreenSize().getWidth(context),
                              height: 0.92 * ScreenSize().getWidth(context),
                              child: Swiper(
                                loop: false,
                                itemBuilder: (BuildContext context, int index) {
                                  return imgList[index].contains("http")
                                      ? Stack(
                                        children: [
                                          GestureDetector(
                                    onTap: (){
                                      if(index != 0) {
                                        showBottomMenu();
                                        imgIndex = index;
                                      }
                                    },
                                    child: Padding(
                                      padding:  EdgeInsets.only(top:0.01.sh,right: 0.01.sh),
                                      child: CachedNetworkImage(
                                            imageUrl: imgList[index],
                                            fit: BoxFit.fill,
                                            placeholder: (context, url) => AspectRatio(
                                              aspectRatio: 1,
                                              child: FractionallySizedBox(
                                                heightFactor: 0.5,
                                                widthFactor: 0.5,
                                                child: SpinKitRing(
                                                  lineWidth:5,
                                                  color:
                                                  OwnColors.tran ,
                                                ),
                                              ),
                                            ),
                                            errorWidget: (context, url, error) =>
                                                Icon(
                                                  Icons.error,
                                                  color: OwnColors.black.withOpacity(0.5),
                                                ),
                                            imageBuilder: (context, imageProvider) =>
                                                Container(
                                                  // width:0.92 * ScreenSize().getWidth(context),
                                                  // height: 0.92 * ScreenSize().getWidth(context),
                                                  decoration: BoxDecoration(
                                                    color: OwnColors.tran,
                                                    borderRadius: BorderRadius.all(
                                                        Radius.circular(14)),
                                                    image: DecorationImage(
                                                        image: imageProvider,
                                                        fit: BoxFit.cover),
                                                  ),
                                                ),
                                            // width: MediaQuery.of(context).size.width,
                                      ),
                                    ),
                                  ),
                                          Positioned(
                                            top:0,
                                            right: 0,
                                            child: Opacity(
                                              opacity: index==0 || index==1 ?0.0:1.0,
                                              child: GestureDetector(
                                                onTap: (){
                                                  if(index!= 0 &&  index!=1) {
                                                    imgList[index] = "";
                                                    setState(() {

                                                    });
                                                  }
                                                },
                                                child:Container(
                                                  width: 0.048.sh,
                                                  height: 0.048.sh,

                                                  child: Stack(
                                                    children: [
                                                      Container(
                                                        decoration: BoxDecoration(
                                                            shape: BoxShape.circle,
                                                            color: OwnColors.white
                                                        ),
                                                      ),
                                                      Image.asset(Res.xmark_fill),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                            top:0,
                                            right: 0,
                                            child: Visibility(
                                              visible: index==0 || index==1 ?true:false,
                                              child: GestureDetector(
                                                onTap: (){

                                                  if(index==0 || index==1 ) {

                                                    showBottomMenu();
                                                    imgIndex = index;
                                                  }
                                                },
                                                child: Container(
                                                  width: 0.048.sh,
                                                  height: 0.048.sh,

                                                  child: Stack(
                                                    children: [
                                                      Container(
                                                        decoration: BoxDecoration(
                                                            shape: BoxShape.circle,
                                                            color: OwnColors.white
                                                        ),
                                                      ),
                                                      Image.asset(Res.icon_edit_circle),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          )


                                        ],
                                      )
                                      : GestureDetector(
                                        onTap: ()  {
                                        print("onTap");
                                        showBottomMenu();
                                        imgIndex = index ;
                                      },
                                        child: Container(
                                          width:0.9 * ScreenSize().getWidth(context),
                                          height: 0.9 * ScreenSize().getWidth(context),
                                          decoration: BoxDecoration(
                                          color: OwnColors.CF7F7F7,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(14)),
                                        ),
                                          child:
                                        // bestTourList.length==0?

                                        Padding(
                                          padding: const EdgeInsets.only(left:0.0,right: 8),
                                          child: Column(
                                            children: [
                                              Spacer(),
                                              Expanded(
                                                  child: Center(
                                                    child: Stack(
                                                        alignment: Alignment.topLeft,
                                                        children:[
                                                          Transform(
                                                            alignment: Alignment.center,
                                                            transform: Matrix4.rotationY(math.pi),
                                                            child: Padding(
                                                              padding: const EdgeInsets.only(right: 6),
                                                              child: Icon(SFSymbols.photo_fill, color: OwnColors.CE6E1E1,size: ScreenSize().getWidth(context)*0.3,),
                                                            ),
                                                          ),
                                                          Container(
                                                            height: 0.10 * ScreenSize().getWidth(context),
                                                            width: 0.10 * ScreenSize().getWidth(context),
                                                            child: Padding(
                                                              padding: const EdgeInsets.only(top:10.0,right: 20),
                                                              child:Stack(
                                                                children: [
                                                                  Image.asset(Res.add_fill),
                                                                ],
                                                              ),
                                                            ),
                                                          ),
                                                        ]
                                                    ),
                                                  ),
                                              ),
                                              Spacer(),
                                            ],
                                          ),
                                        ),
                                        ),
                                      );
                                },
                                itemCount: imgList.length,
                                itemWidth: 300.0,
                                itemHeight: 200.0,
                                onIndexChanged: (index) {
                                  currentIndexPage = index;
                                  setState(() {});
                                },
//                             pagination: new SwiperPagination(
//                                 builder: new DotSwiperPaginationBuilder(
//                                   color: Colors.white,
//                                   activeColor: OwnColors.CTourOrange,
//                                 ),
// ),
                                // control: new SwiperControl(),
                              ),
                            ),
                            HorizontalSpace().create(context, 0.02),
                            DotsIndicator(
                              dotsCount: imgList.length,
                              position: currentIndexPage.toDouble(),
                              decorator: DotsDecorator(
                                color: OwnColors.CC8C8C8,
                                activeColor: OwnColors.CTourOrange,
                              ),
                            ),
                            HorizontalSpace().create(context, 0.01),

                            Row(
                              children: [
                                Text(
                                  S().name,
                                  style: GoogleFonts.inter(
                                      fontWeight: FontWeight.w600,
                                      color: OwnColors.black,
                                      fontSize: 16
                                  ),
                                ),
                              ],
                            ),
                            HorizontalSpace().create(context, 0.01),
                            Container(
                              height: 0.035.sh,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                border: Border.all(color: OwnColors.C989898,width: 1),

                              ),
                              child: Center(
                                child: TextField(
                                    controller: nameEdit,

                                    style: GoogleFonts.inter(
                                        fontSize: 16.nsp,
                                        color: OwnColors.black,
                                        fontWeight: FontWeight.w500
                                    ),

                                    onChanged: (S){
                                      setState(() {
                                        nameChange = true;
                                      });
                                    },

                                    maxLength:64,
                                    maxLines: null,

                                    buildCounter: null,
                                    decoration: InputDecoration(
                                        hintStyle: GoogleFonts.inter(
                                            fontSize: 16.nsp,
                                            color: OwnColors.C989898,
                                            fontWeight: FontWeight.w500
                                        ),
                                        counterText: "",
                                        hintText: S().personal_become_guilder_name_hint,
                                        fillColor: OwnColors.white,

                                        counterStyle: TextStyle(color: Colors.transparent,fontSize: 0),
                                        contentPadding: EdgeInsets.only(left: 8,right: 8,top: 0,bottom: 0),

                                        //Change this value to custom as you like
                                        isDense: true,
                                        filled: true,
                                        border: OutlineInputBorder(
                                            borderRadius:
                                            new BorderRadius.circular(10.0),
                                            borderSide: BorderSide.none)


                                    )
                                ),
                              ),
                            ),
                            HorizontalSpace().create(context, 0.008),
                            Row(
                              children: [
                                Spacer(),
                                Text(nameEdit.text.length.toString() + "/64",style: GoogleFonts.inter(
                                    fontSize: 12.nsp,
                                    fontWeight: FontWeight.w500,
                                    color: nameEdit.text.length > 64 ? Colors.red:OwnColors.C989898
                                ),)
                              ],
                            ),

                            HorizontalSpace().create(context, 0.01),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  S().personal_become_guilder_location,
                                  style: GoogleFonts.inter(
                                      fontWeight: FontWeight.w600,
                                      color: OwnColors.black,
                                      fontSize: 16.nsp
                                  ),
                                ),

                                ClipOval(
                                  child: Material(
                                    color: Colors.transparent, // button color
                                    child: GestureDetector(

                                      child: SizedBox(width:  0.03.sh, height: 0.03.sh, child: Image.asset(Res.add_orange)),

                                      onTap: () {


                                  WarningDialog.showTextFieldDialog(context, S().personal_become_add_location_title,
                                      S().personal_become_add_location_title, S().personal_become_add_location_hint, locationEdit
                                      , addLocation);
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      HorizontalSpace().create(context, 0.02),
                      Align(
                        alignment: Alignment.topLeft,
                        child: Wrap(

                          spacing: 0,
                          runSpacing : 15.0,
                          children: List.generate(
                            locationList.length,

                                (i) {
                              return GestureDetector(
                                onTap: (){
                                  locationEdit.text = locationList[i];

                                  locationIndex = i;
                                  WarningDialog.showTextFieldSaveDeleteDialog(context, S().personal_become_add_location_title,
                                      S().personal_become_add_location_title, S().personal_become_add_location_hint, locationEdit
                                      ,delLocation, changeLocation);
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(left:8.0,right: 8),
                                  child: Container(

                                      decoration: BoxDecoration(
                                          color: OwnColors.white,
                                          border: Border.all(color: OwnColors.CC8C8C8,width: 0.5),
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(12),
                                              topRight: Radius.circular(12),
                                              bottomLeft: Radius.circular(12),
                                              bottomRight: Radius.circular(12)),

                                          boxShadow: [
                                            BoxShadow(
                                              color: OwnColors.primaryText.withOpacity(0.4),
                                              spreadRadius: 1,
                                              blurRadius: 4,
                                              offset: Offset(0, 3), // changes position of shadow
                                            ),
                                          ]),
                                      child: Padding(
                                        padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                        child: Text(locationList[i],
                                          textAlign: TextAlign.center,
                                          style: GoogleFonts.inter(
                                              height: 1.3,
                                              fontSize: 14.nsp,
                                              color: OwnColors.black
                                          ),
                                        ),
                                      )),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      HorizontalSpace().create(context, 0.02),

                            Row(
                              children: [
                                Text(
                                  S().personal_become_guilder_about,
                                  style: GoogleFonts.inter(
                                      fontWeight: FontWeight.w600,
                                      color: OwnColors.black,
                                      fontSize: 16
                                  ),
                                ),
                              ],
                            ),
                            HorizontalSpace().create(context, 0.01),
                            Container(
                              height: 0.18.sh,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                border: Border.all(color: OwnColors.C989898,width: 1),

                              ),
                              child: TextField(
                                  controller: aboutMeEdit,

                                  style: GoogleFonts.inter(
                                      fontSize: 16.nsp,
                                      color: OwnColors.black,
                                    fontWeight: FontWeight.w500
                                  ),

                                  onChanged: (S){
                                    setState(() {
                                      aboutChange = true;
                                    });
                                  },

                                  maxLength:300,
                                  maxLines: null,

                                  buildCounter: null,
                                  decoration: InputDecoration(
                                      hintStyle: GoogleFonts.inter(
                                          fontSize: 16.nsp,
                                          color: OwnColors.C989898,
                                          fontWeight: FontWeight.w500
                                      ),
                                      counterText: "",
                                      hintText: S().personal_become_guilder_about_hint,
                                      fillColor: OwnColors.white,

                                      counterStyle: TextStyle(color: Colors.transparent,fontSize: 0),
                                      contentPadding: EdgeInsets.only(left: 8,right: 8,top: 0,bottom: 0),

                                      //Change this value to custom as you like
                                      isDense: true,
                                      filled: true,
                                      border: OutlineInputBorder(
                                          borderRadius:
                                          new BorderRadius.circular(10.0),
                                          borderSide: BorderSide.none)


                                  )
                              ),
                            ),
                            HorizontalSpace().create(context, 0.008),
                            Row(
                              children: [
                                Spacer(),
                                Text(aboutMeEdit.text.length.toString() + "/300",style: GoogleFonts.inter(
                                    fontSize: 12.nsp,
                                    fontWeight: FontWeight.w500,
                                    color: aboutMeEdit.text.length > 300 ? Colors.red:OwnColors.C989898
                                ),)
                              ],
                            ),
                            HorizontalSpace().create(context, 0.01),
                            Row(
                              children: [
                                Text(
                                  S().personal_become_guilder_lan,
                                  style: GoogleFonts.inter(
                                      fontWeight: FontWeight.w600,
                                      color: OwnColors.black,
                                      fontSize: 16.nsp
                                  ),
                                ),
                              ],
                            ),
                            HorizontalSpace().create(context, 0.02),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: GroupButton(
                                controller: lansController,
                                isRadio: false,
                                onSelected: (val,index, isSelected) {
                                  print("onSelected");
                                  int check = 0;
                                  for(bool value in lansSelList){
                                    if(value){
                                      check++;
                                    }
                                  }

                                  if(check == 1){
                                    if(isSelected){
                                      languageChange = true;
                                      lansSelList[index] = isSelected;
                                      print(lansSelList);
                                    }
                                  }else if(check > 1){
                                    languageChange = true;
                                    lansSelList[index] = !lansSelList[index];
                                    print(lansSelList);
                                  }
                                  setState(() {

                                  });

                                },

                                buttons: lansList,
                                buttonIndexedBuilder: (selected, index, context) {
                                  return  lansSelList[index]?Padding(
                                    padding: const EdgeInsets.only(left:8.0,right: 8),
                                    child: Container(

                                        decoration: BoxDecoration(
                                            color: OwnColors.white,
                                            border: Border.all(color: OwnColors.CC8C8C8,width:0.5),
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(12),
                                                topRight: Radius.circular(12),
                                                bottomLeft: Radius.circular(12),
                                                bottomRight: Radius.circular(12)),
                                            boxShadow: [
                                              BoxShadow(
                                                color: OwnColors.primaryText.withOpacity(0.4),
                                                spreadRadius: 1,
                                                blurRadius: 4,
                                                offset: Offset(0, 3), // changes position of shadow
                                              ),
                                            ]),
                                        child: Padding(
                                          padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                          child: Text(lansList[index],
                                            textAlign: TextAlign.center,
                                            style: GoogleFonts.inter(
                                                fontWeight: FontWeight.w400,
                                                height: 1.3,
                                                fontSize: 14.nsp,
                                                color: OwnColors.black
                                            ),
                                          ),
                                        )),
                                  ):
                                  Padding(
                                    padding: const EdgeInsets.only(left:8.0,right: 8),
                                    child: Container(

                                        decoration: BoxDecoration(
                                          color: OwnColors.white,
                                          border: Border.all(color: OwnColors.CC8C8C8,width: 0.5),
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(12),
                                              topRight: Radius.circular(12),
                                              bottomLeft: Radius.circular(12),
                                              bottomRight: Radius.circular(12)),),
                                        child: Padding(
                                          padding: const EdgeInsets.only(left:12.0,right: 12,bottom: 2,top: 2),
                                          child: Text(lansList[index],
                                            textAlign: TextAlign.center,
                                            style: GoogleFonts.inter(
                                                fontWeight: FontWeight.w400,
                                                height: 1.3,
                                                fontSize: 14.nsp,
                                                color: OwnColors.C989898
                                            ),
                                          ),
                                        )),
                                  );

                                },
                                options: GroupButtonOptions(


                                  borderRadius: BorderRadius.circular(8),
                                  spacing: 0,
                                  runSpacing: 10,
                                  groupingType: GroupingType.wrap,
                                  direction: Axis.horizontal,
                                  buttonHeight: 60,
                                  buttonWidth: 60,

                                  mainGroupAlignment: MainGroupAlignment.start,
                                  crossGroupAlignment: CrossGroupAlignment.start,
                                  groupRunAlignment: GroupRunAlignment.start,
                                  textAlign: TextAlign.center,
                                  textPadding: EdgeInsets.only(left: 8,right: 8),
                                  alignment: Alignment.centerLeft,
                                  elevation: 0,
                                ),
                              ),
                            ),
                            HorizontalSpace().create(context, 0.03),
                            Row(
                              children: [
                                Text(
                                  S().personal_become_guilder_gender,
                                  style: GoogleFonts.inter(

                                      fontWeight: FontWeight.w600,
                                      color: OwnColors.black,
                                      fontSize: 16.nsp
                                  ),
                                ),
                                Spacer(),
                                Container(
                                  height: ScreenSize().getHeight(context) * 0.04,
                                  width: 0.20 * ScreenSize().getWidth(context),
                                  decoration: const BoxDecoration(
                                    color: Color(0xffEDEDED),
                                    borderRadius: BorderRadius.all(Radius.circular(10)),
                                  ),
                                  child: Stack(
                                    children: [
                                      AnimatedPositioned(
                                        top: 0.003 * ScreenSize().getHeight(context),
                                        left: !genderSwitchState
                                            ? 5
                                            : 0.12 * ScreenSize().getWidth(context) - 5,
                                        duration: const Duration(milliseconds: 300),
                                        curve: Curves.easeInOut,
                                        child: Align(
                                          alignment: Alignment.center,
                                          child: Container(
                                            height: 0.034 * ScreenSize().getHeight(context),
                                            width: 0.08 * ScreenSize().getWidth(context),
                                            decoration: BoxDecoration(
                                                color: (OwnColors.white),
                                                borderRadius: BorderRadius.all(Radius.circular(8)),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: OwnColors.primaryText.withOpacity(0.1),
                                                    spreadRadius: 0.5,
                                                    blurRadius: 0.5,
                                                    offset: Offset(0, 1),
                                                  )
                                                ]),
                                          ),
                                        ),
                                      ),
                                      Row(
                                        children: [
                                          //Container(width: 10,),
                                          Expanded(
                                            child: Center(
                                                child :Icon(Icons.male,color:switch1Color,)


                                            ),
                                          ),

                                          Expanded(
                                            child :Container(
                                              //height: ScreenSize().getHeight(context)*0.03,
                                                child :Icon(Icons.female,color:switch2Color,)
                                              // child: AspectRatio(
                                              //   aspectRatio: 1.5,
                                              //   child: Ink.image( image: AssetImage(Res.bi_female),
                                              //     fit: BoxFit.fill,
                                              //   ),
                                              // ),

                                            )
                                          ),

                                          //Container(width: 10,),
                                        ],
                                      ),
                                      new Positioned.fill(
                                          child: GestureDetector(

                                            onTap: () {
                                              switchClick();
                                            },
                                          ))
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            HorizontalSpace().create(context, 0.02),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  S().personal_become_guilder_want_companion,
                                  style: GoogleFonts.inter(
                                      fontWeight: FontWeight.bold,
                                      color: OwnColors.black,
                                      fontSize: 16.nsp),
                                ),
                                Transform.scale(
                                  scale: 0.8,
                                  child: CupertinoSwitch(
                                    value: wantCompanion,
                                    activeColor: OwnColors.CFF644E,
                                    onChanged: (bool value) {

                                      setState(() {
                                        wantCompanion = value;
                                        transBool = wantCompanion;
                                        if(transBool == false){
                                          _checkboxesController.unselectAll();
                                          _checkboxesController.disableIndexes([0,1,2]);
                                        }else {
                                          _checkboxesController.enableIndexes([0,1,2]);
                                          // _checkboxesController.selectIndex(0);
                                          transSelList[0] = true;
                                        }
                                      });
                                    },
                                  ),
                                )
                              ],
                            ),
                            Padding(
                              padding:  EdgeInsets.only(bottom: 8.0,left:0.09.sw,top: 4),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    S().personal_become_guilder_trans,
                                    style: GoogleFonts.inter(
                                        fontWeight: FontWeight.bold,
                                        color: transBool ? OwnColors.black: OwnColors.C989898,
                                        fontSize: 16.nsp),
                                  ),
                                  Container(width: 5,),
                                  JustTheTooltip(

                                    controller: tooltipController2,
                                    borderRadius: BorderRadius.all(Radius.circular(11)),
                                    backgroundColor: OwnColors.CEEEEEE,

                                    onDismiss: (){
                                      // showInfo = false;
                                    },
                                    onShow: (){
                                      // showInfo = true;
                                    },
                                    isModal: true,
                                    preferredDirection: AxisDirection.up,
                                    elevation: 0,
                                    offset: 0,
                                    margin: EdgeInsets.only(left:0.15.sw),
                                    // tailBuilder: (_, __, ___) => JustTheInterface.defaultTailBuilder(Offset(_.dx+10, _.dy), Offset(__.dx+10, __.dy), Offset(___.dx+10, ___.dy),),
                                    child: Material(
                                      color: Colors.grey.shade800,
                                      shape: const CircleBorder(),


                                      child:   ClipOval(
                                        child: Material(
                                          color: OwnColors.CEEEEEE, // button color
                                          child: GestureDetector(

                                            child: SizedBox(width: 0.02.sh, height: 0.02.sh, child:Image.asset(
                                                Res.question_fill)),
                                            onTap: () {
                                              setState(() {
                                                if(showInfo){
                                                  tooltipController2.hideTooltip();
                                                }else {
                                                  tooltipController2.showTooltip();
                                                }


                                              });
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                    content: Padding(
                                        padding: EdgeInsets.all(12.0),
                                        child: Container(


                                          child: Text(
                                            S().personal_become_guilder_have_car
                                            ,style: GoogleFonts.inter(fontSize: 14.nsp,color: OwnColors.black,fontWeight: FontWeight.w300

                                          ),),
                                        )
                                    ),

                                  ),
                                  Spacer(),
                                  // Transform.scale(
                                  //   scale: 0.8,
                                  //   child: CupertinoSwitch(
                                  //     value: transBool,
                                  //     activeColor: OwnColors.CFF644E,
                                  //     onChanged: (bool value) {
                                  //       tranChange = true;
                                  //       setState(() {
                                  //         transBool = value;
                                  //         if(transBool == false){
                                  //           _checkboxesController.unselectAll();
                                  //           _checkboxesController.disableIndexes([0,1,2]);
                                  //         }else {
                                  //           _checkboxesController.enableIndexes([0,1,2]);
                                  //           _checkboxesController.selectIndex(0);
                                  //           transSelList[0] = true;
                                  //         }
                                  //       });
                                  //     },
                                  //   ),
                                  // )
                                ],
                              ),
                            ),
                            Padding(
                              padding:   EdgeInsets.only(right: 12,left:0.09.sw),
                              child: GroupButton(
                                controller: _checkboxesController,
                                isRadio: false,

                                onSelected: (val,index, isSelected) {
                                  tranChange = true;

                                },

                                buttons: transList,
                                buttonIndexedBuilder: (selected, index, context) {


                                  return
                                    CheckBoxTile(
                                      title: transList[index],
                                      selected: selected,
                                      canSelected: transBool,
                                      onTap: () {

                                        if(transBool) {
                                          if (!selected) {
                                            tranChange = true;
                                            _checkboxesController.selectIndex(
                                                index);
                                            transSelList[index] = true;
                                            return;
                                          }else{
                                            // int check = 0;
                                            // for(bool value in transSelList){
                                            //   if(value){
                                            //     check++;
                                            //   }
                                            // }
                                            // if(check > 1){
                                              tranChange = true;
                                              transSelList[index] = false;
                                              _checkboxesController.unselectIndex(index);
                                            // }

                                          }
                                        }
                                      },
                                    );
                                },
                                options: GroupButtonOptions(
                                  // selectedShadow: const [],
                                  // selectedTextStyle: TextStyle(
                                  //   fontSize: 20,
                                  //   color: OwnColors.white,
                                  // ),
                                  // selectedColor: OwnColors.CF39800,
                                  // unselectedShadow: const [],
                                  // unselectedColor: OwnColors.white,
                                  // unselectedTextStyle: TextStyle(
                                  //   fontSize: 20,
                                  //   color: OwnColors.black_a48,
                                  // ),
                                  // selectedBorderColor: OwnColors.CFFD796,
                                  // unselectedBorderColor: OwnColors.black_a12,
                                  borderRadius: BorderRadius.circular(8),
                                  spacing: 0,
                                  runSpacing: 0,
                                  groupingType: GroupingType.column,
                                  direction: Axis.horizontal,
                                  buttonHeight: 40,
                                  buttonWidth: 60,
                                  mainGroupAlignment: MainGroupAlignment.start,
                                  crossGroupAlignment: CrossGroupAlignment.center,
                                  groupRunAlignment: GroupRunAlignment.center,
                                  textAlign: TextAlign.center,
                                  textPadding: EdgeInsets.only(left: 8,right: 8),
                                  alignment: Alignment.center,
                                  elevation: 0,
                                ),
                              ),
                            ),
                            HorizontalSpace().create(context, 0.02),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  S().personal_become_guilder_invite,
                                  style: GoogleFonts.inter(
                                      fontWeight: FontWeight.bold,
                                      color: OwnColors.black,
                                      fontSize: 16),
                                ),
                                Container(width: 5,),
                                JustTheTooltip(

                                  controller: tooltipController,
                                  borderRadius: BorderRadius.all(Radius.circular(11)),
                                  backgroundColor: OwnColors.CEEEEEE,

                                  onDismiss: (){
                                    // showInfo = false;
                                  },
                                  onShow: (){
                                    // showInfo = true;
                                  },
                                  isModal: true,
                                  preferredDirection: AxisDirection.up,
                                  elevation: 0,
                                  offset: 0,
                                  margin: EdgeInsets.only(left:0.15.sw),
                                  // tailBuilder: (_, __, ___) => JustTheInterface.defaultTailBuilder(Offset(_.dx+10, _.dy), Offset(__.dx+10, __.dy), Offset(___.dx+10, ___.dy),),
                                  child: Material(
                                    color: Colors.grey.shade800,
                                    shape: const CircleBorder(),


                                    child:   ClipOval(
                                      child: Material(
                                        color: OwnColors.CEEEEEE, // button color
                                        child: GestureDetector(

                                          child: SizedBox(width: 0.02.sh, height: 0.02.sh, child:Image.asset(
                                              Res.question_fill)),
                                          onTap: () {
                                            setState(() {
                                              if(showInfo){
                                                tooltipController.hideTooltip();
                                              }else {
                                                tooltipController.showTooltip();
                                              }


                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                  content: Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: Container(
                                        width: 0.57.sw,

                                        child: Text(
                                          S().personal_become_guilder_invite_hint2
                                          ,style: GoogleFonts.inter(fontSize: 14.nsp,color: OwnColors.black,fontWeight: FontWeight.w300

                                        ),),
                                      )
                                  ),

                                ),
                              ],
                            ),
                            HorizontalSpace().create(context, 0.02),
                            Container(

                              width: double.infinity,
                              decoration: BoxDecoration(
                                borderRadius:
                                BorderRadius.all(Radius.circular(10)),
                                border: Border.all(color: OwnColors.C989898,width: 1),

                              ),
                              child: Center(
                                child: TextField(
                                    controller: inviteEdit,
                                    maxLength: 4,
                                    style: GoogleFonts.inter(
                                        fontSize: 14.nsp,
                                        fontWeight: FontWeight.w500,
                                        color: OwnColors.black
                                    ),
                                    onChanged: (S){
                                      inviteChange = true;
                                      setState(() {

                                      });
                                    },


                                    maxLines: 1,
                                    // maxLength: 4,
                                    decoration: InputDecoration(
                                        hintText: S().personal_become_guilder_invite_hint,
                                        fillColor: OwnColors.white,
                                        counterStyle: TextStyle(color: Colors.transparent),
                                        contentPadding: EdgeInsets.symmetric(horizontal: 10,vertical:4),

                                        //Change this value to custom as you like
                                        isDense: true,
                                        filled: true,
                                        counterText: "",
                                        border: OutlineInputBorder(
                                            borderRadius:
                                            new BorderRadius.circular(20.0),
                                            borderSide: BorderSide.none)


                                    ),
                                  keyboardType: TextInputType.number,
                                ),
                              ),
                            ),
                            HorizontalSpace().create(context, 0.02),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  S().personal_become_guilder_visible,
                                  style: GoogleFonts.inter(
                                      fontWeight: FontWeight.bold,
                                      color: OwnColors.black,
                                      fontSize: 16.nsp),
                                ),
                                Transform.scale(
                                  scale: 0.8,
                                  child: CupertinoSwitch(
                                    value: becomeBool,
                                    activeColor: OwnColors.CFF644E,
                                    onChanged: (bool value) {
                                      becomeBool = false;
                                      print("onChange");
                                      setState(() {
                                        becomeBool = value;

                                      });
                                    },
                                  ),
                                )
                              ],
                            ),
                            HorizontalSpace().create(context, 0.05),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Stack(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Container(height: 0.03.sh,child: Icon(SFSymbols.trash)),
                                        Align(
                                          alignment: Alignment.centerLeft,
                                          child: AutoSizeText(

                                            S().personal_edit_delete,

                                            minFontSize: 10,
                                            maxFontSize: 50,
                                            style:GoogleFonts.inter(
                                                fontSize: 16.nsp,
                                                color: OwnColors.black.withOpacity(1),
                                                fontWeight: FontWeight.w500,
                                                decoration: TextDecoration.underline
                                            ),
                                            maxLines: 1,
                                          ),
                                        ),
                                      ],
                                    ),
                                    new Positioned.fill(
                                        child: new Material(
                                          color: Colors.transparent,
                                          child: new GestureDetector(

                                            onTap: () => {delete()},
                                          ),
                                        ))
                                  ],
                                ),
                              ],
                            ),
                            HorizontalSpace().create(context, 0.1),
                            ScreenSize().createBottomPad(context)
                          ],
                        ),
                      ),
                    ))
              ],
            ),
          )),
    );
  }
  void delete() async {
    WarningDialog.showIOSAlertDialog2(
        context, S().delete_account_title, S().delete_account_content, S().delete, sure2Delete);
  }
  void sure2Delete() async {

    if(await  FirebaseAuth.instance.currentUser == null){
      WarningDialog.showIOSAlertDialogWithFunc(context, S().personal_edit_delete_no_firebase_title, S().personal_edit_delete_no_firebase_hint, S().confirm,sure2logout);
    }else {

      ResultData resultData = await AppApi.getInstance().delete_user(context, true, SharePre.prefs.getString(GDefine.user_id));
      if (resultData.isSuccess()) {
        print(resultData.data.toString());
        FirebaseAuth.instance.currentUser.delete();
        PushNewScreen().popAllAndPush(context, LoginChoosePage());
        setState(() {
          print("delete");
        });
      } else {
        WarningDialog.showIOSAlertDialog(
            context,
            S().personal_edit_delete_fail_title,
            S().personal_edit_delete_fail_hint,
            S().confirm);
      }
    }

  }

  void switchClick() {
    genderChange = true;
    setState(() {
      genderSwitchState = !genderSwitchState;
    });

    Future.delayed(Duration(milliseconds: 150), () {
      setState(() {
        if (genderSwitchState) {

          switch1Color = OwnColors.C989898;
          switch2Color = OwnColors.CTourOrange  ;
        } else {
            switch1Color = OwnColors.CTourOrange;
            switch2Color = OwnColors.C989898 ;

        }
      });
    });
  }

  void checkIfSave(){


    if(imgChange || locationChange || aboutChange ||languageChange || genderChange || tranChange || inviteChange || nameChange) {
      WarningDialog.showIOSAlertDialog3(
          context,
          S().save_not_yet,
          S().save_not_yet_hint2,
          S().cancel,
          S().personal_edit_save,
          backClick,
          saveClick);
    }else {
      backClick();
    }
  }

  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }

  void addLocation() {
    locationChange = true;
    locationList.add( locationEdit.text );
    locationEdit.text = "";
    setState(() {

    });

  }

  void changeLocation() {
    locationChange = true;
    locationList[locationIndex] = locationEdit.text;
    locationEdit.text = "";
    setState(() {

    });

  }

  void delLocation() {
    locationChange = true;
    print("delLocation");
    if(locationList.length > 1){
      locationList.removeAt(locationIndex);
      locationEdit.text = "";
    }else{
      WarningDialog.showIOSAlertDialog(context, S().no_trip, S().no_trip_hint, S().confirm);
    }
    setState(() {

    });

  }
  void saveClick() {
    print(imgList.toString());
    if(imgList[0].length == 0 && imgList[1].length == 0 && imgList[2].length == 0 && imgList[3].length == 0 && imgList[4].length == 0 ){
      WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_no_img_title, S().no_image_hint, S().confirm);
    }else if (locationList.isEmpty){
      WarningDialog.showIOSAlertDialog(context, S().no_trip, S().no_trip_hint, S().confirm);
    }else if (!lansSelList.contains(true)) {
      WarningDialog.showIOSAlertDialog(context, S().no_language, S().no_language_hint, S().confirm);
    // }else if(transBool && !transSelList.contains(true)){
    //   WarningDialog.showIOSAlertDialog(context, S().home_have_no_car, S().no_trans_hint, S().confirm);
    }else if(inviteEdit.text.isNotEmpty && inviteEdit.text.length>4) {
      WarningDialog.showIOSAlertDialog(context, S().personal_become_guilder_invite, S().invite_code_extra, S().confirm);
    }else if(aboutMeEdit.text.length>300) {
      WarningDialog.showIOSAlertDialog(context, S().personal_become_guilder_about, S().about_me_extra, S().confirm);
    }
      else{
      List<String> langs = [];
      List<String> trans = [];
      // List<String>  = [];
      // String location = locationList.join("|");

      for (int i = 0 ; i< lansSelList.length ; i++){
        if(lansSelList[i]){
          langs.add(lansList[i]);
        }
      }
      print(transBool);

      if(transSelList.contains(true)) {
        for (int i = 0; i < transSelList.length; i++) {
          if (transSelList[i]) {
            trans.add(i.toString());
          }
        }
      }else {
        trans.add(3.toString());
      }

      setUserInfo(langs,trans,locationList);
    }
    print("saveClick");
  }

  void previewClick() {
    if(imgList[0].length == 0 && imgList[1].length == 0 && imgList[2].length == 0 && imgList[3].length == 0 && imgList[4].length == 0 ){
      WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_no_img_title, S().no_image_hint, S().confirm);
    }else if (locationList.isEmpty){
      WarningDialog.showIOSAlertDialog(context, S().no_trip, S().no_trip_hint, S().confirm);
    }else if (!lansSelList.contains(true)) {
      WarningDialog.showIOSAlertDialog(context, S().no_language, S().no_language_hint, S().confirm);
    // }else if(transBool && !transSelList.contains(true)){
    //   WarningDialog.showIOSAlertDialog(context, S().home_have_no_car, S().no_trans_hint, S().confirm);
    }else if(inviteEdit.text.isNotEmpty && inviteEdit.text.length>4) {
      WarningDialog.showIOSAlertDialog(context, S().personal_become_guilder_invite, S().invite_code_extra, S().confirm);
    }else if(aboutMeEdit.text.length>300) {
      WarningDialog.showIOSAlertDialog(context, S().personal_become_guilder_about, S().about_me_extra, S().confirm);
    }
    else {
      print("previewClick");
      print("previewClickWWWW");
      List<String> langs = [];
      List<String> trans = [];
      // List<String>  = [];
      // String location = locationList.join("|");

      for (int i = 0 ; i< lansSelList.length ; i++){
        if(lansSelList[i]){
          langs.add(lansList[i]);
        }
      }
      print(transBool);

      if(transSelList.contains(true)) {
        for (int i = 0; i < transSelList.length; i++) {
          if (transSelList[i]) {
            trans.add(i.toString());
          }
        }
      }else {
        trans.add(3.toString());
      }


      print(trans);

      List<dynamic> imageListNew = List.from(imgList).toList();
      String  avater =  imgList[0];

      imageListNew.removeAt(0);

      GuildClass guildClass = new GuildClass(
          name: nameEdit.text,
          avatar: avater,
          description: aboutMeEdit.text,
          trans: trans,
          location: locationList,
          tourist_img: imageListNew,
          lang: langs,
        want_companion: wantCompanion?"1":"0"
 

      );

      PushNewScreen().normalPush(context, GuilderPage(guildClass: guildClass,id: SharePre.getString(GDefine.user_id),preview: true,));
      // PushNewScreen().normalPush(
      //     context, PreviewGuilderPage(guildClass: guildClass));
    }
  }

  void showBottomMenu() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 150,
              child: Column(children: <Widget>[
                ListTile(
                  leading: Icon(Icons.photo_camera),
                  title: Text(S().take_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromCamera();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.photo_library),
                  title: Text(S().choose_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromPhoto();
                  },
                )
              ]));
        });
  }

  final picker = ImagePicker();
  void pcikImageFromCamera( ) async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);

    File img = File(pickedFile.path);
    Uint8List imageBytes = await pickedFile.readAsBytes();



    CroppedFile  croppedFile = await CropTool().crop(1, 1, CropAspectRatioPreset.square, false, img.path);
    if(croppedFile != null) {
      imageBytes = await croppedFile.readAsBytes();

      String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + base64Image);
      _uploadImage(base64Image);

      print(croppedFile);
    }



  }
  void pcikImageFromPhoto() async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);
    File img = File(pickedFile.path);
    Uint8List imageBytes = await pickedFile.readAsBytes();

    CroppedFile  croppedFile = await CropTool().crop(1, 1, CropAspectRatioPreset.square, false, img.path);
    if(croppedFile != null) {
      imageBytes = await croppedFile.readAsBytes();

      String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + base64Image);
      _uploadImage(base64Image);

      print(croppedFile);
    }


  }


  // void pcikImageFromPhoto() async {
  //   PickedFile pickedFile = await picker.getImage(
  //       source: ImageSource.gallery,
  //       maxHeight: 1440,
  //       maxWidth: 1440,
  //       imageQuality: 80);
  //   File img = File(pickedFile.path);
  //   List<int> imageBytes = await img.readAsBytes();
  //   avatar = img.path;
  //   avatarFileName = avatar
  //       .split("/")
  //       .last;
  //
  //   String base64Image = "data:image/jpg;base64," +base64Encode(imageBytes);
  //   print("base64Image: " + base64Image);
  //   _uploadImage(base64Image);
  //
  //   setState(() {
  //
  //   });
  //
  // }

  Future<void> _uploadImage(String data)  async {
    // Loading.show(context);
    imgChange = true;
    ResultData resultData = await AppApi.getInstance().setFileBase64(
        context,
        true,
        data
    );

    if (resultData.isSuccess()) {
      // Loading.dismiss(context);
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data);
      String img_url = GDefine.imageUrl + resultData.data["path"];
      print("img_url " + img_url);
      imgList[imgIndex] = img_url;
      setState(() {

      });
    } else {
      // Loading.dismiss(context);
      WarningDialog.showIOSAlertDialog(context, S().upload_fail_title,  S().upload_fail_hint, S().confirm);

    }

  }


  Future<void> setUserInfo(List<String> langs,List<String> trans, List<String> location)  async {
    Loading.show(context);

    imgList. removeWhere((element) => element == "");
    String  avater =  imgList[0];

    imgList.removeAt(0);
    ResultData resultData = await AppApi.getInstance().setUserInfo(
        context,
        true,
        imgList,
      langs,
      genderSwitchState?"f":"m",
        !becomeBool?"1":"0",
      trans,
      location,
      inviteEdit.text,
      aboutMeEdit.text,
      nameEdit.text,
      wantCompanion? "1":"0",
        avater
    );

    if (resultData.isSuccess()) {

      loginAPI();
    } else {
      Loading.dismiss(context);
      WarningDialog.showIOSAlertDialog(context, S().update+S().fail, S().update+S().fail, S().confirm);

    }

  }
  void loginAPI( ) async {
    print("loginAPI");

    ResultData resultData = await AppApi.getInstance().login(context, true,  SharePre.prefs.getString(GDefine.firebase_id), SharePre.prefs.getString(GDefine.login_type));
    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data.toString());

      SharePre.prefs.setString(GDefine.name, resultData.data["name"]);
      SharePre.prefs.setString(GDefine.user_id, resultData.data["user_id"].toString());
      SharePre.prefs.setString(GDefine.avatar, resultData.data["avatar"].toString());
      SharePre.prefs.setString(GDefine.gender, resultData.data["gender"].toString());

      SharePre.prefs.setString(GDefine.is_fcm_open, resultData.data["is_fcm_open"].toString());
      SharePre.prefs.setString(GDefine.description, resultData.data["description"].toString());
      SharePre.prefs.setString(GDefine.invite_code, resultData.data["invite_code"].toString());
      SharePre.prefs.setString(GDefine.want_companion, resultData.data["want_companion"].toString());

      List<dynamic> trans =  resultData.data["trans"];
      List<dynamic> location =  resultData.data["location"];
      List<dynamic> tourist_img =  resultData.data["tourist_img"];
      List<dynamic> lang =  resultData.data["lang"];
      SharePre.prefs.setString(GDefine.trans, trans.join(","));
      SharePre.prefs.setString(GDefine.location, location.join(","));
      SharePre.prefs.setString(GDefine.tourist_img, tourist_img.join(","));
      SharePre.prefs.setString(GDefine.lang, lang.join(","));

      String role_slug  = resultData.data["role_slug"].toString();
      if(role_slug.contains("guide")){
        SharePre.prefs.setString(GDefine.isGuilder, "true");
      }else {
        SharePre.prefs.setString(GDefine.isGuilder, "false");
      }

      Loading.dismiss(context);
      backClick();
    }else  if(resultData.data.toString().contains("停用")){


      WarningDialog.showIOSAlertDialog2(context,S().account_unactivated_title, S().account_unactivated_hint,S().confirm,sure2logout);

    }


    else {
      Loading.dismiss(context);
      WarningDialog.showIOSAlertDialog(context, S().update+S().fail, S().update+S().fail, S().confirm);
    }
  }


  Future<void> sure2logout() async {
    SharePre.prefs.setInt(GDefine.isLogin, 0);
    if(await FirebaseAuth.instance.currentUser != null){
      await FirebaseAuth.instance.signOut();
    }
    PushNewScreen().popAllAndPush(context, LoginChoosePage());
  }




}
class CheckBoxTile extends StatelessWidget {
  const CheckBoxTile({
    Key key,
     this.selected,
     this.canSelected,
     this.onTap,
     this.title,
  }) : super(key: key);

  final String title;
  final bool selected;
  final bool canSelected;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return
      Row(
        children: [
          new Text(title,
          style: GoogleFonts.inter(
            fontSize: 14.nsp,
            color: canSelected ? OwnColors.black : OwnColors.C989898
          ),),
          Expanded(child: Container(
            color: OwnColors.white,
            height: 0.03.sh,
          )),
          // CustomCheckBox(
          //   value: selected,
          //   shouldShowBorder: true,
          //   borderColor: Colors.red,
          //   checkedFillColor: Colors.red,
          //   borderRadius: 8,
          //   borderWidth: 1,
          //   checkBoxSize: 22,
          //   onChanged: (val) {
          //     //do your stuff here
          //     onTap();
          //   },
          // ),
          Transform.scale(
            scale: 1.1,
            child: Checkbox(
              value: selected,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(3.0))),
              visualDensity: VisualDensity(horizontal: -4, vertical: -4),
              activeColor: OwnColors.CTourOrange,
              side: MaterialStateBorderSide.resolveWith(
                    (states) => BorderSide(width: 1.0, color: selected ? OwnColors.tran : canSelected?
                    OwnColors.black : OwnColors.C989898),
              ),
              onChanged: (val) {
                if(canSelected) {
                  onTap();
                }
              },
            ),
          ),
        ],

    );
  }


}