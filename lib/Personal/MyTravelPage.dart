import 'dart:async';
import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/TravelCard.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Guild/GuildReviewPage.dart';
import 'package:ctour/SQLite/Database.dart';
import 'package:ctour/SQLite/model/TravelModel.dart';
import 'package:ctour/Travel/TravelPage.dart';
import 'package:ctour/tool/DateTools.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:google_fonts/google_fonts.dart';
import '../Travel/TravelClass.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/Travel/NewTravelPage.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:shimmer/shimmer.dart';
import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/TopBar.dart';
import '../res.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_star/flutter_star.dart';

class MyTravelPage extends StatefulWidget {



  const MyTravelPage({
    Key key,


  }) : super(key: key);

  @override
  State createState() {
    return _MyTravelPageState();
  }
}

class _MyTravelPageState extends State<MyTravelPage> with TickerProviderStateMixin {


  List<TravelClass> postList = new List();
  List<TravelClass> postShowList = new List();
  List<TravelClass> draftList = new List();
  List<TravelClass> draftShowList = new List();

  TextEditingController searchEdit= TextEditingController();
  String searchText1 = "";

  bool loadingBool = true;
  List<GuildClass> tourList = new List();
  TabController _tabFirstController;
  int currentFirstTab = 0;
  EasyRefreshController refreshController = new EasyRefreshController( ) ;
  FocusNode searchFocus = new FocusNode();
  final ScrollController listScrollController1 = ScrollController();
  final ScrollController listScrollController2 = ScrollController();

  String delArticalID = "0";
  @override
  void initState() {
    super.initState();
    // if (Platform.isAndroid) {
    SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(
        statusBarColor: OwnColors.tran, //设置为透明
        statusBarIconBrightness: Brightness.dark);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
    // }
    refreshController = EasyRefreshController(
      controlFinishRefresh: true,
      controlFinishLoad: true,
    );

    // listScrollController1.addListener(() {Tools().dismissK(context);});
    // listScrollController2.addListener(() {Tools().dismissK(context);});

  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: OwnColors.white,
        body: FocusDetector(
          onFocusGained: (){
            updateAll();
          },
          child: GestureDetector(
            onTap:(){
              Tools().dismissK(context);

            },
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: ScreenSize().getWidth(context)*1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ScreenSize().createTopPad(context),
                  HorizontalSpace().create(context, 0.01),
                  Container(
                    height:  0.05.sh,
                    child: Topbar().createAdd(context,S().personal_my_travel, ScreenSize().getHeight(context) * 0.073,  backClick, addClick ),
                  ),
                  HorizontalSpace().create(context, 0.015),
                  Container(
                    height: 0.05 .sh,
                    width: ScreenSize().getWidth(context) * 0.88,
                    // height: height,
                    decoration: BoxDecoration(
                      color: OwnColors.white,
                      border: Border.all(color: OwnColors.CC8C8C8),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30),
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30)),
                    ),

                    alignment: Alignment.center,
                    child: Row(
                      children: [
                        GestureDetector(
                          onTap: (){
                            FocusScope.of(context).requestFocus(searchFocus);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 12.0, bottom: 4.0),
                            child: Container(
                                height: 0.027 .sh,
                                child: Icon(SFSymbols.search, color: OwnColors.black)),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            height: 0.027 .sh,
                            child: TextField(
                              focusNode: searchFocus,
                              controller: searchEdit,
                              onChanged: (text) {
                                print("onChanged");
                                if (searchText1 == "") {
                                  searchText1 = text;
                                  if(currentFirstTab == 0){
                                    searchTravel1();
                                  }else {
                                    searchTravel2();
                                  }
                                  // searchChat();
                                } else {
                                  if (searchText1 != text) {
                                    searchText1 = text;
                                    if(currentFirstTab == 0){
                                      searchTravel1();
                                    }else {
                                      searchTravel2();
                                    }
                                  }
                                }
                              },
                              autocorrect: false,
                              decoration: InputDecoration(
                                  fillColor: OwnColors.tran,
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 0),
                                  //Change this value to custom as you like
                                  // isDense: true,
                                  hintText:  S().personal_my_travel_search_hint ,
                                  filled: true,
                                  hintStyle: GoogleFonts.inter(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16.nsp, color:OwnColors.C989898),
                                  border: OutlineInputBorder(
                                      borderRadius:
                                      new BorderRadius.circular(20.0),
                                      borderSide: BorderSide.none)),
                              style: GoogleFonts.inter(
                                  fontWeight: FontWeight.w500,
                                  color: Colors.black.withOpacity(1),
                                  fontSize: ScreenUtil().setSp(16)),
                              maxLines: 1,
                              keyboardType: TextInputType.text,
                              obscureText: false,
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                  HorizontalSpace().create(context, 0.01),
                  _tabFirst(),

                  currentFirstTab == 0?travelPage():travelPage2()

                ],
              ),
            ),
          ),
        ));
  }
  Widget _tabFirst() {
    return Container(
      width: 0.92.sw,
      child: DefaultTabController(
        length: 2,
        child: TabBar(
          controller: _tabFirstController,
          isScrollable: false,
          indicatorColor: OwnColors.CTourOrange,
          labelColor: OwnColors.CTourOrange,
          unselectedLabelColor: OwnColors.C989898,
          // indicatorPadding: EdgeInsets.all(8.0),
          indicatorWeight: 2.0,

          onTap: (index) {
            currentFirstTab = index ;
            searchEdit.text = "";
            setState(() {

            });
          },
          tabs: [
            Tab(child: Text(S().personal_my_travel_posted,
              style: GoogleFonts.inter(
                  fontWeight: currentFirstTab == 0? FontWeight.w600 : FontWeight.w500,
                  fontSize: 16.nsp
              ),
            ),),
            Tab(child: Text(S().personal_my_travel_not_posted,
              style: GoogleFonts.inter(
                  fontWeight: currentFirstTab == 1? FontWeight.w600 : FontWeight.w500,


                  fontSize: 16.nsp
              ),
            ),),

          ],
        ),
      ),
    );
  }


  Widget travelPage(){
    return Expanded(
      child: Container(
        width: 0.9.sw,
        child: EasyRefresh.builder(

            controller: refreshController,
            // header: ListenerHeader(
            //   triggerOffset: 100,
            //   // listenable: _listenable,
            //   safeArea: false,
            // ),
            header: ClassicHeader(
                dragText: '',
                armedText: '',
                readyText: '',
                processingText: '',
                processedText: '',
                noMoreText: '',
                failedText: '',
                messageText: '',
                iconTheme: IconThemeData(
                    color: OwnColors.CTourOrange
                )

            ),
            footer: NotLoadFooter(),

            onRefresh: () async {
              print("onRefresh");

              getMyArticleAPI();
            },
            onLoad: () async {
              print("onLoad");
              refreshController.finishLoad(IndicatorResult.noMore);

            },

            childBuilder: (context, physics) {
              return ListView.builder(
                  physics:physics,
                  shrinkWrap: false,
                  padding: const EdgeInsets.only(left: 0.0),
                  itemCount: postShowList.length,
                  scrollDirection: Axis.vertical,
                  controller: listScrollController1,
                  keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
                  itemBuilder: (context, index) {
                    return
                      index == postShowList.length -1 ?
                      Column(
                        children: [
                          travelCard(postShowList[index], index),
                          ScreenSize().createBottomPad(context)
                        ],
                      )
                          :
                      travelCard(postShowList[index], index);
                  });
            }
        ),
      ),
    );
  }
  Widget travelPage2(){
    return Expanded(
      child: Container(
        width: 0.9.sw,
        child:  ListView.builder(
            physics: BouncingScrollPhysics(
                parent: AlwaysScrollableScrollPhysics()),
            shrinkWrap: true,
            itemCount: draftShowList.length,
            scrollDirection: Axis.vertical,
            padding:const EdgeInsets.all(0),
            keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
            controller: listScrollController2,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: Padding(
                  padding: const EdgeInsets.only(left: 0.0, right: 0),
                  child:
                  index == draftShowList.length -1 ?
                  Column(
                    children: [
                      travelDraftard(draftShowList[index], index),
                      ScreenSize().createBottomPad(context)
                    ],
                  ):
                  travelDraftard(draftShowList[index], index),
                ),
              );
            }),
      ),
    );
  }

  Widget travelCard(TravelClass travelClass, int index) {
    return Stack(
      children: [
        TravelCard().recommendCard(context, travelClass, index, travelClick,0.9 * ScreenSize().getWidth(context),),
        // new Positioned.fill(
        //     child: new Material(
        //       color: Colors.transparent,
        //       child: new GestureDetector(
        //
        //         onTap: ()  {
        //           print("travelClick");
        //           travelClick(travelClass);
        //
        //         },
        //       ),
        //     )),
        Positioned(
          right: 10,
          top: 5,
          left:10,
          child: Column(
            children: [
              HorizontalSpace().create(context, 0.015),

              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Spacer(),
                  GestureDetector(
                    onTap:(){

                      delArticalID = travelClass.id;
                      WarningDialog.showIOSAlertDialog2(context, S().personal_my_travel_del_title, S().personal_my_travel_del_hint,
                          S().delete, deleteArtical);


                      print("deleteArtical");
                    },
                    child:Container(
                      height: 0.03 .sh,
                      child: AspectRatio(
                        aspectRatio: 1,
                        child: Padding(
                          padding: EdgeInsets.only(top:0.005 *
                              ScreenSize().getHeight(context), bottom: 0.005 *
                              ScreenSize().getHeight(context)),
                          child: Icon(SFSymbols.xmark, color: OwnColors.white,size: 20,),
                        ),
                      ),
                    ),

                  ),
                ],
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget travelDraftard(TravelClass travelClass, int index) {
    return Stack(
      children: [
        Container(
          width: 0.9 * ScreenSize().getWidth(context),


          child: Container(
            decoration: BoxDecoration(
              color: OwnColors.white,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(15),
                topLeft: Radius.circular(15),
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [

                HorizontalSpace().create(context, 0.015),
                AspectRatio(
                  aspectRatio: 16/9,
                  child: Container(
                    width: 0.9 * ScreenSize().getWidth(context),
                    child: Stack(
                      children: [
                        travelClass.cover_image.contains("http")? CachedNetworkImage(
                          imageUrl: travelClass.cover_image,
                          fit: BoxFit.fill,
                          placeholder: (context, url) => AspectRatio(
                            aspectRatio: 1,
                            child: FractionallySizedBox(
                              heightFactor: 0.5,
                              widthFactor: 0.5,
                              child: SpinKitRing(
                                lineWidth:5,
                                color:
                                OwnColors.tran ,
                              ),
                            ),
                          ),
                          errorWidget: (context, url, error) => Icon(
                            Icons.error,
                            color: OwnColors.black.withOpacity(0.5),
                          ),
                          imageBuilder: (context, imageProvider) =>
                              Container(
                                width: 0.9 * ScreenSize().getWidth(context),
                                decoration: BoxDecoration(
                                  color: OwnColors.tran,
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(15.0)),
                                  image: DecorationImage(
                                      image: imageProvider,
                                      fit: BoxFit.cover),
                                ),
                              ),
                          // width: MediaQuery.of(context).size.width,
                        ):
                        Positioned.fill(child: ClipRRect(borderRadius: BorderRadius.all(Radius.circular(15)),child: Container(
                          color: OwnColors.CF7F7F7,
                          child: Column(
                            children: [
                              Spacer(),
                              Container(height: 0.9 * ScreenSize().getWidth(context) / 16 * 9 /3,child: Icon(SFSymbols.photo_fill, color: OwnColors.CE6E1E1,size: ScreenSize().getWidth(context)*0.20)),
                              Spacer(),
                            ],
                          ),
                        )))

                        ,


                        Positioned(
                          bottom: 0,
                          // right: 5,
                          child: Container(
                            height: 0.13 * ScreenSize().getHeight(context),
                            width: 0.9 * ScreenSize().getWidth(context),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(15),
                                    bottomRight: Radius.circular(15)),
                                gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    OwnColors.C797979.withOpacity(0.0),
                                    OwnColors.C787878.withOpacity(0.0073),
                                    OwnColors.C3E3E3E.withOpacity(0.3391),
                                    Colors.black.withOpacity(0.7),
                                  ],
                                )),
                            child: Center(
                              child: Container(
                                width: 0.80 * ScreenSize().getWidth(context),
                                child: Column(
                                  mainAxisAlignment:
                                  MainAxisAlignment.start,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    Spacer(
                                      flex: 10,
                                    ),
                                    Text(
                                      travelClass.type == "1"?
                                      travelClass.duration.toString() + S().home_filter_type2_string :
                                      travelClass.type == "2"? S().home_filter_type3 :
                                      S(). home_filter_type4,

                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style:GoogleFonts.inter (
                                          fontSize: 12.nsp,
                                          fontWeight: FontWeight.w300,
                                          color: OwnColors.white),
                                    ),
                                    Text(
                                      travelClass.title,

                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                      style:GoogleFonts.inter (
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16.nsp,
                                          color: OwnColors.white),
                                    ),
                                    HorizontalSpace().create(context, 0.008),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          height: 0.1 * ScreenSize().getHeight(context),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(15),
                                  topRight: Radius.circular(15)),
                              gradient: LinearGradient(
                                begin: Alignment.bottomCenter ,
                                end: Alignment.topCenter,
                                colors: [
                                  OwnColors.C797979
                                      .withOpacity(0.0),
                                  OwnColors.C787878
                                      .withOpacity(0.0073),
                                  OwnColors.C3E3E3E
                                      .withOpacity(0.3391),
                                  Colors.black.withOpacity(0.7),
                                ],
                              )),

                        ),

                      ],
                    ),
                  ),
                ),
                HorizontalSpace().create(context, 0.01),



              ],
            ),
          ),
        ),
        new Positioned.fill(
            child: new Material(
              color: Colors.transparent,
              child: new GestureDetector(

                onTap: ()  {
                  print("travelClick");
                  travelDraftClick(travelClass);

                },
              ),
            )),
        Positioned(
          right: 10,
          top: 5,
          left:10,
          child: Column(
            children: [
              HorizontalSpace().create(context, 0.015),

              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Spacer(),
                  GestureDetector(
                    onTap:(){

                      delArticalID = travelClass.id;
                      WarningDialog.showIOSAlertDialog2(context, S().personal_my_travel_del_title, S().personal_my_travel_del_hint,
                          S().confirm, deleteDraftTravel);



                      print("deleteDraftTravel");
                    },
                    child:Container(
                      height: 0.03 * ScreenSize().getHeight(context),
                      child: AspectRatio(
                        aspectRatio: 1,
                        child: Padding(
                          padding: EdgeInsets.only(top:0.005 *
                              ScreenSize().getHeight(context), bottom: 0.005 *
                              ScreenSize().getHeight(context)),
                          child: Icon(SFSymbols.xmark, color: OwnColors.white,size: 20,)
                        ),
                      ),
                    ),

                  ),
                ],
              ),
            ],
          ),
        )

      ],
    );
  }





  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }
  void addClick( ){
    PushNewScreen().normalPush(context, NewTravelPage());
  }

  Future<void> deleteDraftTravel(  ) async {
    var db = new DatabaseHelper();
    await db.deleteTravel( int.parse(delArticalID));
    getDraft();
  }
  void travelClick(TravelClass travelClass){
    PushNewScreen().normalPush(context, TravelPage(travelClass:travelClass,edit: true));

  }  void travelDraftClick(TravelClass travelClass){
    PushNewScreen().normalPush(context, NewTravelPage(travelClass:travelClass));

  }

  void tourClick(GuildClass guildClass){

  }

  void updateAll(){
    refreshController.callRefresh();
    getMyArticleAPI();
    getDraft();

  }

  void getMyArticleAPI( ) async {
    print("getMyArticleAPI");

    ResultData resultData = await AppApi.getInstance().getArticleList(context, true,
        SharePre.prefs.getString(GDefine.user_id),
        SharePre.prefs.getString(GDefine.user_id),"","","","","","","","false");
    if (resultData.isSuccess()) {
      print(resultData.data.toString());
      postList.clear();
      for (Map<String, dynamic> a in resultData.data) {
        TravelClass travelClass = TravelClass.fromJson(a);
        travelClass.sectionTag = new List();

        print("name : " + TravelClass.fromJson(a).title.toString());
        print("contentList : " + TravelClass.fromJson(a).contentList.toString());
        print("contentList : " + a["content_list"].toString());
        print("DDDDDD" + jsonDecode(a["content_list"].toString()).toString());
        if(a["content_list"] != null) {
          List<dynamic> content_list = jsonDecode(a["content_list"].toString());
          travelClass.contentListDynamic = new List();


          List<TravelSubClass> subList = new List();
          for (Map<String, dynamic> sub in content_list) {
            TravelSubClass travelSubClass = new TravelSubClass();
            List<dynamic> content_list2 = sub["contentList"];
            List<TravelContentClass> travelContentClassList = new List();

            // if(content_list2.length>0) {
              Map<String, dynamic> dy = new Map();
              dy["title"] =
              sub["title"].toString() == "null" ? "" : sub["title"].toString();
              dy["type"] = "0";
              dy["description"] = "";
              dy["google"] = "";
              dy["image"] = [];
              travelClass.contentListDynamic.add(dy);
            // }

            for (Map<String, dynamic> subsub in content_list2) {
              TravelContentClass travelContentClass = new TravelContentClass();
              travelContentClass.image = List<String>.from(subsub["image"]);
              travelContentClass.image.removeWhere((element) =>
              element == "" || element == "null");
              travelContentClass.title = subsub["title"];
              travelContentClass.description =
              subsub["description"].toString() == "null"
                  ? ""
                  : subsub["description"].toString();
              travelContentClass.id = subsub["id"].toString();
              travelContentClass.google =
              subsub["google"].toString() == "null" ? "" : subsub["google"]
                  .toString();
              travelContentClass.key = new GlobalKey();

              travelContentClassList.add(travelContentClass);

              Map<String,dynamic> dy2 = new Map();
              dy2["title"] = travelContentClass.title;
              dy2["type"] = "1";
              dy2["description"] = travelContentClass.description;
              dy2["google"] = travelContentClass.google;
              dy2["image"] = List<String>.from(subsub["image"]);

              travelClass.contentListDynamic.add(dy2);

            }
            travelSubClass.title =
            sub["title"].toString() == "null" ? "" : sub["title"].toString();
            travelSubClass.id = sub["id"].toString();
            travelSubClass.key = new GlobalKey();
            if(sub["title"]!="" && sub["title"].toString() != "null") {
              travelClass.sectionTag.add(new SectionClass(title: sub["title"], key:travelSubClass.key  ));
            }

            if (travelContentClassList.isNotEmpty) {
              travelSubClass.content = travelContentClassList;
            }

            subList.add(travelSubClass);
          }

          travelClass.contentList = subList;


          postList.add(travelClass);
        }

        print("travelClass.contentListDynamic " +travelClass.contentListDynamic.toString());

      }

      postShowList.clear();
      postShowList.addAll(postList);


      setState(() {
        print("getTouristAPI");
        loadingBool = false;

      });
      print (refreshController.controlFinishLoad);
      refreshController.finishRefresh();
      refreshController.resetFooter();

    } else {
      WarningDialog.showIOSAlertDialog(context, S().download_fail, resultData.msg,S().confirm);
    }
  }

  void getDraft( ) async {
    var db = new DatabaseHelper();
    List<TravelModel> c = await db.getTravelByTouristID(SharePre.prefs.getString(GDefine.user_id));
    print(c.length);
    draftList.clear();

    for (TravelModel travelModel in c){
      draftList.add(new TravelClass(
        id: travelModel.id.toString(),
        cover_image:
        travelModel.cover_image,
        title: travelModel.title,
        date: "",
        duration: travelModel.duration,
        views: "",
        description: travelModel.description,

        type:  travelModel.type,
        tag: jsonDecode(travelModel.tag),
        contentListDynamic :  jsonDecode(travelModel.contentList),

      ));

    }
    draftShowList.clear();
    draftShowList.addAll(draftList);
    setState(() {

    });
  }

  void deleteArtical( ) async {

    ResultData resultData = await AppApi.getInstance().delArticle(context, true, SharePre.prefs.getString(GDefine.user_id),
        delArticalID);
    if (resultData.isSuccess()) {
      setState(() {

      });
      updateAll();

    }
    else {
      WarningDialog.showIOSAlertDialog(context, S().delete_fail_title,  S().delete_fail_hint,S().confirm);
    }
  }



  Future<void> searchTravel1() async {
    postShowList.clear();


    if(searchEdit.text == ""){
      postShowList.addAll(postList);

    }else {
      List<TravelClass> temp = postList.where((element) =>
      element.title.toLowerCase().contains(searchEdit.text.toLowerCase()) ||
          element.guilder_name.toLowerCase().contains(searchEdit.text.toLowerCase()) ||
          element.tag.indexWhere((cc) => cc.toString().toLowerCase().contains(searchEdit.text.toLowerCase())) != -1).toList();
      postShowList.addAll(temp);

    }

    setState(() {

    });


  }
  Future<void> searchTravel2() async {
    print("searchTravel2");

    print("searchEdit.text" + searchEdit.text);
    draftShowList.clear();


    if(searchEdit.text == ""){
      draftShowList.addAll(draftList);

    }else {
      List<TravelClass> temp = draftList.where((element) =>
      element.title.toLowerCase().contains(searchEdit.text.toLowerCase()) ||

          element.tag.indexWhere((cc) => cc.toString().toLowerCase().contains(searchEdit.text.toLowerCase())) != -1).toList();
      draftShowList.addAll(temp);

    }

    setState(() {

    });
    //
    // for(TravelArticleClass t in travelList){
    //   print(t.title);
    //   print(t.type);
    //   print(chapter);
    //   print("------");
    //   Map<String,dynamic> contentSub=  <String,dynamic>{};
    //   if(t.type == "0"){
    //     if(chapter){
    //       contentChapter["contentList"]  =  new List.from(contentSubList);
    //       contentList.add(new Map.from(contentChapter));
    //       contentSubList.clear();
    //       contentChapter.clear();
    //       contentChapter["title"] = t.title;
    //       chapter = true;
    //     }else {
    //       if(contentSubList.length != 0 && contentChapter.length!= 0  ){
    //         contentChapter["contentList"]  =  new List.from(contentSubList);
    //         contentList.add(new Map.from(contentChapter));
    //       }
    //       contentSubList.clear();
    //       contentChapter.clear();
    //       contentChapter["title"] = t.title;
    //       chapter = true;
    //
    //     }
    //   } else {
    //     if(chapter){
    //       contentSub["title"] = t.title;
    //       contentSub["description"] = t.description;
    //       contentSub["google"] = t.google;
    //       contentSub["image"] = t.image;
    //       contentSubList.add(new Map.from(contentSub));
    //     }else {
    //       contentChapter.clear();
    //       contentChapter["title"] = "";
    //       contentSub["title"] = t.title;
    //       contentSub["description"] = t.description;
    //       contentSub["google"] = t.google;
    //       contentSub["image"] = t.image;
    //       contentSubList.add(new Map.from(contentSub));
    //
    //       // setAPI = false;
    //       // WarningDialog.showIOSAlertDialog(context, S().personal_my_travel_warning_structure_title,
    //       //     S().personal_my_travel_warning_structure_hint, S().confirm);
    //     }
    //   }
    // }


  }

}



