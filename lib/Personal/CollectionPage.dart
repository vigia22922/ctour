import 'dart:async';
import 'dart:convert';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/GuildCard.dart';
import 'package:ctour/CreateWidget/TravelCard.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Guild/GuildClass.dart';
import 'package:ctour/Guild/GuildReviewPage.dart';
import 'package:ctour/Guild/GuilderPage.dart';
import 'package:ctour/Travel/TravelPage.dart';
import 'package:ctour/tool/DateTools.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:easy_refresh/easy_refresh.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:focus_detector/focus_detector.dart';
import 'package:google_fonts/google_fonts.dart';
import '../Travel/TravelClass.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

import '../CreateWidget/HorizontalSpace.dart';
import '../CreateWidget/TopBar.dart';
import '../res.dart';
import '../tool/OwnColors.dart';
import '../tool/ScreenSize.dart';
import 'package:flutter/material.dart';
import 'package:flutter_star/flutter_star.dart';

class CollectionPage extends StatefulWidget {



  const CollectionPage({
    Key key,


  }) : super(key: key);

  @override
  State createState() {
    return _CollectionPageState();
  }
}

class _CollectionPageState extends State<CollectionPage> with TickerProviderStateMixin {



  TextEditingController searchEdit= TextEditingController();
  String searchText1 = "";

  bool loadingBool = true;
  List<TravelClass> hotTravelList = new List();
  List<TravelClass> showTravelList = new List();

  List<GuildClass> tourList = new List();
  List<GuildClass> showTourList = new List();
  TabController _tabFirstController;
  int currentFirstTab = 0;
  EasyRefreshController refreshController1 = new EasyRefreshController( ) ;
  EasyRefreshController refreshController2 = new EasyRefreshController( ) ;
  final ScrollController listScrollController1 = ScrollController();
  final ScrollController listScrollController2 = ScrollController();
  FocusNode searchFocus = new FocusNode();
  @override
  void initState() {
    super.initState();
    // if (Platform.isAndroid) {
    SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(
        statusBarColor: OwnColors.tran, //设置为透明
        statusBarIconBrightness: Brightness.dark);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);

    refreshController1 = EasyRefreshController(
      controlFinishRefresh: true,
      controlFinishLoad: true,
    );
    refreshController2 = EasyRefreshController(
      controlFinishRefresh: true,
      controlFinishLoad: true,
    );

    // listScrollController1.addListener(() {Tools().dismissK(context);});
    // listScrollController2.addListener(() {Tools().dismissK(context);});
    _tabFirstController = new TabController(vsync: this, length: 2);

    Future.delayed(Duration(milliseconds: 100), () {
      // 5s over, navigate to a new page
      refreshController1.callRefresh();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap:(){
        Tools().dismissK(context);
        print("onTap");
      },
      child: Scaffold(
          backgroundColor: OwnColors.white,
          body: SafeArea(
              child: FocusDetector(
                onFocusGained: (){
                  getTouristAPI();
                  getArticleCollectListAPI();
                },
                child: Center(
                  child: Container(
                    height: 1.sh,
                    width: ScreenSize().getWidth(context)*1,
                    child: Column(
                      children: [
                        HorizontalSpace().create(context, 0.01),
                        Container(
                          height: 0.05.sh,
                          child: Topbar().titleCreate(context,S().personal_collection, ScreenSize().getHeight(context) * 0.073,  backClick,false ),
                        ),
                        HorizontalSpace().create(context, 0.015),
                        Container(
                          height: 0.05.sh,
                          width: ScreenSize().getWidth(context) * 0.88,
                          // height: height,
                          decoration: BoxDecoration(
                            color: OwnColors.white,
                            border: Border.all(color: OwnColors.CC8C8C8),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(30),
                                topRight: Radius.circular(30),
                                bottomLeft: Radius.circular(30),
                                bottomRight: Radius.circular(30)),
                          ),

                          alignment: Alignment.center,
                          child: Row(
                            children: [
                              GestureDetector(
                                onTap: (){
                                  FocusScope.of(context).requestFocus(searchFocus);
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 12.0, bottom: 4.0),
                                  child: Container(
                                      height: 0.027 .sh,
                                      child: Icon(SFSymbols.search, color: OwnColors.black)),
                                ),
                              ),
                              Expanded(
                                child: Container(
                                  height: 0.027.sh,
                                  child: TextField(
                                    focusNode: searchFocus,
                                    controller: searchEdit,
                                    onChanged: (text) {
                                      print("onChanged");
                                      if (searchText1 == "") {
                                        searchText1 = text;
                                        if(currentFirstTab == 0){
                                          searchGuild();
                                        }else {
                                          searchTravel();
                                        }

                                      } else {
                                        if (searchText1 != text) {
                                          searchText1 = text;
                                          if(currentFirstTab == 0){
                                            searchGuild();
                                          }else {
                                            searchTravel();
                                          }
                                        }
                                      }
                                    },
                                    autocorrect: false,
                                    decoration: InputDecoration(
                                        fillColor: OwnColors.tran,
                                        contentPadding: EdgeInsets.symmetric(
                                            horizontal: 10, vertical: 0),
                                        //Change this value to custom as you like
                                        // isDense: true,
                                        hintText: currentFirstTab == 1? S().personal_collection_search_hint1: S().personal_collection_search_hint2,
                                        filled: true,
                                        hintStyle: TextStyle(
                                            fontSize: 16.nsp, color:OwnColors.C989898),
                                        border: OutlineInputBorder(
                                            borderRadius:
                                            new BorderRadius.circular(20.0),
                                            borderSide: BorderSide.none)),
                                    style: TextStyle(
                                        color: Colors.black.withOpacity(1),
                                        fontSize: ScreenUtil().setSp(16)),
                                    maxLines: 1,
                                    keyboardType: TextInputType.text,
                                    obscureText: false,
                                  ),
                                ),
                              ),

                            ],
                          ),
                        ),
                        HorizontalSpace().create(context, 0.01),
                        _tabFirst(),
                        currentFirstTab == 0?guilderPage():travelPage()

                      ],
                    ),
                  ),
                ),
              )
          )),
    );
  }
  Widget _tabFirst() {
    return Container(
      width: 0.92.sw,
      child: DefaultTabController(
        length: 2,
        child: TabBar(
          controller: _tabFirstController,
          isScrollable: false,
          indicatorColor: OwnColors.CTourOrange,
          labelColor: OwnColors.CTourOrange,
          unselectedLabelColor: OwnColors.C989898,
          // indicatorPadding: EdgeInsets.all(8.0),
          indicatorWeight: 2.0,


          onTap: (index) {
            currentFirstTab = index ;

            searchEdit.text = "";
            setState(() {

            });
          },
          tabs: [
            Tab(child: Text(S().personal_collection_tab1,

              style: TextStyle(
                fontSize: 16.nsp
              ),
            ),),
            Tab(child: Text(S().personal_collection_tab2,
              style: TextStyle(
                  fontSize: 16.nsp
              ),
            ),),

          ],
        ),
      ),
    );
  }


  Widget guilderPage(){
    return                     Expanded(
      child: Container(
        width:  0.88*ScreenSize().getWidth(context),

        child:


        EasyRefresh(
          // key: _refresherKey,

          controller: refreshController1,
          // enablePullUp: false,
          // enablePullDown: true,

          header: ClassicHeader(
              dragText: '',
              armedText: '',
              readyText: '',
              processingText: '',
              processedText: '',
              noMoreText: '',
              failedText: '',
              messageText: '',
              iconTheme: IconThemeData(
                  color: OwnColors.CTourOrange
              )

          ),
          // physics: BouncingScrollPhysics(),
          footer: NotLoadFooter(),
          onRefresh: () async {
            getTouristAPI();

          },
          child: ListView.builder(
              // physics: BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
              shrinkWrap: false,
              itemCount: showTourList.length,
              scrollDirection: Axis.vertical,
              controller: listScrollController1,
              keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.only(
                      top: 0.0 ),
                  child: GestureDetector(
                      child:

                      Padding(
                        padding: const EdgeInsets.only(left:0.0,right: 0),
                        child: GuildCard().tourCard(context,showTourList[index],index,tourClick),),
                      onTap: () => {}),

                );
              }

          ),
        )
        ,
      ),
    );
  }
  Widget travelPage(){
    return Expanded(
      child:  Container(
        width: 0.88 * ScreenSize().getWidth(context),
        child:   EasyRefresh(
          // key: _refresherKey,

          controller: refreshController2,
          // enablePullUp: false,
          // enablePullDown: true,

          header: ClassicHeader(
              dragText: '',
              armedText: '',
              readyText: '',
              processingText: '',
              processedText: '',
              noMoreText: '',
              failedText: '',
              messageText: '',
              iconTheme: IconThemeData(
                  color: OwnColors.CTourOrange
              )

          ),
          // physics: BouncingScrollPhysics(),
          footer: NotLoadFooter(),
          onRefresh: () async {
            getArticleCollectListAPI();

          },
          // onLoading: () async {
          //   print("onLoading");
          //   // monitor fetch data from network
          //   await Future.delayed(Duration(milliseconds: 300));
          // },
              child: ListView.builder(
              // physics: BouncingScrollPhysics(
              //     parent: AlwaysScrollableScrollPhysics()),
              shrinkWrap: false,
              itemCount: showTravelList.length,
              scrollDirection: Axis.vertical,
              controller: listScrollController2,
                  keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 0.0, right: 0),
                    child: TravelCard().recommendCard(context,
                        showTravelList[index], index,recommendClick,0.88 * ScreenSize().getWidth(context),),
                  ),
                );
              }),
            ),
      ),
    );
  }

  // Widget travelCard(TravelClass travelClass, int index) {
  //   return GestureDetector(
  //     onTap: () => {recommendClick(travelClass)},
  //     child: Container(
  //       width: 0.9 * ScreenSize().getWidth(context),
  //
  //
  //       child: Container(
  //         decoration: BoxDecoration(
  //           color: OwnColors.white,
  //           borderRadius: BorderRadius.only(
  //             topRight: Radius.circular(15),
  //             topLeft: Radius.circular(15),
  //             bottomLeft: Radius.circular(15),
  //             bottomRight: Radius.circular(15),
  //           ),
  //         ),
  //         child: Column(
  //           mainAxisAlignment: MainAxisAlignment.spaceAround,
  //           crossAxisAlignment: CrossAxisAlignment.center,
  //           children: [
  //             HorizontalSpace().create(context, 0.015),
  //             AspectRatio(
  //               aspectRatio: 16/9,
  //               child: Container(
  //                 width: 0.9 * ScreenSize().getWidth(context),
  //                 child: Stack(
  //                   children: [
  //                     CachedNetworkImage(
  //                       imageUrl: travelClass.cover_image,
  //                       fit: BoxFit.fill,
  //                     placeholder: (context, url) => AspectRatio(
  //                       aspectRatio: 1,
  //                       child: FractionallySizedBox(
  //                         heightFactor: 0.5,
  //                         widthFactor: 0.5,
  //                         child: SpinKitRing(
  //                           lineWidth:5,
  //                           color:
  //                           OwnColors.tran ,
  //                         ),
  //                       ),
  //                     ),
  //                       errorWidget: (context, url, error) => Icon(
  //                         Icons.error,
  //                         color: OwnColors.black.withOpacity(0.5),
  //                       ),
  //                       imageBuilder: (context, imageProvider) =>
  //                           Container(
  //                             width: 0.9 * ScreenSize().getWidth(context),
  //                             decoration: BoxDecoration(
  //                               color: OwnColors.black,
  //                               borderRadius: BorderRadius.all(
  //                                   Radius.circular(15.0)),
  //                               image: DecorationImage(
  //                                   image: imageProvider,
  //                                   fit: BoxFit.cover),
  //                             ),
  //                           ),
  //                       // width: MediaQuery.of(context).size.width,
  //                     ),
  //                     // Positioned(
  //                     //   right: 10,
  //                     //   top: 10,
  //                     //   child: Container(
  //                     //       height: 0.03 *
  //                     //           ScreenSize().getHeight(context),
  //                     //       child: Image.asset(Res.favorite)),
  //                     // ),
  //                     Positioned(
  //                       bottom: 0,
  //                       // right: 5,
  //                       child: Container(
  //                         height: 0.1 *
  //                             ScreenSize().getHeight(context),
  //                         width: 0.88 *
  //                             ScreenSize().getWidth(context),
  //                         decoration: BoxDecoration(
  //                             borderRadius: BorderRadius.only(
  //                                 bottomLeft: Radius.circular(15),
  //                                 bottomRight: Radius.circular(15)),
  //                             gradient: LinearGradient(
  //                               begin: Alignment.topCenter,
  //                               end: Alignment.bottomCenter,
  //                               colors: [
  //                                 OwnColors.C797979
  //                                     .withOpacity(0.0),
  //                                 OwnColors.C787878
  //                                     .withOpacity(0.0073),
  //                                 OwnColors.C3E3E3E
  //                                     .withOpacity(0.3391),
  //                                 Colors.black.withOpacity(0.7),
  //                               ],
  //                             )),
  //                         child: Center(
  //                           child: Container(
  //                             width: 0.83 *
  //                                 ScreenSize().getWidth(context),
  //                             child: Column(
  //                               mainAxisAlignment:
  //                               MainAxisAlignment.start,
  //                               crossAxisAlignment:
  //                               CrossAxisAlignment.start,
  //                               children: [
  //                                 Spacer(
  //                                   flex: 10,
  //                                 ),
  //                                 AutoSizeText(
  //                                   travelClass.type == "1"?
  //                                   travelClass.duration.toString() + S().home_filter_type2_string :
  //                                   travelClass.type == "2"? S().home_filter_type3 :
  //                                   S(). home_filter_type4,
  //                                   minFontSize: 12,
  //                                   overflow: TextOverflow.ellipsis,
  //                                   maxLines: 1,
  //                                   style: TextStyle(
  //                                       fontSize: 12.nsp,
  //                                       color: OwnColors.white),
  //                                 ),
  //                                 AutoSizeText(
  //                                   travelClass.title,
  //                                   minFontSize: 16,
  //                                   overflow: TextOverflow.ellipsis,
  //                                   maxLines: 1,
  //                                   style: TextStyle(
  //                                       fontSize: 18.nsp,
  //                                       color: OwnColors.white),
  //                                 ),
  //                                 HorizontalSpace()
  //                                     .create(context, 0.008),
  //                               ],
  //                             ),
  //                           ),
  //                         ),
  //                       ),
  //                     ),
  //
  //                     Container(
  //                       height: 0.1 *
  //                           ScreenSize().getHeight(context),
  //                       decoration: BoxDecoration(
  //                           borderRadius: BorderRadius.only(
  //                               topLeft: Radius.circular(15),
  //                               topRight: Radius.circular(15)),
  //                           gradient: LinearGradient(
  //                             begin: Alignment.bottomCenter ,
  //                             end: Alignment.topCenter,
  //                             colors: [
  //                               OwnColors.C797979
  //                                   .withOpacity(0.0),
  //                               OwnColors.C787878
  //                                   .withOpacity(0.0073),
  //                               OwnColors.C3E3E3E
  //                                   .withOpacity(0.3391),
  //                               Colors.black.withOpacity(0.7),
  //                             ],
  //                           )),
  //                       child: Padding(
  //                         padding: const EdgeInsets.only(top:5.0,left: 10),
  //                         child: Row(
  //                           mainAxisAlignment: MainAxisAlignment.start,
  //                           crossAxisAlignment: CrossAxisAlignment.start,
  //                           children: [
  //                             SizedBox(
  //                               height: 0.045 *  ScreenSize().getHeight(context),
  //                               child: AspectRatio(
  //                                 aspectRatio: 1,
  //                                 child: CachedNetworkImage(
  //                                     placeholder: (context, url) => AspectRatio(
  //                                       aspectRatio: 1,
  //                                       child: FractionallySizedBox(
  //                                         heightFactor: 0.5,
  //                                         widthFactor: 0.5,
  //                                         child: SpinKitRing(
  //                                           lineWidth:5,
  //                                           color:
  //                                           OwnColors.tran ,
  //                                         ),
  //                                       ),
  //                                     ),
  //                                   errorWidget:
  //                                       (context, url, error) =>
  //                                       Icon(
  //                                         Icons.error,
  //                                         color: Colors.red,
  //                                       ),
  //                                   imageUrl: travelClass.guilder_avatar,
  //                                   fit: BoxFit.fitHeight,
  //                                   imageBuilder: (context,
  //                                       imageProvider) =>
  //                                       Container(
  //                                         // width: 80.0,
  //                                         // height: 80.0,
  //                                         decoration: BoxDecoration(
  //                                           shape: BoxShape.circle,
  //                                           border: Border.all(width: 1,color: OwnColors.CC8C8C8
  //
  //                                           ),
  //                                           image: DecorationImage(
  //                                               image: imageProvider,
  //                                               fit: BoxFit.cover),
  //                                         ),
  //                                       ),
  //                                   // width: MediaQuery.of(context).size.width,
  //                                   // placeholder: (context, url) => new CircularProgressIndicator(),
  //                                   // placeholder: new CircularProgressIndicator(),
  //                                 ),
  //                               ),
  //                             ),
  //
  //                             VerticalSpace().create(context, 0.017),
  //                             Container(
  //                               child: Column(
  //                                 mainAxisAlignment: MainAxisAlignment.start,
  //                                 crossAxisAlignment: CrossAxisAlignment.start,
  //                                 mainAxisSize: MainAxisSize.min,
  //                                 children: [
  //                                   Text(travelClass.guilder_name,
  //                                     maxLines: 1,
  //
  //                                     overflow: TextOverflow.ellipsis,
  //                                     style: TextStyle(
  //                                         fontSize: 16.nsp,
  //                                         color: OwnColors.white
  //                                     ),),
  //                                   Row(
  //                                     mainAxisAlignment: MainAxisAlignment.end,
  //                                     crossAxisAlignment: CrossAxisAlignment.end,
  //                                     children: [
  //                                       Container(
  //                                         height: 0.02 * ScreenSize().getHeight(context),
  //                                         child: Image.asset(Res.star),
  //                                       ),
  //                                       Container(width: 2,),
  //                                       Text(
  //                                         travelClass.guilder_rate  ,
  //
  //                                         style: TextStyle(
  //                                             fontSize: 12.nsp,
  //                                             color: OwnColors.white,
  //                                             fontWeight: FontWeight.bold
  //                                         ),
  //                                       ),
  //                                       Text(
  //                                         " (" + double.parse(travelClass.guilder_comment).toStringAsFixed(0)+S().home_rate_number+")",
  //
  //                                         style: TextStyle(
  //                                           fontSize: 12.nsp,
  //                                           color: OwnColors.white,
  //
  //                                         ),
  //                                       ),
  //                                     ],
  //                                   ),
  //                                 ],
  //                               ),
  //                             ),
  //                           ],
  //                         ),
  //                       ),
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //             ),
  //             HorizontalSpace().create(context, 0.01),
  //             Container(
  //               width: 0.83 * ScreenSize().getWidth(context),
  //               height: 0.025 * ScreenSize().getHeight(context),
  //               child: Row(
  //                 mainAxisAlignment: MainAxisAlignment.start,
  //                 crossAxisAlignment: CrossAxisAlignment.center,
  //                 children: [
  //
  //                   Padding(
  //                     padding: const EdgeInsets.only(
  //                         top: 2.0, bottom: 2.0, right: 4),
  //                     child: Image.asset(Res.peoples),
  //                   ),
  //                   AutoSizeText(
  //                     travelClass.views + " " + S().travel_views,
  //                     minFontSize: 10,
  //                     style:GoogleFonts.inter(
  //                       fontSize: 10.nsp,
  //                       color: OwnColors.black,
  //                       fontWeight: FontWeight.w500,
  //                     ),
  //                   ),
  //                   Padding(
  //                     padding: const EdgeInsets.only(
  //                         top: 2.0, bottom: 2.0, right: 4, left: 8),
  //                     child: Image.asset(Res.chat),
  //                   ),
  //                   Expanded(
  //                     child: AutoSizeText(
  //                       travelClass.comment_num +
  //                           " " +
  //                           S().travel_comment,
  //                       minFontSize: 10,
  //                       style:GoogleFonts.inter(
  //                         fontSize: 10.nsp,
  //                         color: OwnColors.black,
  //                         fontWeight: FontWeight.w500,
  //                       ),
  //                     ),
  //                   ),
  //                   AutoSizeText(
  //                     DateTools().intlYMD( travelClass.date),
  //                     minFontSize: 10,
  //                     style: TextStyle(
  //                       fontSize: 12.nsp,
  //                       color: OwnColors.CCCCCCC,
  //                     ),
  //                   )
  //                 ],
  //               ),
  //             ),
  //
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }


  // Widget tourCard(GuildClass guildClass,int index) {
  //
  //   return Stack(
  //     children: [
  //       Container(
  //         width: double.infinity,
  //
  //
  //
  //         child: Container(
  //           decoration: BoxDecoration(
  //             color: OwnColors.white,
  //             borderRadius: BorderRadius.only(
  //               topRight: Radius.circular(15),
  //               topLeft: Radius.circular(15),
  //               bottomLeft: Radius.circular(15),
  //               bottomRight: Radius.circular(15),
  //             ),
  //
  //           ),
  //           child: Column(
  //             children: [
  //               HorizontalSpace().create(context, 0.01),
  //               Row(
  //                 mainAxisAlignment: MainAxisAlignment.center,
  //                 crossAxisAlignment: CrossAxisAlignment.center,
  //                 children: [
  //                   Align(
  //                     alignment: Alignment.topRight,
  //                     child: Container(
  //                       height: 0.174 *  ScreenSize().getHeight(context),
  //                       child: Center(
  //                         child: AspectRatio(
  //                           aspectRatio: 1.384,
  //                           child: Stack(
  //                             children: [
  //                               CachedNetworkImage(
  //                                 imageUrl: guildClass.tourist_img.length>0?  guildClass.tourist_img[0]:guildClass.avatar,
  //                                 fit: BoxFit.fill ,
  //                               placeholder: (context, url) => AspectRatio(
  //                                 aspectRatio: 1,
  //                                 child: FractionallySizedBox(
  //                                   heightFactor: 0.5,
  //                                   widthFactor: 0.5,
  //                                   child: SpinKitRing(
  //                                     lineWidth:5,
  //                                     color:
  //                                     OwnColors.tran ,
  //                                   ),
  //                                 ),
  //                               ),
  //                                 errorWidget: (context, url, error) => Icon(Icons.error,color: OwnColors.black.withOpacity(0.5),),
  //                                 imageBuilder: (context, imageProvider) => Container(
  //
  //                                   decoration: BoxDecoration(
  //                                     color: OwnColors.black,
  //                                     borderRadius: BorderRadius.all(Radius.circular(15.0)),
  //
  //                                     image: DecorationImage(
  //                                         image: imageProvider, fit: BoxFit.cover),
  //                                   ),
  //                                 ),
  //                                 // width: MediaQuery.of(context).size.width,
  //
  //                               ),
  //                               // Positioned(
  //                               //     right: 10,
  //                               //     top: 10,
  //                               //     child: Padding(
  //                               //       padding: const EdgeInsets.only(left:0.0,right: 0),
  //                               //       child: Container(
  //                               //         height: 0.03*ScreenSize().getHeight(context),
  //                               //         child: guildClass.is_collect.contains("1")? Icon(SFSymbols.heart_fill, color: OwnColors.CTourOrange,): Image.asset(Res.favorite),
  //                               //       ),)
  //                               //
  //                               // )
  //                             ],
  //                           ),
  //                         ),
  //                       ),
  //                     ),
  //                   ),
  //                   Spacer(flex: 15,),
  //                   Expanded(
  //                     flex: 150,
  //                     child: Center(
  //                       child: Column(
  //
  //                         mainAxisAlignment: MainAxisAlignment.center,
  //                         crossAxisAlignment: CrossAxisAlignment.center,
  //                         children: [
  //                           Row(
  //                             mainAxisAlignment: MainAxisAlignment.start,
  //                             crossAxisAlignment: CrossAxisAlignment.center,
  //                             children: [
  //                               Flexible(
  //                                 child: AutoSizeText(
  //
  //                                   guildClass.name,
  //                                   maxLines: 1,
  //                                   minFontSize: 16,
  //                                   overflow: TextOverflow.ellipsis,
  //                                   style: TextStyle(
  //                                     fontSize: 16.nsp,
  //                                     color: OwnColors.black,
  //
  //                                   ),
  //                                 ),
  //                               ),
  //                               Container(
  //                                 height: 0.02 * ScreenSize().getHeight(context),
  //                                 child: Padding(
  //                                   padding: const EdgeInsets.only(top:2.0,bottom: 2.0,left: 4),
  //                                   child: Image.asset(Res.star),
  //                                 ),
  //                               ),
  //                               AutoSizeText(
  //                                 guildClass.score  ,
  //                                 minFontSize: 1,
  //                                 style: TextStyle(
  //                                     fontSize: 12.nsp,
  //                                     color: OwnColors.black,
  //                                     fontWeight: FontWeight.bold
  //                                 ),
  //                               ),
  //                               AutoSizeText(
  //                                 " (" + guildClass.comment_num+S().home_rate_number+")",
  //                                 minFontSize: 1,
  //                                 style: TextStyle(
  //                                   fontSize: 12.nsp,
  //                                   color: OwnColors.black,
  //
  //                                 ),
  //                               )
  //
  //                             ],
  //                           ),
  //                           Row(
  //
  //                             mainAxisAlignment: MainAxisAlignment.start,
  //                             crossAxisAlignment: CrossAxisAlignment.center,
  //                             children: [
  //
  //                               Container(
  //                                 height: 0.02* ScreenSize().getHeight(context),
  //                                 child: Padding(
  //                                   padding: const EdgeInsets.only(top:2.0,bottom: 2.0,right: 4),
  //                                   child: Image.asset(Res.location),
  //                                 ),
  //                               ),
  //                               Flexible(
  //                                 child: AutoSizeText(
  //                                   guildClass.location.join(" | ") ,
  //
  //                                   minFontSize: 1,
  //                                   style: TextStyle(
  //                                     fontSize: 12.nsp,
  //                                     color: OwnColors.black,
  //
  //                                   ),
  //                                 ),
  //                               )
  //                             ],
  //                           ),
  //                           Row(
  //
  //                             mainAxisAlignment: MainAxisAlignment.start,
  //                             crossAxisAlignment: CrossAxisAlignment.center,
  //                             children: [
  //
  //                               Container(
  //                                 height: 0.02* ScreenSize().getHeight(context),
  //                                 child: Padding(
  //                                   padding: const EdgeInsets.only(top:2.0,bottom: 2.0,right: 4),
  //                                   child: Image.asset(Res.car),
  //                                 ),
  //                               ),
  //                               Flexible(
  //                                 child: AutoSizeText(
  //                                   S().home_have_car,
  //                                   minFontSize: 1,
  //                                   style: TextStyle(
  //                                     fontSize: 12.nsp,
  //                                     color: OwnColors.black,
  //
  //                                   ),
  //                                 ),
  //                               )
  //                             ],
  //                           ),
  //                           Container(height: 5,),
  //                           Align(
  //                             alignment: Alignment.topLeft,
  //                             child: Wrap(
  //
  //                               spacing: 5,
  //                               runSpacing : 5.0,
  //                               children: List.generate(
  //                                 guildClass.lang.length,
  //
  //                                     (i) {
  //                                   return Container(
  //                                       height: 0.023*ScreenSize().getHeight(context),
  //                                       decoration: BoxDecoration(
  //                                           color: OwnColors.white,
  //                                           borderRadius: BorderRadius.only(
  //                                               topLeft: Radius.circular(10),
  //                                               topRight: Radius.circular(10),
  //                                               bottomLeft: Radius.circular(10),
  //                                               bottomRight: Radius.circular(10)),
  //                                           border: Border.all(
  //                                               width: 0.5,color: OwnColors.CC8C8C8
  //                                           ),
  //                                           boxShadow: [
  //                                             BoxShadow(
  //                                               color: OwnColors.primaryText.withOpacity(0.4),
  //                                               spreadRadius: 1,
  //                                               blurRadius: 4,
  //                                               offset: Offset(0, 3), // changes position of shadow
  //                                             ),
  //                                           ]),
  //                                       child: Padding(
  //                                         padding: const EdgeInsets.only(left:8.0,right: 8,bottom: 0),
  //                                         child: AutoSizeText(guildClass.lang[i],
  //                                           textAlign: TextAlign.center,
  //                                           style: TextStyle(
  //                                               height: 1.3,
  //                                               fontSize: 12.nsp,
  //                                               color: OwnColors.black
  //                                           ),
  //                                         ),
  //                                       ));
  //                                 },
  //                               ),
  //                             ),
  //                           )
  //
  //                         ],
  //                       ),
  //                     ),
  //                   )
  //                 ],
  //               ),
  //               HorizontalSpace().create(context, 0.01),
  //             ],
  //           ),
  //         ),
  //       ),
  //       new Positioned.fill(
  //           child: new Material(
  //             color: Colors.transparent,
  //             child: new GestureDetector(
  //
  //               onTap: () => {tourClick(guildClass)},
  //             ),
  //           )),
  //     ],
  //   );
  // }

  void recommendClick(TravelClass travelClass) {


    PushNewScreen().normalPush(context, TravelPage(travelClass:travelClass,edit: false,));
  }
  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }
  void travelClick(TravelClass travelClass){

  }

  void tourClick(GuildClass guildClass){
    PushNewScreen().normalPush(context, GuilderPage(guildClass: guildClass,preview: false));
  }

  void getTouristAPI( ) async {

    ResultData resultData = await AppApi.getInstance().getTouristCollectList(context, true,  SharePre.prefs.getString(GDefine.user_id));
    if (resultData.isSuccess()) {
      print(resultData.data.toString());
      tourList.clear();
      for (Map<String, dynamic> a in resultData.data) {
        // GuildClass c = GuildClass.fromJson(a);
        // tourList.add(new GuildClass(id: c.id,tourist_img: [c.avatar],name: c.name,score: double.parse(c.score).toStringAsFixed(1),location: c.location ,comment_num: c.comment_num, avatar: c.avatar, lang: c.lang));
        tourList.add( GuildClass.fromJson(a));
        //bestTourList.add(new GuildClass(id: c.id,tourist_img: [c.avatar],name: c.name,score: double.parse(c.score).toStringAsFixed(1),location: c.location ,comment_num: c.comment_num, avatar: c.avatar, lang: c.lang));

      }

      showTourList.clear();
      showTourList.addAll(tourList);

      refreshController1.finishRefresh();
      refreshController1.resetFooter();

      setState(() {
        print("getTouristAPI");

      });
    } else {
      WarningDialog.showIOSAlertDialog(context, S().download_fail_title, S().download_fail_hint,S().confirm);
    }
  }

  void getArticleCollectListAPI( ) async {

    ResultData resultData = await AppApi.getInstance().getArticleCollectList(context, true,  SharePre.prefs.getString(GDefine.user_id));
    if (resultData.isSuccess()) {
      print(resultData.data.toString());
      hotTravelList.clear();
      for (Map<String, dynamic> a in resultData.data) {
        TravelClass travelClass = TravelClass.fromJson(a);
        travelClass.sectionTag = new List();
        print("duration " + travelClass.duration);

        if (a["content_list"] != null) {
          List<dynamic> content_list = jsonDecode(a["content_list"].toString());
          print("len " + content_list.length.toString());
          travelClass.contentListDynamic = new List();
          List<TravelSubClass> subList = new List();

          for (Map<String, dynamic> sub in content_list) {
            TravelSubClass travelSubClass = new TravelSubClass();
            List<dynamic> content_list2 = sub["contentList"];
            List<TravelContentClass> travelContentClassList = new List();
            Map<String, dynamic> dy = new Map();
            dy["title"] =
            sub["title"].toString() == "null" ? "" : sub["title"].toString();
            dy["type"] = "0";
            dy["description"] = "";
            dy["google"] = "";
            dy["image"] = [];
            travelClass.contentListDynamic.add(dy);

            for (Map<String, dynamic> subsub in content_list2) {
              TravelContentClass travelContentClass = new TravelContentClass();
              travelContentClass.image = List<String>.from(subsub["image"]);
              travelContentClass.image.removeWhere((element) =>
              element == "" || element == "null");
              travelContentClass.title = subsub["title"];
              travelContentClass.description =
              subsub["description"].toString() == "null"
                  ? ""
                  : subsub["description"].toString();
              travelContentClass.id = subsub["id"].toString();
              travelContentClass.google =
              subsub["google"].toString() == "null" ? "" : subsub["google"]
                  .toString();

              travelContentClass.key = new GlobalKey();
              travelContentClassList.add(travelContentClass);
            }
            travelSubClass.title =
            sub["title"].toString() == "null" ? "" : sub["title"].toString();
            travelSubClass.id = sub["id"].toString();
            travelSubClass.key = new GlobalKey();
            if(sub["title"]!="" && sub["title"].toString() != "null") {
              travelClass.sectionTag.add(new SectionClass(title: sub["title"], key:travelSubClass.key  ));
            }

            if (travelContentClassList.isNotEmpty) {
              travelSubClass.content = travelContentClassList;
            }

            subList.add(travelSubClass);
          }

          travelClass.contentList = subList;
          hotTravelList.add(travelClass);
        }


      }
      showTravelList.clear();
      showTravelList.addAll(hotTravelList);
      refreshController2.finishRefresh();
      refreshController2.resetFooter();

      setState(() {
        print("getArticleCollectListAPI");

      });
    } else {
      WarningDialog.showIOSAlertDialog(context, S().download_fail_title, S().download_fail_hint,S().confirm);
    }
  }


  Future<void> searchTravel() async {
    showTravelList.clear();


    if(searchEdit.text == ""){
      showTravelList.addAll(hotTravelList);

    }else {
      List<TravelClass> temp = hotTravelList.where((element) =>
      element.title.toLowerCase().contains(searchEdit.text.toLowerCase()) ||
          element.guilder_name.toLowerCase().contains(searchEdit.text.toLowerCase()) ||
          element.tag.indexWhere((cc) => cc.toString().toLowerCase().contains(searchEdit.text.toLowerCase())) != -1).toList();
      showTravelList.addAll(temp);

    }

    setState(() {

    });


  }
  Future<void> searchGuild() async {
    showTourList.clear();


    if(searchEdit.text == ""){
      showTourList.addAll(tourList);

    }else {
      List<GuildClass> temp = tourList.where((element) =>
      element.name.toLowerCase().contains(searchEdit.text.toLowerCase()) ||

          element.location.indexWhere((cc) => cc.toString().toLowerCase().contains(searchEdit.text.toLowerCase())) != -1).toList();
      showTourList.addAll(temp);

    }

    setState(() {

    });


  }


}
