import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:auto_size_text_field/auto_size_text_field.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:ctour/CreateWidget/HorizontalSpace.dart';
import 'package:ctour/CreateWidget/VerticalSpace.dart';
import 'package:ctour/Define/GDefine.dart';
import 'package:ctour/Firebase/FirebaseRealtime.dart';
import 'package:ctour/Login/LoginChoosePage.dart';
import 'package:ctour/Network/API/app_api.dart';
import 'package:ctour/Network/service/result_data.dart';
import 'package:ctour/generated/l10n.dart';
import 'package:ctour/tool/CropSample.dart';
import 'package:ctour/tool/CropTool.dart';
import 'package:ctour/tool/Loading.dart';

import 'package:ctour/tool/OwnColors.dart';
import 'package:ctour/tool/PushNewScreen.dart';
import 'package:ctour/tool/ScreenSize.dart';
import 'package:ctour/tool/SharePre.dart';
import 'package:ctour/tool/Tools.dart';
import 'package:ctour/tool/WarningDialog.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sfsymbols/flutter_sfsymbols.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import 'package:intl/intl.dart';

import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../res.dart';

class EditPersonalPage extends StatefulWidget {
  const EditPersonalPage({
    Key key,
  }) : super(key: key);

  @override
  _EditPersonalPageState createState() => _EditPersonalPageState();
}

class _EditPersonalPageState extends State<EditPersonalPage> {
  String avatar = "";
  String avatarFileName = "";
  String avatar_url = "";
  int nowTextLen = 0;
  TextEditingController nameEdit = new TextEditingController();

  bool change = false;

  @override
  Future<void> initState() {
    super.initState();
    avatar = SharePre.prefs.getString(GDefine.avatar).toString();
    avatar_url = SharePre.prefs.getString(GDefine.avatar).toString();
    nameEdit.text = SharePre.prefs.getString(GDefine.name);
    nowTextLen = nameEdit.text.toString().length;
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: OwnColors.white,
      child: GestureDetector(
        onTap: () {
          Tools().dismissK(context);
          print("onTap");
        },
        child: SafeArea(
          top: true,
          child: Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Container(
              height: 0.98.sh,
              child: Column(
                children: <Widget>[
                  HorizontalSpace().create(context, 0.0005),
                  Container(
                      height: 0.007.sh,
                      width: 0.2 * ScreenSize().getWidth(context),
                      decoration: const BoxDecoration(
                        color: OwnColors.white,
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                      )),
                  HorizontalSpace().create(context, 0.02),
                  Container(
                    height: 0.03.sh,
                    width: 0.9 * ScreenSize().getWidth(context),
                    child: Row(
                      children: [
                        GestureDetector(
                            onTap: () {

                              if(    change) {
                                WarningDialog.showIOSAlertDialog3(
                                    context,
                                    S().save_not_yet,
                                    S().save_not_yet_hint2,
                                    S().cancel,
                                    S().personal_edit_save,
                                    backClick,
                                    saveClick);
                              }else {
                                backClick();
                              }
                            },
                            child: Icon(SFSymbols.xmark)),
                      ],
                    ),
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          HorizontalSpace().create(context, 0.12),
                          Container(
                            height: 0.19.sh,
                            width: ScreenSize().getWidth(context) * 0.8,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                AspectRatio(
                                  aspectRatio: 1,
                                  child: ClipOval(
                                    child: Material(
                                      // color: Colors.transparent, // button color
                                      child: Container(
                                        height:
                                        0.05.sh,
                                        child:

                                        //   avatar.length > 5 &&! avatar.contains("AdminLTE")
                                             CachedNetworkImage(
                                          errorWidget:
                                              (context, url, error) =>
                                              Icon(
                                                Icons.error,
                                                color: Colors.red,
                                              ),
                                          imageUrl: avatar_url,
                                          fit: BoxFit.fitHeight,
                                          placeholder: (context, url) => AspectRatio(
                                            aspectRatio: 1,
                                            child: FractionallySizedBox(
                                              heightFactor: 0.5,
                                              widthFactor: 0.5,
                                              child: SpinKitRing(
                                                lineWidth:5,
                                                color:
                                                OwnColors.tran ,
                                              ),
                                            ),
                                          ),
                                          imageBuilder:
                                              (context, imageProvider) =>
                                              Container(
                                                // width: 80.0,
                                                // height: 80.0,
                                                decoration: BoxDecoration(
                                                  color: OwnColors.tran,
                                                  shape: BoxShape.circle,
                                                  image: DecorationImage(
                                                      image: imageProvider,
                                                      fit: BoxFit.cover),
                                                ),
                                              ),
                                          // width: MediaQuery.of(context).size.width,
                                          // placeholder: (context, url) => new CircularProgressIndicator(),
                                          // placeholder: new CircularProgressIndicator(),
                                        )
                                            //: Image.asset(Res.choose_img),
                                      ),
                                    ),
                                  ),
                                ),

                              ],
                            ),
                          ),
                          HorizontalSpace().create(context, 0.02),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(

                                child: Stack(alignment: Alignment.center, children: [
                                  Column(
                                    children: [
                                      Center(
                                          child: AutoSizeText(
                                            S().personal_edit_avatar,
                                            minFontSize: 10,
                                            maxFontSize: 30,
                                            style:GoogleFonts.inter(
                                                fontSize: 16.nsp,
                                                color: OwnColors.black,
                                                fontWeight: FontWeight.w700,
                                                decoration: TextDecoration.underline
                                            ),
                                            maxLines: 1,
                                          )),

                                    ],
                                  ),
                                  new Positioned.fill(
                                      child: new Material(
                                        color: Colors.transparent,
                                        child: new GestureDetector(

                                          onTap: () => {
                                            // Loading.show(context)
                                            showBottomMenu()

                                          },
                                        ),
                                      )),
                                ]),
                              ),
                            ],
                          ),
                          HorizontalSpace().create(context, 0.05),
                          Container(
                            width: 0.68 * ScreenSize().getWidth(context),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              //列布局
                              children: <Widget>[

                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: AutoSizeText(

                                    S().personal_edit_name,

                                    minFontSize: 10,
                                    maxFontSize: 50,
                                    style:GoogleFonts.inter(
                                      fontSize: 16.nsp,
                                      color: OwnColors.black,
                                      fontWeight: FontWeight.w700,
                                    ),
                                    maxLines: 1,
                                  ),
                                ),

                                HorizontalSpace().create(context, 0.02),
                                Container(
                                  height: 0.045.sh,
                                  decoration: BoxDecoration(
                                    color: OwnColors.white,
                                    border: Border(
                                      top: BorderSide(width: 1.0, color: OwnColors.C989898),
                                      bottom: BorderSide(width: 1.0, color: OwnColors.C989898),
                                      left:BorderSide(width: 1.0, color:  OwnColors.C989898),
                                      right: BorderSide(width: 1.0, color:  OwnColors.C989898),
                                    ),
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        topRight: Radius.circular(10),
                                        bottomLeft: Radius.circular(10),
                                        bottomRight: Radius.circular(10)),
                                  ),
                                  alignment: Alignment.center,
                                  child:  TextField(
                                    controller: nameEdit,
                                    maxLength: 64,
                                    decoration: InputDecoration(
                                        hintText: S().name_hint,
                                        fillColor: OwnColors.white,
                                        contentPadding: EdgeInsets.symmetric(horizontal: 10),
                                        //Change this value to custom as you like
                                        // isDense: true,
                                        counterStyle:
                                        TextStyle(color: Colors.transparent),
                                        counterText: "",
                                        filled: true,
                                        border: OutlineInputBorder(
                                            borderRadius:
                                            new BorderRadius.circular(20.0),
                                            borderSide: BorderSide.none)
                                    ),
                                    onChanged:(text){
                                      nowTextLen = nameEdit.text.toString().length;
                                      change = true;
                                      setState(() {

                                      });
                                    },

                                    style:GoogleFonts.inter(
                                      fontSize: 12.nsp,
                                      color: OwnColors.black,
                                      fontWeight: FontWeight.w300,
                                    ),
                                    maxLines: 1,
                                  ),
                                ),
                                Row(
                                  children: [
                                    Spacer(),
                                    Padding(
                                      padding: const EdgeInsets.only(top:2.0),
                                      child: Text(
                                        nowTextLen.toString() + "/"+"64",
                                        style:GoogleFonts.inter(
                                          fontSize: 12.nsp,
                                          color: nowTextLen <= 64 ?OwnColors.C989898:Colors.red,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    )
                                  ],
                                )

                              ],
                            ),
                          ),
                          HorizontalSpace().create(context, 0.028),
                          Container(
                            height:  0.05.sh,
                            width: ScreenSize().getWidth(context) * 0.68,
                            child: FlatButton(
                              color: nowTextLen.bitLength > 0?OwnColors.CTourOrange:OwnColors.CC8C8C8,
                              splashColor: OwnColors.tran,
                              textColor: OwnColors.white,

                              highlightColor: Colors.transparent,
                              // elevation: 10,
                              child: Padding(
                                padding: const EdgeInsets.all(6.0),
                                child: AutoSizeText(
                                  S().personal_edit_save,
                                  minFontSize: 10,
                                  maxFontSize: 30,
                                  style:GoogleFonts.inter(
                                    fontSize: 16.nsp,
                                    color: OwnColors.white,
                                    fontWeight: FontWeight.w300,
                                  ),
                                  maxLines: 1,
                                ),
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0),
                                // side: BorderSide(color: Colors.red)
                              ),
                              onPressed: saveClick,
                            ),
                          ),
                          HorizontalSpace().create(context, 0.13),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Stack(
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Container(height: 0.03.sh,child: Icon(SFSymbols.trash)),
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: AutoSizeText(

                                          S().personal_edit_delete,

                                          minFontSize: 10,
                                          maxFontSize: 50,
                                          style:GoogleFonts.inter(
                                              fontSize: 16.nsp,
                                              color: OwnColors.black.withOpacity(1),
                                              fontWeight: FontWeight.w500,
                                              decoration: TextDecoration.underline
                                          ),
                                          maxLines: 1,
                                        ),
                                      ),
                                    ],
                                  ),
                                  new Positioned.fill(
                                      child: new Material(
                                        color: Colors.transparent,
                                        child: new GestureDetector(

                                          onTap: () => {delete()},
                                        ),
                                      ))
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void closeBtn() {
    Navigator.of(context).pop();
  }

  void delete() async {
    WarningDialog.showIOSAlertDialog2(
        context, S().delete_account_title, S().delete_account_content, S().delete, sure2Delete);
  }

  void sure2Delete() async {

    if(await  FirebaseAuth.instance.currentUser == null){
      WarningDialog.showIOSAlertDialogWithFunc(context, S().personal_edit_delete_no_firebase_title, S().personal_edit_delete_no_firebase_hint, S().confirm,sure2logout);
    }else {

      ResultData resultData = await AppApi.getInstance().delete_user(context, true, SharePre.prefs.getString(GDefine.user_id));
      if (resultData.isSuccess()) {
        print(resultData.data.toString());
        FirebaseAuth.instance.currentUser.delete();
        PushNewScreen().popAllAndPush(context, LoginChoosePage());
        setState(() {
          print("delete");
        });
      } else {
        WarningDialog.showIOSAlertDialog(
            context,
            S().personal_edit_delete_fail_title,
            S().personal_edit_delete_fail_hint,
            S().confirm);
      }
    }

  }



  void backClick() {
    print("back");
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }

  @override
  void dispose() {
    print("dispose");

    super.dispose();
  }

  @override
  void deactivate() {
    print("deactivate");

    super.deactivate();
  }

  void showBottomMenu() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
              height: 150,
              child: Column(children: <Widget>[
                ListTile(
                  leading: Icon(Icons.photo_camera),
                  title: Text(S().take_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromCamera();
                  },
                ),
                ListTile(
                  leading: Icon(Icons.photo_library),
                  title: Text(S().choose_pic),
                  onTap: () {
                    Navigator.pop(context);
                    pcikImageFromPhoto();
                  },
                )
              ]));
        });
  }

  final picker = ImagePicker();

  void pcikImageFromCamera( ) async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.camera,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);

    File img = File(pickedFile.path);
    Uint8List imageBytes = await pickedFile.readAsBytes();

    CroppedFile  croppedFile = await CropTool().crop(1, 1, CropAspectRatioPreset.square, true, img.path);
    if(croppedFile != null) {
      imageBytes = await croppedFile.readAsBytes();

      String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + base64Image);
      _uploadImage(base64Image);

      print(croppedFile);
    }


  }


  // void pcikImageFromCamera() async {
  //   PickedFile pickedFile = await picker.getImage(
  //       source: ImageSource.camera,
  //       maxHeight: 1440,
  //       maxWidth: 1440,
  //       imageQuality: 80);
  //
  //   File img = File(pickedFile.path);
  //   List<int> imageBytes = await img.readAsBytes();
  //   // img22.Image capturedImage =
  //   // img22.decodeImage(await File(pickedFile.path).readAsBytes());
  //   // img22.Image orientedImage = img22.bakeOrientation(capturedImage);
  //   // img = await File(pickedFile.path)
  //   //     .writeAsBytes(img22.encodeJpg(orientedImage));
  //   avatar = img.path;
  //   avatarFileName = avatar.split("/").last;
  //   String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
  //   print("base64Image: " + base64Image);
  //   _uploadImage(base64Image);
  //
  //   setState(() {});
  // }

  void pcikImageFromPhoto() async {
    PickedFile pickedFile = await picker.getImage(
        source: ImageSource.gallery,
        maxHeight: 1440,
        maxWidth: 1440,
        imageQuality: 80);
    File img = File(pickedFile.path);
    Uint8List imageBytes = await pickedFile.readAsBytes();
    CroppedFile  croppedFile = await CropTool().crop(1, 1, CropAspectRatioPreset.square, true, img.path);
    if(croppedFile != null) {
      imageBytes = await croppedFile.readAsBytes();

      String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
      print("base64Image: " + base64Image);
      _uploadImage(base64Image);

      print(croppedFile);
    }

  }



  // void pcikImageFromPhoto() async {
  //   PickedFile pickedFile = await picker.getImage(
  //       source: ImageSource.gallery,
  //       maxHeight: 1440,
  //       maxWidth: 1440,
  //       imageQuality: 80);
  //   File img = File(pickedFile.path);
  //   List<int> imageBytes = await img.readAsBytes();
  //   avatar = img.path;
  //   avatarFileName = avatar.split("/").last;
  //
  //   String base64Image = "data:image/jpg;base64," + base64Encode(imageBytes);
  //   print("base64Image: " + base64Image);
  //   _uploadImage(base64Image);
  //
  //   setState(() {});
  // }

  Future<void> _uploadImage(String data) async {
    // Loading.show(context);

    ResultData resultData =
        await AppApi.getInstance().setFileBase64(context, true, data);

    if (resultData.isSuccess()) {
      // Loading.dismiss(context);
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data);
      avatar_url = GDefine.imageUrl + resultData.data["path"];
      print("avatar_url " + avatar_url);
      setState(() {

      });
      change = true;
    } else {
      // Loading.dismiss(context);
      WarningDialog.showIOSAlertDialog(context, S().upload_fail_title,
          S().upload_fail_title + S().upload_fail_hint, S().confirm);
    }
  }

  void saveClick() async {
    print("updateUserAPI");

    if (!Tools().stringNotNullOrSpace(nameEdit.text)) {
      WarningDialog.showIOSAlertDialog(
          context,
          S().personal_edit_name_empty_title,
          S().personal_edit_name_empty_hint,
          S().confirm);
    } else if (nameEdit.text.length > 64) {
      WarningDialog.showIOSAlertDialog(
          context,
          S().personal_edit_name_overflow_title,
          S().personal_edit_name_overflow_hint,
          S().confirm);
    } else {
      Loading.show(context);

      ResultData resultData = await AppApi.getInstance().setUserInfoAvatarName(
          context,
          true,
          SharePre.prefs.getString(GDefine.user_id),
          nameEdit.text,
          avatar_url);

      // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
      if (resultData.isSuccess()) {
        // Notice how you have to call body from the response if you are using http to retrieve json
        print(resultData.data.toString());
        loginAPI();
      } else {
        WarningDialog.showIOSAlertDialog(
            context, S().update + S().fail, S().update + S().fail, S().confirm);
      }
    }
  }

  void loginAPI() async {
    print("loginAPI");

    ResultData resultData = await AppApi.getInstance().login(
        context,
        true,
        SharePre.prefs.getString(GDefine.firebase_id),
        SharePre.prefs.getString(GDefine.login_type));

    // Navigator.pushNamed(context, '/LobbyPage',  arguments: {'name': 'Raymond'});
    if (resultData.isSuccess()) {
      // Notice how you have to call body from the response if you are using http to retrieve json
      print(resultData.data.toString());

      SharePre.prefs.setString(GDefine.name, resultData.data["name"]);
      SharePre.prefs
          .setString(GDefine.user_id, resultData.data["user_id"].toString());
      SharePre.prefs
          .setString(GDefine.avatar, resultData.data["avatar"].toString());
      SharePre.prefs
          .setString(GDefine.gender, resultData.data["gender"].toString());
      SharePre.prefs.setString(GDefine.want_companion, resultData.data["want_companion"].toString());
      FirebaseRealtime.pushToChatPerson();
      Loading.dismiss(context);
      backClick();
    } else  if(resultData.data.toString().contains("停用")){


      WarningDialog.showIOSAlertDialog2(context,S().account_unactivated_title, S().account_unactivated_hint,S().confirm,sure2logout);

    }
  }

  Future<void> sure2logout() async {
    SharePre.prefs.setInt(GDefine.isLogin, 0);
    if(await FirebaseAuth.instance.currentUser != null){
      await FirebaseAuth.instance.signOut();
    }
    PushNewScreen().popAllAndPush(context, LoginChoosePage());
  }


}
